<?php

namespace App\Controller;
use App\CoreCinex\CinexService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use App\Lib\DaoLib;
use App\Lib\ApiUtilFunctions;
use App\Lib\Constantes;
use App\CoreCinex\ComplejoFactory;

class ComplejoController extends Controller
{
     


    // Complejo Detalle URI"'s
    /**
     * @Rest\Get("/api/complejos/complejo/detalle/{id}", name="getcomplejodetalle", defaults={"_format":"json"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Detalle de Complejo."
     * )
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="string",
     *     description="Id Complejo"
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Detalle de Complejos"
     * )
     *
     * @SWG\Tag(name="ComplejosDetalle")
     */
    public function getComplejoDetallesAction(Request $request,$id) {
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $data= [];
        $data = $em->getRepository("App:Complejo")->getDetallesComplejo($id);
        foreach($data as $clave=>$valor){
            if(isset($data[$clave]["servicios"])){
                $listaServicios=explode(";",$data[$clave]["servicios"]);
                foreach($listaServicios as $row){
                    if(strlen($row)>1)
                        $data[$clave]["arrayServicios"][]=array("servicios"=>$row);
                }
            }
        }
        if (is_null($data)) {
            $data = [];
        }   
        return $http->responseAcceptHttp(array("data"=>$data));
    }

     // Complejos URI"'s
    /**
     * @Rest\Get("/api/complejos/", name="getcomplejos", defaults={"_format":"json"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Complejos."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Complejos"
     * )
     *
     * @SWG\Tag(name="Complejos")
     */
    public function getComplejosAction(Request $request) {
        $complejoFactory = new ComplejoFactory();
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $data= [];
        $data = $em->getRepository("App:Complejo")->getComplejos();
        
        
        
        foreach($data as $clave=>$valor){
            $complejos[]=array(
                            "direccion"=>$data[$clave]["direccion"],
                            "place_id"=>$data[$clave]["place_id"],
                            'ciudad'=>array(
                                            "id"=>$data[$clave]["ciudadid"],
                                            "descripcion"=>$data[$clave]["nombreCiudad"]
                                      ),
                            "urlImagen1"=>$data[$clave]["urlImagen1"],
                            "id"=>$data[$clave]["id"],
                            "nombre"=>$data[$clave]["nombre"], 
                            "urlImagen8"=>$data[$clave]["urlImagen8"],
                            "urlImagen7"=>$data[$clave]["urlImagen7"],
                            "urlImagen6"=>$data[$clave]["urlImagen6"],
                            "descripcionBoletos"=>$data[$clave]["descripcionBoletos"],
                            "urlImagen5"=>$data[$clave]["urlImagen5"],
                            "urlImagen4"=>$data[$clave]["urlImagen4"],
                            "urlImagen3"=>$data[$clave]["urlImagen3"],
                            "urlImagen2"=>$data[$clave]["urlImagen2"],
                            "latitud"=>$data[$clave]["latitud"],
                            "longitud"=>$data[$clave]["longitud"],
                            "esproductivo"=>$data[$clave]["esproductivo"],
                            "servicios"=>$complejoFactory->getServicios($data[$clave]["servicios"])
            );
        }
        if (is_null($data)) {
            $data = [];
        }   
        return $http->responseAcceptHttp(array("data"=>$data));
    }
}
