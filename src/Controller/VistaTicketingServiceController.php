<?php
namespace App\Controller;
use App\CoreCinex\CinexService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use App\Lib\DaoLib;
use App\Lib\ApiUtilFunctions;
use App\Lib\Constantes;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Translation\TranslatorInterface;
use App\Lib\XmlLib;
class VistaTicketingServiceController extends Controller
{     
    protected $client;
    protected $params;
    protected $arrayConnect;
    protected $xmlLib;
    
    /**
     * Constructor.
    **/
    public function __construct(ParameterBagInterface $params)
    {
        $this->xmlLib= new XmlLib();
        $this->params=$params;
        $this->client=new \SoapClient($this->params->get('soap.cinexdataservice.wsdl'), array("trace" => 1, "exception" => 1,'encoding'=>'UTF8'));
        
        $this->clientTicketing =  new \nusoap_client($this->params->get('soap.cinexdataservice.wsdl'),'wsdl');
        $this->clientTicketing->soap_encoding = 'UTF-8';
        $this->clientTicketing->decode_utf8 = TRUE;
    }
        
    private function getOrdenarLetras($numcolumnas, $numfilas, $rowdata, $letrasfiltrodata, $areacategorycode){
        $contletras=0;
        $counter = 0;
        $notfound = true;
        $letrasrow = "";
        $letrasrownf = "";
        $dataseats = array();
        $i=0;        
        $AreaCat_strCode = "";
        foreach ($letrasfiltrodata as $letrasfiltro) {
            $aja=$letrasfiltro;            
            for ($x = 0; $x < $numcolumnas; $x++){
                for ($y = 0; $y < $numfilas; $y++){
                    $notfound = true;
                    $letrasrow = "";
                    $letrasrownf = "";
                    for ($w = 0; $w < sizeof($rowdata); $w++){
                        for ($z = 0; $z < sizeof($rowdata[$w]["children"]["seats"][0]["children"]["seat"]); $z++){
                            if (intval($rowdata[$w]["children"]["seats"][0]["children"]["seat"][$z]["children"]["position"][0]["children"]["rowindex"][0]["text"]) == $y && intval($rowdata[$w]["children"]["seats"][0]["children"]["seat"][$z]["children"]["position"][0]["children"]["columnindex"][0]["text"]) == $x){
                                $letrasrow = $rowdata[$w]["children"]["physicalname"][0]["text"];
                                if ($letrasfiltro == $letrasrow){
                                    $numAread = $rowdata[$w]["children"]["seats"][0]["children"]["seat"][$z]["children"]["position"][0]["children"]["areanumber"][0]["text"];
                                    if (strtoupper($rowdata[$w]["children"]["seats"][0]["children"]["seat"][$z]["children"]["status"][0]["text"]) == "WALL"){
                                        $dataseats[$counter] = array("codArea"=>null,"numArea"=>null,"area"=>"null","letter"=>$letrasrow, "row"=>$y, "column"=>$x, "status"=>strtoupper($rowdata[$w]["children"]["seats"][0]["children"]["seat"][$z]["children"]["status"][0]["text"]),"id"=>"null");
                                    }else{
                                        $dataseats[$counter] = array("codArea"=>$areacategorycode,"numArea"=>$numAread,"area"=>$numAread,"letter"=>$letrasrow, "row"=>$y, "column"=>$x, "status"=>strtoupper($rowdata[$w]["children"]["seats"][0]["children"]["seat"][$z]["children"]["status"][0]["text"]),"id"=>$rowdata[$w]["children"]["seats"][0]["children"]["seat"][$z]["children"]["id"][0]["text"]);
                                    }
                                    $counter++;
                                    $notfound = false;
                                }
                                break;
                            }
                        }
                    }
                    if ($notfound){                        
                        $letrasrow = $rowdata[$y]["children"]["physicalname"][0]["text"];
                        if ($letrasfiltro == $letrasrow){
                            $dataseats[$counter] = array("codArea"=>null,"numArea"=>null,"area"=>"null","letter"=>$letrasrow, "row"=>$y, "column"=>$x, "status"=>"WALL","id"=>"null");
                            $counter++;
                        }
                    }
                }
            }            
        }        
        return $dataseats;            
    }
        
    /**
     * @Rest\Get("/api/vistaticketingservice/cancelorder/usersession/{usersession}", name="vistaTicketingServiceCancelOrder", defaults={"_format":"json"})
     *
     * @SWG\Parameter(
     *     name="usersession",
     *     in="path",
     *     type="string",
     *     description="cadena que indica el identificador de sesión en VISTA."
     * )

     * @SWG\Response(
     *     response=200,
     *     description="respuesta del servicio."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de CancelOrder"
     * )
     *
     * @SWG\Tag(name="VistaTicketingCancelOrder")
     */
    
     public function cancelOrderAction($usersession) {
        $http = $this->get('httpcustom');        
        $xml_data = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://vista.co.nz/services/WSVistaWebClient.ServiceContracts/1" xmlns:ns1="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/"><soapenv:Header/><soapenv:Body><ns:CancelOrderRequest><ns:UserSessionId>{USER_SESSION}</ns:UserSessionId></ns:CancelOrderRequest></soapenv:Body></soapenv:Envelope>';
        $xml_data = str_replace("{USER_SESSION}", $usersession, $xml_data);
        $ch = curl_init("http://172.20.5.242/WSVistaWebClient/TicketingService.asmx");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml', 'soapaction: http://vista.co.nz/services/WSVistaWebClient.ServiceContracts/1/CancelOrder'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml_data");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);    
        if (strpos($output, "OK") !== false){
            $data = array("result"=>true, "error"=>false, "response"=>"OK", "data"=>"Cancelada la orden del usuario con el sessionid = [".$usersession."]");
        }else{
            $data = array("result"=>true, "error"=>false, "response"=>"No existe orden para cancelar", "data"=>"");
        }
        curl_close($ch);
        return $http->responseAcceptHttpVista($data);
    } 
    
    /**
     * @Rest\Get("/api/vistaticketingservice/getorder/sessionid/{sessionid}", name="vistaTicketingServiceGetOrder", defaults={"_format":"json"})
     *
     * @SWG\Parameter(
     *     name="sessionid",
     *     in="path",
     *     type="string",
     *     description="cadena que indica el identificador de sesión de un usuario en VISTA."
     * )
     
     * @SWG\Response(
     *     response=200,
     *     description="respuesta del servicio."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de GetOrder"
     * )
     *
     * @SWG\Tag(name="VistaTicketingGetOrder")
     */
    
    public function getOrderAction($sessionid) {
        $http = $this->get('httpcustom');
        $xml_data = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://vista.co.nz/services/WSVistaWebClient.ServiceContracts/1" xmlns:ns1="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/"><soapenv:Header/><soapenv:Body><ns:GetOrderRequest><ns1:OptionalClientClass>{OPTIONAL_CLIENT_CLASS}</ns1:OptionalClientClass><ns1:OptionalClientId>{OPTIONAL_CLIENT_ID}</ns1:OptionalClientId><ns1:OptionalClientName>{OPTIONAL_CLIENT_NAME}</ns1:OptionalClientName><ns:UserSessionId>{SESSION_ID}</ns:UserSessionId></ns:GetOrderRequest></soapenv:Body></soapenv:Envelope>';
        $xml_data = str_replace("{OPTIONAL_CLIENT_CLASS}", "WWW", $xml_data);
        $xml_data = str_replace("{OPTIONAL_CLIENT_ID}", "111.111.111.111", $xml_data);
        $xml_data = str_replace("{OPTIONAL_CLIENT_NAME}", "WEBSERVER", $xml_data);
        $xml_data = str_replace("{SESSION_ID}", $sessionid, $xml_data);
        $ch = curl_init("http://172.20.5.242/WSVistaWebClient/TicketingService.asmx");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml', 'soapaction: http://vista.co.nz/services/WSVistaWebClient.ServiceContracts/1/GetOrder'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml_data");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        if (strpos($output, "OK") !== false){
            $data = array("result"=>true, "response"=>"OK", "data"=>$output, "error"=>false);            
        }else{
            $data = array("result"=>false, "response"=>"Error en la llamada getOrder ".$output, "error"=>true, "data"=>"");
        }
        curl_close($ch);
        return $http->responseAcceptHttpVista($data);        
    }
    
    
    /**
     * @Rest\Get("/api/vistaticketingservice/addtvouchert/datatickets/{datatickets}/params/{params}/mode/{mode}", name="vistaTicketingServiceAddVoucher", defaults={"_format":"json"})
     *
     * @SWG\Parameter(
     *     name="datatickets",
     *     in="path",
     *     type="string",
     *     description="cadena separada con ';' que contiene la información de los tickets a agregar."
     * )
     * @SWG\Parameter(
     *     name="params",
     *     in="path",
     *     type="string",
     *     description="cadena separada con ';' que contiene la información para la transacción contra VISTA."
     * )
     * @SWG\Parameter(
     *     name="mode",
     *     in="path",
     *     type="string",
     *     description="indica si se debe enviar el VOUCHER como parte de esta petición, 1 - ENVIAR VOUCHER, 2 - NO ENVIAR VOUCHER."
     * )
      
     * @SWG\Response(
     *     response=200,
     *     description="respuesta del servicio."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de AddVoucher"
     * )
     *
     * @SWG\Tag(name="VistaTicketingAddVoucher")
     */
    
    public function addVoucherAction($datatickets, $params, $mode){
        $http = $this->get('httpcustom');
        $xml_ticket_structure = "";
        $array_tickets = explode(";", $datatickets);
        $array_params = explode(";", $params);
        if ($mode == "1"){
            $xml_data = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://vista.co.nz/services/WSVistaWebClient.ServiceContracts/1" xmlns:ns1="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/"><soapenv:Header/><soapenv:Body><ns:AddTicketsRequest><ns1:OptionalClientClass>{OPTIONAL_CLIENT_CLASS}</ns1:OptionalClientClass><ns1:OptionalClientId>{OPTIONAL_CLIENT_ID}</ns1:OptionalClientId><ns1:OptionalClientName>{OPTIONAL_CLIENT_NAME}</ns1:OptionalClientName><ns:UserSessionId>{USER_SESSION}</ns:UserSessionId><ns:CinemaId>{CINEMA_ID}</ns:CinemaId><ns:SessionId>{SESSION_ID}</ns:SessionId><ns:TicketTypes>{TICKET_TYPE_CODE}</ns:TicketTypes><ns:ReturnOrder>true</ns:ReturnOrder><ns:ReturnSeatData>true</ns:ReturnSeatData><ns:ProcessOrderValue>0</ns:ProcessOrderValue><ns:UserSelectedSeatingSupported>true</ns:UserSelectedSeatingSupported><ns:SkipAutoAllocation>true</ns:SkipAutoAllocation><ns:IncludeAllSeatPriorities>true</ns:IncludeAllSeatPriorities><ns:IncludeSeatNumbers>true</ns:IncludeSeatNumbers><ns:ExcludeAreasWithoutTickets>true</ns:ExcludeAreasWithoutTickets><ns:ReturnDiscountInfo>true</ns:ReturnDiscountInfo><ns:BookingFeeOverride>1</ns:BookingFeeOverride><ns:BookingMode>2</ns:BookingMode><ns:ReorderSessionTickets>true</ns:ReorderSessionTickets><ns:ReturnSeatDataFormat>2</ns:ReturnSeatDataFormat></ns:AddTicketsRequest></soapenv:Body></soapenv:Envelope>';
            $xml_ticket_structure.= '<ns:TicketType><ns1:TicketTypeCode>{TICKET_TYPE_CODE}</ns1:TicketTypeCode><ns1:Qty>{TICKET_QUANTITY}</ns1:Qty><ns1:PriceInCents>0</ns1:PriceInCents><ns1:OptionalBarcode>{TICKET_BARCODE}</ns1:OptionalBarcode><ns1:OptionalAreaCatSequence>{TICKET_AREACATSEQUENCE}</ns1:OptionalAreaCatSequence><ns1:BookingFeeOverride>1</ns1:BookingFeeOverride><ns1:LoyaltyRecognitionSequence>0</ns1:LoyaltyRecognitionSequence><ns1:PromotionTicketTypeId>0</ns1:PromotionTicketTypeId><ns1:PromotionInstanceGroupNumber>0</ns1:PromotionInstanceGroupNumber><ns1:PriceCardPriceInCents>0</ns1:PriceCardPriceInCents></ns:TicketType>';
        }
        if ($mode == "2"){
            $xml_data = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://vista.co.nz/services/WSVistaWebClient.ServiceContracts/1" xmlns:ns1="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/"><soapenv:Header/><soapenv:Body><ns:AddTicketsRequest><ns1:OptionalClientClass>{OPTIONAL_CLIENT_CLASS}</ns1:OptionalClientClass><ns1:OptionalClientId>{OPTIONAL_CLIENT_ID}</ns1:OptionalClientId><ns1:OptionalClientName>{OPTIONAL_CLIENT_NAME}</ns1:OptionalClientName><ns:UserSessionId>{USER_SESSION}</ns:UserSessionId><ns:CinemaId>{CINEMA_ID}</ns:CinemaId><ns:SessionId>{SESSION_ID}</ns:SessionId><ns:TicketTypes>{TICKET_TYPE_CODE}</ns:TicketTypes><ns:ReturnOrder>true</ns:ReturnOrder><ns:ReturnSeatData>true</ns:ReturnSeatData><ns:ProcessOrderValue>0</ns:ProcessOrderValue><ns:UserSelectedSeatingSupported>true</ns:UserSelectedSeatingSupported><ns:SkipAutoAllocation>true</ns:SkipAutoAllocation><ns:IncludeAllSeatPriorities>true</ns:IncludeAllSeatPriorities><ns:IncludeSeatNumbers>true</ns:IncludeSeatNumbers><ns:ExcludeAreasWithoutTickets>true</ns:ExcludeAreasWithoutTickets><ns:ReturnDiscountInfo>true</ns:ReturnDiscountInfo><ns:BookingFeeOverride>1</ns:BookingFeeOverride><ns:BookingMode>2</ns:BookingMode><ns:ReorderSessionTickets>true</ns:ReorderSessionTickets><ns:ReturnSeatDataFormat>2</ns:ReturnSeatDataFormat></ns:AddTicketsRequest></soapenv:Body></soapenv:Envelope>';
            $xml_ticket_structure.= '<ns:TicketType><ns1:TicketTypeCode>{TICKET_TYPE_CODE}</ns1:TicketTypeCode><ns1:Qty>{TICKET_QUANTITY}</ns1:Qty><ns1:PriceInCents>0</ns1:PriceInCents><ns1:OptionalAreaCatSequence>{TICKET_AREACATSEQUENCE}</ns1:OptionalAreaCatSequence><ns1:BookingFeeOverride>1</ns1:BookingFeeOverride><ns1:LoyaltyRecognitionSequence>0</ns1:LoyaltyRecognitionSequence><ns1:PromotionTicketTypeId>0</ns1:PromotionTicketTypeId><ns1:PromotionInstanceGroupNumber>0</ns1:PromotionInstanceGroupNumber><ns1:PriceCardPriceInCents>0</ns1:PriceCardPriceInCents></ns:TicketType>';
        }
        // *** INICIO ARMAR ESTRUCTURA DE LOS TICKETS ***
        $x = 0;
        $counter = 0;
        $ticket_type = "";
        $ticket_type = $xml_ticket_structure;
        for ($x = 0; $x < sizeof($array_tickets); $x++){
            if ($counter == 0){
                $ticket_type = str_replace("{TICKET_TYPE_CODE}", $array_tickets[$x], $ticket_type);
            }
            if ($counter == 1){
                $ticket_type = str_replace("{TICKET_QUANTITY}", $array_tickets[$x], $ticket_type);
            }
            if ($counter == 2){
                $ticket_type = str_replace("{TICKET_PRICE}", $array_tickets[$x], $ticket_type);
            }
            if ($counter == 3 && $mode == 1){
                $ticket_type = str_replace("{TICKET_BARCODE}", $array_tickets[$x], $ticket_type);
            }
            if ($counter == 4){
                $ticket_type = str_replace("{TICKET_AREACATSEQUENCE}", $array_tickets[$x], $ticket_type);
                $counter = 0;
                if ($x+1 != sizeof($array_tickets)){
                    $ticket_type.= $xml_ticket_structure;
                }else{
                    break;
                }
            }else{
                $counter++;
            }
        }
        if(count($array_params)>=8){
            $xml_data = str_replace("{TICKET_TYPE_CODE}", $ticket_type, $xml_data);
            // *** FIN ARMAR ESTRUCTURA DE LOS TICKETS ***
            $xml_data = str_replace("{OPTIONAL_CLIENT_CLASS}", $array_params[0], $xml_data);
            $xml_data = str_replace("{OPTIONAL_CLIENT_ID}", $array_params[1], $xml_data);
            $xml_data = str_replace("{OPTIONAL_CLIENT_NAME}", $array_params[2], $xml_data);
            $xml_data = str_replace("{USER_SESSION}", $array_params[3], $xml_data);
            $xml_data = str_replace("{CINEMA_ID}", $array_params[4], $xml_data);
            $xml_data = str_replace("{SESSION_ID}", $array_params[5], $xml_data);
            $xml_data = str_replace("{TICKET_STRUCTURE}", $ticket_type, $xml_data);
            $xml_data = str_replace("{TICKET_TYPE_CODE}", $array_params[6], $xml_data);
            $xml_data = str_replace("{TICKET_QUANTITY}", $array_params[7], $xml_data);
            $xml_data = str_replace("{TICKET_PRICE}", $array_params[8], $xml_data);
            $xml_data = str_replace("{TICKET_AREACATSEQUENCE}", $array_params[9], $xml_data);
            $ch = curl_init("http://172.20.5.242/WSVistaWebClient/TicketingService.asmx");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml', 'soapaction: http://vista.co.nz/services/WSVistaWebClient.ServiceContracts/1/AddTickets'));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);    
            $output = curl_exec($ch);
            if (strpos($output, "OK") !== false){
                $data = array("result"=>false, "response"=>"OK", "data"=>$output, "error"=>false);
            }else{
                $data = array("result"=>true, "response"=>"ERROR", "data"=>$output, "error"=>false, "response"=>"Error en la llamada addVoucher ".$output);
            }
            curl_close($ch);
        }else{
            $data = array("result"=>true, "response"=>"ERROR", "data"=>array(), "error"=>true, "response"=>"Error en parametros");            
        }    
        return $http->responseAcceptHttpVista($data); 
    }
    
    /**
     * @Rest\Get("/api/vistaticketingservice/addtickets/ticketdata/{ticketdata}/purchasedata/{purchasedata}", name="AddTickets", defaults={"_format":"json"})
     *
     * @SWG\Parameter(
     *     name="ticketdata",
     *     in="path",
     *     type="string",
     *     description="cadena separada con ';' que contiene la información de los tickets a agregar. Ejemplo de estructura => {TICKETTYPECODE};{QUANTITY};{PRICE};{AREACATEGORYSECUENCE}, para agregar varios tickets repetir la estructura separada por dos puntos ejemplo {TICKETTYPECODE};{QUANTITY};{PRICE};{AREACATEGORYSECUENCE}:{TICKETTYPECODE};{QUANTITY};{PRICE};{AREACATEGORYSECUENCE}"
     * )
     * @SWG\Parameter(
     *     name="purchasedata",
     *     in="path",
     *     type="string",
     *     description="cadena separada con ';' que contiene la información de la transaccion. Ejemplo de estructura => {CINEMAID};{SESSIONID};{USERSESSIONID};{LOYALTYCARDNUMBER};{RECOGNITIONID} en donde LOYALTYCARDNUMBER y RECOGNITIONID son valores para transacciones de cinexclusivo (loyalty) y son valores opcionales."
     * )
     * @SWG\Response(
     *     response=200,
     *     description="respuesta del servicio."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de AddTickets"
     * )
     *
     * @SWG\Tag(name="AddTickets")
     */    
    public function addTicketsAction($ticketdata, $purchasedata){
        $http = $this->get('httpcustom');
        $xml_ticket_structure = "";
        $xml_data = ""; 
        $result = "";
        $outputxml = "";
        $wsdl = "http://172.20.5.242/WSVistaWebClient/TicketingService.asmx";
        $array_tickets = array();
        $array_tickets_data = array();
        $array_purchase = array();
        $json_mapa = array();
        $array_tickets = explode(":", $ticketdata);
        $array_purchase = explode(";", $purchasedata);
        $xml_data = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://vista.co.nz/services/WSVistaWebClient.ServiceContracts/1" xmlns:ns1="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/"><soapenv:Header/><soapenv:Body><ns:AddTicketsRequest><ns1:OptionalClientClass>{OPTIONAL_CLIENT_CLASS}</ns1:OptionalClientClass><ns1:OptionalClientId>{OPTIONAL_CLIENT_ID}</ns1:OptionalClientId><ns1:OptionalClientName>{OPTIONAL_CLIENT_NAME}</ns1:OptionalClientName><ns:UserSessionId>{USER_SESSION}</ns:UserSessionId><ns:CinemaId>{CINEMA_ID}</ns:CinemaId><ns:SessionId>{SESSION_ID}</ns:SessionId><ns:TicketTypes>{TICKET_TYPE_CODE}</ns:TicketTypes><ns:ReturnOrder>true</ns:ReturnOrder><ns:ReturnSeatData>true</ns:ReturnSeatData><ns:ProcessOrderValue>true</ns:ProcessOrderValue><ns:UserSelectedSeatingSupported>true</ns:UserSelectedSeatingSupported><ns:SkipAutoAllocation>false</ns:SkipAutoAllocation><ns:IncludeAllSeatPriorities>true</ns:IncludeAllSeatPriorities><ns:IncludeSeatNumbers>1</ns:IncludeSeatNumbers><ns:ExcludeAreasWithoutTickets>true</ns:ExcludeAreasWithoutTickets><ns:ReturnDiscountInfo>false</ns:ReturnDiscountInfo><ns:BookingFeeOverride>1</ns:BookingFeeOverride><ns:BookingMode>1</ns:BookingMode><ns:ReorderSessionTickets>true</ns:ReorderSessionTickets><ns:ReturnSeatDataFormat>2</ns:ReturnSeatDataFormat></ns:AddTicketsRequest></soapenv:Body></soapenv:Envelope>';
        $xml_ticket_structure = '<ns:TicketType><ns1:TicketTypeCode>{TICKET_TYPE_CODE}</ns1:TicketTypeCode><ns1:Qty>{TICKET_QUANTITY}</ns1:Qty><ns1:PriceInCents>{TICKET_PRICE}</ns1:PriceInCents><ns1:OptionalAreaCatSequence>{TICKET_AREACATSEQUENCE}</ns1:OptionalAreaCatSequence><ns1:BookingFeeOverride>1</ns1:BookingFeeOverride>{RECOGNITIONID}<ns1:LoyaltyRecognitionSequence>0</ns1:LoyaltyRecognitionSequence><ns1:PromotionTicketTypeId>0</ns1:PromotionTicketTypeId><ns1:PromotionInstanceGroupNumber>0</ns1:PromotionInstanceGroupNumber><ns1:PriceCardPriceInCents>0</ns1:PriceCardPriceInCents></ns:TicketType>';
        // *** INICIO ARMAR ESTRUCTURA DE LOS TICKETS ***
        $x = 0;
        $counter = 0;
        $ticket_type = "";
        $ticket_type_list = "";
        for ($x = 0; $x < sizeof($array_tickets); $x++){
            $ticket_type = $xml_ticket_structure;
            $array_tickets_data = explode(";", $array_tickets[$x]);
            $ticket_type = str_replace("{TICKET_TYPE_CODE}", $array_tickets_data[0], $ticket_type);
            $ticket_type = str_replace("{TICKET_QUANTITY}", $array_tickets_data[1], $ticket_type);
            $ticket_type = str_replace("{TICKET_PRICE}", $array_tickets_data[2], $ticket_type);
            $ticket_type = str_replace("{TICKET_AREACATSEQUENCE}", $array_tickets_data[3], $ticket_type);
            if (isset($array_purchase[3])){
                $ticket_type = str_replace("{RECOGNITIONID}", "<ns1:ThirdPartyMemberScheme><ns1:MemberCard>".$array_purchase[3]."</ns1:MemberCard></ns1:ThirdPartyMemberScheme><ns1:LoyaltyRecognitionId>".$array_purchase[4]."</ns1:LoyaltyRecognitionId>", $ticket_type);
            }else{
                $ticket_type = str_replace("{RECOGNITIONID}", "", $ticket_type);
            }
            $ticket_type_list.= $ticket_type;
        }
        $xml_data = str_replace("{TICKET_TYPE_CODE}", $ticket_type_list, $xml_data);
        // *** FIN ARMAR ESTRUCTURA DE LOS TICKETS ***
        $xml_data = str_replace("{OPTIONAL_CLIENT_CLASS}", "WWW", $xml_data);
        $xml_data = str_replace("{OPTIONAL_CLIENT_ID}", "111.111.111.111", $xml_data);
        $xml_data = str_replace("{OPTIONAL_CLIENT_NAME}", "WEBSERVER", $xml_data);
        $xml_data = str_replace("{USER_SESSION}", @$array_purchase[2], $xml_data);
        $xml_data = str_replace("{CINEMA_ID}", @$array_purchase[0], $xml_data);
        $xml_data = str_replace("{SESSION_ID}", @$array_purchase[1], $xml_data);
        $xml_data = str_replace("{TICKET_STRUCTURE}", $ticket_type, $xml_data);
        
        $this->logPago("addTickets;XML GENERADO [".$xml_data."];NA;NA");
                       
        $ch = curl_init($wsdl);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml', 'soapaction: http://vista.co.nz/services/WSVistaWebClient.ServiceContracts/1/AddTickets'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
                        
        $outputxml = $this->xmlLib->xmlObjToArray( simplexml_load_string(utf8_encode($output), 'SimpleXMLElement') );
        $result = $outputxml["children"]["soap:body"][0]["children"]["addticketsresult"][0]["children"]["result"][0]["text"];
        if (isset($outputxml["children"]["soap:body"][0]["children"]["addticketsresult"][0]["children"]["seatlayoutdata"])){
            $json_mapa = json_encode($outputxml["children"]["soap:body"][0]["children"]["addticketsresult"][0]["children"]["seatlayoutdata"][0]["children"]["areas"][0]["children"]["area"][0]);            
        }
        
        if ($result == "OK"){
            $data = array("result"=>true, "response"=>"OK", "data"=>$json_mapa, "error"=>false);
        }else{
            $data = array("result"=>false, "response"=>"ERROR", "data"=>$output, "error"=>true, "response"=>"Error en la llamada addTicket al usuario [".@$array_purchase[2]."]");
        }
        return $http->responseAcceptHttpVista($data);        
    }        
    
    /**
     * @Rest\Get("/api/vistaticketingservice/addticket/datatickets/{datatickets}/params/{params}", name="vistaTicketingServiceAddTicket", defaults={"_format":"json"})
     *
     * @SWG\Parameter(
     *     name="datatickets",
     *     in="path",
     *     type="string",
     *     description="cadena separada con ';' que contiene la información de los tickets a agregar."
     * )
     * @SWG\Parameter(
     *     name="params",
     *     in="path",
     *     type="string",
     *     description="cadena separada con ';' que contiene la información para la transacción contra VISTA."
     * )
     
     * @SWG\Response(
     *     response=200,
     *     description="respuesta del servicio."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de AddTicket"
     * )
     *
     * @SWG\Tag(name="VistaTicketingAddTicket")
     */
    
    public function addTicketAction($datatickets, $params){
        $http = $this->get('httpcustom');
        $xml_ticket_structure = "";
        $xml_data = "";        
        $array_tickets = array();
        $ticketparam = array();
        $array_tickets = explode(";", $datatickets);
        $array_params = explode(";", $params);
        $xml_data = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://vista.co.nz/services/WSVistaWebClient.ServiceContracts/1" xmlns:ns1="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/"><soapenv:Header/><soapenv:Body><ns:AddTicketsRequest><ns1:OptionalClientClass>{OPTIONAL_CLIENT_CLASS}</ns1:OptionalClientClass><ns1:OptionalClientId>{OPTIONAL_CLIENT_ID}</ns1:OptionalClientId><ns1:OptionalClientName>{OPTIONAL_CLIENT_NAME}</ns1:OptionalClientName><ns:UserSessionId>{USER_SESSION}</ns:UserSessionId><ns:CinemaId>{CINEMA_ID}</ns:CinemaId><ns:SessionId>{SESSION_ID}</ns:SessionId><ns:TicketTypes>{TICKET_TYPE_CODE}</ns:TicketTypes><ns:ReturnOrder>true</ns:ReturnOrder><ns:ReturnSeatData>true</ns:ReturnSeatData><ns:ProcessOrderValue>true</ns:ProcessOrderValue><ns:UserSelectedSeatingSupported>true</ns:UserSelectedSeatingSupported><ns:SkipAutoAllocation>false</ns:SkipAutoAllocation><ns:IncludeAllSeatPriorities>true</ns:IncludeAllSeatPriorities><ns:IncludeSeatNumbers>1</ns:IncludeSeatNumbers><ns:ExcludeAreasWithoutTickets>true</ns:ExcludeAreasWithoutTickets><ns:ReturnDiscountInfo>false</ns:ReturnDiscountInfo><ns:BookingFeeOverride>1</ns:BookingFeeOverride><ns:BookingMode>1</ns:BookingMode><ns:ReorderSessionTickets>true</ns:ReorderSessionTickets><ns:ReturnSeatDataFormat>2</ns:ReturnSeatDataFormat></ns:AddTicketsRequest></soapenv:Body></soapenv:Envelope>';
        $xml_ticket_structure = '<ns:TicketType><ns1:TicketTypeCode>{TICKET_TYPE_CODE}</ns1:TicketTypeCode><ns1:Qty>{TICKET_QUANTITY}</ns1:Qty><ns1:PriceInCents>{TICKET_PRICE}</ns1:PriceInCents><ns1:OptionalAreaCatSequence>{TICKET_AREACATSEQUENCE}</ns1:OptionalAreaCatSequence><ns1:BookingFeeOverride>1</ns1:BookingFeeOverride><ns1:LoyaltyRecognitionSequence>0</ns1:LoyaltyRecognitionSequence><ns1:PromotionTicketTypeId>0</ns1:PromotionTicketTypeId><ns1:PromotionInstanceGroupNumber>0</ns1:PromotionInstanceGroupNumber><ns1:PriceCardPriceInCents>0</ns1:PriceCardPriceInCents></ns:TicketType>';
        // *** INICIO ARMAR ESTRUCTURA DE LOS TICKETS ***
        $x = 0;
        $counter = 0;
        $ticket_type = "";
        $ticket_type = $xml_ticket_structure;
        for ($x = 0; $x < sizeof($array_tickets); $x++){
            if ($counter == 0){
                $ticket_type = str_replace("{TICKET_TYPE_CODE}", $array_tickets[$x], $ticket_type);
            }
            if ($counter == 1){
                $ticket_type = str_replace("{TICKET_QUANTITY}", $array_tickets[$x], $ticket_type);
            }
            if ($counter == 2){
                $ticket_type = str_replace("{TICKET_PRICE}", $array_tickets[$x], $ticket_type);
            }
            if ($counter == 3){
                $ticket_type = str_replace("{TICKET_AREACATSEQUENCE}", $array_tickets[$x], $ticket_type);
                $counter = 0;
                if ($x+1 != sizeof($array_tickets)){
                    $ticket_type.= $xml_ticket_structure;
                }else{
                    break;
                }
            }else{
                $counter++;
            }
        }
        if(count($array_params)>=8){
            $xml_data = str_replace("{TICKET_TYPE_CODE}", $ticket_type, $xml_data);
            // *** FIN ARMAR ESTRUCTURA DE LOS TICKETS *** 
            $xml_data = str_replace("{OPTIONAL_CLIENT_CLASS}", @$array_params[0], $xml_data);
            $xml_data = str_replace("{OPTIONAL_CLIENT_ID}", @$array_params[1], $xml_data);
            $xml_data = str_replace("{OPTIONAL_CLIENT_NAME}", @$array_params[2], $xml_data);
            $xml_data = str_replace("{USER_SESSION}", @$array_params[3], $xml_data);
            $xml_data = str_replace("{CINEMA_ID}", @$array_params[4], $xml_data);
            $xml_data = str_replace("{SESSION_ID}", @$array_params[5], $xml_data);
            $xml_data = str_replace("{TICKET_STRUCTURE}", $ticket_type, $xml_data);
            $xml_data = str_replace("{TICKET_TYPE_CODE}", @$array_params[6], $xml_data);
            $xml_data = str_replace("{TICKET_QUANTITY}", @$array_params[7], $xml_data);
            $xml_data = str_replace("{TICKET_PRICE}", @$array_params[8], $xml_data);
            $xml_data = str_replace("{TICKET_AREACATSEQUENCE}", @$array_params[9], $xml_data);
            $ch = curl_init("http://172.20.5.242/WSVistaWebClient/TicketingService.asmx");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml', 'soapaction: http://vista.co.nz/services/WSVistaWebClient.ServiceContracts/1/AddTickets'));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $output = curl_exec($ch);
                                    
            if (strpos($output, "OK") !== false){
                $data = array("result"=>true, "response"=>"OK", "data"=>"Se han agregado exitosamente los tickets a la orden del usuario [".@$array_params[3]."]", "error"=>false);
            }else{
                $data = array("result"=>false, "response"=>"ERROR", "data"=>$output, "error"=>true, "response"=>"Error en la llamada addTicket al usuario [".@$array_params[3]."]");
            }
            curl_close($ch);
        }else{
            $data = array("result"=>false, "response"=>"ERROR", "data"=>array(), "error"=>true, "response"=>"Error en parametros");            
        }     
        return $http->responseAcceptHttpVista($data);
    }
    
    /**
     * @Rest\Get("/api/vistaticketingservice/gettickettypelist/cinemaid/{cinemaid}/sessionid/{sessionid}", name="vistaTicketingServiceGetTicketTypeList", defaults={"_format":"json"})
     *
     * @SWG\Parameter(
     *     name="cinemaid",
     *     in="path",
     *     type="string",
     *     description="cadena con el identificador del complejo."
     * )
     * @SWG\Parameter(
     *     name="sessionid",
     *     in="path",
     *     type="integer",
     *     description="identificador de una sesion de una pelicula en VISTA."
     * )
     
     * @SWG\Response(
     *     response=200,
     *     description="respuesta del servicio."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de GetTicketTypeList"
     * )
     *
     * @SWG\Tag(name="VistaTicketingGetTicketTypeList")
     */
        
    public function getTicketTypeListAction($cinemaid, $sessionid){
        $http = $this->get('httpcustom');
        $data= [];
        $data2= [];
        $response= [];
        $params = array(
            "CinemaId"=> isset($cinemaid) ? utf8_decode($cinemaid) : null,
            "SessionId"=> isset($sessionid) ? $sessionid : "0",
            "OptionalShowNonATMTickets"=>"1",
            "OptionalReturnAllRedemptionAndCompTickets"=>"1",
            "OptionalReturnAllLoyaltyTickets"=>"1",
            "OptionalClientClass"=>"WWW",
            "OptionalReturnLoyaltyRewardFlag"=>"1",
            "OptionalSeparatePaymentBasedTickets"=>"1",
            "OptionalShowLoyaltyTicketsToNonMembers"=>"1",
            "OptionalEnforceChildTicketLogic"=>"1",
        );        
        $response = $this->clientTicketing->call('GetTicketTypeList',array($params));
        $data = json_decode( json_encode( simplexml_load_string( trim( utf8_encode( $response["DatasetXML"] ) ) ) ) , true );
        if ($data == null){
            $data2 = array("result"=>true, "response"=>"ERROR", "data"=>"", "error"=>true);
        }else{
            $data2 = array("result"=>true, "response"=>"OK", "data"=>$data, "error"=>false);
        }        
        return $http->responseAcceptHttpVista($data2);
    }
    
    /**
     * @Rest\Get("/api/vistaticketingservice/yeipiiUser/cinemaid/{cinemaid}", name="yeipiiUser", defaults={"_format":"json"})
     *
     * @SWG\Parameter(
     *     name="cinemaid",
     *     in="path",
     *     type="string",
     *     description="identificador del cine."
     * )
     * @SWG\Response(
     *     response=200,
     *     description="resultado de la operacion"
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al obtener el usuario de yeipii."
     * )#
     *
     * @SWG\Tag(name="yeipiiUser")
     */
    public function yeipiiUser($cinemaid){        
        $http = $this->get('httpcustom');
        $user = "";
        $daoLib = null;
        $sql = "";
        $data2 = array();        
        $daoLib = new DaoLib($this->getDoctrine()->getManager());
        $sql = "SELECT usuario FROM usuarios_yeipii_complejos WHERE complejo = '".$cinemaid."'";
        $user = $daoLib->prepareStament($sql,"asociativo");
        if (isset($user[0]["usuario"]) && $user[0]["usuario"] != null){
            $data2 = array("result"=>true, "response"=>"OK", "data"=>$user[0]["usuario"], "error"=>false);
        }else{
            $data2 = array("result"=>true, "response"=>"ERROR", "data"=>"", "error"=>true);
        }
        return $http->responseAcceptHttpVista($data2);
    }
    
    
    /**
     * @Rest\Get("/api/vistaticketingservice/getticketlist/cinemaid/{cinemaid}/sessionid/{sessionid}", name="GetTicketList", defaults={"_format":"json"})
     *
     * @SWG\Parameter(
     *     name="cinemaid",
     *     in="path",
     *     type="string",
     *     required=false,     
     *     description="cadena con el identificador del complejo."
     * )
     * @SWG\Parameter(
     *     name="sessionid",
     *     in="path",
     *     type="integer",
     *     required=false,     
     *     description="identificador de una sesion de una pelicula en VISTA."
     * )
     * @SWG\Response(
     *     response=200,
     *     description="respuesta del servicio."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de GetTicketList"
     * )
     *
     * @SWG\Tag(name="GetTicketList")
     */
    
    public function getTicketListAction($cinemaid, $sessionid){
        $http = $this->get('httpcustom');
        $data= [];
        $data2= [];
        $response= [];
        $output = "";
        $nombreboleto = "";
        $ruta_produccion = 'http://172.28.85.132:8090';
        $x = 0;
        $Cinema_strID = "";
        $HOCode = "";
        $webway = false;
        if (strstr($cinemaid, "w")){
            $cinemaid = substr($cinemaid, 0, strlen($cinemaid)-1);
            $webway = true;
        }
        $this->logPago("GetTicketList;Recibiendo parametros ".$cinemaid.";".$sessionid.";NA;NA");
        $json_return = array();
        $params = array(
            "CinemaId"=> isset($cinemaid) ? utf8_decode($cinemaid) : null,
            "SessionId"=> isset($sessionid) ? $sessionid : "0",
            "OptionalShowNonATMTickets"=>"1",
            "OptionalReturnAllRedemptionAndCompTickets"=>"1",
            "OptionalReturnAllLoyaltyTickets"=>"1",
            "OptionalClientClass"=>"WWW",
            "OptionalReturnLoyaltyRewardFlag"=>"1",
            "OptionalSeparatePaymentBasedTickets"=>"1",
            "OptionalShowLoyaltyTicketsToNonMembers"=>"1",
            "OptionalEnforceChildTicketLogic"=>"1",
        );   
        // buscar palabras claves 
        $daoLib = new DaoLib($this->getDoctrine()->getManager());
        $sql= "SELECT keyword FROM ticketkeywords WHERE activo = 'S'";
        $reservedw = $daoLib->prepareStament($sql,"asociativo");
        $z = 0;
        for ($x = 0; $x < sizeof($reservedw); $x++){
            $reservedwords[$z] = $reservedw[$x]["keyword"];
            $z++;  
        }
        if ($webway){
            $reservedwords[] = "movistar";
        }
        $this->logPago("GetTicketList;Obteniendo palabras claves para filtro del listado de tickets.;NA;NA");
        $response = $this->clientTicketing->call('GetTicketTypeList',array($params));
        $output = $this->xmlLib->xmlObjToArray( simplexml_load_string(utf8_encode($response["DatasetXML"]), 'SimpleXMLElement') );
        for ($x = 0; $x < sizeof($output["children"]["table"]); $x++){
            $nombreboleto = trim($output["children"]["table"][$x]["children"]["price_strticket_type_description"][0]["text"]);
            if (strstr($output["children"]["table"][$x]["children"]["ttype_strsaleschannels"][0]["text"], "WWW") && strstr($output["children"]["table"][$x]["children"]["ticketcategory"][0]["text"], "STANDARD")){
                for ($y = 0; $y < sizeof($reservedwords); $y++){
                    if (strstr(strtolower(trim($nombreboleto)), "yeipii") !== false) continue;
                    if (strstr(strtolower(trim($nombreboleto)), $reservedwords[$y]) !== false){
                        $Cinema_strID = trim($output["children"]["table"][$x]["children"]["cinema_strid"][0]["text"]);
                        $HOCode = trim($output["children"]["table"][$x]["children"]["ttype_strhocode"][0]["text"]);
                        $curl = curl_init();
                        curl_setopt_array($curl, array(
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_URL => $ruta_produccion."/api/cinexdataservice/getbookingfeevalue?complejoid=".$Cinema_strID."&codigoticketho=".$HOCode,
                            CURLOPT_USERAGENT => 'CINEX'
                        ));
                        $resp = curl_exec($curl);
                        curl_close($curl);
                        $json_bookingfee = json_decode($resp);
                        if (isset($json_bookingfee) && isset($json_bookingfee->response[0])){
                            $comision = $json_bookingfee->response[0];
                            if ($comision > 0){
                                $comision = number_format($comision, 2, '.', '');
                                $comision = str_replace('.', '', $comision);
                            }                            
                        }else{
                            $comision = 0;
                        }  
                        $this->logPago("GetTicketList;Obteniendo costo de booking fee [".$comision."];NA;NA");
                        $nombreboleto = $output["children"]["table"][$x]["children"]["price_strticket_type_description"][0]["text"];
                        if (substr($nombreboleto, 0, 1) == "E" || substr($nombreboleto, 0, 1) == "T"){
                            $nombreboleto = substr($output["children"]["table"][$x]["children"]["price_strticket_type_description"][0]["text"], 1, 255);
                        }
                        $json_return[] = array("cinema_strid"=>$output["children"]["table"][$x]["children"]["cinema_strid"][0]["text"],"session_strid"=>$output["children"]["table"][$x]["children"]["session_strid"][0]["text"], "areacat_strcode"=>$output["children"]["table"][$x]["children"]["areacat_strcode"][0]["text"], "areacat_strseatallocationon"=>$output["children"]["table"][$x]["children"]["areacat_strseatallocationon"][0]["text"], "cinema_strid1"=>$output["children"]["table"][$x]["children"]["cinema_strid1"][0]["text"], "price_strticket_type_code"=>$output["children"]["table"][$x]["children"]["price_strticket_type_code"][0]["text"], "price_strticket_type_description"=>$nombreboleto, "price_strgroup_code"=>$output["children"]["table"][$x]["children"]["price_strgroup_code"][0]["text"], "price_intticket_price"=>$output["children"]["table"][$x]["children"]["price_intticket_price"][0]["text"], "price_strchild_ticket"=>$output["children"]["table"][$x]["children"]["price_strchild_ticket"][0]["text"], "areacat_strcode1"=>$output["children"]["table"][$x]["children"]["areacat_strcode1"][0]["text"], "areacat_intseq"=>$output["children"]["table"][$x]["children"]["areacat_intseq"][0]["text"], "price_strpackage"=>$output["children"]["table"][$x]["children"]["price_strpackage"][0]["text"], "ttype_stravailloyaltyonly"=>$output["children"]["table"][$x]["children"]["ttype_stravailloyaltyonly"][0]["text"], "ttype_strhocode"=>$output["children"]["table"][$x]["children"]["ttype_strhocode"][0]["text"], "price_strredemption"=>$output["children"]["table"][$x]["children"]["price_strredemption"][0]["text"], "price_strcomp"=>$output["children"]["table"][$x]["children"]["price_strcomp"][0]["text"], "ttype_strsaleschannels"=>$output["children"]["table"][$x]["children"]["ttype_strsaleschannels"][0]["text"], "hopk"=>$output["children"]["table"][$x]["children"]["hopk"][0]["text"], "ticketcategory"=>$output["children"]["table"][$x]["children"]["ticketcategory"][0]["text"], "comision"=>$comision);
                        
                    }
                }
            }
        }       
        $data = $json_return;
        if ($data == null){
            $this->logPago("GetTicketList;Error obteniendo listado de boletos [".$cinemaid.",".$sessionid."];NA;NA");            
            $data2 = array("result"=>true, "response"=>"ERROR", "data"=>"", "error"=>true);
        }else{
            $this->logPago("GetTicketList;Listado de boletos obtenido exitosamente [".$cinemaid.",".$sessionid."];NA;NA");            
            $data2 = array("result"=>true, "response"=>"OK", "data"=>$data, "error"=>false);
        }
        return $http->responseAcceptHttpVista($data2);
    }
    
    /**
     * @Rest\Get("/api/vistaticketingservice/getticketlistbychannel/cinemaid/{cinemaid}/sessionid/{sessionid}/channel/{channel}", name="GetTicketListByChannel", defaults={"_format":"json"})
     *
     * @SWG\Parameter(
     *     name="cinemaid",
     *     in="path",
     *     type="string",
     *     required=false,
     *     description="cadena con el identificador del complejo."
     * )
     * @SWG\Parameter(
     *     name="sessionid",
     *     in="path",
     *     type="integer",
     *     required=false,
     *     description="identificador de una sesion de una pelicula en VISTA."
     * )
     * @SWG\Parameter(
     *     name="channel",
     *     in="path",
     *     type="string",
     *     required=false,
     *     description="Canal donde se efectua la peticion de la lista de boletos."
     * )
     * @SWG\Response(
     *     response=200,
     *     description="respuesta del servicio."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de GetTicketListByChannel"
     * )
     *
     * @SWG\Tag(name="GetTicketTypeListByChannel")
     */
    
    public function getTicketListByChannelAction($cinemaid, $sessionid, $channel){
        $http = $this->get('httpcustom');
        $data= [];
        $data2= [];
        $response= [];
        $output = "";
        $nombreboleto = "";
        $ruta_produccion = 'http://172.28.85.132:8090';
        $x = 0;
        $Cinema_strID = "";
        $HOCode = "";
        $webway = false;
        $this->logPago("GetTicketTypeListByChannel;Recibiendo parametros ".$cinemaid.";".$sessionid.";NA;NA");
        $json_return = array();
        $params = array(
            "CinemaId"=> isset($cinemaid) ? utf8_decode($cinemaid) : null,
            "SessionId"=> isset($sessionid) ? $sessionid : "0",
            "OptionalShowNonATMTickets"=>"1",
            "OptionalReturnAllRedemptionAndCompTickets"=>"1",
            "OptionalReturnAllLoyaltyTickets"=>"1",
            "OptionalClientClass"=>"WWW",
            "OptionalReturnLoyaltyRewardFlag"=>"1",
            "OptionalSeparatePaymentBasedTickets"=>"1",
            "OptionalShowLoyaltyTicketsToNonMembers"=>"1",
            "OptionalEnforceChildTicketLogic"=>"1",
        );
        // buscar palabras claves
        $daoLib = new DaoLib($this->getDoctrine()->getManager());
        $sql= "SELECT keyword FROM ticketkeywords WHERE activo = 'S'";
        $reservedw = $daoLib->prepareStament($sql,"asociativo");
        $z = 0;
        for ($x = 0; $x < sizeof($reservedw); $x++){
            $reservedwords[$z] = $reservedw[$x]["keyword"];
            $z++;
        }
        if ($channel == "WEB"){
            $reservedwords[] = "movistar";
        }
        $this->logPago("GetTicketList;Obteniendo palabras claves para filtro del listado de tickets.;NA;NA");
        $response = $this->clientTicketing->call('GetTicketTypeList',array($params));
        $output = $this->xmlLib->xmlObjToArray( simplexml_load_string(utf8_encode($response["DatasetXML"]), 'SimpleXMLElement') );
        for ($x = 0; $x < sizeof($output["children"]["table"]); $x++){
            if (strstr($output["children"]["table"][$x]["children"]["ttype_strsaleschannels"][0]["text"], "WWW") && (strstr($output["children"]["table"][$x]["children"]["ticketcategory"][0]["text"], "STANDARD") || strstr($output["children"]["table"][$x]["children"]["ticketcategory"][0]["text"], "VOUCHER"))){
                $nombreboleto = trim($output["children"]["table"][$x]["children"]["price_strticket_type_description"][0]["text"]);                
                for ($y = 0; $y < sizeof($reservedwords); $y++){
                    if (strstr(strtolower(trim($nombreboleto)), $reservedwords[$y]) && !strstr(strtolower(trim($nombreboleto)), "mpfull")){
                        if (strstr(strtolower(trim($nombreboleto)), "yeipii") !== false) continue;
                        if (strstr(strtolower(trim($nombreboleto)), "cupon") !== false) continue;
                        $Cinema_strID = trim($output["children"]["table"][$x]["children"]["cinema_strid"][0]["text"]);
                        $HOCode = trim($output["children"]["table"][$x]["children"]["ttype_strhocode"][0]["text"]);
                        $curl = curl_init();
                        curl_setopt_array($curl, array(
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_URL => $ruta_produccion."/api/cinexdataservice/getbookingfeevalue?complejoid=".$Cinema_strID."&codigoticketho=".$HOCode,
                            CURLOPT_USERAGENT => 'CINEX'
                        ));
                        $resp = curl_exec($curl);
                        curl_close($curl);
                        $json_bookingfee = json_decode($resp);
                        if (isset($json_bookingfee) && isset($json_bookingfee->response[0])){
                            $comision = $json_bookingfee->response[0];
                            if ($comision > 0){
                                $comision = number_format($comision, 2, '.', '');
                                $comision = str_replace('.', '', $comision);
                            }
                        }else{
                            $comision = 0;
                        }
                        $this->logPago("GetTicketList;Obteniendo costo de booking fee [".$comision."];NA;NA");
                        $nombreboleto = $output["children"]["table"][$x]["children"]["price_strticket_type_description"][0]["text"];
                        if (substr($nombreboleto, 0, 1) == "E" || substr($nombreboleto, 0, 1) == "T"){
                            $nombreboleto = substr($output["children"]["table"][$x]["children"]["price_strticket_type_description"][0]["text"], 1, 255);
                        }
                        $json_return[] = array("cinema_strid"=>$output["children"]["table"][$x]["children"]["cinema_strid"][0]["text"],"session_strid"=>$output["children"]["table"][$x]["children"]["session_strid"][0]["text"], "areacat_strcode"=>$output["children"]["table"][$x]["children"]["areacat_strcode"][0]["text"], "areacat_strseatallocationon"=>$output["children"]["table"][$x]["children"]["areacat_strseatallocationon"][0]["text"], "cinema_strid1"=>$output["children"]["table"][$x]["children"]["cinema_strid1"][0]["text"], "price_strticket_type_code"=>$output["children"]["table"][$x]["children"]["price_strticket_type_code"][0]["text"], "price_strticket_type_description"=>$nombreboleto, "price_strgroup_code"=>$output["children"]["table"][$x]["children"]["price_strgroup_code"][0]["text"], "price_intticket_price"=>$output["children"]["table"][$x]["children"]["price_intticket_price"][0]["text"], "price_strchild_ticket"=>$output["children"]["table"][$x]["children"]["price_strchild_ticket"][0]["text"], "areacat_strcode1"=>$output["children"]["table"][$x]["children"]["areacat_strcode1"][0]["text"], "areacat_intseq"=>$output["children"]["table"][$x]["children"]["areacat_intseq"][0]["text"], "price_strpackage"=>$output["children"]["table"][$x]["children"]["price_strpackage"][0]["text"], "ttype_stravailloyaltyonly"=>$output["children"]["table"][$x]["children"]["ttype_stravailloyaltyonly"][0]["text"], "ttype_strhocode"=>$output["children"]["table"][$x]["children"]["ttype_strhocode"][0]["text"], "price_strredemption"=>$output["children"]["table"][$x]["children"]["price_strredemption"][0]["text"], "price_strcomp"=>$output["children"]["table"][$x]["children"]["price_strcomp"][0]["text"], "ttype_strsaleschannels"=>$output["children"]["table"][$x]["children"]["ttype_strsaleschannels"][0]["text"], "hopk"=>$output["children"]["table"][$x]["children"]["hopk"][0]["text"], "ticketcategory"=>$output["children"]["table"][$x]["children"]["ticketcategory"][0]["text"], "comision"=>$comision);                        
                    }
                }
            }
        }
        $data = $json_return;
        if ($data == null){
            $this->logPago("GetTicketTypeListByChannel;Listado de boletos obtenido exitosamente [".$cinemaid.",".$sessionid."];NA;NA");
            $data2 = array("result"=>true, "response"=>"ERROR", "data"=>"", "error"=>true);
        }else{
            $this->logPago("GetTicketTypeListByChannel;Error obteniendo listado de boletos [".$cinemaid.",".$sessionid."];NA;NA");
            $data2 = array("result"=>true, "response"=>"OK", "data"=>$data, "error"=>false);
        }
        return $http->responseAcceptHttpVista($data2);
    }
    
    /**
     * @Rest\Get("/api/vistaticketingservice/getmovistartickettypecode/cinemaid/{cinemaid}/sessionid/{sessionid}/channel/{channel}", name="GetMovistarTicketTypeCode", defaults={"_format":"json"})
     *
     * @SWG\Parameter(
     *     name="cinemaid",
     *     in="path",
     *     type="string",
     *     required=false,
     *     description="cadena con el identificador del complejo."
     * )
     * @SWG\Parameter(
     *     name="sessionid",
     *     in="path",
     *     type="integer",
     *     required=false,
     *     description="identificador de una sesion de una pelicula en VISTA."
     * )
     * @SWG\Parameter(
     *     name="channel",
     *     in="path",
     *     type="string",
     *     required=false,
     *     description="Canal donde se efectua la peticion de la lista de boletos."
     * )
     * @SWG\Response(
     *     response=200,
     *     description="respuesta del servicio."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de GetMovistarTicketTypeCode"
     * )
     *
     * @SWG\Tag(name="GetMovistarTicketTypeCode")
     */
    
    public function GetMovistarTicketTypeCodeAction($cinemaid, $sessionid, $channel){
        $http = $this->get('httpcustom');
        $data= [];
        $data2= [];
        $response= [];
        $output = "";
        $nombreboleto = "";
        $ruta_produccion = 'http://172.28.85.132:8090';
        $x = 0;
        $Cinema_strID = "";
        $HOCode = "";
        $webway = false;
        $this->logPago("GetMovistarTicketTypeCode;Recibiendo parametros ".$cinemaid.";".$sessionid.";NA;NA");
        $json_return = array();
        $params = array(
            "CinemaId"=> isset($cinemaid) ? utf8_decode($cinemaid) : null,
            "SessionId"=> isset($sessionid) ? $sessionid : "0",
            "OptionalShowNonATMTickets"=>"1",
            "OptionalReturnAllRedemptionAndCompTickets"=>"1",
            "OptionalReturnAllLoyaltyTickets"=>"1",
            "OptionalClientClass"=>"WWW",
            "OptionalReturnLoyaltyRewardFlag"=>"1",
            "OptionalSeparatePaymentBasedTickets"=>"1",
            "OptionalShowLoyaltyTicketsToNonMembers"=>"1",
            "OptionalEnforceChildTicketLogic"=>"1",
        );
        if ($channel == "WEB"){
            $reservedwords[] = "movist";
        }
        $this->logPago("GetMovistarTicketTypeCode;Obteniendo palabras claves para filtro del listado de tickets.;NA;NA");
        $response = $this->clientTicketing->call('GetTicketTypeList',array($params));
        $output = $this->xmlLib->xmlObjToArray( simplexml_load_string(utf8_encode($response["DatasetXML"]), 'SimpleXMLElement') );
        for ($x = 0; $x < sizeof($output["children"]["table"]); $x++){
            if (strstr($output["children"]["table"][$x]["children"]["ttype_strsaleschannels"][0]["text"], "WWW") && (strstr($output["children"]["table"][$x]["children"]["ticketcategory"][0]["text"], "STANDARD") || strstr($output["children"]["table"][$x]["children"]["ticketcategory"][0]["text"], "VOUCHER"))){
                $nombreboleto = trim($output["children"]["table"][$x]["children"]["price_strticket_type_description"][0]["text"]);
                for ($y = 0; $y < sizeof($reservedwords); $y++){
                    if (strstr(strtolower(trim($nombreboleto)), $reservedwords[$y]) && !strstr(strtolower(trim($nombreboleto)), "mpfull")){
                        if (strstr(strtolower(trim($nombreboleto)), "yeipii") !== false) continue;
                        $Cinema_strID = trim($output["children"]["table"][$x]["children"]["cinema_strid"][0]["text"]);
                        $HOCode = trim($output["children"]["table"][$x]["children"]["ttype_strhocode"][0]["text"]);
                        $curl = curl_init();
                        curl_setopt_array($curl, array(
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_URL => $ruta_produccion."/api/cinexdataservice/getbookingfeevalue?complejoid=".$Cinema_strID."&codigoticketho=".$HOCode,
                            CURLOPT_USERAGENT => 'CINEX'
                        ));
                        $resp = curl_exec($curl);
                        curl_close($curl);
                        $json_bookingfee = json_decode($resp);
                        if (isset($json_bookingfee) && isset($json_bookingfee->response[0])){
                            $comision = $json_bookingfee->response[0];
                            if ($comision > 0){
                                $comision = number_format($comision, 2, '.', '');
                                $comision = str_replace('.', '', $comision);
                            }
                        }else{
                            $comision = 0;
                        }
                        $this->logPago("GetMovistarTicketTypeCode;Obteniendo costo de booking fee [".$comision."];NA;NA");
                        $nombreboleto = $output["children"]["table"][$x]["children"]["price_strticket_type_description"][0]["text"];
                        if (substr($nombreboleto, 0, 1) == "E" || substr($nombreboleto, 0, 1) == "T"){
                            $nombreboleto = substr($output["children"]["table"][$x]["children"]["price_strticket_type_description"][0]["text"], 1, 255);
                        }
                        $json_return[] = array("cinema_strid"=>$output["children"]["table"][$x]["children"]["cinema_strid"][0]["text"],"session_strid"=>$output["children"]["table"][$x]["children"]["session_strid"][0]["text"], "areacat_strcode"=>$output["children"]["table"][$x]["children"]["areacat_strcode"][0]["text"], "areacat_strseatallocationon"=>$output["children"]["table"][$x]["children"]["areacat_strseatallocationon"][0]["text"], "cinema_strid1"=>$output["children"]["table"][$x]["children"]["cinema_strid1"][0]["text"], "price_strticket_type_code"=>$output["children"]["table"][$x]["children"]["price_strticket_type_code"][0]["text"], "price_strticket_type_description"=>$nombreboleto, "price_strgroup_code"=>$output["children"]["table"][$x]["children"]["price_strgroup_code"][0]["text"], "price_intticket_price"=>$output["children"]["table"][$x]["children"]["price_intticket_price"][0]["text"], "price_strchild_ticket"=>$output["children"]["table"][$x]["children"]["price_strchild_ticket"][0]["text"], "areacat_strcode1"=>$output["children"]["table"][$x]["children"]["areacat_strcode1"][0]["text"], "areacat_intseq"=>$output["children"]["table"][$x]["children"]["areacat_intseq"][0]["text"], "price_strpackage"=>$output["children"]["table"][$x]["children"]["price_strpackage"][0]["text"], "ttype_stravailloyaltyonly"=>$output["children"]["table"][$x]["children"]["ttype_stravailloyaltyonly"][0]["text"], "ttype_strhocode"=>$output["children"]["table"][$x]["children"]["ttype_strhocode"][0]["text"], "price_strredemption"=>$output["children"]["table"][$x]["children"]["price_strredemption"][0]["text"], "price_strcomp"=>$output["children"]["table"][$x]["children"]["price_strcomp"][0]["text"], "ttype_strsaleschannels"=>$output["children"]["table"][$x]["children"]["ttype_strsaleschannels"][0]["text"], "hopk"=>$output["children"]["table"][$x]["children"]["hopk"][0]["text"], "ticketcategory"=>$output["children"]["table"][$x]["children"]["ticketcategory"][0]["text"], "comision"=>$comision);
                    }
                }
            }
        }
        $data = $json_return;
        if ($data == null){
            $this->logPago("GetMovistarTicketTypeCode;Listado de boletos obtenido exitosamente [".$cinemaid.",".$sessionid."];NA;NA");
            $data2 = array("result"=>true, "response"=>"ERROR", "data"=>"", "error"=>true);
        }else{
            $this->logPago("GetMovistarTicketTypeCode;Error obteniendo listado de boletos [".$cinemaid.",".$sessionid."];NA;NA");
            $data2 = array("result"=>true, "response"=>"OK", "data"=>$data, "error"=>false);
        }
        return $http->responseAcceptHttpVista($data2);
    }
    
    
    /**
     * @Rest\Get("/api/vistaticketingservice/getcinexpasslist/cinemaid/{cinemaid}/sessionid/{sessionid}", name="GetCinexpassList", defaults={"_format":"json"})
     *
     * @SWG\Parameter(
     *     name="cinemaid",
     *     in="path",
     *     type="string",
     *     required=false,
     *     description="cadena con el identificador del complejo."
     * )
     * @SWG\Parameter(
     *     name="sessionid",
     *     in="path",
     *     type="integer",
     *     required=false,
     *     description="identificador de una sesion de una pelicula en VISTA."
     * )
     * @SWG\Response(
     *     response=200,
     *     description="respuesta del servicio."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de GetCinexpassList"
     * )
     *
     * @SWG\Tag(name="GetCinexpassList")
     */
    
    public function getCinexpassListAction($cinemaid, $sessionid){
        $http = $this->get('httpcustom');
        $data= [];
        $data2= [];
        $response= [];
        $output = "";
        $nombreboleto = "";
        $ruta_produccion = 'http://172.28.85.132:8090';
        $x = 0;
        $Cinema_strID = "";
        $HOCode = "";
        $webway = false;
        $comision = 0;
        $this->logPago("GetCinexpassList;Recibiendo parametros ".$cinemaid.";".$sessionid.";NA;NA");
        $json_return = array();
        $params = array(
            "CinemaId"=> isset($cinemaid) ? utf8_decode($cinemaid) : null,
            "SessionId"=> isset($sessionid) ? $sessionid : "0",
            "OptionalShowNonATMTickets"=>"1",
            "OptionalReturnAllRedemptionAndCompTickets"=>"1",
            "OptionalReturnAllLoyaltyTickets"=>"1",
            "OptionalClientClass"=>"WWW",
            "OptionalReturnLoyaltyRewardFlag"=>"1",
            "OptionalSeparatePaymentBasedTickets"=>"1",
            "OptionalShowLoyaltyTicketsToNonMembers"=>"1",
            "OptionalEnforceChildTicketLogic"=>"1",
        );
        // buscar palabras claves
        $daoLib = new DaoLib($this->getDoctrine()->getManager());
        $sql= "SELECT keyword FROM cinexpasskeywords WHERE activo = 'S'";
        $reservedw = $daoLib->prepareStament($sql,"asociativo");
        $z = 0;
        for ($x = 0; $x < sizeof($reservedw); $x++){
            $reservedwords[$z] = $reservedw[$x]["keyword"];
            $z++;
        }        
        $this->logPago("GetCinexpassList;Obteniendo palabras claves para filtro del listado de tickets.;NA;NA");
        $response = $this->clientTicketing->call('GetTicketTypeList',array($params));
        $output = $this->xmlLib->xmlObjToArray( simplexml_load_string(utf8_encode($response["DatasetXML"]), 'SimpleXMLElement') );
        for ($x = 0; $x < sizeof($output["children"]["table"]); $x++){
            if ($output["children"]["table"][$x]["children"]["ticketcategory"][0]["text"] == "VOUCHER"){
                $nombreboleto = trim($output["children"]["table"][$x]["children"]["price_strticket_type_description"][0]["text"]);
                for ($y = 0; $y < sizeof($reservedwords); $y++){
                    if (strstr(strtolower(trim($nombreboleto)), strtolower($reservedwords[$y]))){
                        $Cinema_strID = trim($output["children"]["table"][$x]["children"]["cinema_strid"][0]["text"]);
                        $HOCode = trim($output["children"]["table"][$x]["children"]["ttype_strhocode"][0]["text"]);
                        $nombreboleto = $output["children"]["table"][$x]["children"]["price_strticket_type_description"][0]["text"];
                        if (substr($nombreboleto, 0, 1) == "E" || substr($nombreboleto, 0, 1) == "T"){
                            $nombreboleto = substr($output["children"]["table"][$x]["children"]["price_strticket_type_description"][0]["text"], 1, 255);
                        }
                        $json_return[] = array("cinema_strid"=>$output["children"]["table"][$x]["children"]["cinema_strid"][0]["text"],"session_strid"=>$output["children"]["table"][$x]["children"]["session_strid"][0]["text"], "areacat_strcode"=>$output["children"]["table"][$x]["children"]["areacat_strcode"][0]["text"], "areacat_strseatallocationon"=>$output["children"]["table"][$x]["children"]["areacat_strseatallocationon"][0]["text"], "cinema_strid1"=>$output["children"]["table"][$x]["children"]["cinema_strid1"][0]["text"], "price_strticket_type_code"=>$output["children"]["table"][$x]["children"]["price_strticket_type_code"][0]["text"], "price_strticket_type_description"=>$nombreboleto, "price_strgroup_code"=>$output["children"]["table"][$x]["children"]["price_strgroup_code"][0]["text"], "price_intticket_price"=>$output["children"]["table"][$x]["children"]["price_intticket_price"][0]["text"], "price_strchild_ticket"=>$output["children"]["table"][$x]["children"]["price_strchild_ticket"][0]["text"], "areacat_strcode1"=>$output["children"]["table"][$x]["children"]["areacat_strcode1"][0]["text"], "areacat_intseq"=>$output["children"]["table"][$x]["children"]["areacat_intseq"][0]["text"], "price_strpackage"=>$output["children"]["table"][$x]["children"]["price_strpackage"][0]["text"], "ttype_stravailloyaltyonly"=>$output["children"]["table"][$x]["children"]["ttype_stravailloyaltyonly"][0]["text"], "ttype_strhocode"=>$output["children"]["table"][$x]["children"]["ttype_strhocode"][0]["text"], "price_strredemption"=>$output["children"]["table"][$x]["children"]["price_strredemption"][0]["text"], "price_strcomp"=>$output["children"]["table"][$x]["children"]["price_strcomp"][0]["text"], "ttype_strsaleschannels"=>$output["children"]["table"][$x]["children"]["ttype_strsaleschannels"][0]["text"], "hopk"=>$output["children"]["table"][$x]["children"]["hopk"][0]["text"], "ticketcategory"=>$output["children"]["table"][$x]["children"]["ticketcategory"][0]["text"], "comision"=>$comision);
                    }
                }
            }
        }
        $data = $json_return;
        if ($data == null){
            $this->logPago("GetCinexpassList;Listado de boletos obtenido exitosamente [".$cinemaid.",".$sessionid."];NA;NA");
            $data2 = array("result"=>true, "response"=>"ERROR", "data"=>"", "error"=>true);
        }else{
            $this->logPago("GetCinexpassList;Error obteniendo listado de boletos [".$cinemaid.",".$sessionid."];NA;NA");
            $data2 = array("result"=>true, "response"=>"OK", "data"=>$data, "error"=>false);
        }
        return $http->responseAcceptHttpVista($data2);
    }
    
    
    /**
     * @Rest\Get("/api/vistaticketingservice/getbookingmap/ticketdata/{ticketdata}/purchasedata/{purchasedata}/reintentos/{reintentos}", name="GetBookingMap", defaults={"_format":"json"})
     *
     * @SWG\Parameter(
     *     name="ticketdata",
     *     in="path",
     *     type="string",
     *     description="cadena separada con ';' que contiene la información de los tickets a agregar. Ejemplo de estructura => {TICKETTYPECODE};{QUANTITY};{PRICE};{AREACATEGORYSECUENCE}, para agregar varios tickets repetir la estructura separada por dos puntos ejemplo {TICKETTYPECODE};{QUANTITY};{PRICE};{AREACATEGORYSECUENCE}:{TICKETTYPECODE};{QUANTITY};{PRICE};{AREACATEGORYSECUENCE}"
     * )
     * @SWG\Parameter(
     *     name="purchasedata",
     *     in="path",
     *     type="string",
     *     description="cadena separada con ';' que contiene la información de la transaccion. Ejemplo de estructura => {CINEMAID};{SESSIONID};{USERSESSIONID};{LOYALTYCARDNUMBER};{RECOGNITIONID} en donde LOYALTYCARDNUMBER y RECOGNITIONID son valores para transacciones de cinexclusivo (loyalty) y son valores opcionales."
     * )
     * @SWG\Parameter(
     *     name="reintentos",
     *     in="path",
     *     type="integer",
     *     description="entero que identifica la cantidad de reintentos a realizar en la solicitud del mapa."
     * )     
     * @SWG\Response(
     *     response=200,
     *     description="respuesta del servicio."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de getBookingMap"
     * )
     *
     * @SWG\Tag(name="GetBookingMap")
     */
    
    public function getBookingMapAction($ticketdata, $purchasedata, $reintentos){
        $http = $this->get('httpcustom');
        $data_map = "";
        // cancelar la orden.
        $this->cancelOrderAction($purchasedata[2]);
        $this->logPago("GetBookingMap;Recibiendo parametros ticketdata[".$ticketdata."]purchasedata[".$purchasedata."]reintentos[".$reintentos."];NA;NA");
        // agregar los tickets a la orden.
        for ($x = 0; $x < $reintentos; $x++){
            $json_response = $this->addTicketsAction($ticketdata, $purchasedata);
            $json_addtickets = json_decode($json_response->getContent());
            if ($json_addtickets->result == "OK"){
                $data_map = json_decode($json_addtickets->response, true);
                break;
            }
        }
        if ($data_map != ""){
            $this->logPago("GetBookingMap;Mapa obtenido exitosamente;NA;NA");
            $response = array("result"=>true, "response"=>"Mapa obtenido exitosamente", "data"=>$data_map, "error"=>false);
        }else{
            $response = array("codRespuesta"=>"-2","desRespuesta"=>"Se ha presentado un error al cargar el mapa de esta función.", "data"=>"", "error"=>true);
        }
        return $http->responseAcceptHttpVista($response); 
    }    
        
    /**
     * @Rest\Get("/api/vistaticketingservice/getmap/ticketdata/{ticketdata}/purchasedata/{purchasedata}/reintentos/{reintentos}", name="GetMap", defaults={"_format":"json"})
     *
     * @SWG\Parameter(
     *     name="ticketdata",
     *     in="path",
     *     type="string",
     *     description="cadena separada con ';' que contiene la información de los tickets a agregar. Ejemplo de estructura => {TICKETTYPECODE};{QUANTITY};{PRICE};{AREACATEGORYSECUENCE}, para agregar varios tickets repetir la estructura separada por dos puntos ejemplo {TICKETTYPECODE};{QUANTITY};{PRICE};{AREACATEGORYSECUENCE}:{TICKETTYPECODE};{QUANTITY};{PRICE};{AREACATEGORYSECUENCE}"
     * )
     * @SWG\Parameter(
     *     name="purchasedata",
     *     in="path",
     *     type="string",
     *     description="cadena separada con ';' que contiene la información de la transaccion. Ejemplo de estructura => {CINEMAID};{SESSIONID};{USERSESSIONID};{LOYALTYCARDNUMBER};{RECOGNITIONID} en donde LOYALTYCARDNUMBER y RECOGNITIONID son valores para transacciones de cinexclusivo (loyalty) y son valores opcionales."
     * )
     * @SWG\Parameter(
     *     name="reintentos",
     *     in="path",
     *     type="integer",
     *     description="entero que identifica la cantidad de reintentos a realizar en la solicitud del mapa."
     * )     
     * @SWG\Parameter(
     *     name="reintentos",
     *     in="path",
     *     type="integer",
     *     description="entero que identifica la cantidad de reintentos a realizar en la solicitud del mapa."
     * )
     
     * @SWG\Response(
     *     response=200,
     *     description="respuesta del servicio."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de getMap"
     * )
     *
     * @SWG\Tag(name="GetMap")
     */
    public function getMapAction($ticketdata, $purchasedata, $reintentos){
        $http = $this->get('httpcustom');        
        $json_response = array();
        $json_addtickets = array();
        $data_map = array();
        $responseadd = "";
        $rowvalues = array();
        $rowdata = array();
        $letrasmapa = array();
        $letrasordenadas = array();
        $dataval = array();
        $numfilas = 0;
        $numcolumnas = 0;
        $numberofseats = 0;
        $errorservicio = false;
        $try = 0;
        $areacategorycode = "";
        $this->logPago("getMap;Recibiendo parametros ticketdata[".$ticketdata."]purchasedata[".$purchasedata."]reintentos[".$reintentos."];NA;NA");
        while ($try < $reintentos){
            $json_response = $this->addTicketsAction($ticketdata, $purchasedata);
            $json_addtickets = json_decode($json_response->getContent());
            if ($json_addtickets->result == "OK"){
                $data_map = json_decode($json_addtickets->response, true);                
                if ($data_map != ""){
                    $rowvalues = array();
                    $rowvalues = $data_map['children']['rows'][0]['children']['row'];
                    $rowdata = $data_map['children']['rows'][0]['children']['row'];
                    $numfilas = $data_map['children']['rowcount'][0]['text'];
                    $numcolumnas = $data_map['children']['columncount'][0]['text'];
                    $numberofseats = $data_map['children']['numberofseats'][0]['text'];
                    $areacategorycode = $data_map['children']['areacategorycode'][0]['text'];
                    $errorservicio = false;
                }else{
                    $errorservicio = true;
                }                
                break;
            }
            $try++;
        }
        // array de letras
        $counter = 0;
        for ($x = 0; $x < sizeof($rowvalues); $x++){
            if (isset($rowvalues[$x]["children"]["physicalname"]) && $rowvalues[$x]["children"]["physicalname"] != null){
                $letrasmapa[$x] = $rowvalues[$x]["children"]["physicalname"][0]["text"];
            }            
        }         
        $letrasordenadas = $this->getOrdenarLetras($numcolumnas, $numfilas, $rowdata, $letrasmapa, $areacategorycode);            
        $dataval = array();
        if ($errorservicio){
            $mapdata = array("result"=>false,"response"=>"ERROR", "error"=>"Error obteniendo el mapa de la sesion [".$sessionid."] complejo [".$cinemaid."]", "data"=>$dataval);
        }else{
            $dataval = array("rows"=>$numfilas, "columns"=>$numcolumnas, "seats"=>$numberofseats, "letters"=>$letrasmapa, "seatData"=>$letrasordenadas);
            $mapdata = array("result"=>true,"response"=>"OK", "error"=>"", "data"=>$dataval);            
        }
        return $http->responseAcceptHttpVista($mapdata);
    }   
    
    /**
     * @Rest\Get("/api/vistaticketingservice/setselectedseat/dataseats/{dataseats}/params/{params}", name="vistaTicketingServiceSetSelectedSeats", defaults={"_format":"json"})
     *
     * @SWG\Parameter(
     *     name="dataseats",
     *     in="path",
     *     type="string",
     *     description="cadena separada con ';' que contiene la información de los asientos a agregar a la orden por completar en VISTA."
     * )
     * @SWG\Parameter(
     *     name="params",
     *     in="path",
     *     type="string",
     *     description="cadena separada con ';' que contiene la información para la transacción contra VISTA."
     * )
     
     * @SWG\Response(
     *     response=200,
     *     description="respuesta del servicio."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de setSelectedSeats"
     * )
     *
     * @SWG\Tag(name="VistaTicketingSetSelectedSeats")
     */
    
    public function setSelectedSeatsAction($dataseats, $params){
        $http = $this->get('httpcustom');
        $array_seats = array();
        $ticketparam = array();
        $array_seats = explode(";", $dataseats);
        $array_params = explode(";", $params);
        $xml_seat_structure = "";
        $xml_data = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://vista.co.nz/services/WSVistaWebClient.ServiceContracts/1" xmlns:ns1="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/"><soapenv:Header/><soapenv:Body><ns:SetSelectedSeatsRequest><ns1:OptionalClientClass>{OPTIONAL_CLIENT_CLASS}</ns1:OptionalClientClass><ns1:OptionalClientId>{OPTIONAL_CLIENT_ID}</ns1:OptionalClientId><ns1:OptionalClientName>{OPTIONAL_CLIENT_NAME}</ns1:OptionalClientName><ns1:CinemaId>{CINEMA_ID}</ns1:CinemaId><ns1:SessionId>{SESSION_ID}</ns1:SessionId><ns1:UserSessionId>{USER_SESSION}</ns1:UserSessionId><ns1:ReturnOrder>true</ns1:ReturnOrder><ns1:SelectedSeats>{SEATS_STRUCTURE}</ns1:SelectedSeats></ns:SetSelectedSeatsRequest></soapenv:Body></soapenv:Envelope>';
        $xml_seat_structure = '<ns1:SelectedSeat><ns1:AreaCategoryCode>{AREA_CATEGORY_CODE}</ns1:AreaCategoryCode><ns1:AreaNumber>{AREA_NUMBER}</ns1:AreaNumber><ns1:RowIndex>{ROW_INDEX}</ns1:RowIndex><ns1:ColumnIndex>{COLUMN_INDEX}</ns1:ColumnIndex></ns1:SelectedSeat>';
        $x = 0;
        $counter = 0;
        $seats = "";
        $seats = $xml_seat_structure;
            for ($x = 0; $x < sizeof($array_seats); $x++){
                if ($counter == 0){
                    $seats = str_replace("{AREA_CATEGORY_CODE}", $array_seats[$x], $seats);
                }
                if ($counter == 1){
                    $seats = str_replace("{AREA_NUMBER}", $array_seats[$x], $seats);
                }
                if ($counter == 2){
                    $seats = str_replace("{COLUMN_INDEX}", $array_seats[$x], $seats);
                }
                if ($counter == 3){
                    $seats = str_replace("{ROW_INDEX}", $array_seats[$x], $seats);
                    $counter = 0;
                    if ($x+1 != sizeof($array_seats)){
                        $seats.= $xml_seat_structure;
                    }else{
                        break;
                    }
                }else{
                    $counter++;
                }
            }
            if (sizeof($array_params) > 4){
                $xml_data = str_replace("{SEATS_STRUCTURE}", $seats, $xml_data);
                $xml_data = str_replace("{OPTIONAL_CLIENT_CLASS}", $array_params[0], $xml_data);
                $xml_data = str_replace("{OPTIONAL_CLIENT_ID}", $array_params[1], $xml_data);
                $xml_data = str_replace("{OPTIONAL_CLIENT_NAME}", $array_params[2], $xml_data);
                $xml_data = str_replace("{USER_SESSION}", $array_params[3], $xml_data);
                $xml_data = str_replace("{CINEMA_ID}", $array_params[4], $xml_data);
                $xml_data = str_replace("{SESSION_ID}", $array_params[5], $xml_data);
                $ch = curl_init("http://172.20.5.242/WSVistaWebClient/TicketingService.asmx");
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml', 'soapaction: http://vista.co.nz/services/WSVistaWebClient.ServiceContracts/1/SetSelectedSeats'));
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml_data");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $output = curl_exec($ch);
                if(curl_exec($ch) === false){
                    return json_encode(array("result"=>false, "response"=>"Error en la llamada setSelectedSeats (CURL) ".curl_error($ch)));
                }else{
                    if (strpos($output, "OK") !== false){
                        $data = array("result"=>true, "response"=>"OK", "error"=>"", "data"=>"Establecidos con exito los asientos para la sesion [".$array_params[5]."] usuario [".$array_params[3]."]");
                    }else{
                        $data = array("result"=>false, "response"=>"ERROR", "error"=>"Error estableciendo asientos para la sesion [".$array_params[5]."] usuario [".$array_params[3]."]", "data"=>$output);
                    }
                }       
            }else{
                $data = array("result"=>false, "response"=>"ERROR", "error"=>"Parametros Errados", "data"=>"Parametros Erroneos dataseats = [".$dataseats."] params = [".$params."]");
            }    
        return $http->responseAcceptHttpVista($data);
    }
    
    /**
     * @Rest\Get("/api/vistaticketingservice/setselectedbookingseats/dataseats/{dataseats}/params/{params}", name="setselectedbookingseats", defaults={"_format":"json"})
     *
     * @SWG\Parameter(
     *     name="dataseats",
     *     in="path",
     *     type="string",
     *     description="cadena separada con ';' que contiene la información de los asientos a seleccionar (bloquear), la estructura de este parametro debe ser {AREA_CATEGORY_CODE};{AREA_NUMBER};{COLUMN_INDEX};{ROW_INDEX}, en caso de tener 2 o mas asientos separar por ':', osea {AREA_CATEGORY_CODE};{AREA_NUMBER};{COLUMN_INDEX};{ROW_INDEX}:{AREA_CATEGORY_CODE};{AREA_NUMBER};{COLUMN_INDEX};{ROW_INDEX}:n."
     * )
     * @SWG\Parameter(
     *     name="params",
     *     in="path",
     *     type="string",
     *     description="cadena separada con ';' que contiene la información para la transacción contra VISTA, la estructura de este parametro debe ser {USER_SESSION};{CINEMA_ID};{SESSION_ID};"
     * )
     
     * @SWG\Response(
     *     response=200,
     *     description="respuesta del servicio."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de setSelectedBookingsSeats"
     * )
     *
     * @SWG\Tag(name="SetSelectedBookingSeats")
     */
    
    public function setSelectedBookingSeatsAction($dataseats, $params){
        $http = $this->get('httpcustom');
        $array_seats = array();
        $ticketparam = array();
        $indataseats = array();
        $array_seats = explode(":", $dataseats);
        $array_params = explode(";", $params);
        $xml_seat_structure = "";
        $xml_data = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://vista.co.nz/services/WSVistaWebClient.ServiceContracts/1" xmlns:ns1="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/"><soapenv:Header/><soapenv:Body><ns:SetSelectedSeatsRequest><ns1:OptionalClientClass>{OPTIONAL_CLIENT_CLASS}</ns1:OptionalClientClass><ns1:OptionalClientId>{OPTIONAL_CLIENT_ID}</ns1:OptionalClientId><ns1:OptionalClientName>{OPTIONAL_CLIENT_NAME}</ns1:OptionalClientName><ns1:CinemaId>{CINEMA_ID}</ns1:CinemaId><ns1:SessionId>{SESSION_ID}</ns1:SessionId><ns1:UserSessionId>{USER_SESSION}</ns1:UserSessionId><ns1:ReturnOrder>true</ns1:ReturnOrder><ns1:SelectedSeats>{SEATS_STRUCTURE}</ns1:SelectedSeats></ns:SetSelectedSeatsRequest></soapenv:Body></soapenv:Envelope>';
        $xml_seat_structure = '<ns1:SelectedSeat><ns1:AreaCategoryCode>{AREA_CATEGORY_CODE}</ns1:AreaCategoryCode><ns1:AreaNumber>{AREA_NUMBER}</ns1:AreaNumber><ns1:RowIndex>{ROW_INDEX}</ns1:RowIndex><ns1:ColumnIndex>{COLUMN_INDEX}</ns1:ColumnIndex></ns1:SelectedSeat>';
        $x = 0;
        $y = 0;
        $counter = 0;
        $seats = "";
        for ($x = 0; $x < sizeof($array_seats); $x++){
            $seats.= $xml_seat_structure;            
            $indataseats = explode(";", $array_seats[$x]);
            $seats = str_replace("{AREA_CATEGORY_CODE}", $indataseats[0], $seats);
            $seats = str_replace("{AREA_NUMBER}", $indataseats[1], $seats);
            $seats = str_replace("{COLUMN_INDEX}", $indataseats[2], $seats);
            $seats = str_replace("{ROW_INDEX}", $indataseats[3], $seats);
        }
        $xml_data = str_replace("{SEATS_STRUCTURE}", $seats, $xml_data);
        $xml_data = str_replace("{OPTIONAL_CLIENT_CLASS}", "WWW", $xml_data);
        $xml_data = str_replace("{OPTIONAL_CLIENT_ID}", "111.111.111.111", $xml_data);
        $xml_data = str_replace("{OPTIONAL_CLIENT_NAME}", "WEBSERVER", $xml_data);
        $xml_data = str_replace("{USER_SESSION}", $array_params[0], $xml_data);
        $xml_data = str_replace("{CINEMA_ID}", $array_params[1], $xml_data);
        $xml_data = str_replace("{SESSION_ID}", $array_params[2], $xml_data);
        $this->logPago("setSelectedBookingSeats;XML Generado [".$xml_data."];NA;NA");
        
        $ch = curl_init("http://172.20.5.242/WSVistaWebClient/TicketingService.asmx");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml', 'soapaction: http://vista.co.nz/services/WSVistaWebClient.ServiceContracts/1/SetSelectedSeats'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml_data");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        if(curl_exec($ch) === false){
            return json_encode(array("result"=>false, "response"=>"Error en la llamada setSelectedSeats (CURL) ".curl_error($ch)));
        }else{
            if (strpos($output, "OK") !== false){
                $data = array("result"=>true, "response"=>"OK", "error"=>"", "data"=>"Establecidos con exito los asientos para la sesion [".$array_params[2]."] usuario [".$array_params[0]."]");
            }else{
                $data = array("result"=>false, "response"=>"ERROR", "error"=>"Error estableciendo asientos para la sesion [".$array_params[2]."] usuario [".$array_params[0]."]", "data"=>$output);
            }
        }
        return $http->responseAcceptHttpVista($data);
    }    
    
    
    /**
     * @Rest\Get("/api/vistaticketingservice/completeorder/datapayment/{datapayment}/params/{params}/mode/{mode}/numvouchers/{numvouchers}", name="vistaTicketingServiceCompleteOrder", defaults={"_format":"json"})
     *
     * @SWG\Parameter(
     *     name="datapayment",
     *     in="path",
     *     type="string",
     *     description="cadena separada con ';' que contiene la información del pago a realizar para orden por completar en VISTA."
     * )
     * @SWG\Parameter(
     *     name="params",
     *     in="path",
     *     type="string",
     *     description="cadena separada con ';' que contiene la información para la transacción contra VISTA."
     * )
     * @SWG\Parameter(
     *     name="mode",
     *     in="path",
     *     type="integer",
     *     description="indica si se debe enviar el VOUCHER como parte de esta petición, 1 - NO ENVIAR VOUCHER, SINO UNA TARJETA FICTICIA, 2 - ENVIAR VOUCHER."
     * )
     * @SWG\Parameter(
     *     name="numvouchers",
     *     in="path",
     *     type="integer",
     *     description="indica la cantidad de vouchers a usar en la transaccion."
     * )
     
     * @SWG\Response(
     *     response=200,
     *     description="respuesta del servicio."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de completeOrder"
     * )
     *
     * @SWG\Tag(name="VistaTicketingCompleteOrder")
     */
    
    public function completeOrderAction($datapayment, $params, $mode, $numvouchers){
        $http = $this->get('httpcustom');
        $array_payment = array();
        $paymentparam = array();
        $array_params = array();
        $initposVistaBookingNumber = 0;
        $endposVistaBookingNumber = 0;
        $VistaBookingNumber = "";        
        $initposVistaBookingId = 0;
        $endposVistaBookingId = 0;
        $VistaBookingId = "";        
        $initposVistaTransNumber = 0;
        $endposVistaTransNumber = 0;
        $VistaTransNumber = "";        
        $initposHistoryID = 0;
        $endposHistoryID = 0;
        $HistoryID = "";        
        $array_payment = explode(";", $datapayment);
        $array_params = explode(";", $params);
        $xml_payment_structure = "";
        if ($mode == "1"){
            $xml_data = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://vista.co.nz/services/WSVistaWebClient.ServiceContracts/1" xmlns:ns1="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/"><soapenv:Header/><soapenv:Body><ns:CompleteOrderRequest><ns1:OptionalClientClass>{OPTIONAL_CLIENT_CLASS}</ns1:OptionalClientClass><ns1:OptionalClientId>{OPTIONAL_CLIENT_ID}</ns1:OptionalClientId><ns1:OptionalClientName>{OPTIONAL_CLIENT_NAME}</ns1:OptionalClientName><ns:UserSessionId>{USER_SESSION}</ns:UserSessionId><ns:PaymentInfo><ns1:CardNumber>{CARD_NUMBER}</ns1:CardNumber><ns1:CardType>{CARD_TYPE}</ns1:CardType><ns1:CardExpiryMonth>{MONTH_EXPIRATION}</ns1:CardExpiryMonth><ns1:CardExpiryYear>{YEAR_EXPIRATION}</ns1:CardExpiryYear><ns1:PaymentValueCents>{PAYMENT_VALUE}</ns1:PaymentValueCents><ns1:PaymentSystemId>{PURCHASE_CHANNEL}</ns1:PaymentSystemId><ns1:CardCVC>{CARD_CVC}</ns1:CardCVC><ns1:PaymentTenderCategory>CREDIT</ns1:PaymentTenderCategory><ns1:UseAsBookingRef>false</ns1:UseAsBookingRef><ns1:BillingValueCents>{PAYMENT_VALUE}</ns1:BillingValueCents><ns1:CardBalance>1</ns1:CardBalance></ns:PaymentInfo><ns:PerformPayment>false</ns:PerformPayment><ns:CustomerEmail>{CUSTOMER_EMAIL}</ns:CustomerEmail><ns:CustomerPhone>{CUSTOMER_PHONE}</ns:CustomerPhone><ns:CustomerName>{CUSTOMER_NAME}</ns:CustomerName><ns:CustomerZipCode></ns:CustomerZipCode></ns:CompleteOrderRequest></soapenv:Body></soapenv:Envelope>';            
        }
        if ($mode == "2"){
            $xml_data = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://vista.co.nz/services/WSVistaWebClient.ServiceContracts/1" xmlns:ns1="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/"><soapenv:Header/><soapenv:Body><ns:CompleteOrderRequest><ns1:OptionalClientClass>{OPTIONAL_CLIENT_CLASS}</ns1:OptionalClientClass><ns1:OptionalClientId>{OPTIONAL_CLIENT_ID}</ns1:OptionalClientId><ns1:OptionalClientName>{OPTIONAL_CLIENT_NAME}</ns1:OptionalClientName><ns:UserSessionId>{USER_SESSION}</ns:UserSessionId>{PAYMENT_INFO}<ns:PerformPayment>true</ns:PerformPayment><ns:CustomerEmail>{CUSTOMER_EMAIL}</ns:CustomerEmail><ns:CustomerPhone>{CUSTOMER_PHONE}</ns:CustomerPhone><ns:CustomerName>{CUSTOMER_NAME}</ns:CustomerName><ns:CustomerZipCode></ns:CustomerZipCode></ns:CompleteOrderRequest></soapenv:Body></soapenv:Envelope>';
            $xml_payment_structure = '<ns:PaymentInfo><ns1:CardNumber>{VOUCHER_CODE}</ns1:CardNumber><ns1:CardType>{CARD_TYPE}</ns1:CardType><ns1:PaymentValueCents>{PAYMENT_VALUE}</ns1:PaymentValueCents><ns1:PaymentSystemId>ANDROID</ns1:PaymentSystemId><ns1:UseAsBookingRef>false</ns1:UseAsBookingRef><ns1:BillingValueCents>0</ns1:BillingValueCents><ns1:CardBalance>1</ns1:CardBalance></ns:PaymentInfo>';
            // *** INICIO ARMAR ESTRUCTURA DE LOS PAYMENTS ***
            $x = 0;
            $counter = 0;
            $payments = "";
            $payments = $xml_payment_structure;
            $ciclos = 0;
            for ($x = 0; $x < sizeof($array_payment); $x++){
                if ($counter == 0){
                    $payments = str_replace("{VOUCHER_CODE}", $array_payment[$x], $payments);
                    $counter++;
                }else if ($counter == 1){
                    $payments = str_replace("{CARD_TYPE}", $array_payment[$x], $payments);
                    $counter++;
                }else if ($counter == 2){
                    $payments = str_replace("{PAYMENT_VALUE}", $array_payment[$x], $payments);
                    $counter = 0;
                    $ciclos++;
                    if ($ciclos < $numvouchers){
                        $payments.= $xml_payment_structure;
                    }else{
                        break;
                    }
                }
            }
            $xml_data = str_replace("{PAYMENT_INFO}", $payments, $xml_data);
        }        
        // *** FIN ARMAR ESTRUCTURA DE LOS PAYMENTS ***
        $xml_data = str_replace("{OPTIONAL_CLIENT_CLASS}", $array_params[0], $xml_data);
        $xml_data = str_replace("{OPTIONAL_CLIENT_ID}", $array_params[1], $xml_data);
        $xml_data = str_replace("{OPTIONAL_CLIENT_NAME}", $array_params[2], $xml_data);
        $xml_data = str_replace("{USER_SESSION}", $array_params[3], $xml_data);
        $xml_data = str_replace("{CUSTOMER_EMAIL}", $array_params[4], $xml_data);
        $xml_data = str_replace("{CUSTOMER_PHONE}", $array_params[5], $xml_data);
        $xml_data = str_replace("{CUSTOMER_NAME}", str_replace("+", " ", $array_params[6]), $xml_data);
        $xml_data = str_replace("{PAYMENT_VALUE}", $array_payment[2], $xml_data);
        
        $xml_data = str_replace("{CARD_NUMBER}", $array_payment[0], $xml_data);
        $xml_data = str_replace("{CARD_TYPE}", $array_params[8], $xml_data);
        $xml_data = str_replace("{MONTH_EXPIRATION}", $array_params[9], $xml_data);
        $xml_data = str_replace("{YEAR_EXPIRATION}", $array_params[10], $xml_data);
        $xml_data = str_replace("{PURCHASE_CHANNEL}", $array_params[11], $xml_data);
        $xml_data = str_replace("{CARD_CVC}", $array_params[12], $xml_data);
//         echo "<pre>";
//         var_dump($xml_data);
//         die();
                
        $xml_data = str_replace('"true"', "", "$xml_data");
        $ch = curl_init("http://172.20.5.242/WSVistaWebClient/TicketingService.asmx");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml', 'soapaction: http://vista.co.nz/services/WSVistaWebClient.ServiceContracts/1/CompleteOrder'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch); 
        
        if(curl_exec($ch) === false){   
            $data = array("result"=>false, "response"=>"Error en la llamada completeOrder (CURL) ".curl_error($ch));
        }else{
           /* $output = '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><CompleteOrderResult xmlns="http://vista.co.nz/services/WSVistaWebClient.ServiceContracts/1"><Result>OK</Result><CinemaID>LID</CinemaID><VistaBookingNumber>37039</VistaBookingNumber><VistaBookingId>WGJRMKR</VistaBookingId><VistaTransNumber>434783</VistaTransNumber><HistoryID>2043094</HistoryID><PrintStream /><PaymentInfoCollection><PaymentInfo><CardNumber xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/">1111111111111111</CardNumber><CardType xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/">VISA</CardType><CardExpiryMonth xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/">06</CardExpiryMonth><CardExpiryYear xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/">2019</CardExpiryYear><CardValidFromMonth xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/" /><CardValidFromYear xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/" /><CardIssueNumber xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/" /><PaymentValueCents xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/">3300</PaymentValueCents><PaymentSystemId xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/">ANDROID</PaymentSystemId><CardCVC xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/" /><PaymentTenderCategory xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/">CREDIT</PaymentTenderCategory><BillFullOutstandingAmount xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/">false</BillFullOutstandingAmount><UseAsBookingRef xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/">true</UseAsBookingRef><PaymentErrorCode xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/" /><PaymentErrorDescription xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/" /><PaymentStatus xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/">NA</PaymentStatus><BillingValueCents xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/">3300</BillingValueCents><CardBalance xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/">0</CardBalance><CardHash xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/" /><SaveCardToWallet xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/">false</SaveCardToWallet></PaymentInfo></PaymentInfoCollection><ExtendedResultCode>0</ExtendedResultCode></CompleteOrderResult></soap:Body></soap:Envelope>';*/ 
            $initposVistaBookingNumber = strpos($output, "<VistaBookingNumber>");
            $endposVistaBookingNumber = strpos($output, "</VistaBookingNumber>");
            $VistaBookingNumber = substr($output, $initposVistaBookingNumber, ($endposVistaBookingNumber-$initposVistaBookingNumber));
            $VistaBookingNumber = str_replace("<VistaBookingNumber>", "", $VistaBookingNumber);
            
            $initposVistaBookingId = strpos($output, "<VistaBookingId>");
            $endposVistaBookingId = strpos($output, "</VistaBookingId>");
            $VistaBookingId = substr($output, $initposVistaBookingId, ($endposVistaBookingId-$initposVistaBookingId));
            $VistaBookingId = str_replace("<VistaBookingId>", "", $VistaBookingId);
            
            $initposVistaTransNumber = strpos($output, "<VistaTransNumber>");
            $endposVistaTransNumber = strpos($output, "</VistaTransNumber>");
            $VistaTransNumber = substr($output, $initposVistaTransNumber, ($endposVistaTransNumber-$initposVistaTransNumber));
            $VistaTransNumber = str_replace("<VistaTransNumber>", "", $VistaTransNumber);
            
            $initposHistoryID = strpos($output, "<VistaTransNumber>");
            $endposHistoryID = strpos($output, "</VistaTransNumber>");
            $HistoryID = substr($output, $initposHistoryID, ($endposHistoryID-$initposHistoryID));
            $HistoryID = str_replace("<VistaTransNumber>", "", $HistoryID);
            
            $json_booking = array();
            $json_booking = array("VistaBookingNumber"=>$VistaBookingNumber, "VistaBookingId"=>$VistaBookingId,"VistaTransNumber"=>$VistaTransNumber,"HistoryID"=>$HistoryID, "xmloutput"=>$output);
            
            if (strpos($output, "OK") !== false){
                $data = array("result"=>true, "response"=>"OK", "data"=>$json_booking, "error"=>"");
            }else{
                $data = array("result"=>false, "response"=>"Error en la llamada completeOrder", "error"=>"Error completando la orden usuario [".$array_params[3]."]", "data"=>$json_booking);
            }
        } 
        
        return $http->responseAcceptHttpVista($data);
    }
    
    /**
     * @Rest\Get("/api/vistaticketingservice/completeorderloyalty/datapayment/{datapayment}/params/{params}/mode/{mode}/numvouchers/{numvouchers}", name="vistaTicketingServiceCompleteOrderLoyalty", defaults={"_format":"json"})
     *
     * @SWG\Parameter(
     *     name="datapayment",
     *     in="path",
     *     type="string",
     *     description="cadena separada con ';' que contiene la información del pago a realizar para orden por completar en VISTA."
     * )
     * @SWG\Parameter(
     *     name="params",
     *     in="path",
     *     type="string",
     *     description="cadena separada con ';' que contiene la información para la transacción contra VISTA."
     * )
     * @SWG\Parameter(
     *     name="mode",
     *     in="path",
     *     type="integer",
     *     description="indica si se debe enviar el VOUCHER como parte de esta petición, 1 - NO ENVIAR VOUCHER, SINO UNA TARJETA FICTICIA, 2 - ENVIAR VOUCHER."
     * )
     * @SWG\Parameter(
     *     name="numvouchers",
     *     in="path",
     *     type="integer",
     *     description="indica la cantidad de vouchers a usar en la transaccion."
     * )
     
     * @SWG\Response(
     *     response=200,
     *     description="respuesta del servicio."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de completeOrder"
     * )
     *
     * @SWG\Tag(name="VistaTicketingCompleteOrderLoyalty")
     */
    
    public function completeOrderLoyaltyAction($datapayment, $params, $mode, $numvouchers){
        $http = $this->get('httpcustom');
        $array_payment = array();
        $paymentparam = array();
        $array_params = array();
        $initposVistaBookingNumber = 0;
        $endposVistaBookingNumber = 0;
        $VistaBookingNumber = "";
        $initposVistaBookingId = 0;
        $endposVistaBookingId = 0;
        $VistaBookingId = "";
        $initposVistaTransNumber = 0;
        $endposVistaTransNumber = 0;
        $VistaTransNumber = "";
        $initposHistoryID = 0;
        $endposHistoryID = 0;
        $HistoryID = "";
        $array_payment = explode(";", $datapayment);
        $array_params = explode(";", $params);
        $xml_payment_structure = "";
        if ($mode == "1"){
            $xml_data = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://vista.co.nz/services/WSVistaWebClient.ServiceContracts/1" xmlns:ns1="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/"><soapenv:Header/><soapenv:Body><ns:CompleteOrderRequest><ns1:OptionalClientClass>{OPTIONAL_CLIENT_CLASS}</ns1:OptionalClientClass><ns1:OptionalClientId>{OPTIONAL_CLIENT_ID}</ns1:OptionalClientId><ns1:OptionalClientName>{OPTIONAL_CLIENT_NAME}</ns1:OptionalClientName><ns:UserSessionId>{USER_SESSION}</ns:UserSessionId><ns:PaymentInfo><ns1:CardNumber>{CARD_NUMBER}</ns1:CardNumber><ns1:CardType>{CARD_TYPE}</ns1:CardType><ns1:CardExpiryMonth>{MONTH_EXPIRATION}</ns1:CardExpiryMonth><ns1:CardExpiryYear>{YEAR_EXPIRATION}</ns1:CardExpiryYear><ns1:PaymentValueCents>{PAYMENT_VALUE}</ns1:PaymentValueCents><ns1:PaymentSystemId>{PURCHASE_CHANNEL}</ns1:PaymentSystemId><ns1:CardCVC>{CARD_CVC}</ns1:CardCVC><ns1:PaymentTenderCategory>CREDIT</ns1:PaymentTenderCategory><ns1:UseAsBookingRef>false</ns1:UseAsBookingRef><ns1:BillingValueCents>{PAYMENT_VALUE}</ns1:BillingValueCents><ns1:CardBalance>1</ns1:CardBalance></ns:PaymentInfo><ns:PerformPayment>false</ns:PerformPayment><ns:CustomerEmail>{CUSTOMER_EMAIL}</ns:CustomerEmail><ns:CustomerPhone>{CUSTOMER_PHONE}</ns:CustomerPhone><ns:CustomerName>{CUSTOMER_NAME}</ns:CustomerName><ns:CustomerZipCode></ns:CustomerZipCode></ns:CompleteOrderRequest></soapenv:Body></soapenv:Envelope>';
        }
        if ($mode == "2"){
            $xml_data = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://vista.co.nz/services/WSVistaWebClient.ServiceContracts/1" xmlns:ns1="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/"><soapenv:Header/><soapenv:Body><ns:CompleteOrderRequest><ns1:OptionalClientClass>{OPTIONAL_CLIENT_CLASS}</ns1:OptionalClientClass><ns1:OptionalClientId>{OPTIONAL_CLIENT_ID}</ns1:OptionalClientId><ns1:OptionalClientName>{OPTIONAL_CLIENT_NAME}</ns1:OptionalClientName><ns:UserSessionId>{USER_SESSION}</ns:UserSessionId>{PAYMENT_INFO}<ns:PerformPayment>true</ns:PerformPayment><ns:CustomerEmail>{CUSTOMER_EMAIL}</ns:CustomerEmail><ns:CustomerPhone>{CUSTOMER_PHONE}</ns:CustomerPhone><ns:CustomerName>{CUSTOMER_NAME}</ns:CustomerName><ns:CustomerZipCode></ns:CustomerZipCode></ns:CompleteOrderRequest></soapenv:Body></soapenv:Envelope>';
            $xml_payment_structure = '<ns:PaymentInfo><ns1:CardNumber>{VOUCHER_CODE}</ns1:CardNumber><ns1:CardType>{CARD_TYPE}</ns1:CardType><ns1:PaymentValueCents>{PAYMENT_VALUE}</ns1:PaymentValueCents><ns1:PaymentSystemId>ANDROID</ns1:PaymentSystemId><ns1:UseAsBookingRef>false</ns1:UseAsBookingRef><ns1:BillingValueCents>0</ns1:BillingValueCents><ns1:CardBalance>1</ns1:CardBalance></ns:PaymentInfo>';
            // *** INICIO ARMAR ESTRUCTURA DE LOS PAYMENTS ***
            $x = 0;
            $counter = 0;
            $payments = "";
            $payments = $xml_payment_structure;
            $ciclos = 0;
            for ($x = 0; $x < sizeof($array_payment); $x++){
                if ($counter == 0){
                    $payments = str_replace("{VOUCHER_CODE}", $array_payment[$x], $payments);
                    $counter++;
                }else if ($counter == 1){
                    $payments = str_replace("{CARD_TYPE}", $array_payment[$x], $payments);
                    $counter++;
                }else if ($counter == 2){
                    $payments = str_replace("{PAYMENT_VALUE}", $array_payment[$x], $payments);
                    $counter = 0;
                    $ciclos++;
                    if ($ciclos < $numvouchers){
                        $payments.= $xml_payment_structure;
                    }else{
                        break;
                    }
                }
            }
            $xml_data = str_replace("{PAYMENT_INFO}", $payments, $xml_data);
        }
        // *** FIN ARMAR ESTRUCTURA DE LOS PAYMENTS ***
        $xml_data = str_replace("{OPTIONAL_CLIENT_CLASS}", $array_params[0], $xml_data);
        $xml_data = str_replace("{OPTIONAL_CLIENT_ID}", $array_params[1], $xml_data);
        $xml_data = str_replace("{OPTIONAL_CLIENT_NAME}", $array_params[2], $xml_data);
        $xml_data = str_replace("{USER_SESSION}", $array_params[3], $xml_data);
        $xml_data = str_replace("{CUSTOMER_EMAIL}", $array_params[4], $xml_data);
        $xml_data = str_replace("{CUSTOMER_PHONE}", $array_params[5], $xml_data);
        $xml_data = str_replace("{CUSTOMER_NAME}", str_replace("+", " ", $array_params[6]), $xml_data);
        $xml_data = str_replace("{PAYMENT_VALUE}", $array_payment[2], $xml_data);
        
        $xml_data = str_replace("{CARD_NUMBER}", $array_payment[0], $xml_data);
        $xml_data = str_replace("{CARD_TYPE}", $array_params[8], $xml_data);
        $xml_data = str_replace("{MONTH_EXPIRATION}", $array_params[9], $xml_data);
        $xml_data = str_replace("{YEAR_EXPIRATION}", $array_params[10], $xml_data);
        $xml_data = str_replace("{PURCHASE_CHANNEL}", $array_params[11], $xml_data);
        $xml_data = str_replace("{CARD_CVC}", $array_params[12], $xml_data);
        $xml_data = str_replace("{MEMBERID}", $array_params[13], $xml_data);
        
        $xml_data = str_replace('"true"', "", "$xml_data");
        $ch = curl_init("http://172.20.5.242/WSVistaWebClient/TicketingService.asmx");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml', 'soapaction: http://vista.co.nz/services/WSVistaWebClient.ServiceContracts/1/CompleteOrder'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        if(curl_exec($ch) === false){
            $data = array("result"=>false, "response"=>"Error en la llamada completeOrder (CURL) ".curl_error($ch));
        }else{
            /* $output = '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><CompleteOrderResult xmlns="http://vista.co.nz/services/WSVistaWebClient.ServiceContracts/1"><Result>OK</Result><CinemaID>LID</CinemaID><VistaBookingNumber>37039</VistaBookingNumber><VistaBookingId>WGJRMKR</VistaBookingId><VistaTransNumber>434783</VistaTransNumber><HistoryID>2043094</HistoryID><PrintStream /><PaymentInfoCollection><PaymentInfo><CardNumber xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/">1111111111111111</CardNumber><CardType xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/">VISA</CardType><CardExpiryMonth xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/">06</CardExpiryMonth><CardExpiryYear xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/">2019</CardExpiryYear><CardValidFromMonth xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/" /><CardValidFromYear xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/" /><CardIssueNumber xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/" /><PaymentValueCents xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/">3300</PaymentValueCents><PaymentSystemId xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/">ANDROID</PaymentSystemId><CardCVC xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/" /><PaymentTenderCategory xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/">CREDIT</PaymentTenderCategory><BillFullOutstandingAmount xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/">false</BillFullOutstandingAmount><UseAsBookingRef xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/">true</UseAsBookingRef><PaymentErrorCode xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/" /><PaymentErrorDescription xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/" /><PaymentStatus xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/">NA</PaymentStatus><BillingValueCents xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/">3300</BillingValueCents><CardBalance xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/">0</CardBalance><CardHash xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/" /><SaveCardToWallet xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/">false</SaveCardToWallet></PaymentInfo></PaymentInfoCollection><ExtendedResultCode>0</ExtendedResultCode></CompleteOrderResult></soap:Body></soap:Envelope>';*/
            $initposVistaBookingNumber = strpos($output, "<VistaBookingNumber>");
            $endposVistaBookingNumber = strpos($output, "</VistaBookingNumber>");
            $VistaBookingNumber = substr($output, $initposVistaBookingNumber, ($endposVistaBookingNumber-$initposVistaBookingNumber));
            $VistaBookingNumber = str_replace("<VistaBookingNumber>", "", $VistaBookingNumber);
            
            $initposVistaBookingId = strpos($output, "<VistaBookingId>");
            $endposVistaBookingId = strpos($output, "</VistaBookingId>");
            $VistaBookingId = substr($output, $initposVistaBookingId, ($endposVistaBookingId-$initposVistaBookingId));
            $VistaBookingId = str_replace("<VistaBookingId>", "", $VistaBookingId);
            
            $initposVistaTransNumber = strpos($output, "<VistaTransNumber>");
            $endposVistaTransNumber = strpos($output, "</VistaTransNumber>");
            $VistaTransNumber = substr($output, $initposVistaTransNumber, ($endposVistaTransNumber-$initposVistaTransNumber));
            $VistaTransNumber = str_replace("<VistaTransNumber>", "", $VistaTransNumber);
            
            $initposHistoryID = strpos($output, "<VistaTransNumber>");
            $endposHistoryID = strpos($output, "</VistaTransNumber>");
            $HistoryID = substr($output, $initposHistoryID, ($endposHistoryID-$initposHistoryID));
            $HistoryID = str_replace("<VistaTransNumber>", "", $HistoryID);
            
            $json_booking = array();
            $json_booking = array("VistaBookingNumber"=>$VistaBookingNumber, "VistaBookingId"=>$VistaBookingId,"VistaTransNumber"=>$VistaTransNumber,"HistoryID"=>$HistoryID);
            
            if (strpos($output, "OK") !== false){
                $data = array("result"=>true, "response"=>"OK", "data"=>$json_booking, "error"=>"");
            }else{
                $data = array("result"=>false, "response"=>"Error en la llamada completeOrder", "error"=>"Error completando la orden usuario [".$array_params[3]."]", "data"=>$output);
            }
        }
        
        return $http->responseAcceptHttpVista($data);
    }
    
    /**
     * @Rest\Get("/api/vistaticketingservice/logpago/params/{params}", name="logPago", defaults={"_format":"json"})
     *
     * @SWG\Parameter(
     *     name="params",
     *     in="path",
     *     type="string",
     *     description="Array con la data a registrar en el log de pago [metodo;descripcion;userid;uservistaid]."
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Array con el resultado de la ejecucion de logPago."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar logPago."
     * )#
     *
     * @SWG\Tag(name="LogPagoAction")
     */
    public function logPago($params){
        $http = $this->get('httpcustom');
        $logpath = "/var/www/html/cinexbackend/log/logpago".date("Ymd").".log";
        $parametros = array();
        $parametros = explode(";", $params);
        // p = metodo;descripcion;usuarioid;uservistaid
        $logentry = "";
        // ESTRUCTURA
        // [FECHA] [IP REMOTA] [METODO] [DESCRIPCION] [USUARIOID] [USERVISTAID]
        $logentry = "[".date("Y-m-d h:i:s")."] [IP : ".$_SERVER['REMOTE_ADDR']."] [METODO : ".$parametros[0]."] [ DESCRIPCION : ".$parametros[1]."] [ USUARIOID : ".$parametros[2]."] [ USERVISTAID : ".$parametros[3]."]\n";
        $result = file_put_contents($logpath,$logentry, FILE_APPEND);

        $ch = curl_init("https://172.28.85.214/clases/vistaMovilCinexMethods.php?metodo=logpago&p=".$params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
        
        if ($output !== false){
            $response = array("result"=>true, "response"=>"OK", "data"=>$output, "error"=>"");
        }else{
            $response = array("result"=>false, "response"=>"Error en la llamada log pago", "error"=>"Error escribiendo en log de pago", "data"=>$output);
        }
        return $http->responseAcceptHttpVista($response);
    }
        
    /**
     * @Rest\Get("/api/vistaticketingservice/sendpurchaseemail/params/{params}", name="SendPurchaseEmail", defaults={"_format":"json"})
     *
     * @SWG\Parameter(
     *     name="params",
     *     in="path",
     *     type="string",
     *     description="String con la data separara por ; para envio del correo de pago exitoso al usuario [{NOMBREPELICULA};{NOMBREUSUARIO};{CINEMAID};{SALA};{FECHAFUNCION};{HORAFUNCION};{FORMATOPELICULA};{CANTIDADBLETOS};{ASIENTOSSEPARADOSPORCOMMMA};{LOCALIZADOR};{EMAIL};{POSTER};{TECNOLOGIA};{VOUCHERCOMPRA}]."
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Array con el resultado de la ejecucion de EnviarCorreo."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar EnviarCorreo."
     * )#
     *
     * @SWG\Tag(name="SendPurchaseEmail")
     */
    public function sendPurchaseEmailAction($params){
        $http = $this->get('httpcustom');
        $valor_correo = "";
        $curl = null;
        $resp = null;
        $response = null;
        $data = explode(";", $params);
        //$voucher = str_replace("&nbsp;", "|", $voucher);                
        $valor_correo ='http://172.28.85.214/correoUsuario_Movil_Pago.php?pelicula='.urlencode($data[0]).'&nombre='.urlencode($data[1]).'&letra=A&complejo='.urlencode(utf8_encode($data[2])).'&idmensaje=1&sala='.urlencode($data[3]).'&fecha='.$data[4].'&hora='.$data[5].'&formato='.urlencode($data[6]).'&boletos='.$data[7].'&asientos='.str_replace(" ", "", $data[8]).'&localizador='.$data[9].'&email='.$data[10].'&poster='.$data[11].'&tecnologia='.$data[12].'&voucher='.$data[13];
        //$this->logPago($metodo.";Enviando correo compra exitosa ".$data[12].";NA;NA");
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $valor_correo,
            CURLOPT_USERAGENT => 'CINEX'
        ));
        $resp = curl_exec($curl);
        curl_close($curl);
        $response = array("result"=>true, "response"=>"OK", "data"=>"", "error"=>"");
        return $http->responseAcceptHttpVista($response);
    }   
        
    /**
     * @Rest\Get("/api/vistaticketingservice/completebookingorder/datapayment/{datapayment}/params/{params}", name="CompleteBookingOrder", defaults={"_format":"json"})
     *
     * @SWG\Parameter(
     *     name="datapayment",
     *     in="path",
     *     type="string",
     *     description="cadena separada con ';' que contiene la información del pago a realizar para orden por completar en VISTA, La estructura de este parametro seria : {CARD_NUMBER};{CARD_TYPE};{MONTH_EXPIRATION};{YEAR_EXPIRATION};{PURCHASE_CHANNEL};{CARD_CVC};{PAYMENT_VALUE}"
     * )
     * @SWG\Parameter(
     *     name="params",
     *     in="path",
     *     type="string",
     *     description="cadena separada con ';' que contiene la información para la transacción contra VISTA, La estructura de este parametro seria : {USER_SESSION};{CUSTOMER_EMAIL};{CUSTOMER_PHONE};{CUSTOMER_NAME}"
     * )     
     * @SWG\Response(
     *     response=200,
     *     description="respuesta del servicio."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de CompleteBookingOrder"
     * )
     *
     * @SWG\Tag(name="CompleteBookingOrder")
     */
    
    public function completeBookingOrderAction($datapayment, $params){
        $http = $this->get('httpcustom');
        $array_payment = array();
        $paymentparam = array();
        $array_params = array();
        $initposVistaBookingNumber = 0;
        $endposVistaBookingNumber = 0;
        $VistaBookingNumber = "";
        $initposVistaBookingId = 0;
        $endposVistaBookingId = 0;
        $VistaBookingId = "";
        $initposVistaTransNumber = 0;
        $endposVistaTransNumber = 0;
        $VistaTransNumber = "";
        $initposHistoryID = 0;
        $endposHistoryID = 0;
        $HistoryID = "";
        $array_payment = explode(";", $datapayment);
        $array_params = explode(";", $params);
        $xml_payment_structure = "";
        $xml_data = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://vista.co.nz/services/WSVistaWebClient.ServiceContracts/1" xmlns:ns1="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/"><soapenv:Header/><soapenv:Body><ns:CompleteOrderRequest><ns1:OptionalClientClass>{OPTIONAL_CLIENT_CLASS}</ns1:OptionalClientClass><ns1:OptionalClientId>{OPTIONAL_CLIENT_ID}</ns1:OptionalClientId><ns1:OptionalClientName>{OPTIONAL_CLIENT_NAME}</ns1:OptionalClientName><ns:UserSessionId>{USER_SESSION}</ns:UserSessionId><ns:PaymentInfo><ns1:CardNumber>{CARD_NUMBER}</ns1:CardNumber><ns1:CardType>{CARD_TYPE}</ns1:CardType><ns1:CardExpiryMonth>{MONTH_EXPIRATION}</ns1:CardExpiryMonth><ns1:CardExpiryYear>{YEAR_EXPIRATION}</ns1:CardExpiryYear><ns1:PaymentValueCents>{PAYMENT_VALUE}</ns1:PaymentValueCents><ns1:PaymentSystemId>{PURCHASE_CHANNEL}</ns1:PaymentSystemId><ns1:CardCVC>{CARD_CVC}</ns1:CardCVC><ns1:PaymentTenderCategory>CREDIT</ns1:PaymentTenderCategory><ns1:UseAsBookingRef>false</ns1:UseAsBookingRef><ns1:BillingValueCents>{PAYMENT_VALUE}</ns1:BillingValueCents><ns1:CardBalance>1</ns1:CardBalance></ns:PaymentInfo><ns:PerformPayment>false</ns:PerformPayment><ns:CustomerEmail>{CUSTOMER_EMAIL}</ns:CustomerEmail><ns:CustomerPhone>{CUSTOMER_PHONE}</ns:CustomerPhone><ns:CustomerName>{CUSTOMER_NAME}</ns:CustomerName></ns:CompleteOrderRequest></soapenv:Body></soapenv:Envelope>';
        // *** FIN ARMAR ESTRUCTURA DE LOS PAYMENTS ***
        $xml_data = str_replace("{OPTIONAL_CLIENT_CLASS}", "WWW", $xml_data);
        $xml_data = str_replace("{OPTIONAL_CLIENT_ID}", "111.111.111.111", $xml_data);
        $xml_data = str_replace("{OPTIONAL_CLIENT_NAME}", "WEBSERVER", $xml_data);
        $xml_data = str_replace("{USER_SESSION}", $array_params[0], $xml_data);
        $xml_data = str_replace("{CUSTOMER_EMAIL}", $array_params[1], $xml_data);
        $xml_data = str_replace("{CUSTOMER_PHONE}", $array_params[2], $xml_data);
        $xml_data = str_replace("{CUSTOMER_NAME}", str_replace("+", " ", $array_params[3]), $xml_data);
        
        $xml_data = str_replace("{CARD_NUMBER}", $array_payment[0], $xml_data);
        $xml_data = str_replace("{CARD_TYPE}", $array_payment[1], $xml_data);
        $xml_data = str_replace("{MONTH_EXPIRATION}", $array_payment[2], $xml_data);
        $xml_data = str_replace("{YEAR_EXPIRATION}", $array_payment[3], $xml_data);
        $xml_data = str_replace("{PURCHASE_CHANNEL}", $array_payment[4], $xml_data);
        $xml_data = str_replace("{CARD_CVC}", $array_payment[5], $xml_data);
        $xml_data = str_replace("{PAYMENT_VALUE}", $array_payment[6], $xml_data);
                
        $xml_data = str_replace('"true"', "", "$xml_data");
        $ch = curl_init("http://172.20.5.242/WSVistaWebClient/TicketingService.asmx");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml', 'soapaction: http://vista.co.nz/services/WSVistaWebClient.ServiceContracts/1/CompleteOrder'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        if(curl_exec($ch) === false){
            $data = array("result"=>false, "response"=>"Error en la llamada completeOrder (CURL) ".curl_error($ch));
        }else{
            /* $output = '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><CompleteOrderResult xmlns="http://vista.co.nz/services/WSVistaWebClient.ServiceContracts/1"><Result>OK</Result><CinemaID>LID</CinemaID><VistaBookingNumber>37039</VistaBookingNumber><VistaBookingId>WGJRMKR</VistaBookingId><VistaTransNumber>434783</VistaTransNumber><HistoryID>2043094</HistoryID><PrintStream /><PaymentInfoCollection><PaymentInfo><CardNumber xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/">1111111111111111</CardNumber><CardType xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/">VISA</CardType><CardExpiryMonth xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/">06</CardExpiryMonth><CardExpiryYear xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/">2019</CardExpiryYear><CardValidFromMonth xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/" /><CardValidFromYear xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/" /><CardIssueNumber xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/" /><PaymentValueCents xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/">3300</PaymentValueCents><PaymentSystemId xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/">ANDROID</PaymentSystemId><CardCVC xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/" /><PaymentTenderCategory xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/">CREDIT</PaymentTenderCategory><BillFullOutstandingAmount xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/">false</BillFullOutstandingAmount><UseAsBookingRef xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/">true</UseAsBookingRef><PaymentErrorCode xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/" /><PaymentErrorDescription xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/" /><PaymentStatus xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/">NA</PaymentStatus><BillingValueCents xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/">3300</BillingValueCents><CardBalance xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/">0</CardBalance><CardHash xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/" /><SaveCardToWallet xmlns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/">false</SaveCardToWallet></PaymentInfo></PaymentInfoCollection><ExtendedResultCode>0</ExtendedResultCode></CompleteOrderResult></soap:Body></soap:Envelope>';*/
            $initposVistaBookingNumber = strpos($output, "<VistaBookingNumber>");
            $endposVistaBookingNumber = strpos($output, "</VistaBookingNumber>");
            $VistaBookingNumber = substr($output, $initposVistaBookingNumber, ($endposVistaBookingNumber-$initposVistaBookingNumber));
            $VistaBookingNumber = str_replace("<VistaBookingNumber>", "", $VistaBookingNumber);
            
            $initposVistaBookingId = strpos($output, "<VistaBookingId>");
            $endposVistaBookingId = strpos($output, "</VistaBookingId>");
            $VistaBookingId = substr($output, $initposVistaBookingId, ($endposVistaBookingId-$initposVistaBookingId));
            $VistaBookingId = str_replace("<VistaBookingId>", "", $VistaBookingId);
            
            $initposVistaTransNumber = strpos($output, "<VistaTransNumber>");
            $endposVistaTransNumber = strpos($output, "</VistaTransNumber>");
            $VistaTransNumber = substr($output, $initposVistaTransNumber, ($endposVistaTransNumber-$initposVistaTransNumber));
            $VistaTransNumber = str_replace("<VistaTransNumber>", "", $VistaTransNumber);
            
            $initposHistoryID = strpos($output, "<VistaTransNumber>");
            $endposHistoryID = strpos($output, "</VistaTransNumber>");
            $HistoryID = substr($output, $initposHistoryID, ($endposHistoryID-$initposHistoryID));
            $HistoryID = str_replace("<VistaTransNumber>", "", $HistoryID);
            
            $json_booking = array();
            $json_booking = array("VistaBookingNumber"=>$VistaBookingNumber, "VistaBookingId"=>$VistaBookingId,"VistaTransNumber"=>$VistaTransNumber,"HistoryID"=>$HistoryID, "xmloutput"=>$output);
            
            if (strpos($output, "OK") !== false){
                $data = array("result"=>true, "response"=>"OK", "data"=>$json_booking, "error"=>"");
            }else{
                $data = array("result"=>false, "response"=>"Error en la llamada completeOrder", "error"=>"Error completando la orden usuario [".$array_params[3]."]", "data"=>$json_booking);
            }
        }
        
        return $http->responseAcceptHttpVista($data);
    } 
    
    /**
     * @Rest\Get("/api/vistaticketingservice/getsinglebooking/bookingid/{bookingid}/bookingnumber/{bookingnumber}/cinemaid/{cinemaid}", name="getsinglebooking", defaults={"_format":"json"})
     *
     * @SWG\Parameter(
     *     name="bookingid",
     *     in="path",
     *     type="string",
     *     description="localizador."
     * )
     * @SWG\Parameter(
     *     name="bookingnumber",
     *     in="path",
     *     type="integer",
     *     description="numero de reserva."
     * )
     * @SWG\Parameter(
     *     name="cinemaid",
     *     in="path",
     *     type="string",
     *     description="siglas del complejo."
     * )
     
     * @SWG\Response(
     *     response=200,
     *     description="Array con el resultado de la ejecucion de getsinglebooking."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar getsinglebooking."
     * )#
     *
     * @SWG\Tag(name="GetSingleBooking")
     */
    public function getSingleBookingAction($bookingid, $bookingnumber, $cinemaid){
        $http = $this->get('httpcustom');
        $xml = "<?xml version='1.0' encoding='utf-8'?>
<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>
  <soap:Body>
    <GetSingleBookingRequest xmlns='http://vista.co.nz/services/WSVistaWebClient.ServiceContracts/1'>
      <ReturnTransactionStatusInfoIfSingleBookingMatch>false</ReturnTransactionStatusInfoIfSingleBookingMatch>
      <BookingId>{bookingid}</BookingId>
      <BookingNumber>{bookingnumber}</BookingNumber>
      <CinemaId>{cinemaid}</CinemaId>
      <IncludeBookingPrintStream>false</IncludeBookingPrintStream>
      <BookingPrintStreamTemplateName>1</BookingPrintStreamTemplateName>
      <OptionalClientId>111.111.111.111</OptionalClientId>
      <GetVoucherStatus>true</GetVoucherStatus>
    </GetSingleBookingRequest>
  </soap:Body>
</soap:Envelope>";
        $xml = str_replace("{bookingid}", $bookingid, $xml);
        $xml = str_replace("{bookingnumber}", $bookingnumber, $xml);
        $xml = str_replace("{cinemaid}", $cinemaid, $xml);
        $ch = curl_init("http://172.20.5.242/WSVistaWebClient/BookingService.asmx");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml', 'soapaction: http://vista.co.nz/services/WSVistaWebClient.ServiceContracts/1/GetSingleBooking'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
        if ($output != ""){
            $response = array("codRespuesta" => "0","desRespuesta" => "informacion de la orden obtenida exitosamente", "data"=>$output, "method"=>"getsinglebooking");
        }else{
            $response = array("codRespuesta" => "-1","desRespuesta" => "Error obteniendo la información de la orden", "data"=>$output, "method"=>"getsinglebooking");
        }
        return $http->responsePaymentAcceptHttp($response);
    }
    
    /**
     * @Rest\Get("/api/vistaticketingservice/refundbooking/bookingnumber/{bookingnumber}/cinemaid/{cinemaid}/montototal/{montototal}", name="refundbooking", defaults={"_format":"json"})
     *
     * @SWG\Parameter(
     *     name="bookingnumber",
     *     in="path",
     *     type="string",
     *     description="numero de reserva."
     * )
     * @SWG\Parameter(
     *     name="cinemaid",
     *     in="path",
     *     type="string",
     *     description="siglas del complejo."
     * )
     * @SWG\Parameter(
     *     name="montototal",
     *     in="path",
     *     type="integer",
     *     description="monto total de la orden."
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Array con el resultado de la ejecucion de RefundBooking."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar RefundBooking."
     * )#
     *
     * @SWG\Tag(name="RefundBooking")
     */
    public function refundBookingAction($bookingnumber, $cinemaid, $montototal){
        $http = $this->get('httpcustom');
        $xml = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://vista.co.nz/services/WSVistaWebClient.ServiceContracts/1" xmlns:ns1="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/">
   <soapenv:Header/>
   <soapenv:Body>
      <ns:RefundBookingRequest>
         <ns:BookingNumber>{bookingnumber}</ns:BookingNumber>
         <ns:CinemaId>{cinemaid}</ns:CinemaId>
         <ns:RefundBookingFee>true</ns:RefundBookingFee>
         <ns:RefundConcessions>false</ns:RefundConcessions>
         <ns:RefundReason>Auto Reverso Backend CAV</ns:RefundReason>
         <ns:RefundAmount>{montototal}</ns:RefundAmount>
	    <ns:IsPriorDayRefund>false</ns:IsPriorDayRefund>
      </ns:RefundBookingRequest>
   </soapenv:Body>
</soapenv:Envelope>';
        $xml = str_replace("{bookingnumber}", $bookingnumber, $xml);
        $xml = str_replace("{cinemaid}", $cinemaid, $xml);
        $xml = str_replace("{montototal}", $montototal, $xml);
        $ch = curl_init("http://172.20.5.242/WSVistaWebClient/BookingService.asmx");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml', 'soapaction: http://vista.co.nz/services/WSVistaWebClient.ServiceContracts/1/RefundBooking'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
        if (strstr($output, "<ResultCode>0</ResultCode>") != ""){
            $response = array("codRespuesta" => "0","desRespuesta" => "orden reversada exitosamente", "data"=>$output, "method"=>"getsinglebooking");
        }else{
            $response = array("codRespuesta" => "-1","desRespuesta" => "Error reversando la orden", "data"=>$output, "method"=>"getsinglebooking");
        }
        return $http->responsePaymentAcceptHttp($response);
    }    
    
    /**
     * @Rest\Get("/api/vistaticketingservice/completePayment/param/{param}", name="completePayment", defaults={"_format":"json"})
     *
     * @SWG\Parameter(
     *     name="param",
     *     in="path",
     *     type="string",
     *     description="Parametros para completar el proceso de cobro de la orden al cliente, debe tener la siguiente estructura [{CORREO};{NOMBRECLIENTE};{CELULAR};{USERID};{MESEXP};{AÑOEXP};{NUMTARJETA};{MONTOBOLETOS};{MONTOCOMISION};{CVV};{TIPODETARJETA};{CEDULA};{COMPLEJO};{ASIENTOS};{TECNOLOGIA};{TOKEN};{SESSIONID};{CODAFILIACION};{MODE}]."
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Array con el resultado de la ejecucion de completePayment."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar completePayment."
     * )#
     *
     * @SWG\Tag(name="completePayment")
     */
    public function completePayment($param){
        $http = $this->get('httpcustom');
        $array_pago = array();
        $array_pago = explode(";", $param);
        $correo = "";
        $nombrecliente = "";
        $tipotrans = 0;
        $celular = "";
        $userid = "";
        $mesexp = "";
        $anoexp = "";
        $numtarjeta = "";
        $montoboletos = "";
        $montocomision = "";
        $cvv = "";
        $complejo = "";
        $asientos = "";
        $tecnologia = "";
        $tipotarjeta = "";
        $usersessionid = "";
        $cantpuestos = 0;
        $montopagototal = 0;
        $valusuarios = "";
        $ruta_produccion = "";
        $curl = null;
        $resp = null;
        $json_datausuario = array();
        $usersessionid = "";
        $cedula = "";
        $ceduladb = "";
        $nombreusuariodb = "";
        $Ntarjfinal = "";
        $cantpuesto = 0;
        $datapayment_completeorder = "";
        $params_completeorder = "";
        $resp = "";
        $token = "";
        $JSONcompleteorder = array();
        $json_datausuario = array();
        $localizador = "";
        $numreserva = "";
        $mensfinal = "";
        $respcomplorden = false;
        $valtoken = "";
        $json_token = array();
        $numeroreferencia = "0";
        $urlbill = "";
        $json_bill = array();
        $codafiliacion = "";
        $valor = "";
        $valpagos = "";
        $json_tdc = array();
        $respdescripcion = "";
        $ordenidcompra = 0;
        $voucher_pago = "";
        $respuesta = "";
        $authid = 0;
        $porreversar = "";
        $causas_error = "";
        $sessionid = "";
        $json_session = array();
        $tipomensajeid = 1;
        $puestos = array();
        $montompfull_movistar = NULL;
        $serviciompfull_movistar = NULL;
        $usersessionid = "";
        $vistavoucher_null = NULL;
        $vencashvoucher_null = NULL;
        $detalleboleto_null = NULL;
        $vistamontoincent_null = NULL;
        $giftnombre = NULL;
        $giftcorreo = NULL;
        $giftmensaje = NULL;
        $giftimage = NULL;
        $giftcinta = NULL;
        $giftde = NULL;
        $fechaHoy =  date("Y-m-d");
        $fechaHoyTrans =  date("Y-m-d H:i:s");
        $memberIDT = "";
        $membercardnumber = "";
        $json_datapayment = array();
        $mode = 0;
        $numasientos = 0;
        $fechexpira = "";
        
        $ruta_produccion = 'http://172.20.4.236';
        
        $correo = $array_pago[0];
        $nombrecliente = $array_pago[1];
        $celular = $array_pago[2];
        $userid = $array_pago[3];
        $mesexp = $array_pago[4];
        $anoexp = $array_pago[5];
        $numtarjeta = $array_pago[6];
        $montoboletos = $array_pago[7];
        $montocomision = $array_pago[8];
        $cvv = $array_pago[9];
        $tipotarjeta = $array_pago[10];
        $cedula = $array_pago[11];
        $complejo = $array_pago[12];
        $asientos = $array_pago[13];
        $tecnologia = $array_pago[14]; 
        $token = $array_pago[15];   
        $sessionid = $array_pago[16];
        $codafiliacion = $array_pago[17];
        $mode = $array_pago[18];
        $fechexpira = $mesexp.$anoexp;
        
        $Ntarjfinal = substr($numtarjeta, 0, 6);
        $Ntarjfinal.= "******". substr($numtarjeta, 12, 6);
                        
        if ($tipotarjeta == ""){
            if (substr($numtarjeta, 0, 1) == "5" || substr($numtarjeta, 0, 1) == "2"){
                $tipotarjeta = "MASTERCARD";
            }
            if (substr($numtarjeta, 0, 1) == "4"){
                $tipotarjeta = "VISA";
            }
            if (substr($numtarjeta, 0, 1) == "3"){
                $tipotarjeta = "AMEX";
            }
        }
        
        $cantpuestos = explode(",", $asientos);
        $montopagototal = ($montoboletos) + ($montocomision);
        
        // *******************************************************************************
        // BUSCAR LA INFORMACION DEL USUARIO QUE EFECTUA LA TRANSACCION
        // *******************************************************************************
        $valusuarios = $ruta_produccion.'/api/usuario/getusuariobyid/usuarioid/'.$userid;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $valusuarios,
            CURLOPT_USERAGENT => 'CINEX'
        ));
        $resp = curl_exec($curl);
        curl_close($curl);
        $json_datausuario = json_decode($resp);
        $uservistaid = $json_datausuario->response->uservistaid;
        $ceduladb = $json_datausuario->response->ci;
        $nombreusuariodb = $json_datausuario->response->nombre." ".$json_datausuario->response->apellido;
        $memberIDT = $json_datausuario->response->memberid;
        if ($memberIDT == "" || $memberIDT == null){
            $memberIDT = "NA";
        }
        $membercardnumber = $json_datausuario->response->membercardnumber;
        // si el nombre del cliente viene vacio o null, usar nombre del cliente que viene de la db.
        if ($nombrecliente == "" || $nombrecliente == null) $nombrecliente = $nombreusuariodb;
        // si la cedula es vacia o null, usar la cedula que viene de la db.
        if ($cedula == "" || $cedula == null) $cedula = $ceduladb;
        $puestos = explode(",", $asientos);
        $numasientos = sizeof($puestos);
        // si la sala no tiene asientos numerados, usar la cantidad de asientos en vez de 00,00...
        if (strstr($asientos, "00") != ""){
            if ($cantpuesto > 0){
                $numasientos = $cantpuesto;
            }else{
                $numasientos = 1;
            }
        }
        // *******************************************************************************
        // INICIO INVOCACION COMPLETAR LA ORDEN
        // *******************************************************************************
        $datapayment_completeorder = $numtarjeta.";CREDIT;".$mesexp.";".$anoexp.";".$tecnologia.";".$cvv.";".$montopagototal;
        $params_completeorder = $uservistaid .";". $correo .";". $celular .";".str_replace(" ", "%20", $nombrecliente);
        $valcompleteorder = $ruta_produccion.'/api/vistaticketingservice/completebookingorder/datapayment/'.$datapayment_completeorder."/params/".$params_completeorder;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $valcompleteorder,
            CURLOPT_USERAGENT => 'CINEX'
        ));
        $resp = curl_exec($curl);
        curl_close($curl);  
        $JSONcompleteorder = json_decode($resp, true);
        $this->logPago("CompletePayment;Respuesta CompleteBookingOrder [".$resp."];NA;NA");
        if (isset($JSONcompleteorder["response"]["VistaBookingId"])){
            $localizador = $JSONcompleteorder["response"]["VistaBookingId"];
        }
        if (isset($JSONcompleteorder["response"]["VistaBookingNumber"])){
            $numreserva = $JSONcompleteorder["response"]["VistaBookingNumber"];
        }
        if ($JSONcompleteorder["result"] == "OK" || $localizador != ""){
            $respcomplorden = true;
        }else{
            $porreversar = 'Y';
            $respcomplorden = false;
            $mensfinal = "VISTA: ERROR COMPLETANDO LA ORDEN";
        }
        // *******************************************************************************
        // FIN COMPLETAR LA ORDEN
        // *******************************************************************************                
        // *******************************************************************************
        // ----------------
        // *******************************************************************************
        // SOLAMENTE SI LA ORDEN FUE COMPLETADA, COBRAR TDC CONTRA MEGASOFT
        if ($localizador != "" && $localizador != "<" && $numreserva != "" && $numreserva != "<" && $respcomplorden){
            // *******************************************************************************
            // BUSCAR EL TOKEN PARA REALIZAR LA TRANSACCION CONTRA MEGASOFT
            // *******************************************************************************
            $valtoken = $ruta_produccion.'/api/payment/megasoftgeneratetoken/userid/'.$userid;
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_POST           => 1,
                CURLOPT_URL => $valtoken,
                CURLOPT_USERAGENT => 'CINEX'
            ));
            $resp = curl_exec($curl);
            $json_token =json_decode($resp);
            $token = $json_token->response->token;
            curl_close($curl);
            // *******************************************************************************
            // ----------------
            // *******************************************************************************
            // *******************************************************************************
            // ** LLAMADA A DOPAYMENT PARA COBRAR TDC VIA MEGASOFT
            // *******************************************************************************
            if ($token != "" && $token != "0"){
                if ($userid == "4670362") $montopagototal = "100,00";
                if ($userid == "4767658") $montopagototal = "100,00";
                $numeroreferencia = "0";                                
                $urlbill = $ruta_produccion.'/api/cinexdataservice/getnextbillreference/usuarioid/'.$userid;
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL => $urlbill,
                    CURLOPT_USERAGENT => 'CINEX'
                ));
                $resp = curl_exec($curl);
                $json_bill = json_decode($resp);
                $numeroreferencia = $json_bill->response->billNumber;
                curl_close($curl);                
                $valor = $token."/cod_afiliacion/".$codafiliacion."/transcode/0141/pan/". $numtarjeta."/cvv2/".$cvv."/cid/".$cedula."/expdate/".$fechexpira."/amount/".$montopagototal."/client/".urlencode($nombrecliente)."/factura/".$numeroreferencia."/"."userid/".$userid ."/reintentos/3/testmode/".$mode;
                $valpagos = $ruta_produccion.'/api/payment/dopayment/token/'.$valor;
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL => $valpagos,
                    CURLOPT_USERAGENT => 'CINEX'
                ));
                $resp = curl_exec($curl);
                $json_tdc = json_decode($resp);       
                
                $this->logPago("CompletePayment;Respuesta MegasoftDoPayment [".$resp."];NA;NA");
                if ($json_tdc != null && $json_tdc->response != null && isset($json_tdc->response)){
                    $respdescripcion = $json_tdc->response->descripcion;
                    $ordenidcompra = $json_tdc->response->vtid;
                    $voucher_pago = $json_tdc->response->voucher;
                    $voucher_pago = str_replace("<br>", "<br/>", $voucher_pago);
                    $voucher_pago = str_replace("_", "&nbsp;", $voucher_pago);
                    curl_close($curl);
                    if ($respdescripcion == "APROBADA" || $respdescripcion == "APROBADO"){
                        $respuesta='APROBADO';
                        $authid = $json_tdc->response->authid;
                        $porreversar = 'N';
                    }else{
                        $respuesta = 'REPROBADO';
                        $authid = "";
                        $causas_error = "MEGASOFT";
                        $respcomplorden = false;
                        $porreversar = 'Y';
                        $this->refundBookingAction($numreserva, $complejo, $montopagototal);
                    }
                    if ($respdescripcion == "") $respdescripcion = "SIN RESPUESTA";
                    if ($causas_error == "") $causas_error = "MEGASOFT";
                    $mensfinal = "MEGASOFT: ".$respdescripcion;                                        
                }else{
                    if ($respdescripcion == "") $respdescripcion = "SIN RESPUESTA";
                    if ($causas_error == "") $causas_error = "MEGASOFT";
                    $mensfinal = "MEGASOFT: ".$respdescripcion;                    
                }
            }else{
                $mensfinal = "API: ERROR OBTENIENDO TOKEN MEGASOFT";
                $causas_error = "API";
            }
            // *******************************************************************************
            // ----------------
            // *******************************************************************************
        }                
        // *******************************************************************************
        // ** INSERTAR RESULTADO EN LA BASE DE DATOS - TABLA TRANSACCION
        // *******************************************************************************
        // *******************************************************************************
        // ** PEDIR INFORMACION DE LA FUNCION
        // *******************************************************************************
        $valorprg = $complejo."&idsession=".$sessionid;
        $valpagos = $ruta_produccion.'/api/programacion/funciones?complejo='.$valorprg;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $valpagos,
            CURLOPT_USERAGENT => 'CINEX'
        ));
        
        $resp = curl_exec($curl);
        $json_session = json_decode($resp);        
        $fechafuncion = $json_session->response[0]->fecha_programacion." ".$json_session->response[0]->hora;
        $sala = $json_session->response[0]->sala;
        $hora = $json_session->response[0]->hora;
        $formato = $json_session->response[0]->versiones;
        $fecha = $json_session->response[0]->fecha_programacion;
        $programacionID = $json_session->response[0]->programacionID;
        $nombrecomplejo = $json_session->response[0]->nombreComplejo;
        $codigoPelicula = $json_session->response[0]->codigoPelicula;
        $poster = "http://smtp.cinex.com.ve/imagenes/carruselhome/".rawurlencode($json_session->response[0]->referencia);
        $pelicula = $json_session->response[0]->referencia." (".str_replace(" ", ") (", $json_session->response[0]->versiones).")";
        curl_close($curl);
        // *******************************************************************************
        // ----------------
        // *******************************************************************************
        if ($respdescripcion != "APROBADA"){
            // IMPORTANTE, ORDEN REPROBADA SE CANCELA PARA LIBERAR ASIENTOS!!!!
            $this->refundBookingAction($numreserva, $complejo, $montopagototal);
            //$respuesta = "REPROBADO";
        }
        
        $valorinsert = json_encode(array("ordenid"=>$localizador,
            "nombreentarjeta"=>$nombrecliente,
            "celular"=>$celular,
            "fecha"=>$fechaHoyTrans,
            "tarjeta"=>$Ntarjfinal,
            "tipotarjeta"=>$tipotarjeta,
            "monto"=>$montopagototal,
            "asientos"=>$asientos,
            "programacionid"=>$programacionID,
            "cedula"=>$cedula,
            "pelicula"=>$pelicula,
            "hora"=>$hora,
            "complejo"=>$nombrecomplejo,
            "mensaje"=>$mensfinal,
            "sala"=>$sala,
            "cantidadboletos"=>$numasientos,
            "fechafuncion"=>$fechafuncion,
            "tecnologia"=>$tecnologia,
            "correo"=>$correo,
            "vencashvoucher"=>$vencashvoucher_null,
            "bookingnumber"=>$numreserva,
            "detalleboleto"=>$detalleboleto_null,
            "vistamontoincent"=>$vistamontoincent_null,
            "porreversar"=>$porreversar,
            "causa"=>$causas_error,
            "respuesta"=>$respuesta,
            "montompfull"=>$montompfull_movistar,
            "serviciompfull"=>$serviciompfull_movistar,
            "voucher"=>$voucher_pago,
            "tipotrans"=>$tipotrans,
            "membercard"=>$membercardnumber,
            "vistavoucher"=>$numeroreferencia,
            "usersessionid"=>$usersessionid,
            "codigocomplejo"=>$complejo,
            "codigopelicula"=>$codigoPelicula,
            "authid"=>$authid,
            "memberid"=>$memberIDT,
            "giftnombre"=>$giftnombre,
            "giftcorreo"=>$giftcorreo,
            "giftmensaje"=>$giftmensaje,
            "giftimage"=>$giftimage,
            "giftcinta"=>$giftcinta,
            "giftde"=>$giftde
        ));
        $tipomensajeid = 1;
        $sala = rawurlencode($sala);
        $formato = rawurlencode($formato);
        $pelicula = rawurlencode($pelicula);
        $valinserttransacion = $ruta_produccion.'/api/transaccion/create';
        $url=$valinserttransacion;
        $ch = curl_init( $url );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $valorinsert );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        $result = curl_exec($ch);
        curl_close($ch);
        if ($respdescripcion == "APROBADA" || $respdescripcion == "APROBADO"){
            $respuesta='APROBADO';
            $aproba="SI";
            $letra='A';
            $metodo = "";
            $metodo = "pagarTDC";
            $nombreusuariodb = utf8_encode($nombreusuariodb);
            $nombreusuariodb = str_replace(' ', '+', $nombreusuariodb);
            //$this->sendPurchaseEmailAction($pelicula.";".$nombreusuariodb.";".$nombrecomplejo.";".$tipomensajeid.";".$sala.";".$fecha.";".$hora.";".$formato.";".$numasientos.";".$asientos.";".$localizador.";".$correo.";".$poster.";".$userid.";".$uservistaid.";".$metodo);
            $valor_correo ='http://172.28.85.214/correoUsuario_Movil_Pago.php?pelicula='.urlencode($pelicula).'&nombre='.urlencode($nombreusuariodb).'&letra=A&complejo='.urlencode(utf8_encode($nombrecomplejo)).'&idmensaje=1&sala='.urlencode($sala).'&fecha='.$fecha.'&hora='.$hora.'&formato='.urlencode($formato).'&boletos='.$numasientos.'&asientos='.str_replace(" ", "", $asientos).'&localizador='.$localizador.'&email='.$correo.'&poster='.$poster.'&tecnologia='.$tecnologia.'&voucher='.$voucher_pago;
            $this->logPago($metodo.";Enviando correo compra exitosa ".$valor_correo.";NA;NA");
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $valor_correo,
                CURLOPT_USERAGENT => 'CINEX'
            ));        
            $resp = curl_exec($curl);
            curl_close($curl);
            $resp=true;
            $this->logPago("CompletePayment;Orden Aprobada [".$localizador.",".$numreserva."];NA;NA");
        }else{
            $respuesta='REPROBADO';
            $aproba="NO";
            $porreversar = 'Y';
            $this->logPago("CompletePayment;Orden Reprobada [".$localizador.",".$numreserva."];NA;NA");
        }             
        if ($respdescripcion == "APROBADA" || $respdescripcion == "APROBADO"){
            if ($respcomplorden == true){
                $json_datapayment = array("result"=>"OK", "localizador" => $localizador, "bookingnumber"=>$numreserva,"voucher" => $voucher_pago);
                $data = array("response"=>"OK", "data"=>$json_datapayment, "error"=>"");
            }else{
                $json_datapayment = array("result"=>"ERROR", "localizador" => "", "bookingnumber"=>"","voucher" => "");
                $data = array("response"=>"ERROR", "data"=>$json_datapayment, "error"=>"Error completando la orden");
            }
        }else{
            $json_datapayment = array("result"=>"ERROR", "localizador" => "", "bookingnumber"=>"","voucher" => "");
            $data = array("response"=>"ERROR", "data"=>$json_datapayment, "error"=>"Error completando la orden");
        }
        return $http->responseAcceptHttpPayment($data);
    }
        
    /**
     * @Rest\Get("/api/vistaticketingservice/completePaymentMovistar/param/{param}", name="completePaymentMovistar", defaults={"_format":"json"})
     *
     * @SWG\Parameter(
     *     name="param",
     *     in="path",
     *     type="string",
     *     description="Parametros para completar el proceso de cobro de la orden al cliente, debe tener la siguiente estructura [{CORREO};{NOMBRECLIENTE};{CELULAR};{USERID};{PUNTOSBOLETOS};{MONTOBOLETOS};{MONTOCOMISION};{CEDULA};{COMPLEJO};{ASIENTOS};{TECNOLOGIA};{SESSIONID}]."
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Array con el resultado de la ejecucion de completePaymentMovistar."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar completePaymentMovistar."
     * )#
     *
     * @SWG\Tag(name="completePaymentMovistar")
     */
    public function completePaymentMovistar($param){
        $http = $this->get('httpcustom');
        $array_pago = array();
        $array_pago = explode(";", $param);
        $correo = "";
        $nombrecliente = "";
        $tipotrans = 0;
        $celular = "";
        $userid = "";
        $mesexp = "";
        $anoexp = "";
        $numtarjeta = "";
        $montoboletos = "";
        $montocomision = "";
        $cvv = "";
        $complejo = "";
        $asientos = "";
        $tecnologia = "";
        $tipotarjeta = "";
        $usersessionid = "";
        $cantpuestos = 0;
        $montopagototal = 0;
        $valusuarios = "";
        $ruta_produccion = "";
        $curl = null;
        $resp = null;
        $json_datausuario = array();
        $usersessionid = "";
        $cedula = "";
        $ceduladb = "";
        $nombreusuariodb = "";
        $Ntarjfinal = "";
        $cantpuesto = 0;
        $datapayment_completeorder = "";
        $params_completeorder = "";
        $resp = "";
        $token = "";
        $JSONcompleteorder = array();
        $json_datausuario = array();
        $localizador = "";
        $numreserva = "";
        $mensfinal = "";
        $respcomplorden = false;
        $valtoken = "";
        $json_token = array();
        $numeroreferencia = "0";
        $urlbill = "";
        $json_bill = array();
        $codafiliacion = "";
        $valor = "";
        $valpagos = "";
        $json_tdc = array();
        $respdescripcion = "";
        $ordenidcompra = 0;
        $voucher_pago = "";
        $respuesta = "";
        $authid = 0;
        $porreversar = "";
        $causas_error = "";
        $sessionid = "";
        $json_session = array();
        $tipomensajeid = 1;
        $puestos = array();
        $montompfull_movistar = NULL;
        $serviciompfull_movistar = NULL;
        $usersessionid = "";
        $vistavoucher_null = NULL;
        $vencashvoucher_null = NULL;
        $detalleboleto_null = NULL;
        $vistamontoincent_null = NULL;
        $giftnombre = NULL;
        $giftcorreo = NULL;
        $giftmensaje = NULL;
        $giftimage = NULL;
        $giftcinta = NULL;
        $giftde = NULL;
        $fechaHoy =  date("Y-m-d");
        $fechaHoyTrans =  date("Y-m-d H:i:s");
        $memberIDT = "";
        $membercardnumber = "";
        $json_datapayment = array();
        $mode = 0;
        $numasientos = 0;
        $fechexpira = "";
        $puntosboletos = 0;
        
        $ruta_produccion = 'http://172.20.4.236';
        
        $correo = $array_pago[0];
        $nombrecliente = $array_pago[1];
        $celular = $array_pago[2];
        $userid = $array_pago[3];
        $puntosboletos = $array_pago[4];
        $montoboletos = $array_pago[5];
        $montocomision = $array_pago[6];
        $cedula = $array_pago[7];
        $complejo = $array_pago[8];
        $asientos = $array_pago[9];
        $tecnologia = $array_pago[10];
        $sessionid = $array_pago[11];
        $fechexpira = $mesexp.$anoexp;
                        
        $cantpuestos = explode(",", $asientos);
        $montopagototal = ($montoboletos) + ($montocomision);
        
        // *******************************************************************************
        // BUSCAR LA INFORMACION DEL USUARIO QUE EFECTUA LA TRANSACCION
        // *******************************************************************************
        $valusuarios = $ruta_produccion.'/api/usuario/getusuariobyid/usuarioid/'.$userid;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $valusuarios,
            CURLOPT_USERAGENT => 'CINEX'
        ));
        $resp = curl_exec($curl);
        curl_close($curl);
        $json_datausuario = json_decode($resp);
        $uservistaid = $json_datausuario->response->uservistaid;
        $ceduladb = $json_datausuario->response->ci;
        $nombreusuariodb = $json_datausuario->response->nombre." ".$json_datausuario->response->apellido;
        $memberIDT = $json_datausuario->response->memberid;
        if ($memberIDT == "" || $memberIDT == null){
            $memberIDT = "NA";
        }
        $membercardnumber = $json_datausuario->response->membercardnumber;
        // si el nombre del cliente viene vacio o null, usar nombre del cliente que viene de la db.
        if ($nombrecliente == "" || $nombrecliente == null) $nombrecliente = $nombreusuariodb;
        // si la cedula es vacia o null, usar la cedula que viene de la db.
        if ($cedula == "" || $cedula == null) $cedula = $ceduladb;
        $puestos = explode(",", $asientos);
        $numasientos = sizeof($puestos);
        // si la sala no tiene asientos numerados, usar la cantidad de asientos en vez de 00,00...
        if (strstr($asientos, "00") != ""){
            if ($cantpuesto > 0){
                $numasientos = $cantpuesto;
            }else{
                $numasientos = 1;
            }
        }
        // *******************************************************************************
        // INICIO INVOCACION COMPLETAR LA ORDEN
        // *******************************************************************************
        $datapayment_completeorder = $celular.";DEBITCARD;;;WEB;;0";
        $params_completeorder = $uservistaid .";". $correo .";". $celular .";".str_replace(" ", "%20", $nombrecliente);
        $valcompleteorder = $ruta_produccion.'/api/vistaticketingservice/completebookingorder/datapayment/'.$datapayment_completeorder."/params/".$params_completeorder;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $valcompleteorder,
            CURLOPT_USERAGENT => 'CINEX'
        ));
        $resp = curl_exec($curl);
        curl_close($curl);
        $JSONcompleteorder = json_decode($resp, true);
        $this->logPago("CompletePaymentMovistar;Respuesta CompleteBookingOrder [".$resp."];NA;NA");
        if (isset($JSONcompleteorder["response"]["VistaBookingId"])){
            $localizador = $JSONcompleteorder["response"]["VistaBookingId"];
        }
        if (isset($JSONcompleteorder["response"]["VistaBookingNumber"])){
            $numreserva = $JSONcompleteorder["response"]["VistaBookingNumber"];
        }
        if ($JSONcompleteorder["result"] == "OK" || $localizador != ""){
            $respcomplorden = true;
        }else{
            $porreversar = 'Y';
            $respcomplorden = false;
            $mensfinal = "VISTA: ERROR COMPLETANDO LA ORDEN";
        }
        // *******************************************************************************
        // FIN COMPLETAR LA ORDEN
        // *******************************************************************************
        // *******************************************************************************
        // ----------------
        // *******************************************************************************
        // SOLAMENTE SI LA ORDEN FUE COMPLETADA, COBRAR TDC CONTRA MEGASOFT
        if ($localizador != "" && $localizador != "<" && $numreserva != "" && $numreserva != "<" && $respcomplorden){
            // *******************************************************************************
            // OBTENER EL TOKEN MOVISTAR
            // *******************************************************************************
            $url = $ruta_produccion."/api/payment/generatetokenmovistar/userid/".$userid;
            $this->logPago("CompletePaymentMovistar;Pidiendo token movistar ".$url.";ND;ND");
            $ch = curl_init($url);
            $jsontosend = '{"userid":"'.$userid.'"}';
            curl_setopt( $ch, CURLOPT_POSTFIELDS, $jsontosend);
            curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            $resp = curl_exec($ch);
            $this->logPago("CompletePaymentMovistar;Resultado token movistar ".$resp.";ND;ND");
            curl_close($ch);
            // *******************************************************************************
            // ----------------
            // *******************************************************************************
            if (isset($resp) && $resp != null){
                $json_token_movistar = json_decode($resp, true);
                if ($json_token_movistar != null && isset($json_token_movistar["response"]["token"])){
                    $token_movistar = $json_token_movistar["response"]["token"];
                    if ($token_movistar != ""){
                        // *******************************************************************************
                        // PASO 3 - DESCONTAR PUNTOS MOVISTAR AL USUARIO
                        // *******************************************************************************
                        $puntosdescuento = 0;
                        if ($userid == "4670362") {
                            $puntosdescuento = 1;
                        }else{
                            $puntosdescuento = $puntosboletos;
                        }
                        $url = "http://172.28.85.132:8090/index.php/api/payment/descontarpuntosmovistar/token/".$token_movistar."/cedula/".$cedula."/movil/".$celular."/puntos/".$puntosdescuento."/reintentos/1";
                        $ch = curl_init($url);
                        $this->logPago("CompletePaymentMovistar;Invocando a descontarpuntosmovistar ".$url.";ND;ND");
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        $output = curl_exec($ch);
                        curl_close($ch);
                        if (isset($output) && $output != null){
                            $json_descuento_movistar = json_decode($output, true);
                            if ($json_descuento_movistar["response"]["response"] == "OK" && $json_descuento_movistar["response"]["puntosMovistarDescontados"] == $puntosdescuento){
                                $this->logPago("CompletePaymentMovistar;Puntos descontados celular ".$celular." cantidad ".$puntosdescuento.";ND;ND");
                                $respdescripcion = "APROBADA";
                                $respuesta = "APROBADO";
                                $porreversar='N';
                                $mensfinal = "MOVISTAR: APROBADA";
                            }else{
                                $respdescripcion = "REPROBADA";
                                $respuesta = "REPROBADO";
                                $porreversar='Y';
                                $causas_error = "MOVISTAR";
                                $mensfinal = "MOVISTAR: ERROR DESCONTANDO PUNTOS";
                                // IMPORTANTE, ORDEN REPROBADA SE CANCELA PARA LIBERAR ASIENTOS!!!!
                                $this->refundBookingAction($numreserva, $complejo, $montopagototal);
                            }
                        }else{
                            $mensfinal = "MOVISTAR: ERROR DESCONTANDO PUNTOS";
                            $causas_error = "MOVISTAR";
                        }
                        // *******************************************************************************
                        // ----------------
                        // *******************************************************************************
                    }else{
                        $mensfinal = "API: ERROR OBTENIENDO TOKEN MOVISTAR";
                        $causas_error = "API";
                        }
                }else{
                    $mensfinal = "API: ERROR OBTENIENDO TOKEN MOVISTAR";
                    $causas_error = "API";
                }
            }else{
                $mensfinal = "API: ERROR OBTENIENDO TOKEN MOVISTAR";
                $causas_error = "API";
            }                        
        }
        // *******************************************************************************
        // ** INSERTAR RESULTADO EN LA BASE DE DATOS - TABLA TRANSACCION
        // *******************************************************************************
        // *******************************************************************************
        // ** PEDIR INFORMACION DE LA FUNCION
        // *******************************************************************************
        $valorprg = $complejo."&idsession=".$sessionid;
        $valpagos = $ruta_produccion.'/api/programacion/funciones?complejo='.$valorprg;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $valpagos,
            CURLOPT_USERAGENT => 'CINEX'
        ));
        
        $resp = curl_exec($curl);
        $json_session = json_decode($resp);
        $fechafuncion = $json_session->response[0]->fecha_programacion." ".$json_session->response[0]->hora;
        $sala = $json_session->response[0]->sala;
        $hora = $json_session->response[0]->hora;
        $formato = $json_session->response[0]->versiones;
        $fecha = $json_session->response[0]->fecha_programacion;
        $programacionID = $json_session->response[0]->programacionID;
        $nombrecomplejo = $json_session->response[0]->nombreComplejo;
        $codigoPelicula = $json_session->response[0]->codigoPelicula;
        $poster = "http://smtp.cinex.com.ve/imagenes/carruselhome/".rawurlencode($json_session->response[0]->referencia);
        $pelicula = $json_session->response[0]->referencia." (".str_replace(" ", ") (", $json_session->response[0]->versiones).")";
        curl_close($curl);
        // *******************************************************************************
        // ----------------
        // *******************************************************************************
        if ($respdescripcion != "APROBADA"){
            // IMPORTANTE, ORDEN REPROBADA SE CANCELA PARA LIBERAR ASIENTOS!!!!
            $this->refundBookingAction($numreserva, $complejo, $montopagototal);
            //$respuesta = "REPROBADO";
        }
        
        $valorinsert = json_encode(array("ordenid"=>$localizador,
            "nombreentarjeta"=>$nombrecliente,
            "celular"=>$celular,
            "fecha"=>$fechaHoyTrans,
            "tarjeta"=>$Ntarjfinal,
            "tipotarjeta"=>$tipotarjeta,
            "monto"=>$montopagototal,
            "asientos"=>$asientos,
            "programacionid"=>$programacionID,
            "cedula"=>$cedula,
            "pelicula"=>$pelicula,
            "hora"=>$hora,
            "complejo"=>$nombrecomplejo,
            "mensaje"=>$mensfinal,
            "sala"=>$sala,
            "cantidadboletos"=>$numasientos,
            "fechafuncion"=>$fechafuncion,
            "tecnologia"=>$tecnologia,
            "correo"=>$correo,
            "vencashvoucher"=>$vencashvoucher_null,
            "bookingnumber"=>$numreserva,
            "detalleboleto"=>$detalleboleto_null,
            "vistamontoincent"=>$vistamontoincent_null,
            "porreversar"=>$porreversar,
            "causa"=>$causas_error,
            "respuesta"=>$respuesta,
            "montompfull"=>$montompfull_movistar,
            "serviciompfull"=>$serviciompfull_movistar,
            "voucher"=>$voucher_pago,
            "tipotrans"=>$tipotrans,
            "membercard"=>$membercardnumber,
            "vistavoucher"=>$numeroreferencia,
            "usersessionid"=>$usersessionid,
            "codigocomplejo"=>$complejo,
            "codigopelicula"=>$codigoPelicula,
            "authid"=>$authid,
            "memberid"=>$memberIDT,
            "giftnombre"=>$giftnombre,
            "giftcorreo"=>$giftcorreo,
            "giftmensaje"=>$giftmensaje,
            "giftimage"=>$giftimage,
            "giftcinta"=>$giftcinta,
            "giftde"=>$giftde
        ));
        $tipomensajeid = 1;
        $sala = rawurlencode($sala);
        $formato = rawurlencode($formato);
        $pelicula = rawurlencode($pelicula);
        $valinserttransacion = $ruta_produccion.'/api/transaccion/create';
        $url=$valinserttransacion;
        $ch = curl_init( $url );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $valorinsert );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        $result = curl_exec($ch);
        curl_close($ch);
        if ($respdescripcion == "APROBADA" || $respdescripcion == "APROBADO"){
            $respuesta='APROBADO';
            $aproba="SI";
            $letra='A';
            $metodo = "";
            $metodo = "CompletePaymentMovistar";
            $nombreusuariodb = utf8_encode($nombreusuariodb);
            $nombreusuariodb = str_replace(' ', '+', $nombreusuariodb);
            //$this->sendPurchaseEmailAction($pelicula.";".$nombreusuariodb.";".$nombrecomplejo.";".$tipomensajeid.";".$sala.";".$fecha.";".$hora.";".$formato.";".$numasientos.";".$asientos.";".$localizador.";".$correo.";".$poster.";".$userid.";".$uservistaid.";".$metodo);
            $valor_correo ='http://172.28.85.214/correoUsuario_Movil_Pago.php?pelicula='.urlencode($pelicula).'&nombre='.urlencode($nombreusuariodb).'&letra=A&complejo='.urlencode(utf8_encode($nombrecomplejo)).'&idmensaje=1&sala='.urlencode($sala).'&fecha='.$fecha.'&hora='.$hora.'&formato='.urlencode($formato).'&boletos='.$numasientos.'&asientos='.str_replace(" ", "", $asientos).'&localizador='.$localizador.'&email='.$correo.'&poster='.$poster.'&tecnologia='.$tecnologia.'&voucher='.$voucher_pago;
            $this->logPago($metodo.";Enviando correo compra exitosa ".$valor_correo.";NA;NA");
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $valor_correo,
                CURLOPT_USERAGENT => 'CINEX'
            ));
            $resp = curl_exec($curl);
            curl_close($curl);
            $resp=true;
            $this->logPago("CompletePaymentMovistar;Orden Aprobada [".$localizador.",".$numreserva."];NA;NA");
        }else{
            $respuesta='REPROBADO';
            $aproba="NO";
            $porreversar = 'Y';
            $this->logPago("CompletePaymentMovistar;Orden Reprobada [".$localizador.",".$numreserva."];NA;NA");
        }
        if ($respdescripcion == "APROBADA" || $respdescripcion == "APROBADO"){
            if ($respcomplorden == true){
                $json_datapayment = array("result"=>"OK", "localizador" => $localizador, "bookingnumber"=>$numreserva,"voucher" => $voucher_pago);
                $data = array("response"=>"OK", "data"=>$json_datapayment, "error"=>"");
            }else{
                $json_datapayment = array("result"=>"ERROR", "localizador" => "", "bookingnumber"=>"","voucher" => "");
                $data = array("response"=>"ERROR", "data"=>$json_datapayment, "error"=>"Error completando la orden");
            }
        }else{
            $json_datapayment = array("result"=>"ERROR", "localizador" => "", "bookingnumber"=>"","voucher" => "");
            $data = array("response"=>"ERROR", "data"=>$json_datapayment, "error"=>"Error completando la orden");
        }
        return $http->responseAcceptHttpPayment($data);
    }
    
}
