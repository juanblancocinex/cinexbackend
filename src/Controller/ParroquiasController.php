<?php


namespace App\Controller;
use App\CoreCinex\CinexService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use App\Lib\DaoLib;
use App\Lib\ApiUtilFunctions;
use App\Lib\Constantes;


class ParroquiasController extends Controller
{



    // Parroquia por Municipio URI"'s
    /**
     * @Rest\Get("/api/municipio/parroquia", name="getparroquiapormunicipio", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="municipioid",
     *     in="query",
     *     type="string",
     *     description="Codigo de Municipio"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Parroquias."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Parroquia por Municipio"
     * )
     *
     * @SWG\Tag(name="ParroquiaPorMunicipioID")
     */   
    
    public function getParroquiaPorMunicipioId(Request $request) {
        $http = $this->get('httpcustom');
        $municipioId= $request->query->get('municipioid');
        $em = $this->getDoctrine()->getManager();
        $data= [];
        $data = $em->getRepository("App:Parroquias")->getParroquiasByIdMunicipio($municipioId);
        if (is_null($data)) {
            $data = [];
        }   
        return $http->responseAcceptHttpWithoutFormat($data);
    }


     

}
