<?php
namespace App\Controller;
use App\CoreCinex\CinexService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use App\Lib\DaoLib;
use App\Lib\ApiUtilFunctions;
use App\Lib\Constantes;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Translation\TranslatorInterface;
use App\Lib\XmlLib;
use App\Entity\TokenTrans;
use App\Entity\Usuario;
use App\Entity\TransactionNumber;

class PaymentProcessController extends Controller
{     
        protected $params;

        /**
         * Constructor.
        **/
        public function __construct(ParameterBagInterface $params)
        {
            $this->xmlLib= new XmlLib();
            $this->params=$params;
        }            
        
        /**
         * @Rest\Get("/api/paymentprocess/getLogData/fecha/{fecha}/desde/{desde}", name="getLogData", defaults={"_format":"json"})
         *
         * @SWG\Parameter(
         *     name="fecha",
         *     in="path",
         *     type="string",
         *     description="fecha del log a consultar ejemplo (20181011)."
         * )
         *
         * @SWG\Parameter(
         *     name="desde",
         *     in="path",
         *     type="integer",
         *     description="cantidad de lineas a recibir."
         * )
         * @SWG\Response(
         *     response=200,
         *     description="Array con la respuesta del log de pago."
         * )
         * @SWG\Response(
         *     response=500,
         *     description="Ocurrio un error al ejecutar getLogData"
         * )#
         *
         * @SWG\Tag(name="getLogDataAction")
         */
        
        public function getLogDataAction($fecha, $desde){
            $http = $this->get('httpcustom');
            $logpath = "http://172.28.85.214/log/logpago".str_replace("-", "",$fecha).".log";
            $response = array();
            $data_log = array();
            $tmpdata = array();
            $fh = null;
            $x = 0;
            $inicio = 0;
            
//             set_time_limit(300);
//             ini_set('memory_limit', '-1');
//             $arr = file($logpath, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
//             $c = ( false === $arr) ? 0 : count($arr);
//             set_time_limit(30);// restore to default
//             ini_set('memory_limit','128M');// restore to default
//             $inicio = $c - $desde;

//            /* $inicio = $c - 500;*/
//             if ($fh = fopen($logpath, 'r')) {
//                 while (!feof($fh)) {
//                     $line = fgets($fh);
//                     $x++;
//                     if ($x > $inicio){
//                         $line = str_replace("] [", ",", $line);
//                         $line = str_replace("[", "", $line);
//                         $line = str_replace("]", "", $line);
//                         $line = str_replace("IP", "", $line);
//                         $line = str_replace("METODO", "", $line);
//                         $line = str_replace("DESCRIPCION", "", $line);
//                         $line = str_replace("DESCRCION", "", $line);
//                         $line = str_replace("USUARIOID", "", $line);
//                         $line = str_replace("VISTAID", "", $line);
//                         $line = str_replace("\n", "", $line);
//                         $line = str_replace(" : ", "", $line);
//                         $line = str_replace(" USERNA", "", $line);
//                         $line = str_replace("USER", "", $line);
//                         $tmpdata = explode(",", $line);
//                         $hora = array();
//                         $hora = explode(" ", $tmpdata[0]);                        
//                         $data_log[] = array("hora" =>$tmpdata[0], "metodo" => $tmpdata[2]);
//                     }
//                 }
//                 fclose($fh);
//             }  
            $input = "";
            $rev_input = array();
            $line = "";
            $input = file($logpath);
            $rev_input = array_reverse($input);
            foreach ($rev_input as $line){
                $data_log[] = array("data" =>$line);
                if ($x == $desde) break;
                $x++;
            }
            
            $response = array("codRespuesta"=>"0", "desRespuesta"=>"Log obtenido exitosamente", "data" => $data_log, "method" => "getLogData");
            return $http->responsePaymentAcceptHttp($response);
        }
        
        /**
         * @Rest\Get("/api/paymentprocess/iniciarsesion/email/{email}/pwd/{pwd}", name="iniciarSesion", defaults={"_format":"json"})
         *
         * @SWG\Parameter(
         *     name="email",
         *     in="path",
         *     type="string",
         *     description="correo electronico del usuario."
         * )
         * @SWG\Parameter(
         *     name="pwd",
         *     in="path",
         *     type="string",
         *     description="contraseña del usuario."
         * )
         
         * @SWG\Response(
         *     response=200,
         *     description="Array con la respuesta del inicio de sesion del usuario"
         * )
         * @SWG\Response(
         *     response=500,
         *     description="Ocurrio un error al ejecutar iniciarsesion"
         * )#
         *
         * @SWG\Tag(name="IniciarSesion")
         */
        
        public function iniciarSesionAction($email, $pwd){
            $http = $this->get('httpcustom');
            $existe = 0;
            $userid = 0;
            $pwdgen = "";
            $response = array();
            if ($email != "" && $pwd != ""){
                $pwdgen = sha1($pwd);
                $em = $this->getDoctrine()->getManager();
                $data = $em->getRepository("App:Usuario")->getUsuarioFindByCorreoClave([
                    "correoelectronico" => $email,
                    "clave"=>$pwdgen
                ]);
                if (count($data)==0) {
                    $message = "Estimado usuario, los datos registrados no son correctos, por favor verifique el correo proporcionado y tenga en cuenta que la clave diferencia mayusculas de minusculas";
                    $response = array("codRespuesta" => "-2", "desRespuesta"=> $message, "usuario" => $userid, "method" => "iniciarSesion");
                }else{
                    $message = "Proceso finalizado exitosamente";
                    $userid = $data[0]["usuarioid"];
                    $response = array("codRespuesta" => "0", "desRespuesta"=> "OK", "usuario" => $userid, "method" => "iniciarSesion");
                }
            }else{
                $response = array("codRespuesta"=>"-2", "desRespuesta"=>"Ingrese usuario y clave", "usuario" => $userid, "method" => "iniciarSesion");
            }
            return $http->responsePaymentAcceptHttp($response);
        }
        
        /**
         * @Rest\Get("/api/paymentprocess/cambiarclave/pwdant/{pwdant}/newpassword/{newpassword}/usuarioID/{usuarioID}", name="cambiarClave", defaults={"_format":"json"})
         *
         * @SWG\Parameter(
         *     name="pwdant",
         *     in="path",
         *     type="string",
         *     description="Contraseña anterior del usuario."
         * )
         * @SWG\Parameter(
         *     name="newpassword",
         *     in="path",
         *     type="string",
         *     description="Nueva contraseña del usuario."
         * )
         * @SWG\Parameter(
         *     name="usuarioID",
         *     in="path",
         *     type="integer",
         *     description="Identificador del usuario."
         * )
         
         * @SWG\Response(
         *     response=200,
         *     description="Array con la respuesta del cambio de clave del usuario"
         * )
         * @SWG\Response(
         *     response=500,
         *     description="Ocurrio un error al ejecutar cambiarclave"
         * )#
         *
         * @SWG\Tag(name="cambiarClave")
         */
        
        public function cambiarClaveAction($pwdant, $newpassword, $usuarioID){
            $http = $this->get('httpcustom');
            if(isset($pwdant) && isset($newpassword)){
                $pwd = sha1($pwdant);
                $pwdnew1 = sha1($newpassword);
                $em = $this->getDoctrine()->getManager();
                $datauser = $em->getRepository("App:Usuario")->getUsuarioFindByUsuarioid($usuarioID);
                $datauser[0]->setClave($pwdnew1);                
                $response2 = $this->forward('App\Controller\UsuarioController::updateUserAction', $datauser);               
                $response = array("codRespuesta"=>"0", "desRespuesta"=>"Su cambio de clave ha sido ejecutado exitosamente.", "method" => "cambiarClave");
                
             /*   $sql="UPDATE usuario SET clave='".$pwdnew1."', ultimaModificacion = CURDATE() WHERE usuarioID=".$usuarioID." and  clave ='".$pwd."' ";
                $gsent = $this->dbh->prepare($sql);
                $gsent->execute();
                $resp=$gsent->rowCount();
                //$gsent->close();
                if ($resp >= 1) {
                    $response = array("codRespuesta" => "0","desRespuesta" => "Su cambio de clave ha sido ejecutado exitosamente.");
                }else{
                    $response = array("codRespuesta" => "1","desRespuesta"=>"Estimado usuario, los datos suministrado no son correctos, por favor verifique la Clave actual proporcionado y tenga en cuenta que la clave diferencia mayusculas de minusculas");
                }
                */
            }else{
                $response = array("codRespuesta"=>"-2", "desRespuesta"=>"Ingrese clave anterior y clave nueva", "method" => "cambiarClave");
            }
            return $http->responsePaymentAcceptHttp($response);
        }
        
        /**
         * @Rest\Get("/api/paymentprocess/registrarUsuario", name="registrarUsuario", defaults={"_format":"json"})
         *         
         * @SWG\Response(
         *     response=200,
         *     description="Array con la respuesta del valor del boleto adulto y comision"
         * )
         * @SWG\Response(
         *     response=500,
         *     description="Ocurrio un error al ejecutar registrarUsuario"
         * )#
         *
         * @SWG\Tag(name="registrarUsuario")
         */
        
        public function registrarUsuarioAction(){
            $http = $this->get('httpcustom');
            $response = array("codRespuesta" => "0","desRespuesta" => "error", "method"=>"registrarUsuario");   
            return $http->responsePaymentAcceptHttp($response);           
        }
                     
        
        /**
         * @Rest\Get("/api/paymentprocess/Loyalty_registros_usuarios/cardnumber/{cardnumber}/UserVistaID/{UserVistaID}", name="Loyalty_registros_usuarios", defaults={"_format":"json"})
         *
         * @SWG\Parameter(
         *     name="cardnumber",
         *     in="path",
         *     type="string",
         *     description="numero de tarjeta loyalty."
         * )
         * @SWG\Parameter(
         *     name="UserVistaID",
         *     in="path",
         *     type="string",
         *     description="identificador del usuario en vista."
         * )         
         * @SWG\Response(
         *     response=200,
         *     description="Array con la respuesta de Loyalty_registros_usuarios"
         * )
         * @SWG\Response(
         *     response=500,
         *     description="Ocurrio un error al ejecutar Loyalty_registros_usuarios"
         * )
         *
         * @SWG\Tag(name="Loyalty_registros_usuarios")
         */        
        public function Loyalty_registros_usuariosAction($cardnumber,$UserVistaID){
            $http = $this->get('httpcustom');
            $json_member = array();
            $json_updatemember = "";
            $json_activate = array();
            $memberId = "";
            $ActivationCode = "";
            $url = "http://172.20.4.236/index.php/api/loyalty/getmember/membercardnumber/".$cardnumber;
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            $response = curl_exec($ch);
            $json_member = json_decode($response);
            curl_close($ch);
            if ($json_member->response->response == "OK"){
                $memberId = $json_member->response->memberId;
                if ($memberId != ""){                    
                    $json_updatemember = "memberid:".$memberId.", cardnumber:".$cardnumber;                                                            
                    $ch = curl_init("http://172.20.4.236/index.php/api/loyalty/updatemember");
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/json'));
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $json_updatemember);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    $output = curl_exec($ch);    
                                        
                    $url = "http://172.20.4.236/index.php/api/loyalty/getwebaccountactivationcode/".$memberId;
                    $ch = curl_init($url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                    $response = curl_exec($ch);
                    $json_activate = json_decode($response);
                    curl_close($ch);
                    if ($json_activate->response->response == "OK"){
                        $ActivationCode = $json_activate->response->ActivationCode;
                        
                    }
                    
                    $url = "http://172.20.4.236/index.php/api/loyalty/activatewebaccount/memberid/".$memberId."/activationcode/".$ActivationCode;
                    $ch = curl_init($url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                    $response = curl_exec($ch);
                    $json_activate = json_decode($response);
                    curl_close($ch);
                    if ($json_activate->response->response == "OK"){
                        $ActivationCode = $json_activate->response->ActivationCode;                       
                    }
                    
                    $json_membershipactivated = "memberid".$memberId.", cardnumber=".$cardnumber.", usersessionid:".$UserVistaID;
                    $ch = curl_init("http://172.20.4.236/index.php/api/loyalty/membershipactivated");
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/json'));
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, "$json_membershipactivated");
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    $output = curl_exec($ch);                    
                    
                    $url = "http://172.20.4.236/index.php/api/loyalty/validatemember/usersessionid/".$UserVistaID."/cardnumber/".$cardnumber;
                    $ch = curl_init($url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                    $response = curl_exec($ch);
                    $json_activate = json_decode($response);
                    curl_close($ch);
                                        
                }                
            }           
            $response=array("codRespuesta" => "0","desRespuesta" => "Proceso realizado exitosamente.", "method"=>"Loyalty_registros_usuarios");
            return $http->responsePaymentAcceptHttp($response);
            
        }
                
        /**
         * @Rest\Get("/api/paymentprocess/Loyalty_usuarios_membresia/memberid/{memberid}/usersessionid/{usersessionid}/cardnumber/{cardnumber}", name="Loyalty_usuarios_membresia", defaults={"_format":"json"})
         *
         * @SWG\Parameter(
         *     name="memberid",
         *     in="path",
         *     type="string",
         *     description="identificador del miembro."
         * )
         * @SWG\Parameter(
         *     name="usersessionid",
         *     in="path",
         *     type="string",
         *     description="identificador del usuario en vista."
         * )
         * @SWG\Parameter(
         *     name="cardnumber",
         *     in="path",
         *     type="string",
         *     description="numero de tarjeta."
         * )         
         * @SWG\Response(
         *     response=200,
         *     description="Array con la respuesta con los items de membresia del usuario."
         * )
         * @SWG\Response(
         *     response=500,
         *     description="Ocurrio un error al ejecutar Loyalty_usuarios_membresia"
         * )#
         *
         * @SWG\Tag(name="Loyalty_usuarios_membresia")
         */
        public function Loyalty_usuarios_membresiaAction($memberid,$usersessionid,$cardnumber){
            $http = $this->get('httpcustom');
            $response = array();
            $json_loyalty = array();
            $ch = null;
            $url = "http://172.20.4.236/index.php/api/loyalty/getMemberItemList/".$usersessionid;
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            $response = curl_exec($ch);
            if ($response != "") {
                $json_loyalty = json_decode($response, true);
                if ($json_loyalty["result"] == "OK"){
                    for ($x = 0; $x < sizeof($json_loyalty["response"]["node"]); $x++){
                        $membresia[]=array("Name"=>$json_loyalty["response"]["node"][$x]["Name"],"QtyAvailable"=>$json_loyalty["response"]["node"][$x]["QtyAvailable"],"LatestDateAvail"=>$json_loyalty["response"]["node"][$x]["LatestDateAvail"]);                        
                    }
                }
            }
            $response=array("codRespuesta" => "0","desRespuesta" => "lista de beneficios obtenida exitosamete","items"=>$membresia, "method"=>"Loyalty_usuarios_membresia");
            return $http->responsePaymentAcceptHttp($response);            
        }
                
        /**
         * @Rest\Get("/api/paymentprocess/enviarCorreo/pelicula/{pelicula}/nombreusuariodb/{nombreusuariodb}/nombrecomplejo/{nombrecomplejo}/tipomensajeid/{tipomensajeid}/sala/{sala}/hora/{hora}/formato/{formato}/cantboletos/{cantboletos}/asientos/{asientos}/localizador/{localizador}/correo/{correo}/poster/{poster}/userid/{userid}/uservistaid/{uservistaid}/metodo/{metodo}", name="enviarCorreo", defaults={"_format":"json"})
         * @SWG\Parameter(
         *     name="pelicula",
         *     in="path",
         *     type="string",
         *     description="nombre de la pelicula."
         * )
         * @SWG\Parameter(
         *     name="nombreusuariodb",
         *     in="path",
         *     type="string",
         *     description="nombre del usuario."
         * )
         * @SWG\Parameter(
         *     name="nombrecomplejo",
         *     in="path",
         *     type="string",
         *     description="nombre del complejo."
         * )
         * @SWG\Parameter(
         *     name="tipomensajeid",
         *     in="path",
         *     type="string",
         *     description="identificador del mensaje en la tabla plantilla."
         * )
         * @SWG\Parameter(
         *     name="sala",
         *     in="path",
         *     type="string",
         *     description="nombre de la sala."
         * )
         * @SWG\Parameter(
         *     name="hora",
         *     in="path",
         *     type="string",
         *     description="hora de la funcion."
         * )
         * @SWG\Parameter(
         *     name="hora",
         *     in="path",
         *     type="string",
         *     description="hora de la funcion."
         * ) 
         * @SWG\Parameter(
         *     name="formato",
         *     in="path",
         *     type="string",
         *     description="formato de la pelicula."
         * )
         * @SWG\Parameter(
         *     name="cantboletos",
         *     in="path",
         *     type="integer",
         *     description="formato de la pelicula."
         * )
         * @SWG\Parameter(
         *     name="asientos",
         *     in="path",
         *     type="string",
         *     description="asientos."
         * )
         * @SWG\Parameter(
         *     name="localizador",
         *     in="path",
         *     type="string",
         *     description="localizador."
         * )
         * @SWG\Parameter(
         *     name="correo",
         *     in="path",
         *     type="string",
         *     description="correo."
         * )
         * @SWG\Parameter(
         *     name="poster",
         *     in="path",
         *     type="string",
         *     description="poster."
         * )
         * @SWG\Parameter(
         *     name="userid",
         *     in="path",
         *     type="integer",
         *     description="identificador de usuario."
         * )
         * @SWG\Parameter(
         *     name="uservistaid",
         *     in="path",
         *     type="string",
         *     description="identificador de usuario en vista."
         * )
         * @SWG\Parameter(
         *     name="metodo",
         *     in="path",
         *     type="string",
         *     description="nombre del metodo."
         * )         
         * @SWG\Response(
         *     response=200,
         *     description="Array con la respuesta del proceso de enviar correo"
         * )
         * @SWG\Response(
         *     response=500,
         *     description="Ocurrio un error al ejecutar enviar correo."
         * )#
         *
         * @SWG\Tag(name="enviarCorreo")
         */        
        public function enviarCorreoAction($pelicula, $nombreusuariodb, $nombrecomplejo, $tipomensajeid, $sala, $fecha, $hora, $formato, $cantboletos, $asientos, $localizador, $correo, $poster, $userid, $uservistaid, $metodo){
            $http = $this->get('httpcustom');
            $valor_correo = "";
            $curl = null;
            $resp = null;
            $valor_correo ='http://172.28.85.214/correoUsuario_Movil_Pago.php?pelicula='.urlencode($pelicula).'&nombre='.urlencode($nombreusuariodb).'&letra='.$letra.'&complejo='.urlencode(utf8_encode($nombrecomplejo)).'&idmensaje='.$tipomensajeid.'&sala='.urlencode($sala).'&fecha='.$fecha.'&hora='.$hora.'&formato='.urlencode($formato).'&boletos='.$cantboletos.'&asientos='.str_replace(" ", "", $asientos).'&localizador='.$localizador .'&email='.$correo.'&poster='.$poster;
            logPago($metodo.";Enviando correo compra exitosa ".$valor_correo.";".$userid.";".$uservistaid);
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $valor_correo,
                CURLOPT_USERAGENT => 'CINEX'
            ));
            $resp = curl_exec($curl);
            curl_close($curl);
            $response = array("codRespuesta" => "0","desRespuesta" => "correo enviado", "method"=>"enviarCorreo");
            return $http->responsePaymentAcceptHttp($response);
        }
                       
        /**
         * @Rest\Get("/api/paymentprocess/obtenerProximosEstrenos/", name="obtenerProximosEstrenos", defaults={"_format":"json"})

         * @SWG\Response(
         *     response=200,
         *     description="Array con la respuesta del proceso de registrar usuario."
         * )
         * @SWG\Response(
         *     response=500,
         *     description="Ocurrio un error al ejecutar registro de usuario."
         * )#
         *
         * @SWG\Tag(name="obtenerProximosEstrenos")
         */
        public function obtenerProximosEstrenosAction(){
            $http = $this->get('httpcustom');
            $ruta_produccion = 'http://172.28.85.132:8090';
            $json_content3 = array();
            $reference = array();
            $peliculasfinal = array();

            $arrayMeses= array(1=>"ENERO",2=>"FEBRERO",3=>"MARZO",4=>"ABRIL",5=>"MAYO",6=>"JUNIO",7=>"JULIO",8=>"AGOSTO",9=>"SEPTIEMBRE",10=>"OCTUBRE",11=>"NOVIEMBRE",12=>"DICIEMBRE");
            $mesesactual='';
            $mesesanterior='';
            
            $reference = $this->forward('App\Controller\CinexDataServiceController::getCommingSoonDataAction', array());
            if ($reference != ""){
                $dd2=$reference->getContent();
                $json_content3 = (array)json_decode($dd2);
            }

            for ($x = 0; $x < sizeof($json_content3["response"]); $x++){
                $peliculas =array();
                if ($mesesanterior==''){
                    $mesesanterior=$json_content3["response"][$x]->meses;
                }else{
                    if ($mesesanterior!=$json_content3["response"][$x]->meses){
                        $descrip=$arrayMeses[$mesesanterior].' '.$json_content3["response"][$x]->ano;
                        $peliculasfinal[]=array("peliculas"=>$peliculas,"descripcion"=>$descrip);
                        $peliculas =array();
                        $mesesanterior=$json_content3["response"][$x]->meses;
                    }else{
                        //$mesesactual=$data[$clave]->meses;
                    }
                }     
                $peliculas[]=array("duracion"=>$json_content3["response"][$x]->duracion,"urlTrailer"=>$json_content3["response"][$x]->urlTrailer,
                    "nombre"=>$json_content3["response"][$x]->nombreSinFormato,"urlCarruselHome"=>'',
                    'versiones'=>array("versiones"=>$json_content3["response"][$x]->versiones),
                    "sinopsis"=>$json_content3["response"][$x]->descripcion,"censura"=>$json_content3["response"][$x]->censura,"urlSliderMini"=>'',
                    'versionesListado'=>array("versiones"=>$json_content3["response"][$x]->versiones),
                    "urlCarrusel"=>(isset($json_content3["response"][$x]->urlPoster4)) ? $json_content3["response"][$x]->urlPoster4 : null,"urlSinopsis"=>(isset($json_content3["response"][$x]->urlPoster2)) ? $json_content3["response"][$x]->urlPoster2 : null,
                    "sinopsisResumen"=>$json_content3["response"][$x]->contenido,"director"=>$json_content3["response"][$x]->director,
                    "esPreVenta"=>(isset($json_content3["response"][$x]->esPreVenta) &&  $json_content3["response"][$x]->esPreVenta=="Y") ? true : false,"urlDestacado"=>(isset($json_content3["response"][$x]->urlPoster3)) ? $json_content3["response"][$x]->urlPoster3 : null,
                    "referencia"=>$json_content3["response"][$x]->referencia,"genero"=>$json_content3["response"][$x]->genero,
                    "esVenezolana"=>(isset($json_content3["response"][$x]->esVenezolana) && $json_content3["response"][$x]->esVenezolana=="Y") ? true : false,
                    'lenguajesListado'=>array("versiones"=>$json_content3["response"][$x]->versiones),
                    "esDestacada"=>(isset($json_content3["response"][$x]->esDestacada) && $json_content3["response"][$x]->esDestacada=="Y") ? true : false,
                    "nombreOriginal"=>$json_content3["response"][$x]->nombreOriginal,
                    "fechaEstreno"=>date("d/m/Y",strtotime( $json_content3["response"][$x]->fechaEstreno)),
                    "semanasCartelera"=>date("d/m/Y",strtotime($json_content3["response"][$x]->fechaEstreno)),
                    "urlCartelera"=>(isset($json_content3["response"][$x]->urlPoster1)) ? $json_content3["response"][$x]->urlPoster1 : null,"elenco"=>$json_content3["response"][$x]->elenco
                );
                $descrip=$arrayMeses[$mesesanterior].' '.$json_content3["response"][$x]->ano;
                $peliculasfinal[]=array("peliculas"=>$peliculas,"descripcion"=>$descrip);                
            }   
            $response = array("codRespuesta"=>0,"desRespuesta"=>"OK","meses"=>$peliculasfinal, "method"=>"obtenerProximosEstrenos");
            return $http->responsePaymentAcceptHttp($response);            
        }
        
        /**
         * @Rest\Get("/api/paymentprocess/obtenerListadoComplejos/", name="obtenerListadoComplejos", defaults={"_format":"json"})
         * @SWG\Response(
         *     response=200,
         *     description="Array con la respuesta del proceso de registrar usuario."
         * )
         * @SWG\Response(
         *     response=500,
         *     description="Ocurrio un error al ejecutar registro de usuario."
         * )#
         *
         * @SWG\Tag(name="obtenerListadoComplejos")
         */
        public function obtenerListadoComplejosAction(){
            $json_complejo = array();
            $http = $this->get('httpcustom');
            $ruta_produccion = 'http://172.28.85.132:8090';
            $valcomplejo = $ruta_produccion.'/api/complejos/';
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $valcomplejo,
                CURLOPT_USERAGENT => 'CINEX'
            ));
            $resp = curl_exec($curl);
            $json_complejo = json_decode($resp);
            for ($x = 0; $x < sizeof($json_complejo->response); $x++){
                $listaServicios=explode(";",$json_complejo->response[$x]->servicios);
                foreach($listaServicios as $row){
                    if(strlen($row)>1)
                        $arrayServicios[]=array("servicios"=>$row);
                }                
                $complejos[]=array("direccion"=>$json_complejo->response[$x]->direccion,
                                    "place_id"=>$json_complejo->response[$x]->place_id,
                                    "ciudad"=>array("id"=>$json_complejo->response[$x]->ciudadid, "descripcion"=>$json_complejo->response[$x]->nombreCiudad),
                                    "urlImagen1"=>$json_complejo->response[$x]->urlImagen1,
                                    "id"=>$json_complejo->response[$x]->id,
                                    "nombre"=>$json_complejo->response[$x]->nombre,
                                    "urlImagen8"=>$json_complejo->response[$x]->urlImagen8,
                                    "urlImagen7"=>$json_complejo->response[$x]->urlImagen7,
                                    "urlImagen6"=>$json_complejo->response[$x]->urlImagen6,
                                    "descripcionBoletos"=>$json_complejo->response[$x]->descripcionBoletos,
                                    "urlImagen5"=>$json_complejo->response[$x]->urlImagen5,
                                    "urlImagen4"=>$json_complejo->response[$x]->urlImagen4,
                                    "urlImagen3"=>$json_complejo->response[$x]->urlImagen3,
                                    "urlImagen2"=>$json_complejo->response[$x]->urlImagen2,
                                    "latitud"=>$json_complejo->response[$x]->latitud,
                                    "longitud"=>$json_complejo->response[$x]->longitud,
                                    "servicios"=>$arrayServicios);
            }
            $response = array("codRespuesta"=>0,"desRespuesta"=>"OK","complejos"=>$complejos,"method"=>"obtenerListadoComplejos");
            return $http->responsePaymentAcceptHttp($response);                        
        }
        
        /**
         * @Rest\Get("/api/paymentprocess/buscarProgramaciones/fecha/{fecha}", name="buscarProgramaciones", defaults={"_format":"json"})
         *
         * @SWG\Parameter(
         *     name="fecha",
         *     in="path",
         *     type="string",
         *     description="parametros del proceso de registrar usuario."
         * )
         * @SWG\Response(
         *     response=200,
         *     description="Array con la respuesta del proceso de registrar usuario."
         * )
         * @SWG\Response(
         *     response=500,
         *     description="Ocurrio un error al ejecutar registro de usuario."
         * )#
         *
         * @SWG\Tag(name="buscarProgramaciones")
         */
        public function buscarProgramacionesAction($fecha){
            $response = "";
//            try {
//                 $f2=strtolower(str_replace('/', '-', $_GET["fecha"]));
//                 $dimew=strtotime($f2);
//                 $fechaHoy = date("Y-m-d", $dimew);
//                 $parametrocomplej='';              
//                 if (isset($_GET["pelicula"]) && !isset($_GET["fecha"]) && !isset($_GET["ciudad"])){
//                     $parametro= " referencia= '".$_GET["pelicula"]."' and date_format(fecha_programacion,'%Y-%m-%d') = '".$fechaHoy."'";
//                 }elseif (isset($_GET["pelicula"]) && isset($_GET["fecha"]) && isset($_GET["ciudad"])){
//                     $parametro = " referencia= '".$_GET["pelicula"]."' and nombreCiudad like '%".$_GET["ciudad"]."%' and date_format(fecha_programacion,'%Y-%m-%d') = '". $fechaHoy ."'";
//                 }else{
//                     $parametro= " referencia= '".$_GET["pelicula"]."' and date_format(fecha_programacion,'%Y-%m-%d') = '".$fechaHoy."'";
//                 }
//                 $valorfinal='';
//                 $ary = explode(",", $_GET["complejo"]);
//                 foreach($ary as $key => $val) {
//                     $valorfinal= $ary[$key];
//                     if ($key==0){
//                         $bsc= "'". $valorfinal."'";
//                     }else{
//                         $bsc=$bsc . "," . "'". $valorfinal . "'";
//                     }
//                 }
//                 $parametrocomplej = " p.codigoComplejo IN (".$bsc.")";                
//                 $sqlcomplejos = 'SELECT distinct(p.codigoComplejo) as codigoComplejo  FROM complejo c
//                INNER JOIN programacion_test p ON p.codigoComplejo = c.codigoComplejo
//                INNER JOIN pelicula_test p1 ON p.codigoPelicula = p1.codVista OR p.codigoPelicula = p1.codSinec
//                INNER JOIN ciudad c1 ON c.ciudadID = c1.ciudadID
//                WHERE ' . $parametrocomplej . ' ORDER BY p.codigoComplejo';                
//                 $gsent = $this->dbh->prepare($sqlcomplejos);
//                 $gsent->execute();
//                 $datacomplejos =  $gsent->fetchAll(PDO::FETCH_OBJ);
//                 $contcomplejos=0;
//                 foreach($datacomplejos as $clavecomp=>$valorcomp){
//                     $parmfucn = " and p.codigoComplejo IN ('".$datacomplejos[$clavecomp]->codigoComplejo."')";
//                     $parametrocomp = $parametro . " and p.codigoComplejo IN ('".$datacomplejos[$clavecomp]->codigoComplejo."')";                    
//                     $sql = 'SELECT distinct(p.sesionID),p.codigoPelicula,p.codigoComplejo, DATE_FORMAT(p.fecha_programacion,"%d/%m/%Y") as fecha,p.sala,DATE_FORMAT(p.fecha_programacion,"%h:%i %p")
//                as hora,c.nombreComplejo,p1.nombreSinFormato,p1.referencia,versiones,c1.nombreCiudad,ventaActiva,seleccionAsientosActiva,p.censura, codVista
//                FROM complejo c
//                INNER JOIN
//                programacion_test p ON p.codigoComplejo = c.codigoComplejo
//                INNER JOIN
//                pelicula_test p1 ON p.codigoPelicula = p1.codVista OR p.codigoPelicula = p1.codSinec
//                INNER JOIN
//                ciudad c1 ON c.ciudadID = c1.ciudadID
//                WHERE ' . $parametrocomp . ' group by p.sala,c.codigoComplejo,c.ciudadID
//                ORDER BY c1.ciudadID , c.nombreComplejo, codVista, p.sala,hora';                    
//                     $gsent = $this->dbh->prepare($sql);
//                     $gsent->execute();
//                     $data =  $gsent->fetchAll(PDO::FETCH_OBJ);
//                     $programaciones =array();
//                     $pelic =array();                    
//                     $conthuertas=0;
//                     $conthuertasS=0;
//                     $haysalas=false;
//                     $contsalas=0;
//                     $sala='E';
//                     $salaanterior='S';                    
//                     $salaanterior5='';
//                     foreach($data as $clave=>$valor){
//                         $haysalas=true;
//                         $conthuertasS++;
//                         if ($contsalas!=0){
//                             $salaanterior=$data[$clave]->sala;
//                         }
//                         if ($sala!=$salaanterior){                            
//                             $parametrof='';
//                             if (isset($_GET["pelicula"]) && !isset($_GET["fecha"]) && !isset($_GET["ciudad"])){
//                                 $parametrof= " referencia= '".$_GET["pelicula"]."' and date_format(fecha_programacion,'%Y-%m-%d') = '".$fechaHoy."'";
//                             }elseif (isset($_GET["pelicula"]) && isset($_GET["fecha"]) && isset($_GET["ciudad"])){
//                                 $parametrof = " referencia= '".$_GET["pelicula"]."' and nombreCiudad like '%".$_GET["ciudad"]."%' and date_format(fecha_programacion,'%Y-%m-%d') = '". $fechaHoy ."'";
//                             }else{
//                                 $parametrof= " referencia= '".$_GET["pelicula"]."' and date_format(fecha_programacion,'%Y-%m-%d') = '".$fechaHoy."'";
//                             }
//                             if (isset($_GET["complejo"])){
//                                 $parametrof = $parametrof . $parmfucn . " and p.sala= '".$data[$clave]->sala."' ";                                
//                             }
//                             $sala=$data[$clave]->sala;
//                             $sqlfict = 'SELECT distinct(p.sesionID),p.codigoPelicula,p.codigoComplejo, DATE_FORMAT(p.fecha_programacion,"%d/%m/%Y") as fecha,p.sala,DATE_FORMAT(p.fecha_programacion,"%h:%i %p")
//                             as hora,c.nombreComplejo,p1.nombreSinFormato,p1.referencia,versiones,c1.nombreCiudad,ventaActiva,seleccionAsientosActiva,p.censura, codVista
//                             FROM complejo c
//                             INNER JOIN
//                             programacion_test p ON p.codigoComplejo = c.codigoComplejo
//                             INNER JOIN
//                             pelicula_test p1 ON p.codigoPelicula = p1.codVista OR p.codigoPelicula = p1.codSinec
//                             INNER JOIN
//                             ciudad c1 ON c.ciudadID = c1.ciudadID
//                             WHERE ' . $parametrof . ' group by codVIsta, c.codigoComplejo, sala,fecha_programacion,hora  order by c1.orden, c.nombreComplejo, sala,fecha_programacion, hora';
//                             $gsentf = $this->dbh->prepare($sqlfict);
//                             $gsentf->execute();
//                             $datafuncion =  $gsentf->fetchAll(PDO::FETCH_OBJ);
//                             foreach($datafuncion as $clavef=>$valorf){
//                                 $conthuertas++;
//                                 if ($contsalas==0){
//                                         $funciones[]=array("funciones"=>mapea_funcionesmovfinal($datafuncion[$clavef]->nombreComplejo,$datafuncion[$clavef]->sala,$datafuncion[$clavef]->codVista,$datafuncion,$datafuncion[$clavef]->sesionID),
//                                             "peliculaNombre"=>$datafuncion[$clavef]->nombreSinFormato,
//                                             "lenguaje"=>$datafuncion[$clavef]->versiones,
//                                             "censura"=>$datafuncion[$clavef]->censura,
//                                             "peliculaCodigo"=>$datafuncion[$clavef]->codigoPelicula,
//                                             "peliculaReferencia"=>$datafuncion[$clavef]->nombreSinFormato,
//                                             "versionDetalle"=>$datafuncion[$clavef]->versiones,
//                                             "version"=>$datafuncion[$clavef]->versiones,                                            
//                                             "SALAssss"=>$datafuncion[$clavef]->sala,                                            
//                                             "CONTADORHUERTAS"=>$contsalas,                                                                                        
//                                         );                                        
//                                         $pelc0[]=array("funciones"=>mapea_funcionesmovfinal($datafuncion[$clavef]->nombreComplejo,$datafuncion[$clavef]->sala,$datafuncion[$clavef]->codVista,$datafuncion,$datafuncion[$clavef]->sesionID),                                            
//                                             "peliculaNombre"=>$datafuncion[$clavef]->nombreSinFormato,
//                                             "lenguaje"=>$datafuncion[$clavef]->versiones,
//                                             "censura"=>$datafuncion[$clavef]->censura,
//                                             "peliculaCodigo"=>$datafuncion[$clavef]->codigoPelicula,
//                                             "peliculaReferencia"=>$datafuncion[$clavef]->nombreSinFormato,
//                                             "versionDetalle"=>$datafuncion[$clavef]->versiones,
//                                             "version"=>$datafuncion[$clavef]->versiones,                                            
//                                             "SALA"=>$datafuncion[$clavef]->sala,                                            
//                                             "CONTADORHUERTAS"=>$conthuertas,
//                                         );                                        
//                                 }else{
//                                     if ($contsalas==0){
//                                         $pelc0[]=array("funciones"=>mapea_funcionesmovfinal($datafuncion[$clavef]->nombreComplejo,$datafuncion[$clavef]->sala,$datafuncion[$clavef]->codVista,$datafuncion,$datafuncion[$clavef]->sesionID),                                            
//                                             "peliculaNombre"=>$datafuncion[$clavef]->nombreSinFormato,
//                                             "lenguaje"=>$datafuncion[$clavef]->versiones,
//                                             "censura"=>$datafuncion[$clavef]->censura,
//                                             "peliculaCodigo"=>$datafuncion[$clavef]->codigoPelicula,
//                                             "peliculaReferencia"=>$datafuncion[$clavef]->nombreSinFormato,
//                                             "versionDetalle"=>$datafuncion[$clavef]->versiones,
//                                             "version"=>$datafuncion[$clavef]->versiones,                                            
//                                             "SALA"=>$datafuncion[$clavef]->sala,                                            
//                                             "CONTADORHUERTAS"=>$conthuertas,
//                                         );
//                                     }else if ($contsalas==1){
//                                         $pelc1[]=array("funciones"=>mapea_funcionesmovfinal($datafuncion[$clavef]->nombreComplejo,$datafuncion[$clavef]->sala,$datafuncion[$clavef]->codVista,$datafuncion,$datafuncion[$clavef]->sesionID),                                            
//                                             "peliculaNombre"=>$datafuncion[$clavef]->nombreSinFormato,
//                                             "lenguaje"=>$datafuncion[$clavef]->versiones,
//                                             "censura"=>$datafuncion[$clavef]->censura,
//                                             "peliculaCodigo"=>$datafuncion[$clavef]->codigoPelicula,
//                                             "peliculaReferencia"=>$datafuncion[$clavef]->nombreSinFormato,
//                                             "versionDetalle"=>$datafuncion[$clavef]->versiones,
//                                             "version"=>$datafuncion[$clavef]->versiones,                                            
//                                             "SALA"=>$datafuncion[$clavef]->sala,                                            
//                                             "CONTADORHUERTAS"=>$conthuertas,
//                                         );
//                                     }else if ($contsalas==2){
//                                         $pelc2[]=array("funciones"=>mapea_funcionesmovfinal($datafuncion[$clavef]->nombreComplejo,$datafuncion[$clavef]->sala,$datafuncion[$clavef]->codVista,$datafuncion,$datafuncion[$clavef]->sesionID),                                            
//                                             "peliculaNombre"=>$datafuncion[$clavef]->nombreSinFormato,
//                                             "lenguaje"=>$datafuncion[$clavef]->versiones,
//                                             "censura"=>$datafuncion[$clavef]->censura,
//                                             "peliculaCodigo"=>$datafuncion[$clavef]->codigoPelicula,
//                                             "peliculaReferencia"=>$datafuncion[$clavef]->nombreSinFormato,
//                                             "versionDetalle"=>$datafuncion[$clavef]->versiones,
//                                             "version"=>$datafuncion[$clavef]->versiones,                                            
//                                             "SALA"=>$datafuncion[$clavef]->sala,                                            
//                                             "CONTADORHUERTAS"=>$conthuertas,
//                                         );
//                                     }else if ($contsalas==3){
//                                         $pelc3[]=array("funciones"=>mapea_funcionesmovfinal($datafuncion[$clavef]->nombreComplejo,$datafuncion[$clavef]->sala,$datafuncion[$clavef]->codVista,$datafuncion,$datafuncion[$clavef]->sesionID),                                            
//                                             "peliculaNombre"=>$datafuncion[$clavef]->nombreSinFormato,
//                                             "lenguaje"=>$datafuncion[$clavef]->versiones,
//                                             "censura"=>$datafuncion[$clavef]->censura,
//                                             "peliculaCodigo"=>$datafuncion[$clavef]->codigoPelicula,
//                                             "peliculaReferencia"=>$datafuncion[$clavef]->nombreSinFormato,
//                                             "versionDetalle"=>$datafuncion[$clavef]->versiones,
//                                             "version"=>$datafuncion[$clavef]->versiones,                                            
//                                             "SALA"=>$datafuncion[$clavef]->sala,                                            
//                                             "CONTADORHUERTAS"=>$conthuertas,
//                                         );
//                                     }else if ($contsalas==4){
//                                         $pelc4[]=array("funciones"=>mapea_funcionesmovfinal($datafuncion[$clavef]->nombreComplejo,$datafuncion[$clavef]->sala,$datafuncion[$clavef]->codVista,$datafuncion,$datafuncion[$clavef]->sesionID),                                           
//                                             "peliculaNombre"=>$datafuncion[$clavef]->nombreSinFormato,
//                                             "lenguaje"=>$datafuncion[$clavef]->versiones,
//                                             "censura"=>$datafuncion[$clavef]->censura,
//                                             "peliculaCodigo"=>$datafuncion[$clavef]->codigoPelicula,
//                                             "peliculaReferencia"=>$datafuncion[$clavef]->nombreSinFormato,
//                                             "versionDetalle"=>$datafuncion[$clavef]->versiones,
//                                             "version"=>$datafuncion[$clavef]->versiones,                                            
//                                             "SALA"=>$datafuncion[$clavef]->sala,                                            
//                                             "CONTADORHUERTAS"=>$conthuertas,
//                                         );
//                                     }else if ($contsalas==5){
//                                         $pelc5[]=array("funciones"=>mapea_funcionesmovfinal($datafuncion[$clavef]->nombreComplejo,$datafuncion[$clavef]->sala,$datafuncion[$clavef]->codVista,$datafuncion,$datafuncion[$clavef]->sesionID),                                           
//                                             "peliculaNombre"=>$datafuncion[$clavef]->nombreSinFormato,
//                                             "lenguaje"=>$datafuncion[$clavef]->versiones,
//                                             "censura"=>$datafuncion[$clavef]->censura,
//                                             "peliculaCodigo"=>$datafuncion[$clavef]->codigoPelicula,
//                                             "peliculaReferencia"=>$datafuncion[$clavef]->nombreSinFormato,
//                                             "versionDetalle"=>$datafuncion[$clavef]->versiones,
//                                             "version"=>$datafuncion[$clavef]->versiones,                                            
//                                             "SALA"=>$datafuncion[$clavef]->sala,                                            
//                                             "CONTADORHUERTAS"=>$conthuertas,
//                                         );
//                                     }else if ($contsalas==6){
//                                         $pelc6[]=array("funciones"=>mapea_funcionesmovfinal($datafuncion[$clavef]->nombreComplejo,$datafuncion[$clavef]->sala,$datafuncion[$clavef]->codVista,$datafuncion,$datafuncion[$clavef]->sesionID),                                            
//                                             "peliculaNombre"=>$datafuncion[$clavef]->nombreSinFormato,
//                                             "lenguaje"=>$datafuncion[$clavef]->versiones,
//                                             "censura"=>$datafuncion[$clavef]->censura,
//                                             "peliculaCodigo"=>$datafuncion[$clavef]->codigoPelicula,
//                                             "peliculaReferencia"=>$datafuncion[$clavef]->nombreSinFormato,
//                                             "versionDetalle"=>$datafuncion[$clavef]->versiones,
//                                             "version"=>$datafuncion[$clavef]->versiones,                                            
//                                             "SALA"=>$datafuncion[$clavef]->sala,                                            
//                                             "CONTADORHUERTAS"=>$conthuertas,
//                                         );
//                                     }else if ($contsalas==7){
//                                         $pelc7[]=array("funciones"=>mapea_funcionesmovfinal($datafuncion[$clavef]->nombreComplejo,$datafuncion[$clavef]->sala,$datafuncion[$clavef]->codVista,$datafuncion,$datafuncion[$clavef]->sesionID),                                            
//                                             "peliculaNombre"=>$datafuncion[$clavef]->nombreSinFormato,
//                                             "lenguaje"=>$datafuncion[$clavef]->versiones,
//                                             "censura"=>$datafuncion[$clavef]->censura,
//                                             "peliculaCodigo"=>$datafuncion[$clavef]->codigoPelicula,
//                                             "peliculaReferencia"=>$datafuncion[$clavef]->nombreSinFormato,
//                                             "versionDetalle"=>$datafuncion[$clavef]->versiones,
//                                             "version"=>$datafuncion[$clavef]->versiones,                                            
//                                             "SALA"=>$datafuncion[$clavef]->sala,                                            
//                                             "CONTADORHUERTAS"=>$conthuertas,
//                                         );
//                                     }
//                                 }
//                                 break;
//                             }                            
//                             if (isset($pelc0) || isset($pelc1) || isset($pelc2) || isset($pelc3) || isset($pelc4) || isset($pelc5) || isset($pelc6) || isset($pelc7) ){
//                                 if ($contsalas==0){
//                                     $salas[]=array("peliculas"=>$funciones,"salaDescriocion"=>$datafuncion[$clavef]->sala);
//                                 }else if ($contsalas==1){
//                                     $salas[]=array("peliculas"=>$pelc1,"salaDescriocion"=>$datafuncion[$clavef]->sala);
//                                 }else if ($contsalas==2){
//                                     $salas[]=array("peliculas"=>$pelc2,"salaDescriocion"=>$datafuncion[$clavef]->sala);
//                                 }else if ($contsalas==3){
//                                     $salas[]=array("peliculas"=>$pelc3,"salaDescriocion"=>$datafuncion[$clavef]->sala);
//                                 }else if ($contsalas==4){
//                                     $salas[]=array("peliculas"=>$pelc4,"salaDescriocion"=>$datafuncion[$clavef]->sala);
//                                 }else if ($contsalas==5){
//                                     $salas[]=array("peliculas"=>$pelc5,"salaDescriocion"=>$datafuncion[$clavef]->sala);
//                                 }else if ($contsalas==6){
//                                     $salas[]=array("peliculas"=>$pelc6,"salaDescriocion"=>$datafuncion[$clavef]->sala);
//                                 }else if ($contsalas==7){
//                                     $salas[]=array("peliculas"=>$pelc7,"salaDescriocion"=>$datafuncion[$clavef]->sala);
//                                 }                                
//                                 if ($contcomplejos>=1){
//                                     $salaanterior5=$datafuncion[$clavef]->sala;
//                                 }
//                             }else{
//                                 $salaanterior5=$datafuncion[$clavef]->sala;
//                             }                            
//                         }
                        
//                         $contsalas++;
//                     }
//                     if ($haysalas==true){
//                         if ($contcomplejos==0){
//                             $compl[]=array("complejoNombre"=>$datafuncion[$clavef]->nombreComplejo,"complejoCodigo"=>$datafuncion[$clavef]->codigoComplejo,"salas"=>$salas);
//                             $funciones =array();                            
//                         }elseif ($contcomplejos==1){
//                             $salas1[]=array("peliculas"=>$funciones,"salaDescriocion"=>$salaanterior5);
//                             $compl[]=array("complejoNombre"=>$datafuncion[$clavef]->nombreComplejo,"complejoCodigo"=>$datafuncion[$clavef]->codigoComplejo, "salas"=>$salas);
//                             $salas1 =array();
//                             $funciones =array();
//                         }elseif ($contcomplejos==2){
//                             $salas2[]=array("peliculas"=>$funciones,"salaDescriocion"=>$salaanterior5);
//                             $compl[]=array("complejoNombre"=>$datafuncion[$clavef]->nombreComplejo,"complejoCodigo"=>$datafuncion[$clavef]->codigoComplejo, "salas"=>$salas);
//                             $salas2 =array();
//                             $funciones =array();
//                         }elseif ($contcomplejos==3){
//                             $salas3[]=array("peliculas"=>$funciones,"salaDescriocion"=>$salaanterior5);
//                             $compl[]=array("complejoNombre"=>$datafuncion[$clavef]->nombreComplejo,"complejoCodigo"=>$datafuncion[$clavef]->codigoComplejo, "salas"=>$salas);
//                             $salas3 =array();
//                             $funciones =array();                            
//                         }elseif ($contcomplejos==4){
//                             $salas4[]=array("peliculas"=>$funciones,"salaDescriocion"=>$salaanterior5);
//                             $compl[]=array("complejoNombre"=>$datafuncion[$clavef]->nombreComplejo,"complejoCodigo"=>$datafuncion[$clavef]->codigoComplejo, "salas"=>$salas);
//                             $salas4 =array();
//                             $funciones =array();
//                         }elseif ($contcomplejos==5){
//                             $salas5[]=array("peliculas"=>$funciones,"salaDescriocion"=>$salaanterior5);
//                             $compl[]=array("complejoNombre"=>$datafuncion[$clavef]->nombreComplejo,"complejoCodigo"=>$datafuncion[$clavef]->codigoComplejo, "salas"=>$salas);
//                             $salas5 =array();
//                             $funciones =array();
//                         }elseif ($contcomplejos==6){
//                             $salas6[]=array("peliculas"=>$funciones,"salaDescriocion"=>$salaanterior5);
//                             $compl[]=array("complejoNombre"=>$datafuncion[$clavef]->nombreComplejo,"complejoCodigo"=>$datafuncion[$clavef]->codigoComplejo, "salas"=>$salas);
//                             $salas6 =array();
//                             $funciones =array();
//                         }elseif ($contcomplejos==7){
//                             $salas7[]=array("peliculas"=>$funciones,"salaDescriocion"=>$salaanterior5);
//                             $compl[]=array("complejoNombre"=>$datafuncion[$clavef]->nombreComplejo,"complejoCodigo"=>$datafuncion[$clavef]->codigoComplejo, "salas"=>$salas);
//                             $salas7 =array();
//                             $funciones =array();
//                         }elseif ($contcomplejos==8){
//                             $salas8[]=array("peliculas"=>$funciones,"salaDescriocion"=>$salaanterior5);
//                             $compl[]=array("complejoNombre"=>$datafuncion[$clavef]->nombreComplejo,"complejoCodigo"=>$datafuncion[$clavef]->codigoComplejo, "salas"=>$salas);
//                             $salas8 =array();
//                             $funciones =array();
//                         }elseif ($contcomplejos==9){
//                             $salas9[]=array("peliculas"=>$funciones,"salaDescriocion"=>$salaanterior5);
//                             $compl[]=array("complejoNombre"=>$datafuncion[$clavef]->nombreComplejo,"complejoCodigo"=>$datafuncion[$clavef]->codigoComplejo, "salas"=>$salas);
//                             $salas9 =array();
//                             $funciones =array();
//                         }elseif ($contcomplejos==10){
//                             $salas10[]=array("peliculas"=>$funciones,"salaDescriocion"=>$salaanterior5);
//                             $compl[]=array("complejoNombre"=>$datafuncion[$clavef]->nombreComplejo,"complejoCodigo"=>$datafuncion[$clavef]->codigoComplejo, "salas"=>$salas);
//                             $salas10 =array();
//                             $funciones =array();
//                         }elseif ($contcomplejos==11){
//                             $salas11[]=array("peliculas"=>$funciones,"salaDescriocion"=>$salaanterior5);
//                             $compl[]=array("complejoNombre"=>$datafuncion[$clavef]->nombreComplejo,"complejoCodigo"=>$datafuncion[$clavef]->codigoComplejo, "salas"=>$salas);
//                             $salas11 =array();
//                             $funciones =array();
//                         }elseif ($contcomplejos==12){
//                             $salas12[]=array("peliculas"=>$funciones,"salaDescriocion"=>$salaanterior5);
//                             $compl[]=array("complejoNombre"=>$datafuncion[$clavef]->nombreComplejo,"complejoCodigo"=>$datafuncion[$clavef]->codigoComplejo, "salas"=>$salas);
//                             $salas12 =array();
//                             $funciones =array();
//                         }elseif ($contcomplejos==13){
//                             $salas13[]=array("peliculas"=>$funciones,"salaDescriocion"=>$salaanterior5);
//                             $compl[]=array("complejoNombre"=>$datafuncion[$clavef]->nombreComplejo,"complejoCodigo"=>$datafuncion[$clavef]->codigoComplejo, "salas"=>$salas);
//                             $salas13 =array();
//                             $funciones =array();
//                         }elseif ($contcomplejos==14){
//                             $salas14[]=array("peliculas"=>$funciones,"salaDescriocion"=>$salaanterior5);
//                             $compl[]=array("complejoNombre"=>$datafuncion[$clavef]->nombreComplejo,"complejoCodigo"=>$datafuncion[$clavef]->codigoComplejo, "salas"=>$salas);
//                             $salas14 =array();
//                             $funciones =array();
//                         }elseif ($contcomplejos==15){
//                             $salas15[]=array("peliculas"=>$funciones,"salaDescriocion"=>$salaanterior5);
//                             $compl[]=array("complejoNombre"=>$datafuncion[$clavef]->nombreComplejo,"complejoCodigo"=>$datafuncion[$clavef]->codigoComplejo, "salas"=>$salas);
//                             $salas15 =array();
//                             $funciones =array();
//                         }
                        
//                     }
//                     if ($haysalas==true){
//                         $programaciones[]=array("ciudad"=>$data[$clave]->nombreCiudad,"complejos"=>$compl,"complejoNombre"=>$data[$clave]->nombreComplejo,"complejoCodigo"=>$data[$clave]->codigoComplejo);
//                     }
//                     $pelc0 =array();
//                     $pelc1 =array();
//                     $pelc2 =array();
//                     $pelc3 =array();
//                     $pelc4 =array();
//                     $pelc5 =array();
//                     $pelc6 =array();
//                     $pelc7 =array();
//                     $salas =array();
//                     if ($haysalas==true){
//                         $contcomplejos++;
//                     }
//                  }                            
//                 $gsent->closeCursor();
//                 $gsent = null;
//                 $response = array("codRespuesta"=>0,"ciudades"=>$programaciones,"desRespuesta"=>"OK");                            
//             } catch(PDOException $e) {
//                 $response = array("result" => false,"response"=>$e->getMessage());
//             }
//             $this->dbh=null;
            return $http->responsePaymentAcceptHttp($response);                        
        }
                
        /**
         * @Rest\Get("/api/paymentprocess/olvidoClave/param/{param}", name="bloquearasientos", defaults={"_format":"json"})
         *
         * @SWG\Parameter(
         *     name="param",
         *     in="path",
         *     type="string",
         *     description="parametros del proceso de olvido de clave."
         * )
         * @SWG\Response(
         *     response=200,
         *     description="Array con la respuesta del proceso de recuperar la clave de usuario."
         * )
         * @SWG\Response(
         *     response=500,
         *     description="Ocurrio un error al ejecutar olvido de clave."
         * )#
         *
         * @SWG\Tag(name="olvidoClave")
         */
        public function olvidoClaveAction($param){
            $dimemen = str_replace('+', ' ', $param);
            $parametros = explode(";", $dimemen);
//             $correo= $parametros[0];
//             $ci = $parametros[1];
//             $tipomensajeid = $parametros[2];
//             $sql="SELECT * FROM usuario WHERE correoElectronico='".$correo."' and CI='".$ci."' ";
//             $gsent = $this->dbh->prepare($sql);
//             $gsent->execute();
//             $data = $gsent->fetch(PDO::FETCH_ASSOC);
//             $resp=$gsent->rowCount();
//             if ($resp <= 0) {
//                 $response = array("codRespuesta" => "","desRespuesta"=>"Estimado usuario, los datos suministrado de cedula y correo electronico no existen verifique");
//             }else{
//                 //Se crea el arreglo
//                 $valoresRandom = array();
//                 //Se crea el primer número aleatorio
//                 $valorRandomPrimero = mt_rand(1,50);
//                 //Se inserta
//                 array_push($valoresRandom, $valorRandomPrimero);
//                 //Se crea variable para iterar
//                 $x = 1;
//                 //El if describe el paso 6. y else describe el paso 7
//                 while ($x <= 3) {
//                     $siguienteValorRadom = mt_rand(1, 50);
//                     if(in_array($siguienteValorRadom, $valoresRandom)){
//                         continue;
//                     }else{
//                         array_push($valoresRandom, $siguienteValorRadom);
//                         $x++;
//                     }
//                 }
//                 $clav = sha1($valoresRandom[0].$valoresRandom[1].$valoresRandom[2].$valoresRandom[3]);
//                 $clavusu=$valoresRandom[0].$valoresRandom[1].$valoresRandom[2].$valoresRandom[3];
//                 $sql12="UPDATE usuario SET clave='".$clav."',ultimaModificacion = CURDATE() WHERE correoElectronico='".$correo."' and CI='".$ci."' ";
//                 $gsent = $this->dbh->prepare($sql12);
//                 $gsent->execute();
//                 $resp=$gsent->rowCount();
//                 //$gsent->close();
//                 if ($resp <= 0) {
//                     $response = array("codRespuesta" => "","desRespuesta"=>"Estimado usuario, en estos momento presentamos incoveniente recuperando clave");
//                 }else{
//                     //enviar correo de recuperacion de clave
//                     $letra='A';
//                     $curl = curl_init();
//                     curl_setopt_array($curl, array(
//                         CURLOPT_RETURNTRANSFER => 1,
//                         CURLOPT_URL => 'http://172.28.85.214/correoUsuario_MovilRecupera.php?email='.$correo.'&nombre='.urlencode($data['nombre']).'&apellido='.urlencode($data['apellido']).'&letra='.$letra.'&cedula='.$ci.'&idmensaje='.$tipomensajeid.'&clave='.$clavusu,
//                         CURLOPT_USERAGENT => 'CINEX'
//                     ));
//                     $resp = curl_exec($curl);
//                     curl_close($curl);
//                     $resp=true;
//                     $response = array("codRespuesta" => "0","desRespuesta" => "Estimado usuario, su solicitud fue procesado éxitosamente. En breves momentos recibirá un correo con su clave temporal.");
//                 }
//             }
//             $this->dbh=null;
            return $http->responsePaymentAcceptHttp($response);            
        }
        
        /**
         * @Rest\Get("/api/paymentprocess/bloquearasientos/param/{param}", name="bloquearasientos", defaults={"_format":"json"})
         *
         * @SWG\Parameter(
         *     name="param",
         *     in="path",
         *     type="string",
         *     description="parametros del proceso de pago."
         * )
         * @SWG\Response(
         *     response=200,
         *     description="Array con la respuesta del proceso de bloquear asientos."
         * )
         * @SWG\Response(
         *     response=500,
         *     description="Ocurrio un error al ejecutar bloquear asientos"
         * )#
         *
         * @SWG\Tag(name="bloquearAsientos")
         */
        public function bloquearAsientosAction($param){
            $http = $this->get('httpcustom');
            $ruta_produccion = 'http://172.28.85.132:8090';
            $dimemen = str_replace('+', ' ', $param);
            $this->logPago("bloquearAsientos;Recibiendo parametros;".str_replace(";", "_", $param).";ND;ND");
            $prc=false;            
            $parametroscant = explode(":", $dimemen);
            $cantidadasiento = sizeof($parametroscant);
            $parametros = explode(";", $dimemen);
            $tickt1=$parametros[3];
            $ticktfinal = explode(":", $tickt1);
            $val1=$parametros[0];
            $val2=$parametros[1];
            $json_content2 = array();
            $dd = "";
            $validusuario=$parametros[2];            
            $valtocken = $ruta_produccion.'/api/usuario/getusuariobyid/usuarioid/'.$validusuario;
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $valtocken,
                CURLOPT_USERAGENT => 'CINEX'
            ));
            $resp = curl_exec($curl);
            $dtoken =json_decode($resp);
            $val3 = $dtoken->response->uservistaid;
            curl_close($curl);
            $this->logPago("bloquearAsientos;Obteniendo informacion usuario;".$validusuario.";".$validusuario.";".$val3);            
            for($i=0; $i<$cantidadasiento; $i++)
            {
                $bloqueo=$ticktfinal[$i];
                $parametros2 = explode("-", $bloqueo);
                $tick1 = strstr($parametros2[4], 'A', true); 
                if ($i===0) {
                    $param="WWW;111.111.111.111;WEBSERVER;".$val3.";".$val1.";".$val2.";".$tick1;
                    $dataseats=$parametros2[0].";".$parametros2[1].";".$parametros2[2].";".$parametros2[3];
                }else{
                    $dataseats.=";" .$parametros2[0].";".$parametros2[1].";".$parametros2[2].";".$parametros2[3];
                }
                
            }            
            $this->logPago("bloquearAsientos;Preparando llamada setSelectedSeats param = ".$param." dataseats = ".$dataseats.";".$validusuario.";".$validusuario.";".$val3);
            $valtocken = $ruta_produccion.'/api/vistaticketingservice/setselectedseat/dataseats/'.$dataseats.'/params/'.$param;
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $valtocken,
                CURLOPT_USERAGENT => 'CINEX'
            ));
            $response2 = curl_exec($curl);
            curl_close($curl);
            if ($response2 != ""){
                $json_content2 = json_decode($response2);
            }            
            if ($json_content2->result!=='OK') {
                $prc=true;
            }            
            if ($prc==='true') {
                $this->logPago("bloquearAsientos;Error al bloquear asientos;".$validusuario.";".$validusuario.";".$val3);
                $response = array("codRespuesta" => "-2","desRespuesta" => "Error en el bloqueo de asientos.", "method"=>"bloquearAsientos");
            }else{
                $this->logPago("bloquearAsientos;Asientos bloqueados exitosamente;".$validusuario.";".$validusuario.";".$val3);
                $response = array("codRespuesta" => "0","desRespuesta" => "Su asientos fueron bloqueados exitosamente.", "method"=>"bloquearAsientos");
            }
            return $http->responsePaymentAcceptHttp($response);    
        }
        
        /**
         * @Rest\Get("/api/paymentprocess/obtenermapa/param/{param}", name="obtenermapa", defaults={"_format":"json"})
         *
         * @SWG\Parameter(
         *     name="param",
         *     in="path",
         *     type="string",
         *     description="parametros del proceso de pago."
         * )
         * @SWG\Response(
         *     response=200,
         *     description="Array con la respuesta del proceso de obtener mapa."
         * )
         * @SWG\Response(
         *     response=500,
         *     description="Ocurrio un error al ejecutar obtenermapa"
         * )#
         *
         * @SWG\Tag(name="obtenerMapa")
         */
        public function obtenerMapaAction($param){
            $http = $this->get('httpcustom');
            $ruta_produccion = 'http://172.28.85.132:8090';
                        
            $dimemen = str_replace('+', ' ', $param);
            $parametros = explode(";", $dimemen);
            $val1=$parametros[0];//complejo
            $val2=$parametros[1]; //session id
            $validusuario=$parametros[3]; //idusuario
            $membercardnumber = "";
            $this->logPago("obtenerMapa;Obteniendo datos del usuario ".$validusuario.";".$validusuario.";ND");            
            $valtocken =  $ruta_produccion.'/api/usuario/getusuariobyid/usuarioid/'.$validusuario;
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $valtocken,
                CURLOPT_USERAGENT => 'CINEX'
            ));
            $resp = curl_exec($curl);
            $dtoken =json_decode($resp);
            $val3 = $dtoken->response->uservistaid;
            if (isset($dtoken->response->membercardnumber)){
                $membercardnumber = $dtoken->response->membercardnumber;
            }
            curl_close($curl);
            $this->logPago("obtenerMapa;Ejecutando cancel order;".$validusuario.";".$val3);            
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $ruta_produccion."/api/vistaticketingservice/cancelorder/usersession/".$val3,
                CURLOPT_USERAGENT => 'CINEX'
            ));
            $resp = curl_exec($curl);
            curl_close($curl);            
            $val4="3"; 
            $tickt1=$parametros[2];
            $parametros2 = explode("-", $tickt1);
            $tick1 = strstr($parametros2[2], 'A', true);
            $ver1 = strstr($parametros2[2], 'A');
            $resp = str_replace('A', '', $ver1);
            $tick2 = str_replace('N', '', $resp);
            $arrayboletos = array();
            $arrayboletos = explode(":", $parametros[2]);
            $ticketf = ($tick1.";".$parametros2[1].";".$parametros2[0].";".$tick2);            
            $param="WWW;111.111.111.111;WEBSERVER;".$val3.";".$val1.";".$val2;            
            $boletos= array();
            $itemboletox = array();
            $itemboletoa = array();
            $addticketparam1 = "";
            $addticketparam2 = "";
            $nfilas=0;
            $ncolumnas=0;
            $ncantidadasientos=0;
            $erroraddticket = false;
            $totaltickets = 0;
            $ticketinfo = array();
            $response2 = "";
            for($x = 0; $x < sizeof($arrayboletos); $x++){
                $ticketinfo = explode("-", $arrayboletos[$x]);
                $totaltickets = $totaltickets + $ticketinfo[1];
            }
            // agregar tantos boletos como el usuario agrego a su orden
            for ($x = 0; $x < sizeof($arrayboletos); $x++){
                $itemboletox = explode("-", $arrayboletos[$x]);
                $itemboletoa = explode("A", $itemboletox[2]);
                $boletos[] = $itemboletoa[0]."A".$itemboletoa[1].":".$itemboletoa[1].":".$itemboletox[1];
            }
            //********************************************************* */
            //Llenar el MAPA
            //********************************************************* */
            $listparamsvista = array("cinemaid" => $val1,"uservistaid" => $val3,"reintentos" => $val4, "sesionid" => $val2);
            $paramforsend = array('GetTicketTypeListRequest' => $listparamsvista);
            /* ESTA VARIABLE INDICA LA CANTIDAD DE REINTENTOS PARA OBTENER EL MAPA EN EL SERVICIO */
            $reintentosmapa = 5;
            $errormapa = true;
            $jsonmapax = array();
            $counterreintentos = 0;
            while ($errormapa){
                if ($counterreintentos > $reintentosmapa) break;
                $this->logPago("obtenerMapa;Invocando a getMapMobile;".str_replace(";", "_", $param).";".$validusuario.";".$val3);
//                $cinemaid, $sessionid, $usersessionid, $numasientos, $ticketparam, $loyaltycardnumber, $reintentos
                 echo $val1.";".$val2.";".$val3.";".$totaltickets.";".$parametros[2].";".$membercardnumber.";".$val4;
                 die();
                $response2 = $this->forward('App\Controller\VistaTicketingServiceController::getMapAction', array("cinemaid" => $val1, "sessionid" => $val2, "usersessionid" => $val3, "numasientos" => $totaltickets, "ticketparam" => $parametros[2], "loyaltycardnumber" => $membercardnumber, "reintentos" => 3));
                if ($response2 != ""){
                    echo "<pre>",
                    var_dump($response2);
                    die();
                }
//                     $dd=$response2->getContent();
//                     $json_content2 = (array)json_decode($dd);
//                     if ($json_content2 != "" && $json_content2 != null){
                        
//                     }
                    
                
//                 $response2 = json_decode($dataservice->getMapMobile($val1,$val2,$val3,$totaltickets,$parametros[2], $membercardnumber, $val4), true);
//                 if (sizeof($response2) > 0){
//                     if ($response2["response"] == "OK"){
//                    //     $this->logPago("obtenerMapa;Resultado getMapMobile;".$response2["response"].";".$validusuario.";".$val3);
//                         $nfilas=$response2['filas'];
//                         $ncolumnas=$response2['columnas'];
//                         $ncantidadasientos=$response2['cantidadasientos'];
//                         $errormapa = false;
//                     }
//                 }                
                 $counterreintentos++;
            }
            //dataasientos
            $cont=0;
            $count=0;
            $countfinal=0;
            $listparamsvista = array();
            foreach ($response2['dataasientos'] as $asientos) {
                if ($cont===0) {
                    $nombreFisico = $asientos['letra'];
                    $cont++;
                }
                $nombreFisicocomp = $asientos['letra'];
                if ($nombreFisico!==$nombreFisicocomp) {
                    $listparamsvistafinal[] = array("nombreFisico"=>$nombreFisico,"asientos"=>$listparamsvista);
                    $countfinal++;
                    $nombreFisico = $asientos['letra'];
                    $count=0;
                    $listparamsvista = array();
                }
                $listparamsvista[] = $asientos;
                $count++;
            }
            $listparamsvistafinal[] = array("nombreFisico"=>$nombreFisico,"asientos"=>$listparamsvista);
            $salad = $listparamsvistafinal;
            if (!$erroraddticket) {
                $this->logPago("obtenerMapa;Se ha tenido exito al obtener el mapa;".$validusuario.";".$val3);
                $response = array("codRespuesta"=>"0","desRespuesta"=>"OK","filas"=>$nfilas,"columnas"=>$ncolumnas,"cantidadasientos"=>$ncantidadasientos,"sala"=>$listparamsvistafinal,"boletos"=>$boletos);
            }else{
                $this->logPago("obtenerMapa;Error al obtener el mapa;".$validusuario.";".$val3);
                $response = array("codRespuesta"=>"-2","desRespuesta"=>"Se ha presentado un error al cargar el mapa de esta función.");
            }                        
            $this->logPago("obtenerMapa;Proceso finalizado;NA;NA");
            return $http->responsePaymentAcceptHttp($response);            
        }
        
        
        /**
         * @Rest\Get("/api/paymentprocess/obtenerticket/param/{param}", name="obtenerticket", defaults={"_format":"json"})
         *
         * @SWG\Parameter(
         *     name="param",
         *     in="path",
         *     type="string",
         *     description="parametros del proceso de pago."
         * )
         * @SWG\Response(
         *     response=200,
         *     description="Array con la respuesta del proceso obtener ticket."
         * )
         * @SWG\Response(
         *     response=500,
         *     description="Ocurrio un error al ejecutar obtenerticket"
         * )#
         *
         * @SWG\Tag(name="obtenerticket")
         */
        public function obtenerTicketAction($param){
            $http = $this->get('httpcustom');
            $ruta_produccion = 'http://172.28.85.132:8090';
            $listparamsvista = array();
            $paramforsend = array();
            $response2 = array();
            $json_content2 = array();
            $json_content3 = array();
            $json_seatavailable = array();
            $json_bookingfee = array();
            $json_movistar = array();
            $response = "";
            $responsex = "";            
            $userid = "";
            $uservistaid = "";
            $taza_movistar = 0;
            $Cod_Movistar = "";
            $boletosp = array();
            $dimemen = str_replace('+', ' ', $param);
            $this->logPago("obtenerTicket;Recibiendo parametros ".$param.";NA;NA");
            $parametros = explode(";", $dimemen);
            $this->logPago("obtenerTicket;Ejecutando getTicketTypeList;".$userid.";".$uservistaid);
            
            $response2 = $this->forward('App\Controller\CinexDataServiceController::getTicketTypeListInternal', array("cinemaid" => $parametros[0], "sessionid" => $parametros[1]));
            if ($response2 != ""){
                $dd=$response2->getContent();
                $json_content2 = (array)json_decode($dd);
                if ($json_content2 != "" && $json_content2 != null){
                    for ($x = 0; $x < sizeof($json_content2["response"][0]->Table); $x++){
                        $vaelim = $json_content2["response"][0]->Table[$x];
                        $dcategoria=$vaelim->TicketCategory;
                        $descripVER = $vaelim->Price_strTicket_Type_Description;
                        $descripVERwww = $vaelim->TType_strSalesChannels;       
                        $epla1 = strpos($vaelim->Price_strTicket_Type_Description,'TMPFULL');
                        $epla2 = strpos($vaelim->Price_strTicket_Type_Description,'CINEXPASS');
                        $epla3 = strpos($vaelim->Price_strTicket_Type_Description,'MULTIPASS');
                        $epla4 = strpos($vaelim->Price_strTicket_Type_Description,'VOLANTE PROMOCIONAL');
                        $pos1 = strstr($vaelim->TType_strSalesChannels,'WWW');
                        $ClubMovistar = strpos($vaelim->Price_strTicket_Type_Description,'Club Movistar');
                        if ($vaelim->TicketCategory == "VOUCHER" AND $ClubMovistar===0) {
                            $dcategoria = "STANDARD";
                        }
                        if ($dcategoria == "STANDARD" AND $epla1===false AND $epla2===false AND $epla3===false AND $pos1!="") {
                            $descrip = $vaelim->Price_strTicket_Type_Description;
                            $monto = $vaelim->Price_intTicket_Price;
                            $categoria = $vaelim->TicketCategory;
                            $Cinema_strID=$vaelim->Cinema_strID;
                            $HOCode=$vaelim->TType_strHOCode;
                            $Session_strID=$vaelim->Session_strID;
                            $codigo=$vaelim->Price_strTicket_Type_Code.'A'.$vaelim->AreaCat_strCode.'AAN';                          
                            $this->logPago("obtenerTicket;Consultando asientos disponibles cinemaid [".$parametros[0]."] sessionid [".$parametros[1]."];NA;NA");
                            $curl = curl_init();
                            curl_setopt_array($curl, array(
                                 CURLOPT_RETURNTRANSFER => 1,
                                 CURLOPT_URL => $ruta_produccion."/api/cinexdataservice/getseatsavailable?cinemaid=".$parametros[0]."&sessionid=".$parametros[1],
                                 CURLOPT_USERAGENT => 'CINEX'
                            ));
                            $resp = curl_exec($curl);
                            curl_close($curl);
                            $json_seatavailable = json_decode($resp);
                            $AsientosDisp1 = $json_seatavailable->response->SeatsAvailable;                                                                                    
                            $this->logPago("obtenerTicket;Consultando bookingfee cinemaid [".$Cinema_strID."] hocode [".$HOCode."];NA;NA");
                            $curl = curl_init();
                            curl_setopt_array($curl, array(
                                CURLOPT_RETURNTRANSFER => 1,
                                CURLOPT_URL => $ruta_produccion."/api/cinexdataservice/getbookingfeevalue?complejoid=".$Cinema_strID."&codigoticketho=".$HOCode,
                                CURLOPT_USERAGENT => 'CINEX'
                            ));
                            $resp = curl_exec($curl);
                            curl_close($curl);
                            $json_bookingfee = json_decode($resp);   
                            if (isset($json_bookingfee) && isset($json_bookingfee->response[0])){
                                $comision = $json_bookingfee->response[0];
                                if ($comision > 0){
                                    $comision = number_format($comision, 2, '.', '');   
                                    $comision = str_replace('.', '', $comision);
                                }
                            }else{
                                $comision = 0;
                            }
                            $eplap1 = strpos($vaelim->Price_strTicket_Type_Description,'E3ra Edad');
                            $eplap2 = strpos($vaelim->Price_strTicket_Type_Description,'Club Movistar');                            
                            if (!strstr(strtolower($vaelim->Price_strTicket_Type_Description), "adulto") && !strstr(strtolower($vaelim->Price_strTicket_Type_Description), "edad") && !strstr(strtolower($vaelim->Price_strTicket_Type_Description), "popular") && !strstr(strtolower($vaelim->Price_strTicket_Type_Description), "niño") && !strstr(strtolower($vaelim->Price_strTicket_Type_Description), "movistar") && !strstr(strtolower($vaelim->Price_strTicket_Type_Description), "jueves") && !strstr(strtolower($vaelim->Price_strTicket_Type_Description), "prive") && !strstr(strtolower($vaelim->Price_strTicket_Type_Description), "cinex") ) continue;
                            if (strstr($descrip, "MPFULL")) continue;
                            if (strstr($descrip, "PROMO")) continue;
                            $nombre = $descrip;
                            if (substr(strtolower($nombre), 0, 1) == "e"){
                                $nombre = substr($nombre, 1, 100);
                            }
                            if (strstr(strtolower($nombre), "club movistar") != ""){
                                $Cod_Movistar = $codigo;
                            }
                            $boletos[]=array("codigo"=>$codigo,"descripcion"=>$nombre
                                ,"monto"=>$monto,"comision"=>$comision
                                ,"category"=>$categoria,"recognitionID"=>''
                                ,"qtyAvailable"=>10,"id"=>$x);
                        }else{
                            $cualcatg=$vaelim->TicketCategory;
                            
                        }                        
                    }
                }
            }
            $longitud = count($boletos);                      
            $i=0;
//             $longitud2 = count($boletosot);
//             foreach ($boletosot as $valor2) {
//                 for($i=0; $i<$longitud2; $i++)
//                 {
//                     $Cod_Movistar = $valor2['codigo'];
//                     break;
//                 }
//             }
            $resposMovistar = array("codRespuesta" => "0","desRespuesta" => "OK","disponibles" =>$AsientosDisp1,"boletos"=>$boletos);
            $em = $this->getDoctrine()->getManager();
            
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => "http://172.20.4.236/api/payment/getmovistarexchangerate/",
                CURLOPT_USERAGENT => 'CINEX'
            ));
            $resp = curl_exec($curl);
            curl_close($curl);
            $json_movistar = json_decode($resp);
            $taza_movistar = $json_movistar->response->data;                     
            if ($taza_movistar != "" && $taza_movistar > 0){
                $nombreboletovista = "";
                // buscar monto del valor del boleto movistar.
                for ($x = 0; $x < sizeof($resposMovistar['boletos']); $x++){
                    $nombreboletovista = trim(strtolower($resposMovistar['boletos'][$x]['descripcion']));
                    if (strstr($nombreboletovista, "adulto") || strstr($nombreboletovista, "popular") || strstr($nombreboletovista, "jueves")){
                        $prev_monto_movistar = (($resposMovistar['boletos'][$x]['monto'] +  $resposMovistar['boletos'][$x]['comision']) / 100) / $taza_movistar;
                        $monto_movistar = round($prev_monto_movistar , 0, PHP_ROUND_HALF_DOWN);
                        $monto_movistar=number_format($monto_movistar,0,',','.');
                        $monto_boleto_adulto = $resposMovistar['boletos'][$x]['monto'];
                        $monto_comision_adulto = ($resposMovistar['boletos'][$x]['comision']);
                    }
                }
            }
                        
//             $i=0;
//             $conti=0;
                                               
//             foreach ($boletos as $valor) {
//                 for($i=0; $i<$longitud; $i++)
//                 {
//                     $conti++;
//                     $cual= $valor['descripcion'];
                    
//                     $boletosp[]=array("codigo"=>$valor['codigo'],"descripcion"=>$valor['descripcion']
//                         ,"monto"=>$valor['monto'],"comision"=>$valor['comision']
//                         ,"category"=>$valor['category'],"recognitionID"=>$valor['recognitionID']
//                         ,"qtyAvailable"=>$valor['qtyAvailable'],"id"=>$conti);
//                 }
//             }
            
//             $i=0;
//             $longitud2 = count($boletosot);
//             foreach ($boletosot as $valor2) {
//                 //$longitud = count($valor2);
//                 for($i=0; $i<$longitud2; $i++)
//                 {
//                     $conti++;
//                     $cual= $valor2['descripcion'];
//                     $boletosp[]=array("codigo"=>$valor2['codigo'],"descripcion"=>$valor2['descripcion']
//                         ,"monto"=>$valor2['monto'],"comision"=>$valor2['comision']
//                         ,"category"=>$valor2['category'],"recognitionID"=>$valor2['recognitionID']
//                         ,"qtyAvailable"=>$valor2['qtyAvailable'],"id"=>$conti);
//                     break;
//                 }
//             }            
            $this->logPago("obtenerTicket;Proceso finalizado;NA;NA");                        
            $response = array("codRespuesta" => "0","desRespuesta" => "OK","disponibles" =>$AsientosDisp1,"codig"=>$Cod_Movistar,"descrip"=>"CLUB MOVISTAR","mont"=>$monto_movistar,"boletos"=>$boletos, "method"=>"obtenerTicket");                
            return $http->responsePaymentAcceptHttp($response);
        }
        
        /**
         * @Rest\Get("/api/paymentprocess/completarordenloyalty/param/{param}", name="completarOrdenLoyalty", defaults={"_format":"json"})
         *
         * @SWG\Parameter(
         *     name="param",
         *     in="path",
         *     type="string",
         *     description="parametros del proceso de pago."
         * )
         * @SWG\Response(
         *     response=200,
         *     description="Array con la respuesta del proceso de pago por canje del club movistar."
         * )
         * @SWG\Response(
         *     response=500,
         *     description="Ocurrio un error al ejecutar pagartdc"
         * )#
         *
         * @SWG\Tag(name="completarOrdenLoyalty")
         */
        public function completarOrdenLoyalty($param){
            $http = $this->get('httpcustom');
            $ruta_produccion = 'http://172.28.85.132:8090';
            $fechaHoy =  date("Y-m-d");
            $fechaHoyTrans =  date("Y-m-d H:i:s");            
            $parametros = array();
            $sessionid = "";
            $complejo = "";
            $uservistaid = "";
            $causas_error = "VISTA";
            $resp = "";
            $dusuarios = array();
            $json_completeorder_loyalty = array();
            $curl = null;
            $p = $param;
            $parametros = explode(";", $p);
            $this->logPago("completarOrdenLoyalty;Recibiendo parametros ".str_replace(";", "_", $p).";ND;ND");
            $complejo = $parametros[13];
            $sessionid = $parametros[12];
            $montoadulto = 0;
            $comisionadulto = 0;
            $membercardnumber = "";
            $correousuario = $parametros[0];
            $telfcel = $parametros[2];
            $cedula = $parametros[10];
            $nombre = urlencode($parametros[1]);
            $nombrecomple = $parametros[1];
            $nombreclient = $parametros[1];
            $nombreusuario = urlencode($parametros[1]);
            $userid = $parametros[3];
            $celular = "";
            $memberIDT = "";
            $porreversar = "Y";
            $montoadultoformateado = 0;
            $comisionadultoformateado = 0;
            $cantpuesto = explode(",", $parametros[15]);
            $cantboletos = sizeof($cantpuesto);
            $tecnologia = explode("&", $parametros[16]);
            $tecnologia = $tecnologia[0];
            $asientos = $parametros[15];
            $montopago = $parametros[7];
            $tecnologia = explode("&", $parametros[16]);
            $tecnologia = $tecnologia[0];
            $montopagototal = ($parametros[7]) + ($parametros[8]);
            $respcomplorden = false;
            $localizador = "";
            $numreserva = "";
            $this->logPago("completarOrdenLoyalty;Obteniendo el valor del boleto adulto cinemaid = ".$complejo." sessionid = ".$sessionid.";ND;ND");
            // *******************************************************************************
            // BUSCAR VALOR DEL BOLETO ADULTO
            // *******************************************************************************
            $response2 = $this->getAdultTicketValueAction($complejo, $sessionid);
            if ($response2 != ""){
                $dd=$response2->getContent();
                $JSONadulto = (array)json_decode($dd);
                if ($JSONadulto["codRespuesta"] == "0"){
                    $montoadultoformateado = number_format($JSONadulto["montoboleto"] / 100, 2, ".", ",");
                    $comisionadultoformateado = number_format($JSONadulto["montocomision"] / 100, 2, ".", ",");
                    $montoadulto = $JSONadulto["montoboleto"];
                    $comisionadulto = $JSONadulto["montocomision"];
                }
            }
            $this->logPago("completarOrdenLoyalty;montoadulto = ".$montoadulto." comisionadulto = ".$comisionadulto.";ND;ND");
            // *******************************************************************************
            // ----------------
            // *******************************************************************************
            if ($montoadulto > 0 && $comisionadulto > 0){
                // *******************************************************************************
                // PASO 1 - OBTENER INFORMACION DEL USUARIO CINEXCLUSIVO
                // *******************************************************************************
                $valusuarios = $ruta_produccion.'/api/usuario/getusuariobyid/usuarioid/'.$parametros[3];
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL => $valusuarios,
                    CURLOPT_USERAGENT => 'CINEX'
                ));
                $resp = curl_exec($curl);
                $dusuarios =json_decode($resp);
                $uservistaid = $dusuarios->response->uservistaid;
                $nombreusuariodb = $dusuarios->response->nombre." ".$dusuarios->response->apellido;
                $memberIDT = $dusuarios->response->memberid;
                if ($memberIDT == "" || $memberIDT == null){
                    $memberIDT = "NA";
                }
                $membercardnumber = $dusuarios->response->membercardnumber;
                curl_close($curl);
                // *******************************************************************************
                // ----------------
                // *******************************************************************************
                if ($membercardnumber != null && $membercardnumber != ""){
                    // *******************************************************************************
                    // PASO 2 - COMPLETAR LA ORDEN
                    // *******************************************************************************
                    $montoboletocalculado = ($montoadulto + $comisionadulto) * $cantboletos;
                    $datapayment = $membercardnumber.";DEBITCARD;0";
                    $params = "WWW;111.111.111.111;WEBSERVER;".$uservistaid.";".$correousuario.";".$celular.";".str_replace(" ", "%20", $nombreusuario).";0;LOYALTY;6;".(date("Y")+1).";LOYALTY;111;".$memberIDT;
                    $valcompleteorder = $ruta_produccion.'/api/vistaticketingservice/completeorderloyalty/datapayment/'.$datapayment."/params/".$params."/mode/1/numvouchers/1";
                    $this->logPago("completarOrdenLoyalty;Invocando a CompleteOrder".$valcompleteorder.";ND;ND");
                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                        CURLOPT_RETURNTRANSFER => 1,
                        CURLOPT_URL => $valcompleteorder,
                        CURLOPT_USERAGENT => 'CINEX'
                    ));
                    $resp = curl_exec($curl);
                    curl_close($curl);
                    $this->logPago("completarOrdenLoyalty;Resultado vistaCompleteOrder ".$resp.";NA;NA");
                    if ($resp != null){
                        $json_completeorder_loyalty = json_decode($resp, true);
                        if (isset($json_completeorder_loyalty["response"]["VistaBookingId"])){
                            $localizador=$json_completeorder_loyalty["response"]["VistaBookingId"];
                        }
                        if (isset($json_completeorder_loyalty["response"]["VistaBookingNumber"])){
                            $numreserva=$json_completeorder_loyalty["response"]["VistaBookingNumber"];
                        }
                        if ($json_completeorder_loyalty["result"] == "OK"){
                            $respcomplorden=true;
                            $this->logPago("completarOrdenLoyalty;COMPRA LOYALTY APROBADA LOCALIZADOR : ".$localizador." BOOKINGID ; ".$numreserva.";ND;ND");
                            $porreversar = "N";
                            $mensfinal = "LOYALTY: APROBADA";
                            $respdescripcion = "APROBADA";
                            $respuesta = "APROBADO";
                        }else{
                            $mensfinal = "LOYALTY: ERROR COMPLETANDO LA ORDEN";
                            $causas_error = "VISTA";
                        }
                    }
                }else{
                    $mensfinal = "API: ERROR OBTENIENDO TARJETA CINEXCLUSIVO";
                    $causas_error = "API";
                }
            }else{
                $mensfinal = "API: ERROR OBTENIENDO VALOR BOLETO ADULTO";
                $causas_error = "API";
            }
            if ($respcomplorden == false){
                $respdescripcion = "REPROBADA";
                $respuesta = "REPROBADO";
                if ($mensfinal == ""){
                    $mensfinal = "LOYALTY: REPROBADA";
                }
                if ($causas_error == ""){
                    $causas_error = "VISTA";
                }
                $this->logPago("completarOrdenLoyalty;COMPRA LOYALTY REPROBADA;ND;ND");
            }
            $valorprg = $complejo."&idsession=". $parametros[12];
            // ** informacion de la funcion de la compra **
            $valpagos = $ruta_produccion.'/api/programacion/funciones?complejo='.$valorprg;
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $valpagos,
                CURLOPT_USERAGENT => 'CINEX'
            ));
            $resp = curl_exec($curl);
            $dresp =json_decode($resp);
            $fechafuncion = $dresp->response[0]->fecha_programacion." ".$dresp->response[0]->hora;
            $sala = $dresp->response[0]->sala;
            $hora = $dresp->response[0]->hora;
            $formato = $dresp->response[0]->versiones;
            $fecha = $dresp->response[0]->fecha_programacion;
            $programacionID = $dresp->response[0]->programacionID;
            $nombrecomplejo= $dresp->response[0]->nombreComplejo;
            $codigoPelicula = $dresp->response[0]->codigoPelicula;
            $poster ="http://smtp.cinex.com.ve/imagenes/carruselhome/".rawurlencode($dresp->response[0]->referencia);
            $pelicula = $dresp->response[0]->referencia." (".str_replace(" ", ") (", $dresp->response[0]->versiones).")";
            curl_close($curl);
            $json_referencia = array();
            $ref = array();
            $dd = "";
            $numeroreferencia = 0;
            $ref = $this->getNextTransactionReference($userid);
            if ($ref != ""){
                $dd=$ref->getContent();
                $json_referencia = (array)json_decode($dd);
                if ($json_referencia["codRespuesta"] == 0){
                    $numeroreferencia = $json_referencia["data"];
                }
            }
            $tecnologia = $parametros[16];
            $usersessionid =$uservistaid;
            $vistavoucher_null=NULL;
            $vencashvoucher_null=NULL;
            $detalleboleto_null=NULL;
            $vistamontoincent_null=NULL;
            $voucher_pago_null = NULL;
            $giftnombre=NULL;
            $giftcorreo=NULL;
            $giftmensaje=NULL;
            $giftimage=NULL;
            $giftcinta=NULL;
            $giftde=NULL;
            $authid_null = NULL;
            $valorinsert = [];
            $valorinsert=json_encode(array("ordenid"=>$localizador,
                "nombreentarjeta"=>$nombreusuariodb,
                "celular"=>$telfcel,
                "fecha"=>$fechaHoyTrans,
                "tarjeta"=>$membercardnumber,
                "tipotarjeta"=>"LOYALTY",
                "monto"=>$montoboletocalculado,
                "asientos"=>$asientos,
                "programacionid"=>$programacionID,
                "cedula"=>$parametros[10],
                "pelicula"=>$pelicula,
                "hora"=>$hora,
                "complejo"=>$nombrecomplejo,
                "mensaje"=>$mensfinal,
                "sala"=>$sala,
                "cantidadboletos"=>$cantboletos,
                "fechafuncion"=>$fechafuncion,
                "tecnologia"=>$tecnologia,
                "correo"=>$correousuario,
                "vencashvoucher"=>$vencashvoucher_null,
                "bookingnumber"=>$numreserva,
                "detalleboleto"=>$detalleboleto_null,
                "vistamontoincent"=>$vistamontoincent_null,
                "porreversar"=>$porreversar,
                "causa"=>$causas_error,
                "respuesta"=>$respuesta,
                "montompfull"=>$montoadultoformateado,
                "serviciompfull"=>$comisionadultoformateado,
                "voucher"=>"",
                "tipotrans"=>"1",
                "membercard"=>$membercardnumber,
                "vistavoucher"=>$numeroreferencia,
                "usersessionid"=>$usersessionid,
                "codigocomplejo"=>$complejo,
                "codigopelicula"=>$codigoPelicula,
                "authid"=>"",
                "memberid"=>$memberIDT,
                "giftnombre"=>$giftnombre,
                "giftcorreo"=>$giftcorreo,
                "giftmensaje"=>$giftmensaje,
                "giftimage"=>$giftimage,
                "giftcinta"=>$giftcinta,
                "giftde"=>$giftde
            ));
            $valinserttransacion = $ruta_produccion.'/api/transaccion/create';
            $ch = curl_init( $valinserttransacion );
            $this->logPago("completarOrdenLoyalty;INSERTANDO REGISTRO DE COMPRA ".$valorinsert.";ND;ND");
            curl_setopt( $ch, CURLOPT_POSTFIELDS, $valorinsert );
            curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            $result = curl_exec($ch);
            curl_close($ch);
            $tipomensajeid = 1;
            if ($respcomplorden){
                $letra='A';
                $metodo = "";
                $metodo = "completarOrdenLoyalty";
                $nombreusuariodb = utf8_encode($nombreusuariodb);
                $nombreusuariodb = str_replace(' ', '+', $nombreusuariodb);
                enviarCorreo($pelicula, $nombreusuariodb, $nombrecomplejo, $tipomensajeid, $sala, $fecha, $hora, $formato, $cantboletos, $asientos, $localizador, $correousuario, $poster, $userid, $uservistaid, $metodo);
                $response = array("codRespuesta" => "0","desRespuesta" => "Orden Completada Exitosamente.","localizador" => $localizador, "method"=>"compĺetarordenloyalty");
            }else{
                $response = array("codRespuesta" => "-1","desRespuesta" => "Error Completando la Orden.","localizador" => "", "method"=>"compĺetarordenloyalty");
            }
            return $http->responsePaymentAcceptHttp($response);
        }
        
        /**
         * @Rest\Get("/api/paymentprocess/completarordenmovistar/param/{param}", name="completarOrdenMovistar", defaults={"_format":"json"})
         *
         * @SWG\Parameter(
         *     name="param",
         *     in="path",
         *     type="string",
         *     description="parametros del proceso de pago."
         * )
         * @SWG\Response(
         *     response=200,
         *     description="Array con la respuesta del proceso de pago por canje del club movistar."
         * )
         * @SWG\Response(
         *     response=500,
         *     description="Ocurrio un error al ejecutar pagartdc"
         * )#
         *
         * @SWG\Tag(name="completarOrdenMovistar")
         */        
        public function completarOrdenMovistar($param){
            $http = $this->get('httpcustom');
            $p = $param;
            $ruta_produccion = 'http://172.28.85.132:8090';
            $this->logPago("completarOrdenMovistar;Recibiendo parametros ".str_replace(";", "_", $p).";ND;ND");
            $fechaHoy =  date("Y-m-d");
            $fechaHoyTrans =  date("Y-m-d H:i:s");            
            $parametros = array();
            $paramforsend = array();
            $responseTicketList = array();
            $JSONadulto = array();
            $ch = null;
            $resp = null;
            $json_token_movistar = array();
            $json_descuento_movistar = array();
            $json_completeorder_movistar = array();
            $parametros = explode(";", $p);
            $token_movistar = "";
            $montoboleto = "";
            $uservistaid = "";
            $correousuario = "";
            $nombreusuario = "";
            $url = "";
            $dd = "";
            $localizador = "";
            $output = "";
            $celular = "";
            $sessionid = "";
            $montoadulto = 0;
            $comisionadulto = 0;
            $complejo = $parametros[7];
            $sessionid = $parametros[6];
            $asientos = $parametros[9];
            $cantboletos = $parametros[13];
            $respcomplorden = false;
            $porreversar='Y';
            $montoboleto = $parametros[4];
            $uservistaid = $parametros[12];
            $celular = $parametros[2];
            $correousuario = $parametros[0];
            $nombreusuario = $parametros[1];
            $montoboletocalculado = "0";
            $jsontosend = "";
            $respdescripcion = "";
            $respuesta = "";
            $numreserva = "0";
            $causas_error = "MOVISTAR";
            $this->logPago("completarOrdenMovistar;Obteniendo el valor del boleto adulto cinemaid = ".$complejo." sessionid = ".$sessionid.";ND;ND");
            // *******************************************************************************
            // BUSCAR VALOR DEL BOLETO ADULTO
            // *******************************************************************************
            $response2 = $this->getAdultTicketValueAction($complejo, $sessionid);
            if ($response2 != ""){
                $dd=$response2->getContent();
                $JSONadulto = (array)json_decode($dd);
                if ($JSONadulto["codRespuesta"] == "0"){
                    $montoadultoformateado = number_format($JSONadulto["montoboleto"] / 100, 2, ".", ",");
                    $comisionadultoformateado = number_format($JSONadulto["montocomision"] / 100, 2, ".", ",");
                    $montoadulto = $JSONadulto["montoboleto"];
                    $comisionadulto = $JSONadulto["montocomision"];
                }
            }
            $this->logPago("completarOrdenMovistar;montoadulto = ".$montoadulto." comisionadulto = ".$comisionadulto.";ND;ND");
            // *******************************************************************************
            // ----------------
            // *******************************************************************************
            if ($montoadulto > 0 && $comisionadulto > 0){
                // *******************************************************************************
                // PASO 1 - COMPLETAR LA ORDEN
                // *******************************************************************************
                $montoboletocalculado = ($montoadulto + $comisionadulto) * $cantboletos;
                $datapayment = $celular.";DEBITCARD;0";
                $params = "WWW;111.111.111.111;WEBSERVER;".$uservistaid.";".$correousuario.";".$celular.";".str_replace(" ", "%20", $nombreusuario).";0;MOVISTAR;6;".(date("Y")+1).";MOVISTAR;111";
                $valcompleteorder = $ruta_produccion.'/api/vistaticketingservice/completeorder/datapayment/'.$datapayment."/params/".$params."/mode/1/numvouchers/1";
                $this->logPago("completarOrdenMovistar;Invocando a CompleteOrder".$valcompleteorder.";ND;ND");
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL => $valcompleteorder,
                    CURLOPT_USERAGENT => 'CINEX'
                ));
                $resp = curl_exec($curl);
                $this->logPago("completarOrdenMovistar;Resultado vistaCompleteOrder ".$resp.";NA;NA");
                if ($resp != null){
                    $json_completeorder_movistar = json_decode($resp, true);
                    if (isset($json_completeorder_movistar["response"]["VistaBookingId"])){
                        $localizador=$json_completeorder_movistar["response"]["VistaBookingId"];
                    }
                    if (isset($json_completeorder_movistar["response"]["VistaBookingNumber"])){
                        $numreserva=$json_completeorder_movistar["response"]["VistaBookingNumber"];
                    }
                    if ($json_completeorder_movistar["result"] == "OK"){
                        $respcomplorden=true;
                        $this->logPago("completarOrdenMovistar;COMPRA MOVISTAR APROBADA LOCALIZADOR : ".$localizador." BOOKINGID : ".$numreserva.";ND;ND");
                    }else{
                        $mensfinal = "VISTA: ERROR COMPLETANDO LA ORDEN";
                        $causas_error = "VISTA";
                    }
                }else{
                    $mensfinal = "VISTA: ERROR COMPLETANDO LA ORDEN";
                    $causas_error = "VISTA";
                }
                // *******************************************************************************
                // ----------------
                // *******************************************************************************
                if ($respcomplorden && $localizador != "" && $localizador != "<" && $numreserva != "" && $numreserva != "<"){
                    // *******************************************************************************
                    // PASO 2 - OBTENER EL TOKEN MOVISTAR
                    // *******************************************************************************
                    $url = $ruta_produccion."/api/payment/generatetokenmovistar/userid/".$parametros[3];
                    $this->logPago("completarOrdenMovistar;Pidiendo token movistar ".$url.";ND;ND");
                    $ch = curl_init($url);
                    $jsontosend = '{"userid":"'.$parametros[3].'"}';
                    curl_setopt( $ch, CURLOPT_POSTFIELDS, $jsontosend);
                    curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
                    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
                    $resp = curl_exec($ch);
                    $this->logPago("completarOrdenMovistar;Resultado token movistar ".$resp.";ND;ND");
                    curl_close($ch);
                    // *******************************************************************************
                    // ----------------
                    // *******************************************************************************
                    if (isset($resp) && $resp != null){
                        $json_token_movistar = json_decode($resp, true);
                        if ($json_token_movistar != null && isset($json_token_movistar["response"]["token"])){
                            $token_movistar = $json_token_movistar["response"]["token"];
                            if ($token_movistar != ""){
                                // *******************************************************************************
                                // PASO 3 - DESCONTAR PUNTOS MOVISTAR AL USUARIO
                                // *******************************************************************************
                                $puntosdescuento = 0;
                                if ($parametros[3] == "4670362") {
                                    $puntosdescuento = 1;
                                }else{
                                    $puntosdescuento = $parametros[4];
                                }
                                $url = "http://172.28.85.132:8090/index.php/api/payment/descontarpuntosmovistar/token/".$token_movistar."/cedula/".$parametros[5]."/movil/".$parametros[2]."/puntos/".$puntosdescuento."/reintentos/1";
                                $ch = curl_init($url);
                                $this->logPago("completarOrdenMovistar;Invocando a descontarpuntosmovistar ".$url.";ND;ND");
                                //curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/json;'));
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                $output = curl_exec($ch);
                                curl_close($ch);
                                if (isset($output) && $output != null){
                                    $json_descuento_movistar = json_decode($output, true);
                                    if ($json_descuento_movistar["response"]["response"] == "OK" && $json_descuento_movistar["response"]["puntosMovistarDescontados"] == $puntosdescuento){
                                        $this->logPago("completarOrdenMovistar;Puntos descontados celular ".$parametros[2]." cantidad ".$puntosdescuento.";ND;ND");
                                        $respdescripcion = "APROBADA";
                                        $respuesta = "APROBADO";
                                        $porreversar='N';
                                        $mensfinal = "MOVISTAR: APROBADA";
                                    }else{
                                        $respdescripcion = "REPROBADA";
                                        $respuesta = "REPROBADO";
                                        $porreversar='Y';
                                        $causas_error = "MOVISTAR";
                                        $mensfinal = "MOVISTAR: ERROR DESCONTANDO PUNTOS";
                                        // IMPORTANTE, ORDEN REPROBADA SE CANCELA PARA LIBERAR ASIENTOS!!!!
                                        $ticketingservice->refundbooking($numreserva, $complejo, $montopagototal);
                                    }
                                }else{
                                    $mensfinal = "MOVISTAR: ERROR DESCONTANDO PUNTOS";
                                    $causas_error = "MOVISTAR";
                                }
                                // *******************************************************************************
                                // ----------------
                                // *******************************************************************************
                            }else{
                                $mensfinal = "API: ERROR OBTENIENDO TOKEN MOVISTAR";
                                $causas_error = "API";
                                }
                        }else{
                            $mensfinal = "API: ERROR OBTENIENDO TOKEN MOVISTAR";
                            $causas_error = "API";
                        }
                    }else{
                        $mensfinal = "API: ERROR OBTENIENDO TOKEN MOVISTAR";
                        $causas_error = "API";
                    }
                }
            }else{
                $mensfinal = "VISTA: ERROR OBTENIENDO MONTO BOLETO ADULTO";
                $causas_error = "VISTA";
            }
            $valorprg = $complejo."&idsession=". $parametros[6];
            // ** getusuariobyid **
            $userid = "";
            $userid = $parametros[3];
            $valusuarios = $ruta_produccion.'/api/usuario/getusuariobyid/usuarioid/'.$userid;
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $valusuarios,
                CURLOPT_USERAGENT => 'CINEX'
            ));
            $resp = curl_exec($curl);
            $dusuarios =json_decode($resp);
            $uservistaid = $dusuarios->response->uservistaid;
            $cedula = "";
            if (isset($parametros[5])){
                $cedula = $parametros[5];
            }
            $nombreusuariodb = $dusuarios->response->nombre." ".$dusuarios->response->apellido;
            $ceduladb = $dusuarios->response->ci;
            // si el nombre del cliente viene vacio o null, usar nombre del cliente que viene de la db.
            if ($nombreusuario == "" || $nombreusuario == null) $nombreusuario = $nombreusuariodb;
            // si la cedula es vacia o null, usar la cedula que viene de la db.
            if ($cedula == "" || $cedula == null) $cedula = $ceduladb;
            // si la sala no tiene asientos numerados, usar la cantidad de asientos en vez de 00,00...
            $asientosmovistar = $asientos;
            if (strstr($asientosmovistar, "00") != ""){
                if ($cantpuesto > 0){
                    $asientosmovistar = $cantboletos;
                }else{
                    $asientosmovistar = 1;
                }
            }
            $memberIDT = $dusuarios->response->memberid;
            if ($memberIDT == "" || $memberIDT == null){
                $memberIDT = "NA";
            }
            $membercardnumber = $dusuarios->response->membercardnumber;
            curl_close($curl);
            if ($respcomplorden == false){
                $porreversar='Y';
                if ($respdescripcion == ""){
                    $respdescripcion = "REPROBADA";
                }
                if ($respuesta == ""){
                    $respuesta = "REPROBADO";
                }
                if ($mensfinal == ""){
                    $mensfinal = "MOVISTAR: REPROBADA";
                }
                $this->logPago("completarOrdenMovistar;COMPRA MOVISTAR REPROBADA;ND;ND");
            }
            // ** informacion de la funcion de la compra **
            $valpagos = $ruta_produccion.'/api/programacion/funciones?complejo='.$valorprg;
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $valpagos,
                CURLOPT_USERAGENT => 'CINEX'
            ));
            $resp = curl_exec($curl);
            $dresp =json_decode($resp);
            $fechafuncion = $dresp->response[0]->fecha_programacion." ".$dresp->response[0]->hora;
            $sala = $dresp->response[0]->sala;
            $hora = $dresp->response[0]->hora;
            $formato = $dresp->response[0]->versiones;
            $fecha = $dresp->response[0]->fecha_programacion;
            $programacionID = $dresp->response[0]->programacionID;
            $nombrecomplejo= $dresp->response[0]->nombreComplejo;
            $codigoPelicula = $dresp->response[0]->codigoPelicula;
            $poster ="http://smtp.cinex.com.ve/imagenes/carruselhome/".rawurlencode($dresp->response[0]->referencia);
            $pelicula = $dresp->response[0]->referencia." (".str_replace(" ", ") (", $dresp->response[0]->versiones).")";
            curl_close($curl);
            $tipomensajeid = 1;
            $json_referencia = array();
            $ref = array();
            $dd = "";
            $numeroreferencia = 0;
            $ref = $this->getNextTransactionReference($userid);
            if ($ref != ""){
                $dd=$ref->getContent();
                $json_referencia = (array)json_decode($dd);
                if ($json_referencia["codRespuesta"] == 0){                    
                    $numeroreferencia = $json_referencia["data"];
                }
            }
            $tecnologia = $parametros[10];
            $montompfull_movistar =NULL;
            $serviciompfull_movistar =NULL;
            $usersessionid =$uservistaid;
            $vistavoucher_null=NULL;
            $vencashvoucher_null=NULL;
            $detalleboleto_null=NULL;
            $vistamontoincent_null=NULL;
            $voucher_pago_null = NULL;
            $giftnombre=NULL;
            $giftcorreo=NULL;
            $giftmensaje=NULL;
            $giftimage=NULL;
            $giftcinta=NULL;
            $giftde=NULL;
            $authid_null = NULL;
            $valorinsert = [];
            $valorinsert=json_encode(array("ordenid"=>$localizador,
                "nombreentarjeta"=>$nombreusuario,
                "celular"=>$celular,
                "fecha"=>$fechaHoyTrans,
                "tarjeta"=>$celular,
                "tipotarjeta"=>"MOVISTAR",
                "monto"=>$montoboletocalculado,
                "asientos"=>$asientosmovistar,
                "programacionid"=>$programacionID,
                "cedula"=>"V".$cedula,
                "pelicula"=>$pelicula,
                "hora"=>$hora,
                "complejo"=>$nombrecomplejo,
                "mensaje"=>$mensfinal,
                "sala"=>$sala,
                "cantidadboletos"=>$cantboletos,
                "fechafuncion"=>$fechafuncion,
                "tecnologia"=>$tecnologia,
                "correo"=>$correousuario,
                "vencashvoucher"=>$vencashvoucher_null,
                "bookingnumber"=>$numreserva,
                "detalleboleto"=>$detalleboleto_null,
                "vistamontoincent"=>$vistamontoincent_null,
                "porreversar"=>$porreversar,
                "causa"=>$causas_error,
                "respuesta"=>$respuesta,
                "montompfull"=>$montoadultoformateado,
                "serviciompfull"=>$comisionadultoformateado,
                "voucher"=>"",
                "tipotrans"=>"1",
                "membercard"=>$membercardnumber,
                "vistavoucher"=>$numeroreferencia,
                "usersessionid"=>$usersessionid,
                "codigocomplejo"=>$complejo,
                "codigopelicula"=>$codigoPelicula,
                "authid"=>"",
                "memberid"=>$memberIDT,
                "giftnombre"=>$giftnombre,
                "giftcorreo"=>$giftcorreo,
                "giftmensaje"=>$giftmensaje,
                "giftimage"=>$giftimage,
                "giftcinta"=>$giftcinta,
                "giftde"=>$giftde
            ));
            $valinserttransacion = $ruta_produccion.'/api/transaccion/create';
            $ch = curl_init( $valinserttransacion );
            $this->logPago("completarOrdenMovistar;INSERTANDO REGISTRO DE COMPRA ".$valorinsert.";ND;ND");
            curl_setopt( $ch, CURLOPT_POSTFIELDS, $valorinsert );
            curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            $result = curl_exec($ch);
            curl_close($ch);
            if ($respcomplorden && $respdescripcion == "APROBADA"){
                $letra='A';
                $metodo = "";
                $metodo = "completarOrdenMovistar";
                $nombreusuario = utf8_encode($nombreusuario);
                $nombreusuario = str_replace(' ', '+', $nombreusuario);
                enviarCorreo($pelicula, $nombreusuario, $nombrecomplejo, $tipomensajeid, $sala, $fecha, $hora, $formato, $cantboletos, $asientosmovistar, $localizador, $correousuario, $poster, $userid, $uservistaid, $metodo);
                $response = array("codRespuesta" => "0","desRespuesta" => "Orden Completada Exitosamente.","localizador" => $localizador, "method"=>"compĺetarordenmovistar");
            }else{
                $response = array("codRespuesta" => "-1","desRespuesta" => "Error Completando la Orden.","localizador" => "", "method"=>"compĺetarordenmovistar");
            }  
            return $http->responsePaymentAcceptHttp($response);
        }
        
        /**
         * @Rest\Get("/api/paymentprocess/pagartdc/param/{param}", name="pagarTDC", defaults={"_format":"json"})
         *
         * @SWG\Parameter(
         *     name="param",
         *     in="path",
         *     type="string",
         *     description="parametros del proceso de pago."
         * )
         * @SWG\Response(
         *     response=200,
         *     description="Array con la respuesta del proceso de pago por tarjeta de credito."
         * )
         * @SWG\Response(
         *     response=500,
         *     description="Ocurrio un error al ejecutar pagartdc"
         * )#
         *
         * @SWG\Tag(name="pagarTDC")
         */
        public function pagarTDCAction($param){
            $http = $this->get('httpcustom');
            $this->logPago("pagarTDC;INICIANDO PAGO TDC;ND;ND");
            $tipotrans = 0;
            $fechaHoy =  date("Y-m-d");
            $fechaHoyTrans =  date("Y-m-d H:i:s");
            $dimemen = str_replace('+', ' ', $param);
            $ruta_produccion = 'http://172.28.85.132:8090';
            $this->logPago("pagarTDC;Recibiendo parametros ".str_replace(";", "_", $param).";ND;ND");
            $parametros = explode(";", $dimemen);
            $correo = $parametros[0];
            $telfcel = $parametros[2];
            $cedula = $parametros[10];
            $nombre = urlencode($parametros[1]);
            $nombrecomple = $parametros[1];
            $nombreclient = $parametros[1];
            $userid = $parametros[3];
            $numtarjeta = $parametros[6];
            $Ntarjfinal = substr($numtarjeta, 0, 6);
            $Ntarjfinal.= "******". substr($numtarjeta, 12, 6);
            $cvv = $parametros[9];
            $fechexpira = $parametros[4].substr($parametros[5], 2, 2);
            $tipotarjetas = $parametros[11];
            $montompfull_movistar = NULL;
            $serviciompfull_movistar = NULL;
            $usersessionid = "";
            $vistavoucher_null = NULL;
            $vencashvoucher_null = NULL;
            $detalleboleto_null = NULL;
            $vistamontoincent_null = NULL;
            $giftnombre = NULL;
            $giftcorreo = NULL;
            $giftmensaje = NULL;
            $giftimage = NULL;
            $giftcinta = NULL;
            $giftde = NULL;
            $localizador='';
            $respuesta='REPROBADO';
            $voucher_pago='';
            $numeroreferencia = "0";
            $authid='';
            $memberIDT='';
            $respcomplorden=false;
            $numreserva=0;
            $porreversar = 'Y';
            $valorinsert = [];
            $respdescripcion = "";
            if ($tipotarjetas == "MOVISTAR"){
                $tipotrans=1;
            }
            if ($tipotarjetas == ""){
                if (substr($numtarjeta, 0, 1) == "5" || substr($numtarjeta, 0, 1) == "2"){
                    $tipotarjetas = "MASTERCARD";
                }
                if (substr($numtarjeta, 0, 1) == "4"){
                    $tipotarjetas = "VISA";
                }
                if (substr($numtarjeta, 0, 1) == "3"){
                    $tipotarjetas = "AMEX";
                }
            }
            $complejo = $parametros[13];
            $cantpuesto = explode(",", $parametros[15]);
            $cantboletos = sizeof($cantpuesto);
            $tecnologia = explode("&", $parametros[16]);
            $tecnologia = $tecnologia[0];
            $asientos = $parametros[15];
            $montopago = $parametros[7];
            $montopagototal = ($parametros[7]) + ($parametros[8]);
            $causas_error = 'VISTA';
            // *******************************************************************************
            // BUSCAR LA INFORMACION DEL USUARIO QUE EFECTUA LA TRANSACCION
            // *******************************************************************************
            $valusuarios = $ruta_produccion.'/api/usuario/getusuariobyid/usuarioid/'.$userid;
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $valusuarios,
                CURLOPT_USERAGENT => 'CINEX'
            ));
            $resp = curl_exec($curl);
            $dusuarios =json_decode($resp);            
            $uservistaid = $dusuarios->response->uservistaid;
            $usersessionid = $uservistaid;
            $ceduladb = $dusuarios->response->ci;
            $nombreusuariodb = $dusuarios->response->nombre." ".$dusuarios->response->apellido;
            // si el nombre del cliente viene vacio o null, usar nombre del cliente que viene de la db.
            if ($nombreclient == "" || $nombreclient == null) $nombreclient = $nombreusuariodb;
            // si la cedula es vacia o null, usar la cedula que viene de la db.
            if ($cedula == "" || $cedula == null) $cedula = $ceduladb;
            // si la sala no tiene asientos numerados, usar la cantidad de asientos en vez de 00,00...
            if (strstr($asientos, "00") != ""){
                if ($cantpuesto > 0){
                    $asientos = $cantpuesto;
                }else{
                    $asientos = 1;
                }
            }
            $memberIDT = $dusuarios->response->memberid;
            if ($memberIDT == "" || $memberIDT == null){
                $memberIDT = "NA";
            }
            $membercardnumber = $dusuarios->response->membercardnumber;
            curl_close($curl);
            // *******************************************************************************
            // ----------------
            // *******************************************************************************
            $mesexp = $parametros[4];
            $anoexp = substr($parametros[5], 2, 2);
            $datapayment_completeorder = $numtarjeta.";CREDIT;". $montopagototal;
            $params_completeorder = "WWW;111.111.111.111;WEBSERVER;".$uservistaid .";". $correo .";". $telfcel .";".str_replace(" ", "%20", $nombre).";".$montopagototal.";".$tipotarjetas.";".$mesexp.";".$anoexp.";".$tecnologia.";".$cvv."/mode/1/numvouchers/1";
            $valcompleteorder = $ruta_produccion.'/api/vistaticketingservice/completeorder/datapayment/'.$datapayment_completeorder."/params/".$params_completeorder;
            // *******************************************************************************
            // COMPLETAR LA ORDEN.
            // *******************************************************************************
            $this->logPago("pagarTDC;Ejecutando vistaCompleteOrder ".$valcompleteorder.";".$userid.";".$uservistaid);
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $valcompleteorder,
                CURLOPT_USERAGENT => 'CINEX'
            ));
            $resp = curl_exec($curl);
            $this->logPago("pagarTDC;Resultado vistaCompleteOrder ".$resp.";".$userid.";".$uservistaid);
            $JSONcompleteorder = json_decode($resp, true);
            $parametrosactivationcode = $JSONcompleteorder["response"];
            if (isset($JSONcompleteorder["response"]["VistaBookingId"])){
                $localizador = $JSONcompleteorder["response"]["VistaBookingId"];
            }
            if (isset($JSONcompleteorder["response"]["VistaBookingNumber"])){
                $numreserva = $JSONcompleteorder["response"]["VistaBookingNumber"];
            }
            if ($JSONcompleteorder["result"] == "OK"){
                $respcomplorden = true;
                $this->logPago("pagarTDC;Orden completada ".$localizador.";".$userid.";".$uservistaid);
            }else{
                $respcomplorden = false;
                $mensfinal = "VISTA: ERROR COMPLETANDO LA ORDEN";
                $this->logPago("pagarTDC;Orden fallida;".$userid.";".$uservistaid);
            }       
            // *******************************************************************************
            // ----------------
            // *******************************************************************************
            // SOLAMENTE SI LA ORDEN FUE COMPLETADA, COBRAR TDC CONTRA MEGASOFT
            if ($localizador != "" && $localizador != "<" && $numreserva != "" && $numreserva != "<" && $respcomplorden){
                // *******************************************************************************
                // BUSCAR EL TOKEN PARA REALIZAR LA TRANSACCION CONTRA MEGASOFT
                // *******************************************************************************
                $valtoken = $ruta_produccion.'/api/payment/megasoftgeneratetoken/userid/'.$userid;
                $this->logPago("pagarTDC;Ejecutando megasoftgeneratetoken ".$valtoken.";".$userid.";".$uservistaid);
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_POST           => 1,
                    CURLOPT_URL => $valtoken,
                    CURLOPT_USERAGENT => 'CINEX'
                ));
                $resp = curl_exec($curl);
                $dtoken =json_decode($resp);
                $token = $dtoken->response->token;
                curl_close($curl);
                $this->logPago("pagarTDC;Token generado por megasoftgeneratetoken ".$token.";".$userid.";".$uservistaid);
                // *******************************************************************************
                // ----------------
                // *******************************************************************************
                // *******************************************************************************
                // ** LLAMADA A DOPAYMENT PARA COBRAR TDC VIA MEGASOFT
                // *******************************************************************************
                if ($token != "" && $token != "0"){
                    if ($userid == "4670362") $montopagototal = "0,01";
                    if ($userid == "4767658") $montopagototal = "0,01";
                    $numeroreferencia = "0";
                    $numeroreferencia = getNextTransactionReference($userid);
                    $this->logPago("pagarTDC;Numero de referencia ".$numeroreferencia.";".$userid.";".$uservistaid);
                    $valor = $token."/cod_afiliacion/2015031701/transcode/0141/pan/". $numtarjeta."/cvv2/".$cvv."/cid/".$cedula."/expdate/".$fechexpira."/amount/".$montopagototal."/client/".$nombre."/factura/".$numeroreferencia."/"."userid/".$userid ."/reintentos/3/testmode/0";
                    $valpagos = $ruta_produccion.'/api/payment/dopayment/token/'.$valor;
                    $this->logPago("pagarTDC;Ejecutando megasoftdopayment ".$valpagos.";".$userid.";".$uservistaid);
                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                        CURLOPT_RETURNTRANSFER => 1,
                        CURLOPT_URL => $valpagos,
                        CURLOPT_USERAGENT => 'CINEX'
                    ));
                    $resp = curl_exec($curl);
                    $this->logPago("pagarTDC;Respuesta megasoftdopayment ".$resp.";".$userid.";".$uservistaid);
                    $dresp = json_decode($resp);
                    $respdescripcion = $dresp->response->descripcion;
                    $ordenidcompra = $dresp->response->vtid;
                    $voucher_pago = $dresp->response->voucher;
                    $voucher_pago = str_replace("<br>", "<br/>", $voucher_pago);
                    $voucher_pago = str_replace("_", "&nbsp;", $voucher_pago);
                    curl_close($curl);
                    $this->logPago("pagarTDC;Resultado megasoftdopayment ".$respdescripcion.";".$userid.";".$uservistaid);
                    if ($respdescripcion == "APROBADA"){
                        $respuesta='APROBADO';
                        $authid = $dresp->response->authid;
                        $porreversar = 'N';
                    }else{
                        $respuesta = 'REPROBADO';
                        $authid = "";
                        $causas_error = "MEGASOFT";
                        $respcomplorden = false;
                    }
                    if ($respdescripcion == "") $respdescripcion = "SIN RESPUESTA";
                    if ($causas_error == "") $causas_error = "MEGASOFT";
                    $mensfinal = "MEGASOFT: ".$respdescripcion;
                }else{
                    $mensfinal = "API: ERROR OBTENIENDO TOKEN MEGASOFT";
                    $causas_error = "API";
                }
                // *******************************************************************************
                // ----------------
                // *******************************************************************************
            }
            // *******************************************************************************
            // ** INSERTAR RESULTADO EN LA BASE DE DATOS - TABLA TRANSACCION
            // *******************************************************************************
            // *******************************************************************************
            // ** PEDIR INFORMACION DE LA FUNCION
            // *******************************************************************************
            $valorprg = $complejo."&idsession=". $parametros[12];
            $valpagos = $ruta_produccion.'/api/programacion/funciones?complejo='.$valorprg;
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $valpagos,
                CURLOPT_USERAGENT => 'CINEX'
            ));
            $resp = curl_exec($curl);
            $dresp = json_decode($resp);
            $fechafuncion = $dresp->response[0]->fecha_programacion." ".$dresp->response[0]->hora;
            $sala = $dresp->response[0]->sala;
            $hora = $dresp->response[0]->hora;
            $formato = $dresp->response[0]->versiones;
            $fecha = $dresp->response[0]->fecha_programacion;
            $programacionID = $dresp->response[0]->programacionID;
            $nombrecomplejo = $dresp->response[0]->nombreComplejo;
            $codigoPelicula = $dresp->response[0]->codigoPelicula;
            $poster = "http://smtp.cinex.com.ve/imagenes/carruselhome/".rawurlencode($dresp->response[0]->referencia);
            $pelicula = $dresp->response[0]->referencia." (".str_replace(" ", ") (", $dresp->response[0]->versiones).")";
            curl_close($curl);
            // *******************************************************************************
            // ----------------
            // *******************************************************************************
            if ($respdescripcion != "APROBADA"){
                // IMPORTANTE, ORDEN REPROBADA SE CANCELA PARA LIBERAR ASIENTOS!!!!
                $this->refundBookingAction($numreserva, $complejo, $montopagototal);
            }
            $valorinsert = json_encode(array("ordenid"=>$localizador,
                "nombreentarjeta"=>$nombreclient,
                "celular"=>$telfcel,
                "fecha"=>$fechaHoyTrans,
                "tarjeta"=>$Ntarjfinal,
                "tipotarjeta"=>$tipotarjetas,
                "monto"=>$montopagototal,
                "asientos"=>$asientos,
                "programacionid"=>$programacionID,
                "cedula"=>$cedula,
                "pelicula"=>$pelicula,
                "hora"=>$hora,
                "complejo"=>$nombrecomplejo,
                "mensaje"=>$mensfinal,
                "sala"=>$sala,
                "cantidadboletos"=>$cantboletos,
                "fechafuncion"=>$fechafuncion,
                "tecnologia"=>$tecnologia,
                "correo"=>$correo,
                "vencashvoucher"=>$vencashvoucher_null,
                "bookingnumber"=>$numreserva,
                "detalleboleto"=>$detalleboleto_null,
                "vistamontoincent"=>$vistamontoincent_null,
                "porreversar"=>$porreversar,
                "causa"=>$causas_error,
                "respuesta"=>$respuesta,
                "montompfull"=>$montompfull_movistar,
                "serviciompfull"=>$serviciompfull_movistar,
                "voucher"=>$voucher_pago,
                "tipotrans"=>$tipotrans,
                "membercard"=>$membercardnumber,
                "vistavoucher"=>$numeroreferencia,
                "usersessionid"=>$usersessionid,
                "codigocomplejo"=>$complejo,
                "codigopelicula"=>$codigoPelicula,
                "authid"=>$authid,
                "memberid"=>$memberIDT,
                "giftnombre"=>$giftnombre,
                "giftcorreo"=>$giftcorreo,
                "giftmensaje"=>$giftmensaje,
                "giftimage"=>$giftimage,
                "giftcinta"=>$giftcinta,
                "giftde"=>$giftde
            ));
            $tipomensajeid = 1;
            $sala = rawurlencode($sala);
            $formato = rawurlencode($formato);
            $pelicula = rawurlencode($pelicula);
            $valinserttransacion = $ruta_produccion.'/api/transaccion/create';
            $url=$valinserttransacion;
            $this->logPago("pagarTDC;Escribiendo en tabla transaccion ".$valorinsert.";".$userid.";".$uservistaid);
            $ch = curl_init( $url );
            curl_setopt( $ch, CURLOPT_POSTFIELDS, $valorinsert );
            curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            $result = curl_exec($ch);
            curl_close($ch);
            if ($respdescripcion == "APROBADA"){
                $this->logPago("pagarTDC;** ORDEN ".$numeroreferencia." APROBADA **;".$userid.";".$uservistaid);
                $respuesta='APROBADO';
                $aproba="SI";
                $letra='A';
                $metodo = "";
                $metodo = "pagarTDC";
                $nombreusuariodb = utf8_encode($nombreusuariodb);
                $nombreusuariodb = str_replace(' ', '+', $nombreusuariodb);
                enviarCorreo($pelicula, $nombreusuariodb, $nombrecomplejo, $tipomensajeid, $sala, $fecha, $hora, $formato, $cantboletos, $asientos, $localizador, $correo, $poster, $userid, $uservistaid, $metodo);
                $resp=true;
            }else{
                $respuesta='REPROBADO';
                $this->logPago("pagarTDC;** ORDEN ".$numeroreferencia." REPROBADA **;".$userid.";".$uservistaid);
                $aproba="NO";
                $porreversar = 'Y';
            }
            $this->logPago("pagarTDC;Proceso compraTDC finalizado;".$userid.";".$uservistaid);
            
            if ($respdescripcion == "APROBADA"){
                if ($respcomplorden == true){
                    $response = array("codRespuesta" => "","desRespuesta" => "Orden Completada Exitosamente.","localizador" => $localizador,"voucher" => $voucher_pago,"megasoftCode" => "0", "method"=>"pagarTDC");
                }else{
                    $response = array("codRespuesta" => "-1","desRespuesta" => "Error Completando la Orden.","localizador" => "","voucher" => "","megasoftCode" => "", "method"=>"pagarTDC");
                }
            }else{
                $response = array("codRespuesta" => "-1","desRespuesta" => "Error Pago de Tarjeta.","localizador" => "","voucher" => "0","megasoftCode" => "0", "method"=>"pagarTDC");
            }	
            return $http->responsePaymentAcceptHttp($response);
        }
        
        
        /**
         * @Rest\Get("/api/paymentprocess/getnexttransactionreference/usuarioid/{usuarioid}", name="getNextTransactionReference", defaults={"_format":"json"})
         *
         * @SWG\Parameter(
         *     name="usuarioid",
         *     in="path",
         *     type="string",
         *     description="Identificador del usuario."
         * )         
         * @SWG\Response(
         *     response=200,
         *     description="Array con el nuevo numero de referencia."
         * )
         * @SWG\Response(
         *     response=500,
         *     description="Ocurrio un error al ejecutar getNextTransactionReference"
         * )#
         *
         * @SWG\Tag(name="GetNextTransactionReference")
         */
        public function getNextTransactionReference($usuarioid){
            $http = $this->get('httpcustom');
            $em = $this->getDoctrine()->getManager();
            $json_content2 = array();
            $reference = "";
            $response = "";
            $transnumber = 0;
            $reference = $this->forward('App\Controller\TransactionNumberController::CreateTransactionNumberAction', array("usuarioid" => $usuarioid));
            $transnumber = $this->forward('App\Controller\TransactionNumberController::GetransactionNumberAction', array("usuarioid" => $usuarioid));
            if ($transnumber != ""){
                $dd=$transnumber->getContent();
                $json_content2 = json_decode($dd);
            }
            if (isset($json_content2->response) && $json_content2->response){
                $response = array("codRespuesta" => "0","desRespuesta" => "Valor de referencia de transaccion obtenido.", "data"=>$json_content2->response, "method"=>"getTransactionNumber");
            }else{
                $response = array("codRespuesta" => "-1","desRespuesta" => "Error obteniendo el valor de referencia de transaccion.", "data"=>$json_content2->response, "method"=>"getTransactionNumber");
            }
            return $http->responsePaymentAcceptHttp($response);            
        }
        
        /**
         * @Rest\Get("/api/paymentprocess/logpago/params/{params}", name="logPago", defaults={"_format":"json"})
         *
         * @SWG\Parameter(
         *     name="params",
         *     in="path",
         *     type="string",
         *     description="Array con la data a registrar en el log de pago [metodo;descripcion;userid;uservistaid]."
         * )
         * @SWG\Response(
         *     response=200,
         *     description="Array con el resultado de la ejecucion de logPago."
         * )
         * @SWG\Response(
         *     response=500,
         *     description="Ocurrio un error al ejecutar logPago."
         * )#
         *
         * @SWG\Tag(name="LogPagoAction")
         */
        public function logPago($params){
            $http = $this->get('httpcustom');
            $logpath = "/var/www/html/cinexbackend/log/logpago".date("Ymd").".log";
            $parametros = array();
            $parametros = explode(";", $params);
            // p = metodo;descripcion;usuarioid;uservistaid
            $logentry = "";
            // ESTRUCTURA
            // [FECHA] [IP REMOTA] [METODO] [DESCRIPCION] [USUARIOID] [USERVISTAID]
            $logentry = "[".date("Y-m-d h:i:s")."] [IP : ".$_SERVER['REMOTE_ADDR']."] [METODO : ".$parametros[0]."] [ DESCRIPCION : ".$parametros[1]."] [ USUARIOID : ".$parametros[2]."] [ USERVISTAID : ".$parametros[3]."]\n";
            $result = file_put_contents($logpath,$logentry, FILE_APPEND);
            if ($result !== false){
                $response = array("codRespuesta" => "0","desRespuesta" => "Entrada en el log de pago creada exitosamente.", "method"=>"logPago");
            }else{
                $response = array("codRespuesta" => "-1","desRespuesta" => "Error escribiendo en el log de pago.", "method"=>"logPago");
            }
            return $http->responsePaymentAcceptHttp($response);            
        }
        
        
        /**
         * @Rest\Get("/api/paymentprocess/getsinglebooking/bookingid/{bookingid}/bookingnumber/{bookingnumber}/cinemaid/{cinemaid}", name="getsinglebooking", defaults={"_format":"json"})
         *
         * @SWG\Parameter(
         *     name="bookingid",
         *     in="path",
         *     type="string",
         *     description="localizador."
         * )
         * @SWG\Parameter(
         *     name="bookingnumber",
         *     in="path",
         *     type="integer",
         *     description="numero de reserva."
         * )
         * @SWG\Parameter(
         *     name="cinemaid",
         *     in="path",
         *     type="string",
         *     description="siglas del complejo."
         * )
 
         * @SWG\Response(
         *     response=200,
         *     description="Array con el resultado de la ejecucion de getsinglebooking."
         * )
         * @SWG\Response(
         *     response=500,
         *     description="Ocurrio un error al ejecutar getsinglebooking."
         * )#
         *
         * @SWG\Tag(name="GetSingleBooking")
         */
        public function getSingleBookingAction($bookingid, $bookingnumber, $cinemaid){
            $http = $this->get('httpcustom');
            $xml = "<?xml version='1.0' encoding='utf-8'?>
<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>
  <soap:Body>
    <GetSingleBookingRequest xmlns='http://vista.co.nz/services/WSVistaWebClient.ServiceContracts/1'>
      <ReturnTransactionStatusInfoIfSingleBookingMatch>false</ReturnTransactionStatusInfoIfSingleBookingMatch>
      <BookingId>{bookingid}</BookingId>
      <BookingNumber>{bookingnumber}</BookingNumber>
      <CinemaId>{cinemaid}</CinemaId>
      <IncludeBookingPrintStream>false</IncludeBookingPrintStream>
      <BookingPrintStreamTemplateName>1</BookingPrintStreamTemplateName>
      <OptionalClientId>111.111.111.111</OptionalClientId>
      <GetVoucherStatus>true</GetVoucherStatus>
    </GetSingleBookingRequest>
  </soap:Body>
</soap:Envelope>";
            $xml = str_replace("{bookingid}", $bookingid, $xml);
            $xml = str_replace("{bookingnumber}", $bookingnumber, $xml);
            $xml = str_replace("{cinemaid}", $cinemaid, $xml);
            $ch = curl_init("http://172.20.5.242/WSVistaWebClient/BookingService.asmx");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml', 'soapaction: http://vista.co.nz/services/WSVistaWebClient.ServiceContracts/1/GetSingleBooking'));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $output = curl_exec($ch);
            curl_close($ch);
            if ($output != ""){                
                $response = array("codRespuesta" => "0","desRespuesta" => "informacion de la orden obtenida exitosamente", "data"=>$output, "method"=>"getsinglebooking");
            }else{
                $response = array("codRespuesta" => "-1","desRespuesta" => "Error obteniendo la información de la orden", "data"=>$output, "method"=>"getsinglebooking");
            }  
            return $http->responsePaymentAcceptHttp($response);
        }
           
        /**
         * @Rest\Get("/api/paymentprocess/refundbooking/bookingnumber/{bookingnumber}/cinemaid/{cinemaid}/montototal/{montototal}", name="refundbooking", defaults={"_format":"json"})
         *
         * @SWG\Parameter(
         *     name="bookingnumber",
         *     in="path",
         *     type="string",
         *     description="numero de reserva."
         * )
         * @SWG\Parameter(
         *     name="cinemaid",
         *     in="path",
         *     type="string",
         *     description="siglas del complejo."
         * )
         * @SWG\Parameter(
         *     name="montototal",
         *     in="path",
         *     type="integer",
         *     description="monto total de la orden."
         * )         
         * @SWG\Response(
         *     response=200,
         *     description="Array con el resultado de la ejecucion de RefundBooking."
         * )
         * @SWG\Response(
         *     response=500,
         *     description="Ocurrio un error al ejecutar RefundBooking."
         * )#
         *
         * @SWG\Tag(name="RefundBooking")
         */
        public function refundBookingAction($bookingnumber, $cinemaid, $montototal){
            $http = $this->get('httpcustom');
            $xml = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://vista.co.nz/services/WSVistaWebClient.ServiceContracts/1" xmlns:ns1="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/">
   <soapenv:Header/>
   <soapenv:Body>
      <ns:RefundBookingRequest>
         <ns:BookingNumber>{bookingnumber}</ns:BookingNumber>
         <ns:CinemaId>{cinemaid}</ns:CinemaId>
         <ns:RefundBookingFee>true</ns:RefundBookingFee>
         <ns:RefundConcessions>false</ns:RefundConcessions>
         <ns:RefundReason>Auto Reverso Backend CAV</ns:RefundReason>
         <ns:RefundAmount>{montototal}</ns:RefundAmount>
	    <ns:IsPriorDayRefund>false</ns:IsPriorDayRefund>
      </ns:RefundBookingRequest>
   </soapenv:Body>
</soapenv:Envelope>';
            $xml = str_replace("{bookingnumber}", $bookingnumber, $xml);
            $xml = str_replace("{cinemaid}", $cinemaid, $xml);
            $xml = str_replace("{montototal}", $montototal, $xml);
            $ch = curl_init("http://172.20.5.242/WSVistaWebClient/BookingService.asmx");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml', 'soapaction: http://vista.co.nz/services/WSVistaWebClient.ServiceContracts/1/RefundBooking'));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $output = curl_exec($ch);
            curl_close($ch);            
            if (strstr($output, "<ResultCode>0</ResultCode>") != ""){
                $response = array("codRespuesta" => "0","desRespuesta" => "orden reversada exitosamente", "data"=>$output, "method"=>"getsinglebooking");
            }else{
                $response = array("codRespuesta" => "-1","desRespuesta" => "Error reversando la orden", "data"=>$output, "method"=>"getsinglebooking");
            }
            return $http->responsePaymentAcceptHttp($response);
        }
        
}
