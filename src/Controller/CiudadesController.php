<?php


namespace App\Controller;
use App\CoreCinex\CinexService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use App\Lib\DaoLib;
use App\Lib\ApiUtilFunctions;
use App\Lib\Constantes;


class CiudadesController extends Controller
{

     // Complejo y Ciudades URI"'s
    /**
     * @Rest\Get("/api/ciudades/complejos", name="getciudadescomplejo", defaults={"_format":"json"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Ciudades y Complejos."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Complejos por Ciudades"
     * )
     *
     * @SWG\Tag(name="CiudadesComplejos")
     */    
    public function getComplejoCiudadesAction() {
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $ciudad=[];
        $data= [];
        $data = $em->getRepository("App:Ciudad")->getCiudades();
        if (is_null($data)) {
            $data = [];
        }   
        $contador=0;
        foreach($data as $clave=>$valor){
            $dataComplejo = $em->getRepository("App:Ciudad")->getComplejoByCiudad($data[$clave]["nombreCiudad"]);
            foreach ($dataComplejo as $claveComplejo=>$valorComplejo){
               
               $data[$clave]["complejos"][]= array("nombre"=>$dataComplejo[$claveComplejo]["nombreComplejo"],
                                                "id"=>$dataComplejo[$claveComplejo]["codigoComplejo"],
                                                "abreviatura"=>$dataComplejo[$claveComplejo]["abreviatura"],
                                                "direccion"=>$dataComplejo[$claveComplejo]["direccion"]);
            }
        }
        return $http->responseAcceptHttp(array("data"=>$data));
    }


    // Ciudades por Estado URI"'s
    /**
     * @Rest\Get("/api/estado/ciudades", name="getciudadesporestado", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="estadoid",
     *     in="query",
     *     type="string",
     *     description="Codigo del Estado"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Ciudades."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Ciudades por Estado"
     * )
     *
     * @SWG\Tag(name="CiudadesPorEstado")
     */   
    

    public function getCiudadesPorEstado(Request $request) {
        $http = $this->get('httpcustom');
        $estadoId= $request->query->get('estadoid');
        $em = $this->getDoctrine()->getManager();
        $ciudad=[];
        $data= [];
        $data = $em->getRepository("App:Ciudades")->getCiudadesByIdEstado($estadoId);
        if (is_null($data)) {
            $data = [];
        }   
        return $http->responseAcceptHttpWithoutFormat($data);
    }


     

}
