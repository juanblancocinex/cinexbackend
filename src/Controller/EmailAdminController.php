<?php
namespace App\Controller;
use App\CoreCinex\CinexService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use App\Lib\DaoLib;
use App\Lib\ApiUtilFunctions;
use App\Lib\Constantes;

class EmailAdminController extends Controller
{              
    /**
     * @Rest\Get("/api/emailadmin", name="getemailadmin", defaults={"_format":"json"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Correos administrativos."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de email admin"
     * )
     *
     * @SWG\Tag(name="GetEmailAdmin")
     */
    public function getEmailAdmin() {
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $ciudad=[];
        $data= [];
        $data = $em->getRepository("App:EmailAdmin")->getEmailAdmin();
        if (is_null($data)) {
            $data = [];
        }
        return $http->responseAcceptHttp(array("data"=>$data));
    }
    
    /**
     * @Rest\Get("/api/emailadmin/type/{type}", name="getemailadminbytype", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="type",
     *     in="path",
     *     type="string",
     *     description="tipo de email administrativo."
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Plantillas por tipo."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de email administrativo por tipo"
     * )
     *
     * @SWG\Tag(name="GetEmailAdminByType")
     */
    public function getEmailAdminByType($type) {
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $ciudad=[];
        $data= [];
        $data = $em->getRepository("App:EmailAdmin")->getEmailAdminByType($type);
        if (is_null($data)) {
            $data = [];
        }
        return $http->responseAcceptHttp(array("data"=>$data));
    }
}
