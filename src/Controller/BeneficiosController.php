<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use Swagger\Annotations as SWG;
use App\Lib\DaoLib;
use App\Lib\ApiUtilFunctions;


class BeneficiosController extends Controller
{
    /**
     * @Route("/beneficios", name="beneficios")
     */
    public function index()
    {
        return $this->render('beneficios/index.html.twig', [
            'controller_name' => 'BeneficiosController',
        ]);
    }


    /**
     * @Rest\Get("/api/beneficios", name="getBeneficios", defaults={"_format":"json"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Beneficios encontrados."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de beneficios."
     * )
     *
     * @SWG\Tag(name="getBeneficios")
     */
    public function getBeneficios() {
        $apifunctions = new ApiUtilFunctions();
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $daoLib = new DaoLib($em);
        $data= [];
        $sql = "SELECT DISTINCT recognitionid as id, namebenefit as beneficio FROM evenpro_cinex_domain_model_loyalty_benefits group by namebenefit order by namebenefit asc"; 
        $data = $daoLib->prepareStament($sql);  
        return $http->responseAcceptHttp(array("data"=>$data));
    }

    // USER Conceciones URI's
    /**
     * @Rest\Get("/api/conceciones", name="getconceciones", defaults={"_format":"json"})
     * @SWG\Response(
     *     response=200,
     *     description="conceciones del Usuario encontrado."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de GetConceciones."
     * )
     *
     * @SWG\Tag(name="GetConceciones")
     */
    public function getConceciones() {
        $apifunctions = new ApiUtilFunctions();
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $daoLib = new DaoLib($em);
        $data= [];
        $sql = "SELECT con_itemcode, con_nombre_item FROM evenprosite.concesiones order by con_nombre_item asc"; 
        $data = $daoLib->prepareStament($sql);  
        return $http->responseAcceptHttp(array("data"=>$data));
    }
}
