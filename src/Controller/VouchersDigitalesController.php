<?php

namespace App\Controller;
use App\CoreCinex\CinexService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use App\Lib\DaoLib;
use App\Lib\ApiUtilFunctions;
use App\Lib\Constantes;


class VouchersDigitalesController extends Controller
{
    // Obtiene la lista de cinexpass por tipo de un usuario URI"'s
    /**
     * @Rest\Get("/api/vouchersdigitales/cedula/{ci}/formato/{formato}", name="getCinexPassCodeList", defaults={"_format":"json"})
     *
     * @SWG\Parameter(
     *     name="ci",
     *     in="path",
     *     type="string",
     *     description="cedula del usuario"
     * )
     * @SWG\Parameter(
     *     name="formato",
     *     in="path",
     *     type="string",
     *     description="formato de la sesion"
     * ) 
     * @SWG\Response(
     *     response=200,
     *     description="Voucher Digitales."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Voucher Digitales."
     * )
     * @SWG\Tag(name="getCinexPassCodeList")
     */
    public function getVouchersDigitalesAction($ci, $formato) {
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $data= [];
        $data = $em->getRepository("App:VouchersDigitales")->getVouchersDigitales($ci, $formato);
        if (is_null($data)) {
            $data = [];
        } 
        return $http->responseAcceptHttp(array("data"=>$data));
    }
    
    // Obtiene el estatus de redencion de un cinexpass especifico URI"'s
    /**
     * @Rest\Get("/api/vouchersdigitalestatus/codigo/{codvoucher}", name="getCinexPassRedeemStatus", defaults={"_format":"json"})
     *
     * @SWG\Parameter(
     *     name="codvoucher",
     *     in="path",
     *     type="string",
     *     description="codigo voucher"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Voucher Digitales."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Voucher Digitales."
     * )
     * @SWG\Tag(name="getCinexPassRedeemStatus")
     */
    public function getCinexPassRedeemStatusAction($codvoucher) {
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $data= [];
        $data = $em->getRepository("App:VouchersDigitales")->getCinexPassRedeemStatus($codvoucher);
        if (is_null($data)) {
            $data = [];
        }
        return $http->responseAcceptHttp(array("data"=>$data));
    }
    
    // Obtiene información del usuario dueño del cinexpass. URI"'s
    /**
     * @Rest\Get("/api/vouchersdigitalesusuario/cedula/{ci}/tipovoucher/{codtipovoucher}", name="getCinexPassOwner", defaults={"_format":"json"})
     * @SWG\Response(
     *     response=200,
     *     description="Dueño del Cinexpass"
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Dueño del Cinepass"
     * )
     * @SWG\Tag(name="getCinexPassOwner")
     */
    public function getCinexPassOwnerAction($ci, $codtipovoucher) {
        $apiFuntion = new ApiUtilFunctions();
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $data= [];
        $data = $em->getRepository("App:VouchersDigitales")->getCinexPassOwner($ci, $codtipovoucher);
        if (is_null($data)) {
            $data = [];
        }
        return $http->responseAcceptHttp(array("data"=>$data));
    }
    
    /**
     * @Rest\Get("/api/setcinexpassredeemed/codvoucher/{codvoucher}/channel/{$channel}", name="setCinexPassRedeemedByChannel", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="codvoucher",
     *     in="path",
     *     type="string",
     *     description="codigo voucher."
     * )
     * @SWG\Parameter(
     *     name="channel",
     *     in="path",
     *     type="string",
     *     description="canal de redencion."
     * ) 
     * @SWG\Response(
     *     response=200,
     *     description="Resultado operacion."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta"
     * )
     * @SWG\Tag(name="setCinexPassRedeemedByChannel")
     */
    public function setCinexPassRedeemedByChannel($codvoucher, $channel) {
        $apiFuntion = new ApiUtilFunctions();
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $data= [];
        $data = $em->getRepository("App:VouchersDigitales")->setCinexPassRedeemed($codvoucher, $channel);
        if (is_null($data)) {
            $data = [];
        }
        return $http->responseAcceptHttp(array("data"=>$data));
    }
}
