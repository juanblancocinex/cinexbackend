<?php

namespace App\Controller;
use App\CoreCinex\CinexService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use App\Lib\DaoLib;
use App\Lib\ApiUtilFunctions;
use App\Lib\Constantes;


class Top5Controller extends Controller
{

    /**
     * @Rest\Get("/api/top5moviedata/", name="gettop5moviedata", defaults={"_format":"json"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Get top5."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Top5."
     * )
     * @SWG\Tag(name="Top5MovieData")
     */
    public function getTop5MovieDataAction(Request $request) {
        $apiFuntion = new ApiUtilFunctions();
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $data= [];
        $data = $em->getRepository("App:Top5")->getTop5Data();
        foreach($data as $clave=>$peliculaTop5){
            $clasificacionPelicula =$em->getRepository("App:PeliculaTest")->getClaficacionPelicula($peliculaTop5["pelicula"]);
            $data[$clave]["venezolana"] = count($clasificacionPelicula)>0 && isset($clasificacionPelicula["esvenezolana"]) ? $clasificacionPelicula["esvenezolana"] : 'N';
            $data[$clave]["preventa"] = count($clasificacionPelicula)>0 && isset($clasificacionPelicula["espreventa"]) ? $clasificacionPelicula["esṕreventa"] : 'N';
            $data[$clave]["reestreno"] = count($clasificacionPelicula)>0 && isset($clasificacionPelicula["esreestreno"]) ? $clasificacionPelicula["esreestreno"] : 'N';
            $versiones = $em->getRepository("App:PeliculaTest")->getVersiones($peliculaTop5["pelicula"]);
            $stringVersiones = "";
            foreach($versiones as $valor){
                $stringVersiones .= $valor["versiones"]."|";
            }
            $data[$clave]["versiones"]=$apiFuntion->getVersionsDB($stringVersiones);
        }
        if (is_null($data)) {
            $data = [];
        }
        $parametros=array("data"=>$data);
        return $http->responseAcceptHttp($parametros);
        
    }

     
    // Top5 URI's
    /**
     * @Rest\Get("/api/top5/", name="gettop5", defaults={"_format":"json"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Get top5."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Top5."
     * )
     * @SWG\Tag(name="Top5")
     */
    public function getTop5Action(Request $request) {
        $apiFuntion = new ApiUtilFunctions();
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $data= [];
        $data = $em->getRepository("App:Top5")->getTop5();
        foreach($data as $clave=>$peliculaTop5){
            $clasificacionPelicula =$em->getRepository("App:PeliculaTest")->getClaficacionPelicula($peliculaTop5["pelicula"]);
            $data[$clave]["venezolana"] = count($clasificacionPelicula)>0 && isset($clasificacionPelicula["esvenezolana"]) ? $clasificacionPelicula["esvenezolana"] : 'N';
            $data[$clave]["preventa"] = count($clasificacionPelicula)>0 && isset($clasificacionPelicula["espreventa"]) ? $clasificacionPelicula["esṕreventa"] : 'N';
            $data[$clave]["reestreno"] = count($clasificacionPelicula)>0 && isset($clasificacionPelicula["esreestreno"]) ? $clasificacionPelicula["esreestreno"] : 'N';
            $versiones = $em->getRepository("App:PeliculaTest")->getVersiones($peliculaTop5["pelicula"]);
            $stringVersiones = "";
            foreach($versiones as $valor){
                $stringVersiones .= $valor["versiones"]."|"; 
            }
            $data[$clave]["versiones"]=$apiFuntion->getVersionsDB($stringVersiones);
        }
        if (is_null($data)) {
            $data = [];
        }
        $parametros=array("data"=>$data);
        return $http->responseAcceptHttp($parametros);
    
    }


     // Top5 URI's
    /**
     * @Rest\Get("/api/cinexdataservice/top5tiny/", name="gettop5tiny", defaults={"_format":"json"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Get top5tiny de peliculas."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Top5Tiny."
     * )
     * @SWG\Tag(name="Top5Tiny")
     */
    public function getTop5TinyAction(Request $request) {
        $apiFuntion = new ApiUtilFunctions();
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $data= [];
        $data = $em->getRepository("App:Top5")->getTop5();
        foreach($data as $clave=>$peliculaTop5){
            $clasificacionPelicula =$em->getRepository("App:PeliculaTest")->getClaficacionPelicula($peliculaTop5["pelicula"]);
            $data[$clave]["venezolana"] = count($clasificacionPelicula)>0 && isset($clasificacionPelicula["esvenezolana"]) ? $clasificacionPelicula["esvenezolana"] : 'N';
            $data[$clave]["preventa"] = count($clasificacionPelicula)>0 && isset($clasificacionPelicula["espreventa"]) ? $clasificacionPelicula["esṕreventa"] : 'N';
            $data[$clave]["reestreno"] = count($clasificacionPelicula)>0 && isset($clasificacionPelicula["esreestreno"]) ? $clasificacionPelicula["esreestreno"] : 'N';
            $versiones = $em->getRepository("App:PeliculaTest")->getVersiones($peliculaTop5["pelicula"]);
            $stringVersiones = "";
            foreach($versiones as $valor){
                $stringVersiones .= $valor["versiones"]."|"; 
            }
            $data[$clave]["versiones"]=$apiFuntion->getVersionsDB($stringVersiones);
        }
        if (is_null($data)) {
            $data = [];
        }
        $parametros=array("data"=>$data);
        return $http->responseAcceptHttp($parametros);
    
    }


}
