<?php
namespace App\Controller;
use App\CoreCinex\CinexService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use App\Lib\DaoLib;
use App\Lib\ApiUtilFunctions;
use App\Lib\Constantes;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Translation\TranslatorInterface;
use App\Lib\XmlLib;

class SyncController extends Controller
{
    protected $clientSinec;
    protected $client;
    protected $params;
    protected $arrayConnect;
    protected $xmlLib;
    protected $apiUtilFunctions;
    
    /**
     * Constructor.
     **/
    public function __construct(ParameterBagInterface $params)
    {        
        $this->xmlLib= new XmlLib();
        $this->apiUtilFunctions= new ApiUtilFunctions();
        $this->params=$params;
        $this->arrayConnect=array("OptionalClientClass" =>$this->params->get('soap.client_class'),
            "OptionalClientId" =>$this->params->get('soap.client_id'),
            "OptionalClientName" =>$this->params->get("soap.client_name"));
        //$this->client = new \SoapClient($this->params->get('soap.cinexdataservice.wsdl'), array("trace" => 1, "exception" => 1,'encoding'=>'UTF8'));
        $this->client =  new \nusoap_client($this->params->get('soap.cinexdataservice.wsdl'),'wsdl');
        $this->client->soap_encoding = 'UTF-8';
        $this->client->decode_utf8 = TRUE;
        $this->clientSinec =  new \nusoap_client($this->params->get('soap.sinec.wsdl'),'wsdl');
        $this->clientSinec->soap_encoding = 'UTF-8';
        $this->clientSinec->decode_utf8 = TRUE;
    }

    /**
     * @Rest\Get("/api/datasynccontroller/getcommingsoon", name="synccontrollergetcommingsoon", defaults={"_format":"json"})
     * @SWG\Response(
     *     response=200,
     *     description="Información de las peliculas proximamente."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de GetCommingSoon"
     * )
     *
     * @SWG\Tag(name="DataSyncControllerGetCommingSoon")
     */
    public function getCommingSoonAction() {
        $http = $this->get('httpcustom');
        $data= [];
        $params= null;
        $dataResponse = $this->clientSinec->call('GetProximosEstrenos',array($params), '', '', false, true);
        $dataTable=	$dataResponse['GetProximosEstrenosResult']['respDataSet']["diffgram"]["NewDataSet"]["Peliculas"];
        foreach ($dataTable as $clave => $valor){
            foreach ($valor as $cl=>$vl){
                $dataTable[$clave][$cl]=utf8_encode($dataTable[$clave][$cl]);
            }
        }
        $data =$this->prepareSinecHttpResponse($dataTable);
        return $http->responseAcceptHttpVista($data);       
    }
    
    /**
     * @Rest\Get("/api/datasynccontroller/getsinecmovieinfo/movieid/{movieid}", name="synccontrollergetsinecmovieinfo", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="movieid",
     *     in="path",
     *     required=true,
     *     type="string",
     *     description="id de la pelicula"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Información de las peliculas proximamente."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de etSinecMovieInfo"
     * )
     *
     * @SWG\Tag(name="DataSyncControllerGetSinecMovieInfo")
     */
    public function getSinecMovieInfoAction($movieid) {
        $http = $this->get('httpcustom');
        $data= [];
        $dataTable=[];
        $params = array(
            "co_pelicula"=>isset($movieid) ? $movieid : null,
            "ind_unica"=>"1",
        );
        $dataResponse = $this->clientSinec->call('GetPelicula_as',array($params), '', '', false, true);
        if (isset($dataResponse) && $dataResponse != null && isset($dataResponse['GetPelicula_asResult'])){
            $dataTable = $dataResponse['GetPelicula_asResult']['respDataSet']['diffgram']['NewDataSet']['Peliculas'];
        }
        foreach ($dataTable as $clave => $valor){ 
            $dataTable[$clave]=utf8_encode($dataTable[$clave]);
        }
        $data =$this->prepareSinecHttpResponse(array($dataTable));
        return $http->responseAcceptHttpVista($data);
    }
    
    /**
     * @Rest\Get("/api/datasynccontroller/dosync", name="synccontrollerdosync", defaults={"_format":"json"})
     * @SWG\Response(
     *     response=200,
     *     description="Información del proceso de sincronizacion."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de doSync"
     * )
     *
     * @SWG\Tag(name="DataSyncControllerDoSync")
     */
    public function doSyncAction() {
        $http = $this->get('httpcustom');
        $response1 = "";
        $response2 = "";
        $response3 = "";
        $response2 = $this->doSyncSinecAction();
        $response3 = $this->doSyncCommingSoonAction();
        $response1 = $this->doSyncVistaAction();
        if ($response2 != "" && $response3 != "" && $response4 != ""){
            $data = array("result"=>true, "response"=>"OK", "error"=>"", "data"=>"");
        }else{            
            $data = array("result"=>true, "response"=>"ERROR", "error"=>"error sincronizando", "data"=>$response2." ".$response3.$response1);
        }
        return $http->responseAcceptHttp(array("data"=>$data));  
    }
        
    /**
     * @Rest\Get("/api/datasynccontroller/getsinecmovielist/cinemaid/{cinemaid}", name="synccontrollergetsinecmovielist", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="cinemaid",
     *     in="path",
     *     required=true,
     *     type="string",
     *     description="id de complejo"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Información de peliculas por complejo y sala."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de GetSinecMovieList"
     * )
     *
     * @SWG\Tag(name="DataSyncControllerGetSinecMovieList")
     */
    public function getSinecMovieListAction($cinemaid) {
        $x = 0;
        $y = 0;
        $http = $this->get('httpcustom');
        $data= [];
        $data_result = array();
        $dataTable = array();
        $stringarray = "";
        $params = array(
            "co_complejo"=>isset($cinemaid) ? $cinemaid : null  ,
            "co_sala"=>"" ,
        );
        $dataResponse = $this->clientSinec->call('GetProgramacionesComplejoSala',array($params), '', '', false, true);
        if (isset($dataResponse) && $dataResponse != null && isset($dataResponse['GetProgramacionesComplejoSalaResult'])){
            $dataTable=	$dataResponse['GetProgramacionesComplejoSalaResult']['respDataSet']['diffgram']['NewDataSet']['Programacionesporsala'];
        }
        for ($x = 0; $x < sizeof($dataTable); $x++){
            $stringarray = json_encode($dataTable[$x], true);
            if ($stringarray != ""){
                $data_result[$y]= json_decode($stringarray, true);
                $y++;
            }
        }
        $data =$this->prepareSinecHttpResponse($data_result);
        return $http->responseAcceptHttpVista($data);
    }
        
    /**
     * @Rest\Get("/api/datasynccontroller/dosynccommingsoon", name="synccontrollerdosynccommingsoon", defaults={"_format":"json"})
     * @SWG\Response(
     *     response=200,
     *     description="Listado Peliculas a estrenar proximamente de sinec."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de DoSyncCommingSoon"
     * )
     *
     * @SWG\Tag(name="DataSyncControllerDoSyncCommingSoon")
     */    
    public function doSyncCommingSoonAction(){        
     /*   $arrayFecha = array();
        $fechaEstreno = "";
        $x = 0;
        $nummovies = 0;
        $ultimaModificacion = "";
        $fechaCreacion = "";
        $mes = "";
        $phpdate = "";
        $arrayPeliculas = array();        
        $data = array();
        $json_content = array();
        $moviedata = array();
        $http = $this->get('httpcustom');
        $response = $this->getCommingSoonAction();
        if ($response != ""){            
            $json_content = json_decode($response->getContent());
            $arrayPeliculas = $json_content->response;
            $fechaCreacion = date("Y-m-d H:m:s");
            $ultimaModificacion = date("Y-m-d H:m:s");            
            for ($x = 0; $x < sizeof($arrayPeliculas); $x++){
                $moviedata['nombresinformato'] = substr($arrayPeliculas[$x]->nb_pelicula_largo, 0, strpos($arrayPeliculas[$x]->nb_pelicula_largo, "(")-1);
                $moviedata['nombreoriginal'] = $arrayPeliculas[$x]->nb_pelicula_original;
                $moviedata['referencia'] = $this->apiUtilFunctions->sanear_string2(substr($arrayPeliculas[$x]->nb_pelicula_largo, 0, strpos($arrayPeliculas[$x]->nb_pelicula_largo, "(")-1));
                $moviedata['censura'] = $arrayPeliculas[$x]->co_clase_pelicula;
                $moviedata['genero'] = $this->apiUtilFunctions->getGenero($arrayPeliculas[$x]->co_genero_pelicula);
                $moviedata['fechaestreno'] = substr($arrayPeliculas[$x]->fe_estreno, 0, 10);
                $moviedata['urltrailer'] = "";
                $moviedata['urlposter1'] = "";
                $moviedata['urlposter2'] = "";
                $moviedata['codvista'] = $arrayPeliculas[$x]->co_pelicula;
                $moviedata['codsinec'] = $arrayPeliculas[$x]->co_pelicula;
                $moviedata['versiones'] = $this->apiUtilFunctions->getFormatFromMovieName($arrayPeliculas[$x]->nb_pelicula_original);
                $moviedata['director'] = "";
                $moviedata['elenco'] = $this->apiUtilFunctions->sanear_genero($arrayPeliculas[$x]->txt_elenco);
                $moviedata['tipo'] = "";
                $moviedata['duracion'] = $arrayPeliculas[$x]->mm_duracion_pelicula;
                $moviedata['descripcion'] = $this->apiUtilFunctions->sanear_string2($arrayPeliculas[$x]->txt_sinopsis);
                $moviedata['contenido'] = $this->apiUtilFunctions->sanear_string2($arrayPeliculas[$x]->txt_sinopsis);
                $moviedata['urlposter3'] = "";
                $moviedata['urlposter4'] = "";
                $moviedata['esdestacada'] = "";
                $moviedata['espreventa'] = "";
                $moviedata['esvenezolana'] = "";
                $moviedata['fechacreacion'] = $fechaCreacion;
                $moviedata['espromocion'] = "";
                $moviedata['ultimamodificacion'] = $ultimaModificacion;
                $em = $this->getDoctrine()->getManager();
                $data = $em->getRepository("App:PeliculaTest")->getPeliculasByCodSinec($codigoPelicula);
                if ($data == null){
                    $response = $this->forward('App\Controller\PeliculaTestController::createPelicula', $moviedata);
                    $nummovies++;
                }else{
                    $response = $this->forward('App\Controller\PeliculaTestController::updatePelicula', $moviedata);                    
                    $nummovies++;
                }
            }
            $data = array("result"=>true, "response"=>"OK", "error"=>"", "data"=>"", "numero de peliculas procesadas"=>$nummovies);            
        }else{
            $data = array("result"=>false, "response"=>"ERROR", "error"=>"Servicio de peliculas proximamente no responde", "data"=>"", "numero de peliculas procesadas"=>"0");
        }*/
        return $http->responseAcceptHttp(array("data"=>$data));        
    }  
    
    /**
     * @Rest\Get("/api/datasynccontroller/dosynccinemasinec/cinemaid/{cinemaid}", name="synccontrollerdosynccinemasinec", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="cinemaid",
     *     in="path",
     *     required=true,
     *     type="string",
     *     description="id de complejo"
     * ) 
     * @SWG\Response(
     *     response=200,
     *     description="Sincroniza programacion de un complejo PROPACINE desde sinec"
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de DoSyncCinemaSinec"
     * )
     *
     * @SWG\Tag(name="DataSyncControllerDoSyncCinemaSinec")
     */
    
    public function doSyncCinemaSinec($cinemaid){        
    /*    $arrayFecha = array();
        $fechaEstreno = "";
        $x = 0;
        $nummovies = 0;
        $ultimaModificacion = "";
        $fechaCreacion = "";
        $mes = "";
        $phpdate = "";
        $response = null;
        $response2 = null;
        $arrayPeliculas = array();
        $data = array();
        $datarev = array();
        $json_content = array();        
        $json_content2 = array();
        $moviedata = array();
        $d = "";
        $fechaCreacion = date("Y-m-d H:m:s");
        $ultimaModificacion = date("Y-m-d H:m:s");       
        $http = $this->get('httpcustom');
        $response = $this->getSinecMovieListAction($cinemaid);
        if ($response != ""){
            $d=$response->getContent();
            $json_content = (array)json_decode($d);
            if ($json_content["result"] == "OK"){
                for ($x = 0; $x < sizeof($json_content["response"]); $x++){
                    $em = $this->getDoctrine()->getManager();                    
                    $response2 = $this->getSinecMovieInfoAction($json_content["response"][$x]->co_pelicula);
                    if ($response2 != ""){
                        $d=$response2->getContent();
                        $json_content2 = (array)json_decode($d);
                        if ($json_content2["result"] == "OK"){                                                                                    
                            $moviedata['nombresinformato'] = substr($json_content2["response"][0]->nb_pelicula_largo, 0, strpos($json_content2["response"][0]->nb_pelicula_largo, "(")-1);
                            $moviedata['nombreoriginal'] = $json_content2["response"][0]->nb_pelicula_original;
                            $moviedata['referencia'] = $this->apiUtilFunctions->sanear_string2(substr($json_content2["response"][0]->nb_pelicula_largo, 0, strpos($json_content2["response"][0]->nb_pelicula_largo, "(")-1));
                            $moviedata['censura'] = $json_content2["response"][0]->co_clase_pelicula;
                            $moviedata['genero'] = $this->apiUtilFunctions->getGenero($json_content2["response"][0]->co_genero_pelicula);
                            $moviedata['fechaestreno'] = substr($json_content2["response"][0]->fe_estreno, 0, 10);
                            $moviedata['urltrailer'] = "";
                            $moviedata['urlposter1'] = "";
                            $moviedata['urlposter2'] = "";
                            $moviedata['codvista'] = $json_content2["response"][0]->co_pelicula;
                            $moviedata['codsinec'] = $json_content2["response"][0]->co_pelicula;
                            $moviedata['versiones'] = $this->apiUtilFunctions->getFormatFromMovieName($json_content2["response"][0]->nb_pelicula_largo);
                            $moviedata['director'] = $json_content2["response"][0]->txt_director;
                            $moviedata['elenco'] = $json_content2["response"][0]->txt_elenco;
                            $moviedata['tipo'] = "";
                            $moviedata['duracion'] = $json_content2["response"][0]->mm_duracion_pelicula;
                            $moviedata['descripcion'] = $this->apiUtilFunctions->sanear_string2($json_content2["response"][0]->txt_sinopsis);
                            $moviedata['contenido'] = $this->apiUtilFunctions->sanear_string2($json_content2["response"][0]->txt_sinopsis);
                            $moviedata['urlposter3'] = "";
                            $moviedata['urlposter4'] = "";
                            $moviedata['esdestacada'] = "";
                            $moviedata['espreventa'] = "";
                            $moviedata['esvenezolana'] = "";
                            $moviedata['fechacreacion'] = $fechaCreacion;
                            $moviedata['espromocion'] = "";
                            $moviedata['ultimamodificacion'] = $ultimaModificacion;
                            $moviedata['orden'] = "0";
                            var_dump($moviedata);
                            die();
                            
                            $data = $em->getRepository("App:PeliculaTest")->getPeliculasByCodVista($movielist[$x]->Movie_strID);
                            if (!isset($data->nombre)){
                                $response = $this->forward('App\Controller\PeliculaTestController::createPelicula', $moviedata);
                                $nummovies++;
                            }else{
                                $response = $this->forward('App\Controller\PeliculaTestController::updatePelicula', $moviedata);
                                $nummovies++;
                            } 
                                                 
                        }
                    }                    
                }
            }
            $data = array("result"=>true, "response"=>"OK", "error"=>"", "data"=>"", "numero de peliculas procesadas"=>$nummovies);
        }else{
            $data = array("result"=>false, "response"=>"ERROR", "error"=>"Servicio de peliculas proximamente no responde", "data"=>"", "numero de peliculas procesadas"=>"0");
        }*/
        return $http->responseAcceptHttp(array("data"=>$data));
    }
    
    
    /**
     * @Rest\Get("/api/datasynccontroller/dosyncsinec/", name="synccontrollerdosyncsinec", defaults={"_format":"json"})
     * @SWG\Response(
     *     response=200,
     *     description="Sincroniza programacion de los complejos PROPACINE desde sinec"
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de DoSyncSinec"
     * )
     *
     * @SWG\Tag(name="DataSyncControllerDoSyncSinec")
     */    
    public function doSyncSinecAction(){
    /*    $arrayFecha = array();
        $fechaEstreno = "";
        $x = 0;
        $nummovies = 0;
        $ultimaModificacion = "";
        $fechaCreacion = "";
        $mes = "";
        $phpdate = "";
        $response = null;
        $response2 = null;
        $datalist = null;
        $arrayPeliculas = array();
        $nombreSinFormato = "";
        $nombreOriginal = "";
        $referencia = "";
        $version = "";
        $censura = "";
        $genero = "";
        $director = "";
        $elenco = "";
        $duracion = "";
        $data = array();
        $datarev = array();
        $json_content = array();
        $http = $this->get('httpcustom');
        $complejoPropacine = array("39", "32", "34", "33", "36", "31", "30", "35", "38", "37", "2", "3");
        for ($y = 0; $y < sizeof($complejoPropacine); $y++){           
            $response2 = $this->getSinecMovieListAction($complejoPropacine[$y]);
            if ($response2 != ""){
                $d=$response2->getContent();
                $json_content = (array)json_decode($d);
                if ($json_content["result"] == "OK"){
                    for ($x = 0; $x < sizeof($json_content["response"]); $x++){
                        $em = $this->getDoctrine()->getManager();
                        $datarev = $em->getRepository("App:PeliculaTest")->getPeliculasByCodSinec($json_content["response"][$x]->co_pelicula);
                        if ($datarev == null){
                            $response = $this->forward('App\Controller\PeliculaTestController::createPelicula', $datarev);
                            $nummovies++;
                        }else{
                            $response = $this->forward('App\Controller\PeliculaTestController::updatePelicula', $datarev);
                            $nummovies++;
                        }
                    }
                }           
            }
        }
        if ($data == ""){
            $data = array("result"=>true, "response"=>"OK", "error"=>"", "data"=>"", "numero de peliculas procesadas"=>$nummovies);
        }else{
            $data = array("result"=>false, "response"=>"ERROR", "error"=>"Servicio de peliculas proximamente no responde", "data"=>"", "numero de peliculas procesadas"=>"0");
        }    */    
        return $http->responseAcceptHttp(array("data"=>$data));
    }
    
    
    /**
     * @Rest\Get("/api/datasynccontroller/dosyncvista", name="synccontrollerdosyncvista", defaults={"_format":"json"})
     * @SWG\Response(
     *     response=200,
     *     description="peliculas y programaciones sincronizadas desde VISTA"
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de doSyncVista"
     * )
     *
     * @SWG\Tag(name="DataSyncControllerDoSyncVista")
     */    
    public function doSyncVistaAction(){
     /*   $response = "";
        $response2 = "";
        $response3 = "";
        $movielist = array();
        $moviedata = array();
        $movieinfo = array();
        $moviepeople = array();
        $d = "";
        $json_content = array();
        $dd = "";
        $json_content2 = array();
        $ddd = "";
        $json_content3 = array();        
        $director = "";
        $error = false;
        $elenco = "";
        $nummovies = 0;
        $complejos = array();
        $http = $this->get('httpcustom');
        $fechaCreacion = date("Y-m-d H:m:s");
        $ultimaModificacion = date("Y-m-d H:m:s");
        $em = $this->getDoctrine()->getManager();
        $complejos = $em->getRepository("App:Complejo")->getcomplejos();
        for ($w = 0; $w < sizeof($complejos); $w++){
            if ($complejos[$w]->esproductivo == "Y"){
                $response = $this->forward('App\Controller\CinexDataServiceController::getMovieListAction', array("cinemaid"=>$complejos[$w]->id));
                if ($response != ""){
                    $d=$response->getContent();
                    $json_content = (array)json_decode($d);
                }
                if ($json_content["result"] == "OK"){
                    $movielist = $json_content["response"][0]->Table;
                    for ($x = 0; $x < sizeof($movielist); $x++){
                        $response2 = $this->forward('App\Controller\CinexDataServiceController::getMovieinfoListAction', array("moviename"=>$movielist[$x]->Movie_strName));
                        if ($response2 != ""){
                            $dd=$response2->getContent();
                            $json_content2 = (array)json_decode($dd);
                        }
                        if ($json_content2["result"] == "OK"){
                            $movieinfo = $json_content2["response"][0]->Table;
                            $response3 = $this->forward('App\Controller\CinexDataServiceController::getMoviePeopleAction', array("movieid"=>$movielist[$x]->Movie_strID));
                            if ($response3 != ""){
                                $ddd=$response3->getContent();
                                $json_content3 = (array)json_decode($ddd);
                            }
                            $json_content3 = (array)json_decode($ddd);
                            if ($json_content3["result"] == "OK"){
                                $moviepeople = $json_content3["response"][0]->Table;
                                for ($y = 0; $y < sizeof($moviepeople); $y++){
                                    if ($moviepeople[$y]->FPerson_strType == "D"){
                                        $director = $moviepeople[$y]->Person_strFirstName." ".$moviepeople[$y]->Person_strLastName;
                                    }else if ($moviepeople[$y]->FPerson_strType == "A"){
                                        $elenco = $moviepeople[$y]->Person_strFirstName." ".$moviepeople[$y]->Person_strLastName;
                                    }
                                }
                                $moviedata['nombresinformato'] = substr($movielist[$x]->Movie_strName, 0, strpos($movielist[$x]->Movie_strName, "(")-1);
                                $moviedata['nombreoriginal'] = $movielist[$x]->Movie_strName;
                                $moviedata['referencia'] = $this->apiUtilFunctions->sanear_string2(substr($movielist[$x]->Movie_strName, 0, strpos($movielist[$x]->Movie_strName, "(")-1));
                                $moviedata['censura'] = $movielist[$x]->Movie_strRating;
                                $moviedata['genero'] = $this->apiUtilFunctions->getGenero($movieinfo->FilmCat_strName);
                                $moviedata['fechaestreno'] = substr($movieinfo->Film_dtmOpeningDate, 0, 10);
                                $moviedata['urltrailer'] = "";
                                $moviedata['urlposter1'] = "";
                                $moviedata['urlposter2'] = "";
                                $moviedata['codvista'] = $movielist[$x]->Movie_strID;
                                $moviedata['codsinec'] = $movielist[$x]->Movie_HOFilmCode;
                                $moviedata['versiones'] = $this->apiUtilFunctions->getFormatFromMovieName($movielist[$x]->Movie_strName);
                                $moviedata['director'] = $director;
                                $moviedata['elenco'] = $elenco;
                                $moviedata['tipo'] = "";
                                $moviedata['duracion'] = $movieinfo->Film_intDuration;
                                $moviedata['descripcion'] = $this->apiUtilFunctions->sanear_string2($movieinfo->Film_strDescriptionLong);
                                $moviedata['contenido'] = $this->apiUtilFunctions->sanear_string2($movieinfo->Film_strContent);
                                $moviedata['urlposter3'] = "";
                                $moviedata['urlposter4'] = "";
                                $moviedata['esdestacada'] = "";
                                $moviedata['espreventa'] = "";
                                $moviedata['esvenezolana'] = "";
                                $moviedata['fechacreacion'] = $fechaCreacion;
                                $moviedata['espromocion'] = "";
                                $moviedata['ultimamodificacion'] = $ultimaModificacion;
                                $moviedata['orden'] = "0";
                                $data = $em->getRepository("App:PeliculaTest")->getPeliculasByCodVista($movielist[$x]->Movie_strID);
                                if (!isset($data->nombre)){
                                    $response = $this->forward('App\Controller\PeliculaTestController::createPelicula', $moviedata);
                                    $nummovies++;
                                }else{
                                    $response = $this->forward('App\Controller\PeliculaTestController::updatePelicula', $moviedata);
                                    $nummovies++;
                                }
                            }else{
                                $error = true;
                            }
                        }else{
                            $error = true;
                        }
                    }
                }else{
                    $error = true;
                }                
            }
        }
        if (!$error){
            $data = array("result"=>true, "response"=>"OK", "error"=>"", "data"=>"", "numero de peliculas procesadas"=>$nummovies);
        }else{
            $data = array("result"=>false, "response"=>"ERROR", "error"=>"Servicio de peliculas proximamente no responde", "data"=>"", "numero de peliculas procesadas"=>"0");
        }*/
        return $http->responseAcceptHttp(array("data"=>$data));        
    }
    
    
    
        
    /**
     * Parsea el XML devueldo del objeto NUSOAP
     * @param array $dataResponse XMl de Xista .
     * @return response.
     ***/
    public function prepareHttpResponse($dataResponse){
        $data=[];
        $dataVista=[];
        $x = 0;
        foreach ($dataResponse as $clave => $valor){
            if ($valor != "OK" && $x == 0){
                $dataVista= json_encode(array("result"=>false,"error"=>true, "response"=>"Error en la llamada getCinemaList ".$valor));
            }else if ($x == 1){
                $dataVista = json_encode(array("result"=>true,"error"=>false, "response"=>json_decode(json_encode(simplexml_load_string(trim(utf8_encode($valor)))),TRUE)));
            }
            $x++;
        }
        $dataVista = json_decode($dataVista);
        if ($dataVista->response){
            $data["result"]=$dataVista->result;
            $data["error"]=false;
            $data["response"]="OK";
            $data["data"]=array(json_decode(json_encode((array)$dataVista->response), TRUE));
        }else{
            $data["result"]=false;
            $data["error"]=true;
            $data["response"]=$dataVista;
            $data["data"]=false;
        }
        return $data;
    }
    
    
    /**
     * Parsea el XML devueldo del objeto NUSOAP
     * @param array $dataResponse XMl de Xista .
     * @return response.
     ***/
    public function prepareSinecHttpResponse($dataResponse){
        $data=[];
        if (count($dataResponse)>0){
            $data["result"]=true;
            $data["error"]=false;
            $data["response"]="OK";
            $data["data"]=$dataResponse;
        }else{
            $data["result"]=false;
            $data["error"]=true;
            $data["response"]=0;
            $data["data"]=false;
        }
        return $data;
    }
    
}
?>
