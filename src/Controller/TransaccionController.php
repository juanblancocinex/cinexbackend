<?php
namespace App\Controller;
use App\CoreCinex\CinexService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use App\Lib\DaoLib;
use App\Lib\ApiUtilFunctions;
use App\Lib\Constantes;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Translation\TranslatorInterface;
use App\Lib\XmlLib;
use App\Entity\Transaccion;


class TransaccionController extends Controller
{     
   

        /**
         * Constructor.
        **/
        public function __construct(ParameterBagInterface $params)
        {
            
        }
        
       
        //  Transaccion URI"'s
        /**
         * @Rest\Post("/api/transaccion/create", name="TransaccionCreate", defaults={"_format":"json"})
         * @SWG\Parameter(
         *          name="body",
         *          in="body",
         *          description="JSON Payload",
         *          required=true,
         *          type="json",
         *          format="application/json",
         *          @SWG\Schema(
         *              type="object",
         *              @SWG\Property(property="ordenid", type="string", example="WTM6DHK"),
         *              @SWG\Property(property="nombreentarjeta", type="string", example="Luis Santos"),
         *              @SWG\Property(property="celular", type="string", example="04247654322"),
         *              @SWG\Property(property="fecha", type="datetime", example="2016-01-01 01:35:24"),
         *              @SWG\Property(property="tarjeta", type="string", example="415467******9020"),
         *              @SWG\Property(property="tipotarjeta", type="string", example="VISA"),
         *              @SWG\Property(property="monto", type="string", example="724.00"),
         *              @SWG\Property(property="asientos", type="date", example="F8,F7,F6,F5"),
         *              @SWG\Property(property="programacionid", type="integer", example="2047652"),
         *              @SWG\Property(property="cedula", type="varchar", example="V9097236"),
         *              @SWG\Property(property="pelicula", type="varchar", example="LOS JÓVENES TITANES EN ACCIÓN DIG ESP"),
         *              @SWG\Property(property="hora", type="string", example="15:50"),
         *              @SWG\Property(property="complejo", type="string", example="TOLÓN"),
         *              @SWG\Property(property="mensaje", type="varchar", example="MEGASOFT: APROBADA"),
         *              @SWG\Property(property="sala", type="varchar", example="Sala 3"),
         *              @SWG\Property(property="cantidadboletos", type="integer", example="4"),
         *              @SWG\Property(property="fechafuncion", type="datetime", example="2016-01-01 01:35:24"),
         *              @SWG\Property(property="tecnologia", type="varchar", example="MOV"),
         *              @SWG\Property(property="correo", type="varchar", example="prueba@gmail.com"),
         *              @SWG\Property(property="vencashvoucher", type="varchar", example="AX2344"),
         *              @SWG\Property(property="bookingnumber", type="varchar", example="212090"),
         *              @SWG\Property(property="detalleboleto", type="varchar", example="2120DD"),
         *              @SWG\Property(property="vistamontoincent", type="integer", example="0"),
         *              @SWG\Property(property="porreversar", type="varchar", example="N"),
         *              @SWG\Property(property="causa", type="varchar", example="AX2344"),
         *              @SWG\Property(property="respuesta", type="varchar", example="AX2344"),
         *              @SWG\Property(property="montompfull", type="varchar", example="AX2344"),
         *              @SWG\Property(property="serviciompfull", type="varchar", example="AX2344"),
         *              @SWG\Property(property="voucher", type="text", example="ORIGINAL-COMERCIO"),
         *              @SWG\Property(property="tipotrans", type="varchar", example="1"),
         *              @SWG\Property(property="membercard", type="varchar", example="1AX2344"),
         *              @SWG\Property(property="vistavoucher", type="varchar", example="1AX2344"),
         *              @SWG\Property(property="usersessionid", type="varchar", example="1cadb7bc9ca8a223"),
         *              @SWG\Property(property="codigocomplejo", type="varchar", example="MPP"),
         *              @SWG\Property(property="codigopelicula", type="varchar", example="22214"),
         *              @SWG\Property(property="authid", type="text", example="AX2344"),
         *              @SWG\Property(property="memberid", type="varchar", example="AX2344"),
         *              @SWG\Property(property="giftnombre", type="varchar", example="AX2344"),
         *              @SWG\Property(property="giftcorreo", type="varchar", example="AX2344"),
         *              @SWG\Property(property="giftmensaje", type="text", example="AX2344"),
         *              @SWG\Property(property="giftimage", type="integer", example="2344"),
         *              @SWG\Property(property="giftcinta", type="integer", example="2344"),
         *              @SWG\Property(property="giftde", type="varchar", example="AX2344")
         *          )
         *      )
         * @SWG\Response(
         *     response=200,
         *     description="Transaccion Creada."
         * )
         * @SWG\Response(
         *     response=500,
         *     description="Ocurrio un error al ejecutar la creación de usuario"
         * )
         *
         * @SWG\Tag(name="TransaccionCreate")
         */

        public function createTransaccionAction(Request $request) {
            $http = $this->get('httpcustom');
            $data= [];
            $transaccion = new Transaccion();
            $em = $this->getDoctrine()->getManager();
            $ordenId = $request->request->get('ordenid');
            $nombreEnTarjeta =$request->request->get('nombreentarjeta');           
            $celular =$request->request->get('celular'); 
            $fecha =$request->request->get('fecha');   
            $tarjeta =$request->request->get('tarjeta');   
            $tipotarjeta =$request->request->get('tipotarjeta');   
            $monto =$request->request->get('monto');   
            $asientos =$request->request->get('asientos');   
            $programacionId =$request->request->get('programacionid');   
            $cedula =$request->request->get('cedula');   
            $causa =$request->request->get('causa');
            $respuesta =$request->request->get('respuesta');  
            $pelicula =$request->request->get('pelicula');   
            $hora =$request->request->get('hora');   
            $complejo =$request->request->get('complejo');   
            $mensaje =$request->request->get('mensaje');   
            $sala =$request->request->get('sala'); 
            $cantidadBoletos =$request->request->get('cantidadboletos');   
            $fechaFuncion =$request->request->get('fechafuncion');   
            $tecnologia =$request->request->get('tecnologia');   
            $correo =$request->request->get('correo');   
            $vencashVoucher =$request->request->get('vencashvoucher');   
            $bookingNumber =$request->request->get('bookingnumber');   
            $detalleBoleto =$request->request->get('detalleboleto');   
            $vistaMontoIncent =$request->request->get('vistamontoincent');   
            $porReversar =$request->request->get('porreversar');   
            $montoMpFull =$request->request->get('montompfull');   
            $servicioMpFull =$request->request->get('serviciompfull');   
            $voucher =$request->request->get('voucher');   
            $tipoTrans =$request->request->get('tipotrans');   
            $memberCard =$request->request->get('membercard');   
            $vistaVoucher =$request->request->get('vistavoucher');   
            $userSessionId =$request->request->get('usersessionid');   
            $codigoComplejo =$request->request->get('codigocomplejo');   
            $codigoPelicula =$request->request->get('codigopelicula');   
            $authId =$request->request->get('authid');  
            $memberId =$request->request->get('memberid'); 
            $giftNombre =$request->request->get('giftnombre');    
            $giftCorreo =$request->request->get('giftcorreo'); 
            $giftMensaje =$request->request->get('giftmensaje'); 
            $giftImage =$request->request->get('giftimage'); 
            $giftCinta =$request->request->get('giftcinta'); 
            $giftDe =$request->request->get('giftde'); 
            $transaccion->setRespuesta(isset($respuesta)?$respuesta:null); 
            $transaccion->setOrdenid(isset($ordenId)?$ordenId:null);
            $transaccion->setNombreentarjeta(isset($nombreEnTarjeta)?$nombreEnTarjeta:null);
            $transaccion->setCelular(isset($celular)?$celular:null);
            $transaccion->setFecha(isset($fecha)? new \Datetime($fecha):null);
            $transaccion->setTarjeta(isset($tarjeta)?$tarjeta:null);
            $transaccion->setTipotarjeta(isset($tipotarjeta)?$tipotarjeta:null);
            $transaccion->setMonto(isset($monto)?$monto:null);
            $transaccion->setAsientos(isset($asientos)?$asientos:null);
            $transaccion->setProgramacionid(isset($programacionId)?$programacionId:null);
            $transaccion->setCedula(isset($cedula)?$cedula:null);
            $transaccion->setPelicula(isset($pelicula)?$pelicula:null);
            $transaccion->setHora(isset($hora)?$hora:null);
            $transaccion->setComplejo(isset($complejo)?$complejo:null);
            $transaccion->setMensaje(isset($mensaje)?$mensaje:null);
            $transaccion->setSala(isset($sala)?$sala:null);
            $transaccion->setCantidadBoletos(isset($cantidadBoletos)?$cantidadBoletos:null);
            $transaccion->setFechafuncion(isset($fechaFuncion)?new \Datetime($fechaFuncion):null);
            $transaccion->setTecnologia(isset($tecnologia)?$tecnologia:null);
            $transaccion->setVencashvoucher(isset($vencashVoucher)?$vencashVoucher:"");
            $transaccion->setBookingnumber(isset($bookingNumber)?$bookingNumber:null);
            $transaccion->setDetalleboleto(isset($detalleBoleto)?$detalleBoleto:"");
            $transaccion->setVistamontoincent(isset($vistaMontoIncent)?$vistaMontoIncent:0);
            $transaccion->setPorreversar(isset($porReversar)?$porReversar:null);
            $transaccion->setMontompfull(isset($montoMpFull)?$montoMpFull:"");
            $transaccion->setServiciompfull(isset($servicioMpFull)?$servicioMpFull:"");
            $transaccion->setVoucher(isset($voucher)?$voucher:null);
            $transaccion->setTipotrans(isset($tipoTrans)?$tipoTrans:null);
            $transaccion->setMembercard(isset($memberCard)?$memberCard:"");
            $transaccion->setVistavoucher(isset($vistaVoucher)?$vistaVoucher:"");
            $transaccion->setUsersessionid(isset($userSessionId)?$userSessionId:null);
            $transaccion->setCodigocomplejo(isset($codigoComplejo)?$codigoComplejo:null);
            $transaccion->setCodigopelicula(isset($codigoPelicula)?$codigoPelicula:null);
            $transaccion->setAuthid(isset($authId)?$authId:null);
            $transaccion->setMemberid(isset($memberId)?$memberId:null);
            $transaccion->setGiftnombre(isset($giftNombre)?$giftNombre:"");
            $transaccion->setGiftcorreo(isset($giftCorreo)?$giftCorreo:"");
            $transaccion->setGiftmensaje(isset($giftMensaje)?$giftMensaje:"");
            $transaccion->setGiftimagen(isset($giftMensaje)?$giftMensaje:0);
            $transaccion->setGiftcinta(isset($giftCinta)?$giftCinta:0);
            $transaccion->setGiftde(isset($giftDe)?$giftDe:"");
            $transaccion->setCausa(isset($causa)?$causa:"");
            $transaccion->setCorreo(isset($correo)?$correo:"");
            $em->persist($transaccion);
            $em->flush();
            return $http->responseAcceptHttp(array("data"=>"Transaccion Creada Con Exito"));
            
        }

        /**
     * @Rest\Get("/api/transaccion/{cedula}", name="gettransaccion", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="cedula",
     *     in="path",
     *     type="integer",
     *     description="Cedula del Usuario"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Get Transaccion."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Transaccion."
     * )
     * @SWG\Tag(name="TransaccionGetDataTable")
     */
    public function getTransaccionAction($cedula) {
        $apiFuntion = new ApiUtilFunctions();
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $data= [];
        $data = $em->getRepository("App:Transaccion")->getTransaccion($cedula);
        if (is_null($data)) {
            $data = [];
        }
        $parametros=array("data"=>$data);

  
        return $http->responseAcceptHttpWithoutFormat($parametros);
        
    } 


         /**
     * @Rest\Get("/api/transaccion/detalle/{detalle}", name="gettransacciondetalle", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="transaccion",
     *     in="path",
     *     type="integer",
     *     description="Transaccion"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Get Transaccion Detalle."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Detalle deTransaccion."
     * )
     * @SWG\Tag(name="TransaccionDetalle")
     */
    public function getTransaccionDetalleAction($detalle) {
        $apiFuntion = new ApiUtilFunctions();
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $data= [];
        $data = $em->getRepository("App:Transaccion")->getTransaccionDetalle($detalle);
        if (is_null($data)) {
            $data = [];
        }else{
            $dataComplejos = $em->getRepository("App:Complejo")->getDetallesComplejo($data[0]["codigoComplejo"]);
            if(count($dataComplejos)>0){
                $data[0]["codigoComplejo"]=$dataComplejos[0]["nombre"];
            }   
            
            $dataPelicula= $em->getRepository("App:PeliculaTest")->getPeliculasByNombre($data[0]["pelicula"]);
            if(count($dataPelicula)>0){
                $data[0]["pelicula"]= $dataPelicula[0]["referencia"];
            }    
           // var_dump($dataPelicula);
            


       /* $sql = "SELECT p.pelicula, p.codigoComplejo, p.sala,
                       p.fechaFuncion, p.cantidad_boletos,
                       p.asientos, p.ordenID, 
                       p.hora, p.respuesta, s.nombreComplejo,p1.versiones
                       FROM programacion_test t JOIN pelicula_test p1 RIGHT JOIN transaccion p  
                        ON (t.codigoPelicula = p1.codSinec
                        OR t.codigoPelicula = p1.codVista) AND (p.programacionID = t.programacionID)
                        LEFT JOIN complejo s ON p.codigoComplejo = s.codigoComplejo 
        WHERE p.transaccionID = '".$idtransaccion."' order by p.fecha desc";*/
        
        }
        $parametros=array("data"=>$data);
        return $http->responseAcceptHttpWithoutFormat($parametros);
        
    } 
}
