<?php

namespace App\Controller;
use App\CoreCinex\CinexService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use App\Lib\DaoLib;
use App\Lib\ApiUtilFunctions;
use App\Lib\Constantes;
use App\Entity\VouchersEnviados;


class VouchersEnviadosController extends Controller
{   
    // Obtiene información de a que usuario fue enviado un cinexpass URI"'s
    /**
     * @Rest\Get("/api/vouchersdigitalesenviados/codigo/{codvoucher}", name="getCinexPassSendedInfo", defaults={"_format":"json"})
     *
     * @SWG\Parameter(
     *     name="codvoucher",
     *     in="path",
     *     type="string",
     *     description="codigo voucher"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Voucher Digitales."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Voucher Enviados."
     * )
     * @SWG\Tag(name="getCinexPassSendedInfo")
     */
    public function getCinexPassSendedInfoAction($codvoucher) {
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $data= [];
        $data = $em->getRepository("App:VouchersEnviados")->getCinexPassSendedInfo($codvoucher);
        if (is_null($data)) {
            $data = [];
        }
        return $http->responseAcceptHttp(array("data"=>$data));
    }


     
        //  Transaccion URI"'s
        /**
         * @Rest\Post("/api/vouchersdigitalesenviados/create", name="postCinexPassCreate", defaults={"_format":"json"})
         * @SWG\Parameter(
         *          name="body",
         *          in="body",
         *          description="JSON Payload",
         *          required=true,
         *          type="json",
         *          format="application/json",
         *          @SWG\Schema(
         *              type="object",
         *              @SWG\Property(property="cinexpasslist", type="string", example="91352003024484062|913520030332840222"),
         *              @SWG\Property(property="usuario", type="string", example="Luis Santos"),
         *              @SWG\Property(property="email", type="string", example="lbaez@mgallina.com"),
         *              @SWG\Property(property="sender", type="string", example="Pedro Perez"),
         *          )
         *      )
         * @SWG\Response(
         *     response=200,
         *     description="Tickes Enviados."
         * )
         * @SWG\Response(
         *     response=500,
         *     description="Ocurrio un error al ejecutar la creación de usuario"
         * )
         *
         * @SWG\Tag(name="postCinexPassCreate")
         */

        public function createTicketsEnviadosAction(Request $request, \Swift_Mailer $mailer) {
            $http = $this->get('httpcustom');
            $data= [];
           
            $em = $this->getDoctrine()->getManager();
            $cinexpasslist = $request->request->get('cinexpasslist');
            $usuario =$request->request->get('usuario');           
            $email =$request->request->get('email');   
            $sender =$request->request->get('sender'); 
            $arrayTickets= explode("|",$cinexpasslist);
            $listCinePassHmtl="";
            foreach($arrayTickets as $valor){
                $vouchersEnviados = new VouchersEnviados();
                $vouchersEnviados->setCodigoVoucher($valor);
                $vouchersEnviados->setNombresapellidos($usuario);
                $vouchersEnviados->setCorreoelectronico($email);
                $listCinePassHmtl.= $valor."<BR>";
                $em->persist($vouchersEnviados);
                $em->flush();
            }
            $json = $em->getRepository("App:Plantilla")->getPlantillasByType("-1");        
            $plantilla = $json[0]["contenido"];

            $plantilla = str_replace("#CINEXPASSLIST", $listCinePassHmtl, $plantilla);
            $plantilla = str_replace("#NOMBREUSUARIO", $usuario, $plantilla);
            $plantilla = str_replace("#SENDER", $sender, $plantilla);
            $plantilla = str_replace("#EMAIL", $email, $plantilla);

            $message = (new \Swift_Message('CINEXPASS'))
            ->setFrom('cinex@cinex.com.ve')
            ->setTo($email)        
            ->setBody($plantilla, 'text/html')
            ;
            $result1 = $mailer->send($message);
            
            if ($result1 != null){
                $data = "Tickes Digitales enviados con exito";
            }else{
                $data = "Error enviando Tickets Digitales";
            }
            return $http->responseAcceptHttp(array("data"=>$data));
            
        }


    // Tickest CinexPass Enviados URI"'s
    /**
     * @Rest\Get("/api/vouchersdigitalesenviadosporusuario/cedula/{ci}/formato/{formato}", name="getCinexPassEnviadosPorUsuario", defaults={"_format":"json"})
     *
   * @SWG\Parameter(
     *     name="ci",
     *     in="path",
     *     type="string",
     *     description="cedula del usuario"
     * )
     * @SWG\Parameter(
     *     name="formato",
     *     in="path",
     *     type="string",
     *     description="formato de la sesion"
     * ) 
     * @SWG\Response(
     *     response=200,
     *     description="Voucher Digitales."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Voucher Digitales Enviados por Usuario."
     * )
     * @SWG\Tag(name="getCinexPassEnviadosPorUsuario")
     */
    public function getCinexPassEnviadosPorUsuarioAction($ci, $formato) {
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $data= [];
        $data = $em->getRepository("App:VouchersEnviados")->getVouchersDigitalesEnviadosPorUsuario($ci, $formato);
        if (is_null($data)) {
            $data = [];
        }
        return $http->responseAcceptHttp(array("data"=>$data));
    }

}