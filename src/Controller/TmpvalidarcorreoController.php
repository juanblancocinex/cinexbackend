<?php

namespace App\Controller;
use App\CoreCinex\CinexService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use App\Lib\DaoLib;
use App\Lib\ApiUtilFunctions;
use App\Lib\Constantes;
use App\Entity\Tmpvalidarcorreo;

class TmpvalidarcorreoController extends Controller
{
 
    /**
     * @Rest\Get("/api/tmpvalidarcorreo/exist/{correo}", name="existEmail", defaults={"_format":"json"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="El correo si existe"
     * )
     * 
     * @SWG\Response(
     *     response=204,
     *     description="El correo no se encuentra en la base de datos"
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Error realizando la consulta, valide con el administrador"
     * )
     * @SWG\Parameter(
     *     name="correo",
     *     in="path",
     *     type="string",
     *     description="Correo Electronico"
     * )
     *
     * @SWG\Tag(name="ValidarCorreo")
     */
    public function getExistMailAction($correo) {
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $data= [];
        //$clave = sha1($clave);
        $data = $em->getRepository("App:Tmpvalidarcorreo")->getExist($correo);
        
        if (count($data)==0) {
            return $http->responseAcceptHttp(array("data"=>$data));
        }else{
            return $http->responseAcceptHttp(array("data"=>$data));
        }
    }

    /**
     * @Rest\Post("/api/tmpvalidarcorreo/registrar", name="registrarTmpCorreo", defaults={"_format":"json"})
     * @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          type="json",
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="nombres", type="string", example="Pedro"),
     *              @SWG\Property(property="apellidos", type="string", example="Perez"),
     *              @SWG\Property(property="tipo_documento", type="string", example="V, E, P"),
     *              @SWG\Property(property="n_documento", type="string", example="14512457"),
     *              @SWG\Property(property="correo_electronico", type="string", example="pperez@cinex.com.ve")
     *          )
     *      )
     * @SWG\Response(
     *     response=200,
     *     description="Correo Temporal Registrado"
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar el registro de usuario temporal"
     * )
     *
     * @SWG\Tag(name="Registrar TmpCorreo")
     */
        
    public function registrarTmpCorreoAction(Request $request){
        $http = $this->get('httpcustom');
        $mailer = $this->get('mailer');
        $em = $this->getDoctrine()->getManager();

        $nombres = $request->request->get('nombres');
        $apellidos = $request->request->get('apellidos');
        $tipo_documento = $request->request->get('tipo_documento');
        $n_documento = $request->request->get('n_documento');
        $correo_electronico = $request->request->get('correo_electronico');

        $cod = base64_encode($request->request->get('correo_electronico'));

        $tmpCorreo = new Tmpvalidarcorreo();
        $tmpCorreo->setNom($nombres);
        $tmpCorreo->setApe($apellidos);
        $tmpCorreo->setLetra($tipo_documento);
        $tmpCorreo->setCi($n_documento);
        $tmpCorreo->setEmail($correo_electronico);
        $em->persist($tmpCorreo);
        $em->flush();


        return $http->responseAcceptHttp(array("data"=>$cod));
    }

    /**
         * @Rest\Delete("/api/tmpvalidarcorreo/eliminar/{email}", name="deleteTmpCorreo", defaults={"_format":"json"})
         *
         * @SWG\Parameter(
         *     name="email",
         *     in="path",
         *     type="string",
         *     description="correo del usuario"
         * )
         
         * @SWG\Response(
         *     response=200,
         *     description="Datos eliminados"
         * )
         * @SWG\Response(
         *     response=500,
         *     description="Ocurrio un error al eliminar el registro"
         * )#
         *
         * @SWG\Tag(name="deleteTmpCorreo")
         */
        
        public function delete($email){
            $em = $this->getDoctrine()->getManager();
            $http = $this->get('httpcustom');
            $user = $em->getRepository("App:Tmpvalidarcorreo")->getExist($email);
            
            $em->remove($user[0]);
            $em->flush();
            
            $data = array("result"=>true, "response"=>"OK");
            return $http->responseAcceptHttp(array("data"=>$data));
        }
}
