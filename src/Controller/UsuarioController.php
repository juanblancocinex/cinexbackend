<?php

namespace App\Controller;
use App\CoreCinex\CinexService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use App\Lib\DaoLib;
use App\Lib\ApiUtilFunctions;
use App\Lib\Constantes;
use App\Entity\Usuario;

class UsuarioController extends Controller
{
     protected $params;

    // USER URI's
 
    /**
     * @Rest\Get("/api/usuario/{usuario}/{clave}", name="getuser", defaults={"_format":"json"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Usuario encontrado."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Usuarios."
     * )
     *
     * @SWG\Parameter(
     *     name="usuario",
     *     in="path",
     *     type="string",
     *     description="Correo Electronico"
     * )
     *
     * @SWG\Parameter(
     *     name="clave",
     *     in="path",
     *     type="string",
     *     description="Password"
     * )
     *
     *
     * @SWG\Tag(name="Usuario")
     */
    public function getUserAction(Request $request,$usuario,$clave) {
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $data= [];
        $clave = sha1($clave);
        $data = $em->getRepository("App:Usuario")->getUsuarioFindByCorreoClave([
            "correoelectronico" => $usuario,
            "clave"=>$clave
        ]);
        if (count($data)==0) {
            $message = "Estimado usuario, los datos registrados no son correctos, 
            por favor verifique el correo proporcionado y 
            tenga en cuenta que la clave diferencia mayusculas de minusculas";
            return $http->responseAcceptHttp(array("data"=>array("idUser"=>"0","mensaje"=>$message)));
        }else{
            $data= $data[0]["usuarioid"];
            $message = "Operacion finalizada exitosamente";
            return $http->responseAcceptHttp(array("data"=>array("idUser"=>$data,"mensaje"=>$message)));
        }
    }
    
    
    /**
     * @Rest\Get("/api/usuario/{email}/", name="getusersessionidbyemail", defaults={"_format":"json"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="UseSessionId del usuario con el correo indicado."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Usuarios."
     * )
     *
     * @SWG\Parameter(
     *     name="email",
     *     in="path",
     *     type="string",
     *     description="Correo Electronico"
     * )
     *
     * @SWG\Tag(name="GetUserSessionIdByEmail")
     */
    public function getUserSessionIdByEmailAction($email) {
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $data= [];
        $data = $em->getRepository("App:Usuario")->getUserVistaIdFindByCorreo($email);
        if (count($data)==0) {
            $data = "Estimado usuario, los datos registrados no son correctos,
            por favor verifique el correo proporcionado.";
        }else{
            $data= $data[0]["uservistaid"];
        }
        return $http->responseAcceptHttp(array("data"=>$data));
    }
    
 
// USER URI's
    /**
     * @Rest\Get("/api/usuario/info/getusuarioinfobyemail/{email}", name="getuserinfobyemail", defaults={"_format":"json"})
    * @SWG\Parameter(
     *     name="email",
     *     in="path",
     *     type="string",
     *     description="Correo Electronico"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Información del Usuario encontrado."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de GetUserInfoByEmail."
     * )
     *
     * @SWG\Tag(name="GetUserInfoByEmail")
     */
    public function getUserInfoByEmailAction($email) {
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $data= [];
        $data = $em->getRepository("App:Usuario")->getUsuarioInfoFindByEmail($email);
        if (count($data)==0) {
            $data = "Usuario no encontrado";
        }else{
            $data= $data[0];
        }
        return $http->responseAcceptHttp(array("data"=>$data));
    }
 
    /**
     * @Rest\Get("/api/usuario/info/getusuarioinfobyci/{ci}", name="getuserinfobyci", defaults={"_format":"json"})
    * @SWG\Parameter(
     *     name="ci",
     *     in="path",
     *     type="string",
     *     description="Cedula"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Información del Usuario encontrado."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de GetUserInfoByCI."
     * )
     *
     * @SWG\Tag(name="GetUserInfoByCI")
     */
    public function getUserInfoByCIAction($ci) {
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $data= [];
        $data = $em->getRepository("App:Usuario")->getUsuarioInfoFindByCI($ci);
        if (count($data)==0) {
            $data = "Usuario no encontrado";
        }else{
            $data= $data[0];
        }
        return $http->responseAcceptHttp(array("data"=>$data));
    }


  // USER URI's
    /**
     * @Rest\Get("/api/usuario/getusuariobyid/usuarioid/{usuarioid}", name="getuserbyid", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="usuarioid",
     *     in="path",
     *     type="string",
     *     description="Usuario Id"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Información del Usuario encontrado."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de GetUserById."
     * )
     *
     * @SWG\Tag(name="GetUserById")
     */
    public function getUserByIdAction($usuarioid) {
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $data= [];
        $data = $em->getRepository("App:Usuario")->getUsuarioFindByUsuarioid($usuarioid);
        if (count($data)==0) {
            $data = "Usuario no encontrado";
        }else{
            $data= $data[0];
        }
        return $http->responseAcceptHttp(array("data"=>$data));
    }

    // USER URI's
    /**
     * @Rest\Get("/api/usuario/getUserDataById/userId/{userId}", name="getUserDataById", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="userId",
     *     in="path",
     *     type="string",
     *     description="Usuario Id"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Información del Usuario encontrado."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de getUserDataById."
     * )
     *
     * @SWG\Tag(name="GetUserDataById")
     */
    public function getUserDataByIdAction($userId) {
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $data= [];
        $data = $em->getRepository("App:Usuario")->getUserDataById($userId);
        //var_dump($data);die;
        if (count($data)==0) {
            $data = "Usuario no encontrado";
        }else{
            $data= $data[0];
        }
        return $http->responseAcceptHttp(array("data"=>$data));
    }
 
    /**
     * @Rest\Post("/api/usuario/registrar", name="registraruser", defaults={"_format":"json"})
     * @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          type="json",
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="correoElectronico", type="string", example="pperez@gmail.com"),
     *              @SWG\Property(property="password", type="string", example="123456"),
     *              @SWG\Property(property="nombre", type="string", example="Pedro"),
     *              @SWG\Property(property="apellido", type="string", example="Perez"),
     *              @SWG\Property(property="recibir_info", type="string", example="Y"),
     *              @SWG\Property(property="CI", type="string", example="V14512457"),
     *              @SWG\Property(property="fecha_nac", type="string", example="1980-05-25"),
     *              @SWG\Property(property="sexo", type="string", example="F"),
     *              @SWG\Property(property="direccion", type="date", example="Avenida los Jabillos"),
     *              @SWG\Property(property="celular", type="integer", example="04244512452"),
     *              @SWG\Property(property="ciudad", type="varchar", example="CARACAS"),
     *              @SWG\Property(property="telefono", type="varchar", example="02124541242"),
     *              @SWG\Property(property="usuario", type="varchar", example="********"),
     *              @SWG\Property(property="cinexclusivo", type="varchar", example="9009123412341234")
     *          )
     *      )
     * @SWG\Response(
     *     response=200,
     *     description="Usuario Registrado"
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar el registro de usuario"
     * )
     *
     * @SWG\Tag(name="RegistrarUser")
     */
        
    public function registrarUserAction(Request $request, UserPasswordEncoderInterface $encoder){
        $http = $this->get('httpcustom');
        $mailer = $this->get('mailer');
        $em = $this->getDoctrine()->getManager();
        $email= $request->request->get('correoElectronico');
        $nombre= $request->request->get('nombre');
        $password = $request->request->get('password');
        $apellido= $request->request->get('apellido');
        $info= $request->request->get('recibir_info');
        $ci= $request->request->get('CI');
        $fechanac= $request->request->get('fecha_nac');
        $sexo= $request->request->get('sexo');
        $direccion= $request->request->get('direccion');
        $celular= $request->request->get('celular');
        $ciudad= $request->request->get('ciudad');
        $telefono= $request->request->get('telefono');
        $username = $request->request->get('usuario');
        $cinexclusivo = $request->request->get('cinexclusivo');

        /**
         * Algoritmo para creacion de UserVistaID
         */
        $p= $email.';'.$password.';'.$nombre.';'.$apellido.';'.$info.';'.$ci.';'.$fechanac.';'.$sexo.';'.$direccion.';'.$celular.';1;'.$telefono.';'.$cinexclusivo;

        $dimemen = str_replace('+', ' ', $p);

        $valoren=sha1($dimemen);
        $i =0;

        for ($i = 1; ; $i++) {
            if ($i > 7) {
                break;
            }
            $valoren=sha1($valoren);
        }

        $userVistaId = substr($valoren, 0, 25);

        /**
         * Fin de algoritmo para creacion de UserVistaID
         */

        $user = new Usuario();
        $user->setUservistaid($userVistaId);
        $user->setCorreoelectronico($email);
        $user->setPlainPassword($password);
        $user->setPassword($encoder->encodePassword($user,$password));
        $user->setUsername($username);
        $user->setNombre($nombre);
        $user->setApellido($apellido);
        $user->setRecibirinformacion($info);
        $user->setCi($ci);
        $user->setNumcelular($celular);
        $user->setNumlocal($telefono);
        $user->setFechanacimiento(\DateTime::createFromFormat('d/m/Y', $fechanac));
        $user->setSexo($sexo);
        $user->setDireccion($direccion);
        $user->setMembercardnumber($cinexclusivo);
        $user->setFechacreacion(new \Datetime());
        $user->setTipousuario(0);
        $em->persist($user);
        $em->flush();


        return $http->responseAcceptHttp(array("data"=>$user->getUsuarioid()));
    }  

    /**
     * @Rest\Post("/api/usuario/update", name="UpdateUser", defaults={"_format":"json"})
     * @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          type="json",
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="correoElectronico", type="string", example="pperez@gmail.com"),
     *              @SWG\Property(property="nombre", type="string", example="Pedro"),
     *              @SWG\Property(property="apellido", type="string", example="Perez"),
     *              @SWG\Property(property="recibir_info", type="string", example="Y"),
     *              @SWG\Property(property="CI", type="string", example="V14512457"),
     *              @SWG\Property(property="fecha_nac", type="string", example="1980-05-25"),
     *              @SWG\Property(property="sexo", type="string", example="F"),
     *              @SWG\Property(property="direccion", type="date", example="Avenida los Jabillos"),
     *              @SWG\Property(property="celular", type="integer", example="04244512452"),
     *              @SWG\Property(property="ciudad", type="varchar", example="CARACAS"),
     *              @SWG\Property(property="telefono", type="varchar", example="02124541242"),
     *              @SWG\Property(property="membercardnumber", type="integer", example="9009230004452190")
     *          )
     *      )
     * @SWG\Response(
     *     response=200,
     *     description="Usuario Actualizado"
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la actualización de usuario"
     * )
     *
     * @SWG\Tag(name="UpdateUser")
     */
        
    public function updateUserAction(Request $request){
        $http = $this->get('httpcustom');
        $data = "";
        $dataUser = "";
        $email= $request->request->get('correoElectronico');
        $nombre= $request->request->get('nombre');
        $apellido= $request->request->get('apellido');
        $info= $request->request->get('recibir_info');
        $ci= $request->request->get('CI');
        $fechanac= $request->request->get('fecha_nac');
        $sexo= $request->request->get('sexo');
        $direccion= $request->request->get('direccion');
        $celular= $request->request->get('celular');
        $ciudad= $request->request->get('ciudad');
        $telefono= $request->request->get('telefono');
        $membercardnumber= $request->request->get('membercardnumber');
       
        $em = $this->getDoctrine()->getManager();
        $dataUser = $em->getRepository("App:Usuario")->getUserFindByCorreoCI($email,$ci);
        $dataUser[0]->setNombre($nombre);
        $dataUser[0]->setApellido($apellido);
        $dataUser[0]->setRecibirinformacion($info);
        $dataUser[0]->setCi($ci);
        $dataUser[0]->setFechanacimiento(\DateTime::createFromFormat('Y-m-d', $fechanac));
        $dataUser[0]->setSexo($sexo);
        $dataUser[0]->setDireccion($direccion);
        $dataUser[0]->setNumcelular($celular);
        $dataUser[0]->setMembercardnumber($membercardnumber);
        $em->flush();
        
        return $http->responseAcceptHttp(array("data"=>$data));
    }
    
    
    /**
     * @Rest\Post("/api/usuario/updatepwd", name="UpdateUser", defaults={"_format":"json"})
     * @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          type="json",
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="clave", type="string", example="pepitoperez"),
     *          )
     *      )
     * @SWG\Response(
     *     response=200,
     *     description="Clave del Usuario Actualizado"
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la actualización de clave"
     * )
     *
     * @SWG\Tag(name="UpdatePwd")
     */
    
    public function updatePwdAction(Request $request){
        $http = $this->get('httpcustom');
        $data = "";
        $dataUser = "";
        $clave= $request->request->get('clave');        
        $em = $this->getDoctrine()->getManager();
        $dataUser[0]->setClave($clave);
        $em->flush();
        
        return $http->responseAcceptHttp(array("data"=>$data));
    }
    
    // USER URI's
        
        /**
         * @Rest\Post("/api/login_check", name="user_login_check")
         *
         * @SWG\Response(
         *     response=200,
         *     description="User was logged in successfully"
         * )
         *
         * @SWG\Response(
         *     response=500,
         *     description="User was not logged in successfully"
         * )
         *
         * @SWG\Parameter(
         *     name="_username",
         *     in="body",
         *     type="string",
         *     description="The username",
         *     schema={
         *     }
         * )
         *
         * @SWG\Parameter(
         *     name="_password",
         *     in="body",
         *     type="string",
         *     description="The password",
         *     schema={}
         * )
         *
         * @SWG\Tag(name="User")
         */
        public function getLoginCheckAction() {}

        
  // USER URI's
    /**
     * @Rest\Get("/api/usuario/getnewpwd/email/{email}", name="getnewpwd", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="email",
     *     in="path",
     *     type="string",
     *     description="Correo Electronico"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Metodo de autenticación Neos"
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="No se encontro la nueva contraseña. Metodo de autenticación de Symfony -> Formulario de Actualización de datos"
     * )
     *
     * @SWG\Tag(name="GetNewPwd")
     */
    public function getNewPwdAction($email) {
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $data= [];
        $data = $em->getRepository("App:Usuario")->getNewPwdUserNeos($email);
        if(count($data)>0){
            if ($data[0]['password']==NULL) {
                $data = 0;
            }else{
                $data= 1;
            }
        }else{
            $data = "No existe";
        }
    
        return $http->responseAcceptHttp(array("data"=>$data));
    }
    
    /**
     * @Rest\Post("/api/usuarioNeos/update", name="UpdateUserNeos", defaults={"_format":"json"})
     * @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          type="json",
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="usuarioId", type="integer", example="54523"),
     *              @SWG\Property(property="correoElectronico", type="string", example="pperez@gmail.com"),
     *              @SWG\Property(property="password", type="string"),
     *              @SWG\Property(property="sexo", type="string", example="F"),
     *              @SWG\Property(property="direccion", type="date", example="Avenida los Jabillos"),
     *              @SWG\Property(property="celular", type="integer", example="04244512452"),
     *              @SWG\Property(property="ciudad", type="varchar", example="CARACAS"),
     *              @SWG\Property(property="telefono", type="varchar", example="02124541242"),
     *              @SWG\Property(property="membercardnumber", type="integer", example="9009230004452190")
     *          )
     *      )
     * @SWG\Response(
     *     response=200,
     *     description="Usuario Actualizado"
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la actualización de usuario"
     * )
     *
     * @SWG\Tag(name="UpdateUserNeos")
     */
        
    public function updateUserNeosAction(Request $request, UserPasswordEncoderInterface $encoder){
        $http = $this->get('httpcustom');
        $data = "";
        $dataUser = "";
        $id = $request->request->get('usuarioId');
        $email= $request->request->get('correoElectronico');
        $password = $request->request->get('password');
        $sexo= $request->request->get('sexo');
        $direccion= $request->request->get('direccion');
        $celular= $request->request->get('celular');
        $ciudad= $request->request->get('ciudad');
        $telefono= $request->request->get('telefono');
        $membercardnumber= $request->request->get('cinexclusivo');
       
        $user = new Usuario();

        $em = $this->getDoctrine()->getManager();
        $dataUser = $em->getRepository("App:Usuario")->getUsuarioFindByUsuarioid($id);
        $dataUser[0]->setCorreoelectronico($email);
        $dataUser[0]->setClave(sha1($password));
        $dataUser[0]->setPlainPassword($password);
        $dataUser[0]->setPassword($encoder->encodePassword($user,$password));
        $dataUser[0]->setRecibirinformacion('Y');
        $dataUser[0]->setSexo($sexo);
        $dataUser[0]->setDireccion($direccion);
        $dataUser[0]->setNumcelular($celular);
        $dataUser[0]->setNumlocal($celular);
        $dataUser[0]->setMembercardnumber($membercardnumber);
        
        $dataUser[0]->setUsername($email);
        $em->flush();

        $data = $dataUser[0]->getUsername();
        
        return $http->responseAcceptHttp(array("data"=>$data));
    }

    /**
     * @Rest\Post("/api/usuarioNeos/actualizarPerfil", name="actualizarPerfil", defaults={"_format":"json"})
     * @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          type="json",
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="usuarioId", type="integer", example="54523"),
     *              @SWG\Property(property="correoElectronico", type="string", example="pperez@gmail.com"),
     *              @SWG\Property(property="password", type="string"),
     *              @SWG\Property(property="sexo", type="string", example="F"),
     *              @SWG\Property(property="direccion", type="date", example="Avenida los Jabillos"),
     *              @SWG\Property(property="celular", type="integer", example="04244512452"),
     *              @SWG\Property(property="ciudad", type="varchar", example="CARACAS"),
     *              @SWG\Property(property="telefono", type="varchar", example="02124541242"),
     *              @SWG\Property(property="membercardnumber", type="integer", example="9009230004452190")
     *          )
     *      )
     * @SWG\Response(
     *     response=200,
     *     description="Usuario Actualizado"
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la actualización de usuario"
     * )
     *
     * @SWG\Tag(name="actualizarPerfil")
     */
        
    public function actualizarPerfilAction(Request $request){
        $http = $this->get('httpcustom');
        $data = "";
        $dataUser = "";
        $id = $request->request->get('usuarioId');
        $email= $request->request->get('correoElectronico');
        $sexo= $request->request->get('sexo');
        $direccion= $request->request->get('direccion');
        $celular= $request->request->get('celular');
        $ciudad= $request->request->get('ciudad');
        $telefono= $request->request->get('telefono');
        $membercardnumber= $request->request->get('cinexclusivo');
       
        $em = $this->getDoctrine()->getManager();
        $dataUser = $em->getRepository("App:Usuario")->getUsuarioFindByUsuarioid($id);
        $dataUser[0]->setCorreoelectronico($email);
        $dataUser[0]->setRecibirinformacion('Y');
        $dataUser[0]->setSexo($sexo);
        $dataUser[0]->setDireccion($direccion);
        $dataUser[0]->setNumcelular($celular);
        $dataUser[0]->setNumlocal($celular);
        $dataUser[0]->setMembercardnumber($membercardnumber);
        $dataUser[0]->setUsername($email);
        $em->flush();

        $data = $dataUser[0]->getUsername();
        
        return $http->responseAcceptHttp(array("data"=>$data));
    }

    // USER URI's
    /**
     * @Rest\Get("/api/usuarioNeos/updatePassword/{usuario}/{password}", name="updatePasswordNeos", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="usuario",
     *     in="path",
     *     type="string",
     *     description="Usuario Id"
     * )
     * @SWG\Parameter(
     *     name="password",
     *     in="path",
     *     type="string",
     *     description="Nueva contraseña"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Contraseña actualizada."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la actualización de contraseña."
     * )
     *
     * @SWG\Tag(name="updatePasswordNeos")
     */
    public function updatePasswordNeosAction(Request $request, $usuario, $password, UserPasswordEncoderInterface $encoder){
        $http = $this->get('httpcustom');
        $data = "";
        $dataUser = "";
       
        $user = new Usuario();

        $em = $this->getDoctrine()->getManager();
        $dataUser = $em->getRepository("App:Usuario")->getUsuarioFindByUsuarioid($usuario);
        $dataUser[0]->setClave(sha1($password));
        $dataUser[0]->setPlainPassword($password);
        $dataUser[0]->setPassword($encoder->encodePassword($user,$password));
        $em->flush();

        $data = $dataUser[0]->getUsername();
        
        return $http->responseAcceptHttp(array("data"=>$data));
    }

    // USER URI's
    /**
     * @Rest\Get("/api/usuarioNeos/recoverPassword/{correo}/{tipo_documento}/{n_documento}", name="recoverPasswordNeos", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="correo",
     *     in="path",
     *     type="string",
     *     description="correo electronico"
     * )
     * @SWG\Parameter(
     *     name="tipo_documento",
     *     in="path",
     *     type="string",
     *     description="Tipo de documento"
     * )
     * @SWG\Parameter(
     *     name="n_documento",
     *     in="path",
     *     type="string",
     *     description="Numero de documento"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Contraseña actualizada."
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Ocurrio un error al ejecutar la actualización de contraseña."
     * )
    * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar el servicio."
     * )
     *
     * @SWG\Tag(name="recoverPasswordNeos")
     */
    public function recoverPasswordNeosAction($correo, $tipo_documento, $n_documento, UserPasswordEncoderInterface $encoder){
        $http = $this->get('httpcustom');
        $data = "";
        $dataUser = "";

        $ci = $tipo_documento.''.$n_documento;
       
        $password = base64_encode(strtotime('now'));

        $user = new Usuario();

        $em = $this->getDoctrine()->getManager();
        $dataUser = $em->getRepository("App:Usuario")->getUserFindByCorreoCI($correo,$ci);
        if($dataUser){
            $dataUser[0]->setClave(sha1($password));
            $dataUser[0]->setPlainPassword($password);
            $dataUser[0]->setPassword($encoder->encodePassword($user,$password));
            $em->flush();
            return $http->responseAcceptHttp(array("data"=>$password));
        }
        else{
            return $http->response400Http(array('data'=>'Datos invalidos'));
        }
    }

    /**
     * @Rest\Get("/api/usuarioNeos/passwordNeosLogin/{correo}/{clave}", name="passwordNeosLogin", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="correo",
     *     in="path",
     *     type="string",
     *     description="correo electronico"
     * )
     * @SWG\Parameter(
     *     name="clave",
     *     in="path",
     *     type="string",
     *     description="contraseña"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Contraseña actualizada"
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Ocurrio un error al ejecutar la actualización de contraseña."
     * )
    * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar el servicio."
     * )
     *
     * @SWG\Tag(name="passwordNeosLogin")
     */
    public function passwordNeosLoginAction($correo, $clave)
    {
        $http = $this->get('httpcustom');
        $dataUser = "";
       
        $em = $this->getDoctrine()->getManager();
        $dataUser = $em->getRepository("App:Usuario")->getUsuarioInfoFindByEmail($correo);
        if($dataUser){
            $dataUser[0]->setClave(sha1($clave));
            $em->flush();
            return $http->responseAcceptHttp(array("data"=>'true'));
        }
        else{
            return $http->response400Http(array('data'=>'Datos invalidos'));
        }
    }
}
