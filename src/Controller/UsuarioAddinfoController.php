<?php

namespace App\Controller;
use App\CoreCinex\CinexService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use App\Lib\DaoLib;
use App\Lib\ApiUtilFunctions;
use App\Lib\Constantes;
use App\Entity\UsuarioAddinfo;

class UsuarioAddinfoController extends Controller
{

    // USER URI's
    /**
     * @Rest\Get("/api/usuarioLoyalty/getloyaltyDataUser/userId/{userId}", name="getloyaltyDataUser", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="userId",
     *     in="path",
     *     type="string",
     *     description="Id del Usuario"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Usuario Encontrado"
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="El usuario no posee datos en Loyalty"
     * )
     *
     * @SWG\Tag(name="GetLoyaltyDataUser")
     */
    public function getLoyaltyDataAction($userId) {
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $data= [];
        $data = $em->getRepository("App:UsuarioAddinfo")->getLoyaltyDataUser($userId);

        if (!$data) {
            $data = 0;
        }else{
            $data= $data[0];
        }
        return $http->responseAcceptHttp(array("data"=>$data));
    }

    /**
     * @Rest\Post("/api/usuarioLoyalty/create", name="CreateLoyaltyUser", defaults={"_format":"json"})
     * @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          type="json",
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="usuarioId", type="string"),
     *              @SWG\Property(property="estado", type="integer"),
     *              @SWG\Property(property="municipio", type="integer"),
     *              @SWG\Property(property="ciudad", type="integer"),
     *              @SWG\Property(property="parroquia", type="integer"),
     *              @SWG\Property(property="cinexclusivo", type="string"),
     *              @SWG\Property(property="facebook", type="string"),
     *              @SWG\Property(property="twitter", type="string"),
     *              @SWG\Property(property="instagram", type="string"),
     *              @SWG\Property(property="linkedin", type="string"),
     *              @SWG\Property(property="emailAlt", type="string"),
     *              @SWG\Property(property="aniversario", type="string"),
     *              @SWG\Property(property="civil", type="string"),
     *              @SWG\Property(property="ingreso", type="string"),
     *              @SWG\Property(property="educacion", type="string"),
     *              @SWG\Property(property="ocupacion", type="string"),
     *              @SWG\Property(property="asiento", type="string"),
     *              @SWG\Property(property="bebida", type="string"),
     *              @SWG\Property(property="comida", type="string"),
     *              @SWG\Property(property="formato", type="string"),
     *              @SWG\Property(property="familiar", type="integer"),
     *              @SWG\Property(property="favorita", type="string"),
     *              @SWG\Property(property="concresa", type="integer"),
     *              @SWG\Property(property="recreo", type="integer"),
     *              @SWG\Property(property="lagomall", type="integer"),
     *              @SWG\Property(property="lido", type="integer"),
     *              @SWG\Property(property="proceres", type="integer"),
     *              @SWG\Property(property="manzanares", type="integer"),
     *              @SWG\Property(property="paraiso", type="integer"),
     *              @SWG\Property(property="hatillo", type="integer"),
     *              @SWG\Property(property="sambil", type="integer"),
     *              @SWG\Property(property="sanignacio", type="integer"),
     *              @SWG\Property(property="tolon", type="integer"),
     *              @SWG\Property(property="ventura", type="integer"),
     *              @SWG\Property(property="suspenso", type="integer"),
     *              @SWG\Property(property="infantil", type="integer"),
     *              @SWG\Property(property="drama", type="integer"),
     *              @SWG\Property(property="deporte", type="integer"),
     *              @SWG\Property(property="musical", type="integer"),
     *              @SWG\Property(property="epica", type="integer"),
     *              @SWG\Property(property="animacion", type="integer"),
     *              @SWG\Property(property="documental", type="integer"),
     *              @SWG\Property(property="comedia", type="integer"),
     *              @SWG\Property(property="fantasia", type="integer"),
     *              @SWG\Property(property="venezolano", type="integer"),
     *              @SWG\Property(property="alternativo", type="integer")
     * 
     *          )
     *      )
     * @SWG\Response(
     *     response=200,
     *     description="Información creada correctamente"
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la carga de la información del usuario"
     * )
     *
     * @SWG\Tag(name="createLoyaltyUser")
     */
        
    public function createLoyaltyUser(Request $request){
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $data = '';
        $usuarioId = $request->request->get('usuarioId');
        $estado = $request->request->get('estado');
        $municipio = $request->request->get('municipio');
        $ciudad = $request->request->get('ciudad');
        $parroquia = $request->request->get('parroquia');
        $cinexclusivo = $request->request->get('cinexclusivo');
        $facebook = $request->request->get('facebook');
        $twitter = $request->request->get('twitter');
        $instagram = $request->request->get('instagram');
        $linkedin = $request->request->get('linkedin');
        $emailAlt = $request->request->get('emailAlt');
        $aniversario = $request->request->get('aniversario');
        $civil = $request->request->get('civil');
        $ingreso = $request->request->get('ingreso');
        $educacion = $request->request->get('educacion');
        $ocupacion = $request->request->get('ocupacion');
        $asiento = $request->request->get('asiento');
        $bebida = $request->request->get('bebida');
        $comida = $request->request->get('comida');
        $formato = $request->request->get('formato');
        $familiar = $request->request->get('familiar');
        $favorita = $request->request->get('favorita');
        $concresa = $request->request->get('concresa');
        $recreo = $request->request->get('recreo');
        $lagomall = $request->request->get('lagomall');
        $lido = $request->request->get('lido');
        $proceres = $request->request->get('proceres');
        $manzanares = $request->request->get('manzanares');
        $paraiso = $request->request->get('paraiso');
        $hatillo = $request->request->get('hatillo');
        $sambil = $request->request->get('sambil');
        $sanignacio = $request->request->get('sanignacio');
        $tolon = $request->request->get('tolon');
        $ventura = $request->request->get('ventura');
        $suspenso = $request->request->get('suspenso');
        $infantil = $request->request->get('infantil');
        $drama = $request->request->get('drama');
        $deporte = $request->request->get('deporte');
        $musical= $request->request->get('musical');
        $epica = $request->request->get('epica');
        $animacion = $request->request->get('animacion');
        $documental = $request->request->get('documental');
        $comedia = $request->request->get('comedia');
        $fantasia = $request->request->get('fantasia');
        $venezolano = $request->request->get('venezolano');
        $alternativo = $request->request->get('alternativo');

        $user = new UsuarioAddinfo();
        $user->setUsuarioid($usuarioId);
        $user->setListeId($estado);
        $user->setListcId($ciudad);
        $user->setListprrId($parroquia);
        $user->setListmId($municipio);
        $user->setFacebook($facebook);
        $user->setTwitter($twitter);
        $user->setInstagram($instagram);
        $user->setLinkedin($linkedin);
        $user->setEmailadd($emailAlt);

        if ($aniversario == '') {
            $user->setAniversario(null);
        } else {
            $user->setAniversario(\DateTime::createFromFormat('d/m/Y', $aniversario));
        }
        
        $user->setMembercardnumber($cinexclusivo);
        $user->setCivil($civil);
        $user->setIngreso($ingreso);
        $user->setFamiliar($familiar);
        $user->setEducacion($educacion);
        $user->setOcupacion($ocupacion);
        $user->setFavorita($favorita);
        $user->setAsiento($asiento);
        $user->setBebida($bebida);
        $user->setComida($comida);
        $user->setFormato($formato);
        $user->setConcresa($concresa);
        $user->setRecreo($recreo);
        $user->setLago($lagomall);
        $user->setLido($lido);
        $user->setProceres($proceres);
        $user->setManzana($manzanares);
        $user->setParaiso($paraiso);
        $user->setHatillo($hatillo);
        $user->setSambil($sambil);
        $user->setSanignacio($sanignacio);
        $user->setTolon($tolon);
        $user->setVentura($ventura);
        $user->setSuspenso($suspenso);
        $user->setInfantil($infantil);
        $user->setDrama($drama);
        $user->setDeporte($deporte);
        $user->setMusical($musical);
        $user->setEpica($epica);
        $user->setAnimacion($animacion);
        $user->setDocumental($documental);
        $user->setComedia($comedia);
        $user->setFantasia($fantasia);
        $user->setVenezolana($venezolano);
        $user->setAlternativo($alternativo);
        $user->setEstatus(1);
        $em->persist($user);
        $em->flush();

        $data = 'Registro culminado';
        return $http->responseAcceptHttp(array("data"=>$data));
    }

    /**
     * @Rest\Post("/api/usuarioLoyalty/createWhithoutLoyalty", name="CreateWithoutLoyaltyUser", defaults={"_format":"json"})
     * @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          type="json",
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="usuarioId", type="string"),
     *              @SWG\Property(property="estado", type="integer"),
     *              @SWG\Property(property="municipio", type="integer"),
     *              @SWG\Property(property="ciudad", type="integer"),
     *              @SWG\Property(property="parroquia", type="integer"),
     *              @SWG\Property(property="cinexclusivo", type="string"),
     *              @SWG\Property(property="facebook", type="string"),
     *              @SWG\Property(property="twitter", type="string"),
     *              @SWG\Property(property="instagram", type="string"),
     *              @SWG\Property(property="linkedin", type="string"),
     *              @SWG\Property(property="emailAlt", type="string"),
     *              @SWG\Property(property="aniversario", type="string"),
     *              @SWG\Property(property="civil", type="string"),
     *              @SWG\Property(property="ingreso", type="string"),
     *              @SWG\Property(property="educacion", type="string"),
     *              @SWG\Property(property="ocupacion", type="string"),
     *              @SWG\Property(property="asiento", type="string"),
     *              @SWG\Property(property="bebida", type="string"),
     *              @SWG\Property(property="comida", type="string"),
     *              @SWG\Property(property="formato", type="string"),
     *              @SWG\Property(property="familiar", type="integer"),
     *              @SWG\Property(property="favorita", type="string"),
     *              @SWG\Property(property="concresa", type="integer"),
     *              @SWG\Property(property="recreo", type="integer"),
     *              @SWG\Property(property="lagomall", type="integer"),
     *              @SWG\Property(property="lido", type="integer"),
     *              @SWG\Property(property="proceres", type="integer"),
     *              @SWG\Property(property="manzanares", type="integer"),
     *              @SWG\Property(property="paraiso", type="integer"),
     *              @SWG\Property(property="hatillo", type="integer"),
     *              @SWG\Property(property="sambil", type="integer"),
     *              @SWG\Property(property="sanignacio", type="integer"),
     *              @SWG\Property(property="tolon", type="integer"),
     *              @SWG\Property(property="ventura", type="integer"),
     *              @SWG\Property(property="suspenso", type="integer"),
     *              @SWG\Property(property="infantil", type="integer"),
     *              @SWG\Property(property="drama", type="integer"),
     *              @SWG\Property(property="deporte", type="integer"),
     *              @SWG\Property(property="musical", type="integer"),
     *              @SWG\Property(property="epica", type="integer"),
     *              @SWG\Property(property="animacion", type="integer"),
     *              @SWG\Property(property="documental", type="integer"),
     *              @SWG\Property(property="comedia", type="integer"),
     *              @SWG\Property(property="fantasia", type="integer"),
     *              @SWG\Property(property="venezolano", type="integer"),
     *              @SWG\Property(property="alternativo", type="integer")
     * 
     *          )
     *      )
     * @SWG\Response(
     *     response=200,
     *     description="Información creada correctamente"
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la carga de la información del usuario"
     * )
     *
     * @SWG\Tag(name="CreateWithoutLoyaltyUser")
     */
        
    public function createWithoutLoyaltyUser(Request $request){
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $data = '';
        $usuarioId = $request->request->get('usuarioId');
        $estado = $request->request->get('estado');
        $municipio = $request->request->get('municipio');
        $ciudad = $request->request->get('ciudad');
        $parroquia = $request->request->get('parroquia');
        $facebook = $request->request->get('facebook');
        $twitter = $request->request->get('twitter');
        $instagram = $request->request->get('instagram');
        $linkedin = $request->request->get('linkedin');
        $emailAlt = $request->request->get('emailAlt');
        $aniversario = $request->request->get('aniversario');

        $user = new UsuarioAddinfo();
        $user->setUsuarioid($usuarioId);
        $user->setListeId($estado);
        $user->setListcId($ciudad);
        $user->setListprrId($parroquia);
        $user->setListmId($municipio);
        $user->setFacebook($facebook);
        $user->setTwitter($twitter);
        $user->setInstagram($instagram);
        $user->setLinkedin($linkedin);
        $user->setEmailadd($emailAlt);

        if ($aniversario == '') {
            $user->setAniversario(null);
        } else {
            $user->setAniversario(\DateTime::createFromFormat('Y-m-d', $aniversario));
        }
        
        $user->setEstatus(1);
        $em->persist($user);
        $em->flush();

        $data = 'Registro culminado';
        return $http->responseAcceptHttp(array("data"=>$data));
    }



    /**
     * @Rest\Post("/api/usuarioLoyalty/update", name="UpdateLoyaltyUser", defaults={"_format":"json"})
     * @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          type="json",
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="usuarioId", type="string"),
     *              @SWG\Property(property="estado", type="integer"),
     *              @SWG\Property(property="municipio", type="integer"),
     *              @SWG\Property(property="ciudad", type="integer"),
     *              @SWG\Property(property="parroquia", type="integer"),
     *              @SWG\Property(property="cinexclusivo", type="string"),
     *              @SWG\Property(property="facebook", type="string|null"),
     *              @SWG\Property(property="twitter", type="string"),
     *              @SWG\Property(property="instagram", type="string"),
     *              @SWG\Property(property="linkedin", type="string"),
     *              @SWG\Property(property="emailAlt", type="string"),
     *              @SWG\Property(property="aniversario", type="string"),
     *              @SWG\Property(property="civil", type="string"),
     *              @SWG\Property(property="ingreso", type="string"),
     *              @SWG\Property(property="educacion", type="string"),
     *              @SWG\Property(property="ocupacion", type="string"),
     *              @SWG\Property(property="asiento", type="string"),
     *              @SWG\Property(property="bebida", type="string"),
     *              @SWG\Property(property="comida", type="string"),
     *              @SWG\Property(property="formato", type="string"),
     *              @SWG\Property(property="familiar", type="integer"),
     *              @SWG\Property(property="favorita", type="string"),
     *              @SWG\Property(property="concresa", type="integer"),
     *              @SWG\Property(property="recreo", type="integer"),
     *              @SWG\Property(property="lagomall", type="integer"),
     *              @SWG\Property(property="lido", type="integer"),
     *              @SWG\Property(property="proceres", type="integer"),
     *              @SWG\Property(property="manzanares", type="integer"),
     *              @SWG\Property(property="paraiso", type="integer"),
     *              @SWG\Property(property="hatillo", type="integer"),
     *              @SWG\Property(property="sambil", type="integer"),
     *              @SWG\Property(property="sanignacio", type="integer"),
     *              @SWG\Property(property="tolon", type="integer"),
     *              @SWG\Property(property="ventura", type="integer"),
     *              @SWG\Property(property="suspenso", type="integer"),
     *              @SWG\Property(property="infantil", type="integer"),
     *              @SWG\Property(property="drama", type="integer"),
     *              @SWG\Property(property="deporte", type="integer"),
     *              @SWG\Property(property="musical", type="integer"),
     *              @SWG\Property(property="epica", type="integer"),
     *              @SWG\Property(property="animacion", type="integer"),
     *              @SWG\Property(property="documental", type="integer"),
     *              @SWG\Property(property="comedia", type="integer"),
     *              @SWG\Property(property="fantasia", type="integer"),
     *              @SWG\Property(property="venezolano", type="integer"),
     *              @SWG\Property(property="alternativo", type="integer")
     *          )
     *      )
     * @SWG\Response(
     *     response=200,
     *     description="Datos Actualizado"
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la actualización de datos"
     * )
     *
     * @SWG\Tag(name="UpdateLoyaltyUser")
     */
        
    public function updateLoyaltyDataAction(Request $request){
        $http = $this->get('httpcustom');
        $data = "";
        $dataUser = "";
        $usuarioId = $request->request->get('usuarioId');
        $estado = $request->request->get('estado');
        $municipio = $request->request->get('municipio');
        $ciudad = $request->request->get('ciudad');
        $parroquia = $request->request->get('parroquia');
        $cinexclusivo = $request->request->get('cinexclusivo');
        $facebook = $request->request->get('facebook');
        $twitter = $request->request->get('twitter');
        $instagram = $request->request->get('instagram');
        $linkedin = $request->request->get('linkedin');
        $emailAlt = $request->request->get('emailAlt');
        $aniversario = $request->request->get('aniversario');
        $civil = $request->request->get('civil');
        $ingreso = $request->request->get('ingreso');
        $educacion = $request->request->get('educacion');
        $ocupacion = $request->request->get('ocupacion');
        $asiento = $request->request->get('asiento');
        $bebida = $request->request->get('bebida');
        $comida = $request->request->get('comida');
        $formato = $request->request->get('formato');
        $familiar = $request->request->get('familiar');
        $favorita = $request->request->get('favorita');
        $concresa = $request->request->get('concresa');
        $recreo = $request->request->get('recreo');
        $lagomall = $request->request->get('lagomall');
        $lido = $request->request->get('lido');
        $proceres = $request->request->get('proceres');
        $manzanares = $request->request->get('manzanares');
        $paraiso = $request->request->get('paraiso');
        $hatillo = $request->request->get('hatillo');
        $sambil = $request->request->get('sambil');
        $sanignacio = $request->request->get('sanignacio');
        $tolon = $request->request->get('tolon');
        $ventura = $request->request->get('ventura');
        $suspenso = $request->request->get('suspenso');
        $infantil = $request->request->get('infantil');
        $drama = $request->request->get('drama');
        $deporte = $request->request->get('deporte');
        $musical= $request->request->get('musical');
        $epica = $request->request->get('epica');
        $animacion = $request->request->get('animacion');
        $documental = $request->request->get('documental');
        $comedia = $request->request->get('comedia');
        $fantasia = $request->request->get('fantasia');
        $venezolano = $request->request->get('venezolano');
        $alternativo = $request->request->get('alternativo');

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository("App:UsuarioAddinfo")->getUserData($usuarioId);
        $user[0]->setListeId($estado);
        $user[0]->setListcId($ciudad);
        $user[0]->setListprrId($parroquia);
        $user[0]->setListmId($municipio);
        $user[0]->setFacebook($facebook);
        $user[0]->setTwitter($twitter);
        $user[0]->setInstagram($instagram);
        $user[0]->setLinkedin($linkedin);
        $user[0]->setEmailadd($emailAlt);

        if ($aniversario == '' || $aniversario == '0000-00-00 00:00:00') {
            $user[0]->setAniversario(null);
        } else {
            $date = new \DateTime($aniversario);
            $aniversario = $date->format('Y-m-d');
            $user[0]->setAniversario(\DateTime::createFromFormat('Y-m-d', $aniversario));
        }
        
        $user[0]->setMembercardnumber($cinexclusivo);
        $user[0]->setCivil($civil);
        $user[0]->setIngreso($ingreso);
        $user[0]->setFamiliar($familiar);
        $user[0]->setEducacion($educacion);
        $user[0]->setOcupacion($ocupacion);
        $user[0]->setFavorita($favorita);
        $user[0]->setAsiento($asiento);
        $user[0]->setBebida($bebida);
        $user[0]->setComida($comida);
        $user[0]->setFormato($formato);
        $user[0]->setConcresa($concresa);
        $user[0]->setRecreo($recreo);
        $user[0]->setLago($lagomall);
        $user[0]->setLido($lido);
        $user[0]->setProceres($proceres);
        $user[0]->setManzana($manzanares);
        $user[0]->setParaiso($paraiso);
        $user[0]->setHatillo($hatillo);
        $user[0]->setSambil($sambil);
        $user[0]->setSanignacio($sanignacio);
        $user[0]->setTolon($tolon);
        $user[0]->setVentura($ventura);
        $user[0]->setSuspenso($suspenso);
        $user[0]->setInfantil($infantil);
        $user[0]->setDrama($drama);
        $user[0]->setDeporte($deporte);
        $user[0]->setMusical($musical);
        $user[0]->setEpica($epica);
        $user[0]->setAnimacion($animacion);
        $user[0]->setDocumental($documental);
        $user[0]->setComedia($comedia);
        $user[0]->setFantasia($fantasia);
        $user[0]->setVenezolana($venezolano);
        $user[0]->setAlternativo($alternativo);
        $user[0]->setEstatus(1);
        $em->flush();

        $data = $user[0]->getAniversario();
        return $http->responseAcceptHttp(array("data"=>$data));
    }
}
