<?php
namespace App\Controller;
use App\CoreCinex\CinexService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use App\Lib\DaoLib;
use App\Lib\ApiUtilFunctions;
use App\Lib\Constantes;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Translation\TranslatorInterface;
use App\Lib\XmlLib;
use App\Entity\TokenTrans;
use App\Entity\SettingsPayment;

class MovistarController extends Controller
{     
        protected $params;

        /**
         * Constructor.
        **/
        public function __construct(ParameterBagInterface $params)
        {
            $this->xmlLib= new XmlLib();
            $this->params=$params;
        }
        
        /**
         * @Rest\Get("/api/payment/buscarpuntosmovistar/cedula/{cedula}/movil/{movil}/reintentos/{reintentos}", name="movistarbuscarpuntos", defaults={"_format":"json"})
         *
         * @SWG\Parameter(
         *     name="cedula",
         *     in="path",
         *     type="string",
         *     description="cedula de identidad del usuario."
         * )
         * @SWG\Parameter(
         *     name="movil",
         *     in="path",
         *     type="integer",
         *     description="numero de celular del usuario."
         * )
         * @SWG\Parameter(
         *     name="reintentos",
         *     in="path",
         *     type="integer",
         *     description="numero de reintentos para resultado positivo."
         * )

         * @SWG\Response(
         *     response=200,
         *     description="Array con la respuesta de club movistar"
         * )
         * @SWG\Response(
         *     response=500,
         *     description="Ocurrio un error al ejecutar la consulta de buscarPuntos"
         * )#
         *
         * @SWG\Tag(name="MovistarBuscarPuntos")
         */
                
        public function buscarPuntosAction($cedula, $movil, $reintentos){
            $http = $this->get('httpcustom');
            if ($reintentos == "") $reintentos = 1;
            $xml_data = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:prov="http://provisioning.movistar.com.ve"><soapenv:Body><prov:buscarPuntos><operador>{OPERADOR}</operador><cedula>{CEDULA}</cedula><movil>{MOVIL}</movil></prov:buscarPuntos></soapenv:Body></soapenv:Envelope>';
            $xml_data = str_replace("{OPERADOR}", "CINEX", $xml_data);
            $xml_data = str_replace("{CEDULA}", $cedula, $xml_data);
            $xml_data = str_replace("{MOVIL}", $movil, $xml_data);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://serviciosti.movistar.net.ve/services/club");
            curl_setopt($ch, CURLOPT_VERBOSE, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data);
            for ($x = 0; $x < $reintentos; $x++){
                $output = curl_exec($ch);
                $datalist2 = $this->xmlLib->xmlObjToArray(simplexml_load_string($output, 'SimpleXMLElement'));
                if ($datalist2 != null){
                    $puntos = $datalist2["children"]["env:body"][0]["children"]["buscarpuntos"][0]["children"]["puntos"][0]["text"];
                    break;
                }
            }
            $message = "";
            if ($puntos == 0){
                $message = $datalist2["children"]["env:body"][0]["children"]["buscarpuntos"][0]["children"]["descripcion"][0]["text"];                
            }
            curl_close($ch);
            if ($puntos > 0){
                $data = array("result"=>true, "response"=>"OK", "puntosMovistar"=>$puntos, "mensaje"=>"");
            }else{
                $data = array("result"=>false, "response"=>"ERROR", "puntosMovistar"=>"0", "mensaje"=>$message);
            }
            return $http->responseAcceptHttp(array("data"=>$data));
        }
        
        /**
         * @Rest\Get("/api/payment/descontarpuntosmovistar/token/{token}/cedula/{cedula}/movil/{movil}/puntos/{puntos}/reintentos/{reintentos}", name="movistardescontarpuntos", defaults={"_format":"json"})
         *
         * @SWG\Parameter(
         *     name="token",
         *     in="path",
         *     type="string",
         *     description="token valido para transacciones."
         * )
         * @SWG\Parameter(
         *     name="cedula",
         *     in="path",
         *     type="integer",
         *     description="cedula de identidad del usuario."
         * )
         * @SWG\Parameter(
         *     name="movil",
         *     in="path",
         *     type="integer",
         *     description="numero de celular del usuario."
         * )
         * @SWG\Parameter(
         *     name="puntos",
         *     in="path",
         *     type="integer",
         *     description="numero de puntos de club movistar a descontar."
         * ) 
         * @SWG\Parameter(
         *     name="reintentos",
         *     in="path",
         *     type="integer",
         *     description="numero de reintentos para resultado positivo."
         * )
         
         * @SWG\Response(
         *     response=200,
         *     description="Array con la respuesta de club movistar"
         * )
         * @SWG\Response(
         *     response=500,
         *     description="Ocurrio un error al ejecutar la consulta de buscarPuntos"
         * )#
         *
         * @SWG\Tag(name="MovistarDescontarPuntos")
         */
        
        public function descontarPuntosAction($token, $cedula, $movil, $puntos, $reintentos){
            $http = $this->get('httpcustom');
            if ($this->validateToken($token, $movil)){
                if ($reintentos == "") $reintentos = 1;
                $xml_data = '';
                $xml_data = '<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:prov="http://provisioning.movistar.com.ve"><soapenv:Header/><soapenv:Body><prov:descontarPuntos soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"><operador>{OPERADOR}</operador><cedula>{CEDULA}</cedula><movil>{MOVIL}</movil><puntos>{PUNTOS}</puntos><justificacion>{JUSTIFICACION}</justificacion></prov:descontarPuntos></soapenv:Body></soapenv:Envelope>';
                $xml_data = str_replace("{OPERADOR}", "CINEX", $xml_data);
                $xml_data = str_replace("{CEDULA}", $cedula, $xml_data);
                $xml_data = str_replace("{MOVIL}", $movil, $xml_data);
                $xml_data = str_replace("{PUNTOS}", $puntos, $xml_data);
                $xml_data = str_replace("{JUSTIFICACION}", "canje simple Cinex", $xml_data);
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "https://serviciosti.movistar.net.ve/services/club");
                curl_setopt($ch, CURLOPT_VERBOSE, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
                curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data);
                for ($x = 0; $x < $reintentos; $x++){
                    $output = curl_exec($ch);
                }
                curl_close($ch);
                $this->deleteToken($token, $movil);
                $data = array("result"=>true, "response"=>"OK", "puntosMovistarDescontados"=>$puntos);
            }else{
                $data = array("result"=>false, "response"=>"ERROR");
            }
            return $http->responseAcceptHttp(array("data"=>$data));
        }        
                        
        /**
         * @Rest\Post("/api/payment/generatetokenmovistar/userid/{userid}", name="movistargeneratetokenmovistar", defaults={"_format":"json"})
         *
         * @SWG\Parameter(
         *     name="userid",
         *     in="path",
         *     type="string",
         *     description="devuelve el token generado"
         * )
         
         * @SWG\Response(
         *     response=200,
         *     description="Array con el token generado"
         * )
         * @SWG\Response(
         *     response=500,
         *     description="Ocurrio un error al generar el token"
         * )#
         *
         * @SWG\Tag(name="MovistarGenerateToken")
         */
        
        public function generateTokenAction($userid){
            $token = 0;
            $em = $this->getDoctrine()->getManager();
            $http = $this->get('httpcustom');
            for($x = 0; $x < 16; $x++) {
                $token += (rand() * 16);
            }
            // limpiar token previo del usuario.
            $userid = $userid;            
            $this->deleteToken($userid);                        
            $token = substr($token, 0, 16);
            $tokenTrans = new TokenTrans();
            $tokenTrans->setTokenToken($token);
            $tokenTrans->setTokenUserid($userid);
            $em->persist($tokenTrans);
            $em->flush();
            $data = array("result"=>true, "response"=>"OK", "token"=>$token);
            return $http->responseAcceptHttp(array("data"=>$data));
        }       
        
        /**
         * @Rest\Delete("/api/payment/deletetokenmovistar/userid/{userid}", name="movistardeletetoken", defaults={"_format":"json"})
         *
         * @SWG\Parameter(
         *     name="userid",
         *     in="path",
         *     type="string",
         *     description="devuelve el token generado"
         * )
         
         * @SWG\Response(
         *     response=200,
         *     description="resultado de la operacion"
         * )
         * @SWG\Response(
         *     response=500,
         *     description="Ocurrio un error al generar el token"
         * )#
         *
         * @SWG\Tag(name="MovistarDeleteToken")
         */ 
        
        public function deleteToken($userid){
            $em = $this->getDoctrine()->getManager();
            $http = $this->get('httpcustom');
            $tokenTrans = $em->getRepository("App:TokenTrans")->findByTokenUserid($userid);
            if (count($tokenTrans) > 0){                
                $em->remove($tokenTrans[0]);
                $em->flush();
            }
            $data = array("result"=>true, "response"=>"OK");
            return $http->responseAcceptHttp(array("data"=>$data));
        }
        
        /**
         * @Rest\Get("/api/payment/validatetokenmovistar/token/{token}/userid/{userid}", name="movistarvalidatetoken", defaults={"_format":"json"})
         *
         * @SWG\Parameter(
         *     name="token",
         *     in="path",
         *     type="string",
         *     description="token a validar"
         * )

         * @SWG\Parameter(
         *     name="userid",
         *     in="path",
         *     type="string",
         *     description="identificador del usuario"
         * )
         *          
         * @SWG\Response(
         *     response=200,
         *     description="resultado de la operacion"
         * )
         * @SWG\Response(
         *     response=500,
         *     description="Ocurrio un error al validar el token"
         * )#
         *
         * @SWG\Tag(name="MovistarValidateToken")
         */  
        
        public function validateToken($token, $userid){            
            $em = $this->getDoctrine()->getManager();
            $http = $this->get('httpcustom');
            $tokenTrans = $em->getRepository("App:TokenTrans")->findOneBy(['tokenToken' => $token, 'tokenUserid' => $userid]);
            if ($tokenTrans){
                $result = true;
            }else{
                $result = false;
            }
            $data = array("result"=>true, "response"=>"OK", "validate"=>$result);
            return $http->responseAcceptHttp(array("data"=>$data));            
        }
        
        /**
         * @Rest\Get("/api/payment/getmovistarexchangerate/", name="getmovistarexchangerate", defaults={"_format":"json"})
         * @SWG\Response(
         *     response=200,
         *     description="resultado de la operacion"
         * )
         * @SWG\Response(
         *     response=500,
         *     description="Ocurrio un error al obtener la taza de movistar"
         * )#
         *
         * @SWG\Tag(name="MovistarExchangeRate")
         */
        
        public function movistarExchangeRateAction(){
            $em = $this->getDoctrine()->getManager();
            $http = $this->get('httpcustom');            
            $settingspayment = array();
            $taza_movistar = "";
            $settingspayment = $em->getRepository("App:SettingsPayment")->getSettingsPayment();
            for ($x = 0; $x < sizeof($settingspayment); $x++){
                if ($settingspayment[$x]["settingDesc"] == "TAZA_MOVISTAR"){
                    $taza_movistar = $settingspayment[$x]["settingValue"];
                }
            }
            if ($taza_movistar != ""){
                $data = array("result"=>true, "response"=>"OK", "data"=>$taza_movistar);
            }else{
                $data = array("result"=>true, "response"=>"OK", "data"=>"");
            }            
            return $http->responseAcceptHttp(array("data"=>$data));
        }        
}
