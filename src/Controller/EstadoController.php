<?php


namespace App\Controller;
use App\CoreCinex\CinexService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use App\Lib\DaoLib;
use App\Lib\ApiUtilFunctions;
use App\Lib\Constantes;


class EstadoController extends Controller
{

     // Estado URI"'s
    /**
     * @Rest\Get("/api/estados/list", name="getestadoscomplejo", defaults={"_format":"json"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Estados."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Estados"
     * )
     *
     * @SWG\Tag(name="Estados")
     */    
    public function getEstadosAction() {
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $estados=[];
        $data= [];
        $data = $em->getRepository("App:Estados")->getEstados();
        if (is_null($data)) {
            $data = [];
        }   
        return $http->responseAcceptHttpWithoutFormat($data);
    }

         // Complejo por ciudad URI"'s
    /**
     * @Rest\Get("/api/complejos/ciudad/{id}", name="getcomplejociudad", defaults={"_format":"json"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Ciudad por Complejo."
     * )
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="string",
     *     description="Id Ciudad"
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Complejos por Ciudad"
     * )
     *
     * @SWG\Tag(name="ComplejosCiudad")
     */
    public function getComplejoCiudadAction(Request $request,$id) {
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $daoLib = new DaoLib($em);
        $data= [];
        $sql = "SELECT nombreComplejo FROM cinex.complejo_propio c inner join cinex.ciudad ci on c.ciudadID = ci.ciudadID where ci.ciudadID = $id"; 
        $data = $daoLib->prepareStament($sql);  
        if (is_null($data)) {
            $data = [];
        }   
        return $http->responseAcceptHttp(array("data"=>$data));
    }
    

}
