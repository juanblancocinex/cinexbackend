<?php


namespace App\Controller;
use App\CoreCinex\CinexService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use App\Lib\DaoLib;
use App\Lib\ApiUtilFunctions;
use App\Lib\Constantes;


class MunicipiosController extends Controller
{



    // Municipios por Estado URI"'s
    /**
     * @Rest\Get("/api/estado/municipios", name="getmunicipiosporestado", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="estadoid",
     *     in="query",
     *     type="string",
     *     description="Codigo de Estado"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Municipios."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Municipios por Estado"
     * )
     *
     * @SWG\Tag(name="MunicipiosPorEstado")
     */   
    

    public function getMunicipiosPorEstado(Request $request) {
        $http = $this->get('httpcustom');
        $estadoId= $request->query->get('estadoid');
        $em = $this->getDoctrine()->getManager();
        $data= [];
        $data = $em->getRepository("App:Municipios")->getMunicipiosByIdEstado($estadoId);
        if (is_null($data)) {
            $data = [];
        }   
        return $http->responseAcceptHttpWithoutFormat($data);
    }


     

}
