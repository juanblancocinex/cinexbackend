<?php
namespace App\Controller;
use App\CoreCinex\CinexService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use App\Lib\DaoLib;
use App\Lib\ApiUtilFunctions;
use App\Lib\Constantes;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Translation\TranslatorInterface;
use App\Lib\XmlLib;
use App\Entity\TokenTrans;

class YeipiiController extends Controller
{     
        protected $params;
        protected $xmlLib;
        protected $apiUtilFunctions;       

        /**
         * Constructor.
        **/
        public function __construct(ParameterBagInterface $params)
        {
            $this->apiUtilFunctions= new ApiUtilFunctions();
            $this->xmlLib= new XmlLib();
            $this->params=$params;
        }
        
        /**
         * @Rest\Get("/api/payment/yeipiiRefund/token/{token}/userid/{userid}/referencia/{referencia}/monto/{monto}/useryeipii/{useryeipii}/testmode/{testmode}", name="yeipiiRefund", defaults={"_format":"json"})
         *
         * @SWG\Parameter(
         *     name="token",
         *     in="path",
         *     type="string",
         *     description="token YeiPii"
         * )
         * @SWG\Parameter(
         *     name="userid",
         *     in="path",
         *     type="string",
         *     description="identificador del usuario en la base de datos cinex"
         * )
         * @SWG\Parameter(
         *     name="referencia",
         *     in="path",
         *     type="string",
         *     description="referencia"
         * )
         * @SWG\Parameter(
         *     name="monto",
         *     in="path",
         *     type="string",
         *     description="monto"
         * )
         * @SWG\Parameter(
         *     name="useryeipii",
         *     in="path",
         *     type="string",
         *     description="usuario de yeipii para autorizar la transaccion."
         * )
         * @SWG\Parameter(
         *     name="testmode",
         *     in="path",
         *     type="string",
         *     description="0 - modo produccion, 1 - modo pruebas"
         * )
         * @SWG\Response(
         *     response=200,
         *     description="resultado de la operacion"
         * )
         * @SWG\Response(
         *     response=500,
         *     description="Ocurrio un error al validar el token"
         * )#
         *
         * @SWG\Tag(name="yeipiiRefund")
         */
        
        public function yeipiiRefund($token, $userid, $referencia, $monto, $useryeipii, $testmode){
            $endpoint = "";
            $json_data = array();
            $json_return = array();
            $datalist2 = array();
            $dataenv = array();
            $data = array();
            $tokenYeipii = "";
            $datayeipii = array();
            $tokenyeipiiprod = "";
            $tokenyeipiitest = "";
            $endpointyeipiiprod = "";
            $endpointyeipiitest = "";
            $output = "";
            $payload = "";
            $em = $this->getDoctrine()->getManager();
            $http = $this->get('httpcustom');            
            $datayeipii = $em->getRepository("App:SettingsPayment")->getSettingsPayment();
            for ($x = 0; $x < sizeof($datayeipii); $x++){
                if (trim($datayeipii[$x]["settingDesc"]) == "ENDPOINT YEIPII PRODUCCION"){
                    $endpointyeipiiprod = trim($datayeipii[$x]["settingValue"]);
                }
                if (trim($datayeipii[$x]["settingDesc"]) == "ENDPOINT YEIPII PRUEBAS"){
                    $endpointyeipiitest = trim($datayeipii[$x]["settingValue"]);
                }
            }                        
            if ($token == ""){
                $data = array("result"=>false, "response"=>"ERROR", "mensaje"=>"Token Yeipii Vacio", "data"=>"");
            }else{
                if ($testmode == 0){
                    $endpoint = $endpointyeipiiprod;
                }else{
                    $endpoint = $endpointyeipiitest;
                }
                $dataenv = array(
                    'ref' => $referencia,
                    'return' => $monto
                );
                $payload = json_encode($dataenv);
                $ch = curl_init($endpoint);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLINFO_HEADER_OUT, true);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Authorization: ' . $token,
                    'user: ' . $useryeipii
                ));
                
                $output = curl_exec($ch);
                curl_close($ch);
                $json_return = json_decode($output);
                if ($json_return->success){
                    $data = array("result"=>true, "response"=>"OK", "mensaje"=>"Transaccion exitosa", "data"=>$output);
                }else{
                    $data = array("result"=>false, "response"=>"ERROR", "mensaje"=>"Transaccion rechazada", "data"=>$output);
                }
            }
            return $http->responseAcceptHttp(array("data"=>$data));
        }                
        
        /**
         * @Rest\Get("/api/payment/yeipiiPayment/token/{token}/userid/{userid}/referencia/{referencia}/monto/{monto}/useryeipii/{useryeipii}/testmode/{testmode}", name="yeipiiPayment", defaults={"_format":"json"})
         *
         * @SWG\Parameter(
         *     name="token",
         *     in="path",
         *     type="string",
         *     description="token YeiPii"
         * )  
         * @SWG\Parameter(
         *     name="userid",
         *     in="path",
         *     type="string",
         *     description="identificador del usuario en la base de datos cinex"
         * )        
         * @SWG\Parameter(
         *     name="referencia",
         *     in="path",
         *     type="string",
         *     description="referencia"
         * )
         * @SWG\Parameter(
         *     name="monto",
         *     in="path",
         *     type="string",
         *     description="monto"
         * )
         * @SWG\Parameter(
         *     name="useryeipii",
         *     in="path",
         *     type="string",
         *     description="usuario de yeipii para autorizar la transaccion."
         * ) 
         * @SWG\Parameter(
         *     name="testmode",
         *     in="path",
         *     type="string",
         *     description="0 - modo produccion, 1 - modo pruebas"
         * ) 
         * @SWG\Response(
         *     response=200,
         *     description="resultado de la operacion"
         * )
         * @SWG\Response(
         *     response=500,
         *     description="Ocurrio un error al validar el token"
         * )#
         *
         * @SWG\Tag(name="yeipiiPayment")
         */
        
        public function yeipiiPayment($token, $userid, $referencia, $monto, $useryeipii, $testmode){
            $endpoint = "";
            $json_data = array();
            $json_return = array();
            $datalist2 = array();
            $dataenv = array();
            $data = array();
            $datayeipii = array();
            $endpointyeipiiprod = "";
            $endpointyeipiitest = "";            
            $tokenYeipii = "";
            $output = "";
            $payload = "";
            $x = 0;
            $em = $this->getDoctrine()->getManager();
            $http = $this->get('httpcustom');          
            $datayeipii = $em->getRepository("App:SettingsPayment")->getSettingsPayment();
            for ($x = 0; $x < sizeof($datayeipii); $x++){
                if (trim($datayeipii[$x]["settingDesc"]) == "ENDPOINT YEIPII PRODUCCION"){
                    $endpointyeipiiprod = trim($datayeipii[$x]["settingValue"]);
                }
                if (trim($datayeipii[$x]["settingDesc"]) == "ENDPOINT YEIPII PRUEBAS"){
                    $endpointyeipiitest = trim($datayeipii[$x]["settingValue"]);
                }                
            }
            if ($token == ""){
                $data = array("result"=>false, "response"=>"ERROR", "mensaje"=>"Token Yeipii Vacio", "data"=>"");
            }else{
                if ($testmode == 0){
                    $endpoint = $endpointyeipiiprod;
                }else{
                    $endpoint = $endpointyeipiitest;
                }
                $dataenv = array(
                    'ref' => $referencia,
                    'amount' => $monto
                );
                $payload = json_encode($dataenv);
                $ch = curl_init($endpoint);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLINFO_HEADER_OUT, true);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Authorization: ' . $token,
                    'user: ' . $useryeipii
                ));
                
                $output = curl_exec($ch);
                curl_close($ch);
                $json_return = json_decode($output);
                if ($json_return->success){
                    $data = array("result"=>true, "response"=>"OK", "mensaje"=>"Transaccion exitosa", "data"=>$output);
                }else{
                    $data = array("result"=>false, "response"=>"ERROR", "mensaje"=>"Transaccion rechazada", "data"=>$output);
                }
            }
            return $http->responseAcceptHttp(array("data"=>$data));
        }                
        
}        
