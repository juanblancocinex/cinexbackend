<?php

namespace App\Controller;
use App\CoreCinex\CinexService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use App\Lib\DaoLib;
use App\Lib\ApiUtilFunctions;
use App\Lib\Constantes;
use App\Entity\PeliculaTest;

class PeliculaTestController extends Controller
{
    // Proximamente URI"'s
    /**
     * @Rest\Get("/api/peliculas/proximamente", name="getproximamente", defaults={"_format":"json"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Lista de Peliculas por estrenarse."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Peliculas por Estrenarses."
     * )
     *
     * @SWG\Parameter(
     *     name="mostrartodo",
     *     in="query",
     *     type="string",
     *     description="Mostrar Todo"
     * )
     *
     * @SWG\Tag(name="Proximamente")
     */
    public function getProximamenteAction(Request $request) {
        $mostrarTodo = $request->query->get('mostrartodo');
        $apifunctions = new ApiUtilFunctions();
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $daoLib = new DaoLib($em);
        $data= [];
        $notIn="";
        $mes="";
        $dataJson=array();

        $arrayMeses = $apifunctions->getMonthDescription();
        $sql = "SELECT distinct peliculaID,nombreSinFormato, month(fechaEstreno) as meses,fechaEstreno,year(fechaEstreno) as ano, referencia,genero,duracion,fechaEstreno, contenido
                FROM evenpro_cinex_domain_model_pelicula_test p where p.fechaEstreno > curdate() and nombreOriginal is not null 
                group by nombreSinFormato order by ano, meses, fechaEstreno,nombreSinFormato"; 
        $data = $daoLib->prepareStament($sql);  

        foreach($data as $clave=>$valor){     
            if ($mes!=$valor["meses"]){  
                $mes = $valor["meses"];
                //$dataJson[]=array("mes"=>$arrayMeses[$valor["meses"]]);
                $dataJson[]=array("mes"=>$arrayMeses[$valor["meses"]],"ano"=>$valor["ano"],"peliculas"=>$apifunctions->mapea_peliculas_proximamente($valor["meses"],$data));
            }
        } 
        return $http->responseAcceptHttp(array("data"=>$dataJson));
    } 

      // Buscar Pelicula URI"'s
    /**
     * @Rest\Get("/api/peliculas/buscarpelicula/{id}", name="getbuscarpelicula", defaults={"_format":"json"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Busqueda de Peliculas."
     * )
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="string",
     *     description="Id Pelicula"
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Busqueda de Peliculas."
     * )
     * @SWG\Tag(name="BuscarPelicula")
     */
    public function getBuscarPeliculasAction(Request $request,$id) {
        $apifunctions = new ApiUtilFunctions();
        $apiUtilFunctions = new ApiUtilFunctions();
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $data= [];
        $versiones =  "";
        $data = $em->getRepository("App:PeliculaTest")->getPeliculasById($id);

        return $http->responseAcceptHttp(array("data"=>$data));
    }

      // Buscar Pelicula URI"'s
    /**
     * @Rest\Get("/api/peliculas/buscarpeliculasinposter/{id}", name="getbuscarpeliculasinposter", defaults={"_format":"json"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Busqueda de Peliculas en caso de no tener poster y evitar error en la pagina."
     * )
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="string",
     *     description="Id Pelicula"
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Busqueda de Peliculas."
     * )
     * @SWG\Tag(name="BuscarPeliculaposter")
     */
    public function getBuscarPeliculasSinPosterAction(Request $request,$id) {
        $apifunctions = new ApiUtilFunctions();
        $apiUtilFunctions = new ApiUtilFunctions();
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $data= [];
        $versiones =  "";
        $data = $em->getRepository("App:PeliculaTest")->getPeliculasSinPosterById($id);

        return $http->responseAcceptHttp(array("data"=>$data));
    }


    // Sesiones Especiales URI"'s
    /**
     * @Rest\Get("/api/peliculas/sesiones/especiales/{id}", name="getsesionesespeciales", defaults={"_format":"json"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Sesiones Especiales."
     * )
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="string",
     *     description="Id Sesion Especial"
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Pelicula Sesiones Especiales"
     * )
     *
     * @SWG\Tag(name="PeliculasSesionesEspeciales")
     */
    public function getPeliculaSesionesEspecialesAction(Request $request,$id) {
        $apifunctions = new ApiUtilFunctions();
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $data= [];
        $data = $em->getRepository("App:PeliculaTest")->getSessionesEspeciales($id);
        foreach($data as $clave=>$valor){                      
            $dataVersiones = $em->getRepository("App:PeliculaTest")->getVersiones($data[$clave]["referencia"]);
            foreach ($dataVersiones as $clavePelicula=>$versionPelicula){
                  $data[$clave]["versiones"][]=array("version"=>$dataVersiones[$clavePelicula]["versiones"]);
                  $data[$clave]["referenciaConEnlace"]=Constantes::PATHSITE['imagenescartelera'].rawurlencode($data[$clave]["referencia"]).".jpg";
            }
        }
        return $http->responseAcceptHttp(array("data"=>$data));
    }
    
    /**
     * @Rest\Post("/api/pelicula/createpelicula", name="createPelicula", defaults={"_format":"json"})
     * @SWG\Response(
     *     response=200,
     *     description="Usuario Creado en Pelicula_test."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la creación de la pelicula."
     * )
     *
     * @SWG\Tag(name="createPelicula")
     */
    public function createPeliculaAction($request) {
        $data = "";
        $apifunctions = new ApiUtilFunctions();
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $peliculaTest = new PeliculaTest();        
        $peliculaTest->setNombresinformato($request['nombresinformato']);
        $peliculaTest->setNombreoriginal($request['nombreoriginal']);
        $peliculaTest->setReferencia($request['referencia']);
        $peliculaTest->setCensura($request['censura']);
        $peliculaTest->setGenero($request['genero']);                   
        $peliculaTest->setFechaestreno($request['fechaestreno']);
        $peliculaTest->setUrltrailer($request['urltrailer']);
        $peliculaTest->setUrlposter1($request['urlposter1']);
        $peliculaTest->setUrlposter2($request['urlposter2']);
        $peliculaTest->setCodvista($request['codvista']);
        $peliculaTest->setCodsinec($request['codsinec']);
        $peliculaTest->setVersiones($request['versiones']);
        $peliculaTest->setDirector($request['director']);
        $peliculaTest->setElenco($request['elenco']);
        $peliculaTest->setTipo($request['tipo']);
        $peliculaTest->setDuracion($request['duracion']);
        $peliculaTest->setDescripcion($request['descripcion']);        
        $peliculaTest->setContenido($request['contenido']);
        $peliculaTest->setUrlposter3($request['urlposter3']);
        $peliculaTest->setUrlposter4($request['urlposter4']);
        $peliculaTest->setEsdestacada($request['esdestacada']);
        $peliculaTest->setEspreventa($request['espreventa']);
        $peliculaTest->setEsvenezolana($request['esvenezolana']);
        $peliculaTest->setFechacreacion($request['fechacreacion']);
        $peliculaTest->setFechacreacion($request['espromocion']);
        $peliculaTest->setUltimamodificacion($request['ultimamodificacion']);
        $peliculaTest->setOrden($request['orden']);
        $em->persist($peliculaTest);
        $em->flush();              
        return $http->responseAcceptHttp(array("data"=>$data));
    }
    
    /**
     * @Rest\Put("/api/pelicula/updatepelicula", name="updatePelicula", defaults={"_format":"json"})
     * @SWG\Response(
     *     response=200,
     *     description="Usuario Creado en Pelicula_test."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la creación de la pelicula."
     * )
     *
     * @SWG\Tag(name="updatePelicula")
     */
    public function updatePeliculaAction($request) {
        $data = "";
        $apifunctions = new ApiUtilFunctions();
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $peliculaTest = $em->getRepository("App:PeliculaTest")->getPeliculasByCodSinec($request['codsinec']);
        $peliculaTest->setNombresinformato($request['nombresinformato']);
        $peliculaTest->setNombreoriginal($request['nombreoriginal']);
        $peliculaTest->setReferencia($request['referencia']);
        $peliculaTest->setCensura($request['censura']);
        $peliculaTest->setGenero($request['genero']);
        $peliculaTest->setFechaestreno($request['fechaestreno']);
        $peliculaTest->setUrltrailer($request['urltrailer']);
        $peliculaTest->setUrlposter1($request['urlposter1']);
        $peliculaTest->setUrlposter2($request['urlposter2']);
        $peliculaTest->setCodvista($request['codvista']);
        $peliculaTest->setCodsinec($request['codsinec']);
        $peliculaTest->setVersiones($request['versiones']);
        $peliculaTest->setDirector($request['director']);
        $peliculaTest->setElenco($request['elenco']);
        $peliculaTest->setTipo($request['tipo']);
        $peliculaTest->setDuracion($request['duracion']);
        $peliculaTest->setDescripcion($request['descripcion']);
        $peliculaTest->setContenido($request['contenido']);
        $peliculaTest->setUrlposter3($request['urlposter3']);
        $peliculaTest->setUrlposter4($request['urlposter4']);
        $peliculaTest->setEsdestacada($request['esdestacada']);
        $peliculaTest->setEspreventa($request['espreventa']);
        $peliculaTest->setEsvenezolana($request['esvenezolana']);
        $peliculaTest->setFechacreacion($request['fechacreacion']);
        $peliculaTest->setFechacreacion($request['espromocion']);
        $peliculaTest->setUltimamodificacion($request['ultimamodificacion']);
        $peliculaTest->setOrden($request['orden']);
        $em->flush();
        return $http->responseAcceptHttp(array("data"=>$data));
    }

    /**
     * @Rest\Get("/api/peliculas/proximamentePoster", name="getproximamenteposter", defaults={"_format":"json"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Lista de Peliculas por estrenarse para llenar combo de carga de  Poster formulario Neos."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Peliculas por Estrenarses."
     * )
     *
     *
     * @SWG\Tag(name="ProximamentePoster")
     */
    public function getProximamentePosterAction(Request $request) {
        $apifunctions = new ApiUtilFunctions();
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $daoLib = new DaoLib($em);
        $data= [];
        $sql = "SELECT distinct referencia FROM cinex.pelicula_test where pelicula_test.fechaEstreno > curdate() and referencia is not null group by nombreSinFormato order by fechaEstreno"; 
        $data = $daoLib->prepareStament($sql);  
        return $http->responseAcceptHttp(array("data"=>$data));
    }

    /**
     * @Rest\Get("/api/peliculas/posterVitrina", name="getpostervitrina", defaults={"_format":"json"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Lista de Peliculas Cartelera para llenar combo de carga de Vitrina."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta cartelera."
     * )
     *
     *
     * @SWG\Tag(name="PosterVitrina")
     */
    public function getPosterVitrinaAction(Request $request) {
        $apifunctions = new ApiUtilFunctions();
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $daoLib = new DaoLib($em);
        $data= [];
        $sql = "SELECT p1.referencia AS value, p1.referencia AS label, p1.director AS director, p1.duracion AS duracion, p1.censura AS censura, p1.genero AS genero
                FROM (((evenpro_cinex_domain_model_programacion_test p JOIN evenpro_cinex_domain_model_complejo c) JOIN evenpro_cinex_domain_model_pelicula_test p1) JOIN evenpro_cinex_domain_model_ciudad c1) WHERE ((c.codigoComplejo = p.codigoComplejo) AND ((p.codigoPelicula = p1.codSinec) 
                OR (p.codigoPelicula = p1.codVista)) AND (c1.ciudadID = c.ciudadID) AND (p.fecha_programacion >= CURDATE()) AND (p1.referencia IS NOT NULL)) AND c.codigoComplejo != 'TST' AND c.codigoComplejo != 'PRU' 
                GROUP BY p1.nombreSinFormato ORDER BY p1.orden ASC, p1.fechaEstreno, p1.referencia , p1.esDestacada DESC"; 
        $data = $daoLib->prepareStament($sql);  
        return $http->responseAcceptHttp(array("data"=>$data));
    }

}
