<?php

namespace App\Controller;
use App\CoreCinex\CinexService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use App\Lib\DaoLib;
use App\Lib\ApiUtilFunctions;
use App\Lib\Constantes;
use Symfony\Component\Translation\TranslatorInterface;
use App\Lib\XmlLib;

class SessionOccupationController extends Controller
{

    protected $client;
    protected $params;
    protected $arrayConnect;
    protected $xmlLib;

    /**
     * Constructor.
    **/
    public function __construct(ParameterBagInterface $params)
    {
        $this->xmlLib= new XmlLib();
        $this->params=$params;
        $this->arrayConnect=array("OptionalClientClass" =>$this->params->get('soap.client_class'),
                                  "OptionalClientId" =>$this->params->get('soap.client_id'),
                                  "OptionalClientName" =>$this->params->get("soap.client_name"));
        $this->client = new \SoapClient($this->params->get('soap.cinexdataservice.wsdl'), array("trace" => 1, "exception" => 1,'encoding'=>'UTF8'));
    }

   // GetSessionOccupation CinexDataService URI"'s
    /**
     * @Rest\Get("/api/cinexdataservice/getsessionoccupation/{sessionid}", name="cinexdataservicegetsessionoccupation", defaults={"_format":"json"})
     *
     * @SWG\Parameter(
     *     name="sessionid",
     *     in="path",
     *     type="integer",
     *     description="identificador de sesion de la función"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="informacion de ocupacion de sala por sesión."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de getSessionOccupation"
     * )
     *
     * @SWG\Tag(name="CinexDataServiceGetSessionOccupation")
     */
    public function getSessionOccupationAction($sessionid) {
        $http = $this->get('httpcustom');
        $colorOCupacion = $this->params->get('soap.client_class');
        $daoLib = new DaoLib($this->getDoctrine()->getManager());
        $sql= " SELECT seats_avalaible, color FROM session_occupation WHERE session_id =".$sessionid;
        $sessiones = $daoLib->prepareStament($sql,"asociativo");
        if(count($sessiones)>0){
            $data["result"]=true;
            $data["error"]=false;
            $data["response"]="OK";
            $data["data"]=$sessiones;
        }else{
            $data["result"]=false;
            $data["error"]=true;
            $data["response"]="OK"; 
            $data["data"]=false;
        }   
       
       
        return $http->responseAcceptHttpVista($data);
    }
    
    // Limpia los datos de ocupacion de sesiones.
    /**
     * @Rest\Delete("/api/cinexdataservice/cleanSessionOccupation/", name="cleanSessionOccupation", defaults={"_format":"json"})
     * 
     * @SWG\Response(
     *     response=200,
     *     description="Limpia los datos de ocupacion de sesiones."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de cleanSessionOccupation"
     * )
     *
     * @SWG\Tag(name="cleanSessionOccupation")
     */
    public function getCleanSessionOccupationAction() {
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $data = $em->getRepository("App:SessionOccupation")->findAll();
        foreach($data as $clave=>$valor){
            $em->remove($valor);
            $em->flush();  
        }
        if(count($data)>0){
            $data["result"]=true;
            $data["error"]=false;
            $data["response"]="OK";
            $data["data"]=array("respuesta"=>"SessionOccupation Eliminadas");
        }else{
            $data["result"]=true;
            $data["error"]=false;
            $data["response"]="OK";
            $data["data"]="No se eliminaron registros";
        }
        return $http->responseAcceptHttpVista($data);
    }

}
