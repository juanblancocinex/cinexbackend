<?php
namespace App\Controller;
use App\CoreCinex\CinexService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use App\Lib\DaoLib;
use App\Lib\ApiUtilFunctions;
use App\Lib\Constantes;

class PlantillaController extends Controller
{              
    /**
     * @Rest\Get("/api/plantilla", name="getplantillas", defaults={"_format":"json"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Plantillas creadas."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Plantillas"
     * )
     *
     * @SWG\Tag(name="GetPlantillas")
     */
    public function getPlantillas() {
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $ciudad=[];
        $data= [];
        $data = $em->getRepository("App:Plantilla")->getPlantillas();
        if (is_null($data)) {
            $data = [];
        }
        return $http->responseAcceptHttp(array("data"=>$data));
    }
    
    /**
     * @Rest\Get("/api/plantilla/type/{type}", name="getplantillasbytype", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="type",
     *     in="path",
     *     type="string",
     *     description="tipo de plantilla."
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Plantillas por tipo."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Plantillas"
     * )
     *
     * @SWG\Tag(name="GetPlantillasByType")
     */
    public function getPlantillasByType($type) {
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $ciudad=[];
        $data= [];
        $data = $em->getRepository("App:Plantilla")->getPlantillasByType($type);
        if (is_null($data)) {
            $data = [];
        }
        return $http->responseAcceptHttp(array("data"=>$data));
    }
}
