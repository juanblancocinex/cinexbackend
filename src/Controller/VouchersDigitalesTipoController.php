<?php

namespace App\Controller;
use App\CoreCinex\CinexService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use App\Lib\DaoLib;
use App\Lib\ApiUtilFunctions;
use App\Lib\Constantes;


class VouchersDigitalesTipoController extends Controller
{
    // Vouchers Limit URI"'s
    /**
     * @Rest\Get("/api/vouchersdigitalestipo/", name="getCinexPassTypeList", defaults={"_format":"json"})
     
    * @SWG\Response(
     *     response=200,
     *     description="Voucher Digitales Tipo."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Voucher Digitales Tipo."
     * )
     * @SWG\Tag(name="getCinexPassTypeList")
     */
    public function getVouchersDigitalesTipoAction() {
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $ciudad=[];
        $data= [];
        $data = $em->getRepository("App:VouchersDigitalesTipo")->getVouchersDigitalesTipo();
        if (is_null($data)) {
            $data = [];
        } 
        return $http->responseAcceptHttp(array("data"=>$data));
    }
    
    
    
}