<?php
namespace App\Controller;
use App\CoreCinex\CinexService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use App\Lib\DaoLib;
use App\Lib\ApiUtilFunctions;
use App\Lib\Constantes;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Translation\TranslatorInterface;
use App\Lib\XmlLib;
use App\Entity\TokenTrans;

class MegaSoftController extends Controller
{     
        protected $params;
        protected $xmlLib;
        protected $apiUtilFunctions;       

        /**
         * Constructor.
        **/
        public function __construct(ParameterBagInterface $params)
        {
            $this->apiUtilFunctions= new ApiUtilFunctions();
            $this->xmlLib= new XmlLib();
            $this->params=$params;
        }
        
        /**
         * @Rest\Get("/api/payment/dopayment/token/{token}/cod_afiliacion/{cod_afiliacion}/transcode/{transcode}/pan/{pan}/cvv2/{cvv2}/cid/{cid}/expdate/{expdate}/amount/{amount}/client/{client}/factura/{factura}/userid/{userid}/reintentos/{reintentos}/testmode/{testmode}", name="megasoftdopayment", defaults={"_format":"json"})
         *
         * @SWG\Parameter(
         *     name="token",
         *     in="path",
         *     type="string",
         *     description="token para la transaccion."
         * )
         * @SWG\Parameter(
         *     name="cod_afiliacion",
         *     in="path",
         *     type="integer",
         *     description="codigo de afiliación."
         * )
         * @SWG\Parameter(
         *     name="transcode",
         *     in="path",
         *     type="integer",
         *     description="codigo de transacción."
         * )
         * @SWG\Parameter(
         *     name="pan",
         *     in="path",
         *     type="integer",
         *     description="codigo pan de la tarjeta de credito."
         * )
         * @SWG\Parameter(
         *     name="cvv2",
         *     in="path",
         *     type="integer",
         *     description="codigo cvv2 de la tarjeta de credito."
         * )
         * @SWG\Parameter(
         *     name="cid",
         *     in="path",
         *     type="string",
         *     description="cedula de identidad del tarjetahabiente."
         * )
         * @SWG\Parameter(
         *     name="expdate",
         *     in="path",
         *     type="string",
         *     description="Fecha de expedición de la tarjeta."
         * )
         * @SWG\Parameter(
         *     name="amount",
         *     in="path",
         *     type="string",
         *     description="monto de la transacción."
         * )
         * @SWG\Parameter(
         *     name="client",
         *     in="path",
         *     type="string",
         *     description="Nombre y Apellido del cliente"
         * )
         * @SWG\Parameter(
         *     name="factura",
         *     in="path",
         *     type="string",
         *     description="Numero de factura"
         * )
         * @SWG\Parameter(
         *     name="userid",
         *     in="path",
         *     type="string",
         *     description="Identificador del usuario"
         * )
         * @SWG\Parameter(
         *     name="reintentos",
         *     in="path",
         *     type="string",
         *     description="Cantidad de reintentos"
         * )
         * @SWG\Parameter(
         *     name="testmode",
         *     in="path",
         *     type="integer",
         *     description="indica si se ejecutara en modo de prueba o no, para el ambiente de pruebas de megasoft (0 - produccion | 1 - ambiente de pruebas)"
         * )

         * @SWG\Response(
         *     response=200,
         *     description="Array con la respuesta de megasoft"
         * )
         * @SWG\Response(
         *     response=500,
         *     description="Ocurrio un error al ejecutar la consulta de doPayment"
         * )#
         *
         * @SWG\Tag(name="MegasoftDoPayment")
         */
        
        public function megasoftdoPaymentAction($token, $cod_afiliacion, $transcode, $pan, $cvv2, $cid, $expdate, $amount, $client, $factura, $userid, $reintentos, $testmode){
            /*
             IMPORTANTE
             #TEST
             #megasoft.endPoint=https://200.71.151.226:8443
             #megasoft.transCode=0141
             #megasoft.vistaAfiliacion=2014081101
             #megasoft.sinecAfiliacion=2014081101
             
             #PRODUCCIÓN
             megasoft.endPoint=https://172.16.24.37
             megasoft.transCode=0141
             megasoft.vistaAfiliacion=2015031701
             megasoft.sinecAfiliacion=2015031701
             
             #MEGASOFT             
            */

            $voucher = "";
            $http = $this->get('httpcustom');
            if ($this->megasoftValidateToken($token, $userid)){
                if ($testmode == "0" || $testmode == ""){
                    $trasactionUrl = "https://172.16.24.37/payment/action/procesar-compra?cod_afiliacion=".$cod_afiliacion."&transcode=".$transcode."&pan=".$pan."&cvv2=".$cvv2."&cid=".$cid."&expdate=".$expdate."&amount=".$amount."&client=".$client."&factura=".$factura;
                }else{
                    $trasactionUrl = "https://200.71.151.226:8443/payment/action/procesar-compra?cod_afiliacion=".$cod_afiliacion."&transcode=".$transcode."&pan=".$pan."&cvv2=".$cvv2."&cid=".$cid."&expdate=".$expdate."&amount=".$amount."&client=".$client."&factura=".$factura;
                }
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $trasactionUrl);
                curl_setopt($ch, CURLOPT_VERBOSE, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
                $output = curl_exec($ch);
                $datalist2 = $this->xmlLib->xmlObjToArray(simplexml_load_string($output, 'SimpleXMLElement'));
                if ($datalist2 != null){
                    $codigo = $datalist2["children"]["codigo"][0]["text"];
                    $descripcion = $datalist2["children"]["descripcion"][0]["text"];
                    $vtid = $datalist2["children"]["vtid"][0]["text"];
                    $seqnum = $datalist2["children"]["seqnum"][0]["text"];
                    $authid = $datalist2["children"]["authid"][0]["text"];
                    $authname = $datalist2["children"]["authname"][0]["text"];
                    $factura = $datalist2["children"]["factura"][0]["text"];
                    $tarjeta = $datalist2["children"]["tarjeta"][0]["text"];
                    $referencia = $datalist2["children"]["referencia"][0]["text"];
                    $terminal = $datalist2["children"]["terminal"][0]["text"];
                    $lote = $datalist2["children"]["lote"][0]["text"];
                    $rifbanco = $datalist2["children"]["rifbanco"][0]["text"];
                    $afiliacion = $datalist2["children"]["afiliacion"][0]["text"];
                    for ($x = 0; $x < sizeof($datalist2["children"]["voucher"][0]["children"]["linea"]); $x++){
                        $voucher.= $datalist2["children"]["voucher"][0]["children"]["linea"][$x]["text"]."<br>";
                    }
                }                
                if(isset( $datalist2)){
                    $data = array("result"=>true, "response"=>"OK", "codigo"=>$codigo, "descripcion"=>$descripcion, "vtid"=>$vtid, "seqnum"=>$seqnum, "authid"=>$authid, "authname"=>$authname, "factura"=>$factura, "tarjeta"=>$tarjeta, "referencia"=>$referencia, "terminal"=>$terminal, "lote"=>$lote, "rifbanco"=>$rifbanco, "afiliacion"=>$afiliacion, "voucher"=>$voucher);
                }else{
                    $data = array("result"=>false, "response"=>"Error en la llamada doPayment, Token incorrecto");
                }   
                return $http->responseAcceptHttp(array("data"=>$data)); 
                curl_close($ch);
            }else{
                $data = array("result"=>false, "response"=>"Error en la llamada doPayment, Token incorrecto");
                return $http->responseAcceptHttp(array("data"=>$data));
            }
        }  
        
        /**
         * @Rest\Post("/api/payment/megasoftgeneratetoken/userid/{userid}", name="megasoftgeneratetoken", defaults={"_format":"json"})
         *
         * @SWG\Parameter(
         *     name="userid",
         *     in="path",
         *     type="string",
         *     description="devuelve el token generado"
         * )
         
         * @SWG\Response(
         *     response=200,
         *     description="Array con el token generado"
         * )
         * @SWG\Response(
         *     response=500,
         *     description="Ocurrio un error al generar el token"
         * )#
         *
         * @SWG\Tag(name="MegasoftGenerateToken")
         */
        
        public function megasoftGenerateTokenAction($userid){
            $token = 0;
            $em = $this->getDoctrine()->getManager();
            $http = $this->get('httpcustom');
            for($x = 0; $x < 16; $x++) {
                $token += (rand() * 16);
            }
            // limpiar token previo del usuario.
            $userid = $userid;
            $this->megasoftDeleteToken($userid);
            $token = substr($token, 0, 16);
            $tokenTrans = new TokenTrans();
            $tokenTrans->setTokenToken($token);
            $tokenTrans->setTokenUserid($userid);
            $em->persist($tokenTrans);
            $em->flush();
            $data = array("result"=>true, "response"=>"OK", "token"=>$token);
            return $http->responseAcceptHttp(array("data"=>$data));
        }
        
        /**
         * @Rest\Delete("/api/payment/megasoftdeleteToken/userid/{userid}", name="deletetoken", defaults={"_format":"json"})
         *
         * @SWG\Parameter(
         *     name="userid",
         *     in="path",
         *     type="string",
         *     description="devuelve el token generado"
         * )
         
         * @SWG\Response(
         *     response=200,
         *     description="resultado de la operacion"
         * )
         * @SWG\Response(
         *     response=500,
         *     description="Ocurrio un error al generar el token"
         * )#
         *
         * @SWG\Tag(name="MegasoftDeleteToken")
         */
        
        public function megasoftDeleteToken($userid){
            $em = $this->getDoctrine()->getManager();
            $http = $this->get('httpcustom');
            $tokenTrans = $em->getRepository("App:TokenTrans")->findByTokenUserid($userid);
            if (count($tokenTrans) > 0){
                $em->remove($tokenTrans[0]);
                $em->flush();
            }
            $data = array("result"=>true, "response"=>"OK");
            return $http->responseAcceptHttp(array("data"=>$data));
        }
        
        /**
         * @Rest\Get("/api/payment/megasoftvalidatetoken/token/{token}/userid/{userid}", name="megasoftvalidatetoken", defaults={"_format":"json"})
         *
         * @SWG\Parameter(
         *     name="token",
         *     in="path",
         *     type="string",
         *     description="token a validar"
         * )
         
         * @SWG\Parameter(
         *     name="userid",
         *     in="path",
         *     type="string",
         *     description="identificador del usuario"
         * )
         *
         * @SWG\Response(
         *     response=200,
         *     description="resultado de la operacion"
         * )
         * @SWG\Response(
         *     response=500,
         *     description="Ocurrio un error al validar el token"
         * )#
         *
         * @SWG\Tag(name="MegasoftvalidateToken")
         */
        
        public function megasoftValidateToken($token, $userid){
            $em = $this->getDoctrine()->getManager();
            $http = $this->get('httpcustom');
            $tokenTrans = $em->getRepository("App:TokenTrans")->findOneBy(['tokenToken' => $token, 'tokenUserid' => $userid]);
            if ($tokenTrans){
                $result = true;
            }else{
                $result = false;
            }
            $data = array("result"=>true, "response"=>"OK", "validate"=>$result);
            return $http->responseAcceptHttp(array("data"=>$data));
        }
        
}
