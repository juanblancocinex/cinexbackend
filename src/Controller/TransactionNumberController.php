<?php

namespace App\Controller;
use App\CoreCinex\CinexService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use App\Lib\DaoLib;
use App\Lib\ApiUtilFunctions;
use App\Lib\Constantes;
use App\Entity\TransactionNumber;

class TransactionNumberController extends Controller
{
     
    /**
     * @Rest\Get("/api/transaction/createtransactionnumber", name="createtransactionnumber", defaults={"_format":"json"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Siguiente numero de transaccion."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de nuevo numero de transaction."
     * )
     *
     * @SWG\Parameter(
     *     name="userid",
     *     in="path",
     *     type="string",
     *     description="identificador del usuario"
     * )
     *
     * @SWG\Tag(name="CreateTransactionNumber")
     */
    public function createTransactionNumberAction($usuarioid) {
       $http = $this->get('httpcustom');
       $data = array();
       $data2 = array();
       // crear el registro vacio.
       $Tnumber = new TransactionNumber();
       $em = $this->getDoctrine()->getManager();
       $reference = '';
       $userid=$usuarioid;
       $datetime = date("Y-m-d h:i:s");
       $Tnumber->setReference(isset($reference)?$reference:"");
       $Tnumber->setUsuarioid(isset($userid)?$userid:0);
       $Tnumber->setFecha(isset($datetime)?$datetime:"0000-00-00 00:00:00");
       $em->persist($Tnumber);
       $em->flush();           
       // buscar el autonumerico.
       $daoLib = new DaoLib($em);
       $sql = "SELECT id FROM transaction_number WHERE usuarioid = ".$usuarioid." AND reference = '' ORDER BY id DESC LIMIT 1";
       $data = $daoLib->prepareStament($sql);                      
       // preparar el numero de referencia.
       $tamano = strlen($data[0]["id"]);
       $faltante = 6 - $tamano;
       $formato1 = "000000";
       $dime = substr($formato1, 0, $faltante);
       $reference = date("Y").date("m").date("d").$dime.$data[0]["id"];
       $sql = "UPDATE transaction_number SET reference = ".$reference." WHERE id = ".$data[0]["id"];
       $data = $daoLib->prepareStament($sql);                
       return $http->responseAcceptHttp(array("data"=>$reference));
    }
    
    /**
     * @Rest\Get("/api/transaction/gettransactionnumber", name="gettransactionnumber", defaults={"_format":"json"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Siguiente numero de transaccion."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de nuevo numero de transaction."
     * )
     *
     * @SWG\Parameter(
     *     name="userid",
     *     in="path",
     *     type="string",
     *     description="identificador de la siguiente trnasaccion"
     * )
     *
     * @SWG\Tag(name="GetTransactionNumber")
     */
    public function getransactionNumberAction($usuarioid) {
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $data = array();
        $data2 = array();
        $daoLib = new DaoLib($em);
        $sql = "SELECT reference FROM transaction_number WHERE usuarioid = ".$usuarioid." AND reference <> '' ORDER BY id DESC LIMIT 1";
        $data = $daoLib->prepareStament($sql);
        return $http->responseAcceptHttp(array("data"=>$data[0]["reference"]));
    }
    
          
}
