<?php


namespace App\Controller;
use App\CoreCinex\CinexService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use App\Lib\DaoLib;
use App\Lib\ApiUtilFunctions;
use App\Lib\Constantes;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Translation\TranslatorInterface;
use App\Lib\XmlLib;
class LoyaltySoapController extends Controller
{
     
        protected $client;
        protected $params;
        protected $arrayConnect;
        protected $nuSoap;
        /**
         * Constructor.
        **/
        public function __construct(ParameterBagInterface $params)
        {
            $this->xmlLib= new XmlLib();
            $this->params=$params;
            $this->arrayConnect=array("OptionalClientClass" =>$this->params->get('soap.client_class'),
                                      "OptionalClientId" =>$this->params->get('soap.client_id'),
                                      "OptionalClientName" =>$this->params->get("soap.client_name"));
            $this->client = new \SoapClient($this->params->get('soap.wsdl'), array("trace" => 1, "exception" => 1,'encoding'=>'UTF8'));
            $this->nuSoap =  new \nusoap_client($this->params->get('soap.wsdl'),'wsdl');
            $this->nuSoap->soap_encoding = 'UTF-8';
            $this->nuSoap->decode_utf8 = TRUE;

        }
 

     // GetMember Loyalty URI"'s
    /**
     * @Rest\Get("/api/loyalty/getmember/membercardnumber/{membercardnumber}", name="getmember", defaults={"_format":"json"})
     *
     * @SWG\Parameter(
     *     name="membercardnumber",
     *     in="path",
     *     type="string",
     *     description="numero de tarjeta de loyalty."
     * )
     * @SWG\Response(
     *     response=200,
     *     description="MemberId de Loyalty."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de GetMember"
     * )
     *
     * @SWG\Tag(name="getmember")
     */
    
    public function getMemberAction($membercardnumber) {
        $http = $this->get('httpcustom');
        $data= [];
        $params = array(
            "MemberCardNumber" => $membercardnumber,
            "MemberPassword" =>null,
            "MemberLogin"=>null
        );
        $response = $this->client->GetMemberId($params);
        if($response->Result=="OK"){
            $data["result"]=true;
            $data["response"]=$response->Result;
            $data["memberId"]=$response->MemberId;
        }else{
            $data["result"]=false;
            $data["response"]= $response->Result; 
        }
        //return $data;
        return $http->responseAcceptHttp(array("data"=>$data));
        
    }


    // CreateMember Loyalty URI"'s
    /**
     * @Rest\Post("/api/loyalty/createmember", name="loyaltycreatemember", defaults={"_format":"json"})
     * @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          type="json",
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="sessionid", type="string", example="kqllxqpsaftrqjluujpd"),
     *              @SWG\Property(property="firstname", type="string", example="Bob"),
     *              @SWG\Property(property="lastname", type="string", example="Jones"),
     *              @SWG\Property(property="mobilephone", type="string", example="04265555555"),
     *              @SWG\Property(property="homephone", type="string", example="1111111111111111"),
     *              @SWG\Property(property="email", type="string", example="prueba@gmail.com"),
     *              @SWG\Property(property="cardnumber", type="string", example="1111111111111111"),
     *              @SWG\Property(property="dateofbirth", type="date", example="2018-01-01"),
     *              @SWG\Property(property="expirydate", type="date", example="2018-01-01"),
     *              @SWG\Property(property="loyaltysessionexpiry", type="date", example="2018-01-01"),
     *              @SWG\Property(property="isbannedfrommakingunpaidbookingsuntil", type="date", example="2018-01-01"),
     *              @SWG\Property(property="clubid", type="integer", example="2"),
     *              @SWG\Property(property="status", type="integer", example="2"),
     *              @SWG\Property(property="cardlist", type="integer", example="1111111111111111"),
     *          )
     *      )
     * @SWG\Response(
     *     response=200,
     *     description="Usuario Creado en Loyalty."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la creación de usuario"
     * )
     *
     * @SWG\Tag(name="LoyaltyCreatemember")
     */
    
    public function createMemberAction($request) {
        var_dump($request);exit;
        //$http = $this->get('httpcustom');
        $data= [];
        //$params = $this->mapeaCreateMember($request);

        $uservistaid= $request['uservistaid'];
        if($request['memberId']){
            $memberId= $request['memberId'];     
        }
        $nombre= $request['nombre'];
        $apellido= $request['apellido'];
        $celular= $request['celular'];
        $email= $request['email'];
        $cinexclusivo= $request['cinexclusivo'];
        $local= $request['local'];
        $dateOfBirth = $request->request->get('dateofbirth');
        $expiryDate = $request->request->get('expirydate');
        $loyaltySessionExpiry = $request->request->get('loyaltysessionexpiry');
        $isBannedFromMakingUnpaidBookingsUntil=  $request->request->get('isbannedfrommakingunpaidbookingsuntil');
        $clubId=  $request->request->get('clubid');
        $status=  $request->request->get('status');
        $cardList=  $request->request->get('cardlist');
        $params = array(
            "OptionalClientClass"=>$this->params->get('soap.client_class'),
            "OptionalClientId" =>$this->params->get('soap.client_id'),
            "OptionalClientName" =>$this->params->get("soap.client_name"),
            "UserSessionId"=>$uservistaid,
            "LoyaltyMember"=>array(
                                    "FirstName"=>isset($nombre) ? $nombre : null ,
                                    "LastName"=>isset($apellido)  ? $apellido : null,
                                    "MobilePhone"=>isset($celular)  ? $celular : null,
                                    "HomePhone"=>isset($local)  ? $local : null,
                                    "Email"=>isset($email)  ? $email : null,
                                    "CardNumber"=>isset($cinexclusivo) ? $cinexclusivo: null,
                                    "DateOfBirth"=>isset($dateOfBirth)  ?  $dateOfBirth."T00:00:00" : "0001-01-01T00:00:00Z",
                                    "ExpiryDate"=>isset($expiryDate)  ? $expiryDate."T00:00:00" : "0001-01-01T00:00:00Z",
                                    "LoyaltySessionExpiry"=>isset($loyaltySessionExpiry)  ? $loyaltySessionExpiry."T00:00:00": "0001-01-01T00:00:00Z",
                                    "IsBannedFromMakingUnpaidBookingsUntil"=>isset($isBannedFromMakingUnpaidBookingsUntil)  ? $isBannedFromMakingUnpaidBookingsUntil."T00:00:00" : "0001-01-01T00:00:00Z",
                                    "Status"=>isset($status)  ? $status : 2,
                                    "CardList"=>array("string"=>isset($cardList)  ? $cardList : null ),
                                    "ClubID"=>isset($clubId)  ? $clubId : 2,
                                    "SendNewsletter"=>false,
                                    "EducationLevel"=>0,   
                                    "HouseholdIncome"=>0,
                                    "PersonsInHousehold"=>0,
                                    "PickupComplex"=>0,  
                                    "PreferredComplex"=>0, 
                                    "ContactByThirdParty"=>false,
                                    "GiftCard"=>false,
                                    "GiftCardBalance"=>0,
                                    "MembershipActivated"=>true,
                                    "IsAnonymous"=>false,
                                    "WishToReceiveSMS"=>true,
                                     "Occupation"=>0,  
                                    "MemberLevelId"=>0, 
                                    "IsAnonymous"=>false                                            
            ),
            
        );
        
        if(isset($memberId)){
            $params["LoyaltyMember"]["MemberId"]=$memberId;
        }




        $response = $this->client->CreateMember($params);
        if($response->Result=="OK"){
            $data["result"]=true;
            $data["response"]=$response->Result;
            $data["mensaje"]="Miembro Creado satisfactoriamente";
        }else{
            $data["result"]=false;
            $data["response"]= $translator->trans($response->ErrorDescription); 
        }    
        return $http->responseAcceptHttp(array("data"=>$data));
        
    }




    // getMemberSearch Loyalty URI"'s
    /**
     * @Rest\Get("/api/loyalty/getmembersearch/usersessionid/{usersessionid}/cardnumber/{cardnumber}", name="loyaltygetmembersearch", defaults={"_format":"json"})
     *
     * @SWG\Parameter(
     *     name="usersessionid",
     *     in="path",
     *     type="string",
     *     description="usersessionid registrado en vista"
     * )
    * @SWG\Parameter(
     *     name="cardnumber",
     *     in="path",
     *     type="integer",
     *     description="cardnumber numero de tarjeta de cinexclusivo"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="informacion del usuario."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de GetMemberSearch"
     * )
     *
     * @SWG\Tag(name="LoyaltyGetMemberSearch")
     */
    
    public function getMemberSearchAction($usersessionid,$cardnumber, TranslatorInterface $translator) {
        $http = $this->get('httpcustom');
        $data= [];
        $params = array(
            "OptionalClientClass"=>$this->params->get('soap.client_class'),
            "OptionalClientId" =>$this->params->get('soap.client_id'),
            "OptionalClientName" =>$this->params->get("soap.client_name"),
            "UserSessionId"=>$usersessionid,
            "OptionalCardNumber"=>$cardnumber,
            
        );
        $response = $this->client->MemberSearch($params);
        if($response->Result=="OK" && !isset($response->ErrorDescription)){
            $data["result"]=true;
            $data["error"]=false;
            $data["response"]=$response->Result;
            $xml = $this->xmlLib->getXmlObject($response->LoyaltyXML);
            $data["data"]=json_decode(json_encode((array)$xml), TRUE);
           // $data["data"]=$response->LoyaltyXML;
        }else{
            $data["result"]=false;
            $data["error"]=true;
            $data["response"]= $translator->trans($response->ErrorDescription); 
            $data["data"]=false;
        }   
        return $http->responseAcceptHttpVista($data);
        
    }


     // Devuelve el codigo de Activación de la membresia de LOYALTY
    /**
     * @Rest\Get("/api/loyalty/getactivacioncode/{memberid}", name="loyaltyactivationcode", defaults={"_format":"json"})
     *
     * @SWG\Parameter(
     *     name="memberid",
     *     in="path",
     *     type="string",
     *     description="memberid Identificador del usuario de Cinexclusivo registrado en vista y usado en las consultas"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Codigo Activación de Membresia."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de GetMemberActivacionCode"
     * )
     *
     * @SWG\Tag(name="LoyaltyGetActivationCode")
     */
    
    public function getMemberActivacionCodeAction($memberid, TranslatorInterface $translator) {
        $http = $this->get('httpcustom');
        $data= [];
        $params = array(
            "MemberId"=>$memberid,
            "CreateIfNotFound"=>false

        );
        $response = $this->client->GetWebAccountActivationCode($params);
        if(isset($response->ActivationCode)){
            $data["result"]=true;
            $data["response"]="OK";
            $data["data"]=$response->ActivationCode;
            $data["error"]=false; 

        }else{
            $data["result"]=false;
            $data["response"]=false; 
            $data["error"]=true; 
            $data["data"]=false;
        }   
        return $http->responseAcceptHttpVista($data);
        
    }
    
     // getMemberItemList Loyalty URI"'s
    /**
     * @Rest\Get("/api/loyalty/getMemberItemList/{usersessionid}", name="loyaltygetmebersearch", defaults={"_format":"json"})
     *
     * @SWG\Parameter(
     *     name="usersessionid",
     *     in="path",
     *     type="string",
     *     description="usersessionid registrado en vista"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="informacion del usuario."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de GetMemberSearch"
     * )
     *
     * @SWG\Tag(name="LoyaltyGetMemberItemList")
     */
    
    public function getMemberItemListAction($usersessionid, TranslatorInterface $translator) {
        $http = $this->get('httpcustom');
        $data= [];
        $params = array(
            "UserSessionId"=>$usersessionid,
            "GetTicketTypes"=>1,
            "GetDiscounts"=>1,
            "GetConcessions"=>1,
            "SelectedSessionDateTime"=>-1,
            "SupressSelectedSessionDateTimeFilter"=>-1,
            "GetAdvanceBookings"=>-1,
            "GetAdvanceSeatingRecognitions"=>-1,
            "OptionalClientClass" =>$this->params->get('soap.client_class'),
            "OptionalClientId" =>$this->params->get('soap.client_id'),
            "OptionalClientName" =>$this->params->get("soap.client_name")
        );
        $response = $this->client->GetMemberItemList($params);
        if($response->Result=="OK" && !isset($response->ErrorDescription)){
            $data["result"]=true;
            $data["error"]=false;
            $data["response"]=$response->Result;
            if(isset($response->TicketTypeList->LoyaltyItem)){
                $xml = $this->xmlLib->generateValidXmlFromArray($response->TicketTypeList->LoyaltyItem);
                $xml = $this->xmlLib->getXmlObject($xml);
                $data["data"]=json_decode(json_encode((array)$xml), TRUE);
            }else{
                $data["data"]=array("mensaje"=>"El usuario no tiene Beneficios");
            }
            
        }else{
           
            $data["result"]=false;
            $data["error"]=true;
            $data["response"]= $translator->trans($response->Result); 
            $data["data"]=false;
        }   
        return $http->responseAcceptHttpVista($data);
    }


    /**
     * @Rest\Put("/api/loyalty/updatemember", name="loyaltyupdatemember", defaults={"_format":"json"})
     * @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          type="json",
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="sessionid", type="string", example="kqllxqpsaftrqjluujpd"),
     *              @SWG\Property(property="memberid", type="string", example="122224444"),
     *              @SWG\Property(property="firstname", type="string", example="Bob"),
     *              @SWG\Property(property="lastname", type="string", example="Jones"),
     *              @SWG\Property(property="mobilephone", type="string", example="04265555555"),
     *              @SWG\Property(property="homephone", type="string", example="1111111111111111"),
     *              @SWG\Property(property="email", type="string", example="prueba@gmail.com"),
     *              @SWG\Property(property="cardnumber", type="string", example="1111111111111111"),
     *              @SWG\Property(property="dateofbirth", type="date", example="2018-01-01"),
     *              @SWG\Property(property="expirydate", type="date", example="2018-01-01"),
     *              @SWG\Property(property="loyaltysessionexpiry", type="date", example="2018-01-01"),
     *              @SWG\Property(property="isbannedfrommakingunpaidbookingsuntil", type="date", example="2018-01-01"),
     *              @SWG\Property(property="clubid", type="integer", example="2"),
     *              @SWG\Property(property="status", type="integer", example="2"),
     *              @SWG\Property(property="cardlist", type="integer", example="1111111111111111"),
     *          )
     *      )
     * @SWG\Response(
     *     response=200,
     *     description="Usuario Creado en Loyalty."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la creación de usuario"
     * )
     *
     * @SWG\Tag(name="LoyaltyUpdateMember")
     */
    
    public function updateMember(TranslatorInterface $translator,Request $request){
        $http = $this->get('httpcustom');
        $data= [];
        $params = $this->mapeaUpdateMember($request);
        $response = $this->client->UpdateMember($params);
        if($response->Result=="OK"){
            $data["result"]=true;
            $data["response"]=$response->Result;
            $data["mensaje"]="Miembro Creado satisfactoriamente";
        }else{
            $data["result"]=false;
            $data["response"]= $translator->trans($response->ErrorDescription); 
        }    
        return $http->responseAcceptHttp(array("data"=>$data));
    }

/**
 * metodo updatemember ghensys
 */
    
    public function updateMemberNeos($params){
        $data= [];
        $array = array(
            "OptionalClientClass"=>$this->params->get('soap.client_class'),
            "OptionalClientId" =>$this->params->get('soap.client_id'),
            "OptionalClientName" =>$this->params->get("soap.client_name"),
            "UserSessionId"=>$params['uservistaid'],
            "UpdatePreferences"=>true,
            "LoyaltyMember"=>array(
                                    "FirstName"=>isset($params['nombre']) ? $params['nombre'] : null ,
                                    "LastName"=>isset($params['apellido']) ? $params['apellido'] : null,
                                    "MobilePhone"=>isset($params['celular']) ? $params['celular'] : null,
                                    "HomePhone"=>isset($params['local']) ? $params['local'] : null,
                                    "Email"=>isset($params['email']) ? $params['email'] : null,
                                    "CardNumber"=>isset($params['cinexclusivo']) ? $params['cinexclusivo'] : null,
                                    "DateOfBirth"=>isset($dateOfBirth)  ?  $dateOfBirth."T00:00:00" : "0001-01-01T00:00:00Z",
                                    "ExpiryDate"=>isset($expiryDate)  ? $expiryDate."T00:00:00" : "0001-01-01T00:00:00Z",
                                    "LoyaltySessionExpiry"=>isset($loyaltySessionExpiry)  ? $loyaltySessionExpiry."T00:00:00": "0001-01-01T00:00:00Z",
                                    "IsBannedFromMakingUnpaidBookingsUntil"=>isset($isBannedFromMakingUnpaidBookingsUntil)  ? $isBannedFromMakingUnpaidBookingsUntil."T00:00:00" : "0001-01-01T00:00:00Z",
                                    "Status"=>isset($status)  ? $status : 2,
                                    "CardList"=>array("string"=>isset($params['cinexclusivo']) ? $params['cinexclusivo'] : null),
                                    "ClubID"=>isset($clubId)  ? $clubId : 2,
                                    "SendNewsletter"=>true,
                                    "EducationLevel"=>0,   
                                    "HouseholdIncome"=>0,
                                    "PersonsInHousehold"=>0,
                                    "PickupComplex"=>0,  
                                    "PreferredComplex"=>0, 
                                    "ContactByThirdParty"=>false,
                                    "GiftCard"=>false,
                                    "GiftCardBalance"=>0,
                                    "MembershipActivated"=>true,
                                    "IsAnonymous"=>false,
                                    "WishToReceiveSMS"=>true,
                                     "Occupation"=>0,  
                                    "MemberLevelId"=>0, 
                                    "IsAnonymous"=>false,                                                     
            ),
        );
        if(isset($params['memberId'])){
            $array["LoyaltyMember"]["MemberId"]=$params['memberId'];
        }

        $response = $this->client->UpdateMember($array);
        if($response->Result=="OK"){
            $data["result"]=true;
            $data["response"]=$response->Result;
            $data["mensaje"]="Miembro Creado satisfactoriamente";
        }else{
            $data["result"]=false;
            $data["response"]= $response->ErrorDescription; 
        }

        return $data;
    }


/**
 * fin updatemember ghensys
 */




// CreateMember Loyalty URI"'s
    /**
     * @Rest\Put("/api/loyalty/membershipactivated", name="loyaltymembershipactivated", defaults={"_format":"json"})
     * @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          type="json",
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="sessionid", type="string", example="kqllxqpsaftrqjluujpd"),
     *              @SWG\Property(property="memberid", type="string", example="122224444"),
     *              @SWG\Property(property="cardnumber", type="string", example="1111111111111111"),
     *              @SWG\Property(property="dateofbirth", type="date", example="2018-01-01"),
     *              @SWG\Property(property="expirydate", type="date", example="2018-01-01"),
     *              @SWG\Property(property="loyaltysessionexpiry", type="date", example="2018-01-01"),
     *              @SWG\Property(property="isbannedfrommakingunpaidbookingsuntil", type="date", example="2018-01-01"),
     *              @SWG\Property(property="isActivated", type="integer", example="1 o 0"),
     *          )
     *      )
     * @SWG\Response(
     *     response=200,
     *     description="Usuario Activado en Loyalty."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la activacion de Usuario"
     * )
     *
     * @SWG\Tag(name="LoyaltyMembershipactivated")
     */
    public function setMembershipActivated($memberId, $membercardnumber, $uservistaid){
        //$http = $this->get('httpcustom');
        $data= [];
        //$params = $this->mapeaActiveMember($request);

        $params = array(
            "OptionalClientClass"=>$this->params->get('soap.client_class'),
            "OptionalClientId" =>$this->params->get('soap.client_id'),
            "OptionalClientName" =>$this->params->get("soap.client_name"),
            "UserSessionId"=>$uservistaid,
            
            "LoyaltyMember"=>array(
                                "CardNumber"=>isset($membercardnumber) ? $membercardnumber: null,
                                    "SendNewsletter"=>false,
                                "EducationLevel"=>0,   
                                "HouseholdIncome"=>0,
                                "PersonsInHousehold"=>0,
                                "PickupComplex"=>0,  
                                "PreferredComplex"=>0, 
                                "ContactByThirdParty"=>false,
                                "GiftCard"=>false,
                                "GiftCardBalance"=>0,
                                "MembershipActivated"=>true,
                                "IsAnonymous"=>false,
                                "WishToReceiveSMS"=>true,
                                    "Occupation"=>0,  
                                "MemberLevelId"=>0, 
                                "IsAnonymous"=>false,
                                "DateOfBirth"=>isset($dateOfBirth)  ?  $dateOfBirth."T00:00:00" : "0001-01-01T00:00:00Z",
                                "ExpiryDate"=>isset($expiryDate)  ? $expiryDate."T00:00:00" : "0001-01-01T00:00:00Z",
                                "LoyaltySessionExpiry"=>isset($loyaltySessionExpiry)  ? $loyaltySessionExpiry."T00:00:00": "0001-01-01T00:00:00Z",
                                "IsBannedFromMakingUnpaidBookingsUntil"=>isset($isBannedFromMakingUnpaidBookingsUntil)  ? $isBannedFromMakingUnpaidBookingsUntil."T00:00:00" : "0001-01-01T00:00:00Z",
            ),
        );
        if(isset($isActivated)){    
           $params["IsActivated"]=$isActivated;
        }else{
            $params["IsActivated"]=0;
        }
        if(isset($memberId)){
            $params["MemberId"]=$memberId;
        }

        $response = $this->client->SetMembershipActivated($params);
        if($response->Result=="OK"){
            $data["result"]=true;
            $data["response"]=$response->Result;
            $data["mensaje"]="Miembro Activado satisfactoriamente";
        }else{
            $data["result"]=false;
            $data["response"]= $response->ErrorDescription; 
        }

        return $data;
    }
    

    

    // ValidarMember Loyalty URI"'s
    /**
     * @Rest\Get("/api/loyalty/activatewebaccount/memberid/{memberid}/activationcode/{activationcode}", name="loyaltyActivateWebAccount", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="memberid",
     *     in="path",
     *     type="string",
     *     description="memberid codigo de miembro en loyalty"
     * )
     * @SWG\Parameter(
     *     name="activationcode",
     *     in="path",
     *     type="string",
     *     description="activationcode codigo de activacion en loyalty"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Activacion de Usuario en Loyalti."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de ActivateWebAccount"
     * )
     *
     * @SWG\Tag(name="LoyaltyActivateWebAccountr")
     */
    
    public function ActivateWebAccountAction($memberId, $activationCode) {
        //$http = $this->get('httpcustom');
        $data= [];
        $params = array(
            "MemberId"=>$memberId,
            "ActivationCode"=>$activationCode,
        );
        $response = $this->client->ActivateWebAccount($params);
        //var_dump($response);exit;
        if(isset($response->ResultCode)){
            $data["result"]=true;
            $data["response"]=array("ResulCode"=>$response->ResultCode);
        }else{
            $data["result"]=false;
            $data["response"]= "Error en el metodo ActiveWebAccount"; 
        }
        var_dump($data);exit;
        return $data;
    }



      // ValidarMember Loyalty URI"'s
    /**
     * @Rest\Get("/api/loyalty/validatemember/usersessionid/{usersessionid}/cardnumber/{cardnumber}", name="loyaltyvalidatemember", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="usersessionid",
     *     in="path",
     *     type="string",
     *     description="usersessionid registrado en vista"
     * )
     * @SWG\Parameter(
     *     name="cardnumber",
     *     in="path",
     *     type="string",
     *     description="numero de tarjeta de Loyalti"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Validación de Membresia en Loyalti."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de validateMember"
     * )
     *
     * @SWG\Tag(name="LoyaltyValidateMember")
     */
    
    public function validateMember($uservistaid,$membercardnumber) {
        //$http = $this->get('httpcustom');
        $data= [];
        $params = array(
            "OptionalClientClass"=>$this->params->get('soap.client_class'),
            "UserSessionId"=>$uservistaid,
            "MemberCardNumber"=>$membercardnumber,
            "ReturnMember"=>1,
            "IncludeAdvanceBooking"=>false
        );
        $response = $this->client->ValidateMember($params);
        if($response->Result=="OK"){
            $data["result"]=true;
            $data["response"]=array((array)$response->LoyaltyMember);
        }else{
            $data["result"]=false;
            $data["response"]= $response->ErrorDescription; 
        }

        return $data;
       
        //return $http->responseAcceptHttp(array("data"=>$data));
        
    }




    /********¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡ REHACER LOGICA DEL METODOOOOOO!!!!!!!!!!!!! */

    // GetMember Loyalty URI"'s
    /**
     * @Rest\Get("/api/loyalty/getwebaccountactivationcode/{memberid}", name="loyaltygetwebaccountactivationcode", defaults={"_format":"json"})
     *
     * @SWG\Parameter(
     *     name="memberid",
     *     in="path",
     *     type="string",
     *     description="memberid numero de tarjeta cine exclusivo"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Codigo de activación de la membresia en LOYALTY."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de GetWebAccountActivationCoder"
     * )
     *
     * @SWG\Tag(name="LoyaltyGetWebAccountActivationCode")
     */
    
    public function getWebAccountActivationCodeAction($memberid/*,TranslatorInterface $translator*/) {
        $wsdl_url = "http://172.20.5.242/WSVistaWebClient/LoyaltyService.asmx?wsdl";
        $xml_data = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://vista.co.nz/services/WSVistaWebClient.DataTypes/1/"><soapenv:Header/><soapenv:Body><ns:GetWebAccountActivationCodeRequest><ns:MemberId>{MEMBER_ID}</ns:MemberId><ns:CreateIfNotFound>1</ns:CreateIfNotFound></ns:GetWebAccountActivationCodeRequest></soapenv:Body></soapenv:Envelope>';
        $xml_data = str_replace("{MEMBER_ID}", $memberid, $xml_data);
        $ch = curl_init($wsdl_url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml', 'soapaction: http://vista.co.nz/services/WSVistaWebClient.ServiceContracts/1/GetWebAccountActivationCode'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml_data");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        if (strpos($output, "<ResultCode>0</ResultCode>") !== false){
            return json_encode(array("result"=>true, "response"=>"OK", "data"=>$output));
        }else{
            return json_encode(array("result"=>false, "response"=>"Error en la llamada getWebAccountActivationCodeRequest ".$output));
        }
        curl_close($ch);
    }


    public function mapeaCreateMember($request){
        $userSessionId= $request->request->get('sessionid');
        if($request->request->get('memberid')){
            $memberId= $request->request->get('memberid');     
        }
        $primerNombre= $request->request->get('firstname');
        $apellido= $request->request->get('lastname');
        $mobilePhone= $request->request->get('mobilephone');
        $email= $request->request->get('email');
        $cardNumber= $request->request->get('cardnumber');
        $homePhone= $request->request->get('homephone');
        $dateOfBirth = $request->request->get('dateofbirth');
        $expiryDate = $request->request->get('expirydate');
        $loyaltySessionExpiry = $request->request->get('loyaltysessionexpiry');
        $isBannedFromMakingUnpaidBookingsUntil=  $request->request->get('isbannedfrommakingunpaidbookingsuntil');
        $clubId=  $request->request->get('clubid');
        $status=  $request->request->get('status');
        $cardList=  $request->request->get('cardlist');
        $params = array(
            "OptionalClientClass"=>$this->params->get('soap.client_class'),
            "OptionalClientId" =>$this->params->get('soap.client_id'),
            "OptionalClientName" =>$this->params->get("soap.client_name"),
            "UserSessionId"=>$userSessionId,
            "LoyaltyMember"=>array(
                                    "FirstName"=>isset($primerNombre) ? $primerNombre : null ,
                                    "LastName"=>isset($apellido)  ? $apellido : null,
                                    "MobilePhone"=>isset($mobilePhone)  ? $mobilePhone : null,
                                    "HomePhone"=>isset($homePhone)  ? $homePhone : null,
                                    "Email"=>isset($email)  ? $email : null,
                                    "CardNumber"=>isset($cardNumber) ? $cardNumber: null,
                                    "DateOfBirth"=>isset($dateOfBirth)  ?  $dateOfBirth."T00:00:00" : "0001-01-01T00:00:00Z",
                                    "ExpiryDate"=>isset($expiryDate)  ? $expiryDate."T00:00:00" : "0001-01-01T00:00:00Z",
                                    "LoyaltySessionExpiry"=>isset($loyaltySessionExpiry)  ? $loyaltySessionExpiry."T00:00:00": "0001-01-01T00:00:00Z",
                                    "IsBannedFromMakingUnpaidBookingsUntil"=>isset($isBannedFromMakingUnpaidBookingsUntil)  ? $isBannedFromMakingUnpaidBookingsUntil."T00:00:00" : "0001-01-01T00:00:00Z",
                                    "Status"=>isset($status)  ? $status : null,
                                    "CardList"=>array("string"=>isset($cardList)  ? $cardList : null ),
                                    "ClubID"=>isset($clubId)  ? $clubId : null,
                                    "SendNewsletter"=>false,
                                    "EducationLevel"=>0,   
                                    "HouseholdIncome"=>0,
                                    "PersonsInHousehold"=>0,
                                    "PickupComplex"=>0,  
                                    "PreferredComplex"=>0, 
                                    "ContactByThirdParty"=>false,
                                    "GiftCard"=>false,
                                    "GiftCardBalance"=>0,
                                    "MembershipActivated"=>true,
                                    "IsAnonymous"=>false,
                                    "WishToReceiveSMS"=>true,
                                     "Occupation"=>0,  
                                    "MemberLevelId"=>0, 
                                    "IsAnonymous"=>false                                            
            ),
            
        );
        
        if(isset($memberId)){
            $params["LoyaltyMember"]["MemberId"]=$memberId;
        }
        return $params;
    }



    public function mapeaUpdateMember($request){
        $userSessionId= $request->request->get('sessionid');
        if($request->request->get('memberid')){
            $memberId= $request->request->get('memberid');     
        }
        if($request->request->get('isactivated')){
            $isActivated= $request->request->get('isactivated');     
        }
        $primerNombre= $request->request->get('firstname');
        $apellido= $request->request->get('lastname');
        $mobilePhone= $request->request->get('mobilephone');
        $email= $request->request->get('email');
        $cardNumber= $request->request->get('cardnumber');
        $homePhone= $request->request->get('homephone');
        $dateOfBirth = $request->request->get('dateofbirth');
        $expiryDate = $request->request->get('expirydate');
        $loyaltySessionExpiry = $request->request->get('loyaltysessionexpiry');
        $isBannedFromMakingUnpaidBookingsUntil=  $request->request->get('isbannedfrommakingunpaidbookingsuntil');
        $clubId=  $request->request->get('clubid');
        $status=  $request->request->get('status');
        $cardList=  $request->request->get('cardlist');
        $params = array(
            "OptionalClientClass"=>$this->params->get('soap.client_class'),
            "OptionalClientId" =>$this->params->get('soap.client_id'),
            "OptionalClientName" =>$this->params->get("soap.client_name"),
            "UserSessionId"=>$userSessionId,
            "UpdatePreferences"=>true,
            "LoyaltyMember"=>array(
                                    "FirstName"=>isset($primerNombre) ? $primerNombre : null ,
                                    "LastName"=>isset($apellido)  ? $apellido : null,
                                    "MobilePhone"=>isset($mobilePhone)  ? $mobilePhone : null,
                                    "HomePhone"=>isset($homePhone)  ? $homePhone : null,
                                    "Email"=>isset($email)  ? $email : null,
                                    "CardNumber"=>isset($cardNumber) ? $cardNumber: null,
                                    "DateOfBirth"=>isset($dateOfBirth)  ?  $dateOfBirth."T00:00:00" : "0001-01-01T00:00:00Z",
                                    "ExpiryDate"=>isset($expiryDate)  ? $expiryDate."T00:00:00" : "0001-01-01T00:00:00Z",
                                    "LoyaltySessionExpiry"=>isset($loyaltySessionExpiry)  ? $loyaltySessionExpiry."T00:00:00": "0001-01-01T00:00:00Z",
                                    "IsBannedFromMakingUnpaidBookingsUntil"=>isset($isBannedFromMakingUnpaidBookingsUntil)  ? $isBannedFromMakingUnpaidBookingsUntil."T00:00:00" : "0001-01-01T00:00:00Z",
                                    "Status"=>isset($status)  ? $status : null,
                                    "CardList"=>array("string"=>isset($cardList)  ? $cardList : null ),
                                    "ClubID"=>isset($clubId)  ? $clubId : null,
                                    "SendNewsletter"=>true,
                                    "EducationLevel"=>0,   
                                    "HouseholdIncome"=>0,
                                    "PersonsInHousehold"=>0,
                                    "PickupComplex"=>0,  
                                    "PreferredComplex"=>0, 
                                    "ContactByThirdParty"=>false,
                                    "GiftCard"=>false,
                                    "GiftCardBalance"=>0,
                                    "MembershipActivated"=>true,
                                    "IsAnonymous"=>false,
                                    "WishToReceiveSMS"=>true,
                                     "Occupation"=>0,  
                                    "MemberLevelId"=>0, 
                                    "IsAnonymous"=>false,                                                     
            ),
        );
        if(isset($memberId)){
            $params["LoyaltyMember"]["MemberId"]=$memberId;
        }
        return $params;
    }

   
    public function mapeaActiveMember($request){
        $userSessionId= $request->request->get('sessionid');
        if($request->request->get('memberid')){
            $memberId= $request->request->get('memberid');     
        }
        if($request->request->get('isactivated')){
            $isActivated= $request->request->get('isactivated');     
        }
        $cardNumber= $request->request->get('cardnumber');
        $dateOfBirth = $request->request->get('dateofbirth');
        $expiryDate = $request->request->get('expirydate');
        $loyaltySessionExpiry = $request->request->get('loyaltysessionexpiry');
        $isBannedFromMakingUnpaidBookingsUntil=  $request->request->get('isbannedfrommakingunpaidbookingsuntil');
        
        
        $params = array(
            "OptionalClientClass"=>$this->params->get('soap.client_class'),
            "OptionalClientId" =>$this->params->get('soap.client_id'),
            "OptionalClientName" =>$this->params->get("soap.client_name"),
            "UserSessionId"=>$userSessionId,
            
            "LoyaltyMember"=>array(
                                    "CardNumber"=>isset($cardNumber) ? $cardNumber: null,
                                     "SendNewsletter"=>false,
                                    "EducationLevel"=>0,   
                                    "HouseholdIncome"=>0,
                                    "PersonsInHousehold"=>0,
                                    "PickupComplex"=>0,  
                                    "PreferredComplex"=>0, 
                                    "ContactByThirdParty"=>false,
                                    "GiftCard"=>false,
                                    "GiftCardBalance"=>0,
                                    "MembershipActivated"=>true,
                                    "IsAnonymous"=>false,
                                    "WishToReceiveSMS"=>true,
                                     "Occupation"=>0,  
                                    "MemberLevelId"=>0, 
                                    "IsAnonymous"=>false,
                                    "DateOfBirth"=>isset($dateOfBirth)  ?  $dateOfBirth."T00:00:00" : "0001-01-01T00:00:00Z",
                                    "ExpiryDate"=>isset($expiryDate)  ? $expiryDate."T00:00:00" : "0001-01-01T00:00:00Z",
                                    "LoyaltySessionExpiry"=>isset($loyaltySessionExpiry)  ? $loyaltySessionExpiry."T00:00:00": "0001-01-01T00:00:00Z",
                                    "IsBannedFromMakingUnpaidBookingsUntil"=>isset($isBannedFromMakingUnpaidBookingsUntil)  ? $isBannedFromMakingUnpaidBookingsUntil."T00:00:00" : "0001-01-01T00:00:00Z",
                                                                                                   
            ),
        );
        if(isset($isActivated)){    
           $params["IsActivated"]=$isActivated;
        }else{
            $params["IsActivated"]=0;
        }
        if(isset($memberId)){
            $params["MemberId"]=$memberId;
        }
        return $params;
    }


    

}
