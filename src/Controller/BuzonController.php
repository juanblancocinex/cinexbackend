<?php

namespace App\Controller;
use App\CoreCinex\CinexService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use App\Lib\DaoLib;
use App\Lib\ApiUtilFunctions;
use App\Lib\Constantes;
use App\Entity\Buzon;
use App\Lib\DataTableSsp;

class BuzonController extends Controller
{
     

    // Buzon URI"'s
    /**
     * @Rest\Get("/api/buzon/correos", name="getbuzon", defaults={"_format":"json"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Lista de Buzon."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Buzon."
     * )
     *
     * @SWG\Parameter(
     *     name="email",
     *     in="query",
     *     type="string",
     *     description="email"
     * )
     *
     * @SWG\Tag(name="Buzon")
     */
    public function getBuzonAction(Request $request) {
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        if ($request->query->get('email')!=""){
            $email =$request->query->get('email');
        }
        $data= [];
        $data = $em->getRepository("App:Buzon")->getBuzon($email);
        if (is_null($data)) {
            $data = [];
        }
        return $http->responseAcceptHttp(array("data"=>$data));
    }
   
    

    /**
     * @Rest\Get("/api/buzon/miscorreos", name="getmiscorreos", defaults={"_format":"json"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Lista de Correos."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Mis Correos."
     * )
     *
     * @SWG\Parameter(
     *     name="search",
     *     in="query",
     *     type="string",
     *     description="search"
     * )
     *
     * @SWG\Tag(name="MisCorreos")
     */
    public function getMiscorreosAction(Request $request) {
        
        $dataTable = new DataTableSsp();



        $table=" usuario a right join buzon b on
        a.usuarioid = b.usuarioid ";
        $primaryKey = 'id';

        $whereOptional = array(array("campo"=>"a.correoElectronico",
                                     "valor"=>$request->query->get('email'),
                                     "tipodato"=>"string",
                                     "operador"=>"=",
                                     "operadorlogico"=>"and"),array("campo"=>"b.usuarioId",
                                     "valor"=>-1,
                                     "tipodato"=>"integer",
                                     "operador"=>"=",
                                     "operadorlogico"=>"and"));
        $columns = array(
            array( 'db' => 'b.lectura','dt' => 0,'alias'=>false,"nombreindice"=>"lectura" ),
            array( 'db' => '"Equipo Cinex" as DE','dt' => 1 ,'alias'=>false,"nombreindice"=>"DE"),
            array( 'db' => 'b.asunto','dt' => 2,'alias'=>false,"nombreindice"=>"asunto" ),
            array( 'db' => 'b.contenido','dt' => 3,'alias'=>false ,"nombreindice"=>"contenido"),
            array( 'db' => 'IF(b.fechasistema,date_format(b.fechasistema, "%d/%m/%Y"),"") as fecha', 'dt' => 4 ,'alias'=>true,"nombreindice"=>"fecha"),
            array( 'db' => 'b.id','dt' => 5,'alias'=>false,"nombreindice"=>"id" ),
        );
  

        $em = $this->getDoctrine()->getManager();  

        $data = $dataTable->simple( $request,$em, $table, $primaryKey, $columns,$whereOptional);
       
        foreach($data["data"] as $clave=>$valor){
           if($data["data"][$clave][0]==1){
                $data["data"][$clave][0]="<i class='fas fa-envelope-open'></i>";
           }else{
                $data["data"][$clave][0]="<i class='fas fa-envelope'></i>";
           }
        }
       
        $http = $this->get('httpcustom');
        // $em = $this->getDoctrine()->getManager();
        // if ($request->query->get('email')!=""){
        //     $parameter["email"] =$request->query->get('email');
        // }
        // if ($request->query->get('search')!=""){
        //     $parameter["search"] =$request->query->get('search');
        // }
        
                
        // $data = $em->getRepository("App:Buzon")->getMisCorreos($parameter);


        // if (is_null($data)) {
        //     $data = [];
        // }

        // $data = array(
        //     array('DE'=>'parvez','ASUNTO'=>11, 'MENSAJE'=>11, 'FECHA'=>101),
        //     array('DE'=>'alam','ASUNTO'=>11, 'MENSAJE'=>1, 'FECHA'=>102),
        //     array('DE'=>'phpflow','ASUNTO'=>11, 'MENSAJE'=>21, 'FECHA'=>103)							
        // );
               
        //  $data =array("draw"=>0,"recordsTotal"=>3,"recordsFiltered"=>3,"data"=>array(["1","jblanco"],["6","ghensy"],["7","lorena"]));   
            
        //     $results = array(
        //         "sEcho" => 1,
        //         "iTotalRecords" => count($data),
        //         "iTotalDisplayRecords" => count($data),
        //         "aaData"=>$data
        //     );


            //$data =  array("draw"=>1,"recordsTotal"=>3,"recordsFiltered"=>3,"data"=>$data); 
     
        return $http->responseAcceptHttpWithoutFormat( $data);
    }

}
