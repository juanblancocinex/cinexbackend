<?php
namespace App\Controller;
use App\CoreCinex\CinexService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use App\Lib\DaoLib;
use App\Lib\ApiUtilFunctions;
use App\Lib\Constantes;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Translation\TranslatorInterface;
use App\Lib\XmlLib;
use App\Entity\TokenTrans;

class InstaPagoController extends Controller
{     
        protected $params;
        protected $xmlLib;
        protected $apiUtilFunctions;       

        /**
         * Constructor.
        **/
        public function __construct(ParameterBagInterface $params)
        {
            $this->apiUtilFunctions= new ApiUtilFunctions();
            $this->xmlLib= new XmlLib();
            $this->params=$params;
        }
        
        /**
         * @Rest\Get("/api/payment/instapagocompletarpago/keyid/{keyid}/publickeyid/{publickeyid}/id/{id}", name="instapagoanularpago", defaults={"_format":"json"})
         *
         * @SWG\Parameter(
         *     name="keyid",
         *     in="path",
         *     type="string",
         *     description="Llave generada desde InstaPago"
         * )
         * @SWG\Parameter(
         *     name="publickeyid",
         *     in="path",
         *     type="string",
         *     description="Llave compartida"
         * )
         * @SWG\Parameter(
         *     name="id",
         *     in="path",
         *     type="string",
         *     description="Identificador único del pago."
         * )
         * @SWG\Response(
         *     response=200,
         *     description="Array con la respuesta del servicio."
         * )
         * @SWG\Response(
         *     response=500,
         *     description="Ocurrio un error al ejecutar InstaPagoAnularPago"
         * )#
         *
         * @SWG\Tag(name="InstaPagoAnularPago")
         */
        
        public function InstaPagoAnularPago($keyid, $publickeyid, $id){
            $data = "";
            $ch = null;
            $output = null;
            $response = null;
            $json_response = null;
            $fields = array();
            $http = $this->get('httpcustom');
            $url = 'https://api.instapago.com/payment';
            $fields = array("KeyID" => $keyid , "PublicKeyId" => $publickeyid, "id" => $id);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $output = curl_exec ($ch);
            curl_close ($ch);
            $json_response = json_decode($output);
            if ($json_response->success){
                $response = array("codRespuesta"=>"1", "desRespuesta"=>"Se ha anulado el pago exitosamente", "request"=>$fields, "response" => $output, "method" => "InstaPagoCompletarPago");
            }else{
                $response = array("codRespuesta"=>"0", "desRespuesta"=>"Ha ocurrido un error al anular el pago", "request"=>$fields, "response" => $output, "method" => "InstaPagoCompletarPago");
            }
            return $http->responsePaymentAcceptHttp($response);
        }        
        
        /**
         * @Rest\Get("/api/payment/instapagoconsultarpago/keyid/{keyid}/publickeyid/{publickeyid}/id/{id}", name="instapagoconsultarpago", defaults={"_format":"json"})
         *
         * @SWG\Parameter(
         *     name="keyid",
         *     in="path",
         *     type="string",
         *     description="Llave generada desde InstaPago"
         * )
         * @SWG\Parameter(
         *     name="publickeyid",
         *     in="path",
         *     type="string",
         *     description="Llave compartida"
         * )
         * @SWG\Parameter(
         *     name="id",
         *     in="path",
         *     type="string",
         *     description="Identificador único del pago."
         * )
         * @SWG\Response(
         *     response=200,
         *     description="Array con la respuesta del servicio."
         * )
         * @SWG\Response(
         *     response=500,
         *     description="Ocurrio un error al ejecutar InstaPagoConsultarPago"
         * )#
         *
         * @SWG\Tag(name="InstaPagoConsultarPago")
         */
        
        public function InstaPagoConsultarPago($keyid, $publickeyid, $id){
            $data = "";
            $ch = null;
            $output = null;
            $response = null;
            $json_response = null;
            $http = $this->get('httpcustom');
            $url = 'https://api.instapago.com/payment?KeyID='.$keyid."&PublicKeyId=".$publickeyid."&id=".$id;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                        
            $output = curl_exec ($ch);
            curl_close ($ch);
            $json_response = json_decode($output);
            if ($json_response->success){
                $response = array("codRespuesta"=>"1", "desRespuesta"=>"Se ha consultado el pago exitosamente", "request"=>$url, "response" => $output, "method" => "InstaPagoCompletarPago");
            }else{
                $response = array("codRespuesta"=>"0", "desRespuesta"=>"Ha ocurrido un error al consultar el pago", "request"=>$url, "response" => $output, "method" => "InstaPagoCompletarPago");
            }
            return $http->responsePaymentAcceptHttp($response);
        }
        
        /**
         * @Rest\Get("/api/payment/instapagoconsultapagolotes/keyid/{keyid}/publickeyid/{publickeyid}/terminal/{terminal}/lote/{lote}", name="instapagoconsultapagolotes", defaults={"_format":"json"})
         *
         * @SWG\Parameter(
         *     name="keyid",
         *     in="path",
         *     type="string",
         *     description="Llave generada desde InstaPago"
         * )
         * @SWG\Parameter(
         *     name="publickeyid",
         *     in="path",
         *     type="string",
         *     description="Llave compartida"
         * )
         * @SWG\Parameter(
         *     name="terminal",
         *     in="path",
         *     type="string",
         *     description="Numero de orden del pago según el comercio."
         * )
         * @SWG\Parameter(
         *     name="lote",
         *     in="path",
         *     type="string",
         *     description="Numero de orden del pago según el comercio."
         * )          
         * @SWG\Response(
         *     response=200,
         *     description="Array con la respuesta del servicio."
         * )
         * @SWG\Response(
         *     response=500,
         *     description="Ocurrio un error al ejecutar InstaPagoConsultaPagoLotes"
         * )#
         *
         * @SWG\Tag(name="InstaPagoConsultaPagoLotes")
         */
        
        public function InstaPagoConsultaPagoLotes($keyid, $publickeyid, $terminal, $lote){
            $data = "";
            $ch = null;
            $output = null;
            $response = null;
            $json_response = null;
            $http = $this->get('httpcustom');
            $url = 'https://api.instapago.com/reports?KeyID='.$keyid."&PublicKeyId=".$publickeyid."&Terminal=".$terminal."&Lote=".$lote;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $output = curl_exec ($ch);
            curl_close ($ch);            
            $response = array("codRespuesta"=>"1", "desRespuesta"=>"Se ha consultado el pago por lote exitosamente", "request"=>$url, "response" => $output, "method" => "InstaPagoCompletarPago");
            return $http->responsePaymentAcceptHttp($response);
        }
        
        
        /**
         * @Rest\Get("/api/payment/instapagoconsultapagolotes/keyid/{keyid}/publickeyid/{publickeyid}/terminal/{terminal}/startdate/{startdate}/enddate/{enddate}", name="instapagoconsultapagofecha", defaults={"_format":"json"})
         *
         * @SWG\Parameter(
         *     name="keyid",
         *     in="path",
         *     type="string",
         *     description="Llave generada desde InstaPago"
         * )
         * @SWG\Parameter(
         *     name="publickeyid",
         *     in="path",
         *     type="string",
         *     description="Llave compartida"
         * )
         * @SWG\Parameter(
         *     name="terminal",
         *     in="path",
         *     type="string",
         *     description="Numero de orden del pago según el comercio."
         * )
         * @SWG\Parameter(
         *     name="startdate",
         *     in="path",
         *     type="string",
         *     description="Fecha inicial de la consulta."
         * )
         * @SWG\Parameter(
         *     name="enddate",
         *     in="path",
         *     type="string",
         *     description="Fecha final de la consulta."
         * )
         * @SWG\Response(
         *     response=200,
         *     description="Array con la respuesta del servicio."
         * )
         * @SWG\Response(
         *     response=500,
         *     description="Ocurrio un error al ejecutar InstaPagoConsultaPagoFecha"
         * )#
         *
         * @SWG\Tag(name="InstaPagoConsultaPagoFecha")
         */
        
        public function InstaPagoConsultaPagoFecha($keyid, $publickeyid, $terminal, $startdate, $enddate){
            $data = "";
            $ch = null;
            $output = null;
            $response = null;
            $json_response = null;
            $http = $this->get('httpcustom');
            $url = 'https://api.instapago.com/reports?KeyID='.$keyid."&PublicKeyId=".$publickeyid."&Terminal=".$terminal."&StartDate=".str_replace("-", "/", $startdate)."&EndDate=".str_replace("-", "/", $enddate);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $output = curl_exec ($ch);
            curl_close ($ch);
            $response = array("codRespuesta"=>"1", "desRespuesta"=>"Se ha consultado el pago por lote exitosamente", "request"=>$url, "response" => $output, "method" => "InstaPagoCompletarPago");
            return $http->responsePaymentAcceptHttp($response);
        }
        
        
        /**
         * @Rest\Get("/api/payment/instapagoconsultarordenpago/keyid/{keyid}/publickeyid/{publickeyid}/ordernumber/{ordernumber}", name="instapagoconsultarordenpago", defaults={"_format":"json"})
         *
         * @SWG\Parameter(
         *     name="keyid",
         *     in="path",
         *     type="string",
         *     description="Llave generada desde InstaPago"
         * )
         * @SWG\Parameter(
         *     name="publickeyid",
         *     in="path",
         *     type="string",
         *     description="Llave compartida"
         * )
         * @SWG\Parameter(
         *     name="ordernumber",
         *     in="path",
         *     type="string",
         *     description="Numero de orden del pago según el comercio."
         * )
         * @SWG\Response(
         *     response=200,
         *     description="Array con la respuesta del servicio."
         * )
         * @SWG\Response(
         *     response=500,
         *     description="Ocurrio un error al ejecutar InstaPagoConsultarOrdenPago"
         * )#
         *
         * @SWG\Tag(name="InstaPagoConsultarOrdenPago")
         */
        
        public function InstaPagoConsultarOrdenPago($keyid, $publickeyid, $ordernumber){
            $data = "";
            $ch = null;
            $output = null;
            $response = null;
            $json_response = null;
            $http = $this->get('httpcustom');
            $url = 'https://api.instapago.com/query?KeyID='.$keyid."&PublicKeyId=".$publickeyid."&OrderNumber=".$ordernumber;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $output = curl_exec ($ch);
            curl_close ($ch);
            $json_response = json_decode($output);
            if ($json_response->success){
                $response = array("codRespuesta"=>"1", "desRespuesta"=>"Se ha consultado el pago exitosamente", "request"=>$url, "response" => $output, "method" => "InstaPagoCompletarPago");
            }else{
                $response = array("codRespuesta"=>"0", "desRespuesta"=>"Ha ocurrido un error al consultar el pago", "request"=>$url, "response" => $output, "method" => "InstaPagoCompletarPago");
            }
            return $http->responsePaymentAcceptHttp($response);
        }               
        
        /**
         * @Rest\Get("/api/payment/instapagocompletarpago/keyid/{keyid}/publickeyid/{publickeyid}/id/{id}/amount/{amount}", name="instapagocompletarpago", defaults={"_format":"json"})
         *
         * @SWG\Parameter(
         *     name="keyid",
         *     in="path",
         *     type="string",
         *     description="Llave generada desde InstaPago"
         * )
         * @SWG\Parameter(
         *     name="publickeyid",
         *     in="path",
         *     type="string",
         *     description="Llave compartida"
         * )
         * @SWG\Parameter(
         *     name="id",
         *     in="path",
         *     type="string",
         *     description="Identificador único del pago."
         * )
         * @SWG\Parameter(
         *     name="amount",
         *     in="path",
         *     type="string",
         *     description="Monto a Debitar, utilizando punto '.' como separador decimal. Por ejemplo: 200.00"
         * )
         * @SWG\Response(
         *     response=200,
         *     description="Array con la respuesta del servicio."
         * )
         * @SWG\Response(
         *     response=500,
         *     description="Ocurrio un error al ejecutar InstaPagoCompletarPago"
         * )#
         *
         * @SWG\Tag(name="InstaPagoCompletarPago")
         */
        
        public function InstaPagoCompletarPago($keyid, $publickeyid, $id, $amount){
            $data = "";
            $ch = null;
            $output = null;
            $response = null;
            $json_response = null;
            $fields = array();
            $http = $this->get('httpcustom');
            $url = 'https://api.instapago.com/complete';
            $fields = array("KeyID" => $keyid , "PublicKeyId" => $publickeyid, "id" => $id, "Amount" => $amount);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $output = curl_exec ($ch);
            curl_close ($ch);
            $json_response = json_decode($output);
            if ($json_response->success){
                $response = array("codRespuesta"=>"1", "desRespuesta"=>"Se ha completado el pago exitosamente", "request"=>$fields, "response" => $output, "method" => "InstaPagoCompletarPago");
            }else{
                $response = array("codRespuesta"=>"0", "desRespuesta"=>"Ha ocurrido un error al completar el pago", "request"=>$fields, "response" => $output, "method" => "InstaPagoCompletarPago");
            }
            return $http->responsePaymentAcceptHttp($response);
        }                
       
        /**
         * @Rest\Get("/api/payment/instapagocrearpago/keyid/{keyid}/publickeyid/{publickeyid}/amount/{amount}/description/{description}/cardholder/{cardholder}/cardholderid/{cardholderid}/cardnumber/{cardnumber}/cvc/{cvc}/expirationdate/{expirationdate}/statusid/{statusid}/ip/{ip}/ordernumber/{ordernumber}/address/{address}/city/{city}/zipcode/{zipcode}/state/{state}", name="InstaPagoCrearPago", defaults={"_format":"json"})
         *
         * @SWG\Parameter(
         *     name="keyid",
         *     in="path",
         *     type="string",
         *     description="Llave generada desde InstaPago"
         * )
         * @SWG\Parameter(
         *     name="publickeyid",
         *     in="path",
         *     type="string",
         *     description="Llave compartida"
         * )
         * @SWG\Parameter(
         *     name="amount",
         *     in="path",
         *     type="string",
         *     description="Monto a Debitar, utilizando punto '.' como separador decimal. Por ejemplo: 200.00"
         * )
         * @SWG\Parameter(
         *     name="description",
         *     in="path",
         *     type="string",
         *     description="Cadena de caracteres con la descripción de la operación."
         * )
         * @SWG\Parameter(
         *     name="cardholder",
         *     in="path",
         *     type="string",
         *     description="Nombre del Tarjeta habiente."
         * )
         * @SWG\Parameter(
         *     name="cardholderid",
         *     in="path",
         *     type="string",
         *     description="Cédula del Tarjeta habiente."
         * )
         * @SWG\Parameter(
         *     name="cardnumber",
         *     in="path",
         *     type="integer",
         *     description="Numero de la tarjeta de crédito, sin espacios ni separadores."
         * )
         * @SWG\Parameter(
         *     name="cvc",
         *     in="path",
         *     type="integer",
         *     description="Código secreto de la Tarjeta de crédito."
         * )
         * @SWG\Parameter(
         *     name="expirationdate",
         *     in="path",
         *     type="string",
         *     description="Fecha de expiración de la tarjeta en el formato mostrado en la misma MM-YYYY. Por Ejemplo: 10-2014."
         * ) 
         * @SWG\Parameter(
         *     name="statusid",
         *     in="path",
         *     type="integer",
         *     description="Estatus en el que se creará la transacción. 1 - PAGAR | 2 - RETENER."
         * )
         * @SWG\Parameter(
         *     name="ip",
         *     in="path",
         *     type="string",
         *     description="Dirección IP del cliente que genera la solicitud del pago."
         * )
         * @SWG\Parameter(
         *     name="ordernumber",
         *     in="path",
         *     type="string",
         *     description="Numero de orden del pago según el comercio."
         * )
         * @SWG\Parameter(
         *     name="address",
         *     in="path",
         *     type="string",
         *     description="Dirección asociada a la tarjeta, Utilizada por algunos bancos para mayor seguridad."
         * )
         * @SWG\Parameter(
         *     name="city",
         *     in="path",
         *     type="string",
         *     description="Ciudad asociada a la tarjeta, Utilizada por algunos bancos para mayor seguridad."
         * )
         * @SWG\Parameter(
         *     name="zipcode",
         *     in="path",
         *     type="string",
         *     description="Código Postal asociada a la tarjeta, Utilizada por algunos bancos para mayor seguridad."
         * )
         * @SWG\Parameter(
         *     name="state",
         *     in="path",
         *     type="string",
         *     description="Estado o provincia asociada a la tarjeta, Utilizada por algunos bancos para mayor seguridad.."
         * )
         * @SWG\Response(
         *     response=200,
         *     description="Array con la respuesta del servicio."
         * )
         * @SWG\Response(
         *     response=500,
         *     description="Ocurrio un error al ejecutar InstaPagoCrearPago"
         * )#
         *
         * @SWG\Tag(name="InstaPagoCrearPago")
         */
        
        public function InstaPagoCrearPago($keyid, $publickeyid, $amount, $description, $cardholder, $cardholderid, $cardnumber, $cvc, $expirationdate, $statusid, $ip, $ordernumber, $address, $city, $zipcode, $state){
            $data = "";
            $ch = null;
            $output = null;
            $response = null;
            $json_response = null;
            $fields = array();
            $http = $this->get('httpcustom'); 
            $url = 'https://api.instapago.com/payment';
            $fields = array("KeyID" => $keyid , "PublicKeyId" => $publickeyid, "Amount" => $amount,"Description" => $description,"CardHolder"=> $cardholder,"CardHolderId"=> $cardholderid, "CardNumber" => $cardnumber, "CVC" => $cvc, "ExpirationDate" => str_replace("-", "/", $expirationdate), "StatusId" => $statusid, "IP" => $ip, "Address" => urlencode($address), "City" => urlencode($city), "ZipCode" => $zipcode, "State" => urlencode($state));
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $output = curl_exec ($ch);
            curl_close ($ch);
            $json_response = json_decode($output);
            if ($json_response->success){
                $response = array("codRespuesta"=>"1", "desRespuesta"=>"Se ha creado el pago exitosamente", "id" => $json_response->id, "request"=>$fields, "response" => $output, "method" => "instaPagoCrearPago");
            }else{
                $response = array("codRespuesta"=>"0", "desRespuesta"=>"Ha ocurrido un error al crear el pago", "id" => $json_response->id, "request"=>$fields, "response" => $output, "method" => "instaPagoCrearPago");
            }                       
            return $http->responsePaymentAcceptHttp($response);
        }
}
