<?php


namespace App\Controller;
use App\CoreCinex\CinexService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use App\Lib\DaoLib;
use App\Lib\ApiUtilFunctions;
use App\Lib\Constantes;


class MotivosContactoController extends Controller
{
     
    
 
     // GetContactosMotivos URI"'s
    /**
     * @Rest\Get("/api/contactos/motivos", name="getcontactosmotivos", defaults={"_format":"json"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Ciudades y Complejos."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Complejos por Ciudades"
     * )
     *
     * @SWG\Tag(name="GetContactosMotivos")
     */    
    public function getContactosMotivosAction() {
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $data= [];
        $data = $em->getRepository("App:Motivoscontacto")->getMotivosContactos();
        return $http->responseAcceptHttp(array("data"=>$data));
    }


    

}
