<?php


namespace App\Controller;
use App\CoreCinex\CinexService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use App\Lib\DaoLib;
use App\Lib\ApiUtilFunctions;
use App\Lib\Constantes;
use App\Entity\OcupacionMapa;

class OcupacionMapaController extends Controller
{
     
    
 
     // Ocupación de la Sala URI"'s
    /**
     * @Rest\Get("/api/cinexdataservice/getmapoccupation/sesionid/{sesionid}/userid/{userid}", name="getmapoccupation", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="sesionid",
     *     in="path",
     *     type="string",
     *     description="Identificador de la sesión en VISTA"
     * )
     * @SWG\Parameter(
     *     name="userid",
     *     in="path",
     *     type="string",
     *     description="Identificador del usuario en la db cinex"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Ocupación del mapa."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de GetMapOccupation"
     * )
     *
     * @SWG\Tag(name="CinexDataServiceGetMapOccupation")
     */
    
    public function getMapOccupationAction($sesionid,$userid ) {        
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $data = $em->getRepository("App:OcupacionMapa")->getOccupationMap($sesionid, $userid);
        return $http->responseAcceptHttp(array("data"=>$data));
    }
    
      
    
    
    
    // Ocupación de la Sala URI"'s
    /**
     * @Rest\Get("/api/cinexdataservice/getcountoccupatedseats/sesionid/{sesionid}/userid/{userid}", name="getcountoccupatedseats", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="sesionid",
     *     in="path",
     *     type="string",
     *     description="Identificador de la sesión en VISTA"
     * )
     * @SWG\Parameter(
     *     name="userid",
     *     in="path",
     *     type="string",
     *     description="Identificador del usuario en la db cinex"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Cantidad de Asientos Seleccionados."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar  getCountOccupatedSeats"
     * )
     *
     * @SWG\Tag(name="CinexDataServiceGetCountMapOccupation")
     */
    public function getCountOccupatedSeatsAction($sesionid,$userid) {        
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $data = $em->getRepository("App:OcupacionMapa")->getCountOccupatedSeats($sesionid, $userid);
        return $http->responseAcceptHttp(array("data"=>$data));
    }
    

     // Lista de Asientos Ocupados por el usuario  URI"'s
    /**
     * @Rest\Get("/api/cinexdataservice/getlistoccupatedseats/sesionid/{sesionid}/userid/{userid}", name="getlistoccupatedSeats", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="sesionid",
     *     in="path",
     *     type="string",
     *     description="Identificador de la sesión en VISTA"
     * )
     * @SWG\Parameter(
     *     name="userid",
     *     in="path",
     *     type="string",
     *     description="Identificador del usuario en la db cinex"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Lista de Asientos Ocupados."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar GetListOccupatedSeats"
     * )
     *
     * @SWG\Tag(name="CinexDataServiceGetListOccupatedSeats")
     */
    public function getListOccupatedSeatsAction($sesionid,$userid) {        
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $data = $em->getRepository("App:OcupacionMapa")->getListOccupatedSeats($sesionid, $userid);
        return $http->responseAcceptHttp(array("data"=>$data));
    }  


    // Estatus de un asiento de una funcion indicado URI"'s
    /**
     * @Rest\Get("/api/cinexdataservice/getseatstatus/sesionid/{sesionid}/fila/{fila}/columna/{columna}", name="getseatstatus", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="sesionid",
     *     in="path",
     *     type="string",
     *     description="Identificador de la sesión en VISTA"
     * )
     * @SWG\Parameter(
     *     name="fila",
     *     in="path",
     *     type="string",
     *     description="Fila"
     * )
     * @SWG\Parameter(
     *     name="columna",
     *     in="path",
     *     type="string",
     *     description="Columna"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Estatus de Silla."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar GetSeatStatus"
     * )
     *
     * @SWG\Tag(name="CinexDataServiceGetSeatStatus")
     */
    public function getSeatStatusAction($sesionid, $fila,$columna) {        
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $data = $em->getRepository("App:OcupacionMapa")->getSeatStatus($sesionid, $fila,$columna);
        if(count($data)>0){
            $data = array("result"=>true, "response"=>"OK", "seatStatus"=>"Asiento Ocupado", "usuarioID"=>$data[0]["ocupId"]);
        }else{
            $data = array("result"=>true, "response"=>"OK", "seatStatus"=>"Asiento Disponible");
        }
        return $http->responseAcceptHttp(array("data"=>$data));
    }

    //  Asientos ocupados en una sesión en VISTA URI"'s
    /**
     * @Rest\Get("/api/cinexdataservice/getoccupatedseats/sesionid/{sesionid}", name="getoccupatedseats", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="sesionid",
     *     in="path",
     *     type="string",
     *     description="Identificador de la sesión en VISTA"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Estatus de Silla."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar GetOccupatedSeats"
     * )
     *
     * @SWG\Tag(name="CinexDataServiceGetoccupatedseats")
     */
    public function getOccupatedSeatsAction($sesionid) {        
        $http = $this->get('httpcustom');
        $dataReturn="";
        $em = $this->getDoctrine()->getManager();
        $data = $em->getRepository("App:OcupacionMapa")->getOccupatedSeats($sesionid);
        foreach($data as $clave=>$valor){
            foreach($valor as $cl=>$vl){
                $dataReturn.= $data[$clave][$cl]."|";
            }    
            $dataReturn.= ";";
        }
        if(count($data)>0){
            $data = array("result"=>true, "response"=>"OK", "ocuppatedSeats"=>$dataReturn);
        }else{
            $data = array("result"=>true, "response"=>"OK", "ocuppatedSeats"=>"none");
        }
        return $http->responseAcceptHttp(array("data"=>$data));
    }

       //  Asientos ocupados en una sesión por un usuario en VISTA URI"'s
    /**
     * @Rest\Get("/api/cinexdataservice/getoccupatedseatsfromuserid/sesionid/{sesionid}/userid/{userid}", name="getoccupatedseatsfromuserid", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="sesionid",
     *     in="path",
     *     type="string",
     *     description="Identificador de la sesión en VISTA"
     * )
     * @SWG\Parameter(
     *     name="userid",
     *     in="path",
     *     type="string",
     *     description="Cadena con el identificador del usuario en la db cinex"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Estatus de Silla."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar GetOccupatedSeats"
     * )
     *
     * @SWG\Tag(name="CinexDataServiceGetOccupatedSeatsFromUserId")
     */
    public function getOccupatedSeatsFromUserIdAction($sesionid, $userid) {        
        $http = $this->get('httpcustom');
        $dataReturn="";
        $em = $this->getDoctrine()->getManager();
        $data = $em->getRepository("App:OcupacionMapa")->getOccupatedSeatsFromUser($sesionid, $userid);
        foreach($data as $clave=>$valor){
            foreach($valor as $cl=>$vl){
                $dataReturn.= $data[$clave][$cl]."|";
            }    
            $dataReturn.= ";";
        }
        if(count($data)>0){
            $data = array("result"=>true, "response"=>"OK", "ocuppatedSeats"=>$dataReturn);
        }else{
            $data = array("result"=>true, "response"=>"OK", "ocuppatedSeats"=>"none");
        }
        return $http->responseAcceptHttp(array("data"=>$data));
    }


   //  Ultimos asientos ocupados en una sesión por un usuario en VISTA URI"'s
    /**
     * @Rest\Get("/api/cinexdataservice/getlastoccupatedseatsfromuserid/sesionid/{sesionid}/userid/{userid}", name="getlastoccupatedseatsfromUseriD", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="sesionid",
     *     in="path",
     *     type="string",
     *     description="Identificador de la sesión en VISTA"
     * )
     * @SWG\Parameter(
     *     name="userid",
     *     in="path",
     *     type="integer",
     *     description="Cadena con el identificador del usuario en la db cinex"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Estatus de Ultimas Sillas."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar getLastOccupatedSeats"
     * )
     *
     * @SWG\Tag(name="CinexDataServiceGetLastOccupatedSeatsFromUserID")
     */
    public function getLastOccupatedSeatsFromUserIDAction($sesionid, $userid) {        
        $http = $this->get('httpcustom');
        $dataReturn="";
        $em = $this->getDoctrine()->getManager();
        $data = $em->getRepository("App:OcupacionMapa")->getLastOccupatedSeatsFromUserID($sesionid, $userid);
        foreach($data as $clave=>$valor){
            foreach($valor as $cl=>$vl){
                $dataReturn.= $data[$clave][$cl]."|";
            }    
            $dataReturn.= ";";
        }
        if(count($data)>0){
            $data = array("result"=>true, "response"=>"OK", "ocuppatedSeats"=>$dataReturn);
        }else{
            $data = array("result"=>true, "response"=>"OK", "ocuppatedSeats"=>"none");
        }
        return $http->responseAcceptHttp(array("data"=>$data));
    }

    //  Limpia los asientos ocupados en una sesión por un usuario en VISTA URI"'s
    /**
     * @Rest\Get("/api/cinexdataservice/cleanoccupatedseatsfromuserid/sesionid/{sesionid}/userid/{userid}", name="cleanoccupatedseatsfromuserid", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="sesionid",
     *     in="path",
     *     type="string",
     *     description="Identificador de la sesión en VISTA"
     * )
     * @SWG\Parameter(
     *     name="userid",
     *     in="path",
     *     type="integer",
     *     description="Cadena con el identificador del usuario en la db cinex"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Borrado de asientos ocupados por un usuario."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar CleanOccupatedSeatsFromUserID"
     * )
     *
     * @SWG\Tag(name="CinexDataServiceCleanOccupatedSeatsFromUserID")
     */
    public function cleanOccupatedSeatsFromUserIdAction($sesionid, $userid) {        
        $http = $this->get('httpcustom');
        $dataReturn="";
        $em = $this->getDoctrine()->getManager();
        $data = $em->getRepository("App:OcupacionMapa")->getOccupeSeatsBySessionAndUsuario($sesionid,$userid);
        foreach($data as $clave=>$valor){
            $em->remove($valor);
        }
        $em->flush();
        if(count($data)>0){
            $data = array("result"=>true, "response"=>"OK", "resultado"=>"Registro Eliminado");
        }else{
            $data = array("result"=>true, "response"=>"OK", "ocuppatedSeats"=>"No se encontro el registro");
        }
        return $http->responseAcceptHttp(array("data"=>$data));
    }

    //  Limpia los asientos ocupados "frios" para todas las sesiones en VISTA URI"'s
    /**
     * @Rest\Get("/api/cinexdataservice/cleancoldseats/minutes/{minutes}", name="cleanColdSeats", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="minutes",
     *     in="path",
     *     type="integer",
     *     description="Identificador de la sesión en VISTA"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Estatus de Ultimas Sillas."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar GetSeatStatus"
     * )
     *
     * @SWG\Tag(name="CinexDataServiceCleanColdSeats")
     */
    public function cleanColdSeatsIdAction($minutes) {        
        $http = $this->get('httpcustom');
        $dataReturn="";
        $em = $this->getDoctrine()->getManager();
        $data = $em->getRepository("App:OcupacionMapa")->findAll();
        foreach($data as $clave=>$valor){
            $start_date = new \DateTime($valor->getOcupFecha()->format('Y-m-d')." ".$valor->getOcupHora()->format('h:m:s'));
            $since_start = $start_date->diff(new \DateTime(date("Y-m-d")." ".date("h:m:s")));
            $minutesev = $since_start->days * 24 * 60;
            $minutesev += $since_start->h * 60;
            $minutesev += $since_start->i;
            if ($minutesev > $minutes){
                $em->remove($valor);
            }
           
        }
        $em->flush();
        if(count($data)>0){
            $data = array("result"=>true, "response"=>"OK", "resultado"=>"Registro Eliminado");
        }else{
            $data = array("result"=>true, "response"=>"OK", "ocuppatedSeats"=>"No se encontro el registro");
        }
        return $http->responseAcceptHttp(array("data"=>$data));
    }


   //  Devuelve el numero de items por tipo de boleto para los badgets URI"'s
    /**
     * @Rest\Get("/api/cinexdataservice/updateBadget/sesionid/{sesionid}/userid/{userid}", name="updateBadget", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="sesionid",
     *     in="path",
     *     type="string",
     *     description="Identificador de la sesión en VISTA"
     * )
     * @SWG\Parameter(
     *     name="userid",
     *     in="path",
     *     type="string",
     *     description="Cadena con el identificador del usuario en la db cinex"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Estatus de Ultimas Sillas."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar UpdateBadget"
     * )
     *
     * @SWG\Tag(name="CinexDataServiceUpdateBadget")
     */
    public function updateBadgetAction($sesionid, $userid) {        
        $http = $this->get('httpcustom');
        $dataReturn="";
        $cantidadBoletos = 0;
        $em = $this->getDoctrine()->getManager();
        $data = $em->getRepository("App:OcupacionMapa")->getLastOccupatedSeatsFromUserIDGroupByTicketTyopCode($sesionid, $userid);
        foreach($data as $clave=>$valor){
            $dataOcupacion = $em->getRepository("App:OcupacionMapa")->findByOcupId($valor["ocupId"]);
            foreach($dataOcupacion as $cl=>$vl){
                $cantidadBoletos++;
            }
            $dataReturn.= $valor["ocupId"].";".$cantidadBoletos."|";
        }    
        if(count($data)>0){
            $data = array("result"=>true, "response"=>"OK", "ocuppatedSeats"=>$dataReturn);
        }else{
            $data = array("result"=>true, "response"=>"OK", "ocuppatedSeats"=>"none");
        }
        return $http->responseAcceptHttp(array("data"=>$data));
    }
    
    
    // Establece un asiento "bloqueado" en una funcion por un usuario especifico, indicado fila y columna. URI"'s
    /**
     * @Rest\Post("/api/cinexdataservice/setseat", name="setseat", defaults={"_format":"json"})
     * @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          type="json",
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="userid", type="string", example="4670362"),
     *              @SWG\Property(property="sessionid", type="string", example="58240"),
     *              @SWG\Property(property="row", type="string", example="8"),
     *              @SWG\Property(property="column", type="string", example="11"),
     *              @SWG\Property(property="posicion", type="string", example="D12"),
     *              @SWG\Property(property="typecode", type="string", example="432"),
     *              @SWG\Property(property="ticketprice", type="string", example="600000"),
     *              @SWG\Property(property="tickettype", type="string", example="STANDARD"),
     *          )
     *      )
     * @SWG\Response(
     *     response=200,
     *     description="Asiento bloqueado."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al establecer un asiento bloqueado"
     * )
     *
     * @SWG\Tag(name="setseat")
     */
    
    public function setSeatAction(Request $request) {
        $http = $this->get('httpcustom');
        $data= [];
        $userID= $request->request->get('userid');
        $sesionID= $request->request->get('sessionid');
        $row= $request->request->get('row');
        $column= $request->request->get('column');
        $posicion= $request->request->get('posicion');
        $typecode= $request->request->get('typecode');
        $ticketprice= $request->request->get('ticketprice');
        $tickettype = $request->request->get('tickettype');
        
        $em = $this->getDoctrine()->getManager();
        $entityManager = $this->getDoctrine()->getManager();
        
        $data = $em->getRepository("App:OcupacionMapa")->getSetSeat($sesionID, $userID, $row, $column);

        foreach($data as $clave=>$valor){
            $em->remove($valor);
        }
        $em->flush();
        
        $OcupMapa = new OcupacionMapa();
        $OcupMapa->setUsuarioid($userID);
        $OcupMapa->setSesionid($sesionID);
        $OcupMapa->setOcupFila($row);
        $OcupMapa->setOcupColumna($column);
        $OcupMapa->setOcupPosicion($posicion);
        $OcupMapa->setOcupTicketTypeCode($typecode);
        $OcupMapa->setOcupTicketPrice( $ticketprice);
        $OcupMapa->setOcupTicketType($tickettype);
        $entityManager->persist($OcupMapa);
        $entityManager->flush();
        
        $data["result"]=true;
        $data["mensaje"]="Asiento Bloqueado";
        return $http->responseAcceptHttp(array("data"=>$data));
        
    }
    
    
    // Establece un asiento "desbloqueado" en una funcion por un usuario especifico, indicado fila y columna. URI"'s
    /**
     * @Rest\Post("/api/cinexdataservice/unsetseat", name="unsetseat", defaults={"_format":"json"})
     * @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          type="json",
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="userid", type="string", example="4670362"),
     *              @SWG\Property(property="sessionid", type="string", example="58240"),
     *              @SWG\Property(property="row", type="string", example="8"),
     *              @SWG\Property(property="column", type="string", example="11"),
     *          )
     *      )
     * @SWG\Response(
     *     response=200,
     *     description="Asiento desbloqueado."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error desbloquear asiento"
     * )
     *
     * @SWG\Tag(name="unsetseat")
     */
    
    public function unsetSeatAction(Request $request) {
        $http = $this->get('httpcustom');
        $data= [];
        $userID= $request->request->get('userid');
        $sesionID= $request->request->get('sessionid');
        $row= $request->request->get('row');
        $column= $request->request->get('column');
        
        $em = $this->getDoctrine()->getManager();
        $data = $em->getRepository("App:OcupacionMapa")->getSetSeat($sesionID, $userID, $row, $column);
        
        foreach($data as $clave=>$valor){
            $em->remove($valor);
        }
        $em->flush();
        
        $data["result"]=true;
        $data["mensaje"]="Asiento desloqueado";
        return $http->responseAcceptHttp(array("data"=>$data));
        
    }

}
