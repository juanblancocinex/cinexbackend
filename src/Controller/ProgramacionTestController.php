<?php

namespace App\Controller;
use App\CoreCinex\CinexService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use App\Lib\DaoLib;
use App\Lib\ApiUtilFunctions;
use App\Lib\Constantes;
use App\CoreCinex\ProgramacionFactory;


class ProgramacionTestController extends Controller
{
     
    // Listado de Cartelera URI"'s
    /**
     * @Rest\Get("/api/programacion/billboard/", name="getbillboard", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="mostrartodo",
     *     in="query",
     *     type="string",
     *     description="Mostrar Todo"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Listado de Cartelera"
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Listado de Carteleras"
     * )
     * @SWG\Tag(name="GetBillboard")
     */
    public function getBillboardAction(Request $request) {
        $apiFuntion = new ApiUtilFunctions();
        $mostrarTodo = $request->query->get('mostrartodo');
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $data= [];
        $data = $em->getRepository("App:ProgramacionTest")->getBillboard($mostrarTodo);
        if (is_null($data)) {
            $data = [];
        }
        $x = 0;
        $listcinemas = "";
        for ($x = 0; $x < sizeof($data); $x++){
            $data2= [];
            $data2 = $em->getRepository("App:ProgramacionTest")->getMovieCinemas($data[$x]["Film_strCode"]);
            if (is_null($data2)) {
                $data2 = [];
            }
            $data3= [];
            $data3 = $em->getRepository("App:PeliculaTest")->getVersiones($data[$x]["referencia"]);
            if (is_null($data3)) {
                $data3 = [];
            }   
            $data4 = [];
            $stringcomp = "";
            for ($y = 0; $y < sizeof($data3); $y++){
                $stringcomp.= strtoupper($data3[$y]["versiones"]);
            }
            if (strstr($stringcomp, "4DX") != ""){
                $data4[] = array("version"=>"4DX");
            }
            if (strstr($stringcomp, "3D") != ""){
                $data4[] = array("version"=>"3D");
            }
            if (strstr($stringcomp, "2D") != "" || strstr($stringcomp, "DIG") != ""){
                $data4[] = array("version"=>"2D");
            }
            $data[$x]["list_cinemas"] = $data2;            
            $data[$x]["list_versions"] = $data4;
        }        
        return $http->responseAcceptHttp(array("data"=>$data));
    }

     // Listado de Cartelera URI"'s
    /**
     * @Rest\Get("/api/programacion/cartelera/", name="getcartelera", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="mostrartodo",
     *     in="query",
     *     type="string",
     *     description="Mostrar Todo"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Listado de Cartelera"
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Listado de Carteleras"
     * )
     * @SWG\Tag(name="ProgramacionCartelera")
     */
    public function getCarteleraAction(Request $request) {
        $apiFuntion = new ApiUtilFunctions();
        $mostrarTodo = $request->query->get('mostrartodo');
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $data= [];
        $data = $em->getRepository("App:ProgramacionTest")->getCartelera($mostrarTodo);
        if (is_null($data)) {
            $data = [];
        } 
        foreach($data as $clave=>$valor){                      
            $clasificacionPelicula =$em->getRepository("App:PeliculaTest")->getClaficacionPelicula($data[$clave]["referencia"]);
            $dataVersiones = $em->getRepository("App:PeliculaTest")->getVersiones($data[$clave]["referencia"]);
            $stringVersiones = "";
            foreach($dataVersiones as $valor){
                $stringVersiones .= $valor["versiones"]."|"; 
                $data[$clave]["referenciaConEnlace"]=Constantes::PATHSITE['imagenescartelera'].rawurlencode($data[$clave]["referencia"]).".jpg";
            }
            $data[$clave]["venezolana"] = count($clasificacionPelicula)>0 && isset($clasificacionPelicula["venezolana"]) ? $clasificacionPelicula["venezolana"] : 'N';
            $data[$clave]["preventa"] = count($clasificacionPelicula)>0 && isset($clasificacionPelicula["preventa"]) ? $clasificacionPelicula["preventa"] : 'N';
            $data[$clave]["reestreno"] = count($clasificacionPelicula)>0 && isset($clasificacionPelicula["reestreno"]) ? $clasificacionPelicula["reestreno"] : 'N';
            $data[$clave]["versiones"]=$apiFuntion->getVersionsDB($stringVersiones);
        }
        
        return $http->responseAcceptHttp(array("data"=>$data));
    }
    
    // Listado de Cartelera Tiny Index (mini) URI"'s
    /**
     * @Rest\Get("/api/programacion/carteleratiny/", name="getcarteleratiny", defaults={"_format":"json"})
     * @SWG\Response(
     *     response=200,
     *     description="Listado de Cartelera Tiny Index"
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Listado de Carteleras Tiny Index"
     * )
     * @SWG\Tag(name="ProgramacionCarteleraTiny")
     */
    public function getCarteleraTinyAction(Request $request) {
        $apiFuntion = new ApiUtilFunctions();
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $data= [];
        $data = $em->getRepository("App:ProgramacionTest")->getCarteleraTiny();
        if (is_null($data)) {
            $data = [];
        }
        return $http->responseAcceptHttp(array("data"=>$data));
    }
    
     // Buscar Programacion por Pelicula,Fecha y Version URI"'s
    /**
     * @Rest\Get("/api/programacion/pelicula/{pelicula}/fecha/{fecha}/version/{version}", name="getbusquedaprogramacionbypeliculaandfechaandversion", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="pelicula",
     *     in="path",
     *     type="string",
     *     description="Nombre pelicula"
     * )
     * @SWG\Parameter(
     *     name="fecha",
     *     in="path",
     *     type="string",
     *     description="Fecha de Programacion"
     * )
     * @SWG\Parameter(
     *     name="version",
     *     in="path",
     *     type="string",
     *     description="Version o Formato"
     * )
      * @SWG\Response(
     *     response=200,
     *     description="Programacion por Peliculas,Fecha y Version"
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Programacion por Peliculas,Fecha y Version"
     * )
     * @SWG\Tag(name="ProgramacionByPeliculaAndFechaAndVersion")
     */
    public function getBusquedaProgramacionByPeliculaAndFechaAndVersionAction(Request $request,$pelicula,$fecha,$version) {
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $daoLib = new DaoLib($em);
        $programacionFactory = new ProgramacionFactory($daoLib);
        $parametros = isset($pelicula) ? " referencia= '".$pelicula."'" : "";
        $parametros .= isset($fecha) ? " and date_format(fecha_programacion,'%Y-%m-%d') = '".$fecha."'" : " and date_format(fecha_programacion,'%Y-%m-%d') = '".date("Y-m-d")."'";
        $parametros .= isset($version) ? " and versiones like '%".$version."%' " : "" ;              
        $data= [];
        $sql = $em->getRepository("App:ProgramacionTest")->getBuscarProgramacionSql($parametros);
        $data = $daoLib->prepareStament($sql);  
        $data = $programacionFactory->getProgramacion($data);
        $parametros = isset($pelicula) ? " referencia= '".$pelicula."'" : "";
        $parametros .= isset($fecha) ? " and date_format(fecha_programacion,'%Y-%m-%d') >= '".$fecha."'" : " and date_format(fecha_programacion,'%Y-%m-%d') >= '".date("Y-m-d")."'";             
       
        $sql = $em->getRepository("App:ProgramacionTest")->getFechasProgramacion($parametros);
        $data["fechas"] = $daoLib->prepareStament($sql); 
        if(isset($data["ciudades"])){
            $data["resultDataPeliculas"]= count($data["ciudades"])>0?true:false;
            $data["resultDataFecha"]= count($data["ciudades"])>0?true:false; 
        }else{
            $data["resultDataPeliculas"]=false;
            $data["resultDataFecha"]=false;

        }


        return $http->responseAcceptHttp(array("data"=>$data));
    }

    // Buscar Programacion por Pelicula y Fecha URI "'s
    /**
     * @Rest\Get("/api/programacion/pelicula/{pelicula}/fecha/{fecha}", name="getbusquedaprogramacionbypeliculaandfecha", defaults={"_format":"json"})
       * @SWG\Parameter(
     *     name="pelicula",
     *     in="path",
     *     type="string",
     *     description="Nombre pelicula"
     * )
     * @SWG\Parameter(
     *     name="fecha",
     *     in="path",
     *     type="string",
     *     description="Fecha de Programacion"
     * )
      * @SWG\Response(
     *     response=200,
     *     description="Programacion por Peliculas,Fecha"
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Programacion por Peliculas"
     * )
     * @SWG\Tag(name="ProgramacionByPeliculaAndFecha")
     */
    public function getBusquedaProgramacionByPeliculaAndFechaAction(Request $request,$pelicula,$fecha) {
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $daoLib = new DaoLib($em);
        $programacionFactory = new ProgramacionFactory($daoLib);
        $parametros = isset($pelicula) ? " referencia= '".$pelicula."'" : "";
        $parametros .= isset($fecha) ? " and date_format(fecha_programacion,'%Y-%m-%d') = '".$fecha."'" : " and date_format(fecha_programacion,'%Y-%m-%d') = '".date("Y-m-d")."'";             
       
        $data= [];
        $sql = $em->getRepository("App:ProgramacionTest")->getBuscarProgramacionSql($parametros);
        $data = $daoLib->prepareStament($sql); 
        $data = $programacionFactory->getProgramacion($data);

        $parametros = isset($pelicula) ? " referencia= '".$pelicula."'" : "";
        $parametros .= isset($fecha) ? " and date_format(fecha_programacion,'%Y-%m-%d') >= '".$fecha."'" : " and date_format(fecha_programacion,'%Y-%m-%d') >= '".date("Y-m-d")."'";             
       
        $sql = $em->getRepository("App:ProgramacionTest")->getFechasProgramacion($parametros);
        $data["fechas"] = $daoLib->prepareStament($sql); 
        if(isset($data["ciudades"])){
            $data["resultDataPeliculas"]= count($data["ciudades"])>0?true:false;
            $data["resultDataFecha"]= count($data["ciudades"])>0?true:false; 
        }else{
            $data["resultDataPeliculas"]=false;
            $data["resultDataFecha"]=false;

        }
        return $http->responseAcceptHttp(array("data"=>$data));
        
    }


  // Buscar Programacion por Pelicula y Version URI "'s
    /**
     * @Rest\Get("/api/programacion/pelicula/{pelicula}/version/{version}", name="getbusquedaprogramacionbypeliculaandversion", defaults={"_format":"json"})
       * @SWG\Parameter(
     *     name="pelicula",
     *     in="path",
     *     type="string",
     *     description="Nombre pelicula"
     * )
     * @SWG\Parameter(
     *     name="version",
     *     in="path",
     *     type="string",
     *     description="Version"
     * )
      * @SWG\Response(
     *     response=200,
     *     description="Programacion por Peliculas,Version"
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Programacion por Peliculas y Version"
     * )
     * @SWG\Tag(name="ProgramacionByPeliculaAndVersion")
     */
    public function getBusquedaProgramacionByPeliculaAndVersionAction(Request $request,$pelicula,$version) {
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $daoLib = new DaoLib($em);
        $programacionFactory = new ProgramacionFactory($daoLib);
        $parametros = isset($pelicula) ? " referencia= '".$pelicula."'" : "";
        $parametros .= isset($version) ? " and versiones like '%".$version."%' " : "" ;             
   
        $data= [];
        $sql = $em->getRepository("App:ProgramacionTest")->getBuscarProgramacionSql($parametros);
        $data = $daoLib->prepareStament($sql); 
        $data = $programacionFactory->getProgramacion($data);

        $parametros = isset($pelicula) ? " referencia= '".$pelicula."'" : "";
        $parametros .= isset($fecha) ? " and date_format(fecha_programacion,'%Y-%m-%d') >= '".$fecha."'" : " and date_format(fecha_programacion,'%Y-%m-%d') >= '".date("Y-m-d")."'";             
       
        $sql = $em->getRepository("App:ProgramacionTest")->getFechasProgramacion($parametros);
        $data["fechas"] = $daoLib->prepareStament($sql); 
        if(isset($data["ciudades"])){
            $data["resultDataPeliculas"]= count($data["ciudades"])>0?true:false;
            $data["resultDataFecha"]= count($data["ciudades"])>0?true:false; 
        }else{
            $data["resultDataPeliculas"]=false;
            $data["resultDataFecha"]=false;

        }
        return $http->responseAcceptHttp(array("data"=>$data));
        
    }



    // Buscar Programacion por Pelicula URI "'s
    /**
     * @Rest\Get("/api/programacion/pelicula/{pelicula}", name="getbusquedaprogramacionbypelicula", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="pelicula",
     *     in="path",
     *     type="string",
     *     description="Nombre pelicula"
     * )
      * @SWG\Response(
     *     response=200,
     *     description="Programacion por Peliculas"
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Programacion por Peliculas"
     * )
     * @SWG\Tag(name="ProgramacionByPelicula")
     */
    public function getBusquedaProgramacionByPeliculaAction(Request $request,$pelicula) {
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $daoLib = new DaoLib($em);
        $programacionFactory = new ProgramacionFactory($daoLib);
        $parametros = isset($pelicula) ? " referencia= '".$pelicula."'" : "";
        $parametros .= isset($fecha) ? " and date_format(fecha_programacion,'%Y-%m-%d') = '".$fecha."'" : " and date_format(fecha_programacion,'%Y-%m-%d') >= '".date("Y-m-d")."'";             
      
        $data= [];
        
        $sql = $em->getRepository("App:ProgramacionTest")->getBuscarProgramacionSql($parametros);
        $data = $daoLib->prepareStament($sql);  
        $data = $programacionFactory->getProgramacion($data);

        $parametros = isset($pelicula) ? " referencia= '".$pelicula."'" : "";
        $parametros .= isset($fecha) ? " and date_format(fecha_programacion,'%Y-%m-%d') >= '".$fecha."'" : " and date_format(fecha_programacion,'%Y-%m-%d') >= '".date("Y-m-d")."'";             
       
        $sql = $em->getRepository("App:ProgramacionTest")->getFechasProgramacion($parametros);
        $data["fechas"] = $daoLib->prepareStament($sql); 
        if(isset($data["ciudades"])){
            $data["resultDataPeliculas"]= count($data["ciudades"])>0?true:false;
            $data["resultDataFecha"]= count($data["ciudades"])>0?true:false; 
        }else{
            $data["resultDataPeliculas"]=false;
            $data["resultDataFecha"]=false;

        }
        
        return $http->responseAcceptHttp(array("data"=>$data));
    }


    // Buscar Programacion por Complejo URI "'s
    /**
     * @Rest\Get("/api/programacion/complejo/{complejo}/fecha/{fecha}", name="getbusquedaprogramacioncomplejo", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="complejo",
     *     in="path",
     *     type="string",
     *     description="Codigo Complejo"
     * )
     * @SWG\Parameter(
     *     name="fecha",
     *     in="path",
     *     type="string",
     *     description="Fecha Programacion"
     * )
      * @SWG\Response(
     *     response=200,
     *     description="Programacion por Complejo"
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Programacion por Complejo"
     * )
     * @SWG\Tag(name="ProgramacionComplejo")
     */
    public function getBusquedaProgramacionComplejoAction(Request $request,$complejo,$fecha) {
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $daoLib = new DaoLib($em);
        $programacionFactory = new ProgramacionFactory($daoLib);
        $parametros = isset($complejo) ? " complejo.codigoComplejo= '".$complejo."'" : "";
        $parametros .= isset($fecha) ? " and date_format(fecha_programacion,'%Y-%m-%d') = '".$fecha."'" : " and date_format(fecha_programacion,'%Y-%m-%d') = '".date("Y-m-d")."'";
        $sql = $em->getRepository("App:ProgramacionTest")->getBuscarProgramacionComplejoSql($parametros);
        $data = $daoLib->prepareStament($sql);
        $data = $programacionFactory->getProgramacionComplejo($data);
        /*if(isset($data["complejos"])){
         $data["resultDataPeliculas"]= count($data["complejos"])>0?true:false;
         }else{
         $data["resultDataPeliculas"]=false;
         $data["resultDataFecha"]=false;
         
         }*/
        return $http->responseAcceptHttp(array("data"=>$data));
    }
    
    
    // Fechas de Programacion URI "'s
    /**
     * @Rest\Get("/api/programacion/cinemafechas/listcinemas/{listcinemas}", name="getmoviedates", defaults={"_format":"json","complejo":null})
     * @SWG\Parameter(
     *     name="fecha",
     *     in="path",
     *     type="string",
     *     description="Fechas de Programacion"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Fechas de Programacion"
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Fechas de Programacion"
     * )
     * @SWG\Tag(name="GetMovieDates")
     */
    public function GetMovieDatesAction(Request $request, $listcinemas) {
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $daoLib = new DaoLib($em);
        $list = array();
        $list = explode(";", $listcinemas);
        $x = 0;
        $parametros = "";
        if (sizeof($list) > 0){
            $parametros = " AND (";
        }
        for ($x = 0; $x < sizeof($list); $x++){
            $parametros.= " programacion_test.codigoPelicula = pelicula_test.codVista AND programacion_test.codigoComplejo = '".$list[$x]."' OR ";
        }
        $parametros = substr($parametros, 0, strlen($parametros)-4).")";        
        $sql = $em->getRepository("App:ProgramacionTest")->getDatesForCinemas($parametros);
        $dataFecha = $daoLib->prepareStament($sql);
        $data=[];
        foreach($dataFecha as $clave=>$valor){
            $data[]= array("cinemaid"=>$valor["cinemaid"], "fecha"=>$valor["fecha"]);
        }
        
        return $http->responseAcceptHttp(array("data"=>array_values($data),));
    }
    


    // Fechas de Programacion URI "'s
    /**
     * @Rest\Get("/api/programacion/fechas/complejo/{complejo}", name="getprogramacionfechas", defaults={"_format":"json","complejo":null})
     * @SWG\Parameter(
     *     name="fecha",
     *     in="path",
     *     type="string",
     *     description="Fechas de Programacion"
     * )
      * @SWG\Response(
     *     response=200,
     *     description="Fechas de Programacion"
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Fechas de Programacion"
     * )
     * @SWG\Tag(name="ProgramacionFechas")
     */
    public function getProgramacionFechasAction(Request $request,$complejo) {
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $daoLib = new DaoLib($em);
        $parametros = isset($complejo) ? " and complejo.codigoComplejo= '".$complejo."'" : "";
        $sql = $em->getRepository("App:ProgramacionTest")->getFechas($parametros);
        $dataFecha = $daoLib->prepareStament($sql); 
        $data=[];
        foreach($dataFecha as $clave=>$valor){
            $data[]=array("fecha"=>$valor["fecha"]);
        }
        
        return $http->responseAcceptHttp(array("data"=>array_values($data),));
    }


     // Fechas de Programacion URI "'s
    /**
     * @Rest\Get("/api/programacion/funciones", name="getprogramacionfunciones", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="complejo",
     *     in="query",
     *     type="string",
     *     description="Id complejo"
     * )
     * @SWG\Parameter(
     *     name="idsession",
     *     in="query",
     *     type="string",
     *     description="Id session"
     * )
    * @SWG\Parameter(
     *     name="fecha",
     *     in="query",
     *     type="string",
     *     description="Fechas de Programacion"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Fechas de Programacion"
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Fechas de Programacion"
     * )
     * @SWG\Tag(name="ProgramacionFunciones")
     */
    public function getProgramacionFuncionesAction(Request $request) {
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $complejo= $request->query->get('complejo');
        $idsession= $request->query->get('idsession');
        $fecha= $request->query->get('fecha');
        $parametros = isset($complejo) ? " and  p.codigoComplejo = '".$complejo."'" : "";
        $parametros .= isset($fecha) ? " and date_format(p.fecha_programacion,'%Y-%m-%d') >= '".$fecha."'" : "";
        $parametros .= isset($idsession) ? " and p.sesionID=".$idsession : "";
        $dateSession= $request->query->get('idsession');
        $data = $em->getRepository("App:ProgramacionTest")->getFuncionesProgramacion($parametros);
        return $http->responseAcceptHttp(array("data"=>$data));
    }
    
    // Buscar Pelicula (Combos-> obtenerBuscadores) URI"'s
    /**
     * @Rest\Get("/api/programacion/peliculas/", name="getpelicula", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="complejo",
     *     in="query",
     *     type="string",
     *     description="Complejo"
     * )
     * @SWG\Parameter(
     *     name="pelicula",
     *     in="query",
     *     type="string",
     *     description="Pelicula"
     * )
     * @SWG\Parameter(
     *     name="version",
     *     in="query",
     *     type="string",
     *     description="Version"
     * )
     * @SWG\Parameter(
     *     name="ciudad",
     *     in="query",
     *     type="string",
     *     description="ciudad"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Listado de Pelicula"
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Buscar Pelicula"
     * )
     * @SWG\Tag(name="ProgramacionBuscarPeliculas")
     */
    public function getPeliculaAction(Request $request) {
        $apiFuntion = new ApiUtilFunctions();
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $parametros= "";
        
        if ($request->query->get('complejo')!=""){
            $parametros .= " and c.codigoComplejo= '".$request->query->get('complejo')."'";
        }
        
        if ($request->query->get('pelicula')!=""){
            $parametros .= " and p1.referencia= '".$request->query->get('pelicula')."'";
        }
        
        if ($request->query->get('version')!=""){
            $parametros .= " and p1.versiones like '%".$request->query->get('version')."%'";
        }
        
        if ($request->query->get('ciudad')!=""){
            $parametros .= " and c.ciudadID= '".$request->query->get('ciudad')."'";
        }
        
        //var_dump($parametros); die();
 
        $data= [];
        $data = $em->getRepository("App:ProgramacionTest")->getPelicula($parametros);
        if (is_null($data)) {
            $data = [];
        }
        foreach($data as $clave=>$valor){
            $clasificacionPelicula =$em->getRepository("App:PeliculaTest")->getClaficacionPelicula($data[$clave]["referencia"]);
            $dataVersiones = $em->getRepository("App:PeliculaTest")->getVersiones($data[$clave]["referencia"]);
            $stringVersiones = "";
            foreach($dataVersiones as $valor){
                $stringVersiones .= $valor["versiones"]."|";
                $data[$clave]["referenciaConEnlace"]=Constantes::PATHSITE['imagenescartelera'].rawurlencode($data[$clave]["referencia"]).".jpg";
            }
            $data[$clave]["venezolana"] = count($clasificacionPelicula)>0 && isset($clasificacionPelicula["venezolana"]) ? $clasificacionPelicula["venezolana"] : 'N';
            $data[$clave]["preventa"] = count($clasificacionPelicula)>0 && isset($clasificacionPelicula["preventa"]) ? $clasificacionPelicula["preventa"] : 'N';
            $data[$clave]["reestreno"] = count($clasificacionPelicula)>0 && isset($clasificacionPelicula["reestreno"]) ? $clasificacionPelicula["reestreno"] : 'N';
            $data[$clave]["versiones"]=$apiFuntion->getVersionsDB($stringVersiones);
        }
        
        return $http->responseAcceptHttp(array("data"=>$data));
    }
    
    // Obtener Buscadores URI "'s
    /**
     * @Rest\Get("/api/programacion/obtenerbuscadores", name="getobtenerbuscadores", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="complejo",
     *     in="query",
     *     type="string",
     *     description="Complejo"
     * )
     * @SWG\Parameter(
     *     name="pelicula",
     *     in="query",
     *     type="string",
     *     description="Pelicula"
     * )
     * @SWG\Parameter(
     *     name="version",
     *     in="query",
     *     type="string",
     *     description="Version"
     * )
     * @SWG\Parameter(
     *     name="ciudad",
     *     in="query",
     *     type="string",
     *     description="ciudad"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Fechas de Programacion"
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Fechas de Programacion"
     * )
     * @SWG\Tag(name="ObtenerBuscadores")
     */
    public function getObtenerBuscadores(Request $request){
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $parametro= "";
        $parametroComplejo = "";
        $parametroPelicula = "";
        $parametroCiudad = "";
        $sql = "";
        $sqlPelicula = "";
        $sqlVersiones = "";
        $sqlCiudad = "";
        
        $peliculas = null;
        $fechas = date("d/m/Y");
        $versiones = null;
        $ciudades =  null;
        $complejos = null;
        
        if ($request->query->get('complejo')!=""){
            $parametroComplejo .= " and complejo.codigoComplejo= '".$request->query->get('complejo')."'";
            $complejos = $request->query->get('complejo');
        }
        
        if ($request->query->get('pelicula')!=""){
            $parametro .= " and pelicula_test.referencia= '".$request->query->get('pelicula')."'";
            $peliculas = $request->query->get('pelicula');
        }
        
        if ($request->query->get('version')!=""){
            $parametro .= " and pelicula_test.versiones like '%".$request->query->get('version')."%'";
            $versiones = $request->query->get('pelicula');
        }
        
        if ($request->query->get('ciudad')!=""){
            $parametroCiudad= " and complejo.ciudadID= '".$request->query->get('ciudad')."'";
            $ciudades = $_GET["ciudad"];
        }
        
        $dataIndices =array("peliculas"=>$peliculas, "fechas"=>$fechas, "versiones"=>$versiones, "ciudades"=>$ciudades, "complejos"=>$complejos);
        
        $data= [];
        $data = $em->getRepository("App:ProgramacionTest")->getFechaObtenerBuscadores($parametro);
        
        $dataPeliculas= [];
        $dataPeliculas = $em->getRepository("App:ProgramacionTest")->getPeliculaObtenerBuscadores($parametroComplejo);
        
        $dataVersion= [];
        $dataVersion = $em->getRepository("App:ProgramacionTest")->getVersionesObtenerBuscadores($parametro);
        
        $version = "";
        $acuversion = "";
        $dataVersiones= [];
        
        foreach($dataVersion as $versiones){
            $version = str_replace("SUB","",$versiones['versiones']);
            if($version != $acuversion){
                if(strpos($versiones['versiones'], "ESP")){
                    $dataVersiones[] =array("itemValue"=>trim(str_replace("ESP","",$versiones['versiones'])), "itemText"=>trim(str_replace("ESP","",$versiones['versiones'])));
                }
                if(strpos($versiones['versiones'], "SUB")){
                    $dataVersiones[] =array("itemValue"=>trim(str_replace("SUB","",$versiones['versiones'])), "itemText"=>trim(str_replace("SUB","",$versiones['versiones'])));
                }
            }
            
            $acuversion = str_replace("ESP","",$versiones['versiones']);
        }
        
        $dataCiudades= [];
        $dataCiudades = $em->getRepository("App:ProgramacionTest")->getCiudadesObtenerBuscadores();
        
        $dataComplejos= [];
        $dataComplejos = $em->getRepository("App:ProgramacionTest")->getComplejosObtenerBuscadores($parametroCiudad);
        
        $datos = [];
        $content=array("codRespuesta"=>"0", "desRespuesta"=>"OK", "indices" => $dataIndices,"fechas"=>$data, "peliculas"=>$dataPeliculas, "versiones"=>$dataVersiones, "ciudades"=>$dataCiudades, "complejos"=>$dataComplejos);

        $response = new Response();
        $response->setContent(json_encode($content));
        $response->headers->set('Content-Type', 'text/plain');
        $response->setStatusCode(200);
        return $response;
        //return $http->responseAcceptHttp(array("data"=>array("codRespuesta"=>"0", "desRespuesta"=>"OK", "indices" => $dataIndices,"fechas"=>$data, "peliculas"=>$dataPeliculas, "versiones"=>$dataVersiones, "ciudades"=>$dataCiudades, "complejos"=>$dataComplejos)));
    }
}
