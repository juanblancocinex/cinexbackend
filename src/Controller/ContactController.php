<?php
namespace App\Controller;
use App\CoreCinex\CinexService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use App\Lib\DaoLib;
use App\Lib\ApiUtilFunctions;
use App\Lib\Constantes;

class ContactController extends Controller
{     
     
    /**
     * @Rest\Get("/api/contacto/agregarContactoMail", name="AgregarContactoMail", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="ciudad",
     *     in="query",
     *     type="string",
     *     description="ciudad indicada por el usuario."
     * )
     * @SWG\Parameter(
     *     name="topico",
     *     in="query",
     *     type="string",
     *     description="topico del contacto."
     * )
     * @SWG\Parameter(
     *     name="nombre",
     *     in="query",
     *     type="string",
     *     description="nombre del usuario."
     * )
     * @SWG\Parameter(
     *     name="apellido",
     *     in="query",
     *     type="string",
     *     description="apellido del usuario."
     * )
     * @SWG\Parameter(
     *     name="email",
     *     in="query",
     *     type="string",
     *     description="email del usuario."
     * )
     * @SWG\Parameter(
     *     name="telefono",
     *     in="query",
     *     type="string",
     *     description="telefono del usuario."
     * )
     * @SWG\Parameter(
     *     name="cine",
     *     in="query",
     *     type="string",
     *     description="cine seleccionado por el usuario."
     * )
     * @SWG\Parameter(
     *     name="comentario",
     *     in="query",
     *     type="string",
     *     description="comentarios del usuario."
     * )
     * 
     * @SWG\Response(
     *     response=200,
     *     description="Ciudades y Complejos."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar agregar contactos"
     * )
     *
     * @SWG\Tag(name="AgregarContactoMail")
     */    
    public function agregarContactoMail(Request $request, \Swift_Mailer $mailer) {
        $http = $this->get('httpcustom');
        $data = null;
        $ciudad= $request->query->get('ciudad');
        $topico= $request->query->get('topico');
        $nombre= $request->query->get('nombre');
        $apellido= $request->query->get('apellido');
        $email= $request->query->get('email');
        $telefono= $request->query->get('telefono');
        $cine= $request->query->get('cine');
        $comentario= $request->query->get('comentario');

        //var_dump($comentario . $cine); die();

        $json = array();
        $json2 = array();
        $json3 = array();
        $plantilla_usuario = "";
        $plantilla_admin = "";
        $list_email_admin = "";
        $result1 = null;
        $result2 = null;
        
        $em = $this->getDoctrine()->getManager();        
        // buscar la plantilla de contacto a usuario.
        $json = $em->getRepository("App:Plantilla")->getPlantillasByType("12");        
        $plantilla_usuario = $json[0]["contenido"];        
        // buscar la plantilla de contacto a admin.
        $json2 = $em->getRepository("App:Plantilla")->getPlantillasByType("13");
        $plantilla_admin = $json2[0]["contenido"];
        // buscar listado de emails de admin.
        $json3 = $em->getRepository("App:EmailAdmin")->getEmailAdminByType("contact");        
               
        $plantilla_usuario = str_replace("#NOMBRE", $nombre." ".$apellido, $plantilla_usuario);
        
        $plantilla_admin = str_replace("#CIUDAD", $ciudad, $plantilla_admin);
        $plantilla_admin = str_replace("#TOPICO", $topico, $plantilla_admin);
        $plantilla_admin = str_replace("#NOMBRESAPELLIDOS", $nombre." ".$apellido, $plantilla_admin);
        $plantilla_admin = str_replace("#EMAIL", $email, $plantilla_admin);
        $plantilla_admin = str_replace("#TELEFONO", $telefono, $plantilla_admin);
        $plantilla_admin = str_replace("#CINE", $cine, $plantilla_admin);
        $plantilla_admin = str_replace("#COMENTARIO", str_replace("+", " ", $comentario), $plantilla_admin);
        $plantilla_admin = str_replace("#NOMBRECIUDAD", $ciudad, $plantilla_admin);
        $plantilla_admin = str_replace("#NOMBRECINE", str_replace("+", " ",$cine), $plantilla_admin);
                        
                
        $message = (new \Swift_Message('CINEX CONTACTO'))
        ->setFrom('cinex@cinex.com.ve')
        ->setTo($email)        
        ->setBody($plantilla_usuario, 'text/html')
        ;
        
        $result1 = $mailer->send($message);
        
        for ($x = 0; $x < sizeof($json3); $x++){            
            $message = (new \Swift_Message('CINEX CONTACTO'))
            ->setFrom('cinex@cinex.com.ve')
            ->setTo($json3[$x]["email"])
            ->setBody($plantilla_admin, 'text/html')
            ;
            $result2 = $mailer->send($message);
        }
        
        
        if ($result1 != null && $result2 != null){
            $data = "Correo enviado";
        }else{
            $data = "Error enviando correos";
        }

        return $http->responseAcceptHttp(array("data"=>$data));
    }
    
    /**
     * @Rest\Get("/api/contacto/agregarContacto/ciudad/{ciudad}/topico/{topico}/nombre/{nombre}/apellido/{apellido}/email/{email}/telefono/{telefono}/cine/{cine}/comentario/{comentario}/nombre_ciudad/{nombre_ciudad}/nombre_cine/{nombre_cine}", name="AgregarContacto", defaults={"_format":"json"})
     *
     * @SWG\Parameter(
     *     name="ciudad",
     *     in="path",
     *     type="string",
     *     description="ciudad indicada por el usuario."
     * )
     * @SWG\Parameter(
     *     name="topico",
     *     in="path",
     *     type="string",
     *     description="topico del contacto."
     * )
     * @SWG\Parameter(
     *     name="nombre",
     *     in="path",
     *     type="string",
     *     description="nombre del usuario."
     * )
     * @SWG\Parameter(
     *     name="apellido",
     *     in="path",
     *     type="string",
     *     description="apellido del usuario."
     * )
     * @SWG\Parameter(
     *     name="email",
     *     in="path",
     *     type="string",
     *     description="email del usuario."
     * )
     * @SWG\Parameter(
     *     name="telefono",
     *     in="path",
     *     type="string",
     *     description="telefono del usuario."
     * )
     * @SWG\Parameter(
     *     name="cine",
     *     in="path",
     *     type="string",
     *     description="cine seleccionado por el usuario."
     * )
     * @SWG\Parameter(
     *     name="comentario",
     *     in="path",
     *     type="string",
     *     description="comentarios del usuario."
     * )
     * @SWG\Parameter(
     *     name="nombre_ciudad",
     *     in="path",
     *     type="string",
     *     description="nombre de la ciudad seleccionada por el usuario."
     * )
     * @SWG\Parameter(
     *     name="nombre_cine",
     *     in="path",
     *     type="string",
     *     description="nombre del cine."
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Ciudades y Complejos."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de agregar contacto usando 214"
     * )
     *
     * @SWG\Tag(name="AgregarContacto")
     */
    public function agregarContacto($ciudad, $topico, $nombre, $apellido, $email, $telefono, $cine, $comentario, $nombre_ciudad, $nombre_cine) {
        $http = $this->get('httpcustom');
        $ch = curl_init("http://smtp.cinex.com.ve/clases/cinexDataServiceMethods.php?method=dosendcontactemail&ciudad=".$ciudad."&topico=".$topico."&nombre=".urlencode($nombre)."&apellido=".urlencode($apellido)."&email=".$email."&telefono=".$telefono."&cine=".$cine."&comentario=".urlencode($comentario)."&nombre_ciudad=".urlencode($nombre_ciudad)."&nombre_cine=".urlencode($nombre_cine));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
        return $http->responseAcceptHttp(array("data"=>$output));
    }
}
