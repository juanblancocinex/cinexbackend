<?php


namespace App\Controller;
use App\CoreCinex\CinexService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use App\Lib\DaoLib;
use App\Lib\ApiUtilFunctions;
use App\Lib\Constantes;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Translation\TranslatorInterface;
use App\Lib\XmlLib;
use App\Entity\TokenTrans;
use App\Entity\Usuario;
use App\Entity\TransactionNumber;

class CinexDataServiceController extends Controller
{
        protected $clientBookinFeed;
        protected $clientSinec;
        protected $client;
        protected $params;
        protected $arrayConnect;
        protected $xmlLib;
        /**
         * Constructor.
        **/
        public function __construct(ParameterBagInterface $params)
        {
            $this->xmlLib= new XmlLib();
            $this->params=$params;
            $this->arrayConnect=array("OptionalClientClass" =>$this->params->get('soap.client_class'),
                                      "OptionalClientId" =>$this->params->get('soap.client_id'),
                                      "OptionalClientName" =>$this->params->get("soap.client_name"));
            //$this->client = new \SoapClient($this->params->get('soap.cinexdataservice.wsdl'), array("trace" => 1, "exception" => 1,'encoding'=>'UTF8'));
            $this->client =  new \nusoap_client($this->params->get('soap.cinexdataservice.wsdl'),'wsdl');
            $this->client->soap_encoding = 'UTF-8';
            $this->client->decode_utf8 = TRUE;
            $this->clientSinec =  new \nusoap_client($this->params->get('soap.sinec.wsdl'),'wsdl');
            $this->clientSinec->soap_encoding = 'UTF-8';
            $this->clientSinec->decode_utf8 = TRUE;
            $this->clientBookinFeed =  new \nusoap_client($this->params->get('soap.booking.wsdl'),'wsdl');
            $this->clientBookinFeed->soap_encoding = 'UTF-8';
            $this->clientBookinFeed->decode_utf8 = TRUE;
            
            $this->sms =  new \nusoap_client($this->params->get('soap.booking.wsdl'),'wsdl');
            $this->sms->soap_encoding = 'UTF-8';
            $this->sms->decode_utf8 = TRUE;
            
        }
 
    
    
     // GetSessionList CinexDataService URI"'s
    /**
     * @Rest\Get("/api/cinexdataservice/getsessionlist/{cinemaid}/{movieid}", name="cinexdataservicegetsessionlist", defaults={"_format":"json"})
     *
     * @SWG\Parameter(
     *     name="cinemaid",
     *     in="path",
     *     type="string",
     *     description="cinemaid codigo del complejo"
     * )
     * @SWG\Parameter(
     *     name="movieid",
     *     in="path",
     *     type="string",
     *     description="movieid codigo de la pelicula"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="informacion del complejo."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de GetSessionList"
     * )
     *
     * @SWG\Tag(name="CinexDataServiceGetSessionList")
     */
    
    public function getSessionListAction($cinemaid,$movieid) {
        $http = $this->get('httpcustom');
        $data= [];
        $params = array(
            "CinemaId"=>utf8_decode($cinemaid),
            "MovieId"=>utf8_decode($movieid),
        );
        $dataResponse = $this->client->call('GetSessionList',array($params), '', '', false, true);
        $data =$this->prepareHttpResponse($dataResponse);
        return $http->responseAcceptHttpVista($data);
    }
    
    
    // GetMovieInfoList CinexDataService URI"'s
    /**
     * @Rest\Get("/api/cinexdataservice/getmovieinfolist/{moviename}", name="cinexdataservicegetmovieinfolist", defaults={"_format":"json"})
     *
     * @SWG\Parameter(
     *     name="moviename",
     *     in="path",
     *     type="string",
     *     description="nombre de la película"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="informacion del complejo."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de GetMovieInfoList"
     * )
     *
     * @SWG\Tag(name="CinexDataServiceGetMovieinfoList")
     */
    public function getMovieinfoListAction($moviename) {
        $http = $this->get('httpcustom');
        $data= [];
        $params = array(
			"OptionalMovieName"=>utf8_decode($moviename), 
        );           
        $dataResponse = $this->client->call('GetMovieInfoList',array($params), '', '', false, true);
        $data =$this->prepareHttpResponse($dataResponse);
        return $http->responseAcceptHttpVista($data);
    }
    

    // GetCinemaList CinexDataService URI"'s
    /**
     * @Rest\Get("/api/cinexdataservice/getcinemalist/moviename/{moviename}/cinemaid/{cinemaid}", name="cinexdataservicegetcinemalist", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="moviename",
     *     in="path",
     *     type="string",
     *     description="nombre de la película"
     * )
     * @SWG\Parameter(
     *     name="cinemaid",
     *     in="path",
     *     type="string",
     *     description="codigo complejo"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Listado de todos los complejos en VISTA."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de GetCinemaList"
     * )
     *
     * @SWG\Tag(name="CinexDataServiceGetCinemaList")
     */
    public function getCinemaListAction(TranslatorInterface $translator,$moviename,$cinemaid) {
        $http = $this->get('httpcustom');
        $data= [];        
        $params = array(
            "OptionalMovieName"=>$moviename,
            "OptionalCinemaId"=>$cinemaid
        );
        $dataResponse = $this->client->call('GetCinemaList',array($params), '', '', false, true);
        $data =$this->prepareHttpResponse($dataResponse);
        return $http->responseAcceptHttpVista($data);
    }
    

     // GetCinemaListByMovie CinexDataService URI"'s
    /**
     * @Rest\Get("/api/cinexdataservice/getcinemalistbymovie/moviename/{moviename}", name="cinexdataservicegetcinemalistbymovie", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="moviename",
     *     in="path",
     *     type="string",
     *     description="nombre de la película"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Listado de todos los complejos por Pelicula en VISTA."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de GetCinemaList"
     * )
     *
     * @SWG\Tag(name="CinexDataServiceGetCinemaListByMovie")
     */
    public function getCinemaListByMovieAction(TranslatorInterface $translator,$moviename) {
        $http = $this->get('httpcustom');
        $data= [];        
        $params = array(
            "OptionalMovieName"=>utf8_decode($moviename),
        );

        $dataResponse = $this->client->call('GetCinemaList',array($params), '', '', false, true);
        $data =$this->prepareHttpResponse($dataResponse);
        return $http->responseAcceptHttpVista($data);

    }
    
    
 // GetCinemaListAll CinexDataService URI"'s
    /**
     * @Rest\Get("/api/cinexdataservice/getcinemalistall", name="cinexdataservicegetcinemalistall", defaults={"_format":"json"})
     * @SWG\Response(
     *     response=200,
     *     description="Listado de todos los complejos en VISTA."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de GetCinemaListAll"
     * )
     *
     * @SWG\Tag(name="CinexDataServiceGetCinemaListAll")
     */
    public function getCinemaListAllAction(TranslatorInterface $translator) {
        $http = $this->get('httpcustom');
        $data= [];        
        $dataResponse = $this->client->call('GetCinemaListAll');
        $data =$this->prepareHttpResponse($dataResponse);
        return $http->responseAcceptHttpVista($data);

    }

    // GetMoviePeople CinexDataService URI"'s
    /**
     * @Rest\Get("/api/cinexdataservice/getmoviepeople/{movieid}", name="cinexdataservicegetmoviepeople", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="movieid",
     *     in="path",
     *     type="string",
     *     required= false,
     *     description="id de pelicula"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Listado de Elenco."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de GetMoviePeople"
     * )
     *
     * @SWG\Tag(name="CinexDataServiceGetMoviePeople")
     */
    public function getMoviePeopleAction($movieid) {
        $http = $this->get('httpcustom');
        $data= [];
        $params = array(
            "MovieId"=>utf8_decode($movieid),
        );        
        $dataResponse = $this->client->call('GetMoviePeople',array($params), '', '', false, true);
        $data =$this->prepareHttpResponse($dataResponse);
        return $http->responseAcceptHttpVista($data);

    }


    // GetMovieList CinexDataService URI"'s
    /**
     * @Rest\Get("/api/cinexdataservice/getmovielist/cinemaid/{cinemaid}", name="cinexdataservicegetmovielist", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="cinemaid",
     *     in="path",
     *     required=false,
     *     type="string",
     *     description="id de complejo"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Listado Peliculas."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de GetMovieList"
     * )
     *
     * @SWG\Tag(name="CinexDataServiceGetMovieList")
     */
    public function getMovieListAction($cinemaid) {
        $http = $this->get('httpcustom');
        $data= [];
        $params = array(
            "OptionalCinemaId"=> isset($cinemaid) ? utf8_decode($cinemaid) : null
        );       
        $dataResponse = $this->client->call('GetMovieList',array($params), '', '', false, true);
        $data =$this->prepareHttpResponse($dataResponse);
        return $http->responseAcceptHttpVista($data);

    }


    // GetTicketTypeList CinexDataService URI"'s
    /**
     * @Rest\Get("/api/cinexdataservice/gettickettypelist", name="cinexdataservicegettickettypelist", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="cinemaid",
     *     in="query",
     *     required=true,
     *     type="string",
     *     description="id de complejo"
     * )
     * @SWG\Parameter(
     *     name="sessionid",
     *     in="query",
     *     required=false,
     *     type="string",
     *     description="sessionid registrado en vista"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Tipo de Boletos."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de getTicketTypeList"
     * )
     *
     * @SWG\Tag(name="CinexDataServiceGetTicketTypeList")
     */
    public function getTicketTypeListAction(TranslatorInterface $translator,Request $request) {
        $http = $this->get('httpcustom');
        $data= [];
        $cinemaId= $request->query->get('cinemaid');
        $sessionId= $request->query->get('sessionid');
        $params = array(
            "CinemaId"=> isset($cinemaId) ? utf8_decode($cinemaId) : null,
            "SessionId"=> isset($sessionId) ? utf8_decode($sessionId) : null
        );       
        $dataResponse = $this->client->call('GetTicketTypeList',array($params), '', '', false, true);
        $data =$this->prepareHttpResponse($dataResponse);
        return $http->responseAcceptHttpVista($data);

    }


    // GetSessionInfo CinexDataService URI"'s
    /**
     * @Rest\Get("/api/cinexdataservice/getsessioninfo", name="cinexdataservicegetsessioninfo", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="cinemaid",
     *     in="query",
     *     required=true,
     *     type="string",
     *     description="id de complejo"
     * )
     * @SWG\Parameter(
     *     name="sessionid",
     *     in="query",
     *     required=false,
     *     type="string",
     *     description="sessionid registrado en vista"
     * )

     * @SWG\Response(
     *     response=200,
     *     description="Información de Sessión de Usuario."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de GetSessionInfo"
     * )
     *
     * @SWG\Tag(name="CinexDataServiceGetSessionInfo")
     */
    public function getSessionInfoAction(TranslatorInterface $translator,Request $request) {
        $http = $this->get('httpcustom');
        $data= [];
        $cinemaId= $request->query->get('cinemaid');
        $sessionId= $request->query->get('sessionid');
        $params = array(
            "CinemaId"=> isset($cinemaId) ? utf8_decode($cinemaId) : null,
            "SessionId"=> isset($sessionId) ? utf8_decode($sessionId) : null
        );       
        $dataResponse = $this->client->call('GetSessionInfo',array($params), '', '', false, true);
        $data =$this->prepareHttpResponse($dataResponse);
        return $http->responseAcceptHttpVista($data);

    }


   // GetSeatsAvailable CinexDataService URI"'s
    /**
     * @Rest\Get("/api/cinexdataservice/getseatsavailable", name="cinexdataservicegetseatsavailable", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="cinemaid",
     *     in="query",
     *     required=true,
     *     type="string",
     *     description="id de complejo"
     * )
     * @SWG\Parameter(
     *     name="sessionid",
     *     in="query",
     *     required=false,
     *     type="string",
     *     description="sessionid registrado en vista"
     * )

     * @SWG\Response(
     *     response=200,
     *     description="Información de Sessión de Usuario."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de GetSeatsAvailable"
     * )
     *
     * @SWG\Tag(name="CinexDataServiceGetSeatsAvailable")
     */
    public function getSeatsAvailableAction(TranslatorInterface $translator,Request $request) {
        $http = $this->get('httpcustom');
        $data= [];
        $x = 0;
        $cinemaId= $request->query->get('cinemaid');
        $sessionId= $request->query->get('sessionid');
        $params = array(
            "CinemaId"=> isset($cinemaId) ? utf8_decode($cinemaId) : null,
            "SessionId"=> isset($sessionId) ? utf8_decode($sessionId) : null
        );       
        $dataResponse = $this->client->call('GetSessionInfo',array($params), '', '', false, true);
        foreach ($dataResponse as $clave => $valor){
			if ($valor != "OK" && $x == 0){
				$dataVista= json_encode(array("result"=>false, "response"=>"Error en la llamada getCinemaList ".$valor));
			}else if ($x == 1){
				$dataVista = json_encode(array("result"=>true, "response"=>json_decode(json_encode(simplexml_load_string(trim(utf8_encode($valor)))),TRUE)));
			}
			$x++;
		}
        $dataVista = json_decode($dataVista);
        if ($dataVista->response){
			$data["result"]=$dataVista->result;
            $data["error"]=false;
            $data["response"]="OK";
            $data["data"]=array("result"=>true, "response"=>"OK", "SeatsAvailable"=>$dataVista->response->Table->Session_decSeats_Available);
        }else{
            $data["result"]=false;
            $data["error"]=true;
            $data["response"]=$dataVista;
            $data["data"]=false;
        }   
        return $http->responseAcceptHttpVista($data);

    }


 // GetPercentageAvailable CinexDataService URI"'s
    /**
     * @Rest\Get("/api/cinexdataservice/getpercentageavailable", name="cinexdataservicegetpercentageavailable", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="cinemaid",
     *     in="query",
     *     required=true,
     *     type="string",
     *     description="id de complejo"
     * )
     * @SWG\Parameter(
     *     name="sessionid",
     *     in="query",
     *     required=false,
     *     type="string",
     *     description="sessionid registrado en vista"
     * )

     * @SWG\Response(
     *     response=200,
     *     description="Información de Sessión de Usuario."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de GetSeatsAvailable"
     * )
     *
     * @SWG\Tag(name="CinexDataServiceGetPercentageAvailable")
     */
    public function GetPercentageAvailableAction(TranslatorInterface $translator,Request $request) {
        $http = $this->get('httpcustom');
        $data= [];
        $x = 0;
        $cinemaId= $request->query->get('cinemaid');
        $sessionId= $request->query->get('sessionid');
        $uservistaId= $request->query->get('uservistaid');
        $reintentos= $request->query->get('reintentos');

        $params = array(
            "CinemaId"=> isset($cinemaId) ? utf8_decode($cinemaId) : null,
            "SessionId"=> isset($sessionId) ? utf8_decode($sessionId) : null
        );       
        $response = $this->forward('App\Controller\VistaTicketingServiceController::getMap', array(
            'name'  => $name,
            'color' => 'green',
        ));
    
        $dataVista = json_decode($dataVista);
        if ($dataVista->response){
			$data["result"]=$dataVista->result;
            $data["error"]=false;
            $data["response"]="OK";
            $data["data"]=array("result"=>true, "response"=>"OK", "SeatsAvailable"=>$dataVista->response->Table->Session_decSeats_Available);
        }else{
            $data["result"]=false;
            $data["error"]=true;
            $data["response"]=$dataVista;
            $data["data"]=false;
        }   
        return $http->responseAcceptHttpVista($data);

    }

    // Obtiene la información de ocupación de sala por sesion para una pelicula en un complejo para una fecha"'s
    /**
     * @Rest\Get("/api/cinexdataservice/getMovieOccupationInfo", name="movieoccupationinfo", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="movieName",
     *     in="query",
     *     required=true,
     *     type="string",
     *     description="nombre la pelicula a usar."
     * )
     * @SWG\Parameter(
     *     name="datesession",
     *     in="query",
     *     required=true,
     *     type="string",
     *     description="fecha de programacion en la cual se buscaran las sesiones de la pelicula"
     * )
     
     * @SWG\Response(
     *     response=200,
     *     description="Información de Sessión de Usuario."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de getMovieOccupationInfo"
     * )
     *
     * @SWG\Tag(name="MovieOccupationInfo")
     */
    public function getMovieOccupationInfoAction(TranslatorInterface $translator,Request $request) {
        $movieName= $request->query->get('movieName');
        $dateSession= $request->query->get('datesession');
        $jsonOutput = [];
        $Verde = "#5cb85c";
        $Naranja = "#f0ad4e";
        $Rojo = "#d9534f";
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $data= [];
        $session = [];
        $y = 0;
       
        $data = $em->getRepository("App:ProgramacionTest")->getMovieOccupationInfo($movieName, $dateSession);
        foreach($data as $clave=>$valor){
            $session = $this->getSessionListAction($valor["codigoComplejo"],$valor["codVista"]); //validado que las variables estan llenas
            $table = json_decode($session->getContent());
            foreach($table->response as $clave=>$valor){
                foreach($valor->Table as $clave=>$valor2){
                    $color = "";
                    $asientosdisponibles = $valor2->Session_decSeats_Available;
                    $sessiondataid = $valor2->Session_strID;
                    $cinemaId = $valor2->Cinema_strID;
                    if (intval($asientosdisponibles) == 0){
                        $color = $Rojo;
                    }else if (intval($asientosdisponibles) >= 1 && intval($asientosdisponibles) <= 100){
                        $color = $Naranja;
                    }else if (intval($asientosdisponibles) >= 101 && intval($asientosdisponibles) <= 300){
                        $color = $Verde;
                    }else{
                        $color = $Verde;
                    }
                    
                    $jsonOutput[$y] = array("sessionid"=>$sessiondataid, "cinemaid"=>$cinemaId, "asientosdisponibles"=>$asientosdisponibles, "color"=>$color);
                    $y++;
                }
            }
        }
        
        if (sizeof($jsonOutput) > 0){
            $data["result"]="true";
            $data["error"]=false;
            $data["response"]="OK";
            $data["data"]=array("SeatsAvailable"=>$jsonOutput);
        }else{
            $data["result"]=false;
            $data["error"]=true;
            $data["response"]="false";
            $data["data"]=false;
        }
        return $http->responseAcceptHttpVista($data);
        
    }
    
    // Obtiene la información de ocupación de sala por sesion para una pelicula en un complejo para una fecha"'s
    /**
     * @Rest\Get("/api/cinexdataservice/getMovieOccupationInfoForCinema", name="movieoccupationinfoforcinema", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="cinemaid",
     *     in="query",
     *     required=true,
     *     type="string",
     *     description="id de complejo"
     * )
     * @SWG\Parameter(
     *     name="datesession",
     *     in="query",
     *     required=true,
     *     type="string",
     *     description="fecha de programacion en la cual se buscaran las sesiones de la pelicula"
     * )
     
     * @SWG\Response(
     *     response=200,
     *     description="Información de Sessión de Usuario."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de getMovieOccupationInfoForCinema"
     * )
     *
     * @SWG\Tag(name="MovieOccupationInfoForCinema")
     */
    public function getMovieOccupationInfoForCinemaAction(TranslatorInterface $translator,Request $request) {
        $cinemaId= $request->query->get('cinemaid');
        $dateSession= $request->query->get('datesession');
        $jsonOutput = [];
        $Verde = "#5cb85c";
        $Naranja = "#f0ad4e";
        $Rojo = "#d9534f";
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $data= [];
        $session = [];
        $y = 0;
        $data = $em->getRepository("App:ProgramacionTest")->getMovieOccupationInfoForCinema($cinemaId, $dateSession);
        
        foreach($data as $clave=>$valor){
            $session = $this->getSessionListAction($cinemaId,$valor["codVista"]);
            $table = json_decode($session->getContent());
            foreach($table->response as $clave=>$valor){
                foreach($valor->Table as $clave=>$valor2){
                    $color = "";
                    $asientosdisponibles = $valor2->Session_decSeats_Available;
                    $sessiondataid = $valor2->Session_strID;
                    if (intval($asientosdisponibles) == 0){
                        $color = $Rojo;
                    }else if (intval($asientosdisponibles) >= 1 && intval($asientosdisponibles) <= 100){
                        $color = $Naranja;
                    }else if (intval($asientosdisponibles) >= 101 && intval($asientosdisponibles) <= 300){
                        $color = $Verde;
                    }else{
                        $color = $Verde;
                    }
                    
                    $jsonOutput[$y] = array("sessionid"=>$sessiondataid, "cinemaid"=>$cinemaId, "asientosdisponibles"=>$asientosdisponibles, "color"=>$color);
                    $y++;
                }
            } 
        }
        
        if (sizeof($jsonOutput) > 0){
            $data["result"]="true";
            $data["error"]=false;
            $data["response"]="OK";
            $data["data"]=array("SeatsAvailable"=>$jsonOutput);
        }else{
            $data["result"]=false;
            $data["error"]=true;
            $data["response"]="false";
            $data["data"]=false;
        }
        return $http->responseAcceptHttpVista($data);
        
    }
    
    // Limpia los datos de ocupacion de sesiones.
    /**
     * @Rest\Delete("/api/cinexdataservice/cleanmovielistschedule", name="cleanMovieListSchedule", defaults={"_format":"json"})
     * 
     * @SWG\Response(
     *     response=200,
     *     description="Elimina programación tiny."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de cleanMovieListSchedule"
     * )
     *
     * @SWG\Tag(name="CleanMovieListSchedule")
     */
    public function cleanMovieListScheduleAction() {
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();
        $qb->delete('App:ProgramacionTiny', 'a');
       // $qb->where('a.programacionid = :programacionid');
       // $qb->setParameter('programacionid', 1);
        $query = $qb->getQuery();
        $query->execute();
        $data["result"]=true;
        $data["error"]=false;
        $data["response"]="OK";
        $data["data"]=array("respuesta"=>"Programación Tiny Eliminada");
        return $http->responseAcceptHttpVista($data);
    }


    // GetBookingFeeValue CinexDataService URI"'s
    /**
     * @Rest\Get("/api/cinexdataservice/getbookingfeevalue", name="cinexdataservicegetbookingfeevalue", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="complejoid",
     *     in="query",
     *     required=true,
     *     type="string",
     *     description="id de complejo"
     * )
     * @SWG\Parameter(
     *     name="codigoticketho",
     *     in="query",
     *     required=false,
     *     type="string",
     *     description="ticket HO code"
     * )

     * @SWG\Response(
     *     response=200,
     *     description="Información de Booking."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de GetBookingFeeValue"
     * )
     *
     * @SWG\Tag(name="CinexdataserviceGetBookingFeeValue")
     */
    public function GetBookingFeeValueAction(TranslatorInterface $translator,Request $request) {
        $http = $this->get('httpcustom');
        $data= [];
        $complejoId= $request->query->get('complejoid');
        $codigoTicketHo= $request->query->get('codigoticketho');
        $params = array(
            "Complejos"=> isset($complejoId) ? utf8_decode($complejoId) : null,
            "CodigoTicketHO"=> isset($codigoTicketHo) ? utf8_decode($codigoTicketHo) : null
        );       
        $dataResponse = $this->clientBookinFeed->call('Consultar_BookingFee_Complejo_Vista_New',array($params), '', '', false, true);
        if(isset($dataResponse["Consultar_BookingFee_Complejo_Vista_NewResult"]["diffgram"]["NewDataSet"])){
            $data= array("result"=>true,"error"=>false,"response"=>true, "data" =>array($dataResponse["Consultar_BookingFee_Complejo_Vista_NewResult"]["diffgram"]["NewDataSet"]["BookingFee"]["BFeeDetail_Fee"]));
        }else{
            $data= array("result"=>false,"error"=>false,"response"=>false, "data" =>array("Data no encontrada"));
        }
        return $http->responseAcceptHttpVista($data);

    }
    
    
    /**
     * @Rest\Get("/api/cinexdataservice/getadultticketvalue/cinemaid/{cinemaid}/sessionid/{sessionid}", name="getadultticketvalue", defaults={"_format":"json"})
     *
     * @SWG\Parameter(
     *     name="cinemaid",
     *     in="path",
     *     type="string",
     *     description="identificador del complejo."
     * )
     * @SWG\Parameter(
     *     name="sessionid",
     *     in="path",
     *     type="string",
     *     description="identificador de la sesion."
     * )
     
     * @SWG\Response(
     *     response=200,
     *     description="Array con la respuesta del valor del boleto adulto y comision"
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar getadultticketvalue"
     * )#
     *
     * @SWG\Tag(name="GetAdultTicketValue")
     */
    
    public function getAdultTicketValueAction($cinemaid, $sessionid){
        $http = $this->get('httpcustom');
        $json_content = array();
        $json_adult = array();
        $response = "";
        $endpoint = "";
        $output = "";        
        $x = 0;
        $ch = null;
        $data = "";
        $nombreboletovista = "";
        $monto_boleto_adulto = "";
        $monto_hopk_adulto = "";
        $monto_comision = "";
        $endpoint = "http://172.20.4.236/api/vistaticketingservice/getticketlistbychannel/cinemaid/{$cinemaid}/sessionid/{$sessionid}/channel/www";
        $ch = curl_init($endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json'
        ));        
        $output = curl_exec($ch);
        curl_close($ch);
        $json_content = json_decode($output);
        if ($json_content != "" && $json_content != null){            
            for ($x = 0; $x < sizeof($json_content->response); $x++){
                $nombreboletovista = trim(strtolower($json_content->response[$x]->price_strticket_type_description));
                if ( strstr(strtolower($nombreboletovista), "adulto") != "" || strstr(strtolower($nombreboletovista), "popular") != "" || strstr(strtolower($nombreboletovista), "jueves") != "" || strstr(strtolower($nombreboletovista), "prive") != "" || strstr(strtolower($nombreboletovista), "vip") != "" || strstr(strtolower($nombreboletovista), "cinexart") != ""  || strstr(strtolower($nombreboletovista), "miti") != ""){
                    // FIX para no permitir 3ra edad ni boleto niño a la hora buscar precio de boleto adulto (20/11/2018).
                    if (strstr(strtolower($nombreboletovista), "3ra") != "" || strstr(strtolower($nombreboletovista), "niño") != "") continue;
                    // FIX para evitar que pase el boleto movistar o yeipii.
                    if (strstr(strtolower($nombreboletovista), "movistar") != "" || strstr(strtolower($nombreboletovista), "yeipii") != "") continue;
                    $monto_boleto_adulto = $json_content->response[$x]->price_intticket_price;
                    $monto_hopk_adulto = $json_content->response[$x]->ttype_strhocode;
                    $codigo_boleto_adulto = $json_content->response[$x]->price_strticket_type_code;
                    $codigo_areacode_adulto = $json_content->response[$x]->areacat_strcode;
                    
                    $xml_data = '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Body><Consultar_BookingFee_Complejo_Vista_New xmlns="http://tempuri.org/"><Complejos>'.$cinemaid.'</Complejos><CodigoTicketHO>'.$monto_hopk_adulto.'</CodigoTicketHO></Consultar_BookingFee_Complejo_Vista_New></soap:Body></soap:Envelope>';
                    $ch = curl_init("http://172.20.5.242/ServicioWebCinex/Servicio.asmx?op=Consultar_BookingFee_Complejo_Vista_New");
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml;charset=UTF-8'));
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml_data");
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    $output = curl_exec($ch);
                    curl_close($ch);
                    if ($output != null && $output != ""){
                        $monto_comision = substr($output, strpos($output, "<BFeeDetail_Fee>"), strpos($output, "</BFeeDetail_Fee>") - strpos($output, "<BFeeDetail_Fee>"));
                        $monto_comision = str_replace("<BFeeDetail_Fee>", "", $monto_comision);
                        $json_adult = array("monto_boleto_adulto"=>$monto_boleto_adulto, "codigo_boleto_adulto"=>$codigo_boleto_adulto, "comision_boleto_adulto"=>$monto_comision, "areacode_boleto_adulto"=>$codigo_areacode_adulto);
                    }    
                }
            }            
        }
        $data= array("result"=>true,"error"=>false,"response"=>true, "data" =>$json_adult);
        return $http->responseAcceptHttpVista($data);
    }
    
    
    /**
     * @Rest\Get("/api/cinexdataservice/getCinexclusivoTicketList/userid/{userid}/sala/{sala}/nombredia/{nombredia}/formato/{formato}/cinemaid/{cinemaid}/sessionid/{sessionid}", name="GetCinexclusivoTicketList", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="userid",
     *     in="path",
     *     required=true,
     *     type="string",
     *     description="id del usuario"
     * )
     * @SWG\Parameter(
     *     name="sala",
     *     in="path",
     *     required=true,
     *     type="string",
     *     description="nombre de la sala"
     * )
     * @SWG\Parameter(
     *     name="nombredia",
     *     in="path",
     *     required=true,
     *     type="string",
     *     description="dia de la compra"
     * )
     * @SWG\Parameter(
     *     name="cinemaid",
     *     in="path",
     *     required=true,
     *     type="string",
     *     description="identificador del cine."
     * )
     * @SWG\Parameter(
     *     name="sessionid",
     *     in="path",
     *     required=true,
     *     type="string",
     *     description="identificador de la sesion."
     * )      
     * @SWG\Response(
     *     response=200,
     *     description="Lista de beneficios de cinexclusivo para el proceso de pago."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de getCinexclusivoTicketList"
     * )
     *
     * @SWG\Tag(name="GetCinexclusivoTicketList")
     */
    public function getCinexclusivoTicketListAction($userid, $sala, $nombredia, $formato, $cinemaid, $sessionid){
        $http = $this->get('httpcustom');
        $data = [];
        $boletoloyalty = [];
        $beneficios2 = array();
        $areacode_boleto_adulto = "";
        $apto = false;
        $cantidadboletoloyalty = 0;
        $cantidad_loyalty_used = 0;
        $discountpercent = 0;
        $strfechaExp = "";
        $codigobol = "";
        $category = "LOYALTY";
        $json_adultvalue = array();
        $em = $this->getDoctrine()->getManager();
        // obtener valor del boleto adulto y comision.
        $ch = curl_init("http://172.20.4.236/api/cinexdataservice/getadultticketvalue/cinemaid/{$cinemaid}/sessionid/{$sessionid}");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml;charset=UTF-8'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
        if ($output != "" && $output != null){
            $json_adultvalue = json_decode($output);
        }
        if ($json_adultvalue != null && isset($json_adultvalue->response)){
            $typecode_boleto_adulto = $json_adultvalue->response->codigo_boleto_adulto;
            $codigobol = $json_adultvalue->response->areacode_boleto_adulto;
            $montoadultobol = $json_adultvalue->response->monto_boleto_adulto;
            $montocomisionbol = $json_adultvalue->response->comision_boleto_adulto;
            $areacode_boleto_adulto = $json_adultvalue->response->areacode_boleto_adulto;
            $daoLib = new DaoLib($this->getDoctrine()->getManager());
            $sql= "SELECT code, name, lb.recognitionid, format, fechavencimiento, cortesia, quantity, discountpercent, status, disponibledesde, disponiblehasta, diaplica, condicionado FROM loyalty_benefits lb, loyalty_benefits_details lbd WHERE lb.recognitionid = lbd.recognitionid AND lb.userid = '".$userid."' AND status = 'S'";
            $beneficios = $daoLib->prepareStament($sql,"asociativo");
            foreach($beneficios as $clave=>$valor){
                $cadcomp = trim(str_replace( "SUB", "", str_replace("ESP", "", trim(strtoupper($valor["format"])))));
                $cadcomp2 = trim(strtoupper($valor["format"]));
                if (trim(strtoupper($sala)) == "SALA VIP" && trim(strtoupper($valor["format"])) == "VIP"){
                    $apto = true;
                }else if (trim(strtoupper($sala)) == "SALA CINEXART" && trim(strtoupper($valor["format"])) == "PRIVE"){
                    $apto = true;
                }else if ($cadcomp == $cadcomp2){
                    $apto = true;
                }
                if ($apto){
                    $cantidadboletoloyalty = $valor["quantity"];
                    // evaluar si el beneficio es de cortesia o no.
                    if ($valor["cortesia"] == "1"){
                        $fechaexp = $valor["fechavencimiento"];
                        $strfechaExp = " Este beneficio expira en la fecha [".substr($fechaexp, 8, 2)."-".substr($fechaexp, 5, 2)."-".substr($fechaexp, 0, 4)."]";
                        $first_date = new DateTime(date("d-m-Y"));
                        $second_date = new DateTime($fechaexp);
                        $difference = $first_date->diff($second_date);
                        if (format_interval($difference) >= 0){ // EN CASO DE NO ESTAR VENCIDOS BUSCAR SI FUERON CONSUMIDOS.
                            $sql= "SELECT cantidad FROM loyalty_benefits_used WHERE nombre = '".$valor["name"]."' AND usuarioid = ".$userid;
                            $beneficios2 = $daoLib->prepareStament($sql,"asociativo");
                            foreach($beneficios2 as $clave2=>$valor2){
                                $cantidad_loyalty_used = $valor2["cantidad"];
                                $cantidadboletoloyalty = $cantidadboletoloyalty - $cantidad_loyalty_used;
                            }
                        }else{
                            // libera el beneficio vencido en la tabla de beneficios usados.
                            ///	$db->query("DELETE FROM loyalty_benefits_used WHERE nombre = '".$rowx[1]."' AND usuarioid = ".$_SESSION["usuarioID"]);
                            $strfechaExp = "<b>Fecha de Vencimiento [".$fechaexp."]</b> ** BENEFICIO VENCIDO **";
                            $cantidadboletoloyalty = 0;
                        }
                    }else{
                        // porcentaje de descuento.
                        $discountpercent = $valor["discountpercent"];
                    }
                    // VERIFICAR SI TOCA EL BENEFICIO DEL MIERCOLES O NO.
                    if (strstr(strtolower($valor["name"]), "miercoles") && strtolower($nombredia) != "miercoles") continue;
                    // VERIFICAR SI EL BENEFICIO VIP ES APLICABLE A LA SALA.
                    if (strstr(strtolower($valor["name"]), "vip") && !strstr(strtolower($sala), "vip")) continue;
                    // VERIFICAR SI EL BENEFICIO PRIVE ES APLICABLE A LA SALA.
                    if (strstr(strtolower($valor["name"]), "priv") && !strstr(strtolower($sala), "cinexart")) continue;
                    if (strstr(strtolower($valor["name"]), "miercoles") && strstr(strtolower($sala), "cinexart")) continue;
                    if (strstr(strtolower($valor["name"]), "miercoles") && strstr(strtolower($sala), "vip")) continue;
                    // VERIFICAR BENEFICIO DE CUMPLEAÑOS
                    if (strstr(strtolower($valor["name"]), "cumple") && $fechanac == date("m-d")){
                        if (strstr(strtolower($valor["name"]), "3d") && !strstr(strtolower($sala), "3d")) continue;
                        if (strstr(strtolower($valor["name"]), "4dx") && !strstr(strtolower($sala), "4dx")) continue;
                    }
                    // VERIFICACION DE FORMATO
                    if (trim(strtolower($valor["format"])) != trim(strtolower($formato))){
                        continue;
                    }
                    if ($discountpercent > 0){
                        $category = "STANDARD";
                    }
                    // si el beneficio no tiene codigo de boleto, tomar el valor del boleto adulto.
                    if ($codigobol == ""){
                        $codigobol = $codigo_boleto_adulto;
                    }
                    $boletoloyalty[] = array("codigo"=>$codigobol, "descripcion"=>$valor["name"], "monto"=>$montoadultobol, "comision"=>$montocomisionbol, "category"=>$category, "recognitionID"=>$valor["recognitionid"], "qtyAvailable"=>$cantidadboletoloyalty, "fechaExp"=>$strfechaExp, "area_strcode"=>$areacode_boleto_adulto);
                }
            }            
        }
        $data = array("result"=>true,"error"=>false,"response"=>true, "data" =>$boletoloyalty);                
        return $http->responseAcceptHttpVista($data);
    }


    // DoCheckCommingSoon CinexDataService URI"'s
    /**
     * @Rest\Delete("/api/cinexdataservice/cinexdataservicedoCheckcommingsoon", name="cinexdataservicedoCheckcommingsoon", defaults={"_format":"json"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Peliculas verificadas en Sinec."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de CinexDataServiceDoCheckCommingSoon"
     * )
     *
     * @SWG\Tag(name="CinexDataServiceDoCheckCommingSoon")
     */
    public function doCheckCommingSoon(){
        $http = $this->get('httpcustom');
        $peliculasComming= $this->getCommingSoonSinecAction();
        $pelicula=json_decode($peliculasComming->getContent());
        $notFound=true;
        $numMovies=0;
        $peliculasNoencontradas="";
        $em = $this->getDoctrine()->getManager();
        if(count($pelicula->response)>0){
            $daoLib = new DaoLib($this->getDoctrine()->getManager());
            $sql= " SELECT distinct 
                            codSinec, nombreSinFormato 
                            FROM cinex.pelicula_test where 
                            pelicula_test.fechaEstreno > curdate() and 
                            nombreOriginal is not null and pelicula_test.nombreSinFormato 
                            not in(select nombreSinFormato from pelicula_blockeada) 
                            group by nombreSinFormato ";
            $peliculasSinec = $daoLib->prepareStament($sql,"asociativo");
            foreach($peliculasSinec as $clave=>$valor){
                $notFound=true;
                foreach($pelicula->response as $claveSinec=>$valorSinec){
                    if($valor["codSinec"]==$valorSinec->co_pelicula){
                        $notFound=false;
                    }
                }
                $numMovies++;
                if ($notFound){
                    $qb = $em->createQueryBuilder();
                    $qb->delete('App:PeliculaTest', 'a');
                    $qb->where('a.codsinec = :codsinec');
                    $qb->setParameter('codsinec', $valor["codSinec"]);
                    $query = $qb->getQuery();
                    $query->execute();
                    $peliculasNoencontradas= strlen($peliculasNoencontradas) > 0 ? "|". $valor["codSinec"] :$valor["codSinec"];
                }
            }
            $data= array("result"=>true,"error"=>false,"response"=>"OK", "data" =>$peliculasNoencontradas, "peliculas_procesadas"=>$numMovies);
             return $http->responseAcceptHttpVista($data);
        }
        
    }

    // GetCommingSoonSinecCinexDataService URI"'s
    /**
     * @Rest\Get("/api/cinexdataservice/getcommingsoonsinec", name="cinexdataservicegetcommingsoonsinec", defaults={"_format":"json"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Proximamente de Sinec."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de GetCommingSoonSinec"
     * )
     *
     * @SWG\Tag(name="CinexDataServiceGetCommingSoonSinec")
     */
    
    public function getCommingSoonSinecAction() {
        $http = $this->get('httpcustom');
        $data= [];
        $params = array();
        $dataResponse = $this->clientSinec->call('GetProximosEstrenos',array($params), '', '', false, true);
        if(isset($dataResponse["GetProximosEstrenosResult"])){
            $peliculas=$dataResponse["GetProximosEstrenosResult"]["respDataSet"]["diffgram"]["NewDataSet"]["Peliculas"];
            foreach($peliculas as $clave=>$valor){
                foreach($valor as $clavePelicula=>$valorPelicula){
                    $peliculas[$clave][$clavePelicula]=utf8_encode($valorPelicula); 
                    if ($clavePelicula == "nb_pelicula_largo"){ 
                        $peliculas[$clave]["referencia"] = trim(str_replace("(SUB)", "", str_replace("(ESP)", "", str_replace("(3D)", "", str_replace("(DIG)", "", trim( $this->sanear_string_fix( $peliculas[$clave]["nb_pelicula_largo"] ) ) )))));
                        $peliculas[$clave]["fecha_estreno"] = substr($peliculas[$clave]["fe_estreno"], 0, strpos($peliculas[$clave]["fe_estreno"], "T") );
                        $peliculas[$clave]["genero"] = $this->getGenero($peliculas[$clave]["co_genero_pelicula"]);
                    }
                }
            }                
            
            $data["result"]=true;
            $data["error"]=false;
            $data["response"]="OK";
            $data["data"]=(array)json_decode(json_encode($peliculas), TRUE); 
        }else{
            $data["result"]=false;
            $data["error"]=false;
            $data["response"]="OK";
            $data["data"]=array();
        }
       
        return $http->responseAcceptHttpVista($data);
    }
    
    private function getGenero($generoID){
        if ($generoID == "4"){
            return "DRAMA";
        }else if ($generoID == "5"){
            return "COMEDIA";
        }else if ($generoID == "6"){
            return "ACCIÓN";
        }else if ($generoID == "7"){
            return "SUSPENSO";
        }else if ($generoID == "8"){
            return "TERROR";
        }else if ($generoID == "9"){
            return "MUSICAL";
        }else if ($generoID == "10"){
            return "EPICA";
        }else if ($generoID == "11"){
            return "ANIMACIÓN";
        }else if ($generoID == "12"){
            return "AVENTURAS";
        }else if ($generoID == "13"){
            return "FICCIÓN";
        }else if ($generoID == "14"){
            return "ADULTOS";
        }else if ($generoID == "15"){
            return "OTRAS";
        }else if ($generoID == "16"){
            return "FANTASÍA";
        }else if ($generoID == "17"){
            return "INFANTIL";
        }else if ($generoID == "18"){
            return "ROMÁNTICA";
        }else if ($generoID == "19"){
            return "DOCUMENTAL";
        }else if ($generoID == "24"){
            return "DEPORTE";
        }
    }
    
    private function sanear_string_fix($string){
        $string = str_replace("Á","A", $string);
        $string = str_replace("É","E", $string);
        $string = str_replace("Í","I", $string);
        $string = str_replace("Ó","O", $string);
        $string = str_replace("Ú","U", $string);
        $string = str_replace("Ñ","N", $string);
        $string = str_replace(":","", $string);
        $string = str_replace(".","", $string);
        $string = str_replace("?","", $string);
        $string = str_replace("¿","", $string);
        $string = str_replace("¡","", $string);
        $string = str_replace("!","", $string);
        $string = str_replace("*","", $string);
        $string = str_replace("&","Y", $string);
        $string = str_replace("@","A", $string);
        $string = str_replace("","%20", $string);
        return trim($string);
    }
    
    /**
     * @Rest\Get("/api/cinexdataservice/getcommingsoondatasinec", name="getcommingsoondatasinec", defaults={"_format":"json"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Proximamente de Sinec."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de GetCommingSoonDataSinec"
     * )
     *
     * @SWG\Tag(name="GetCommingSoonDataSinec")
     */
    
    public function getCommingSoonDataSinecAction() {
        $http = $this->get('httpcustom');
        $data= [];
        $params = array();
        $urltrailer = array();
        $peliculasdep = array();
        $movieused = "";
        $x = 0;
        $y = 0;
        $tipoGenero = array("4"=>"DRAMA","5"=>"COMEDIA","6"=>"ACCIÓN","7"=>"SUSPENSO","8"=>"TERROR","9"=>"MUSICAL","10"=>"EPICA","11"=>"ANIMACIÓN","12"=>"AVENTURAS","13"=>"FICCIÓN","14"=>"ADULTOS","15"=>"OTRAS","16"=>"FANTASÍA","17"=>"INFANTIL","18"=>"ROMÁNTICA","19"=>"DOCUMENTAL","24"=>"DEPORTE");       
        $dataResponse = $this->clientSinec->call('GetProximosEstrenos',array($params), '', '', false, true);
        if(isset($dataResponse["GetProximosEstrenosResult"])){
            $peliculas=$dataResponse["GetProximosEstrenosResult"]["respDataSet"]["diffgram"]["NewDataSet"]["Peliculas"];
            foreach($peliculas as $clave=>$valor){
                foreach($valor as $clavePelicula=>$valorPelicula){
                    $peliculas[$clave][$clavePelicula]=utf8_encode($valorPelicula);
                }                                                
            }
            $daoLib = new DaoLib($this->getDoctrine()->getManager());            
            for ($x = 0; $x < sizeof($peliculas); $x++){
                $fixnombre = "";
                $peliculas[$x]["co_genero_pelicula"] = $tipoGenero[$peliculas[$x]["co_genero_pelicula"]];
                $fixnombre = $this->sanear_string_fix(substr($peliculas[$x]["nb_pelicula_largo"], 0, strpos($peliculas[$x]["nb_pelicula_largo"], "(")-1));
                $peliculas[$x]["nb_pelicula_corto"] = $fixnombre;                
                $sql= "SELECT urlTrailer FROM pelicula_test WHERE codSinec = ".$peliculas[$x]["co_pelicula"];
                $urltrailer = $daoLib->prepareStament($sql,"asociativo");
                if (isset($urltrailer[0]["urlTrailer"])){
                    $peliculas[$x]["urlTrailer"] = $urltrailer[0]["urlTrailer"];
                }else{
                    $peliculas[$x]["urlTrailer"] = "";
                }
                if (isset($peliculas[$x])){
                    if (strstr($movieused, $peliculas[$x]["nb_pelicula_original"])){
                        //unset($peliculas[$x]);
                    }else{
                        $peliculasdep[] = $peliculas[$x];
                        $movieused.= $peliculas[$x]["nb_pelicula_original"].";";
                    }
                }                
            }
            $data["result"]=true;
            $data["error"]=false;
            $data["response"]="OK";
            $data["data"]=(array)json_decode(json_encode($peliculasdep), TRUE);
        }else{
            $data["result"]=false;
            $data["error"]=false;
            $data["response"]="OK";
            $data["data"]=array();
        }
        return $http->responseAcceptHttpVista($data);
    }


    // DoCheckSinec URI"'s
    /**
     * @Rest\Get("/api/cinexdataservice/dochecksinec", name="cinexdataservicedochecksinec", defaults={"_format":"json"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Programación Sinec Eliminada."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de doCheckSinec"
     * )
     *
     * @SWG\Tag(name="CinexDataServiceDoCheckSinec")
     */
    
    public function doCheckSinecAction() {
        $http = $this->get('httpcustom');
        $data= [];
        $nummovies=0;
        $params = array();
        $em = $this->getDoctrine()->getManager();
        $sesionNoEncontradas="";
        for ($y = 0; $y < count( Constantes::COMPLEJOPROPACINE ); $y++){
            
            $programacion= $this->getSinecMovieListAction(Constantes::COMPLEJOPROPACINE[$y]);
            $table =json_decode($programacion->getContent());
            $programacionTest=  $sql = $em->getRepository("App:ProgramacionTest")->getSesionID(Constantes::COMPLEJOPROPACINE[$y]);
            $table=$table->response;
            
            foreach($programacionTest as $claveProgramacion=>$valorProgramacion){ 
                $notfound = true;
                foreach($table as $clave=>$valor){
                    $nummovies++;
                    if( $valor->id_programacion==$valorProgramacion["sesionid"]) {
                        $notfound =false;
                        break;
                    }
                }
                if ($notfound){
                    if (strlen($sesionNoEncontradas) > 0){
                        $sesionNoEncontradas.= "|".$valor->id_programacion;
                    }else{
                        $sesionNoEncontradas = $valor->id_programacion;
                    }
                    $qb = $em->createQueryBuilder();
                    $qb->delete('App:ProgramacionTest', 'a');
                    $qb->where('a.sesionid = :sesionid');
                    $qb->andWhere('a.codigocomplejo = :codigocomplejo');                    
                    $qb->setParameter('sesionid', $valor->id_programacion);
                    $qb->setParameter('codigocomplejo', Constantes::COMPLEJOPROPACINE[$y]);
                    $query = $qb->getQuery();
                    $query->execute();
               
                }
            }
               
        }
        $data["result"]=true;
        $data["error"]=false;
        $data["response"]="OK";
        $data["data"]=$sesionNoEncontradas;
        $data["sesiones_procesadas"]=$nummovies;
        return $http->responseAcceptHttpVista($data);
    }

    /**
     * @Rest\Get("/api/cinexdataservice/getnextbillreference/usuarioid/{usuarioid}", name="GetNextBillReference", defaults={"_format":"json"})
     *
     * @SWG\Parameter(
     *     name="usuarioid",
     *     in="path",
     *     type="string",
     *     description="Identificador del usuario."
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Array con el nuevo numero de referencia."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar GetNextBillReference"
     * )#
     *
     * @SWG\Tag(name="GetNextBillReference")
     */
    public function GetNextBillReference($usuarioid){
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $json_content2 = array();
        $reference = "";
        $response = "";
        $transnumber = 0;
        $reference = $this->forward('App\Controller\TransactionNumberController::CreateTransactionNumberAction', array("usuarioid" => $usuarioid));
        $transnumber = $this->forward('App\Controller\TransactionNumberController::GetransactionNumberAction', array("usuarioid" => $usuarioid));
        if ($transnumber != ""){
            $dd=$transnumber->getContent();
            $json_content2 = json_decode($dd);
        }
        if (isset($json_content2->response) && $json_content2->response){
            $data["result"]=true;
            $data["error"]=false;
            $data["response"]="OK";
            $data["data"]=array("billNumber"=>$json_content2->response);
        }else{
            $data["result"]=false;
            $data["error"]=true;
            $data["response"]="ERROR";
            $data["data"]=array("billNumber"=>"0");
        }
        return $http->responseAcceptHttpVista($data);
    }


// DoCheckSinec URI"'s
    /**
     * @Rest\Get("/api/cinexdataservice/getsinecmovielist/{cinemaid}", name="cinexdataGetSinecMovieList", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="cinemaid",
     *     in="path",
     *     required=true,
     *     type="integer",
     *     description="id de complejo"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Peliculas Programadas en Sinec."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de GetSinecMovieList"
     * )
     *
     * @SWG\Tag(name="CinexDataGetSinecMovieList")foreach($peliculasSinec as $clave=>$valor){
     */
    public function getSinecMovieListAction($cinemaid){
        $http = $this->get('httpcustom');
        $params = array(
            "co_complejo"=>$cinemaid,
            "co_sala"=>""
         );
        $dataResponse = $this->clientSinec->call('GetProgramacionesComplejoSala',array($params), '', '', false, true);
        if(isset($dataResponse["GetProgramacionesComplejoSalaResult"]["respDataSet"])){
            $programacion=$dataResponse["GetProgramacionesComplejoSalaResult"]["respDataSet"]["diffgram"]["NewDataSet"]["Programacionesporsala"];
            foreach($programacion as $clave=>$valor){
                foreach($valor as $claveDetalle=>$valorDetalle){
                    $programacion[$clave][$claveDetalle]=utf8_encode($valorDetalle);
                }
            }    
            $data["result"]=true;
            $data["error"]=false;
            $data["response"]="OK";
            $data["data"]=(array)json_decode(json_encode($programacion), TRUE);
        }else{ 
            $data["result"]=true;
            $data["error"]=false;
            $data["response"]="OK";
            $data["data"]=array();
        }    
        return $http->responseAcceptHttpVista($data);
    }

    /**
     * @Rest\Get("/api/cinexdataservice/enviarsms/{mensaje}/mensaje/celular/{celular}", name="enviarsms", defaults={"_format":"json"})
     * @SWG\Parameter(
     *     name="mensaje",
     *     in="path",
     *     required=true,
     *     type="string",
     *     description="mensaje a enviar al numero de celular."
     * )
     * @SWG\Parameter(
     *     name="celular",
     *     in="path",
     *     required=true,
     *     type="integer",
     *     description="numero de celular."
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Resultado de la operacion de envio de SMS."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar EnviarSMS"
     * )
     *
     * @SWG\Tag(name="EnviarSMS")
     */
    function enviarSMSAction ($mensaje, $celular){
        $http = $this->get('httpcustom');        
        $xml = '';
        $sms_service = "";
        $ch = null;
        $output = null;
        $response = "";        
        $sms_service = "http://provider.wtfe.com:8080/SMSSender/SMSSenderWSService";
        $xml = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://serviciosterceros.sms/"><soapenv:Header><ser:user>cinex</ser:user><ser:password>c1n3x</ser:password></soapenv:Header><soapenv:Body><ser:sendSMS><payload>{MENSAJE}</payload><ani>{CELULAR}</ani></ser:sendSMS></soapenv:Body></soapenv:Envelope>';
        $xml = str_replace("{MENSAJE}", $mensaje, $xml);
        $xml = str_replace("{CELULAR}", $celular, $xml);
        $ch = curl_init($sms_service);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, "$xml" );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml;charset=UTF-8'));
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        $output = curl_exec($ch);
        curl_close($ch);        
        if (strstr($output, "<code>0</code>") != ""){
            $data["result"]=true;
            $data["error"]=false;
            $data["response"]="OK";
            $data["data"]=$output;            
        }else{
            $data["result"]=false;
            $data["error"]=true;
            $data["response"]="ERROR";
            $data["data"]=$output;            
        }
        return $http->responseAcceptHttpVista($data);
    }   
    
    
    /**
     * @Rest\Get("/api/cinexdataservice/movistartokensms/", name="movistartokensms", defaults={"_format":"json"})
     * @SWG\Response(
     *     response=200,
     *     description="Token para la transaccion de canje de puntos movistar por entradas cinex."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al generar el token."
     * )
     *
     * @SWG\Tag(name="MovistarTokenSMS")
     */
    function movistarTokenSMSAction(){
        $http = $this->get('httpcustom');
        $token = "";
        $x = 0;
        for ($x = 0; $x < 8; $x++){
            $token.= rand(0, 9);
        }
        if ($token != ""){
            $data["result"]=true;
            $data["error"]=false;
            $data["response"]="OK";
            $data["data"]=$token;
        }else{
            $data["result"]=false;
            $data["error"]=true;
            $data["response"]="ERROR";
            $data["data"]=$token;            
        }
        return $http->responseAcceptHttpVista($data);
    }

    /**
         * Parsea el XML devueldo del objeto NUSOAP
         * @param array $dataResponse XMl de Xista .
         * @return response.
    */
    public function prepareHttpResponse($dataResponse){
        $data=[];
        $dataVista=[];
        $x = 0;
        foreach ($dataResponse as $clave => $valor){
			if ($valor != "OK" && $x == 0){
				$dataVista= json_encode(array("result"=>false,"error"=>true, "response"=>"Error en la llamada getCinemaList ".$valor));
			}else if ($x == 1){
				$dataVista = json_encode(array("result"=>true,"error"=>false, "response"=>json_decode(json_encode(simplexml_load_string(trim(utf8_encode($valor)))),TRUE)));
			}
			$x++;
        }
        $dataVista = json_decode($dataVista);
        if ($dataVista->response){
			$data["result"]=$dataVista->result;
            $data["error"]=false;
            $data["response"]="OK";
            $data["data"]=array(json_decode(json_encode((array)$dataVista->response), TRUE));
        }else{
            $data["result"]=false;
            $data["error"]=true;
            $data["response"]=$dataVista;
            $data["data"]=false;
        }   
        return $data;
    }    


}
