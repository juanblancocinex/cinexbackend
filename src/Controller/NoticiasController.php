<?php

namespace App\Controller;
use App\CoreCinex\CinexService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use App\Lib\DaoLib;
use App\Lib\ApiUtilFunctions;
use App\Lib\Constantes;


class NoticiasController extends Controller
{
     
  

    // Noticias Limit URI"'s
    /**
     * @Rest\Get("/api/noticias/limit/{cantidad}", name="getnoticiaslimit", defaults={"_format":"json"})
     
    * @SWG\Response(
     *     response=200,
     *     description="Noticias."
     * )
     * @SWG\Parameter(
     *     name="cantidad",
     *     in="path",
     *     type="string",
     *     description="Cantidad"
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Noticias con limites."
     * )
     * @SWG\Tag(name="NoticiasLimit")
     */
    public function getNoticiasLimitAction(Request $request,$cantidad) {
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $ciudad=[];
        $data= [];
        $data = $em->getRepository("App:Noticias")->getNoticiasLimit($cantidad);
        if (is_null($data)) {
            $data = [];
        } 
        return $http->responseAcceptHttp(array("data"=>$data));
    }

     // Noticias  URI"'s
    /**
     * @Rest\Get("/api/noticias/", name="getnoticias", defaults={"_format":"json"})
     
    * @SWG\Response(
     *     response=200,
     *     description="Noticias."
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Noticias."
     * )
     * @SWG\Tag(name="Noticias")
     */
    public function getNoticiasAction(Request $request) {
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $ciudad=[];
        $data= [];
        $data = $em->getRepository("App:Noticias")->getNoticias();
        if (is_null($data)) {
            $data = [];
        } 
        return $http->responseAcceptHttp(array("data"=>$data));
    }

    // Noticias by Id URI"'s
    /**
     * @Rest\Get("/api/noticias/id/{id}", name="getnoticiasbyid", defaults={"_format":"json"})
     
    * @SWG\Response(
     *     response=200,
     *     description="Noticias por id"
     * )
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="string",
     *     description="Id noticias"
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Ocurrio un error al ejecutar la consulta de Noticias por Id."
     * )
     * @SWG\Tag(name="NoticiasByID")
     */
    public function getNoticiasByIdAction(Request $request,$id) {
        $http = $this->get('httpcustom');
        $em = $this->getDoctrine()->getManager();
        $ciudad=[];
        $data= [];
        $data = $em->getRepository("App:Noticias")->getNoticiasById($id);
        if (is_null($data)) {
            $data = [];
        } 
        return $http->responseAcceptHttp(array("data"=>$data));
    }


   

}
