<?php
namespace App\Lib;

/**
 * Summary.
 * Description: Libreria XML
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 */
/**
  * @method array getXmlObject() Construye XML Object
  */ 
class XmlLib
{
        /**
         * Construye Xml Object.
         * @param string $xmlString string xml.
         * @return array[].
         **/
        public function getXmlObject($xmlString){
                libxml_use_internal_errors(true);
                $xml = preg_replace('/(<\?xml[^?]+?)utf-16/i', '$1utf-8', $xmlString); 
                $xml = simplexml_load_string($xml);
                if (false === $xml) {
                    $errors = libxml_get_errors();
                    echo 'Errors: '.var_export($errors, true);
                    throw new \Exception('invalid XML');
                }
                return  $xml;
        }

        /**
         * Ejecuta servicios SOAP mendiante el uso de CURL
         * @param string $xmlString string xml.
         * @return xml de respuesta del servicio.
         */
        public function execSoapService($xmlString,$wsdlUrl,$SoapAction){
                $ch = curl_init($wsdlUrl);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml;charset=UTF-8', $SoapAction));
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlString);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $output = curl_exec($ch);
                curl_close($ch);
                return $output;
        }

        /**
         * Construye XML
         * @param array $array XMLElement.
         * @param string $nodeName nodo hijo principal.
         * @return xml de respuesta del servicio.
         */	
        function generateXmlFromArray($array, $nodeName) {
            $xml = '';
            if (is_array($array) || is_object($array)) {
                foreach ($array as $key=>$value) {
                    if (is_numeric($key)) {
                        $key = $nodeName;
                    }
                    $xml .= '<' . $key . '>' . $this->generateXmlFromArray($value, $nodeName) . '</' . $key . '>';
                }
            } else {
                $xml = htmlspecialchars($array, ENT_QUOTES);
            }
            return $xml;
        }
        
            
        /**
         * Construye Formato XML
         * @param array $array XMLElement.
         * @param string $odeBlock nodo padre del xml.
         * @param string $nodeName nodo hijo principal.
         * @return xml de respuesta del servicio.
         */
        function generateValidXmlFromArray($array, $nodeBlock='nodes', $nodeName='node') {
            $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
            $xml .= '<' . $nodeBlock. '>';
            $xml .= $this->generateXmlFromArray($array, $nodeName);
            $xml .= '</' . $nodeBlock . '>';
            return $xml;
        }
        
        /**
         * Construye Formato XML
         * @param array $obj objeto a deserializar.
         * @return xml de respuesta del servicio.
         */        
        function xmlObjToArray($obj) {
            $namespace = $obj->getDocNamespaces(true);
            $namespace[NULL] = NULL;
            $children = array();
            $attributes = array();
            $name = strtolower((string)$obj->getName());
            $text = trim((string)$obj);
            if( strlen($text) <= 0 ) {
                $text = NULL;
            }
            if(is_object($obj)) {
                foreach( $namespace as $ns=>$nsUrl ) {
                    // atributes
                    $objAttributes = $obj->attributes($ns, true);
                    foreach( $objAttributes as $attributeName => $attributeValue ) {
                        $attribName = strtolower(trim((string)$attributeName));
                        $attribVal = trim((string)$attributeValue);
                        if (!empty($ns)) {
                            $attribName = $ns . ':' . $attribName;
                        }
                        $attributes[$attribName] = $attribVal;
                    }
                    $objChildren = $obj->children($ns, true);
                    foreach( $objChildren as $childName=>$child ) {
                        $childName = strtolower((string)$childName);
                        if( !empty($ns) ) {
                            $childName = $ns.':'.$childName;
                        }
                        $children[$childName][] = $this->xmlObjToArray($child);
                    }
                }
            }
            return array(
                'name'=>$name,
                'text'=>$text,
                'attributes'=>$attributes,
                'children'=>$children
            );
        } 


        


}






