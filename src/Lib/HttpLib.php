<?php
namespace App\Lib;
use Symfony\Component\HttpFoundation\Response;

/**
 * Summary.
 *
 * Description: Libreria de Comunicaciones Htttp
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 */
/**
  * @method void __construct($serializer) Constructor
  * @method response responseAcceptHttp($parameters) Metodo de Respuesta afirmativa del objeto HTTP
  * @method response responseErrorHttp($parameters) Metodo de Respuesta negativa del objeto HTTP
  */ 
class HttpLib
{       
        protected $serializer;
        
        /**
         * Constructor.
         * @param servicio $serializer Objeto de la clase Serializer.
        **/
        public function __construct($serializer)
        {
          $this->serialize = $serializer;
        }

        /**
         * Metodo de Respuesta afirmativa del objeto HTTP
         * @param array $parameters Parametros del objeto HTTP.
         * @return response.
         **/
        public function responseAcceptHttp($parameters){

                    $content = [
                        'code' =>200,
                        'error' =>false,
                        'result' => true,
                        'response' => $parameters['data'],
                    ];
                    /*return new Response($this->serialize->serialize($response, "json"));*/
                    $response = new Response();
                    $response->setContent($this->serialize->serialize($content, "json"));
                    $response->headers->set('Content-Type', 'text/plain');
                    $response->setStatusCode(200);
                    return $response;
    
         }

         /**
         * Metodo de Respuesta negativa del objeto HTTP
         * @param array $parameters Parametros del objeto HTTP.
         * @return response.
         **/
        public function response400Http($parameters){

            $content = [
                'code' =>400,
                'error' =>true,
                'result' => true,
                'response' => $parameters['data'],
            ];
            /*return new Response($this->serialize->serialize($response, "json"));*/
            $response = new Response();
            $response->setContent($this->serialize->serialize($content, "json"));
            $response->headers->set('Content-Type', 'text/plain');
            $response->setStatusCode(200);
            return $response;

 }
         
                 /**
         * Metodo de Respuesta afirmativa del objeto HTTP
         * @param array $parameters Parametros del objeto HTTP.
         * @return response.
         **/
        public function responseAcceptHttpWithoutFormat($data){

            /*return new Response($this->serialize->serialize($response, "json"));*/
            $response = new Response();
            $response->setContent($this->serialize->serialize($data, "json"));
            $response->headers->set('Content-Type', 'application/json');
            $response->setStatusCode(200);
            return $response;
        }


         /**
         * Metodo de Respuesta afirmativa del objeto HTTP para PaymentProcess
         * @param array $parameters Parametros del objeto HTTP.
         * @return response.
         **/
         public function responsePaymentAcceptHttp($parameters){
             if ($parameters['method'] == "iniciarSesion"){
                 $content = [
                     'codRespuesta' => $parameters['codRespuesta'],
                     'desRespuesta' => $parameters['desRespuesta'],                  
                     'usuario' => $parameters['usuario'],
                 ];
             }
             if ($parameters['method'] == "cambiarClave"){
                 $content = [
                     'codRespuesta' => $parameters['codRespuesta'],
                     'desRespuesta' => $parameters['desRespuesta'],                     
                 ];                 
             }
             if ($parameters['method'] == "logPago"){
                 $content = [
                     'codRespuesta' => $parameters['codRespuesta'],
                     'desRespuesta' => $parameters['desRespuesta'],
                 ];
             }
             if ($parameters['method'] == "getsinglebooking"){
                 $content = [
                     'codRespuesta' => $parameters['codRespuesta'],
                     'desRespuesta' => $parameters['desRespuesta'],
                     'data' => $parameters['data'],
                 ];
             }
             if ($parameters['method'] == "getTransactionNumber"){
                 $content = [
                     'codRespuesta' => $parameters['codRespuesta'],
                     'desRespuesta' => $parameters['desRespuesta'],
                     'data' => $parameters['data'],
                 ];
             }
             if ($parameters['method'] == "pagarTDC"){
                 $content = [
                     'codRespuesta' => $parameters['codRespuesta'],
                     'desRespuesta' => $parameters['desRespuesta'],
                     'localizador' => $parameters['localizador'],
                     'voucher' => $parameters['voucher'],
                     'megasoftCode' => $parameters['megasoftCode'],
                 ];
             }
             if ($parameters['method'] == "getadultticketvalue"){
                 $content = [
                     'codRespuesta' => $parameters['codRespuesta'],
                     'desRespuesta' => $parameters['desRespuesta'],
                     'montoboleto' => $parameters['montoboleto'],
                     'montocomision' => $parameters['montocomision']
                 ];
             }
             if ($parameters['method'] == "compĺetarordenmovistar"){
                 $content = [
                     'codRespuesta' => $parameters['codRespuesta'],
                     'desRespuesta' => $parameters['desRespuesta'],
                     'localizador' => $parameters['localizador']
                 ];
             }             
             if ($parameters['method'] == "compĺetarordenloyalty"){
                 $content = [
                     'codRespuesta' => $parameters['codRespuesta'],
                     'desRespuesta' => $parameters['desRespuesta'],
                     'localizador' => $parameters['localizador']
                 ];
             }      
             if ($parameters['method'] == "obtenerTicket"){
                 $content = [
                     'codRespuesta' => $parameters['codRespuesta'],
                     'desRespuesta' => $parameters['desRespuesta'],
                     'disponibles' => $parameters['disponibles'],
                     'codig' => $parameters['codig'],
                     'descrip' => $parameters['descrip'],
                     'mont' => $parameters['mont'],
                     'boletos' => $parameters['boletos'],
                 ];
             }         
             if ($parameters['method'] == "bloquearAsientos"){
                 $content = [
                     'codRespuesta' => $parameters['codRespuesta'],
                     'desRespuesta' => $parameters['desRespuesta'],
                 ];
             }
             if ($parameters['method'] == "obtenerListadoComplejos"){
                 $content = [
                     'codRespuesta' => $parameters['codRespuesta'],
                     'desRespuesta' => $parameters['desRespuesta'],
                     'complejos' => $parameters['complejos'],                     
                 ];
             }
             if ($parameters['method'] == "obtenerProximosEstrenos"){
                 $content = [
                     'codRespuesta' => $parameters['codRespuesta'],
                     'desRespuesta' => $parameters['desRespuesta'],
                     'meses' => $parameters['meses'],
                 ];
             }
             if ($parameters['method'] == "enviarCorreo"){
                 $content = [
                     'codRespuesta' => $parameters['codRespuesta'],
                     'desRespuesta' => $parameters['desRespuesta'],
                 ];
             }        
             if ($parameters['method'] == "Loyalty_usuarios_membresia"){
                 $content = [
                     'codRespuesta' => $parameters['codRespuesta'],
                     'desRespuesta' => $parameters['desRespuesta'],
                     'items' => $parameters['items'],
                 ];
             }        
             if ($parameters['method'] == "Loyalty_registros_usuarios"){
                 $content = [
                     'codRespuesta' => $parameters['codRespuesta'],
                     'desRespuesta' => $parameters['desRespuesta'],
                 ];
             }
             if ($parameters['method'] == "getLogData"){
                 $content = [
                     'codRespuesta' => $parameters['codRespuesta'],
                     'desRespuesta' => $parameters['desRespuesta'],
                     'data' => $parameters['data'],
                 ];
             }             
             if ($parameters['method'] == "instaPagoCrearPago"){
                 $content = [
                     'codRespuesta' => $parameters['codRespuesta'],
                     'desRespuesta' => $parameters['desRespuesta'],
                     'id' => $parameters['id'],
                     'request' => $parameters['request'],
                     'response' => $parameters['response'],
                 ];
             }
             if ($parameters['method'] == "InstaPagoCompletarPago"){
                 $content = [
                     'codRespuesta' => $parameters['codRespuesta'],
                     'desRespuesta' => $parameters['desRespuesta'],
                     'request' => $parameters['request'],
                     'response' => $parameters['response'],
                 ];
             }
             
             /*return new Response($this->serialize->serialize($response, "json"));*/
             $response = new Response();
             $response->setContent($this->serialize->serialize($content, "json"));
             $response->headers->set('Content-Type', 'text/plain');
             $response->setStatusCode(200);
             return $response;             
         }

        /**
         * Metodo de Respuesta negativa del objeto HTTP
         * @param array $parameters Parametros del objeto HTTP.
         * @return response.
         **/        
        public function responseErrorHttp($parameters){

                $response = [
                    'code' => 500,
                    'error' => true,
                    'result' => false,
                    'response' => $parameters['message'],
                ];
                return new Response($this->serialize->serialize($response, "json"));
        }

        /**
         * Metodo de Respuesta afirmativa del objeto HTTP del API DE Vista
         * @param array $parameters Parametros del objeto HTTP.
         * @return response.
         ***/
        public function responseAcceptHttpVista($parameters){
            $content = [
                'code' =>200,
                'error' =>$parameters["error"],
                'result' => $parameters["response"],
                'response' => $parameters['data'],
            ];
            $response = new Response();
            $response->setContent($this->serialize->serialize($content,'json'));
            $response->headers->set('Content-Type', 'text/plain');
            $response->setStatusCode(200);
            return $response;
        }
        
        
        /**
         * Metodo de Respuesta afirmativa del objeto HTTP del API DE Vista
         * @param array $parameters Parametros del objeto HTTP.
         * @return response.
         ***/
        public function responseAcceptHttpPayment($parameters){
            $content = [
                'code' =>200,
                'error' =>$parameters["error"],
                'response' => $parameters['data'],
            ];
            $response = new Response();
            $response->setContent($this->serialize->serialize($content,'json'));
            $response->headers->set('Content-Type', 'text/plain');
            $response->setStatusCode(200);
            return $response;
        }

}