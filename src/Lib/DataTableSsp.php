<?php
namespace App\Lib;
use Symfony\Component\HttpFoundation\Response;
use App\Lib\DaoLib;
/**
 * Summary.
 *
 * Description: Libreria PHP para datatables
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 */
/**
  * @method void __construct() Constructor
  * @method response responseAcceptHttp($parameters) Metodo de Respuesta afirmativa del objeto HTTP
  */ 
class DataTableSsp
{       
        
    /**
	 * Perform the SQL queries needed for an server-side processing requested,
	 * utilising the helper functions of this class, limit(), order() and
	 * filter() among others. The returned array is ready to be encoded as JSON
	 * in response to an SSP request, or can be modified if needed before
	 * sending back to the client.
	 *
	 *  @param  array $request Data sent to server by DataTables
     *  @param  string $em Entity Manager
	 *  @param  string $table SQL table to query
	 *  @param  string $primaryKey Primary key of the table
	 *  @param  array $columns Column information array
	 *  @return array          Server-side processing response array
	 */
	static function simple ( $request,$em, $table, $primaryKey, $columns, $whereOptional )
	{
        $bindings = array();
        $daoLib  =  new DaoLib($em);

        $parametros="";
        $where="";
        $limit="";
        $order="";

		//$db = self::db( $conn );
		// Build the SQL query string from the request
		$limit = self::limit( $request, $columns );
		$order = self::order( $request, $columns );
		$where = self::filter( $request, $columns, $bindings );
        // Main query to actually get the data
        
        if ( $whereOptional !== '' ) {
            if ( $where !== '') {
                foreach($whereOptional as $valor){
                    $valorParametro = $valor["tipodato"]=="string" ? "'".$valor['valor']."'" : $valor["valor"];
                    $parametros =  $valor["operadorlogico"] ." ". $valor["campo"] ." ".$valor["operador"]. $valorParametro ;
                }
                $where .= $parametros;
                
            }else{
				$count=0;
						
                $where = 'WHERE ';
                foreach($whereOptional as $valor){
					if($count==0){
                    	$valorParametro = $valor["tipodato"]=="string" ? "'".$valor['valor']."'" : $valor["valor"];
						$parametros .= $valor["campo"] ." ".$valor["operador"]. $valorParametro ;
					}else{
						$valorParametro = $valor["tipodato"]=="string" ? "'".$valor['valor']."'" : $valor["valor"];
						$parametros .= $valor["operadorlogico"] ." ". $valor["campo"] ." ".$valor["operador"]. $valorParametro ;
					}
					$count++;
				}
				
                $where = $where . $parametros; 
            }
        
        }


		$sql= "SELECT ".implode(", ", self::pluck($columns, 'db'))." FROM $table $where $order $limit";
        $data = $daoLib->prepareStament($sql,"asociativo");  
   
       // var_dump($data);
		/*$data = self::sql_exec( $db, $bindings,
			"SELECT `".implode("`, `", self::pluck($columns, 'db'))."`
			 FROM `$table`
			 $where
			 $order
			 $limit"
		);*/
		// Data set length after filtering
		/*$resFilterLength = self::sql_exec( $db, $bindings,
			"SELECT COUNT(`{$primaryKey}`)
			 FROM   `$table`
			 $where"
        );*/
        $sql =" SELECT COUNT($primaryKey) as total
        FROM $table
        $where";
        $resFilterLength = $daoLib->prepareStament($sql,"asociativo");  
        $recordsFiltered = $resFilterLength[0]["total"];
        
        // Total data set length
		/*$resTotalLength = self::sql_exec( $db,
			"SELECT COUNT(`{$primaryKey}`)
			 FROM   `$table`"
        );*/
        $sql ="SELECT COUNT($primaryKey) as total
        FROM $table";
        $resTotalLength = $daoLib->prepareStament($sql,"asociativo");  
        $recordsTotal = $resTotalLength[0]["total"];
		/*$recordsTotal = $resTotalLength[0][0];*/
		/*
		 * Output
		 */
		/*return array(
			"draw"            => isset ( $request['draw'] ) ?
				intval( $request['draw'] ) :
				0,
			"recordsTotal"    => intval( $recordsTotal ),
			"recordsFiltered" => intval( $recordsFiltered ),
			"data"            => self::data_output( $columns, $data )
        );*/
         $draw = $request->query->get('draw');   
        //  $x =array(
		// 	"draw"=> isset ( $draw ) ?
		// 		intval( $draw ) :
		// 		0,
		// 	"recordsTotal"    => intval( $recordsTotal ),
		// 	"recordsFiltered" => intval( $recordsFiltered ),
		// 	"data"            => self::data_output( $columns, $data )
        // );

        return array(
			"draw"=> isset ( $draw ) ?
				intval( $draw ) :
				0,
			"recordsTotal"    => intval($recordsTotal) ,
			"recordsFiltered" => intval( $recordsFiltered ),
			"data"            => self::data_output( $columns, $data )
		);


	}

    /**
	 * Paging
	 *
	 * Construct the LIMIT clause for server-side processing SQL query
	 *
	 *  @param  array $request Data sent to server by DataTables
	 *  @param  array $columns Column information array
	 *  @return string SQL limit clause
	 */
	static function limit ( $request, $columns )
	{
        $limit = '';
        $start=$request->query->get('start');
		if ( isset($start) && $request->query->get('length') != -1 ) {
			$limit = "LIMIT ".intval($start).", ".intval($request->query->get('length'));
		}
		return $limit;
	}
       
    	/**
	 * Ordering
	 *
	 * Construct the ORDER BY clause for server-side processing SQL query
	 *
	 *  @param  array $request Data sent to server by DataTables
	 *  @param  array $columns Column information array
	 *  @return string SQL order by clause
	 */
	static function order ( $request, $columns )
	{
        $order = '';
		$order=$request->query->get('order');
        if ( isset($order) && count($order) ) {
			
			//if(!(strpos($columns[$order[0]["column"]]["db"],"AS")) and (!strpos($columns[$order[0]["column"]]["db"],"as"))){
				$dir = $order[0]["dir"] === 'asc' ? 'ASC' : 'DESC';
				$order = 'order by '.$columns[$order[0]["column"]]["db"].' '.$order[0]["dir"];	
			/*}else{
				$order = 'order by 1 '.$order[0]["dir"];	
			}*/
          
        }
		return  $order;
    }
    
    /**
	 * Searching / Filtering
	 *
	 * Construct the WHERE clause for server-side processing SQL query.
	 *
	 * NOTE this does not match the built-in DataTables filtering which does it
	 * word by word on any field. It's possible to do here performance on large
	 * databases would be very poor
	 *
	 *  @param  array $request Data sent to server by DataTables
	 *  @param  array $columns Column information array
	 *    sql_exec() function
	 *  @return string SQL where clause
	 */
	static function filter ( $request, $columns )
	{
		$globalSearch=array();
        $columnSearch= $request->query->get('columns');
        $search= $request->query->get('search');
        
  
       
        foreach($columnSearch as $clave=>$valor){
             if($valor["searchable"]){      
                if(!(strpos($columns[$valor['data']]['db'],"AS")) and (!strpos($columns[$valor['data']]['db'],"as"))){
					if(!empty($search["value"])){
						$globalSearch[] = $columns[$valor['data']]['db']." LIKE '%".$search["value"]."%'";  
					} 	
                }
               /* strpos($columns[$valor['data']]['db'],"AS"){
                    
                } */ 
             }

        }
		$where = '';
		if ( count( $globalSearch )>0 ) {
			$where = '('.implode(' OR ', $globalSearch).')';
		}
		
		if ( $where !== '' ) {
			$where = 'WHERE '.$where;
        }
       
		return $where;
    }
    

	/**
	 * Pull a particular property from each assoc. array in a numeric array, 
	 * returning and array of the property values from each item.
	 *
	 *  @param  array  $a    Array to get data from
	 *  @param  string $prop Property to read
	 *  @return array        Array of property values
	 */
	static function pluck ( $a, $prop )
	{
		$out = array();
		for ( $i=0, $len=count($a) ; $i<$len ; $i++ ) {
			$out[] = $a[$i][$prop];
		}
		return $out;
    }
    

    /**
	 * Create the data output array for the DataTables rows
	 *
	 *  @param  array $columns Column information array
	 *  @param  array $data    Data from the SQL get
	 *  @return array          Formatted data in a row based format
	 */
	static function data_output ( $columns, $data )
	{

		$out = array();
		for ( $i=0, $ien=count($data) ; $i<$ien ; $i++ ) {
			$row = array();
			for ( $j=0, $jen=count($columns) ; $j<$jen ; $j++ ) {
				$column = $columns[$j];
				// Is there a formatter?
				if ( isset( $column['formatter'] ) ) {
                    
					$row[ $column['dt'] ] = $column['formatter']( $data[$i][ $column['db'] ], $data[$i] );
				}
				else {
                    $row[ $column['dt'] ] =$data[$i][$columns[$j]['nombreindice']];
				}
			}
			$out[] = $row;
		}
		return $out;
	}

}