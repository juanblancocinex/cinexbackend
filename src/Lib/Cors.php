<?php
namespace App\Lib;

/**
 * Summary.
 *
 * Description: Clase para el manejo de protocolo HTTP con corns
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 */
/**
  * @method object response requestGet($url,$headers,$query) Realiza una petición - Get a la URI especificada
  */ 
class Cors
{

        /**
         * Realiza una petición - Get a la URI especificada.
         * @param string $url url del site.
         * @param string $headers cabecera http.
         * @param string $query query de busqueda.
         * @return string array.
         **/
        public static function requestGet($url,$headers,$query){
                $response = \Unirest\Request::get($url,$headers,$query);
                return $response;
        }
}