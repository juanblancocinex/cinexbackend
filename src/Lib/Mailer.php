<?php
namespace App\Lib;

/**
 * Summary.
 * Description: Mail Librery
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 */
/**
  * @method array enviarEmail() EnviaEmail
  */ 
class Mailer
{

        private $mymailer;
        public function __construct($mailer)
        {
            $this->mailer = $mailer;
        }

        /**
         * Envia Email
         * @param string $xmlString string xml.
         * @return string Respuesta del Servicio,
         */
        public function enviarEmail($emailTo,$html,$titulo, $mailer){
            $message = (new \Swift_Message($titulo))
            ->setFrom('cinex@cinex.com.ve')
            ->setTo($emailTo)        
            ->setBody($html, 'text/html')
            ;
            $this->mymailer->send($message);
        }

     


        


}






