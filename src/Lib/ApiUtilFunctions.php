<?php
namespace App\Lib;
use App\Lib\XmlLib;
use App\Entity\TokenTrans;

/**
 * Summary.
 *
 * Description: Funciones comunes del Backend
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 */
/**
  * @method array getMonthDescription() Construye Array de Meses
  * @method array getLenguajeVersionAndIcono(array $versiones) Construye Array de Icono según la versión
  */ 
class ApiUtilFunctions
{

        protected $http;
    
        /**
         * Constructor.
         * @param servicio $http Objeto de la clase Http.
         **/
        public function __construct()
        {
 
        }
        
        /**
         * mapea las peliculas de un complejo.
         * @param string $complejo nombre del complejo.
         * @param string $data del complejo.
         * @return peliculas[].
         **/ 
        function mapea_peliculas_complejo($complejo,$data){
            $pelicula = "";
            foreach($data as $valor){
                if($valor["codigoComplejo"]==$complejo && $pelicula!=$valor["referencia"]){
                    $pelicula=$valor["referencia"];
                    $peliculas[] = array("nombre"=>$valor["referencia"],"genero"=>$valor["genero"],"censura"=>$valor["censura"],"duracion"=>$valor["duracion"],"director"=>$valor["director"],"detallePelicula"=>mapea_peliculas_detalle($pelicula,$data));
                }
            }
            return $peliculas;
        }
        
        /**
         * mapea las funciones de una pelicula.
         * @param string $complejo nombre del compljo.
         * @param string $sala nombre de la sala.
         * @param string $pelicula nombre de la pelicula.
         * @return detalles[].
         **/        
        function mapea_funciones_peliculas($complejo,$sala,$pelicula,$data){
            $url = "";
            $JSONsemafono = "";
            $funcion="";
            $url = "";
            foreach($data as $valor){
                if($valor["nombreComplejo"]==$complejo && $valor["sala"]==$sala && $valor["codVista"] == $pelicula){
                    
                    if($funcion!=$valor["hora"] && $valor["codVista"] == $pelicula){
                        $funcion=$valor["hora"];
                        
                        $url = "http://smtp.cinex.com.ve/clases/cinexDataServiceMethods.php?method=getsessionoccupation&sesionid=".$valor["sesionID"];
                        $CinexWar = curl_init($url);
                        curl_setopt($CinexWar, CURLOPT_RETURNTRANSFER, TRUE);
                        $archivo = curl_exec($CinexWar);
                        if($archivo === false) {
                            $jResultService["color"]="#5cb85c";
                            $jResultService["asientosdisponibles"]="";
                        } else {
                            $JsonSemaforoProgramacion = json_decode($archivo);
                            $jResultService["color"]=$JsonSemaforoProgramacion->color;
                            $jResultService["asientosdisponibles"]=$JsonSemaforoProgramacion->asientosdisponibles;
                        }
                        
                        $funciones[] = array("hora"=>$valor["hora"],
                            "ventaActiva"=>$valor["ventaActiva"],
                            "fecha"=>$valor["fecha"],
                            "abierta"=>$valor["ventaActiva"],
                            "seleccionAsientosActiva"=>($valor["seleccionAsientosActiva"]=="Y") ? true : false,
                            "horaFormateada"=>$valor["horaFormateada"],
                            "sesionID"=>$valor["sesionID"],
                            "asientosDisponible"=>$jResultService["asientosdisponibles"],
                            "colorSemaforo"=>$jResultService["color"],
                        );
                    }
                }
            }
            return $funciones;
        }
                
        /**
         * mapea las peliculas de un complejo.
         * @param string $pelicula nombre de la pelicula.
         * @param string $data del complejo.
         * @return detalles[].
         **/        
        function mapea_peliculas_detalle($pelicula,$data){
            $versiones =  "";
            $lenguaje = "";
            $icono_lenguaje = "";
            $versionDetalle = "";
            $icono_version = "";
            $codigoPelicula = "";
            $sala = "";
            foreach($data as $valor){                
                if($valor["referencia"]==$pelicula  &&  $valor["codVista"] != $codigoPelicula){
                    $codigoPelicula=$valor["codVista"];
                    $sala = $valor["sala"];
                    
                    $versiones = explode(" ",$valor["versiones"]);
                    if(strtolower($versiones[0])=="esp" || strtolower($versiones[1])=="esp" ){
                        $lenguaje= "ESP";
                        $icono_lenguaje = 'LengES.png';
                    }elseif(strtolower($versiones[0])=="sub" || strtolower($versiones[1])=="sub" ){
                        $lenguaje= "SUB";
                        $icono_lenguaje = 'LengSUB.png';
                    }
                    if(strtolower($versiones[0])=="dig" || strtolower($versiones[1])=="dig" ){
                        $versionDetalle= "DIG";
                        $icono_version = "2d-white.png";
                    }elseif(strtolower($versiones[0])=="3d" || strtolower($versiones[1])=="3d" ){
                        $versionDetalle= "3D";
                        $icono_version = "Vers3D.png";
                    }elseif(strtolower($versiones[0])=="4dx" || strtolower($versiones[1])=="4dx" ){
                        $versionDetalle= "4DX";
                        $icono_version = "Vers4DX.png";
                    }
                    $detalles[] = array(
                        "sala"=>strtoupper($valor["sala"]),
                        "lenguaje"=>$lenguaje,
                        "icono_lenguaje"=>$icono_lenguaje,
                        "peliculaCodigo"=>$valor["codVista"],
                        "versionDetalle"=>$versionDetalle,
                        "icono_version"=>$icono_version,
                        "version"=>$valor["versiones"],
                        "funciones"=>mapea_funciones_peliculas($valor["nombreComplejo"],$valor["sala"],$valor["codVista"],$data)
                    );                    
                }else if($valor["referencia"]==$pelicula  &&  $valor["sala"] != $sala){
                    $versiones = explode(" ",$valor["versiones"]);
                    $sala = $valor["sala"];
                    if(strtolower($versiones[0])=="esp" || strtolower($versiones[1])=="esp" ){
                        $lenguaje= "ESP";
                        $icono_lenguaje = 'LengES.png';
                    }elseif(strtolower($versiones[0])=="sub" || strtolower($versiones[1])=="sub" ){
                        $lenguaje= "SUB";
                        $icono_lenguaje = 'LengSUB.png';
                    }
                    if(strtolower($versiones[0])=="dig" || strtolower($versiones[1])=="dig" ){
                        $versionDetalle= "DIG";
                        $icono_version = "2d-white.png";
                    }elseif(strtolower($versiones[0])=="3d" || strtolower($versiones[1])=="3d" ){
                        $versionDetalle= "3D";
                        $icono_version = "Vers3D.png";
                    }elseif(strtolower($versiones[0])=="4dx" || strtolower($versiones[1])=="4dx" ){
                        $versionDetalle= "4DX";
                        $icono_version = "Vers4DX.png";
                    }
                    $detalles[] = array(
                        "sala"=>strtoupper($valor["sala"]),
                        "lenguaje"=>$lenguaje,
                        "icono_lenguaje"=>$icono_lenguaje,
                        "peliculaCodigo"=>$valor["codVista"],
                        "versionDetalle"=>$versionDetalle,
                        "icono_version"=>$icono_version,
                        "version"=>$valor["versiones"],
                        "funciones"=>mapea_funciones_peliculas($valor["nombreComplejo"],$valor["sala"],$valor["codVista"],$data)
                    );
                }
            }
            return $detalles;
        }
        
                
        /**
         * mapea data de las peliculas.
         * @param string $complejo nombre del complejo.
         * @param string $data del complejo.
         * @return peliculas[].
         **/        
        function mapea_peliculasmov($complejo,$data){
            $versiones =  "";
            $lenguaje = "";
            $icono_lenguaje = "";
            $versionDetalle = "";
            $icono_version = "";
            $pelicula = "";            
            foreach($data as $valor){
                if($valor["nombreComplejo"]==$complejo  && $pelicula!=$valor["codVista"]){
                    $pelicula=$valor["codVista"];
                    $sala = $valor["sala"];
                    $versiones = explode(" ",$valor["versiones"]);                    
                    if(strtolower($versiones[0])=="esp" || strtolower($versiones[1])=="esp" ){
                        $lenguaje= "ESP";
                        $icono_lenguaje = 'LengES.png';
                    }elseif(strtolower($versiones[0])=="sub" || strtolower($versiones[1])=="sub" ){
                        $lenguaje= "SUB";
                        $icono_lenguaje = 'LengSUB.png';
                    }
                    if(strtolower($versiones[0])=="dig" || strtolower($versiones[1])=="dig" ){
                        $versionDetalle= "DIG";
                        $icono_version = "2d-white.png";
                    }elseif(strtolower($versiones[0])=="3d" || strtolower($versiones[1])=="3d" ){
                        $versionDetalle= "3D";
                        $icono_version = "Vers3D.png";
                    }elseif(strtolower($versiones[0])=="4dx" || strtolower($versiones[1])=="4dx" ){
                        $versionDetalle= "4DX";
                        $icono_version = "Vers4DX.png";
                    }
                    $peliculas[] = array(
                        "funciones"=>mapea_funcionesmov($valor["nombreComplejo"],$valor["sala"],$valor["codVista"],$data
                            ),                        
                        "peliculaNombre"=>$valor["referencia"],
                        "sala"=>$valor["sala"],
                        "lenguaje"=>$lenguaje,
                        "icono_lenguaje"=>$icono_lenguaje,
                        "censura"=>$valor["censura"],
                        "peliculaCodigo"=>$valor["codVista"],
                        "peliculaReferencia"=>$valor["referencia"],
                        "versionDetalle"=>$versionDetalle,
                        "icono_version"=>$icono_version,
                        "version"=>$valor["versiones"]                        
                    );                    
                }elseif($valor["nombreComplejo"]==$complejo  && $valor["sala"] != $sala) {
                    $pelicula=$valor["codVista"];
                    $versiones = explode(" ",$valor["versiones"]);
                    $sala = $valor["sala"];
                    if(strtolower($versiones[0])=="esp" || strtolower($versiones[1])=="esp" ){
                        $lenguaje= "ESP";
                        $icono_lenguaje = 'LengES.png';
                    }elseif(strtolower($versiones[0])=="sub" || strtolower($versiones[1])=="sub" ){
                        $lenguaje= "SUB";
                        $icono_lenguaje = 'LengSUB.png';
                    }
                    if(strtolower($versiones[0])=="dig" || strtolower($versiones[1])=="dig" ){
                        $versionDetalle= "DIG";
                        $icono_version = "2d-white.png";
                    }elseif(strtolower($versiones[0])=="3d" || strtolower($versiones[1])=="3d" ){
                        $versionDetalle= "3D";
                        $icono_version = "Vers3D.png";
                    }elseif(strtolower($versiones[0])=="4dx" || strtolower($versiones[1])=="4dx" ){
                        $versionDetalle= "4DX";
                        $icono_version = "Vers4DX.png";
                    }
                    $peliculas[] = array(
                        "funciones"=>mapea_funcionesmov($valor["nombreComplejo"],$valor["sala"],$valor["codVista"],$data),
                        "peliculaNombre"=>$valor["referencia"],
                        "sala"=>$valor["sala"],
                        "lenguaje"=>$lenguaje,
                        "icono_lenguaje"=>$icono_lenguaje,
                        "censura"=>$valor["censura"],
                        "peliculaCodigo"=>$valor["codVista"],
                        "peliculaReferencia"=>$valor["referencia"],
                        "versionDetalle"=>$versionDetalle,
                        "icono_version"=>$icono_version,
                        "version"=>$valor["versiones"]
                    );
                    
                }
            }
            return $peliculas;
        }        
    
        /**
         * mapea data de los complejos.
         * @param string $ciudad string de la ciudad.
         * @param string $data string de servicios de complejos.
         * @return complejos[].
         **/
        function mapea_complejos($ciudad,$data){
            $complejo = "";
            foreach($data as $valor){
                if($valor["nombreCiudad"]==$ciudad){
                    if($complejo!=$valor["nombreComplejo"]){
                        $complejo=$valor["nombreComplejo"];
                        $complejos[] = array("complejoNombre"=>$valor["nombreComplejo"],"complejoCodigo"=>$valor["codigoComplejo"],"complejoDireccion"=>$valor["direccion"],
                            "complejoTelefono"=>$valor["telefonoAtencion"],"peliculas"=>mapea_peliculas($valor["nombreComplejo"],$data));
                    }
                }
            }
            return $complejos;
        }
                      
        /**
         * mapea data de los complejos.
         * @param string $ciudad string de la ciudad
         * @param string $data string de servicios de complejos.
         * @return complejos[].
         **/        
        function mapea_complejosmov($ciudad,$data){
            $complejo = "";
            foreach($data as $valor){
                if($valor["nombreCiudad"]==$ciudad){
                    if($complejo!=$valor["nombreComplejo"]){
                        $complejo=$valor["nombreComplejo"];
                        $complejos[] = array("salas"=>array("peliculas"=>mapea_peliculasmov($valor["nombreComplejo"],$data),
                            
                            //$complejos[] = array("complejoNombre"=>$valor["nombreComplejo"],"complejoCodigo"=>$valor["codigoComplejo"],"salas"=>array("salaDescriocion"=>$valor["sala"],"peliculas"=>mapea_peliculasmov($valor["nombreComplejo"],$data),
                            
                        )
                        );
                        
                    }
                }
            }
            return $complejos;
        }
        
        /**
         * mapea data de las funciones.
         * @param string $complejo nombre de la funcion.
         * @param string $sala sala de la funcion.
         * @param string $pelicula pelicula de la funcion.
         * @param string $data data de la funcion. 
         * @return complejos[].
         **/
        function mapea_funciones($complejo,$sala,$pelicula,$data){
            $url = "";
            $funcion = "";
            foreach($data as $valor){
                if($valor["nombreComplejo"]==$complejo && $valor["sala"]==$sala && $valor["codVista"] == $pelicula){
                    
                    if($funcion!=$valor["hora"] &&  $valor["codVista"] == $pelicula){
                        
                        $url = "http://smtp.cinex.com.ve/clases/cinexDataServiceMethods.php?method=getsessionoccupation&sesionid=".$valor["sesionID"];
                        $CinexWar = curl_init($url);
                        curl_setopt($CinexWar, CURLOPT_RETURNTRANSFER, TRUE);
                        $archivo = curl_exec($CinexWar);
                        if($archivo === false) {
                            $jResultService["color"]="#5cb85c";
                            $jResultService["asientosdisponibles"]="";
                        } else {
                            $JsonSemaforoProgramacion = json_decode($archivo);
                            $jResultService["color"]=$JsonSemaforoProgramacion->color;
                            $jResultService["asientosdisponibles"]=$JsonSemaforoProgramacion->asientosdisponibles;
                            
                        }
                        $funcion=$valor["hora"];
                        $funciones[] = array("hora"=>$valor["hora"],
                            "ventaActiva"=>$valor["ventaActiva"],
                            "fecha"=>$valor["fecha"],
                            "abierta"=>$valor["ventaActiva"],
                            "censura"=>$valor["censura"],
                            "seleccionAsientosActiva"=>($valor["seleccionAsientosActiva"]=="Y") ? true : false,
                            "horaFormateada"=>$valor["horaFormateada"],
                            "sesionID"=>$valor["sesionID"],
                            "asientosdisponibles"=>$jResultService["asientosdisponibles"],
                            "color"=>$jResultService["color"],
                        );
                    }
                }
            }
            return $funciones;
        }
        
        /**
         * mapea funciones de las peliculas.
         * @param string $complejo nombre del complejo.
         * @param string $sala del complejo.
         * @param string $pelicula nombre de la pelicula.
         * @param string $datafuncion datos de la funcion.
         * @param string $sesionID identificador de la sesion.
         * @return funciones[].
         **/        
        function mapea_funcionesmovfinal($complejo,$sala,$pelicula,$datafuncion,$sesionID){
            $url = "";
            $funcion = "";
            $conthuertasSV=0;            
            foreach($datafuncion as  $clave=>$valor){
                $conthuertasSV++;
                    $dimew=strtotime("now");
                    $fin1= date("d-m-Y h:ia", $dimew);                    
                    $horf=strtolower(str_replace(' ', '', $datafuncion[$clave]->hora));
                    $f2=strtolower(str_replace('/', '-', $datafuncion[$clave]->fecha));
                    $fec = $f2." ".$horf;
                    $fechahora=strtotime($fec);
                    $fechaHoy=date("Y/m/d h:i:s a");
                    $url = "http://smtp.cinex.com.ve/clases/cinexDataServiceMethods.php?method=getsessionoccupation&sesionid=".$datafuncion[$clave]->sesionID;
                    $CinexWar = curl_init($url);
                    curl_setopt($CinexWar, CURLOPT_RETURNTRANSFER, TRUE);
                    $archivo = curl_exec($CinexWar);
                    if($archivo === false) {
                        $jResultService["color"]="#5cb85c";
                        $jResultService["asientosdisponibles"]="";
                    } else {
                        $JsonSemaforoProgramacion = json_decode($archivo);
                        if(strtotime("now")>$fechahora){
                            $colors = "gray";
                            $jResultService["color"]=$colors;
                        }else{
                            $jResultService["color"]=$JsonSemaforoProgramacion->color;
                        }
                        $jResultService["asientosdisponibles"]=$JsonSemaforoProgramacion->asientosdisponibles;
                        
                    }
                    $funcion=$datafuncion[$clave]->hora;                                        
                    if ($datafuncion[$clave]->codigoComplejo == "30" or $datafuncion[$clave]->codigoComplejo == "31" or  $datafuncion[$clave]->codigoComplejo == "32" or $datafuncion[$clave]->codigoComplejo == "33" or $datafuncion[$clave]->codigoComplejo == "34" or $datafuncion[$clave]->codigoComplejo == "35" or $datafuncion[$clave]->codigoComplejo == "36" or $datafuncion[$clave]->codigoComplejo == "37" or $datafuncion[$clave]->codigoComplejo == "38" or $datafuncion[$clave]->codigoComplejo == "39"){                        
                        $funciones[] = array("hora"=>$datafuncion[$clave]->hora,                          
                            "ventaActiva"=>false,                            
                            "fecha"=>$datafuncion[$clave]->fecha,                            
                            "abierta"=>($datafuncion[$clave]->ventaActiva=="Y") ? true : false,                            
                            "censura"=>$datafuncion[$clave]->censura,
                            "seleccionAsientosActiva"=>($datafuncion[$clave]->seleccionAsientosActiva=="Y") ? true : false,
                            "horaFormateada"=>$datafuncion[$clave]->hora,
                            "sesionID"=>$datafuncion[$clave]->sesionID,
                            "asientosdisponibles"=>$jResultService["asientosdisponibles"],
                            "color"=>$jResultService["color"],                            
                            "codigoComplejo"=>$datafuncion[$clave]->codigoComplejo,
                            "CANTIDAD HUERTAS"=>$conthuertasSV,                                                        
                        );
                        
                    }else{
                        $funciones[] = array("hora"=>$datafuncion[$clave]->hora,                            
                            "ventaActiva"=>($datafuncion[$clave]->ventaActiva=="Y") ? true : false,                            
                            "fecha"=>$datafuncion[$clave]->fecha,                            
                            "abierta"=>($datafuncion[$clave]->ventaActiva=="Y") ? true : false,                            
                            "censura"=>$datafuncion[$clave]->censura,
                            "seleccionAsientosActiva"=>($datafuncion[$clave]->seleccionAsientosActiva=="Y") ? true : false,
                            "horaFormateada"=>$datafuncion[$clave]->hora,
                            "sesionID"=>$datafuncion[$clave]->sesionID,
                            "asientosdisponibles"=>$jResultService["asientosdisponibles"],
                            "color"=>$jResultService["color"],                            
                            "codigoComplejo"=>$datafuncion[$clave]->codigoComplejo,
                            "CANTIDAD HUERTAS"=>$conthuertasSV,                                                        
                        );                        
                    }
                }
                return $funciones;
            }
            
        
        /**
         * mapea data de las peliculas.
         * @param string $complejo nombre del complejo.
         * @param string $data del complejo.
         * @return peliculas[].
         **/        
        function mapea_peliculas($complejo,$data){
            $versiones =  "";
            $lenguaje = "";
            $icono_lenguaje = "";
            $versionDetalle = "";
            $icono_version = "";
            $pelicula = "";
            
            foreach($data as $valor){
                if($valor["nombreComplejo"]==$complejo  && $pelicula!=$valor["codVista"]){
                    $pelicula=$valor["codVista"];
                    $sala = $valor["sala"];
                    $versiones = explode(" ",$valor["versiones"]);
                    
                    if(strtolower($versiones[0])=="esp" || strtolower($versiones[1])=="esp" ){
                        $lenguaje= "ESP";
                        $icono_lenguaje = 'LengES.png';
                    }elseif(strtolower($versiones[0])=="sub" || strtolower($versiones[1])=="sub" ){
                        $lenguaje= "SUB";
                        $icono_lenguaje = 'LengSUB.png';
                    }
                    if(strtolower($versiones[0])=="dig" || strtolower($versiones[1])=="dig" ){
                        $versionDetalle= "DIG";
                        $icono_version = "2d-white.png";
                    }elseif(strtolower($versiones[0])=="3d" || strtolower($versiones[1])=="3d" ){
                        $versionDetalle= "3D";
                        $icono_version = "Vers3D.png";
                    }elseif(strtolower($versiones[0])=="4dx" || strtolower($versiones[1])=="4dx" ){
                        $versionDetalle= "4DX";
                        $icono_version = "Vers4DX.png";
                    }
                    $peliculas[] = array("peliculaNombre"=>$valor["referencia"],
                        "sala"=>$valor["sala"],
                        "lenguaje"=>$lenguaje,
                        "icono_lenguaje"=>$icono_lenguaje,
                        "censura"=>$valor["censura"],
                        "peliculaCodigo"=>$valor["codVista"],
                        "peliculaReferencia"=>$valor["referencia"],
                        "versionDetalle"=>$versionDetalle,
                        "icono_version"=>$icono_version,
                        "version"=>$valor["versiones"],
                        "funciones"=>mapea_funciones($valor["nombreComplejo"],$valor["sala"],$valor["codVista"],$data)
                    );
                    
                }elseif($valor["nombreComplejo"]==$complejo  && $valor["sala"] != $sala) {
                    $pelicula=$valor["codVista"];
                    $versiones = explode(" ",$valor["versiones"]);
                    $sala = $valor["sala"];
                    if(strtolower($versiones[0])=="esp" || strtolower($versiones[1])=="esp" ){
                        $lenguaje= "ESP";
                        $icono_lenguaje = 'LengES.png';
                    }elseif(strtolower($versiones[0])=="sub" || strtolower($versiones[1])=="sub" ){
                        $lenguaje= "SUB";
                        $icono_lenguaje = 'LengSUB.png';
                    }
                    if(strtolower($versiones[0])=="dig" || strtolower($versiones[1])=="dig" ){
                        $versionDetalle= "DIG";
                        $icono_version = "2d-white.png";
                    }elseif(strtolower($versiones[0])=="3d" || strtolower($versiones[1])=="3d" ){
                        $versionDetalle= "3D";
                        $icono_version = "Vers3D.png";
                    }elseif(strtolower($versiones[0])=="4dx" || strtolower($versiones[1])=="4dx" ){
                        $versionDetalle= "4DX";
                        $icono_version = "Vers4DX.png";
                    }
                    $peliculas[] = array("peliculaNombre"=>$valor["referencia"],
                        "sala"=>$valor["sala"],
                        "lenguaje"=>$lenguaje,
                        "icono_lenguaje"=>$icono_lenguaje,
                        "censura"=>$valor["censura"],
                        "peliculaCodigo"=>$valor["codVista"],
                        "peliculaReferencia"=>$valor["referencia"],
                        "versionDetalle"=>$versionDetalle,
                        "icono_version"=>$icono_version,
                        "version"=>$valor["versiones"],
                        "funciones"=>mapea_funciones($valor["nombreComplejo"],$valor["sala"],$valor["codVista"],$data)
                    );
                }
            }
            return $peliculas;
        }
        
    
        /**
         * Construye Array de Meses.
         * @param string $data string de servicios de complejos.
         * @return meses[].
         **/
        public static function getMonthDescription(){
                $arrayMeses= array(
                        1=>"ENERO",2=>"FEBRERO",
                        3=>"MARZO",4=>"ABRIL",5=>"MAYO",
                        6=>"JUNIO",7=>"JULIO",8=>"AGOSTO",
                        9=>"SEPTIEMBRE",10=>"OCTUBRE",
                        11=>"NOVIEMBRE",12=>"DICIEMBRE");
                return  $arrayMeses;
        }

        /**
         * Construye Array de Icono según la versión.
         * @param array $versiones.
         * @return versiones[].
        **/
        public static function getLenguajeVersionAndIcono($versiones){

                if(strtolower($versiones[0])=="esp" || strtolower($versiones[1])=="esp" ){
                        $lenguaje= "ESP";
                        $icono_lenguaje = 'LengES.png';
                }elseif(strtolower($versiones[0])=="sub" || strtolower($versiones[1])=="sub" ){
                        $lenguaje= "SUB";
                        $icono_lenguaje = 'LengSUB.png';
                }
                if(strtolower($versiones[0])=="dig" || strtolower($versiones[1])=="dig" ){
                        $versionDetalle= "DIG";
                        $icono_version = "2d-white.png";
                }elseif(strtolower($versiones[0])=="3d" || strtolower($versiones[1])=="3d" ){
                        $versionDetalle= "3D";
                        $icono_version = "Vers3D.png";
                }elseif(strtolower($versiones[0])=="4dx" || strtolower($versiones[1])=="4dx" ){
                        $versionDetalle= "4DX";
                        $icono_version = "Vers4DX.png";   
                }
                return array("lenguaje"=>$lenguaje,
                             "icono_lenguaje"=>$icono_lenguaje,
                             "versionDetalle"=>$versionDetalle,
                             "icono_version"=>$icono_version
                            );   

        }

        /**
         * Construye String de Versiones con un operador "|" entre ellos ejemplo (2D | 3D | 4DX).
         * @param array $versiones.
         * @return versiones[].
         **/
        public static function getVersionsDB($versions){
            $versions = str_replace("ESP", "", $versions);
            $versions = str_replace("SUB", "", $versions);
            $versions = str_replace("DIG", "2D", $versions);
            $versions = str_replace("35MM", "2D", $versions);
            $versions = str_replace(" ", "", $versions);
            $arrayver = explode("|", $versions);
            $versionsReturn = "";
            for ($y = 0; $y < sizeof($arrayver); $y++){
                if ($arrayver[$y] != ""){
                    if ($arrayver[$y] == "2D" && !strstr($versionsReturn, "2D")){
                        $versionsReturn.= "2D|";
                    }
                    if ($arrayver[$y] == "3D" && !strstr($versionsReturn, "3D")){
                        $versionsReturn.= "3D|";
                    }
                    if ($arrayver[$y] == "4DX" && !strstr($versionsReturn, "4DX")){
                        $versionsReturn.= "4DX|";
                    }
                }
            }
            if ($versionsReturn == "") $versionsReturn = "2D|";
                return substr($versionsReturn, 0, strlen($versionsReturn)-1);
        }
        
        public static function generateToken($userid, $em){
            $token = 0;            
            for($x = 0; $x < 16; $x++) {
                $token += (rand() * 16);
            }
            // limpiar token previo del usuario.
            $userid = $userid;
            $this->deleteToken($userid);
            $token = substr($token, 0, 16);
            $tokenTrans = new TokenTrans();
            $tokenTrans->setTokenToken($token);
            $tokenTrans->setTokenUserid($userid);
            $em->persist($tokenTrans);
            $em->flush();
            $data = array("result"=>true, "response"=>"OK", "token"=>$token);
            return $http->responseAcceptHttp(array("data"=>$data));
        }
                
        public static function deleteToken($userid, $em){
            $tokenTrans = $em->getRepository("App:TokenTrans")->findByTokenUserid($userid);
            $em->remove($tokenTrans[0]);
            $em->flush();
            $data = array("result"=>true, "response"=>"OK");
            return $http->responseAcceptHttp(array("data"=>$data));
        }
                
        public static function validateToken($token, $userid, $em,$http){
            $http = new HttpLib();
            $tokenTrans = $em->getRepository("App:TokenTrans")->findOneBy(['tokenToken' => $token, 'tokenUserid' => $userid]);
            if ($tokenTrans){
                $result = true;
            }else{
                $result = false;
            }
            $data = array("result"=>true, "response"=>"OK", "validate"=>$result);
            return $http->responseAcceptHttp(array("data"=>$data));
        }   
        
        /**
         * Sanear cadenas de caracteres especiales.
         * @param string $string cadena a limpiar
         * @return cadena limpia de caracteres.
         */
        public function sanear_string2($string){
            $string = str_replace("Á","A", $string);
            $string = str_replace("É","E", $string);
            $string = str_replace("Í","I", $string);
            $string = str_replace("Ó","O", $string);
            $string = str_replace("Ú","U", $string);
            $string = str_replace("Ñ","N", $string);
            $string = str_replace(":","", $string);
            $string = str_replace(".","", $string);
            $string = str_replace("?","", $string);
            $string = str_replace("¿","", $string);
            $string = str_replace("¡","", $string);
            $string = str_replace("!","", $string);
            $string = str_replace("*","", $string);
            $string = str_replace("&","Y", $string);
            $string = str_replace("@","A", $string);
            $string = str_replace("","%20", $string);
            return trim($string);
        }
        
        /**
         * Sanear cadenas de caracteres especiales en generos de peliculas.
         * @param string $string cadena a limpiar
         * @return cadena limpia de caracteres.
         */        
        public function sanear_genero($string){
            $string = str_replace("á","Á", $string);
            $string = str_replace("é","É", $string);
            $string = str_replace("í","Í", $string);
            $string = str_replace("ó","Ó", $string);
            $string = str_replace("ú","Ú", $string);
            $string = str_replace("ñ","N", $string);
            $string = str_replace(":","", $string);
            $string = str_replace(".","", $string);
            $string = str_replace("?","", $string);
            $string = str_replace("¿","", $string);
            $string = str_replace("¡","", $string);
            $string = str_replace("!","", $string);
            $string = str_replace("*","", $string);
            $string = str_replace("&","Y", $string);
            $string = str_replace("@","A", $string);
            $string = str_replace("","%20", $string);
            return trim($string);
        }
        
        /**
         * Devuelve el formato de una pelicula basado en su nombre
         * @return formato de la pelicula
         */
        public function getFormatFromMovieName($strName){
            $formato = "";
            if (strstr(strtolower($strName), "dig") || strstr(strtolower($strName), "35mm")){
                $formato = "DIG";
            }else if (strstr(strtolower($strName), "3d")){
                $formato = "3D";
            }else if (strstr(strtolower($strName), "4dx")){
                $formato = "4DX";
            }else{
                $formato = "2D";
            }
            if (strstr(strtolower($strName), "esp")){
                $formato.= " ESP";
            }else if (strstr(strtolower($strName), "sub")){
                $formato.= " SUB";
            }
            return $formato;
        }
        
        /**
         * Devuelve el genero de una pelicuila
         * @return genero de la pelicula
         */
        public function getGenero($generoID){
            if ($generoID == "4"){
                return "DRAMA";
            }else if ($generoID == "5"){
                return "COMEDIA";
            }else if ($generoID == "6"){
                return "ACCIÓN";
            }else if ($generoID == "7"){
                return "SUSPENSO";
            }else if ($generoID == "8"){
                return "TERROR";
            }else if ($generoID == "9"){
                return "MUSICAL";
            }else if ($generoID == "10"){
                return "EPICA";
            }else if ($generoID == "11"){
                return "ANIMACIÓN";
            }else if ($generoID == "12"){
                return "AVENTURAS";
            }else if ($generoID == "13"){
                return "FICCIÓN";
            }else if ($generoID == "14"){
                return "ADULTOS";
            }else if ($generoID == "15"){
                return "OTRAS";
            }else if ($generoID == "16"){
                return "FANTASÍA";
            }else if ($generoID == "17"){
                return "INFANTIL";
            }else if ($generoID == "18"){
                return "ROMÁNTICA";
            }else if ($generoID == "19"){
                return "DOCUMENTAL";
            }else if ($generoID == "24"){
                return "DEPORTE";
            }
        }
        
}






