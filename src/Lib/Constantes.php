<?php
namespace App\Lib;

/**
 * Summary.
 *
 * Description: Constantes del Sistemas
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 */
class Constantes
{
   
  const TIPE_ARRAY = array("objeto"=>\PDO::FETCH_OBJ,"asociativo"=>\PDO::FETCH_ASSOC);  
  
  
  const PATHSITE = array(
                           "imagenescartelera"=>'/imagenes/cartelera/',
                           "urlsipnosiscartelera"=>'/sinopsis.php?cartelera=1&pelicula=',
                           "imagenessinopsis"=>'/imagenes/sinopsis/',
                           'imagenescarrusel'=>'/imagenes/carrusel/',
                           "imagenesdestacados"=>'/imagenes/destacados/'
                         );  
                
                        
  const APICORS = array(
                          "semaforo"=>'http://172.28.85.214/clases/cinexDataServiceMethods.php',
                      );        
 
  /*const APICORS = array(
      "semaforo"=>'http://172.28.85.214/clases/cinexDataServiceMethods.php',
  ); */   
  

  const COMPLEJOPROPACINE = array( "39", "32", "34", "33", "36", 
                                   "31", "30", "35", "38", "37", 
                                   "2", "3");   


}