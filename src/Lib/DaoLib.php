<?php
namespace App\Lib;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Summary.
 *
 * Description: Manejo de Acceso a Datos
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 */
/**
  * @method void __construct(EntityManagerInterface $entityManager) Constructor
  * @method array prepareStament($sql,$tipo) Crea una Query en la base de datos
  */ 
class DaoLib
{
        private $entityManager;

        public function __construct(EntityManagerInterface $entityManager)
        {
           $this->entityManager = $entityManager;
        }

        /**
         * Crea una Query en la base de datos
         * @param string $ql Sentencia SQL.
         * @param string $tipo Determina si el retorno del metodo es un objeto o array
         * @return string array.
         **/
        public function prepareStament($sql,$tipo="asociativo"){
                //try {          
                        $tipo=Constantes::TIPE_ARRAY[$tipo];
                        $statement = $this->entityManager->getConnection()->prepare($sql);
                        $statement->execute();
                      // return $statement->fetchAll();
                     
                        return $statement->fetchAll($tipo);
               /* } catch (Exception $ex) {
                        return $ex->message();
                } catch (\Doctrine\DBAL\Exception\InvalidFieldNameException $ex) {
                        return "Nombre de Campo Invalido";*/
                //}
               
        }
}