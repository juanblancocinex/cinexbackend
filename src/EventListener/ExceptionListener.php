<?php
namespace App\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

/**
 * Summary.
 *
 * Description: Listener of Exceptions
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 */
/**
  * @method void __construct(service serializer) Constructor
  * @method object reponse onKernelException(GetResponseForExceptionEvent $event) Metodo de Escucha para las excepciones
  */ 
class ExceptionListener
{
    protected $serializer;
        
    /**
     * Constructor.
     * @param servicio $serializer Objeto de la clase Serializer.
    **/
    public function __construct($serializer)
    {
      $this->serialize = $serializer;
    }

     /**
     * Listener de Excepciones.
     * @param object $event string de servicios de complejos.
     * @return response json.
    **/
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        // You get the exception object from the received event
        $exception = $event->getException();
       /* $message = sprintf(
            'My Error says: %s with code: %s',
            $exception->getMessage(),
            $exception->getCode()
        );*/

        // Customize your response object to display the exception details
        $response = new Response();
        $message = [
            'code' => 500,
            'error' => true,
            'result' => false,
            'response' => $exception->getMessage(),
        ];
        $response->setContent($this->serialize->serialize($message, "json"));
     
        // HttpExceptionInterface is a special type of exception that
        // holds status code and header details
        if ($exception instanceof HttpExceptionInterface) {
            $response->setStatusCode($exception->getStatusCode());
            $response->headers->replace($exception->getHeaders());
        } else {
            $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        $event->setResponse($response);
    }
}