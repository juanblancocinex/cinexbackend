<?php

namespace App\Repository;

use App\Entity\PeliculaTest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Lib\DaoLib;
/**
    * Summary.
    * Description: PeliculaTestRepository
    * @since 1.0
    * @author Juan Blanco <jblanco@cinex.com.ve>
    * @copyright Envepro 2018
    * @method versiones[]||null getVersiones($referencia) Versiones de la Pelicula
    * @method peliculas[]||null getPeliculasById($id) Consulta de Pelicula por Codigo
    * @method peliculas[]||null getSessionesEspeciales($id) Selecciones especial de sesiones de pelicula
*/ 
class PeliculaTestRepository extends ServiceEntityRepository
{
        
    /**
        * Constructor.
        * @param DaoLib $daoLib Libreria.
        * @param RegistryInterface $registry Interfaz.
    **/
    public function __construct(DaoLib $daoLib,RegistryInterface $registry)
    {
        $this->daoLib =$daoLib;
        parent::__construct($registry, PeliculaTest::class);
    }
    
    /**
     * Versiones de la pelicula
     * @param string $referencia nombre de pelicula.
     * @return versiones[]||null.
     **/
    public function getVersiones($referencia)
    {
        $dql="SELECT COALESCE(p.versiones,'') as versiones
               FROM App:PeliculaTest p 
               WHERE p.referencia  ='".$referencia."' GROUP BY p.versiones" ;
        //var_dump($dql);
        $data=$this->getEntityManager()->createQuery($dql)->getResult();
        //var_dump($data);
        //exit;
        if(count($data)<0){
            $data=[];
        }       
        return $data;
    } 
        
     /**
     * Datos de Pelicula por Id
     * @param string $id nombre de pelicula.
     * @return pelicula[]||null.
     **/
    public function getPeliculasById($id){
        $dql=" SELECT 
        COALESCE(p.duracion,'') as duracion,
        COALESCE(p.urltrailer,'') as urlTrailer, 
        COALESCE(p.nombresinformato,'') as nombreSinFormato, 
        COALESCE(p.censura,'')as censura , 
        COALESCE(p.director,'')as director , 
        COALESCE(p.espreventa,'')as esPreventa, 
        COALESCE(p.referencia,'')as referencia,
        COALESCE(p.nombre,'') as nombre,
        COALESCE(p.genero,'')as genero, 
        COALESCE(p.esvenezolana,'')as esVenezolana , 
        COALESCE(p.esdestacada,'')as esDestacada, 
        COALESCE(p.fechaestreno,'')as fechaEstreno, 
        COALESCE(p.elenco,'')as elenco,
        COALESCE(p.descripcion,'')as sinopsisResumen,
        COALESCE(p.nombreoriginal,'') as nombreOriginal
        FROM App:PeliculaTest p 
        WHERE p.referencia  = '".$id."' group by p.nombresinformato order by p.peliculaid" ;
        
        $data=$this->getEntityManager()->createQuery($dql)->getResult();

        if(count($data)<0){
            $data=[];
        }       
        return $data;
    }

     /**
     * Sesion de Peliculas
     * @param string $id Id de la session.
     * @return peliculas[]||null.
     **/
    public function getSessionesEspeciales($id)
    {
        $dql="SELECT a.peliculaid AS PELICULAID,
        COALESCE(a.nombresinformato,'') AS nombreSinFormato,
        COALESCE(a.referencia,'') AS referencia,
        COALESCE(a.nombre,'') AS nombre,
        COALESCE(a.nombreoriginal,'') AS nombreOriginal, 
        COALESCE(a.censura,'') AS censura,
        COALESCE(a.genero,'') AS genero,
        COALESCE(dateformat(a.fechaestreno, '%d/%m/%Y'),'') as fechaEstreno,
        COALESCE(a.urltrailer,'') AS urlTrailer,
        COALESCE(a.director,'') AS director,
        COALESCE(a.duracion,'') AS duracion,
        COALESCE(a.esdestacada,'') AS esDestacada,
        COALESCE(a.espreventa,'') AS esPreVenta,
        COALESCE(a.esvenezolana,'') AS esVenezolana,
        COALESCE(a.elenco,'') AS elenco
        FROM App:ProgramacionTest p INNER JOIN p.codigopelicula a 
        INNER JOIN p.codigocomplejo b INNER JOIN b.ciudadId c
        INNER JOIN a.sesionesespeciales d
        where d.ids =".$id." and p.fechaProgramacion >='".date("Y-m-d")."'
        GROUP BY a.nombresinformato
        ORDER BY a.fechaestreno DESC , a.referencia , a.esdestacada DESC" ;
        return $this->getEntityManager()->createQuery($dql)->getResult();
    } 
    
    
    
    /**
     * Clasificacion de Peliculas Venezolana && Restreno && Preventa
     * @param string $referencia Nombre de la Pelicula.
     * @return peliculasdetalles[]||null.
     **/
    public function getClaficacionPelicula($referencia){
                    
                    $sql = "SELECT COALESCE(pelicula_detalles.esPreventa,'N')as espreventa, 
                             COALESCE(pelicula_detalles.esVenezolana,'N')as esvenezolana,
                             COALESCE(pelicula_detalles.esRestreno,'N') as esreestreno
                             FROM pelicula_detalles inner join pelicula_test on
                             pelicula_test.codVista = pelicula_detalles.codVista 
                            WHERE pelicula_test.referencia = '".$referencia."'";
                    return $this->daoLib->prepareStament($sql,"asociativo");    
        
    }
    
    
    /**
     * Datos de Pelicula por codigo de sinec
     * @param string $codSinec codigo sinec de la pelicula.
     * @return pelicula[]||null.
     **/
    public function getPeliculasByCodSinec($codSinec){
        $dql=" SELECT
        COALESCE(p.duracion,'') as duracion,
        COALESCE(p.urltrailer,'') as urlTrailer,
        COALESCE(p.nombresinformato,'') as nombreSinFormato,
        COALESCE(p.censura,'')as censura ,
        COALESCE(p.director,'')as director ,
        COALESCE(p.espreventa,'')as esPreventa,
        COALESCE(p.referencia,'')as referencia,
        COALESCE(p.nombre,'') as nombre,
        COALESCE(p.genero,'')as genero,
        COALESCE(p.esvenezolana,'')as esVenezolana ,
        COALESCE(p.esdestacada,'')as esDestacada,
        COALESCE(p.fechaestreno,'')as fechaEstreno,
        COALESCE(p.elenco,'')as elenco,
        COALESCE(p.descripcion,'')as sinopsisResumen,
        COALESCE(p.nombreoriginal,'') as nombreOriginal
        FROM App:PeliculaTest p
        WHERE p.codsinec = '".$codSinec."' group by p.nombresinformato order by p.peliculaid" ;
        
        $data=$this->getEntityManager()->createQuery($dql)->getResult();
        
        if(count($data)<0){
            $data=[];
        }
        return $data;
    }
    
    /**
     * Datos de Pelicula por codigo de vista
     * @param string $codVista codigo vista de la pelicula.
     * @return pelicula[]||null.
     **/
    public function getPeliculasByCodVista($codVista){
        $dql=" SELECT
        COALESCE(p.duracion,'') as duracion,
        COALESCE(p.urltrailer,'') as urlTrailer,
        COALESCE(p.nombresinformato,'') as nombreSinFormato,
        COALESCE(p.censura,'')as censura ,
        COALESCE(p.director,'')as director ,
        COALESCE(p.espreventa,'')as esPreventa,
        COALESCE(p.referencia,'')as referencia,
        COALESCE(p.nombre,'') as nombre,
        COALESCE(p.genero,'')as genero,
        COALESCE(p.esvenezolana,'')as esVenezolana ,
        COALESCE(p.esdestacada,'')as esDestacada,
        COALESCE(p.fechaestreno,'')as fechaEstreno,
        COALESCE(p.elenco,'')as elenco,
        COALESCE(p.descripcion,'')as sinopsisResumen,
        COALESCE(p.nombreoriginal,'') as nombreOriginal
        FROM App:PeliculaTest p
        WHERE p.codvista = '".$codVista."' group by p.nombresinformato order by p.peliculaid" ;
        
        $data=$this->getEntityManager()->createQuery($dql)->getResult();

        if(count($data)<0){
            $data=[];
        }
        return $data;
    }
    
 /**
     * Datos de Pelicula por codigo de vista
     * @param string $nombre nombre de la pelicula.
     * @return pelicula[]||null.
     **/
    public function getPeliculasByNombre($nombre){
        $dql=" SELECT
        COALESCE(p.duracion,'') as duracion,
        COALESCE(p.urltrailer,'') as urlTrailer,
        COALESCE(p.nombresinformato,'') as nombreSinFormato,
        COALESCE(p.censura,'')as censura ,
        COALESCE(p.director,'')as director ,
        COALESCE(p.espreventa,'')as esPreventa,
        COALESCE(p.referencia,'')as referencia,
        COALESCE(p.nombre,'') as nombre,
        COALESCE(p.genero,'')as genero,
        COALESCE(p.esvenezolana,'')as esVenezolana ,
        COALESCE(p.esdestacada,'')as esDestacada,
        COALESCE(p.fechaestreno,'')as fechaEstreno,
        COALESCE(p.elenco,'')as elenco,
        COALESCE(p.descripcion,'')as sinopsisResumen,
        COALESCE(p.nombreoriginal,'') as nombreOriginal
        FROM App:PeliculaTest p
        WHERE p.nombre = '".$nombre."' group by p.nombresinformato order by p.peliculaid" ;
        
        $data=$this->getEntityManager()->createQuery($dql)->getResult();

        if(count($data)<0){
            $data=[];
        }
        return $data;
    }
}
