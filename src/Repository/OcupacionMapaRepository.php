<?php

namespace App\Repository;



use App\Entity\OcupacionMapa;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Summary.
 *
 * Description: OcupacionMapaRepository
 *
 * @since 1.0
 * @author Humberto Suarez <hsuarez@cinex.com.ve>
 * @copyright Evenpro 2018
 */
/**
  * @method getOccupationMap[]||null Ocupación del Mapa
  * @method getCountOccupatedSeats[]||null Cantidad de asientos seleccionados en total por el usuario
  * @method getListOccupatedSeats[]||null Lista de los asientos ocupados por el usuario
 
  */ 
  
class OcupacionMapaRepository extends ServiceEntityRepository
{
    
    /****
        * Constructor.
        * @param RegistryInterface $registry Interfaz.
    ****/
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, OcupacionMapa::class);
    }
    
    /****
         * Ocupación del mapá
         * @return array|null.
     **/
    public function getOccupationMap($sesionID, $userID)
    {

        $dql="SELECT p.ocupFila, p.ocupColumna, p.ocupFecha, p.ocupHora, p.usuarioid, p.ocupPosicion, p.ocupTicketTypeCode
        FROM App:OcupacionMapa p where p.sesionid ='".$sesionID."' and p.usuarioid='".$userID."'  ORDER BY p.ocupId ASC";
        return $this->getEntityManager()->createQuery($dql)->getResult();
    }

    /****
         * Cantidad de asientos seleccionados en total por el usuario
         * @return array|null.
    ****/
    public function getCountOccupatedSeats($sesionID, $userID)
    {
        $dql="SELECT p.ocupId as listOcuppatedSeats
        FROM App:OcupacionMapa p where p.sesionid ='".$sesionID."' and p.usuarioid='".$userID."' ORDER BY p.ocupId ASC";
        return $this->getEntityManager()->createQuery($dql)->getResult();
    }



    /*****
         * Lista de los asientos ocupados por el usuario
         * @return array|null.
    ****/
    public function getListOccupatedSeats($sesionID, $userID)
    {
        $dql="SELECT COUNT(p.ocupId) as countOcuppatedSeats
        FROM App:OcupacionMapa p where p.sesionid ='".$sesionID."' and p.usuarioid='".$userID."'";
        return $this->getEntityManager()->createQuery($dql)->getResult();
    }


    /*****
         * Estatus de un asiento de una funcion
         * @return array|null.
    ****/
    public function getSeatStatus($sesionId, $fila,$columna)
    {
        $dql=" SELECT p.ocupId,p.usuarioid
        FROM App:OcupacionMapa p where 
        p.sesionid ='".$sesionId."' and 
        p.ocupFila=".$fila." 
        and p.ocupColumna=".$columna;
        return $this->getEntityManager()->createQuery($dql)->getResult();
    }
  
    /*****
         * Asientos ocupados en una sesión en VISTA
         * @return array|null.
    ****/
    public function getOccupatedSeats($sesionId)
    {
        $dql=" SELECT p.ocupFila, p.ocupColumna, 
                COALESCE(dateformat(p.ocupFecha, '%d/%m/%Y'),'') as ocupFecha ,
                COALESCE(dateformat(p.ocupHora, '%h:%i'),'') as ocupHora ,
                p.usuarioid, p.ocupPosicion, 
                p.ocupTicketTypeCode
                FROM App:OcupacionMapa p where 
                p.sesionid ='".$sesionId."'";
        return $this->getEntityManager()->createQuery($dql)->getResult();
    }

    /*****
         * Asientos ocupados en una sesión en VISTA
         * @return array|null.
    ****/
    public function getOccupatedSeatsFromUser($sesionId,$userid)
    {
        $dql=" SELECT p.ocupFila, p.ocupColumna, 
                COALESCE(dateformat(p.ocupFecha, '%d/%m/%Y'),'') as ocupFecha ,
                COALESCE(dateformat(p.ocupHora, '%h:%i'),'') as ocupHora ,
                p.usuarioid, p.ocupPosicion, 
                p.ocupTicketTypeCode
                FROM App:OcupacionMapa p where 
                p.sesionid ='".$sesionId."' AND p.usuarioid = ".$userid." ORDER BY p.ocupHora DESC";
        return $this->getEntityManager()->createQuery($dql)->getResult();
    }


   /*****
         * Asientos ocupados en una sesión en VISTA
         * @return array|null.
    ****/
    public function getLastOccupatedSeatsFromUserID($sesionId,$userid)
    {
 
         $query = $this->getEntityManager()->createQuery("SELECT 
                    p.ocupFila, p.ocupColumna, 
                    COALESCE(dateformat(p.ocupFecha, '%d/%m/%Y'),'') as ocupFecha ,
                    COALESCE(dateformat(p.ocupHora, '%h:%i'),'') as ocupHora ,
                    p.usuarioid, 
                    p.ocupPosicion, 
                    p.ocupTicketTypeCode FROM App:OcupacionMapa p 
                    where 
                p.sesionid ='".$sesionId."' AND p.usuarioid = ".$userid."");
                $query->setMaxResults(1);
                return $query->getResult();
    }
    
   /*****
         * Asientos ocupados en una sesión en VISTA
         * @return array|null.
    ****/
    public function getOccupeSeatsBySessionAndUsuario($sesionId,$userid)
    {
 
        $dql=" SELECT p
        FROM App:OcupacionMapa p where 
        p.sesionid ='".$sesionId."' AND p.usuarioid = ".$userid;
        return $this->getEntityManager()->createQuery($dql)->getResult();
    }


    
    /*****
         * Asientos ocupados en una sesión en VISTA
         * @return array|null.
    ****/
    public function getLastOccupatedSeatsFromUserIDGroupByTicketTyopCode($sesionId,$userid)
    {
 
        $dql=" SELECT p.ocupId, p.ocupTicketTypeCode
        FROM App:OcupacionMapa p where 
        p.sesionid ='".$sesionId."' AND p.usuarioid = ".$userid." GROUP BY p.ocupTicketTypeCode" ;
        return $this->getEntityManager()->createQuery($dql)->getResult();
    }
    
    /*****
     * Devuelve asientos que se van a Eliminar en el controlador
     * @return array|null.
     ****/
    public function getSetSeat($sesionId,$userid, $row, $column)
    {
        $dql="SELECT p
        FROM App:OcupacionMapa p where
        p.sesionid ='".$sesionId."' AND p.usuarioid = '".$userid."' AND p.ocupFila = '".$row."' AND p.ocupColumna = '".$column."'";
        return $this->getEntityManager()->createQuery($dql)->getResult();
    }
    
}
