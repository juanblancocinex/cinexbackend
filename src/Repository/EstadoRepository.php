<?php

namespace App\Repository;



use App\Entity\Estados;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Summary.
 *
 * Description: EstadoRepository
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 */
/**
  * @method estados[]||null getEstados() Estados
  */ 

class EstadoRepository extends ServiceEntityRepository
{
    
    /**
        * Constructor.
        * @param RegistryInterface $registry Interfaz.
    **/
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Estados::class);
    }
    
    /**
         * Estados
         * @return estados[]||null.
     **/
    public function getEstados()
    {

        $dql="SELECT p.idEstado AS estadoID,p.estado AS Estados
        FROM App:Estados p ORDER BY p.estado ";
        return $this->getEntityManager()->createQuery($dql)->getResult();
    }
    
}
