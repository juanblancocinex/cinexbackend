<?php

namespace App\Repository;

use App\Entity\VouchersEnviados;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Lib\DaoLib;

/**
  * Summary.
  * Description: VouchersRepository
  *
  * @since 1.0
  * @author Sir Sarmiento <ssarmiento@cinex.com.ve>
  * @copyright Envepro 2018
  * @method vouchersEnviados[]||null getCinexPassSendedInfo() Vouchers
  */ 

class VouchersEnviadosRepository extends ServiceEntityRepository
{
    /**
        * Constructor.
        * @param RegistryInterface $registry Interfaz.
    **/
    public function __construct(DaoLib $daoLib,RegistryInterface $registry)
    {
        $this->daoLib =$daoLib;
        parent::__construct($registry, VouchersEnviados::class);
    }
    
    /**
     * VouchersEnviados
     * @param string $codvouchers codigo de voucher.
     * @return VouchersEnviados[]||null.
     **/
    public function getCinexPassSendedInfo($codvoucher){
        $qb = $this->createQueryBuilder('p')
        ->Select('p.nombresapellidos')
        ->addSelect('p.correoelectronico')
        ->addSelect('p.fecha')
        ->setParameter('codvoucher', $codvoucher)
        ->where('p.codigoVoucher=:codvoucher');
        return $qb->getQuery()->getResult();
    }


  /**
     * VouchersDigitales
     * @param string $ci cedula del cliente.
     * @param string $codtipovouchers codigo del tipo de voucher.
     * @return VouchersDigitales[]||null.
     **/
    public function getVouchersDigitalesEnviadosPorUsuario($ci, $formato){
    

                $sql="SELECT * FROM vouchers_digitales A inner join               
                vouchers_digitales_tipo B on B.cod_tipo_voucher = A.cod_tipo_voucher inner join vouchers_enviados C
                on A.codigo_voucher = C.codigo_voucher
                where B.formatos_validos = '".$formato."' AND A.codigo_voucher  in (select (vouchers_enviados.codigo_voucher) 
                from  vouchers_enviados) and A.id_usuario = '".$ci."'";
                return $this->daoLib->prepareStament($sql,"asociativo");        
    }
            
           
        


}