<?php

namespace App\Repository;



use App\Entity\Municipios;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Summary.
 *
 * Description: MunicipiosRepository
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 */
/**
  * @method complejos[]||null getMunicipiosByIdEstado() Municipios
  */ 

class MunicipiosRepository extends ServiceEntityRepository
{
    
    /**
        * Constructor.
        * @param RegistryInterface $registry Interfaz.
    **/
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Municipios::class);
    }
    
    /**
         * Municipios
         * @return municipios[]||null.
     **/
    public function getMunicipiosByIdEstado($id)
    {
        $dql="SELECT p.idMunicipio AS municipioID,p.municipio AS Municipio,x.estado as Estado
        FROM App:Municipios p JOIN p.estado x  where p.idEstado=".$id." ORDER BY p.municipio";
        return $this->getEntityManager()->createQuery($dql)->getResult();
    }

 
}
