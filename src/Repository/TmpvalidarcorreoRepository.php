<?php

namespace App\Repository;

use App\Entity\Tmpvalidarcorreo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Lib\DaoLib;

/**
    * Summary.
    * Description: TmpvalidarcorreoRepository
    * @since 1.0
    * @author Ghensys Valero <gvalero@cinex.com.ve>
    * @copyright Envepro 2019
    */

class TmpvalidarcorreoRepository extends ServiceEntityRepository
{
    /**
        * Constructor.
        * @param RegistryInterface $registry Interfaz.
    **/
    public function __construct(DaoLib $daoLib, RegistryInterface $registry)
    {
        $this->daoLib =$daoLib;
        parent::__construct($registry, Tmpvalidarcorreo::class);
    }
    

    /**
     * Validar si el correo existe
     * @return void
     **/
    public function getExist($correo)
    {
        return $this->createQueryBuilder('p')
            ->select("p")
            ->andWhere('p.email = :correo')
            ->setParameter('correo', $correo)
            ->getQuery()
            ->getResult()
        ;
    }
}
