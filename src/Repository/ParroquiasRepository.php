<?php

namespace App\Repository;



use App\Entity\Parroquias;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Summary.
 *
 * Description: ParroquiasRepository
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 */
/**
  * @method complejos[]||null getParroquiasByIdMunicipio() Parroquias
  */ 

class ParroquiasRepository extends ServiceEntityRepository
{
    
    /**
        * Constructor.
        * @param RegistryInterface $registry Interfaz.
    **/
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Parroquias::class);
    }
    
    /**
         * Municipios
         * @return parroquias[]||null.
     **/
    public function getParroquiasByIdMunicipio($id)
    {
        $dql="SELECT p.idParroquia AS parroquiaID,p.parroquia AS Parroquia,x.municipio as Municipio
        FROM App:Parroquias p JOIN p.idMunicipio x  where x.idMunicipio=".$id." ORDER BY p.parroquia";
        return $this->getEntityManager()->createQuery($dql)->getResult();
    }

 
}
