<?php

namespace App\Repository;

use App\Entity\VouchersDigitalesTipo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
/**
  * Summary.
  * Description: VouchersRepository
  *
  * @since 1.0
  * @author Sir Sarmiento <ssarmiento@cinex.com.ve>
  * @copyright Envepro 2018
  * @method vouchersDigitalesTipo[]||null getVouchersDigitalesTipo() Vouchers
  */ 

class VouchersDigitalesTipoRepository extends ServiceEntityRepository
{
    
    /**
        * Constructor.
        * @param RegistryInterface $registry Interfaz.
    **/
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, VouchersDigitalesTipo::class);
    }
    
    /**
     * VouchersDigitalesTipo 
     * @return VouchersDigitalesTipo[]||null.
     **/
    public function getVouchersDigitalesTipo(){
        $qb = $this->createQueryBuilder('p')
            ->Select('p.codTipoVoucher')
            ->addSelect('p.categoriaId')
            ->addSelect('p.descripcion')
            ->orderBy('p.descripcion','DESC');
        return $qb->getQuery()->getResult();
    }
}