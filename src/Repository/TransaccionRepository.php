<?php

namespace App\Repository;

use App\Entity\Transaccion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Lib\DaoLib;
/**
    * Summary.
    * Description: TransaccionRepository
    * @since 1.0
    * @author Juan Blanco <jblanco@cinex.com.ve>
    * @copyright Envepro 2018
    * @method cedula[]||null getBuzon($cedula)
*/ 
class TransaccionRepository extends ServiceEntityRepository
{
        
    /**
        * Constructor.
        * @param DaoLib $daoLib Libreria.
        * @param RegistryInterface $registry Interfaz.
    **/
    public function __construct(DaoLib $daoLib,RegistryInterface $registry)
    {
        $this->daoLib =$daoLib;
        parent::__construct($registry, Transaccion::class);
    }
    
    /**
     * Lista de trasacciones
     * @param string $cedula de usuario.
     * @return cedula[]||null.
     **/
    public function getTransaccion($cedula)
    {       
        $button = "\"<i class='fas fa-search-plus' style='cursor: pointer'>\" as button";

        $sql = "SELECT p.ordenid,p.pelicula,DATE_FORMAT(p.fecha,'%d/%m/%Y') as fecha,p.respuesta,p.tipotarjeta,
        p.monto,p.transaccionid,p.voucher, p.sala, p.fechafuncion, p.complejo, 
        p.asientos,p.tecnologia,$button
        FROM transaccion p
        WHERE p.cedula = '".$cedula."' order by p.fecha desc";
        return $this->daoLib->prepareStament($sql,"asociativo");  
    } 


    /**
     * Detalle de trasaccion
     * @param string $idtransaccion.
     * @return cedula[]||null.
     **/
    public function getTransaccionDetalle($idtransaccion)
    {       
        $button = "\"<i class='fas fa-search-plus' style='cursor: pointer'>\" as button";

       /* $sql = "SELECT p.pelicula, p.codigoComplejo, p.sala,
                       p.fechaFuncion, p.cantidad_boletos,
                       p.asientos, p.ordenID, 
                       p.hora, p.respuesta, s.nombreComplejo,p1.versiones
                       FROM programacion_test t JOIN pelicula_test p1 RIGHT JOIN transaccion p  
                        ON (t.codigoPelicula = p1.codSinec
                        OR t.codigoPelicula = p1.codVista) AND (p.programacionID = t.programacionID)
                        LEFT JOIN complejo s ON p.codigoComplejo = s.codigoComplejo 
        WHERE p.transaccionID = '".$idtransaccion."' order by p.fecha desc";*/
        
        $sql = "SELECT p.pelicula, p.codigoComplejo, p.sala,
        p.fechaFuncion, p.cantidad_boletos,
        p.asientos, p.ordenID, 
        p.hora, p.respuesta
        FROM  transaccion p  
        WHERE p.transaccionID = '".$idtransaccion."' order by p.fecha desc";

        return $this->daoLib->prepareStament($sql,"asociativo");  
    } 

}
