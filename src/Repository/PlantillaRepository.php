<?php

namespace App\Repository;

use App\Entity\Plantilla;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Summary.
 *
 * Description: PlantillaRepository
 *
 * @since 1.0
 * @author Humberto Suarez <hsuarez@cinex.com.ve>
 * @copyright Envepro 2018
 */
/**
  * @method plantilla[]||null getPlantillas() Plantilla
  * @method plantilla[]||null getPlantillasByType($type) Plantilla por Tipos
  */ 

class PlantillaRepository extends ServiceEntityRepository
{
    
    /**
        * Constructor.
        * @param RegistryInterface $registry Interfaz.
    **/
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Plantilla::class);
    }
    
    /**
         * Plantilla
         * @return plantilla[]||null.
     **/
    public function getPlantillas()
    {

        $dql="SELECT p.nombreplantilla as nombre, p.tipomensajeid as tipomensaje, p.contenido AS contenido FROM App:Plantilla p";
        return $this->getEntityManager()->createQuery($dql)->getResult();
    }

    /**
         * Plantilla por tipo
         * @param string tipo de plantlla.
         * @return plantilla[]||null.
     **/
    public function getPlantillasByType($type)
    {
        $dql="SELECT p.nombreplantilla as nombre, p.tipomensajeid as tipomensaje, p.contenido AS contenido FROM App:Plantilla p WHERE p.tipomensajeid = ".$type;
        return $this->getEntityManager()->createQuery($dql)->getResult();
    } 
    
}
