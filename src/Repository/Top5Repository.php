<?php

namespace App\Repository;

use App\Entity\Top5;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Lib\DaoLib;
/**
    * Summary.
    * Description: Top5Repository
    * @since 1.0
    * @author Juan Blanco <jblanco@cinex.com.ve>
    * @copyright Envepro 2018
    * @method peliculas[]||null getTop5() Top5 de Peliculas
    */
class Top5Repository extends ServiceEntityRepository
{
    public $daoLib;


     /**
        * Constructor.
        * @param DaoLib $daoLib Libreria.
        * @param RegistryInterface $registry Interfaz.
    **/
    public function __construct(DaoLib $daoLib, RegistryInterface $registry)
    {
        $this->daoLib =$daoLib;
        parent::__construct($registry, Top5::class);
    }
    
     /**
     * Top5 de Peliculas
     * @return peliculas[]||null.
     **/
    public function getTop5(){
            $sql = " SELECT nombrePelicula as pelicula, 
                           urlTrailer as trailer, 
                           DATE_FORMAT(fechaEstreno, '%Y-%m-%d') as fecha_estreno, 
                           versiones, 
                           posicion,
                           codVista 
                    FROM top5 inner join pelicula_test on top5.nombrePelicula = pelicula_test.referencia
                    group by nombrePelicula order by posicion, trailer desc ";          
            return $this->daoLib->prepareStament($sql,"asociativo");
    }
    
    /**
     * Top5Data de Peliculas
     * @return peliculas[]||null.
     **/
    public function getTop5Data(){
        $sql = "SELECT nombrePelicula as pelicula,
                           urlTrailer as trailer,
                           DATE_FORMAT(fechaEstreno, '%Y-%m-%d') as fecha_estreno,
                           versiones,
                           posicion,
                           codVista,
                           descripcion,
                           director, 
                           elenco,
                           genero
                    FROM top5 inner join pelicula_test on top5.nombrePelicula = pelicula_test.referencia
                    group by nombrePelicula order by posicion, trailer desc ";
        return $this->daoLib->prepareStament($sql,"asociativo");
    }
}
