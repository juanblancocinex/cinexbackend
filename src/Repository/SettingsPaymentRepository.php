<?php

namespace App\Repository;

use App\Entity\SettingsPayment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Summary.
 *
 * Description: TransactionNumberRepository
 *
 * @since 1.0
 * @author Humberto Suarez <hsuarez@cinex.com.ve>
 * @copyright Envepro 2018
 */
/**
  * @method SettingsPayment[]||null getSettingsPayment() SettingsPayment
  */ 

class SettingsPaymentRepository extends ServiceEntityRepository
{
    
    /**
        * Constructor.
        * @param RegistryInterface $registry Interfaz.
    **/
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SettingsPayment::class);
    }
    
    /**
         * getSettingsPayment
         * @return SettingsPayments[]||null.
     **/
    public function getSettingsPayment()
    {        
        $dql="select p.settingDesc, p.settingValue FROM App:SettingsPayment p";
        return $this->getEntityManager()->createQuery($dql)->getResult();
    }
        
}
