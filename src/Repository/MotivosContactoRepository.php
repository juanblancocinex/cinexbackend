<?php

namespace App\Repository;

use App\Entity\Motivoscontacto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Summary.
 *
 * Description: MotivosContactoRepository
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 */
/**
  * @method complejos[]||null getMotivosContactos() Motivos
  */ 

class MotivosContactoRepository extends ServiceEntityRepository
{
    
    /**
        * Constructor.
        * @param RegistryInterface $registry Interfaz.
    **/
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Motivoscontacto::class);
    }
    
    /**
         * getMotivosContactos
         * @return motivos[]||null.
     **/
    public function getMotivosContactos()
    {

        $dql="SELECT p.motivoid AS motivoId,p.motivodescripcion AS motivoDescripcion,p.email as Email
        FROM App:Motivoscontacto p ORDER BY p.motivodescripcion ";
        return $this->getEntityManager()->createQuery($dql)->getResult();
    }

  
}
