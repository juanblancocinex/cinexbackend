<?php

namespace App\Repository;

use App\Entity\TokenTrans;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Summary.
 *
 * Description: TokenTransRepository
 *
 * @since 1.0
 * @author Humberto Suarez <hsuarez@cinex.com.ve>
 * @copyright Evenpro 2018
 */
/**
  * @method TokenTrans[]||null getDetallesComplejo($id) Detalles de Complejos
  */ 

class TokenTransRepository extends ServiceEntityRepository
{
    
    
    /**
        * Constructor.
        * @param RegistryInterface $registry Interfaz.
    **/
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TokenTrans::class);
    }
    
    /**
         * obtener token
         * @param string id Complejo.
         * @return complejos[]||null.
     **/



}
