<?php

namespace App\Repository;

use App\Entity\emailadmin;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Summary.
 *
 * Description: EmailAdminRepository
 *
 * @since 1.0
 * @author Humberto Suarez <hsuarez@cinex.com.ve>
 * @copyright Envepro 2018
 */
/**
  * @method emailadmin[]||null getEmailAdmin() emailadmin
  * @method emailadmin[]||null getEmailAdminByType($type) emails administrativos por Tipos
  */ 

class EmailAdminRepository extends ServiceEntityRepository
{
    
    /**
        * Constructor.
        * @param RegistryInterface $registry Interfaz.
    **/
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EmailAdmin::class);
    }
    
    /**
         * Plantilla
         * @return emailadmin[]||null.
     **/
    public function getEmailAdmin()
    {

        $dql="SELECT p.email as email, p.type as type FROM App:EmailAdmin p";
        return $this->getEntityManager()->createQuery($dql)->getResult();
    }

    /**
         * Plantilla por tipo
         * @param string tipo de plantlla.
         * @return emailadmin[]||null.
     **/
    public function getEmailAdminByType($type)
    {
        $dql="SELECT p.email as email, p.type as type FROM App:EmailAdmin p WHERE p.type = '".$type."'";
        return $this->getEntityManager()->createQuery($dql)->getResult();
    } 
    
}
