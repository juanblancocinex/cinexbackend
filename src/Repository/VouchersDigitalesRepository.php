<?php

namespace App\Repository;

use App\Entity\VouchersDigitales;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Lib\DaoLib;
/**
  * Summary.
  * Description: VouchersRepository
  *
  * @since 1.0
  * @author Sir Sarmiento <ssarmiento@cinex.com.ve>
  * @copyright Envepro 2018
  * @method vouchersDigitales[]||null getVouchersDigitales() Vouchers
  */ 

class VouchersDigitalesRepository extends ServiceEntityRepository
{
    
    /**
        * Constructor.
        * @param RegistryInterface $registry Interfaz.
    **/
    public function __construct(DaoLib $daoLib, RegistryInterface $registry)
    {
        $this->daoLib =$daoLib;
        parent::__construct($registry, VouchersDigitales::class);
    }
    
    /**
     * VouchersDigitales
     * @param string $ci cedula del cliente.
     * @param string $codtipovouchers codigo del tipo de voucher.
     * @return VouchersDigitales[]||null.
     **/
    public function getVouchersDigitales($ci, $formato){
//         $qb = $this->createQueryBuilder('p')
//             ->Select('p.codTipoVoucher')
//             ->addSelect('p.codigoVoucher')
//             ->addSelect('p.redimido')
//             ->addSelect('p.medioRedencion')
//             ->addSelect('p.fechaRedencion')
//             ->addSelect('p.activo')
//             ->addSelect('p.categoriaId')
//             ->addSelect('p.mesAsignacion')
//             ->addSelect('p.anoAsignacion')
//             ->setParameter('ci', $ci)
//             ->setParameter('codtipovoucher', $codtipovoucher)
//             ->where('p.idUsuario=:ci')
//             ->andwhere('p.codTipoVoucher=:codtipovoucher');
//         return $qb->getQuery()->getResult();
        /*$sql="SELECT * FROM vouchers_digitales, vouchers_digitales_tipo, vouchers_enviados 
        WHERE vouchers_digitales.id_usuario = '".$ci."' 
        AND vouchers_digitales_tipo.cod_tipo_voucher = vouchers_digitales.cod_tipo_voucher 
        AND vouchers_digitales_tipo.formatos_validos = '".$formato."' 
        AND vouchers_digitales.redimido != 1 AND 
        vouchers_digitales.codigo_voucher != vouchers_enviados.codigo_voucher 
        AND fecha_vencimiento >= CURDATE()";    */          
        
        $sql="SELECT * FROM vouchers_digitales A inner join 
        vouchers_digitales_tipo B on B.cod_tipo_voucher = A.cod_tipo_voucher
        where B.formatos_validos = '".$formato."'  and A.redimido != 1 AND A.codigo_voucher not in (select (vouchers_enviados.codigo_voucher) 
        from  vouchers_enviados) and A.id_usuario = '".$ci."' and A.fecha_vencimiento >= CURDATE()";


        return $this->daoLib->prepareStament($sql,"asociativo");        
    }
    
   



    /**
     * VouchersDigitales
     * @param string $ci cedula del cliente.
     * @param string $codigovouchers codigo de voucher.
     * @return VouchersDigitales[]||null.
     **/
    public function getCinexPassRedeemStatus($codvoucher){
        $qb = $this->createQueryBuilder('p')
        ->Select('p.codTipoVoucher')
        ->addSelect('p.redimido')
        ->addSelect('p.medioRedencion')
        ->addSelect('p.fechaRedencion')
        ->addSelect('p.activo')
        ->addSelect('p.categoriaId')
        ->addSelect('p.mesAsignacion')
        ->addSelect('p.anoAsignacion')
        ->setParameter('codvoucher', $codvoucher)
        ->where('p.codigoVoucher=:codvoucher');
        return $qb->getQuery()->getResult();
    }
    
    /**
     * Obtiene información del usuario dueño del cinexpass
     * @return cinexpass[]|null.
     **/
    public function getCinexPassOwner($cedula, $codvoucher)
    {
        $sql="SELECT id_usuario, nb_cliente FROM usuario, vouchers_digitales WHERE usuario.CI = '".$cedula."' AND usuario.CI = CONCAT('V', vouchers_digitales.id_usuario) AND vouchers_digitales.codigo_voucher = ".$codvoucher;
        return $this->daoLib->prepareStament($sql,"asociativo");
    }
    
    /**
     * Coloca un cinexpass como redimido.
     * @param string $codvoucher codigo de voucher.
     * @param string $channel canal de redencion.
     * @return cinexpass[]|null.
     **/
    public function setCinexPassRedeemed($codvoucher, $channel)
    {
        $sql="UPDATE vouchers_digitales SET redimido = 1 AND medio_redencion = '".$channel."' WHERE codigo_voucher = '".$codvoucher."'";
        return $this->daoLib->prepareStament($sql,"asociativo");
    }
    
}