<?php

namespace App\Repository;

use App\Entity\UsuarioAddinfo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Lib\DaoLib;
/**
    * Summary.
    * Description: UsuarioAddinfoRepository
    * @since 1.0
    * @author Ghensys Valero <gvalero@cinex.com.ve>
    * @copyright Envepro 2019
    */

class UsuarioAddinfoRepository extends ServiceEntityRepository
{
    /**
        * Constructor.
        * @param RegistryInterface $registry Interfaz.
    **/
    public function __construct(DaoLib $daoLib, RegistryInterface $registry)
    {
        $this->daoLib =$daoLib;
        parent::__construct($registry, UsuarioAddinfo::class);
    }
    
    /**
     * Consulta de datos de usuario Loyalty
     * @param string $userId ID usuario.
     * @return usuarioAddinfo[]||null.
     **/
    public function getLoyaltyDataUser($userId)
    {
        $dql="SELECT * FROM usuario_addinfo WHERE usuarioID =".$userId.";";
        return $this->daoLib->prepareStament($dql,"asociativo");
    }

    /**
     * Consulta de la info del usuario
     * @param string $usuarioId
     * @return void
     */
    public function getUserData($usuarioId)
    {
        return $this->createQueryBuilder('p')
            ->select("p")
            ->andWhere('p.usuarioid = :usuario')
            ->setParameter('usuario', $usuarioId)
            ->getQuery()
            ->getResult()
        ;
    }
}
