<?php

namespace App\Repository;

use App\Entity\ProgramacionTest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Lib\DaoLib;

/**
    * Summary.
    * Description: ProgramacionTestRepository
    * @since 1.0
    * @author Juan Blanco <jblanco@cinex.com.ve>
    * @copyright Envepro 2018
    * @method peliculas[]||null getCartelera() Cartelera de Peliculas
    * @method programacion[]||null getBuscarProgramacionSql($parametros) Programacion de Peliculas
    * @method fechas[]||null getFechasProgramacion($parametros) Fechas de Programacion de Peliculas
    * @method programacion[]|null getBuscarProgramacionComplejoSql($parametro) Programacion por Complejos
    * @method fechas[]||null getFechas($parametro) Fechas de Programación por Complejos

    */ 
class ProgramacionTestRepository extends ServiceEntityRepository
{
     /**
        * Constructor.
        * @param RegistryInterface $registry Interfaz.
    **/
    public function __construct(DaoLib $daoLib, RegistryInterface $registry)
    {
        $this->daoLib =$daoLib;
        parent::__construct($registry, ProgramacionTest::class);
    }
    
    /**
     * Cartelera de Peliculas
     * @return peliculas[]|null.
     **/
    public function getPelicula($parametros)
    {
        
        $sql= "SELECT p1.peliculaID AS PELICULAID,
                p1.nombreSinFormato AS nombre,
                p1.referencia AS referencia,
                p1.nombre AS nombrepelicula,
                p1.nombreOriginal AS nombreOriginal,
                p1.censura AS censura,
                p1.genero AS genero,
                DATE_FORMAT(p1.fechaEstreno, '%d/%m/%Y') as fechaEstreno,
                p1.urlTrailer AS urlTrailer,
                p1.director AS director,
                p1.duracion AS duracion,
                p1.esDestacada AS destacada,
                p1.esPreVenta AS esPreVenta,
                p1.esVenezolana AS esVenezolana,
                p1.elenco AS elenco
                FROM (((programacion_test p JOIN complejo c) JOIN pelicula_test p1) JOIN ciudad c1)
                WHERE ((c.codigoComplejo = p.codigoComplejo) AND ((p.codigoPelicula = p1.codSinec)
                OR (p.codigoPelicula = p1.codVista)) AND (c1.ciudadID = c.ciudadID) AND (p.fecha_programacion >= CURDATE())
                AND (p1.referencia IS NOT NULL)) and p1.nombreSinFormato not in(select nombreSinFormato from pelicula_blockeada) $parametros
                AND c.codigoComplejo != 'TST' AND c.codigoComplejo != 'PRU' GROUP BY p1.referencia
                ORDER BY p1.orden ASC, p1.fechaEstreno, p1.referencia , p1.esDestacada DESC ";
       
        return $this->daoLib->prepareStament($sql,"asociativo");
    } 
    
     /**
     * Cartelera de Peliculas
     * @return peliculas[]|null.
     **/
    public function getCartelera($mostrartodo)
    {
        
        if(!isset($mostrarTodo)){
            $notIn = "  and p1.nombreSinFormato not in(select nombreSinFormato from pelicula_blockeada) ";
        }
        
        $sql= " SELECT p1.peliculaID AS PELICULAID, 
                p1.nombreSinFormato AS nombresinformato,
                p1.referencia AS referencia, 
                p1.nombre AS nombrepelicula, 
                p1.nombreOriginal AS nombreoriginal, 
                p1.censura AS censura, 
                p1.genero AS genero, 
                DATE_FORMAT(p1.fechaEstreno, '%d/%m/%Y') as fechaestreno, 
                p1.urlTrailer AS trailer, 
                p1.director AS director, 
                p1.duracion AS duracion, 
                p1.esDestacada AS destacada, 
                p1.esPreVenta AS preVenta, 
                p1.esVenezolana AS venezolana, 
                p1.elenco AS elenco 
                FROM (((programacion_test p JOIN complejo c) JOIN pelicula_test p1) JOIN ciudad c1) 
                WHERE ((c.codigoComplejo = p.codigoComplejo) AND ((p.codigoPelicula = p1.codSinec) 
                OR (p.codigoPelicula = p1.codVista)) AND (c1.ciudadID = c.ciudadID) AND (p.fecha_programacion >= CURDATE()) 
                AND (p1.referencia IS NOT NULL)) $notIn
                AND c.codigoComplejo != 'TST' AND c.codigoComplejo != 'PRU' GROUP BY p1.referencia
                ORDER BY p1.orden ASC, p1.fechaEstreno, p1.referencia , p1.esDestacada DESC ";



        /*
        $dql="SELECT a.peliculaid AS PELICULAID,
        COALESCE(a.nombresinformato,'') AS nombresinformato,
        COALESCE(a.referencia,'') AS referencia,
        COALESCE(a.nombre,'') AS nombrepelicula,
        COALESCE(a.nombreoriginal,'') AS nombreoriginal, 
        COALESCE(a.censura,'') AS censura,
        COALESCE(a.genero,'') AS genero,
        COALESCE(dateformat(a.fechaestreno, '%d/%m/%Y'),'') as fechaestreno,
        COALESCE(a.urltrailer,'') AS trailer,
        COALESCE(a.director,'') AS director,
        COALESCE(a.duracion,'') AS duracion,
        COALESCE(a.elenco,'') AS elenco,
        COALESCE(a.orden,'') AS orden
        FROM App:ProgramacionTest p INNER JOIN p.codigopelicula a 
        INNER JOIN p.codigocomplejo b INNER JOIN b.ciudadId c
        where a.referencia is not null and p.fechaProgramacion >='".date("Y-m-d")."' $notIn
         GROUP BY a.referencia
         ORDER BY a.orden ASC, a.fechaestreno, a.referencia , a.esdestacada DESC" ;
        return $this->getEntityManager()->createQuery($dql)->getResult();*/
        return $this->daoLib->prepareStament($sql,"asociativo");
    } 
    
    /**
     * Cartelera de Peliculas
     * @return peliculas[]|null.
     **/
    public function getBillboard($mostrartodo)
    {
        
        if(!isset($mostrarTodo)){
            $notIn = "  and p1.nombreSinFormato not in(select nombreSinFormato from pelicula_blockeada) ";
        }
        
        $sql= "SELECT p1.peliculaID AS PELICULAID,
                p1.nombreSinFormato AS nombresinformato,
                p1.referencia AS referencia,
                p1.duracion AS duracion,
                DATE_FORMAT(p1.fechaEstreno, '%d/%m/%Y') as fechaestreno,
                p1.director AS director,
                p1.elenco AS elenco,
                p1.genero AS genero,
                p1.censura AS censura,
                p1.codVista AS Film_strCode,
                p1.descripcion AS Film_strContent                       
                FROM (((programacion_test p JOIN complejo c) JOIN pelicula_test p1) JOIN ciudad c1)
                WHERE ((c.codigoComplejo = p.codigoComplejo) AND ((p.codigoPelicula = p1.codSinec)
                OR (p.codigoPelicula = p1.codVista)) AND (c1.ciudadID = c.ciudadID) AND (p.fecha_programacion >= CURDATE())
                AND (p1.referencia IS NOT NULL)) $notIn
                GROUP BY p1.referencia
                ORDER BY p1.orden ASC, p1.fechaEstreno, p1.referencia , p1.esDestacada DESC";                
        
        /*
         $dql="SELECT a.peliculaid AS PELICULAID,
         COALESCE(a.nombresinformato,'') AS nombresinformato,
         COALESCE(a.referencia,'') AS referencia,
         COALESCE(a.nombre,'') AS nombrepelicula,
         COALESCE(a.nombreoriginal,'') AS nombreoriginal,
         COALESCE(a.censura,'') AS censura,
         COALESCE(a.genero,'') AS genero,
         COALESCE(dateformat(a.fechaestreno, '%d/%m/%Y'),'') as fechaestreno,
         COALESCE(a.urltrailer,'') AS trailer,
         COALESCE(a.director,'') AS director,
         COALESCE(a.duracion,'') AS duracion,
         COALESCE(a.elenco,'') AS elenco,
         COALESCE(a.orden,'') AS orden
         FROM App:ProgramacionTest p INNER JOIN p.codigopelicula a
         INNER JOIN p.codigocomplejo b INNER JOIN b.ciudadId c
         where a.referencia is not null and p.fechaProgramacion >='".date("Y-m-d")."' $notIn
         GROUP BY a.referencia
         ORDER BY a.orden ASC, a.fechaestreno, a.referencia , a.esdestacada DESC" ;
         return $this->getEntityManager()->createQuery($dql)->getResult();*/
        return $this->daoLib->prepareStament($sql,"asociativo");
    } 
    
    /**
     * Cartelera de Peliculas Index
     * @return peliculas[]|null.
     **/
    public function getCarteleraTiny()
    {
        $sql="SELECT cart_name as nombre, cart_trailer as trailer, cart_versions as versions, cart_fechaestreno as fechaestreno, 
              cart_preventa as preventa, cart_venezolana as venezolana, cart_preestreno as preestreno, cart_estreno as estreno, cart_proximamente as proximamente FROM get_tiny_billboard" ;
        return $this->daoLib->prepareStament($sql,"asociativo");
    }
    
    public function getMovieCinemas($codigoVista){
        $sql="SELECT pt.codigoComplejo FROM programacion_test pt WHERE pt.codigoPelicula = '".$codigoVista."' GROUP BY pt.codigoComplejo ORDER BY pt.codigoComplejo";
        return $this->daoLib->prepareStament($sql,"asociativo");
    }

      /**
     * Programacion de Peliculas
     * @param string $parametros parametros de consulta
     * @return programacion[]||null.
     **/
    public function getBuscarProgramacionSql($parametros){
        return " SELECT nombreCiudad, 
                    nombreComplejo, 
                    complejo.codigoComplejo,
                    complejo.direccion,
                    complejo.telefonoAtencion,
                    sala,
                    nombreSinFormato,
                    versiones,
                    pelicula_test.censura,
                    codVista,
                    referencia,
                    hora,
                    ventaActiva,
                    ventaActiva as abierta,
                    DATE_FORMAT(fecha_programacion,'%d-%m-%Y') as fecha,
                    seleccionAsientosActiva,
                    sesionID,
                    DATE_FORMAT(fecha_programacion,'%h:%i %p') as horaFormateada
                    FROM ciudad INNER JOIN complejo ON ciudad.ciudadID = complejo.ciudadID
                    INNER JOIN programacion_test ON programacion_test.codigoComplejo = complejo.codigoComplejo
                    INNER JOIN pelicula_test ON (programacion_test.codigoPelicula = pelicula_test.codVista OR programacion_test.codigoPelicula = pelicula_test.codSinec)
                    WHERE $parametros group by codVIsta, complejo.codigoComplejo,complejo.ciudadID, sala,fecha,hora
                    ORDER BY ciudad.ciudadID , nombreComplejo, codVista, sala,hora";


    }

    /**
     * Fechas de programación por peliculas
     * @param string $parametros parametros de consulta
     * @return fechas[]||null.
     **/
    public function getFechasProgramacion($parametros){
        return "    SELECT  DATE_FORMAT(fecha_programacion,'%Y-%m-%d') as fecha
                    FROM ciudad INNER JOIN complejo ON ciudad.ciudadID = complejo.ciudadID
                    INNER JOIN programacion_test ON programacion_test.codigoComplejo = complejo.codigoComplejo
                    INNER JOIN pelicula_test ON (programacion_test.codigoPelicula = pelicula_test.codVista OR programacion_test.codigoPelicula = pelicula_test.codSinec)
                    WHERE 
                    $parametros group by fecha
                    ORDER BY fecha";
    }

     /**
     * Programacion de Peliculas por Complejos
     * @param string $parametros parametros de consulta
     * @return programacion[]||null.
     **/
    public function getBuscarProgramacionComplejoSql ($parametro){

           
        return  "SELECT nombreComplejo, evenpro_cinex_domain_model_complejo.codigoComplejo,
                        sala,nombreSinFormato,versiones,
                        evenpro_cinex_domain_model_pelicula_test.censura,
                        codVista,
                        referencia,
                        duracion,
                        genero,
                        director,
                        hora,
                        ventaActiva,
                        ventaActiva as abierta,
                        DATE_FORMAT(fecha_programacion,'%Y/%m/%d') as fecha,
                        seleccionAsientosActiva,
                        sesionID,
                        DATE_FORMAT(fecha_programacion,'%h:%i %p') as horaFormateada
                        FROM
                        evenpro_cinex_domain_model_complejo 
                        INNER JOIN
                        evenpro_cinex_domain_model_programacion_test ON evenpro_cinex_domain_model_programacion_test.codigoComplejo = evenpro_cinex_domain_model_complejo.codigoComplejo
                        INNER JOIN
                        evenpro_cinex_domain_model_pelicula_test ON (evenpro_cinex_domain_model_programacion_test.codigoPelicula = evenpro_cinex_domain_model_pelicula_test.codVista 
                        OR evenpro_cinex_domain_model_programacion_test.codigoPelicula = evenpro_cinex_domain_model_pelicula_test.codSinec)
                        WHERE                                        
                        $parametro group by codVista, evenpro_cinex_domain_model_complejo.codigoComplejo, sala,fecha,hora
                        ORDER BY nombreComplejo, referencia, sala, versiones, hora, codVista";

    }

      /**
     * Fechas de Programacion de Peliculas por Complejos
     * @param string $parametros parametros de consulta
     * @return fechas[]||null.
     **/
    public function getFechas($parametro){
        return 'SELECT DATE_FORMAT(fecha_programacion,"%Y-%m-%d") as fecha FROM complejo
        INNER JOIN programacion_test ON 
        programacion_test.codigoComplejo = complejo.codigoComplejo
        INNER JOIN pelicula_test ON (programacion_test.codigoPelicula = 
        pelicula_test.codVista OR programacion_test.codigoPelicula = pelicula_test.codSinec)
        WHERE  codVIsta != "" and fecha_programacion >= CURDATE() '.$parametro.'
        group by fecha ORDER BY fecha';

    }
    
    /**
     * Fechas de Programacion de Peliculas por Complejos
     * @param string $parametros parametros de consulta
     * @return fechas[]||null.
     **/
    public function getDatesForCinemas($list_cinemas){
        return 'SELECT DATE_FORMAT(fecha_programacion,"%Y-%m-%d") as fecha, programacion_test.codigoComplejo as cinemaid FROM complejo
        INNER JOIN programacion_test ON
        programacion_test.codigoComplejo = complejo.codigoComplejo
        INNER JOIN pelicula_test ON (programacion_test.codigoPelicula =
        pelicula_test.codVista OR programacion_test.codigoPelicula = pelicula_test.codSinec)
        WHERE  codVIsta != "" and fecha_programacion >= CURDATE() '.$list_cinemas.'
        GROUP BY fecha, cinemaid ORDER BY cinemaid ASC';
        
    }
    
    /**
     * Obtiene sessionID y CodVista
     * @return MovieOccupationForCinema[]|null.
     **/
    public function getMovieOccupationInfoForCinema($cinemaId, $dateSession)
    {
        $sql="SELECT programacion_test.sesionID, programacion_test.codigoComplejo, programacion_test.fecha_programacion, pelicula_test.codVista FROM pelicula_test, programacion_test WHERE
            programacion_test.codigoComplejo = '".$cinemaId."' AND programacion_test.codigoPelicula = pelicula_test.codVista AND programacion_test.fecha_programacion LIKE '".$dateSession."%' ORDER BY sesionID";
        return $this->daoLib->prepareStament($sql,"asociativo");
    }
    
    /**
     * Obtiene sessionID y CodVista
     * @return MovieOccupationFor[]|null.
     **/
    public function getMovieOccupationInfo($movieName, $dateSession)
    {
        $sql = "SELECT programacion_test.sesionID, programacion_test.codigoComplejo, programacion_test.fecha_programacion, pelicula_test.codVista FROM pelicula_test, programacion_test WHERE 
               pelicula_test.referencia = '".$movieName."' AND programacion_test.codigoPelicula = pelicula_test.codVista AND programacion_test.fecha_programacion LIKE '".$dateSession."%' ORDER BY sesionID";
        return $this->daoLib->prepareStament($sql,"asociativo");
    }
  

    /**
     * Obtiene programacion 
     * @return MovieOccupationFor[]|null.
     **/
    public function getFuncionesProgramacion($parametros)
    {
        $sql = "SELECT distinct(p.sesionID),p.codigoPelicula,p.codigoComplejo,date_format(p.fecha_programacion,'%d-%m-%Y') as fecha_programacion ,
                    p.sala,p.hora,c.nombreComplejo,p1.nombreSinFormato,versiones,c1.ciudadID,c1.nombreCiudad,ventaActiva,seleccionAsientosActiva,
                    p.censura,p1.referencia,p.programacionID FROM complejo c
                    INNER JOIN programacion_test p ON p.codigoComplejo = c.codigoComplejo
                    INNER JOIN pelicula_test p1 ON p.codigoPelicula = p1.codVista OR p.codigoPelicula = p1.codSinec
                    INNER JOIN ciudad c1 ON c.ciudadID = c1.ciudadID
                    WHERE  codVIsta != '' $parametros  
                    group by codVIsta, c.codigoComplejo, sala,fecha_programacion,hora
                    ORDER BY fecha_programacion,nombreComplejo, referencia, sala, versiones, hora, codVista";
        return $this->daoLib->prepareStament($sql,"asociativo");
    }

    /**
     * Obtiene fechas obtenerBuscadores
     * @return fechasBuscadores[]|null.
     **/
    public function getFechaObtenerBuscadores($parametro)
    {
        $sql = 'SELECT DATE_FORMAT(fecha_programacion,"%d/%m/%Y") as itemValue, DATE_FORMAT(fecha_programacion,"%d/%m/%Y") as itemText FROM complejo
						      INNER JOIN
                                    programacion_test ON programacion_test.codigoComplejo = complejo.codigoComplejo
                                        INNER JOIN
                                          pelicula_test ON (programacion_test.codigoPelicula = pelicula_test.codVista OR programacion_test.codigoPelicula = pelicula_test.codSinec)
                                WHERE  codVIsta != "" and fecha_programacion >= CURDATE() '.$parametro.'
                                         group by itemValue ORDER BY itemValue';
        return $this->daoLib->prepareStament($sql,"asociativo");
    }
    
    /**
     * Obtiene peliculas obtenerBuscadores
     * @return peliculasBuscadores[]|null.
     **/
    public function getPeliculaObtenerBuscadores($parametro)
    {
        $sql = 'SELECT referencia as itemValue, referencia as itemText FROM complejo
						      INNER JOIN
                                    programacion_test ON programacion_test.codigoComplejo = complejo.codigoComplejo
                                        INNER JOIN
                                          pelicula_test ON (programacion_test.codigoPelicula = pelicula_test.codVista OR programacion_test.codigoPelicula = pelicula_test.codSinec)
                                WHERE  codVIsta != "" and fecha_programacion >= CURDATE() '.$parametro.'
                                         group by itemValue ORDER BY itemValue';
        return $this->daoLib->prepareStament($sql,"asociativo");
    }
    
    
    /**
     * Obtiene versiones obtenerBuscadores
     * @return versionesBuscadores[]|null.
     **/
    public function getVersionesObtenerBuscadores($parametro)
    {
        $sql = 'SELECT versiones FROM complejo
						      INNER JOIN
                                    programacion_test ON programacion_test.codigoComplejo = complejo.codigoComplejo
                                        INNER JOIN
                                          pelicula_test ON (programacion_test.codigoPelicula = pelicula_test.codVista OR programacion_test.codigoPelicula = pelicula_test.codSinec)
                                WHERE  codVIsta != "" and fecha_programacion >= CURDATE() '.$parametro.'
                                         group by versiones ORDER BY versiones';
        return $this->daoLib->prepareStament($sql,"asociativo");
    }
    
    /**
     * Obtiene ciudades obtenerBuscadores
     * @return ciudadesBuscadores[]|null.
     **/
    public function getCiudadesObtenerBuscadores()
    {
        $sql = "SELECT ciudadID as itemValue, nombreCiudad as itemText FROM cinex.ciudad order by nombreCiudad";
        return $this->daoLib->prepareStament($sql,"asociativo");
    }
    
    /**
     * Obtiene complejos obtenerBuscadores
     * @return complejosBuscadores[]|null.
     **/
    public function getComplejosObtenerBuscadores($parametro)
    {
        $sql = "SELECT codigoComplejo as itemValue, nombreComplejo as itemText
                            FROM cinex.complejo where esProductivo = 'Y'  $parametro
                            and codigoComplejo NOT IN (30,31,32,33,34,35,36,37,38,39,'CON')  order by itemText";
        return $this->daoLib->prepareStament($sql,"asociativo");
    }


   
    /**
     * Obtiene sesionID getSesionID
     * @return complejosBuscadores[]|null.
     **/
    public function getSesionID($complejoId)
    {
       /* $sql = "SELECT sesionID FROM programacion_test WHERE codigoComplejo = ".$complejoId." GROUP BY sesionID";
        return $this->daoLib->prepareStament($sql,"asociativo");*/
        $dql=" SELECT COALESCE(p.sesionid,'') as sesionid
        FROM App:ProgramacionTest p 
        WHERE p.codigocomplejo  =".$complejoId." GROUP BY p.sesionid" ;
        $data=$this->getEntityManager()->createQuery($dql)->getResult();
        if(count($data)<0){
            $data=[];
        }       
        return $data;
    }

    

}
