<?php

namespace App\Repository;



use App\Entity\Ciudades;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Summary.
 *
 * Description: CiudadRepository
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 */
/**
  * @method complejos[]||null getCiudadesByIdEstado() Ciudades
  */ 

class CiudadesRepository extends ServiceEntityRepository
{
    
    /**
        * Constructor.
        * @param RegistryInterface $registry Interfaz.
    **/
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Ciudades::class);
    }
    
    /**
         * Ciudades
         * @return ciudades[]||null.
     **/
    public function getCiudadesByIdEstado($id)
    {
        $dql="SELECT p.idCiudad AS ciudadID,p.ciudad AS nombreCiudad,x.estado as Estado
        FROM App:Ciudades p JOIN p.estado x  where x.idEstado=".$id." ORDER BY p.ciudad";
        return $this->getEntityManager()->createQuery($dql)->getResult();
    }

}
