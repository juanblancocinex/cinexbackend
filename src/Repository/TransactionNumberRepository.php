<?php

namespace App\Repository;

use App\Entity\Plantilla;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Summary.
 *
 * Description: TransactionNumberRepository
 *
 * @since 1.0
 * @author Humberto Suarez <hsuarez@cinex.com.ve>
 * @copyright Envepro 2018
 */
/**
  * @method TransactionNumber[]||null getNumber() TransactionNumber
  * @method TransactionNumber[]||null getTransaction() TransactionNumber
  */ 

class TransactionNumberRepository extends ServiceEntityRepository
{
    
    /**
        * Constructor.
        * @param RegistryInterface $registry Interfaz.
    **/
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TransactionNumber::class);
    }
    
    /**
         * getNumberAction
         * @param integer $usuarioid identificador del usuario. 
         * @return TransactionNumber[]||null.
     **/
    public function getNumberAction($usuarioid)
    {        
        $dql="SELECT p.id FROM transaction_number p WHERE p.usuarioid = ".$usuarioid." AND p.reference = '' ORDER BY p.id DESC LIMIT 1";
        return $this->getEntityManager()->createQuery($dql)->getResult();
    }

    /**
     * getTransactionNumberByIDAction
     * @param integer $id identificador del registro de numero de transaccion.
     * @return TransactionNumber[]||null.
     **/
    public function getTransactionNumberByIDAction($id)
    {
        $dql="SELECT p.id, p.reference, p.usuarioid, p.datetime FROM transaction_number p WHERE p.id = ".$id;
        return $this->getEntityManager()->createQuery($dql)->getResult();
    }       
}
