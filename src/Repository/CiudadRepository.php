<?php

namespace App\Repository;



use App\Entity\Ciudad;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Summary.
 *
 * Description: CiudadRepository
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 */
/**
  * @method complejos[]||null getCiudades() Ciudades
  * @method complejos[]||null getComplejoByCiudad($nombreCiudad) Complejos por Ciudades
  */ 

class CiudadRepository extends ServiceEntityRepository
{
    
    /**
        * Constructor.
        * @param RegistryInterface $registry Interfaz.
    **/
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Ciudad::class);
    }
    
    /**
         * Ciudades
         * @return complejos[]||null.
     **/
    public function getCiudades()
    {

        $dql="SELECT p.ciudadid AS ciudadID,p.nombreciudad AS nombreCiudad
        FROM App:Ciudad p ORDER BY p.ciudadid ";
        return $this->getEntityManager()->createQuery($dql)->getResult();
    }

    /**
         * Complejos de Cada Ciudad
         * @param string nombre de ciudad.
         * @return complejos[]||null.
     **/
    public function getComplejoByCiudad($nombreCiudad)
    {
        $dql="SELECT a.nombrecomplejo AS nombreComplejo,
        a.complejoid AS codigoComplejo,a.direccion,a.codigocomplejo AS abreviatura 
        FROM App:Ciudad p INNER JOIN p.complejos a 
        WHERE p.nombreciudad = '".$nombreCiudad."' AND a.esproductivo = 'Y'" ;
        return $this->getEntityManager()->createQuery($dql)->getResult();
    } 
    
}
