<?php

namespace App\Repository;

use App\Entity\Complejo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Summary.
 *
 * Description: ComplejoRepository
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 */
/**
  * @method complejos[]||null getDetallesComplejo($id) Detalles de Complejos
  * @method complejos[]||null getComplejos() Complejos
  */ 

class ComplejoRepository extends ServiceEntityRepository
{
    
    
    /**
        * Constructor.
        * @param RegistryInterface $registry Interfaz.
    **/
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Complejo::class);
    }
    
    /**
         * Detalles de Complejos
         * @param string id Complejo.
         * @return complejos[]||null.
     **/
    public function getDetallesComplejo($id){
              
        $qb = $this->createQueryBuilder('p')
            ->leftJoin('p.ciudadId', 'a')
            ->Select("COALESCE(p.codigocomplejo,'') as id")
            ->addSelect("COALESCE(p.nombrecomplejo,'') as nombre")
            ->addSelect("COALESCE(p.direccion,'') as direccion ")
            ->addSelect("COALESCE(p.placeId,'') as place_id")
            ->addSelect("COALESCE(p.urlimagen1,'') as urlImagen1")
            ->addSelect("COALESCE(p.urlimagen2,'')as urlImagen2")
            ->addSelect("COALESCE(p.urlimagen3,'') as urlImagen3")
            ->addSelect("COALESCE(p.urlimagen4,'') as urlImagen4")
            ->addSelect("COALESCE(p.longitud,'') as longitud")
            ->addSelect("COALESCE(p.latitud,'')as latitud")
            ->addSelect("COALESCE(p.servicios,'') as servicios")
            ->setParameter('codigocomplejo', $id)
            ->where('p.codigocomplejo=:codigocomplejo');
        return $qb->getQuery()->getResult();
    }    

    /**
         * Complejos
         * @return complejos[]||null.
     **/
    public function getComplejos(){
              
        $qb = $this->createQueryBuilder('p')
            ->leftJoin('p.ciudadId', 'a')
            ->Select("COALESCE(a.ciudadid,'') as ciudadid")
            ->addSelect("COALESCE(a.nombreciudad,'') as nombreCiudad")
            ->addSelect("COALESCE(p.codigocomplejo,'') as id")
            ->addSelect("COALESCE(p.nombrecomplejo,'') as nombre")
            ->addSelect("COALESCE(p.direccion,'') as direccion ")
            ->addSelect("COALESCE(p.placeId,'') as place_id")
            ->addSelect("COALESCE(p.servicios,'') as servicios")
            ->addSelect("COALESCE(p.urlimagen1,'') as urlImagen1")
            ->addSelect("COALESCE(p.urlimagen2,'')as urlImagen2")
            ->addSelect("COALESCE(p.urlimagen3,'') as urlImagen3")
            ->addSelect("COALESCE(p.urlimagen4,'') as urlImagen4")
            ->addSelect("COALESCE(p.urlimagen5,'') as urlImagen5")
            ->addSelect("COALESCE(p.urlimagen6,'') as urlImagen6")
            ->addSelect("COALESCE(p.urlimagen7,'') as urlImagen7")
            ->addSelect("COALESCE(p.urlimagen8,'') as urlImagen8")
            ->addSelect("COALESCE(p.longitud,'') as longitud")
            ->addSelect("COALESCE(p.latitud,'')as latitud")
            ->addSelect("COALESCE(p.esproductivo,'')as esproductivo")
            ->addSelect("COALESCE(p.descripcionboletos,'')as descripcionBoletos");
        return $qb->getQuery()->getResult();
    }    



}
