<?php

namespace App\Repository;

use App\Entity\PeliculaTest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Lib\DaoLib;
/**
    * Summary.
    * Description: BuzonRepository
    * @since 1.0
    * @author Juan Blanco <jblanco@cinex.com.ve>
    * @copyright Envepro 2018
    * @method emails[]||null getBuzon($usuarioid)
*/ 
class BuzonRepository extends ServiceEntityRepository
{
        
    /**
        * Constructor.
        * @param DaoLib $daoLib Libreria.
        * @param RegistryInterface $registry Interfaz.
    **/
    public function __construct(DaoLib $daoLib,RegistryInterface $registry)
    {
        $this->daoLib =$daoLib;
        parent::__construct($registry, PeliculaTest::class);
    }
    
    /**
     * Lista de email
     * @param string $referencia email de usuario.
     * @return emails[]||null.
     **/
    public function getBuzon($email)
    {       
        $sql = "SELECT count(b.usuarioid) as totalemailsinleer
        FROM usuario a  inner join buzon b on
        a.usuarioid = b.usuarioid
        WHERE a.correoelectronico = '".$email."' and b.lectura=0";
        return $this->daoLib->prepareStament($sql,"asociativo");   
    } 
    
    /**
     * Lista de correos
     * @param string $referencia email de usuario.
     * @return emails[]||null.
     **/
    public function getMisCorreos($parameter)
    {       
        $where =" WHERE a.correoelectronico = '".$parameter["email"]."' ";
        if(!empty($parameter["search"]["value"])){
           $where .= " and b.asunto like '%".$parameter["search"]["value"]."%' || b.contenido like '%".$parameter["search"]["value"]."%' || b.fechasistema like '%".$parameter["search"]["value"]."%'" ;
        }
        $sql = "SELECT 'EQUIPO CINEX' as DE, b.asunto as ASUNTO,b.contenido as MENSAJE,b.fechasistema AS FECHA
        FROM usuario a  inner join buzon b on
        a.usuarioid = b.usuarioid
        $where order by b.lectura";
        return $this->daoLib->prepareStament($sql,"asociativo");   
    } 

}
