<?php

namespace App\Repository;

use App\Entity\Usuario;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Lib\DaoLib;

/**
    * Summary.
    * Description: UsuarioRepository
    * @since 1.0
    * @author Juan Blanco <jblanco@cinex.com.ve>
    * @copyright Envepro 2018
    * @method usuarios[]||null getUsuario() Usuarios del Sistema
    * @method usuario[]||null getUsuarioFindByCorreoClave() Consulta de Usuario  por Correo y Clave
    */
class UsuarioRepository extends ServiceEntityRepository
{
    /**
        * Constructor.
        * @param RegistryInterface $registry Interfaz.
    **/
    public function __construct(DaoLib $daoLib, RegistryInterface $registry)
    {
        $this->daoLib =$daoLib;
        parent::__construct($registry, Usuario::class);
    }
    

    /**
     * Usuarios del Sistema
     * @return usuarios[]||null.
     **/
    public function getUsuario()
    {
        return $this->createQueryBuilder('p')
            ->getQuery()
            ->getResult()
        ;
    }


     /**
         * Consulta de Usuario por Correo y Clave
         * @param string $parametros parametros de consulta
         * @return usuario[]||null.
     **/
    public function getUsuarioFindByCorreoClave($parametros)
    {  
        return $this->createQueryBuilder('p')
            ->select("p.usuarioid")
            ->andWhere('p.correoelectronico = :usuario')
            ->andWhere('p.clave = :clave')
            ->setParameter('usuario', $parametros["correoelectronico"])
            ->setParameter('clave', $parametros["clave"])
            ->getQuery()
            ->getResult()
        ;
    }

     /**
         * Consulta de Usuario por Id
         * @param string $parametros parametros de consulta
         * @return usuario[]||null.
     **/
    public function getUsuarioFindByUsuarioid($usuarioid)
    {  
        return $this->createQueryBuilder('p')
            ->select("p")
            ->andWhere('p.usuarioid = :usuario')
            ->setParameter('usuario', $usuarioid)
            ->getQuery()
            ->getResult()
        ;
    }

 
    /**
         * Consulta Informacion de Usuario por Email
         * @param string $parametros parametros de consulta
         * @return usuario[]||null.
     **/
    public function getUsuarioInfoFindByEmail($email)
    {  
        return $this->createQueryBuilder('p')
            ->select("p")
            ->andWhere('p.correoelectronico = :email')
            ->setParameter('email', $email)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
         * Consulta Informacion de Usuario por cedula
         * @param string $parametros parametros de consulta
         * @return usuario[]||null.
     **/
    public function getUsuarioInfoFindByCI($ci)
    {  
        return $this->createQueryBuilder('p')
            ->select("p")
            ->andWhere('p.ci = :ci')
            ->setParameter('ci', $ci)
            ->getQuery()
            ->getResult()
        ;
    }


    /**
     * Consulta de usersessionid por Correo
     * @param string $email email del usuario.
     * @return usuario[]||null.
     **/
    public function getUserVistaIdFindByCorreo($email)
    {
        return $this->createQueryBuilder('p')
        ->select("p.uservistaid")
        ->andWhere('p.correoelectronico = :usuario')
        ->setParameter('usuario', $email)
        ->getQuery()
        ->getResult()
        ;
    }

    /**
     * Consulta de Usuario por Correo y Cedula
     * @param string $email email del usuario.
     * @param string $ci del usuario.
     * @return usuario[]||null.
     **/
    public function getUserFindByCorreoCI($email, $ci)
    {
        return $this->createQueryBuilder('p')
        ->select("p")
        ->andWhere('p.correoelectronico = :email')
        ->andWhere('p.ci = :ci')
        ->setParameter('email', $email)
        ->setParameter('ci', $ci)
        ->getQuery()
        ->getResult()
        ;
    }

    /**
     * Consulta de la nueva Contraseña del Usuario - NeosCMS
     * @param string $email email del usuario.
     * @return usuario[]||null.
     **/
    public function getNewPwdUserNeos($email)
    {
        return $this->createQueryBuilder('p')
        ->select("p.password")
        ->andWhere('p.correoelectronico = :email')
        ->setParameter('email', $email)
        ->getQuery()
        ->getResult()
        ;
    }

    /**
     * Consulta de información del Usuario by id
     * @param string $userId ID usuario.
     * @return userInfo[]||null.
     **/
    public function getUserDataById($userId)
    {
        $dql="SELECT * FROM usuario WHERE usuarioID = ".$userId.";";
        //var_dump($dql);die;
        return $this->daoLib->prepareStament($dql,"asociativo");
    }
}
