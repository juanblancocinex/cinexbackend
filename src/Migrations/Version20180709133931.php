<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180709133931 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE partidos CHANGE resultado1 resultado1 INT UNSIGNED NOT NULL, CHANGE resultado2 resultado2 INT UNSIGNED NOT NULL, CHANGE usuario_registro usuario_registro INT UNSIGNED NOT NULL, CHANGE usuario_actualizacion usuario_actualizacion INT UNSIGNED NOT NULL');
        $this->addSql('ALTER TABLE localizador CHANGE localizador localizador CHAR(10) NOT NULL');
        $this->addSql('ALTER TABLE top5_tiny CHANGE top5t_position top5t_position INT DEFAULT NULL');
        $this->addSql('ALTER TABLE usuario_avatar CHANGE usuarioID usuarioID INT AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE pagos_imagenes CHANGE imagen_orden imagen_orden INT DEFAULT NULL');
        $this->addSql('ALTER TABLE partido CHANGE equipo1_id equipo1_id INT DEFAULT NULL, CHANGE equipo2_id equipo2_id INT DEFAULT NULL, CHANGE estatus estatus TINYINT(1) NOT NULL COMMENT \'0 por efectuar, 1 terminado, 2 aplicado\'');
        $this->addSql('ALTER TABLE actionlog CHANGE user_id user_id BIGINT NOT NULL');
        $this->addSql('ALTER TABLE usuario_recordarme CHANGE usuarioID usuarioID INT AUTO_INCREMENT NOT NULL, CHANGE recordarme recordarme TINYINT(1) NOT NULL, CHANGE email email VARCHAR(50) NOT NULL, CHANGE pwd pwd VARCHAR(50) NOT NULL');
        $this->addSql('ALTER TABLE pelicula_detalles CHANGE peliculaID peliculaID INT AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE imagenes_especiales_bg CHANGE bg_color bg_color VARCHAR(255) NOT NULL, CHANGE bg_blanconegro bg_blanconegro VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE tipo_imagen CHANGE id_tipo id_tipo INT UNSIGNED DEFAULT NULL, CHANGE id_imagen id_imagen INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE promociones CHANGE promo_orden promo_orden INT NOT NULL');
        $this->addSql('ALTER TABLE vitrinas_home CHANGE vitrina_orden vitrina_orden INT DEFAULT NULL');
        $this->addSql('ALTER TABLE voucher_beneficio CHANGE id_tipo id_tipo INT UNSIGNED DEFAULT NULL, CHANGE id_beneficio id_beneficio INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE usuarios_cupon_cheff CHANGE email email INT DEFAULT NULL');
        $this->addSql('ALTER TABLE canal CHANGE canalID canalID INT UNSIGNED AUTO_INCREMENT NOT NULL COMMENT \'identificador de la ciudad\'');
        $this->addSql('ALTER TABLE usuarios_guaco_taq CHANGE emailLista emailLista INT NOT NULL');
        $this->addSql('ALTER TABLE usuarios_cinexclusivo CHANGE emailLista emailLista INT NOT NULL');
        $this->addSql('ALTER TABLE vouchers_digitales CHANGE idvouchers_digitales idvouchers_digitales INT NOT NULL COMMENT \'id de la tabla\'');
        $this->addSql('ALTER TABLE pelicula_versiones CHANGE nombreSinFormato nombreSinFormato VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE usuarios_prueba CHANGE email email INT DEFAULT NULL');
        $this->addSql('ALTER TABLE banners CHANGE bann_orden bann_orden VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE usuarios_guaco_web CHANGE emailLista emailLista INT NOT NULL');
        $this->addSql('ALTER TABLE usuario_fondo CHANGE usuarioID usuarioID INT AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE top5_settings CHANGE automatico automatico TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE ciudades CHANGE capital capital TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE sesiones_especiales CHANGE id id INT NOT NULL');
        $this->addSql('ALTER TABLE programacion_clone MODIFY programacionID INT UNSIGNED NOT NULL COMMENT \'clave artificial de la sesion\'');
        $this->addSql('ALTER TABLE programacion_clone DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE programacion_clone CHANGE programacionID programacionID INT UNSIGNED NOT NULL COMMENT \'clave artificial de la sesion\'');
        $this->addSql('ALTER TABLE programacion_clone ADD PRIMARY KEY (fecha_programacion, programacionID)');
        $this->addSql('ALTER TABLE buzon CHANGE lectura lectura TINYINT(1) DEFAULT NULL, CHANGE oculto oculto TINYINT(1) DEFAULT NULL, CHANGE borrado borrado TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE usuarios_csi_4dx CHANGE email email INT DEFAULT NULL');
        $this->addSql('ALTER TABLE imagen_items_concesiones CHANGE con_strid con_strid VARCHAR(10) NOT NULL');
        $this->addSql('ALTER TABLE concursoDoblete CHANGE id id BIGINT UNSIGNED NOT NULL');
        $this->addSql('ALTER TABLE parroquias DROP FOREIGN KEY parroquias_ibfk_1');
        $this->addSql('ALTER TABLE parroquias CHANGE id_municipio id_municipio INT DEFAULT NULL');
        $this->addSql('ALTER TABLE parroquias ADD CONSTRAINT FK_584481777EAD49C7 FOREIGN KEY (id_municipio) REFERENCES municipios (id_municipio)');
        $this->addSql('ALTER TABLE equipos CHANGE grupo grupo INT UNSIGNED NOT NULL, CHANGE posicion posicion INT UNSIGNED NOT NULL, CHANGE usuario_registro usuario_registro INT UNSIGNED NOT NULL, CHANGE usuario_actualizacion usuario_actualizacion INT UNSIGNED NOT NULL');
        $this->addSql('ALTER TABLE cartelera_tiny CHANGE cart_position cart_position INT DEFAULT NULL');
        $this->addSql('DROP INDEX id ON usuarios_movil');
        $this->addSql('ALTER TABLE usuarios_movil CHANGE emailListaMovil emailListaMovil TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE usuarios_corp CHANGE emailLista emailLista INT NOT NULL');
        $this->addSql('ALTER TABLE pelicula_blockeada CHANGE codvista codvista VARCHAR(255) NOT NULL, CHANGE codsinec codsinec VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE session_data CHANGE session_id session_id VARCHAR(32) NOT NULL, CHANGE hash hash VARCHAR(32) NOT NULL, CHANGE session_expire session_expire INT NOT NULL');
        $this->addSql('ALTER TABLE imagenes_cinexclusivo CHANGE img_orden img_orden INT DEFAULT NULL');
        $this->addSql('ALTER TABLE beneficio CHANGE id_imagen id_imagen INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE promocion CHANGE idpromocion idpromocion INT AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE usuario_addinfo CHANGE estatus estatus INT DEFAULT NULL COMMENT \'0=neutro  1=insert  2=update\'');
        $this->addSql('ALTER TABLE cms_submenus CHANGE submenu_orden submenu_orden INT NOT NULL');
        $this->addSql('ALTER TABLE voucher_50_sombras CHANGE voucher voucher VARCHAR(16) NOT NULL');
        $this->addSql('ALTER TABLE grupos CHANGE usuario_registro usuario_registro INT UNSIGNED NOT NULL, CHANGE usuario_actualizacion usuario_actualizacion INT UNSIGNED NOT NULL');
        $this->addSql('ALTER TABLE usuarios_festival CHANGE emailLista emailLista INT NOT NULL');
        $this->addSql('ALTER TABLE imagenes_especiales CHANGE img_orden img_orden INT NOT NULL');
        $this->addSql('ALTER TABLE usuarios_csi_vip CHANGE email email INT DEFAULT NULL');
        $this->addSql('ALTER TABLE lote CHANGE id_voucher id_voucher INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE regquiniela CHANGE idparticipante idparticipante INT AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE usuarios_cumbre_exito CHANGE emailLista emailLista INT NOT NULL');
        $this->addSql('ALTER TABLE usuario_partido CHANGE estatus estatus TINYINT(1) NOT NULL COMMENT \'0,1\', CHANGE puntaje puntaje TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE concursoNinja CHANGE id id INT NOT NULL');
        $this->addSql('ALTER TABLE ciudad CHANGE ciudadID ciudadID INT UNSIGNED AUTO_INCREMENT NOT NULL COMMENT \'identificador de la ciudad\'');
        $this->addSql('ALTER TABLE usuarios_batma_vs_superman_35mm CHANGE emailLista emailLista INT NOT NULL');
        $this->addSql('ALTER TABLE concursoConde CHANGE id id INT NOT NULL');
        $this->addSql('ALTER TABLE tipo CHANGE sell_one sell_one INT NOT NULL, CHANGE comentario comentario VARCHAR(1000) NOT NULL');
        $this->addSql('ALTER TABLE vouchers_digitales_tipo CHANGE cod_tipo_voucher cod_tipo_voucher INT AUTO_INCREMENT NOT NULL COMMENT \'codigo de tipo de vouchers\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE actionlog CHANGE user_id user_id BIGINT DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE banners CHANGE bann_orden bann_orden VARCHAR(255) DEFAULT \'0\' COLLATE latin1_swedish_ci');
        $this->addSql('ALTER TABLE beneficio CHANGE id_imagen id_imagen INT UNSIGNED NOT NULL');
        $this->addSql('ALTER TABLE buzon CHANGE lectura lectura TINYINT(1) DEFAULT \'0\', CHANGE oculto oculto TINYINT(1) DEFAULT \'0\', CHANGE borrado borrado TINYINT(1) DEFAULT \'0\'');
        $this->addSql('ALTER TABLE canal CHANGE canalID canalID INT UNSIGNED NOT NULL COMMENT \'identificador de la ciudad\'');
        $this->addSql('ALTER TABLE cartelera_tiny CHANGE cart_position cart_position INT DEFAULT 0');
        $this->addSql('ALTER TABLE ciudad CHANGE ciudadID ciudadID INT UNSIGNED NOT NULL COMMENT \'identificador de la ciudad\'');
        $this->addSql('ALTER TABLE ciudades CHANGE capital capital TINYINT(1) DEFAULT \'0\' NOT NULL');
        $this->addSql('ALTER TABLE cms_submenus CHANGE submenu_orden submenu_orden INT DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE concursoConde CHANGE id id INT AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE concursoDoblete CHANGE id id BIGINT UNSIGNED AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE concursoNinja CHANGE id id INT AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE equipos CHANGE grupo grupo INT UNSIGNED DEFAULT 0 NOT NULL, CHANGE posicion posicion INT UNSIGNED DEFAULT 0 NOT NULL, CHANGE usuario_registro usuario_registro INT UNSIGNED DEFAULT 0 NOT NULL, CHANGE usuario_actualizacion usuario_actualizacion INT UNSIGNED DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE grupos CHANGE usuario_registro usuario_registro INT UNSIGNED DEFAULT 0 NOT NULL, CHANGE usuario_actualizacion usuario_actualizacion INT UNSIGNED DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE imagen_items_concesiones CHANGE con_strid con_strid VARCHAR(10) DEFAULT \'0\' NOT NULL COLLATE latin1_swedish_ci');
        $this->addSql('ALTER TABLE imagenes_cinexclusivo CHANGE img_orden img_orden INT DEFAULT 0');
        $this->addSql('ALTER TABLE imagenes_especiales CHANGE img_orden img_orden INT DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE imagenes_especiales_bg CHANGE bg_color bg_color VARCHAR(255) DEFAULT \'0\' NOT NULL COLLATE latin1_swedish_ci, CHANGE bg_blanconegro bg_blanconegro VARCHAR(255) DEFAULT \'0\' NOT NULL COLLATE latin1_swedish_ci');
        $this->addSql('ALTER TABLE localizador CHANGE localizador localizador CHAR(10) NOT NULL COLLATE utf8_general_ci');
        $this->addSql('ALTER TABLE lote CHANGE id_voucher id_voucher INT UNSIGNED NOT NULL');
        $this->addSql('ALTER TABLE pagos_imagenes CHANGE imagen_orden imagen_orden INT DEFAULT 0');
        $this->addSql('ALTER TABLE parroquias DROP FOREIGN KEY FK_584481777EAD49C7');
        $this->addSql('ALTER TABLE parroquias CHANGE id_municipio id_municipio INT NOT NULL');
        $this->addSql('ALTER TABLE parroquias ADD CONSTRAINT parroquias_ibfk_1 FOREIGN KEY (id_municipio) REFERENCES municipios (id_municipio) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE partido CHANGE equipo1_id equipo1_id INT NOT NULL, CHANGE equipo2_id equipo2_id INT NOT NULL, CHANGE estatus estatus TINYINT(1) DEFAULT \'0\' NOT NULL COMMENT \'0 por efectuar, 1 terminado, 2 aplicado\'');
        $this->addSql('ALTER TABLE partidos CHANGE resultado1 resultado1 INT UNSIGNED DEFAULT 0 NOT NULL, CHANGE resultado2 resultado2 INT UNSIGNED DEFAULT 0 NOT NULL, CHANGE usuario_registro usuario_registro INT UNSIGNED DEFAULT 0 NOT NULL, CHANGE usuario_actualizacion usuario_actualizacion INT UNSIGNED DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE pelicula_blockeada CHANGE codvista codvista VARCHAR(255) DEFAULT \'0\' NOT NULL COLLATE latin1_swedish_ci, CHANGE codsinec codsinec VARCHAR(255) DEFAULT \'0\' NOT NULL COLLATE latin1_swedish_ci');
        $this->addSql('ALTER TABLE pelicula_detalles CHANGE peliculaID peliculaID INT NOT NULL');
        $this->addSql('ALTER TABLE pelicula_versiones CHANGE nombreSinFormato nombreSinFormato VARCHAR(255) NOT NULL COLLATE latin1_swedish_ci');
        $this->addSql('ALTER TABLE programacion_clone DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE programacion_clone CHANGE programacionID programacionID INT UNSIGNED AUTO_INCREMENT NOT NULL COMMENT \'clave artificial de la sesion\'');
        $this->addSql('ALTER TABLE programacion_clone ADD PRIMARY KEY (programacionID, fecha_programacion)');
        $this->addSql('ALTER TABLE promocion CHANGE idpromocion idpromocion INT NOT NULL');
        $this->addSql('ALTER TABLE promociones CHANGE promo_orden promo_orden INT DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE regquiniela CHANGE idparticipante idparticipante INT NOT NULL');
        $this->addSql('ALTER TABLE sesiones_especiales CHANGE id id INT AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE session_data CHANGE session_id session_id VARCHAR(32) DEFAULT \'\' NOT NULL COLLATE utf8_general_ci, CHANGE hash hash VARCHAR(32) DEFAULT \'\' NOT NULL COLLATE utf8_general_ci, CHANGE session_expire session_expire INT DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE tipo CHANGE sell_one sell_one INT DEFAULT 0 NOT NULL, CHANGE comentario comentario VARCHAR(1000) DEFAULT \'\' NOT NULL COLLATE utf8_general_ci');
        $this->addSql('ALTER TABLE tipo_imagen CHANGE id_imagen id_imagen INT UNSIGNED NOT NULL, CHANGE id_tipo id_tipo INT UNSIGNED NOT NULL');
        $this->addSql('ALTER TABLE top5_settings CHANGE automatico automatico TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE top5_tiny CHANGE top5t_position top5t_position INT DEFAULT 0');
        $this->addSql('ALTER TABLE usuario_addinfo CHANGE estatus estatus INT DEFAULT 0 COMMENT \'0=neutro  1=insert  2=update\'');
        $this->addSql('ALTER TABLE usuario_avatar CHANGE usuarioID usuarioID INT NOT NULL');
        $this->addSql('ALTER TABLE usuario_fondo CHANGE usuarioID usuarioID INT NOT NULL');
        $this->addSql('ALTER TABLE usuario_partido CHANGE estatus estatus TINYINT(1) DEFAULT \'0\' NOT NULL COMMENT \'0,1\', CHANGE puntaje puntaje TINYINT(1) DEFAULT \'0\' NOT NULL');
        $this->addSql('ALTER TABLE usuario_recordarme CHANGE usuarioID usuarioID INT NOT NULL, CHANGE recordarme recordarme TINYINT(1) DEFAULT \'0\' NOT NULL, CHANGE email email VARCHAR(50) DEFAULT \'0\' NOT NULL COLLATE latin1_swedish_ci, CHANGE pwd pwd VARCHAR(50) DEFAULT \'0\' NOT NULL COLLATE latin1_swedish_ci');
        $this->addSql('ALTER TABLE usuarios_batma_vs_superman_35mm CHANGE emailLista emailLista INT DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE usuarios_cinexclusivo CHANGE emailLista emailLista INT DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE usuarios_corp CHANGE emailLista emailLista INT DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE usuarios_csi_4dx CHANGE email email INT DEFAULT 0');
        $this->addSql('ALTER TABLE usuarios_csi_vip CHANGE email email INT DEFAULT 0');
        $this->addSql('ALTER TABLE usuarios_cumbre_exito CHANGE emailLista emailLista INT DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE usuarios_cupon_cheff CHANGE email email INT DEFAULT 0');
        $this->addSql('ALTER TABLE usuarios_festival CHANGE emailLista emailLista INT DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE usuarios_guaco_taq CHANGE emailLista emailLista INT DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE usuarios_guaco_web CHANGE emailLista emailLista INT DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE usuarios_movil CHANGE emailListaMovil emailListaMovil TINYINT(1) DEFAULT \'0\'');
        $this->addSql('CREATE UNIQUE INDEX id ON usuarios_movil (id)');
        $this->addSql('ALTER TABLE usuarios_prueba CHANGE email email INT DEFAULT 0');
        $this->addSql('ALTER TABLE vitrinas_home CHANGE vitrina_orden vitrina_orden INT DEFAULT 0');
        $this->addSql('ALTER TABLE voucher_50_sombras CHANGE voucher voucher VARCHAR(16) NOT NULL COLLATE latin1_swedish_ci');
        $this->addSql('ALTER TABLE voucher_beneficio CHANGE id_beneficio id_beneficio INT UNSIGNED NOT NULL, CHANGE id_tipo id_tipo INT UNSIGNED NOT NULL');
        $this->addSql('ALTER TABLE vouchers_digitales CHANGE idvouchers_digitales idvouchers_digitales INT AUTO_INCREMENT NOT NULL COMMENT \'id de la tabla\'');
        $this->addSql('ALTER TABLE vouchers_digitales_tipo CHANGE cod_tipo_voucher cod_tipo_voucher INT NOT NULL COMMENT \'codigo de tipo de vouchers\'');
    }
}
