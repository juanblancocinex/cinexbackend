<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
  *  Summary.
 *
 * Description: TipoMensaje Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="tipo_mensaje")
 * @ORM\Entity
 */
class TipoMensaje
{
    /**
     * @var int
     *
     * @ORM\Column(name="tipomensajeid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $tipomensajeid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tiponombre", type="string", length=255, nullable=true)
     */
    private $tiponombre;

    public function getTipomensajeid()
    {
        return $this->tipomensajeid;
    }

    public function getTiponombre()
    {
        return $this->tiponombre;
    }

    public function setTiponombre(string $tiponombre)
    {
        $this->tiponombre = $tiponombre;

        return $this;
    }


}
