<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
*  Summary.
 *
 * Description: TransaccionBackup Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 *
 * @ORM\Table(name="transaccion_backup", indexes={@ORM\Index(name="cedula", columns={"cedula"}), @ORM\Index(name="complejo", columns={"complejo"}), @ORM\Index(name="localizador", columns={"ordenID"}), @ORM\Index(name="fecha", columns={"fecha"}), @ORM\Index(name="tarjeta", columns={"tarjeta"})})
 * @ORM\Entity
 */
class TransaccionBackup
{
    /**
     * @var int
     *
     * @ORM\Column(name="transaccionID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $transaccionid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ordenID", type="string", length=15, nullable=true)
     */
    private $ordenid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombreEnTarjeta", type="string", length=100, nullable=true)
     */
    private $nombreentarjeta;

    /**
     * @var string|null
     *
     * @ORM\Column(name="celular", type="string", length=20, nullable=true)
     */
    private $celular;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=true)
     */
    private $fecha;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tarjeta", type="string", length=16, nullable=true)
     */
    private $tarjeta;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tipoTarjeta", type="string", length=20, nullable=true)
     */
    private $tipotarjeta;

    /**
     * @var string|null
     *
     * @ORM\Column(name="monto", type="string", length=20, nullable=true)
     */
    private $monto;

    /**
     * @var string|null
     *
     * @ORM\Column(name="asientos", type="string", length=100, nullable=true)
     */
    private $asientos;

    /**
     * @var int|null
     *
     * @ORM\Column(name="programacionID", type="integer", nullable=true)
     */
    private $programacionid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="respuesta", type="string", length=20, nullable=true)
     */
    private $respuesta;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cedula", type="string", length=10, nullable=true)
     */
    private $cedula;

    /**
     * @var string|null
     *
     * @ORM\Column(name="pelicula", type="string", length=100, nullable=true)
     */
    private $pelicula;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hora", type="string", length=100, nullable=true)
     */
    private $hora;

    /**
     * @var string|null
     *
     * @ORM\Column(name="complejo", type="string", length=100, nullable=true)
     */
    private $complejo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="mensaje", type="string", length=500, nullable=true)
     */
    private $mensaje;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sala", type="string", length=20, nullable=true)
     */
    private $sala;

    /**
     * @var int|null
     *
     * @ORM\Column(name="cantidad_boletos", type="integer", nullable=true)
     */
    private $cantidadBoletos;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaFuncion", type="datetime", nullable=true)
     */
    private $fechafuncion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tecnologia", type="string", length=5, nullable=true)
     */
    private $tecnologia;

    /**
     * @var string|null
     *
     * @ORM\Column(name="correo", type="string", length=50, nullable=true)
     */
    private $correo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vencashVoucher", type="string", length=45, nullable=true)
     */
    private $vencashvoucher;

    /**
     * @var string|null
     *
     * @ORM\Column(name="bookingNumber", type="string", length=45, nullable=true)
     */
    private $bookingnumber;

    /**
     * @var string|null
     *
     * @ORM\Column(name="detalleBoleto", type="string", length=500, nullable=true)
     */
    private $detalleboleto;

    /**
     * @var int|null
     *
     * @ORM\Column(name="vistaMontoInCent", type="integer", nullable=true)
     */
    private $vistamontoincent;

    /**
     * @var string|null
     *
     * @ORM\Column(name="porReversar", type="string", length=1, nullable=true)
     */
    private $porreversar;

    /**
     * @var string|null
     *
     * @ORM\Column(name="causa", type="string", length=45, nullable=true)
     */
    private $causa;

    public function getTransaccionid()
    {
        return $this->transaccionid;
    }

    public function getOrdenid()
    {
        return $this->ordenid;
    }

    public function setOrdenid(string $ordenid)
    {
        $this->ordenid = $ordenid;

        return $this;
    }

    public function getNombreentarjeta()
    {
        return $this->nombreentarjeta;
    }

    public function setNombreentarjeta(string $nombreentarjeta)
    {
        $this->nombreentarjeta = $nombreentarjeta;

        return $this;
    }

    public function getCelular()
    {
        return $this->celular;
    }

    public function setCelular(string $celular)
    {
        $this->celular = $celular;

        return $this;
    }

    public function getFecha()
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getTarjeta()
    {
        return $this->tarjeta;
    }

    public function setTarjeta(string $tarjeta)
    {
        $this->tarjeta = $tarjeta;

        return $this;
    }

    public function getTipotarjeta()
    {
        return $this->tipotarjeta;
    }

    public function setTipotarjeta(string $tipotarjeta)
    {
        $this->tipotarjeta = $tipotarjeta;

        return $this;
    }

    public function getMonto()
    {
        return $this->monto;
    }

    public function setMonto(string $monto)
    {
        $this->monto = $monto;

        return $this;
    }

    public function getAsientos()
    {
        return $this->asientos;
    }

    public function setAsientos(string $asientos)
    {
        $this->asientos = $asientos;

        return $this;
    }

    public function getProgramacionid()
    {
        return $this->programacionid;
    }

    public function setProgramacionid(int $programacionid)
    {
        $this->programacionid = $programacionid;

        return $this;
    }

    public function getRespuesta()
    {
        return $this->respuesta;
    }

    public function setRespuesta(string $respuesta)
    {
        $this->respuesta = $respuesta;

        return $this;
    }

    public function getCedula()
    {
        return $this->cedula;
    }

    public function setCedula(string $cedula)
    {
        $this->cedula = $cedula;

        return $this;
    }

    public function getPelicula()
    {
        return $this->pelicula;
    }

    public function setPelicula(string $pelicula)
    {
        $this->pelicula = $pelicula;

        return $this;
    }

    public function getHora()
    {
        return $this->hora;
    }

    public function setHora(string $hora)
    {
        $this->hora = $hora;

        return $this;
    }

    public function getComplejo()
    {
        return $this->complejo;
    }

    public function setComplejo(string $complejo)
    {
        $this->complejo = $complejo;

        return $this;
    }

    public function getMensaje()
    {
        return $this->mensaje;
    }

    public function setMensaje(string $mensaje)
    {
        $this->mensaje = $mensaje;

        return $this;
    }

    public function getSala()
    {
        return $this->sala;
    }

    public function setSala(string $sala)
    {
        $this->sala = $sala;

        return $this;
    }

    public function getCantidadBoletos()
    {
        return $this->cantidadBoletos;
    }

    public function setCantidadBoletos(int $cantidadBoletos)
    {
        $this->cantidadBoletos = $cantidadBoletos;

        return $this;
    }

    public function getFechafuncion()
    {
        return $this->fechafuncion;
    }

    public function setFechafuncion(\DateTimeInterface $fechafuncion)
    {
        $this->fechafuncion = $fechafuncion;

        return $this;
    }

    public function getTecnologia()
    {
        return $this->tecnologia;
    }

    public function setTecnologia(string $tecnologia)
    {
        $this->tecnologia = $tecnologia;

        return $this;
    }

    public function getCorreo()
    {
        return $this->correo;
    }

    public function setCorreo(string $correo)
    {
        $this->correo = $correo;

        return $this;
    }

    public function getVencashvoucher()
    {
        return $this->vencashvoucher;
    }

    public function setVencashvoucher(string $vencashvoucher)
    {
        $this->vencashvoucher = $vencashvoucher;

        return $this;
    }

    public function getBookingnumber()
    {
        return $this->bookingnumber;
    }

    public function setBookingnumber(string $bookingnumber)
    {
        $this->bookingnumber = $bookingnumber;

        return $this;
    }

    public function getDetalleboleto()
    {
        return $this->detalleboleto;
    }

    public function setDetalleboleto(string $detalleboleto)
    {
        $this->detalleboleto = $detalleboleto;

        return $this;
    }

    public function getVistamontoincent()
    {
        return $this->vistamontoincent;
    }

    public function setVistamontoincent(int $vistamontoincent)
    {
        $this->vistamontoincent = $vistamontoincent;

        return $this;
    }

    public function getPorreversar()
    {
        return $this->porreversar;
    }

    public function setPorreversar(string $porreversar)
    {
        $this->porreversar = $porreversar;

        return $this;
    }

    public function getCausa()
    {
        return $this->causa;
    }

    public function setCausa(string $causa)
    {
        $this->causa = $causa;

        return $this;
    }


}
