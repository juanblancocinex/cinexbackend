<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Summary.
 *
 * Description:  CarteleraTiny Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 *
 * @ORM\Table(name="cartelera_tiny")
 * @ORM\Entity
 */
class CarteleraTiny
{
    /**
     * @var int
     *
     * @ORM\Column(name="cart_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $cartId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cart_name", type="string", length=255, nullable=true)
     */
    private $cartName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cart_trailer", type="string", length=255, nullable=true)
     */
    private $cartTrailer;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cart_versions", type="string", length=255, nullable=true)
     */
    private $cartVersions;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="cart_fechaestreno", type="date", nullable=true)
     */
    private $cartFechaestreno;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cart_preventa", type="string", length=1, nullable=true, options={"default"="N","fixed"=true})
     */
    private $cartPreventa = 'N';

    /**
     * @var string|null
     *
     * @ORM\Column(name="cart_venezolana", type="string", length=1, nullable=true, options={"default"="N","fixed"=true})
     */
    private $cartVenezolana = 'N';

    /**
     * @var string|null
     *
     * @ORM\Column(name="cart_preestreno", type="string", length=1, nullable=true, options={"default"="N","fixed"=true})
     */
    private $cartPreestreno = 'N';

    /**
     * @var int|null
     *
     * @ORM\Column(name="cart_position", type="integer", nullable=true)
     */
    private $cartPosition = '0';

    public function getCartId()
    {
        return $this->cartId;
    }

    public function getCartName()
    {
        return $this->cartName;
    }

    public function setCartName(string $cartName)
    {
        $this->cartName = $cartName;

        return $this;
    }

    public function getCartTrailer()
    {
        return $this->cartTrailer;
    }

    public function setCartTrailer(string $cartTrailer)
    {
        $this->cartTrailer = $cartTrailer;

        return $this;
    }

    public function getCartVersions()
    {
        return $this->cartVersions;
    }

    public function setCartVersions(string $cartVersions)
    {
        $this->cartVersions = $cartVersions;

        return $this;
    }

    public function getCartFechaestreno()
    {
        return $this->cartFechaestreno;
    }

    public function setCartFechaestreno(\DateTimeInterface $cartFechaestreno)
    {
        $this->cartFechaestreno = $cartFechaestreno;

        return $this;
    }

    public function getCartPreventa()
    {
        return $this->cartPreventa;
    }

    public function setCartPreventa(string $cartPreventa)
    {
        $this->cartPreventa = $cartPreventa;

        return $this;
    }

    public function getCartVenezolana()
    {
        return $this->cartVenezolana;
    }

    public function setCartVenezolana(string $cartVenezolana)
    {
        $this->cartVenezolana = $cartVenezolana;

        return $this;
    }

    public function getCartPreestreno()
    {
        return $this->cartPreestreno;
    }

    public function setCartPreestreno(string $cartPreestreno)
    {
        $this->cartPreestreno = $cartPreestreno;

        return $this;
    }

    public function getCartPosition()
    {
        return $this->cartPosition;
    }

    public function setCartPosition(int $cartPosition)
    {
        $this->cartPosition = $cartPosition;

        return $this;
    }


}
