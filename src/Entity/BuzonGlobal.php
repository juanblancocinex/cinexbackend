<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * 
 * Summary.
 *
 * Description:  BuzonGlobal Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="buzon_global")
 * @ORM\Entity
 */
class BuzonGlobal
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="usuarioid", type="integer", nullable=true)
     */
    private $usuarioid;

    /**
     * @var int|null
     *
     * @ORM\Column(name="buzonid", type="integer", nullable=true)
     */
    private $buzonid;

    /**
     * @var int|null
     *
     * @ORM\Column(name="borrado", type="integer", nullable=true)
     */
    private $borrado;

    public function getId()
    {
        return $this->id;
    }

    public function getUsuarioid()
    {
        return $this->usuarioid;
    }

    public function setUsuarioid(int $usuarioid)
    {
        $this->usuarioid = $usuarioid;

        return $this;
    }

    public function getBuzonid()
    {
        return $this->buzonid;
    }

    public function setBuzonid(int $buzonid)
    {
        $this->buzonid = $buzonid;

        return $this;
    }

    public function getBorrado()
    {
        return $this->borrado;
    }

    public function setBorrado(int $borrado)
    {
        $this->borrado = $borrado;

        return $this;
    }


}
