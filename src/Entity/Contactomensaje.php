<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * * Summary.
 *
 * Description:  Contactomensaje Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 * 
 * @ORM\Table(name="contactomensaje")
 * @ORM\Entity
 */
class Contactomensaje
{
    /**
     * @var int
     *
     * @ORM\Column(name="idcontactomensaje", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcontactomensaje;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombrecompleto", type="string", length=100, nullable=true)
     */
    private $nombrecompleto;

    /**
     * @var string|null
     *
     * @ORM\Column(name="correoelectronico", type="string", length=100, nullable=true)
     */
    private $correoelectronico;

    /**
     * @var string|null
     *
     * @ORM\Column(name="numerocontactotelefono", type="string", length=45, nullable=true)
     */
    private $numerocontactotelefono;

    /**
     * @var string|null
     *
     * @ORM\Column(name="codigocomplejo", type="string", length=50, nullable=true)
     */
    private $codigocomplejo;

    /**
     * @var int|null
     *
     * @ORM\Column(name="idcontactomotivos", type="integer", nullable=true)
     */
    private $idcontactomotivos;

    /**
     * @var int|null
     *
     * @ORM\Column(name="idcontactotemas", type="integer", nullable=true)
     */
    private $idcontactotemas;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comentario", type="string", length=255, nullable=true)
     */
    private $comentario;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=true)
     */
    private $fecha;

    public function getIdcontactomensaje()
    {
        return $this->idcontactomensaje;
    }

    public function getNombrecompleto()
    {
        return $this->nombrecompleto;
    }

    public function setNombrecompleto(string $nombrecompleto)
    {
        $this->nombrecompleto = $nombrecompleto;

        return $this;
    }

    public function getCorreoelectronico()
    {
        return $this->correoelectronico;
    }

    public function setCorreoelectronico(string $correoelectronico)
    {
        $this->correoelectronico = $correoelectronico;

        return $this;
    }

    public function getNumerocontactotelefono()
    {
        return $this->numerocontactotelefono;
    }

    public function setNumerocontactotelefono(string $numerocontactotelefono)
    {
        $this->numerocontactotelefono = $numerocontactotelefono;

        return $this;
    }

    public function getCodigocomplejo()
    {
        return $this->codigocomplejo;
    }

    public function setCodigocomplejo(string $codigocomplejo)
    {
        $this->codigocomplejo = $codigocomplejo;

        return $this;
    }

    public function getIdcontactomotivos()
    {
        return $this->idcontactomotivos;
    }

    public function setIdcontactomotivos(int $idcontactomotivos)
    {
        $this->idcontactomotivos = $idcontactomotivos;

        return $this;
    }

    public function getIdcontactotemas()
    {
        return $this->idcontactotemas;
    }

    public function setIdcontactotemas(int $idcontactotemas)
    {
        $this->idcontactotemas = $idcontactotemas;

        return $this;
    }

    public function getComentario()
    {
        return $this->comentario;
    }

    public function setComentario(string $comentario)
    {
        $this->comentario = $comentario;

        return $this;
    }

    public function getFecha()
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }


}
