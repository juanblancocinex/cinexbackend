<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
  *  Summary.
 *
 * Description: Top5Tiny Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="top5_tiny")
 * @ORM\Entity
 */
class Top5Tiny
{
    /**
     * @var int
     *
     * @ORM\Column(name="top5t_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $top5tId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="top5t_name", type="string", length=255, nullable=true)
     */
    private $top5tName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="top5t_trailer", type="string", length=255, nullable=true)
     */
    private $top5tTrailer;

    /**
     * @var string|null
     *
     * @ORM\Column(name="top5t_versions", type="string", length=255, nullable=true)
     */
    private $top5tVersions;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="top5t_fechaestreno", type="date", nullable=true)
     */
    private $top5tFechaestreno;

    /**
     * @var string|null
     *
     * @ORM\Column(name="top5t_preventa", type="string", length=1, nullable=true, options={"default"="N","fixed"=true})
     */
    private $top5tPreventa = 'N';

    /**
     * @var string|null
     *
     * @ORM\Column(name="top5t_venezolana", type="string", length=1, nullable=true, options={"default"="N","fixed"=true})
     */
    private $top5tVenezolana = 'N';

    /**
     * @var string|null
     *
     * @ORM\Column(name="top5t_preestreno", type="string", length=1, nullable=true, options={"default"="N","fixed"=true})
     */
    private $top5tPreestreno = 'N';

    /**
     * @var int|null
     *
     * @ORM\Column(name="top5t_position", type="integer", nullable=true)
     */
    private $top5tPosition = '0';

    public function getTop5tId()
    {
        return $this->top5tId;
    }

    public function getTop5tName()
    {
        return $this->top5tName;
    }

    public function setTop5tName(string $top5tName)
    {
        $this->top5tName = $top5tName;

        return $this;
    }

    public function getTop5tTrailer()
    {
        return $this->top5tTrailer;
    }

    public function setTop5tTrailer(string $top5tTrailer)
    {
        $this->top5tTrailer = $top5tTrailer;

        return $this;
    }

    public function getTop5tVersions()
    {
        return $this->top5tVersions;
    }

    public function setTop5tVersions(string $top5tVersions)
    {
        $this->top5tVersions = $top5tVersions;

        return $this;
    }

    public function getTop5tFechaestreno()
    {
        return $this->top5tFechaestreno;
    }

    public function setTop5tFechaestreno(\DateTimeInterface $top5tFechaestreno)
    {
        $this->top5tFechaestreno = $top5tFechaestreno;

        return $this;
    }

    public function getTop5tPreventa()
    {
        return $this->top5tPreventa;
    }

    public function setTop5tPreventa(string $top5tPreventa)
    {
        $this->top5tPreventa = $top5tPreventa;

        return $this;
    }

    public function getTop5tVenezolana()
    {
        return $this->top5tVenezolana;
    }

    public function setTop5tVenezolana(string $top5tVenezolana)
    {
        $this->top5tVenezolana = $top5tVenezolana;

        return $this;
    }

    public function getTop5tPreestreno()
    {
        return $this->top5tPreestreno;
    }

    public function setTop5tPreestreno(string $top5tPreestreno)
    {
        $this->top5tPreestreno = $top5tPreestreno;

        return $this;
    }

    public function getTop5tPosition()
    {
        return $this->top5tPosition;
    }

    public function setTop5tPosition(int $top5tPosition)
    {
        $this->top5tPosition = $top5tPosition;

        return $this;
    }


}
