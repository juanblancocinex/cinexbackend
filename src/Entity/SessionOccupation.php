<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: SessionOccupation Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="session_occupation")
 * @ORM\Entity
 */
class SessionOccupation
{
    /**
     * @var int
     *
     * @ORM\Column(name="occup_id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $occupId;

    /**
     * @var int
     *
     * @ORM\Column(name="session_id", type="integer", nullable=false)
     */
    private $sessionId;

    /**
     * @var string
     *
     * @ORM\Column(name="seats_avalaible", type="string", length=255, nullable=false)
     */
    private $seatsAvalaible;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=255, nullable=false)
     */
    private $color;

    public function getOccupId()
    {
        return $this->occupId;
    }

    public function getSessionId()
    {
        return $this->sessionId;
    }

    public function setSessionId(int $sessionId)
    {
        $this->sessionId = $sessionId;

        return $this;
    }

    public function getSeatsAvalaible()
    {
        return $this->seatsAvalaible;
    }

    public function setSeatsAvalaible(string $seatsAvalaible)
    {
        $this->seatsAvalaible = $seatsAvalaible;

        return $this;
    }

    public function getColor()
    {
        return $this->color;
    }

    public function setColor(string $color)
    {
        $this->color = $color;

        return $this;
    }


}
