<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: Motivoscontacto Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="motivosContacto")
 * @ORM\Entity(repositoryClass="App\Repository\MotivosContactoRepository")
 */
class Motivoscontacto
{
    /**
     * @var int
     *
     * @ORM\Column(name="motivoID", type="integer", nullable=false, options={"unsigned"=true,"comment"="identificador de la ciudad"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $motivoid;

    /**
     * @var string
     *
     * @ORM\Column(name="motivoDescripcion", type="string", length=50, nullable=false, options={"comment"="nombre de la ciudad"})
     */
    private $motivodescripcion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=true)
     */
    private $email;

    public function getMotivoid()
    {
        return $this->motivoid;
    }

    public function getMotivodescripcion()
    {
        return $this->motivodescripcion;
    }

    public function setMotivodescripcion(string $motivodescripcion)
    {
        $this->motivodescripcion = $motivodescripcion;

        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail(string $email)
    {
        $this->email = $email;

        return $this;
    }


}
