<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: Municipios Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="municipios", indexes={@ORM\Index(name="id_estado", columns={"id_estado"})})
 * @ORM\Entity(repositoryClass="App\Repository\MunicipiosRepository")
 */
class Municipios
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_municipio", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idMunicipio;

    /**
     * @var int
     *
     * @ORM\Column(name="id_estado", type="integer", nullable=false)
     */
    private $idEstado;


    /**
     * @var string
     *
     * @ORM\Column(name="municipio", type="string", length=100, nullable=false)
     */
    private $municipio;

     /**
     * Many Municipios have One Estado.
     * @ORM\ManyToOne(targetEntity="Estados", inversedBy="municipios")
     * @ORM\JoinColumn(name="id_estado", referencedColumnName="id_estado", nullable=true)
     */
    private $estado;


    /**
     * One Municipio has Many Parroquias.
     * @ORM\OneToMany(targetEntity="Parroquias", mappedBy="idMunicipio")
     */
    private $parroquias;

    public function __construct()
    {
        $this->parroquias = new ArrayCollection();
    }

    public function getIdMunicipio(): ?int
    {
        return $this->idMunicipio;
    }

    public function getIdEstado(): ?int
    {
        return $this->idEstado;
    }

    public function setIdEstado(int $idEstado): self
    {
        $this->idEstado = $idEstado;

        return $this;
    }

    public function getMunicipio(): ?string
    {
        return $this->municipio;
    }

    public function setMunicipio(string $municipio): self
    {
        $this->municipio = $municipio;

        return $this;
    }

    public function getEstado(): ?Estados
    {
        return $this->estado;
    }

    public function setEstado(?Estados $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * @return Collection|Parroquias[]
     */
    public function getParroquias(): Collection
    {
        return $this->parroquias;
    }

    public function addParroquia(Parroquias $parroquia): self
    {
        if (!$this->parroquias->contains($parroquia)) {
            $this->parroquias[] = $parroquia;
            $parroquia->setIdMunicipio($this);
        }

        return $this;
    }

    public function removeParroquia(Parroquias $parroquia): self
    {
        if ($this->parroquias->contains($parroquia)) {
            $this->parroquias->removeElement($parroquia);
            // set the owning side to null (unless already changed)
            if ($parroquia->getIdMunicipio() === $this) {
                $parroquia->setIdMunicipio(null);
            }
        }

        return $this;
    }
    
}
