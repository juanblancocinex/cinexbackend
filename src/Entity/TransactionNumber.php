<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: TransaccionNumber Entity
 *
 * @since 1.0
 * @author Humberto Suarez <hsuarez@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="transaction_number", indexes={@ORM\Index(name="id", columns={"id"}), @ORM\Index(name="reference", columns={"reference"}), @ORM\Index(name="usuarioid", columns={"usuarioid"}), @ORM\Index(name="datetime", columns={"datetime"})})
 * @ORM\Entity(repositoryClass="App\Repository\TransactionNumberRepository")
 */
class TransactionNumber
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="reference", type="string", length=20, nullable=true)
     */
    private $reference;

    /**
     * @var string|null
     *
     * @ORM\Column(name="usuarioid", type="integer", length=11, nullable=true)
     */
    private $usuarioid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="datetime", type="string", length=45, nullable=true)
     */
    private $datetime;

    public function getId()
    {
        return $this->id;
    }

    public function setId(string $id)
    {
        $this->id = $id;

        return $this;
    }

    public function getReference()
    {
        return $this->reference;
    }

    public function setReference(string $reference)
    {
        $this->reference = $reference;

        return $this;
    }

    public function getUsuarioid()
    {
        return $this->usuarioid;
    }

    public function setUsuarioid(string $usuarioid)
    {
        $this->usuarioid = $usuarioid;

        return $this;
    }

    public function getDatetime()
    {
        return $this->datetime;
    }

    public function setFecha(string $fecha)
    {
        $this->datetime = $fecha;

        return $this;
    }

    public function setDatetime(?string $datetime): self
    {
        $this->datetime = $datetime;

        return $this;
    }
}
