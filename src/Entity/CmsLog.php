<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
  * Summary.
 *
 * Description:  CmsLog Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="cms_log")
 * @ORM\Entity
 */
class CmsLog
{
    /**
     * @var int
     *
     * @ORM\Column(name="log_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $logId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="usuario_id", type="integer", nullable=true)
     */
    private $usuarioId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="log_descripcion", type="text", length=0, nullable=true)
     */
    private $logDescripcion;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="log_fechahora", type="datetime", nullable=true)
     */
    private $logFechahora;

    /**
     * @var string|null
     *
     * @ORM\Column(name="log_remotehost", type="string", length=255, nullable=true)
     */
    private $logRemotehost;

    /**
     * @var string|null
     *
     * @ORM\Column(name="log_remoteip", type="string", length=255, nullable=true)
     */
    private $logRemoteip;

    public function getLogId()
    {
        return $this->logId;
    }

    public function getUsuarioId()
    {
        return $this->usuarioId;
    }

    public function setUsuarioId(int $usuarioId)
    {
        $this->usuarioId = $usuarioId;

        return $this;
    }

    public function getLogDescripcion()
    {
        return $this->logDescripcion;
    }

    public function setLogDescripcion(string $logDescripcion)
    {
        $this->logDescripcion = $logDescripcion;

        return $this;
    }

    public function getLogFechahora()
    {
        return $this->logFechahora;
    }

    public function setLogFechahora(\DateTimeInterface $logFechahora)
    {
        $this->logFechahora = $logFechahora;

        return $this;
    }

    public function getLogRemotehost()
    {
        return $this->logRemotehost;
    }

    public function setLogRemotehost(string $logRemotehost)
    {
        $this->logRemotehost = $logRemotehost;

        return $this;
    }

    public function getLogRemoteip()
    {
        return $this->logRemoteip;
    }

    public function setLogRemoteip(string $logRemoteip)
    {
        $this->logRemoteip = $logRemoteip;

        return $this;
    }


}
