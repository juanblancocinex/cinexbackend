<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
*  Summary.
 *
 * Description: Voucher50Sombras Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="voucher_50_sombras")
 * @ORM\Entity
 */
class Voucher50Sombras
{
    /**
     * @var string
     *
     * @ORM\Column(name="voucher", type="string", length=16, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $voucher;

    /**
     * @var int
     *
     * @ORM\Column(name="usado", type="integer", nullable=false, options={"default"="1"})
     */
    private $usado = '1';

    /**
     * @var int|null
     *
     * @ORM\Column(name="usuarioID", type="integer", nullable=true)
     */
    private $usuarioid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="premio", type="string", length=200, nullable=true)
     */
    private $premio;

    public function getVoucher()
    {
        return $this->voucher;
    }

    public function getUsado()
    {
        return $this->usado;
    }

    public function setUsado(int $usado)
    {
        $this->usado = $usado;

        return $this;
    }

    public function getUsuarioid()
    {
        return $this->usuarioid;
    }

    public function setUsuarioid(int $usuarioid)
    {
        $this->usuarioid = $usuarioid;

        return $this;
    }

    public function getPremio()
    {
        return $this->premio;
    }

    public function setPremio(string $premio)
    {
        $this->premio = $premio;

        return $this;
    }


}
