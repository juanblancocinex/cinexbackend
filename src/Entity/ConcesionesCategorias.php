<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Summary.
 *
 * Description:  ConcesionesCategorias Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="concesiones_categorias")
 * @ORM\Entity
 */
class ConcesionesCategorias
{
    /**
     * @var int
     *
     * @ORM\Column(name="cat_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $catId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cat_nombre", type="text", length=16777215, nullable=true)
     */
    private $catNombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cat_imagen_selected", type="blob", length=16777215, nullable=true)
     */
    private $catImagenSelected;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cat_imagen_over", type="blob", length=16777215, nullable=true)
     */
    private $catImagenOver;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cat_imagen_noselected", type="blob", length=16777215, nullable=true)
     */
    private $catImagenNoselected;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cat_imagen_card", type="blob", length=16777215, nullable=true)
     */
    private $catImagenCard;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cat_file_imagen_selected", type="string", length=255, nullable=true)
     */
    private $catFileImagenSelected;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cat_file_imagen_over", type="string", length=255, nullable=true)
     */
    private $catFileImagenOver;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cat_file_imagen_noselected", type="string", length=255, nullable=true)
     */
    private $catFileImagenNoselected;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cat_file_imagen_card", type="string", length=255, nullable=true)
     */
    private $catFileImagenCard;

    /**
     * @var int|null
     *
     * @ORM\Column(name="cat_orden", type="integer", nullable=true)
     */
    private $catOrden;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cat_activo", type="string", length=1, nullable=true, options={"default"="S","fixed"=true})
     */
    private $catActivo = 'S';

    public function getCatId()
    {
        return $this->catId;
    }

    public function getCatNombre()
    {
        return $this->catNombre;
    }

    public function setCatNombre(string $catNombre)
    {
        $this->catNombre = $catNombre;

        return $this;
    }

    public function getCatImagenSelected()
    {
        return $this->catImagenSelected;
    }

    public function setCatImagenSelected($catImagenSelected)
    {
        $this->catImagenSelected = $catImagenSelected;

        return $this;
    }

    public function getCatImagenOver()
    {
        return $this->catImagenOver;
    }

    public function setCatImagenOver($catImagenOver)
    {
        $this->catImagenOver = $catImagenOver;

        return $this;
    }

    public function getCatImagenNoselected()
    {
        return $this->catImagenNoselected;
    }

    public function setCatImagenNoselected($catImagenNoselected)
    {
        $this->catImagenNoselected = $catImagenNoselected;

        return $this;
    }

    public function getCatImagenCard()
    {
        return $this->catImagenCard;
    }

    public function setCatImagenCard($catImagenCard)
    {
        $this->catImagenCard = $catImagenCard;

        return $this;
    }

    public function getCatFileImagenSelected()
    {
        return $this->catFileImagenSelected;
    }

    public function setCatFileImagenSelected(string $catFileImagenSelected)
    {
        $this->catFileImagenSelected = $catFileImagenSelected;

        return $this;
    }

    public function getCatFileImagenOver()
    {
        return $this->catFileImagenOver;
    }

    public function setCatFileImagenOver(string $catFileImagenOver)
    {
        $this->catFileImagenOver = $catFileImagenOver;

        return $this;
    }

    public function getCatFileImagenNoselected()
    {
        return $this->catFileImagenNoselected;
    }

    public function setCatFileImagenNoselected(string $catFileImagenNoselected)
    {
        $this->catFileImagenNoselected = $catFileImagenNoselected;

        return $this;
    }

    public function getCatFileImagenCard()
    {
        return $this->catFileImagenCard;
    }

    public function setCatFileImagenCard(string $catFileImagenCard)
    {
        $this->catFileImagenCard = $catFileImagenCard;

        return $this;
    }

    public function getCatOrden()
    {
        return $this->catOrden;
    }

    public function setCatOrden(int $catOrden)
    {
        $this->catOrden = $catOrden;

        return $this;
    }

    public function getCatActivo()
    {
        return $this->catActivo;
    }

    public function setCatActivo(string $catActivo)
    {
        $this->catActivo = $catActivo;

        return $this;
    }


}
