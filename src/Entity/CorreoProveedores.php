<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description:  CorreoProveedores Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="correo_proveedores")
 * @ORM\Entity
 */
class CorreoProveedores
{
    /**
     * @var int
     *
     * @ORM\Column(name="idCorreo", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcorreo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre", type="string", length=100, nullable=true)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="correo", type="string", length=100, nullable=true)
     */
    private $correo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="enviado", type="string", length=1, nullable=true, options={"default"="N"})
     */
    private $enviado = 'N';

    public function getIdcorreo()
    {
        return $this->idcorreo;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getCorreo(): string
    {
        return $this->correo;
    }

    public function setCorreo(string $correo)
    {
        $this->correo = $correo;

        return $this;
    }

    public function getEnviado(): string
    {
        return $this->enviado;
    }

    public function setEnviado(string $enviado)
    {
        $this->enviado = $enviado;

        return $this;
    }


}
