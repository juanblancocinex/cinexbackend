<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Summary.
 *
 * Description:  ComplejoPropio Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="complejo_propio", uniqueConstraints={@ORM\UniqueConstraint(name="codigoComplejo", columns={"codigoComplejo"})}, indexes={@ORM\Index(name="ciudadID", columns={"ciudadID"})})
 * @ORM\Entity
 */
class ComplejoPropio
{
    /**
     * @var int
     *
     * @ORM\Column(name="complejoID", type="integer", nullable=false, options={"unsigned"=true,"comment"="clave artificial del complejo"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $complejoid;

    /**
     * @var string
     *
     * @ORM\Column(name="nombreComplejo", type="string", length=50, nullable=false, options={"comment"="nombre del complejo"})
     */
    private $nombrecomplejo;

    /**
     * @var string
     *
     * @ORM\Column(name="codigoComplejo", type="string", length=50, nullable=false, options={"comment"="codigo interno para interactuar con Vista y Sinec"})
     */
    private $codigocomplejo;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ciudadID", type="integer", nullable=true, options={"unsigned"=true,"comment"="ciudad donde esta ubicado el complejo"})
     */
    private $ciudadid;

    /**
     * @var int|null
     *
     * @ORM\Column(name="codigoInterno", type="integer", nullable=true)
     */
    private $codigointerno;

    /**
     * @var int|null
     *
     * @ORM\Column(name="codigoNativa", type="integer", nullable=true)
     */
    private $codigonativa;

    public function getComplejoid()
    {
        return $this->complejoid;
    }

    public function getNombrecomplejo()
    {
        return $this->nombrecomplejo;
    }

    public function setNombrecomplejo(string $nombrecomplejo)
    {
        $this->nombrecomplejo = $nombrecomplejo;

        return $this;
    }

    public function getCodigocomplejo()
    {
        return $this->codigocomplejo;
    }

    public function setCodigocomplejo(string $codigocomplejo)
    {
        $this->codigocomplejo = $codigocomplejo;

        return $this;
    }

    public function getCiudadid()
    {
        return $this->ciudadid;
    }

    public function setCiudadid(int $ciudadid)
    {
        $this->ciudadid = $ciudadid;

        return $this;
    }

    public function getCodigointerno()
    {
        return $this->codigointerno;
    }

    public function setCodigointerno(int $codigointerno)
    {
        $this->codigointerno = $codigointerno;

        return $this;
    }

    public function getCodigonativa()
    {
        return $this->codigonativa;
    }

    public function setCodigonativa(int $codigonativa)
    {
        $this->codigonativa = $codigonativa;

        return $this;
    }


}
