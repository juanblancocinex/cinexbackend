<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: SessionData Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 *
 * @ORM\Table(name="session_data")
 * @ORM\Entity
 */
class SessionData
{
    /**
     * @var string
     *
     * @ORM\Column(name="session_id", type="string", length=32, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $sessionId = '';

    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="string", length=32, nullable=false)
     */
    private $hash = '';

    /**
     * @var string
     *
     * @ORM\Column(name="session_data", type="blob", length=65535, nullable=false)
     */
    private $sessionData;

    /**
     * @var int
     *
     * @ORM\Column(name="session_expire", type="integer", nullable=false)
     */
    private $sessionExpire = '0';

    public function getSessionId()
    {
        return $this->sessionId;
    }

    public function getHash()
    {
        return $this->hash;
    }

    public function setHash(string $hash)
    {
        $this->hash = $hash;

        return $this;
    }

    public function getSessionData()
    {
        return $this->sessionData;
    }

    public function setSessionData($sessionData)
    {
        $this->sessionData = $sessionData;

        return $this;
    }

    public function getSessionExpire()
    {
        return $this->sessionExpire;
    }

    public function setSessionExpire(int $sessionExpire)
    {
        $this->sessionExpire = $sessionExpire;

        return $this;
    }


}
