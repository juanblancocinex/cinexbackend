<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: User Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="user")
 * @ORM\Entity
 */
class User
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=45, nullable=false)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=45, nullable=false)
     */
    private $password;

    /**
     * @var string|null
     *
     * @ORM\Column(name="authKey", type="string", length=45, nullable=true)
     */
    private $authkey;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vigente", type="string", length=1, nullable=true)
     */
    private $vigente;

    /**
     * @var string|null
     *
     * @ORM\Column(name="list_permisos", type="string", length=45, nullable=true)
     */
    private $listPermisos;

    /**
     * @var string|null
     *
     * @ORM\Column(name="permisos", type="string", length=45, nullable=true)
     */
    private $permisos;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=45, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="verification_code", type="string", length=250, nullable=false)
     */
    private $verificationCode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombres", type="string", length=255, nullable=true)
     */
    private $nombres;

    /**
     * @var string|null
     *
     * @ORM\Column(name="apellidos", type="string", length=255, nullable=true)
     */
    private $apellidos;

    public function getId()
    {
        return $this->id;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername(string $username)
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword(string $password)
    {
        $this->password = $password;

        return $this;
    }

    public function getAuthkey()
    {
        return $this->authkey;
    }

    public function setAuthkey(string $authkey)
    {
        $this->authkey = $authkey;

        return $this;
    }

    public function getVigente()
    {
        return $this->vigente;
    }

    public function setVigente(string $vigente)
    {
        $this->vigente = $vigente;

        return $this;
    }

    public function getListPermisos()
    {
        return $this->listPermisos;
    }

    public function setListPermisos(string $listPermisos)
    {
        $this->listPermisos = $listPermisos;

        return $this;
    }

    public function getPermisos()
    {
        return $this->permisos;
    }

    public function setPermisos(string $permisos)
    {
        $this->permisos = $permisos;

        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail(string $email)
    {
        $this->email = $email;

        return $this;
    }

    public function getVerificationCode()
    {
        return $this->verificationCode;
    }

    public function setVerificationCode(string $verificationCode)
    {
        $this->verificationCode = $verificationCode;

        return $this;
    }

    public function getNombres()
    {
        return $this->nombres;
    }

    public function setNombres(string $nombres)
    {
        $this->nombres = $nombres;

        return $this;
    }

    public function getApellidos()
    {
        return $this->apellidos;
    }

    public function setApellidos(string $apellidos)
    {
        $this->apellidos = $apellidos;

        return $this;
    }


}
