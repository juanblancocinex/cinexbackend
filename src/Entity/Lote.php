<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
  *  Summary.
 *
 * Description: Lote Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="lote", uniqueConstraints={@ORM\UniqueConstraint(name="fk_lote_tipo", columns={"id_tipo", "id_voucher"}), @ORM\UniqueConstraint(name="fk_lote_voucher", columns={"id_voucher", "id_tipo"})}, indexes={@ORM\Index(name="IDX_65B4329FFB0D0145", columns={"id_tipo"}), @ORM\Index(name="IDX_65B4329FCD0F0CA8", columns={"id_voucher"})})
 * @ORM\Entity
 */
class Lote
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcion", type="string", length=100, nullable=true)
     */
    private $descripcion;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha_registro", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $fechaRegistro = 'CURRENT_TIMESTAMP';

    /**
     * @var \Tipo
     *
     * @ORM\ManyToOne(targetEntity="Tipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tipo", referencedColumnName="id")
     * })
     */
    private $idTipo;

    /**
     * @var \Voucher
     *
     * @ORM\ManyToOne(targetEntity="Voucher")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_voucher", referencedColumnName="id")
     * })
     */
    private $idVoucher;

    public function getId()
    {
        return $this->id;
    }

    public function getDescripcion()
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getFechaRegistro()
    {
        return $this->fechaRegistro;
    }

    public function setFechaRegistro(\DateTimeInterface $fechaRegistro)
    {
        $this->fechaRegistro = $fechaRegistro;

        return $this;
    }

    public function getIdTipo()
    {
        return $this->idTipo;
    }

    public function setIdTipo(Tipo $idTipo)
    {
        $this->idTipo = $idTipo;

        return $this;
    }

    public function getIdVoucher()
    {
        return $this->idVoucher;
    }

    public function setIdVoucher(Voucher $idVoucher)
    {
        $this->idVoucher = $idVoucher;

        return $this;
    }


}
