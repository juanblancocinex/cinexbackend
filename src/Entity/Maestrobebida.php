<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: Maestrobebida Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="maestroBebida")
 * @ORM\Entity
 */
class Maestrobebida
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="bebida", type="string", length=45, nullable=true)
     */
    private $bebida;

    public function getId()
    {
        return $this->id;
    }

    public function getBebida()
    {
        return $this->bebida;
    }

    public function setBebida(string $bebida)
    {
        $this->bebida = $bebida;

        return $this;
    }


}
