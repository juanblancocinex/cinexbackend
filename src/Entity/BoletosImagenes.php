<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * 
* Summary.
 *
 * Description:  BoletosImagenes Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 * 
 * @ORM\Table(name="boletos_imagenes")
 * @ORM\Entity
 */
class BoletosImagenes
{
    /**
     * @var int
     *
     * @ORM\Column(name="bol_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $bolId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="bol_img_selected", type="blob", length=16777215, nullable=true)
     */
    private $bolImgSelected;

    /**
     * @var string|null
     *
     * @ORM\Column(name="bol_img_over", type="blob", length=16777215, nullable=true)
     */
    private $bolImgOver;

    /**
     * @var string|null
     *
     * @ORM\Column(name="bol_img_noselected", type="blob", length=16777215, nullable=true)
     */
    private $bolImgNoselected;

    /**
     * @var string|null
     *
     * @ORM\Column(name="bol_ticket_type_description_key", type="string", length=255, nullable=true)
     */
    private $bolTicketTypeDescriptionKey;

    /**
     * @var string|null
     *
     * @ORM\Column(name="bol_ticket_description", type="string", length=255, nullable=true)
     */
    private $bolTicketDescription;

    /**
     * @var string|null
     *
     * @ORM\Column(name="bol_activo", type="string", length=1, nullable=true, options={"default"="S","fixed"=true})
     */
    private $bolActivo = 'S';

    /**
     * @var int|null
     *
     * @ORM\Column(name="bol_orden", type="integer", nullable=true)
     */
    private $bolOrden;

    /**
     * @var string|null
     *
     * @ORM\Column(name="bol_file_img_selected", type="string", length=255, nullable=true)
     */
    private $bolFileImgSelected;

    /**
     * @var string|null
     *
     * @ORM\Column(name="bol_file_img_over", type="string", length=255, nullable=true)
     */
    private $bolFileImgOver;

    /**
     * @var string|null
     *
     * @ORM\Column(name="bol_file_img_noselected", type="string", length=255, nullable=true)
     */
    private $bolFileImgNoselected;

    public function getBolId()
    {
        return $this->bolId;
    }

    public function getBolImgSelected()
    {
        return $this->bolImgSelected;
    }

    public function setBolImgSelected($bolImgSelected)
    {
        $this->bolImgSelected = $bolImgSelected;

        return $this;
    }

    public function getBolImgOver()
    {
        return $this->bolImgOver;
    }

    public function setBolImgOver($bolImgOver)
    {
        $this->bolImgOver = $bolImgOver;

        return $this;
    }

    public function getBolImgNoselected()
    {
        return $this->bolImgNoselected;
    }

    public function setBolImgNoselected($bolImgNoselected)
    {
        $this->bolImgNoselected = $bolImgNoselected;

        return $this;
    }

    public function getBolTicketTypeDescriptionKey()
    {
        return $this->bolTicketTypeDescriptionKey;
    }

    public function setBolTicketTypeDescriptionKey(string $bolTicketTypeDescriptionKey)
    {
        $this->bolTicketTypeDescriptionKey = $bolTicketTypeDescriptionKey;

        return $this;
    }

    public function getBolTicketDescription()
    {
        return $this->bolTicketDescription;
    }

    public function setBolTicketDescription(string $bolTicketDescription)
    {
        $this->bolTicketDescription = $bolTicketDescription;

        return $this;
    }

    public function getBolActivo()
    {
        return $this->bolActivo;
    }

    public function setBolActivo(string $bolActivo)
    {
        $this->bolActivo = $bolActivo;

        return $this;
    }

    public function getBolOrden()
    {
        return $this->bolOrden;
    }

    public function setBolOrden(int $bolOrden)
    {
        $this->bolOrden = $bolOrden;

        return $this;
    }

    public function getBolFileImgSelected()
    {
        return $this->bolFileImgSelected;
    }

    public function setBolFileImgSelected(string $bolFileImgSelected)
    {
        $this->bolFileImgSelected = $bolFileImgSelected;

        return $this;
    }

    public function getBolFileImgOver()
    {
        return $this->bolFileImgOver;
    }

    public function setBolFileImgOver(string $bolFileImgOver)
    {
        $this->bolFileImgOver = $bolFileImgOver;

        return $this;
    }

    public function getBolFileImgNoselected()
    {
        return $this->bolFileImgNoselected;
    }

    public function setBolFileImgNoselected(string $bolFileImgNoselected)
    {
        $this->bolFileImgNoselected = $bolFileImgNoselected;

        return $this;
    }


}
