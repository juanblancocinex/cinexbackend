<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: Ronda Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve
 *
 * @ORM\Table(name="ronda", indexes={@ORM\Index(name="ronda", columns={"ronda"})})
 * @ORM\Entity
 */
class Ronda
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ronda", type="string", length=100, nullable=true)
     */
    private $ronda;

    /**
     * @var int|null
     *
     * @ORM\Column(name="npartidos", type="smallint", nullable=true)
     */
    private $npartidos;

    public function getId()
    {
        return $this->id;
    }

    public function getRonda()
    {
        return $this->ronda;
    }

    public function setRonda(string $ronda)
    {
        $this->ronda = $ronda;

        return $this;
    }

    public function getNpartidos()
    {
        return $this->npartidos;
    }

    public function setNpartidos(int $npartidos)
    {
        $this->npartidos = $npartidos;

        return $this;
    }


}
