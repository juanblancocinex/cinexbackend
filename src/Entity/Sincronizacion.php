<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: Sincronizacion Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="sincronizacion", uniqueConstraints={@ORM\UniqueConstraint(name="codigoComplejo_UNIQUE", columns={"codigoComplejo"})})
 * @ORM\Entity
 */
class Sincronizacion
{
    /**
     * @var int
     *
     * @ORM\Column(name="sincronizacionID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $sincronizacionid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="codigoComplejo", type="string", length=50, nullable=true)
     */
    private $codigocomplejo;

    public function getSincronizacionid()
    {
        return $this->sincronizacionid;
    }

    public function getCodigocomplejo()
    {
        return $this->codigocomplejo;
    }

    public function setCodigocomplejo(string $codigocomplejo)
    {
        $this->codigocomplejo = $codigocomplejo;

        return $this;
    }


}
