<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: ImagenItemsConcesioneso Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="imagen_items_concesiones")
 * @ORM\Entity
 */
class ImagenItemsConcesiones
{
    /**
     * @var int
     *
     * @ORM\Column(name="con_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $conId;

    /**
     * @var string
     *
     * @ORM\Column(name="con_ruta", type="string", length=255, nullable=false, options={"default"="/imagenes/concesiones/condef.jpg"})
     */
    private $conRuta = '/imagenes/concesiones/condef.jpg';

    /**
     * @var string
     *
     * @ORM\Column(name="con_strid", type="string", length=10, nullable=false)
     */
    private $conStrid = '0';

    public function getConId()
    {
        return $this->conId;
    }

    public function getConRuta()
    {
        return $this->conRuta;
    }

    public function setConRuta(string $conRuta)
    {
        $this->conRuta = $conRuta;

        return $this;
    }

    public function getConStrid()
    {
        return $this->conStrid;
    }

    public function setConStrid(string $conStrid)
    {
        $this->conStrid = $conStrid;

        return $this;
    }


}
