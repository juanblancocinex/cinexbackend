<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: Localizador Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 *
 * @ORM\Table(name="localizador")
 * @ORM\Entity
 */
class Localizador
{
    /**
     * @var string
     *
     * @ORM\Column(name="localizador", type="string", length=10, nullable=false, options={"fixed"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $localizador;

    public function getLocalizador()
    {
        return $this->localizador;
    }


}
