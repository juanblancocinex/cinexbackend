<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description:  Estados Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="estados")
 * @ORM\Entity(repositoryClass="App\Repository\EstadoRepository")
 */
class Estados
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_estado", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idEstado;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=250, nullable=false)
     */
    private $estado;


    /**
     * One Estado has Many Ciudades.
     * @ORM\OneToMany(targetEntity="Ciudades", mappedBy="estado")
     */
    private $ciudades;


    /**
     * One Estado has Many Municipios.
     * @ORM\OneToMany(targetEntity="Municipios", mappedBy="estado")
     */
    private $municipios;


    /**
     * @var string
     *
     * @ORM\Column(name="iso_3166-2", type="string", length=4, nullable=false)
     */
    private $iso31662;

    public function __construct()
    {
        $this->ciudades = new ArrayCollection();
        $this->municipios = new ArrayCollection();
    }

    public function getIdEstado(): ?int
    {
        return $this->idEstado;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getIso31662(): ?string
    {
        return $this->iso31662;
    }

    public function setIso31662(string $iso31662): self
    {
        $this->iso31662 = $iso31662;

        return $this;
    }

    /**
     * @return Collection|Ciudades[]
     */
    public function getCiudades(): Collection
    {
        return $this->ciudades;
    }

    public function addCiudade(Ciudades $ciudade): self
    {
        if (!$this->ciudades->contains($ciudade)) {
            $this->ciudades[] = $ciudade;
            $ciudade->setEstado($this);
        }

        return $this;
    }

    public function removeCiudade(Ciudades $ciudade): self
    {
        if ($this->ciudades->contains($ciudade)) {
            $this->ciudades->removeElement($ciudade);
            // set the owning side to null (unless already changed)
            if ($ciudade->getEstado() === $this) {
                $ciudade->setEstado(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Municipios[]
     */
    public function getMunicipios(): Collection
    {
        return $this->municipios;
    }

    public function addMunicipio(Municipios $municipio): self
    {
        if (!$this->municipios->contains($municipio)) {
            $this->municipios[] = $municipio;
            $municipio->setEstado($this);
        }

        return $this;
    }

    public function removeMunicipio(Municipios $municipio): self
    {
        if ($this->municipios->contains($municipio)) {
            $this->municipios->removeElement($municipio);
            // set the owning side to null (unless already changed)
            if ($municipio->getEstado() === $this) {
                $municipio->setEstado(null);
            }
        }

        return $this;
    }

   


}
