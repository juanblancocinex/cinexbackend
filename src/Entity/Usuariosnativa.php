<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
*  Summary.
 *
 * Description: Usuariosnativa Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="usuariosNativa")
 * @ORM\Entity
 */
class Usuariosnativa
{
    /**
     * @var int
     *
     * @ORM\Column(name="usuarioID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $usuarioid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre", type="string", length=50, nullable=true)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="apellido", type="string", length=50, nullable=true)
     */
    private $apellido;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fNacimiento", type="datetime", nullable=true)
     */
    private $fnacimiento;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sexo", type="string", length=1, nullable=true)
     */
    private $sexo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cedula", type="string", length=20, nullable=true)
     */
    private $cedula;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=true)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="movil", type="string", length=20, nullable=true)
     */
    private $movil;

    /**
     * @var string|null
     *
     * @ORM\Column(name="pin", type="string", length=40, nullable=true)
     */
    private $pin;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaDeCreacion", type="datetime", nullable=true)
     */
    private $fechadecreacion;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="ultimaActualizacion", type="datetime", nullable=true)
     */
    private $ultimaactualizacion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tecnologia", type="string", length=5, nullable=true)
     */
    private $tecnologia;

    public function getUsuarioid()
    {
        return $this->usuarioid;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getApellido()
    {
        return $this->apellido;
    }

    public function setApellido(string $apellido)
    {
        $this->apellido = $apellido;

        return $this;
    }

    public function getFnacimiento()
    {
        return $this->fnacimiento;
    }

    public function setFnacimiento(\DateTimeInterface $fnacimiento)
    {
        $this->fnacimiento = $fnacimiento;

        return $this;
    }

    public function getSexo()
    {
        return $this->sexo;
    }

    public function setSexo(string $sexo)
    {
        $this->sexo = $sexo;

        return $this;
    }

    public function getCedula()
    {
        return $this->cedula;
    }

    public function setCedula(string $cedula)
    {
        $this->cedula = $cedula;

        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail(string $email)
    {
        $this->email = $email;

        return $this;
    }

    public function getMovil()
    {
        return $this->movil;
    }

    public function setMovil(string $movil)
    {
        $this->movil = $movil;

        return $this;
    }

    public function getPin()
    {
        return $this->pin;
    }

    public function setPin(string $pin)
    {
        $this->pin = $pin;

        return $this;
    }

    public function getFechadecreacion()
    {
        return $this->fechadecreacion;
    }

    public function setFechadecreacion(\DateTimeInterface $fechadecreacion)
    {
        $this->fechadecreacion = $fechadecreacion;

        return $this;
    }

    public function getUltimaactualizacion()
    {
        return $this->ultimaactualizacion;
    }

    public function setUltimaactualizacion(\DateTimeInterface $ultimaactualizacion)
    {
        $this->ultimaactualizacion = $ultimaactualizacion;

        return $this;
    }

    public function getTecnologia()
    {
        return $this->tecnologia;
    }

    public function setTecnologia(string $tecnologia)
    {
        $this->tecnologia = $tecnologia;

        return $this;
    }


}
