<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description:  Contactotemas Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="contactotemas")
 * @ORM\Entity
 */
class Contactotemas
{
    /**
     * @var int
     *
     * @ORM\Column(name="idcontactotemas", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcontactotemas;

    /**
     * @var string|null
     *
     * @ORM\Column(name="contactotemasdescripcion", type="string", length=255, nullable=true)
     */
    private $contactotemasdescripcion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="contactotemasactivo", type="string", length=1, nullable=true, options={"fixed"=true})
     */
    private $contactotemasactivo;

    public function getIdcontactotemas()
    {
        return $this->idcontactotemas;
    }

    public function getContactotemasdescripcion()
    {
        return $this->contactotemasdescripcion;
    }

    public function setContactotemasdescripcion(string $contactotemasdescripcion)
    {
        $this->contactotemasdescripcion = $contactotemasdescripcion;

        return $this;
    }

    public function getContactotemasactivo()
    {
        return $this->contactotemasactivo;
    }

    public function setContactotemasactivo(string $contactotemasactivo)
    {
        $this->contactotemasactivo = $contactotemasactivo;

        return $this;
    }


}
