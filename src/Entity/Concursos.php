<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
  * Summary.
 *
 * Description:  Concurso Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 * 
 *
 * @ORM\Table(name="concursos")
 * @ORM\Entity
 */
class Concursos
{
    /**
     * @var int
     *
     * @ORM\Column(name="idconcursos", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idconcursos;

    /**
     * @var string
     *
     * @ORM\Column(name="nb_concurso", type="string", length=100, nullable=false)
     */
    private $nbConcurso;

    /**
     * @var string
     *
     * @ORM\Column(name="urlImg", type="string", length=45, nullable=false)
     */
    private $urlimg;

    /**
     * @var string
     *
     * @ORM\Column(name="urlDestino", type="string", length=45, nullable=false)
     */
    private $urldestino;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="ganadores", type="boolean", nullable=true)
     */
    private $ganadores;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_registro", type="date", nullable=false)
     */
    private $fechaRegistro;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_vencimiento", type="date", nullable=false)
     */
    private $fechaVencimiento;

    public function getIdconcursos()
    {
        return $this->idconcursos;
    }

    public function getNbConcurso()
    {
        return $this->nbConcurso;
    }

    public function setNbConcurso(string $nbConcurso)
    {
        $this->nbConcurso = $nbConcurso;

        return $this;
    }

    public function getUrlimg()
    {
        return $this->urlimg;
    }

    public function setUrlimg(string $urlimg)
    {
        $this->urlimg = $urlimg;

        return $this;
    }

    public function getUrldestino()
    {
        return $this->urldestino;
    }

    public function setUrldestino(string $urldestino)
    {
        $this->urldestino = $urldestino;

        return $this;
    }

    public function getGanadores()
    {
        return $this->ganadores;
    }

    public function setGanadores(bool $ganadores)
    {
        $this->ganadores = $ganadores;

        return $this;
    }

    public function getFechaRegistro()
    {
        return $this->fechaRegistro;
    }

    public function setFechaRegistro(\DateTimeInterface $fechaRegistro)
    {
        $this->fechaRegistro = $fechaRegistro;

        return $this;
    }

    public function getFechaVencimiento()
    {
        return $this->fechaVencimiento;
    }

    public function setFechaVencimiento(\DateTimeInterface $fechaVencimiento)
    {
        $this->fechaVencimiento = $fechaVencimiento;

        return $this;
    }


}
