<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: Menuxusuario Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 *
 * @ORM\Table(name="menuXusuario")
 * @ORM\Entity
 */
class Menuxusuario
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="administrativoID", type="integer", nullable=true)
     */
    private $administrativoid;

    /**
     * @var int|null
     *
     * @ORM\Column(name="menuID", type="integer", nullable=true)
     */
    private $menuid;

    public function getId()
    {
        return $this->id;
    }

    public function getAdministrativoid()
    {
        return $this->administrativoid;
    }

    public function setAdministrativoid(int $administrativoid)
    {
        $this->administrativoid = $administrativoid;

        return $this;
    }

    public function getMenuid()
    {
        return $this->menuid;
    }

    public function setMenuid(int $menuid)
    {
        $this->menuid = $menuid;

        return $this;
    }


}
