<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
  *  Summary.
 *
 * Description: MovieVersions Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="movie_versions")
 * @ORM\Entity
 */
class MovieVersions
{
    /**
     * @var int
     *
     * @ORM\Column(name="movie_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $movieId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="movie_name", type="string", length=255, nullable=true)
     */
    private $movieName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="movie_versions", type="string", length=255, nullable=true)
     */
    private $movieVersions;

    public function getMovieId()
    {
        return $this->movieId;
    }

    public function getMovieName()
    {
        return $this->movieName;
    }

    public function setMovieName(string $movieName)
    {
        $this->movieName = $movieName;

        return $this;
    }

    public function getMovieVersions()
    {
        return $this->movieVersions;
    }

    public function setMovieVersions(string $movieVersions)
    {
        $this->movieVersions = $movieVersions;

        return $this;
    }


}
