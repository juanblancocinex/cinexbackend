<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
  *  Summary.
 *
 * Description: Tmpvalidarcorreo Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="tmpValidarCorreo")
 * @ORM\Entity(repositoryClass="App\Repository\TmpvalidarcorreoRepository")
 */
class Tmpvalidarcorreo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="letra", type="string", length=1, nullable=false)
     */
    private $letra;

    /**
     * @var int
     *
     * @ORM\Column(name="ci", type="integer", nullable=false)
     */
    private $ci;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=50, nullable=false)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="ape", type="string", length=50, nullable=false)
     */
    private $ape;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=false)
     */
    private $email;

    public function getId()
    {
        return $this->id;
    }

    public function getLetra()
    {
        return $this->letra;
    }

    public function setLetra(string $letra)
    {
        $this->letra = $letra;

        return $this;
    }

    public function getCi()
    {
        return $this->ci;
    }

    public function setCi(int $ci)
    {
        $this->ci = $ci;

        return $this;
    }

    public function getNom()
    {
        return $this->nom;
    }

    public function setNom(string $nom)
    {
        $this->nom = $nom;

        return $this;
    }

    public function getApe()
    {
        return $this->ape;
    }

    public function setApe(string $ape)
    {
        $this->ape = $ape;

        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail(string $email)
    {
        $this->email = $email;

        return $this;
    }


}
