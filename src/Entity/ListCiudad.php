<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: ListCiudad Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 *
 * @ORM\Table(name="list_ciudad")
 * @ORM\Entity
 */
class ListCiudad
{
    /**
     * @var int
     *
     * @ORM\Column(name="listc_id", type="integer", nullable=false, options={"comment"="ID de la Ciudad"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $listcId;

    /**
     * @var string
     *
     * @ORM\Column(name="listc_codigo", type="string", length=5, nullable=false, options={"fixed"=true,"comment"="Codigo de la Ciudad"})
     */
    private $listcCodigo;

    /**
     * @var string
     *
     * @ORM\Column(name="listc_nombre", type="string", length=60, nullable=false, options={"comment"="Nombre de la Ciudad"})
     */
    private $listcNombre;

    /**
     * @var int
     *
     * @ORM\Column(name="listc_relacion", type="integer", nullable=false, options={"comment"="ID del municipio"})
     */
    private $listcRelacion;

    public function getListcId()
    {
        return $this->listcId;
    }

    public function getListcCodigo()
    {
        return $this->listcCodigo;
    }

    public function setListcCodigo(string $listcCodigo)
    {
        $this->listcCodigo = $listcCodigo;

        return $this;
    }

    public function getListcNombre()
    {
        return $this->listcNombre;
    }

    public function setListcNombre(string $listcNombre)
    {
        $this->listcNombre = $listcNombre;

        return $this;
    }

    public function getListcRelacion()
    {
        return $this->listcRelacion;
    }

    public function setListcRelacion(int $listcRelacion)
    {
        $this->listcRelacion = $listcRelacion;

        return $this;
    }


}
