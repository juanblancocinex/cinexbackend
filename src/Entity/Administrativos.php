<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * Summary.
 *
 * Description:  Administrativos Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018

 *
 * @ORM\Table(name="administrativos")
 * @ORM\Entity
 */
class Administrativos
{
    /**
     * @var string
     *
     * @ORM\Column(name="login", type="string", length=30, nullable=false)
     */
    private $login;

    /**
     * @var string|null
     *
     * @ORM\Column(name="password", type="string", length=256, nullable=true)
     */
    private $password;

    /**
     * @var int|null
     *
     * @ORM\Column(name="perfil", type="integer", nullable=true)
     */
    private $perfil;

    /**
     * @var string|null
     *
     * @ORM\Column(name="claveVigente", type="string", length=1, nullable=true)
     */
    private $clavevigente;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaCreacion", type="datetime", nullable=true)
     */
    private $fechacreacion;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="ultimaModificacion", type="datetime", nullable=true)
     */
    private $ultimamodificacion;

    /**
     * @var int
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre", type="string", length=45, nullable=true)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="esNuevo", type="string", length=1, nullable=true)
     */
    private $esnuevo;

    public function getLogin()
    {
        return $this->login;
    }

    public function setLogin(string $login)
    {
        $this->login = $login;

        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword(string $password)
    {
        $this->password = $password;

        return $this;
    }

    public function getPerfil()
    {
        return $this->perfil;
    }

    public function setPerfil(int $perfil)
    {
        $this->perfil = $perfil;

        return $this;
    }

    public function getClavevigente()
    {
        return $this->clavevigente;
    }

    public function setClavevigente(string $clavevigente)
    {
        $this->clavevigente = $clavevigente;

        return $this;
    }

    public function getFechacreacion()
    {
        return $this->fechacreacion;
    }

    public function setFechacreacion(\DateTimeInterface $fechacreacion)
    {
        $this->fechacreacion = $fechacreacion;

        return $this;
    }

    public function getUltimamodificacion()
    {
        return $this->ultimamodificacion;
    }

    public function setUltimamodificacion(\DateTimeInterface $ultimamodificacion)
    {
        $this->ultimamodificacion = $ultimamodificacion;

        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getEsnuevo()
    {
        return $this->esnuevo;
    }

    public function setEsnuevo(string $esnuevo)
    {
        $this->esnuevo = $esnuevo;

        return $this;
    }


}
