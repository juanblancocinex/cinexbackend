<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Summary.
 *
 * Description:  Ciudad Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="ciudad")
 * @ORM\Entity(repositoryClass="App\Repository\CiudadRepository")
 */
class Ciudad
{
    /**
     * @var int
     *
     * @ORM\Column(name="ciudadID", type="integer", nullable=false, options={"unsigned"=true,"comment"="identificador de la ciudad"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $ciudadid;

    /**
     * @var string
     *
     * @ORM\Column(name="nombreCiudad", type="string", length=50, nullable=false, options={"comment"="nombre de la ciudad"})
     */
    private $nombreciudad;

    /**
     * @var int|null
     *
     * @ORM\Column(name="orden", type="integer", nullable=true)
     */
    private $orden;


     /**
     * One Ciudad has Many Complejos.
     * @ORM\OneToMany(targetEntity="Complejo", mappedBy="ciudadId")
     */
    private $complejos;

    public function __construct()
    {
        $this->complejos = new ArrayCollection();
    }

    public function getCiudadid()
    {
        return $this->ciudadid;
    }

    public function getNombreciudad()
    {
        return $this->nombreciudad;
    }

    public function setNombreciudad(string $nombreciudad)
    {
        $this->nombreciudad = $nombreciudad;

        return $this;
    }

    public function getOrden()
    {
        return $this->orden;
    }

    public function setOrden(int $orden)
    {
        $this->orden = $orden;

        return $this;
    }

    /**
     * @return Collection|Complejo[]
     */
    public function getComplejos(): Collection
    {
        return $this->complejos;
    }

    public function addComplejo(Complejo $complejo)
    {
        if (!$this->complejos->contains($complejo)) {
            $this->complejos[] = $complejo;
            $complejo->setCiudadId($this);
        }

        return $this;
    }

    public function removeComplejo(Complejo $complejo)
    {
        if ($this->complejos->contains($complejo)) {
            $this->complejos->removeElement($complejo);
            // set the owning side to null (unless already changed)
            if ($complejo->getCiudadId() === $this) {
                $complejo->setCiudadId(null);
            }
        }

        return $this;
    }

    

}
