<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: SesionesEspeciales Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="sesiones_especiales")
 * @ORM\Entity
 */
class SesionesEspeciales
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;


    /**
     * Many SesionesEspeciales have One Pelicula.
     * @ORM\ManyToOne(targetEntity="PeliculaTest", inversedBy="sesionesespeciales")
     * @ORM\JoinColumn(name="idp", referencedColumnName="peliculaID", nullable=true)
     */
    private $idp;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ids", type="integer", nullable=true)
     */
    private $ids;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaCreacion", type="datetime", nullable=true)
     */
    private $fechacreacion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombresesion", type="string", length=45, nullable=true)
     */
    private $nombresesion;

    public function getId()
    {
        return $this->id;
    }

    public function getIds()
    {
        return $this->ids;
    }

    public function setIds(int $ids)
    {
        $this->ids = $ids;

        return $this;
    }

    public function getFechacreacion()
    {
        return $this->fechacreacion;
    }

    public function setFechacreacion(\DateTimeInterface $fechacreacion)
    {
        $this->fechacreacion = $fechacreacion;

        return $this;
    }

    public function getNombresesion()
    {
        return $this->nombresesion;
    }

    public function setNombresesion(string $nombresesion)
    {
        $this->nombresesion = $nombresesion;

        return $this;
    }

    public function getIdp()
    {
        return $this->idp;
    }

    public function setIdp(PeliculaTest $idp)
    {
        $this->idp = $idp;

        return $this;
    }

    

}
