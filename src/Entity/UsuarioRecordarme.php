<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: UsuarioRecordarme Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 *
 * @ORM\Table(name="usuario_recordarme")
 * @ORM\Entity
 */
class UsuarioRecordarme
{
    /**
     * @var int
     *
     * @ORM\Column(name="usuarioID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $usuarioid;

    /**
     * @var bool
     *
     * @ORM\Column(name="recordarme", type="boolean", nullable=false)
     */
    private $recordarme = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=50, nullable=false)
     */
    private $email = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="pwd", type="string", length=50, nullable=false)
     */
    private $pwd = '0';

    public function getUsuarioid()
    {
        return $this->usuarioid;
    }

    public function getRecordarme()
    {
        return $this->recordarme;
    }

    public function setRecordarme(bool $recordarme)
    {
        $this->recordarme = $recordarme;

        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail(string $email)
    {
        $this->email = $email;

        return $this;
    }

    public function getPwd()
    {
        return $this->pwd;
    }

    public function setPwd(string $pwd)
    {
        $this->pwd = $pwd;

        return $this;
    }


}
