<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: Vitrinas Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="vitrinas")
 * @ORM\Entity
 */
class Vitrinas
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="id_tipo", type="integer", nullable=false)
     */
    private $idTipo;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=150, nullable=false)
     */
    private $titulo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="detalle", type="string", length=100, nullable=true)
     */
    private $detalle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="urlImagen", type="string", length=500, nullable=true)
     */
    private $urlimagen;

    /**
     * @var string|null
     *
     * @ORM\Column(name="urlBaseDestino", type="string", length=500, nullable=true)
     */
    private $urlbasedestino;

    /**
     * @var string|null
     *
     * @ORM\Column(name="urlVIdeo", type="string", length=500, nullable=true)
     */
    private $urlvideo;

    /**
     * @var int
     *
     * @ORM\Column(name="posicion", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $posicion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vigente", type="string", length=1, nullable=true)
     */
    private $vigente;

    /**
     * @var string|null
     *
     * @ORM\Column(name="esPelicula", type="string", length=1, nullable=true)
     */
    private $espelicula;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaInicio", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $fechainicio = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaFin", type="datetime", nullable=false, options={"default"="0000-00-00 00:00:00"})
     */
    private $fechafin = '0000-00-00 00:00:00';

    public function getId()
    {
        return $this->id;
    }

    public function getIdTipo()
    {
        return $this->idTipo;
    }

    public function setIdTipo(int $idTipo)
    {
        $this->idTipo = $idTipo;

        return $this;
    }

    public function getTitulo()
    {
        return $this->titulo;
    }

    public function setTitulo(string $titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    public function getDetalle()
    {
        return $this->detalle;
    }

    public function setDetalle(string $detalle)
    {
        $this->detalle = $detalle;

        return $this;
    }

    public function getUrlimagen()
    {
        return $this->urlimagen;
    }

    public function setUrlimagen(string $urlimagen)
    {
        $this->urlimagen = $urlimagen;

        return $this;
    }

    public function getUrlbasedestino()
    {
        return $this->urlbasedestino;
    }

    public function setUrlbasedestino(string $urlbasedestino)
    {
        $this->urlbasedestino = $urlbasedestino;

        return $this;
    }

    public function getUrlvideo()
    {
        return $this->urlvideo;
    }

    public function setUrlvideo(string $urlvideo)
    {
        $this->urlvideo = $urlvideo;

        return $this;
    }

    public function getPosicion()
    {
        return $this->posicion;
    }

    public function setPosicion(int $posicion)
    {
        $this->posicion = $posicion;

        return $this;
    }

    public function getVigente()
    {
        return $this->vigente;
    }

    public function setVigente(string $vigente)
    {
        $this->vigente = $vigente;

        return $this;
    }

    public function getEspelicula()
    {
        return $this->espelicula;
    }

    public function setEspelicula(string $espelicula)
    {
        $this->espelicula = $espelicula;

        return $this;
    }

    public function getFechainicio()
    {
        return $this->fechainicio;
    }

    public function setFechainicio(\DateTimeInterface $fechainicio)
    {
        $this->fechainicio = $fechainicio;

        return $this;
    }

    public function getFechafin()
    {
        return $this->fechafin;
    }

    public function setFechafin(\DateTimeInterface $fechafin)
    {
        $this->fechafin = $fechafin;

        return $this;
    }


}
