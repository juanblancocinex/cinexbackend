<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: Localizadoressinec Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="localizadoresSinec")
 * @ORM\Entity
 */
class Localizadoressinec
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="userID", type="string", length=50, nullable=true)
     */
    private $userid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="localizador", type="string", length=10, nullable=true)
     */
    private $localizador;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=true)
     */
    private $fecha;

    /**
     * @var string|null
     *
     * @ORM\Column(name="valido", type="string", length=1, nullable=true)
     */
    private $valido;

    public function getId()
    {
        return $this->id;
    }

    public function getUserid()
    {
        return $this->userid;
    }

    public function setUserid(string $userid)
    {
        $this->userid = $userid;

        return $this;
    }

    public function getLocalizador()
    {
        return $this->localizador;
    }

    public function setLocalizador(string $localizador)
    {
        $this->localizador = $localizador;

        return $this;
    }

    public function getFecha()
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getValido()
    {
        return $this->valido;
    }

    public function setValido(string $valido)
    {
        $this->valido = $valido;

        return $this;
    }


}
