<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
  * Summary.
 *
 * Description:  CmsUsuarios Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="cms_usuarios")
 * @ORM\Entity
 */
class CmsUsuarios
{
    /**
     * @var int
     *
     * @ORM\Column(name="usuario_id", type="integer", nullable=false, options={"comment"="id del usuario"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $usuarioId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="perfil_id", type="integer", nullable=true, options={"comment"="id del perfil"})
     */
    private $perfilId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="usuario_nombres", type="string", length=255, nullable=true, options={"comment"="nombres"})
     */
    private $usuarioNombres;

    /**
     * @var string|null
     *
     * @ORM\Column(name="usuario_apellidos", type="string", length=255, nullable=true, options={"comment"="apellidos"})
     */
    private $usuarioApellidos;

    /**
     * @var string|null
     *
     * @ORM\Column(name="usuario_username", type="string", length=255, nullable=true, options={"comment"="password"})
     */
    private $usuarioUsername;

    /**
     * @var string|null
     *
     * @ORM\Column(name="usuario_pwd", type="string", length=255, nullable=true, options={"comment"="password"})
     */
    private $usuarioPwd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="usuario_activo", type="string", length=1, nullable=true, options={"default"="S","fixed"=true,"comment"="S=ACTIVO;N=INACTIVO"})
     */
    private $usuarioActivo = 'S';

    public function getUsuarioId()
    {
        return $this->usuarioId;
    }

    public function getPerfilId()
    {
        return $this->perfilId;
    }

    public function setPerfilId(int $perfilId)
    {
        $this->perfilId = $perfilId;

        return $this;
    }

    public function getUsuarioNombres()
    {
        return $this->usuarioNombres;
    }

    public function setUsuarioNombres(string $usuarioNombres)
    {
        $this->usuarioNombres = $usuarioNombres;

        return $this;
    }

    public function getUsuarioApellidos()
    {
        return $this->usuarioApellidos;
    }

    public function setUsuarioApellidos(string $usuarioApellidos)
    {
        $this->usuarioApellidos = $usuarioApellidos;

        return $this;
    }

    public function getUsuarioUsername()
    {
        return $this->usuarioUsername;
    }

    public function setUsuarioUsername(string $usuarioUsername)
    {
        $this->usuarioUsername = $usuarioUsername;

        return $this;
    }

    public function getUsuarioPwd()
    {
        return $this->usuarioPwd;
    }

    public function setUsuarioPwd(string $usuarioPwd)
    {
        $this->usuarioPwd = $usuarioPwd;

        return $this;
    }

    public function getUsuarioActivo()
    {
        return $this->usuarioActivo;
    }

    public function setUsuarioActivo(string $usuarioActivo)
    {
        $this->usuarioActivo = $usuarioActivo;

        return $this;
    }


}
