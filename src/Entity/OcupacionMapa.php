<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: OcupacionMapa Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="ocupacion_mapa")
 * @ORM\Entity(repositoryClass="App\Repository\OcupacionMapaRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class OcupacionMapa
{
    /**
     * @var int
     *
     * @ORM\Column(name="ocup_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $ocupId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="usuarioID", type="integer", nullable=true)
     */
    private $usuarioid;

    /**
     * @var int|null
     *
     * @ORM\Column(name="sesionID", type="integer", nullable=true)
     */
    private $sesionid;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ocup_fila", type="integer", nullable=true)
     */
    private $ocupFila;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ocup_columna", type="integer", nullable=true)
     */
    private $ocupColumna;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ocup_posicion", type="string", length=3, nullable=true)
     */
    private $ocupPosicion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ocup_ticket_type_code", type="string", length=255, nullable=true)
     */
    private $ocupTicketTypeCode;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ocup_ticket_price", type="integer", nullable=true)
     */
    private $ocupTicketPrice;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ocup_ticket_type", type="string", length=255, nullable=true)
     */
    private $ocupTicketType;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="ocup_fecha", type="date", nullable=true)
     */
    private $ocupFecha;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="ocup_hora", type="time", nullable=true)
     */
    private $ocupHora;

    public function getOcupId()
    {
        return $this->ocupId;
    }

    public function getUsuarioid()
    {
        return $this->usuarioid;
    }

    public function setUsuarioid(int $usuarioid)
    {
        $this->usuarioid = $usuarioid;

        return $this;
    }

    public function getSesionid()
    {
        return $this->sesionid;
    }

    public function setSesionid(int $sesionid)
    {
        $this->sesionid = $sesionid;

        return $this;
    }

    public function getOcupFila()
    {
        return $this->ocupFila;
    }

    public function setOcupFila(int $ocupFila)
    {
        $this->ocupFila = $ocupFila;

        return $this;
    }

    public function getOcupColumna()
    {
        return $this->ocupColumna;
    }

    public function setOcupColumna(int $ocupColumna)
    {
        $this->ocupColumna = $ocupColumna;

        return $this;
    }

    public function getOcupPosicion()
    {
        return $this->ocupPosicion;
    }

    public function setOcupPosicion(string $ocupPosicion)
    {
        $this->ocupPosicion = $ocupPosicion;

        return $this;
    }

    public function getOcupTicketTypeCode()
    {
        return $this->ocupTicketTypeCode;
    }

    public function setOcupTicketTypeCode(string $ocupTicketTypeCode)
    {
        $this->ocupTicketTypeCode = $ocupTicketTypeCode;

        return $this;
    }

    public function getOcupTicketPrice()
    {
        return $this->ocupTicketPrice;
    }

    public function setOcupTicketPrice(int $ocupTicketPrice)
    {
        $this->ocupTicketPrice = $ocupTicketPrice;

        return $this;
    }

    public function getOcupTicketType()
    {
        return $this->ocupTicketType;
    }

    public function setOcupTicketType(string $ocupTicketType)
    {
        $this->ocupTicketType = $ocupTicketType;

        return $this;
    }

    public function getOcupFecha()
    {
        return $this->ocupFecha;
    }
  
    
    /**
     * @ORM\PrePersist
     */
    public function setOcupFecha() 
    {
        $this->ocupFecha = new \DateTime();

        return $this;
    }

    public function getOcupHora()
    {
        return $this->ocupHora;
    }
    
    /**
     * @ORM\PrePersist
     */
    public function setOcupHora()
    {
        $this->ocupHora = new \DateTime();

        return $this;
    }


}
