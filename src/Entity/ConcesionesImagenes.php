<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Summary.
 *
 * Description:  ConcesionesImagenes Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="concesiones_imagenes")
 * @ORM\Entity
 */
class ConcesionesImagenes
{
    /**
     * @var int
     *
     * @ORM\Column(name="img_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $imgId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="con_id", type="integer", nullable=true)
     */
    private $conId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="con_imagen", type="blob", length=16777215, nullable=true)
     */
    private $conImagen;

    public function getImgId()
    {
        return $this->imgId;
    }

    public function getConId()
    {
        return $this->conId;
    }

    public function setConId(int $conId)
    {
        $this->conId = $conId;

        return $this;
    }

    public function getConImagen()
    {
        return $this->conImagen;
    }

    public function setConImagen($conImagen)
    {
        $this->conImagen = $conImagen;

        return $this;
    }


}
