<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description:  Counter Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="counter")
 * @ORM\Entity
 */
class Counter
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="counter_counter", type="integer", nullable=true)
     */
    private $counterCounter;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="counter_date", type="datetime", nullable=true)
     */
    private $counterDate;

    public function getId()
    {
        return $this->id;
    }

    public function getCounterCounter()
    {
        return $this->counterCounter;
    }

    public function setCounterCounter(int $counterCounter)
    {
        $this->counterCounter = $counterCounter;

        return $this;
    }

    public function getCounterDate()
    {
        return $this->counterDate;
    }

    public function setCounterDate(\DateTimeInterface $counterDate)
    {
        $this->counterDate = $counterDate;

        return $this;
    }


}
