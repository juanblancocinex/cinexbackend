<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: UsuarioStatus Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="usuario_status")
 * @ORM\Entity
 */
class UsuarioStatus
{
    /**
     * @var int
     *
     * @ORM\Column(name="usuarioID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $usuarioid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="status", type="string", length=1, nullable=true, options={"default"="S","fixed"=true})
     */
    private $status = 'S';

    public function getUsuarioid()
    {
        return $this->usuarioid;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus(string $status)
    {
        $this->status = $status;

        return $this;
    }


}
