<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: PreciosConcesiones Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="precios_concesiones")
 * @ORM\Entity
 */
class PreciosConcesiones
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="complejo", type="string", length=3, nullable=false)
     */
    private $complejo;

    /**
     * @var string
     *
     * @ORM\Column(name="categoria", type="string", length=25, nullable=false)
     */
    private $categoria;

    /**
     * @var int
     *
     * @ORM\Column(name="itemid", type="integer", nullable=false)
     */
    private $itemid;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=100, nullable=false)
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="precio", type="decimal", precision=18, scale=2, nullable=false)
     */
    private $precio;

    /**
     * @var string
     *
     * @ORM\Column(name="lista", type="string", length=100, nullable=false)
     */
    private $lista;

    /**
     * @var int
     *
     * @ORM\Column(name="id_lista", type="integer", nullable=false)
     */
    private $idLista;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha_creacion", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $fechaCreacion = 'CURRENT_TIMESTAMP';

    public function getId()
    {
        return $this->id;
    }

    public function getComplejo()
    {
        return $this->complejo;
    }

    public function setComplejo(string $complejo)
    {
        $this->complejo = $complejo;

        return $this;
    }

    public function getCategoria()
    {
        return $this->categoria;
    }

    public function setCategoria(string $categoria)
    {
        $this->categoria = $categoria;

        return $this;
    }

    public function getItemid()
    {
        return $this->itemid;
    }

    public function setItemid(int $itemid)
    {
        $this->itemid = $itemid;

        return $this;
    }

    public function getDescripcion()
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getPrecio()
    {
        return $this->precio;
    }

    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    public function getLista()
    {
        return $this->lista;
    }

    public function setLista(string $lista)
    {
        $this->lista = $lista;

        return $this;
    }

    public function getIdLista()
    {
        return $this->idLista;
    }

    public function setIdLista(int $idLista)
    {
        $this->idLista = $idLista;

        return $this;
    }

    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    public function setFechaCreacion(\DateTimeInterface $fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }


}
