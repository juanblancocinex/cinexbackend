<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
  *  Summary.
 *
 * Description: TipoImagen Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="tipo_imagen", indexes={@ORM\Index(name="fk_tipo_imagen_tipo", columns={"id_tipo"}), @ORM\Index(name="fk_tipo_imagen_imagen", columns={"id_imagen"})})
 * @ORM\Entity
 */
class TipoImagen
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \ImagenProducto
     *
     * @ORM\ManyToOne(targetEntity="ImagenProducto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_imagen", referencedColumnName="id")
     * })
     */
    private $idImagen;

    /**
     * @var \Tipo
     *
     * @ORM\ManyToOne(targetEntity="Tipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tipo", referencedColumnName="id")
     * })
     */
    private $idTipo;

    public function getId()
    {
        return $this->id;
    }

    public function getIdImagen()
    {
        return $this->idImagen;
    }

    public function setIdImagen(ImagenProducto $idImagen)
    {
        $this->idImagen = $idImagen;

        return $this;
    }

    public function getIdTipo()
    {
        return $this->idTipo;
    }

    public function setIdTipo(Tipo $idTipo)
    {
        $this->idTipo = $idTipo;

        return $this;
    }


}
