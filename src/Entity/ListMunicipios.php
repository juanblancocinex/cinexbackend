<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: ListMunicipios Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="list_municipios")
 * @ORM\Entity
 */
class ListMunicipios
{
    /**
     * @var int
     *
     * @ORM\Column(name="listm_id", type="integer", nullable=false, options={"comment"="ID de los Municipios"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $listmId;

    /**
     * @var string
     *
     * @ORM\Column(name="listm_codigo", type="string", length=4, nullable=false, options={"fixed"=true,"comment"="Codigo del Municipio"})
     */
    private $listmCodigo;

    /**
     * @var string
     *
     * @ORM\Column(name="listm_nombre", type="string", length=60, nullable=false, options={"comment"="Nombre del Municipio"})
     */
    private $listmNombre;

    /**
     * @var int
     *
     * @ORM\Column(name="listm_relacion", type="integer", nullable=false, options={"comment"="ID de los Estados"})
     */
    private $listmRelacion;

    public function getListmId()
    {
        return $this->listmId;
    }

    public function getListmCodigo()
    {
        return $this->listmCodigo;
    }

    public function setListmCodigo(string $listmCodigo)
    {
        $this->listmCodigo = $listmCodigo;

        return $this;
    }

    public function getListmNombre()
    {
        return $this->listmNombre;
    }

    public function setListmNombre(string $listmNombre)
    {
        $this->listmNombre = $listmNombre;

        return $this;
    }

    public function getListmRelacion()
    {
        return $this->listmRelacion;
    }

    public function setListmRelacion(int $listmRelacion)
    {
        $this->listmRelacion = $listmRelacion;

        return $this;
    }


}
