<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: Parroquias Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 *
 * @ORM\Table(name="parroquias", indexes={@ORM\Index(name="id_municipio", columns={"id_municipio"})})
  * @ORM\Entity(repositoryClass="App\Repository\ParroquiasRepository")
 */
class Parroquias
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_parroquia", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idParroquia;

    /**
     * @var string
     *
     * @ORM\Column(name="parroquia", type="string", length=250, nullable=false)
     */
    private $parroquia;

    /**
     * @var \Municipios
     *
     * @ORM\ManyToOne(targetEntity="Municipios", inversedBy="parroquias")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_municipio", referencedColumnName="id_municipio")
     * })
     */
    private $idMunicipio;

    public function getIdParroquia(): ?int
    {
        return $this->idParroquia;
    }

    public function getParroquia(): ?string
    {
        return $this->parroquia;
    }

    public function setParroquia(string $parroquia): self
    {
        $this->parroquia = $parroquia;

        return $this;
    }

    public function getIdMunicipio(): ?Municipios
    {
        return $this->idMunicipio;
    }

    public function setIdMunicipio(?Municipios $idMunicipio): self
    {
        $this->idMunicipio = $idMunicipio;

        return $this;
    }


    


}
