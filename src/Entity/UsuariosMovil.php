<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
*  Summary.
 *
 * Description: UsuariosMovil Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="usuarios_movil", uniqueConstraints={@ORM\UniqueConstraint(name="id", columns={"id"}), @ORM\UniqueConstraint(name="id_2", columns={"id"})})
 * @ORM\Entity
 */
class UsuariosMovil
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="text", length=65535, nullable=false)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="apellido", type="text", length=65535, nullable=false)
     */
    private $apellido;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="text", length=65535, nullable=false)
     */
    private $telefono;

    /**
     * @var string
     *
     * @ORM\Column(name="correo", type="text", length=65535, nullable=false)
     */
    private $correo;

    /**
     * @var string
     *
     * @ORM\Column(name="tecnologia", type="text", length=65535, nullable=false)
     */
    private $tecnologia;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="emailListaMovil", type="boolean", nullable=true)
     */
    private $emaillistamovil = '0';

    public function getId()
    {
        return $this->id;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getApellido()
    {
        return $this->apellido;
    }

    public function setApellido(string $apellido)
    {
        $this->apellido = $apellido;

        return $this;
    }

    public function getTelefono()
    {
        return $this->telefono;
    }

    public function setTelefono(string $telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    public function getCorreo()
    {
        return $this->correo;
    }

    public function setCorreo(string $correo)
    {
        $this->correo = $correo;

        return $this;
    }

    public function getTecnologia()
    {
        return $this->tecnologia;
    }

    public function setTecnologia(string $tecnologia)
    {
        $this->tecnologia = $tecnologia;

        return $this;
    }

    public function getEmaillistamovil()
    {
        return $this->emaillistamovil;
    }

    public function setEmaillistamovil(bool $emaillistamovil)
    {
        $this->emaillistamovil = $emaillistamovil;

        return $this;
    }


}
