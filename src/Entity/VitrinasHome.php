<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: VitrinasHome Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="vitrinas_home")
 * @ORM\Entity
 */
class VitrinasHome
{
    /**
     * @var int
     *
     * @ORM\Column(name="vitrina_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $vitrinaId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vitrina_nombre", type="string", length=255, nullable=true)
     */
    private $vitrinaNombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vitrina_url", type="string", length=255, nullable=true)
     */
    private $vitrinaUrl;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vitrina_link", type="string", length=255, nullable=true)
     */
    private $vitrinaLink;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vitrina_trailer", type="string", length=255, nullable=true)
     */
    private $vitrinaTrailer;

    /**
     * @var int|null
     *
     * @ORM\Column(name="vitrina_orden", type="integer", nullable=true)
     */
    private $vitrinaOrden = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="vitrina_activo", type="string", length=1, nullable=true, options={"default"="S","fixed"=true})
     */
    private $vitrinaActivo = 'S';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="vitrina_fechahora", type="datetime", nullable=true)
     */
    private $vitrinaFechahora;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vitrina_target", type="string", length=255, nullable=true)
     */
    private $vitrinaTarget;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vitrina_pelicula_versiones", type="string", length=255, nullable=true)
     */
    private $vitrinaPeliculaVersiones;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vitrina_pelicula_trailer", type="string", length=255, nullable=true)
     */
    private $vitrinaPeliculaTrailer;

    public function getVitrinaId()
    {
        return $this->vitrinaId;
    }

    public function getVitrinaNombre()
    {
        return $this->vitrinaNombre;
    }

    public function setVitrinaNombre(string $vitrinaNombre)
    {
        $this->vitrinaNombre = $vitrinaNombre;

        return $this;
    }

    public function getVitrinaUrl()
    {
        return $this->vitrinaUrl;
    }

    public function setVitrinaUrl(string $vitrinaUrl)
    {
        $this->vitrinaUrl = $vitrinaUrl;

        return $this;
    }

    public function getVitrinaLink()
    {
        return $this->vitrinaLink;
    }

    public function setVitrinaLink(string $vitrinaLink)
    {
        $this->vitrinaLink = $vitrinaLink;

        return $this;
    }

    public function getVitrinaTrailer()
    {
        return $this->vitrinaTrailer;
    }

    public function setVitrinaTrailer(string $vitrinaTrailer)
    {
        $this->vitrinaTrailer = $vitrinaTrailer;

        return $this;
    }

    public function getVitrinaOrden()
    {
        return $this->vitrinaOrden;
    }

    public function setVitrinaOrden(int $vitrinaOrden)
    {
        $this->vitrinaOrden = $vitrinaOrden;

        return $this;
    }

    public function getVitrinaActivo()
    {
        return $this->vitrinaActivo;
    }

    public function setVitrinaActivo(string $vitrinaActivo)
    {
        $this->vitrinaActivo = $vitrinaActivo;

        return $this;
    }

    public function getVitrinaFechahora()
    {
        return $this->vitrinaFechahora;
    }

    public function setVitrinaFechahora(\DateTimeInterface $vitrinaFechahora)
    {
        $this->vitrinaFechahora = $vitrinaFechahora;

        return $this;
    }

    public function getVitrinaTarget()
    {
        return $this->vitrinaTarget;
    }

    public function setVitrinaTarget(string $vitrinaTarget)
    {
        $this->vitrinaTarget = $vitrinaTarget;

        return $this;
    }

    public function getVitrinaPeliculaVersiones()
    {
        return $this->vitrinaPeliculaVersiones;
    }

    public function setVitrinaPeliculaVersiones(string $vitrinaPeliculaVersiones)
    {
        $this->vitrinaPeliculaVersiones = $vitrinaPeliculaVersiones;

        return $this;
    }

    public function getVitrinaPeliculaTrailer()
    {
        return $this->vitrinaPeliculaTrailer;
    }

    public function setVitrinaPeliculaTrailer(string $vitrinaPeliculaTrailer)
    {
        $this->vitrinaPeliculaTrailer = $vitrinaPeliculaTrailer;

        return $this;
    }


}
