<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: PeliculaDetalles Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="pelicula_detalles")
 * @ORM\Entity
 */
class PeliculaDetalles
{
    /**
     * @var int
     *
     * @ORM\Column(name="peliculaID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $peliculaid;

    /**
     * @var string
     *
     * @ORM\Column(name="esPreVenta", type="string", length=1, nullable=false)
     */
    private $espreventa;

    /**
     * @var string
     *
     * @ORM\Column(name="esVenezolana", type="string", length=1, nullable=false)
     */
    private $esvenezolana;

    /**
     * @var string
     *
     * @ORM\Column(name="esRestreno", type="string", length=1, nullable=false)
     */
    private $esrestreno;

    /**
     * @var string
     *
     * @ORM\Column(name="codVista", type="string", length=20, nullable=false)
     */
    private $codvista;

    public function getPeliculaid()
    {
        return $this->peliculaid;
    }

    public function getEspreventa()
    {
        return $this->espreventa;
    }

    public function setEspreventa(string $espreventa)
    {
        $this->espreventa = $espreventa;

        return $this;
    }

    public function getEsvenezolana()
    {
        return $this->esvenezolana;
    }

    public function setEsvenezolana(string $esvenezolana)
    {
        $this->esvenezolana = $esvenezolana;

        return $this;
    }

    public function getEsrestreno()
    {
        return $this->esrestreno;
    }

    public function setEsrestreno(string $esrestreno)
    {
        $this->esrestreno = $esrestreno;

        return $this;
    }

    public function getCodvista()
    {
        return $this->codvista;
    }

    public function setCodvista(string $codvista)
    {
        $this->codvista = $codvista;

        return $this;
    }


}
