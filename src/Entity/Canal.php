<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Summary.
 *
 * Description:  Canal Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="canal")
 * @ORM\Entity
 */
class Canal
{
    /**
     * @var int
     *
     * @ORM\Column(name="canalID", type="integer", nullable=false, options={"unsigned"=true,"comment"="identificador de la ciudad"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $canalid;

    /**
     * @var string
     *
     * @ORM\Column(name="nombreCanal", type="string", length=50, nullable=false, options={"comment"="nombre de la ciudad"})
     */
    private $nombrecanal;

    /**
     * @var string
     *
     * @ORM\Column(name="abreviatura", type="string", length=3, nullable=false, options={"comment"="nombre de la ciudad"})
     */
    private $abreviatura;

    public function getCanalid()
    {
        return $this->canalid;
    }

    public function getNombrecanal()
    {
        return $this->nombrecanal;
    }

    public function setNombrecanal(string $nombrecanal): self
    {
        $this->nombrecanal = $nombrecanal;

        return $this;
    }

    public function getAbreviatura()
    {
        return $this->abreviatura;
    }

    public function setAbreviatura(string $abreviatura): self
    {
        $this->abreviatura = $abreviatura;

        return $this;
    }


}
