<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
  *  Summary.
 *
 * Description:  VouchersDigitales Entity
 *
 * @since 1.0
 * @author Sir Sarmiento <ssarmiento@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="vouchers_digitales")
 * @ORM\Entity(repositoryClass="App\Repository\VouchersDigitalesRepository")
 */
class VouchersDigitales
{
    /**
     * @var int
     *
     * @ORM\Column(name="idvouchers_digitales", type="integer", nullable=false, options={"comment"="id de la tabla"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idvouchersDigitales;

    /**
     * @var int
     *
     * @ORM\Column(name="codigo_voucher", type="bigint", nullable=false, options={"comment"="codigo del voucher a redimir"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $codigoVoucher;

    /**
     * @var int
     *
     * @ORM\Column(name="id_plantilla", type="integer", nullable=false, options={"comment"="id de la plantilla que va a utilizar para emsamblar el html"})
     */
    private $idPlantilla;

    /**
     * @var int
     *
     * @ORM\Column(name="id_usuario", type="integer", nullable=false, options={"comment"="id del usuario que se le asignaron los vouchers"})
     */
    private $idUsuario;

    /**
     * @var int
     *
     * @ORM\Column(name="id_cliente", type="integer", nullable=false, options={"comment"="id de la empresa a la cual pertenece"})
     */
    private $idCliente;

    /**
     * @var int|null
     *
     * @ORM\Column(name="cod_tipo_voucher", type="integer", nullable=true)
     */
    private $codTipoVoucher;

    /**
     * @var int|null
     *
     * @ORM\Column(name="mes_asignacion", type="integer", nullable=true, options={"comment"="mes en el cual aplica el vouchers"})
     */
    private $mesAsignacion;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ano_asignacion", type="integer", nullable=true, options={"comment"="año en el cual aplica el vouchers"})
     */
    private $anoAsignacion;

    /**
     * @var string
     *
     * @ORM\Column(name="nb_cliente", type="string", length=100, nullable=false, options={"comment"="Nombre del Cliente"})
     */
    private $nbCliente;

    /**
     * @var int
     *
     * @ORM\Column(name="id_campana", type="integer", nullable=false, options={"comment"="id de la campaña a la cual pertenecen los vouchers"})
     */
    private $idCampana;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_vencimiento", type="datetime", nullable=false, options={"comment"="fecha de vencimiento del vouchers"})
     */
    private $fechaVencimiento;

    /**
     * @var bool
     *
     * @ORM\Column(name="redimido", type="boolean", nullable=false, options={"comment"="indica si el voucher fue redimido o no"})
     */
    private $redimido;

    /**
     * @var string|null
     *
     * @ORM\Column(name="medio_redencion", type="string", length=15, nullable=true, options={"comment"="medios por el cual se redimio (www, pos, atm)"})
     */
    private $medioRedencion;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha_redencion", type="datetime", nullable=true, options={"comment"="fecha en la cual se redimio el vouchers"})
     */
    private $fechaRedencion;

    /**
     * @var bool
     *
     * @ORM\Column(name="activo", type="boolean", nullable=false, options={"comment"="si esta activo o inactivo el registro"})
     */
    private $activo;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha_crea", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $fechaCrea = 'CURRENT_TIMESTAMP';

    /**
     * @var string|null
     *
     * @ORM\Column(name="categoria_id", type="string", length=45, nullable=true)
     */
    private $categoriaId;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="manual", type="boolean", nullable=true)
     */
    private $manual;

    /**
     * @var int|null
     *
     * @ORM\Column(name="transaccionID", type="integer", nullable=true)
     */
    private $transaccionid;

    public function getIdvouchersDigitales()
    {
        return $this->idvouchersDigitales;
    }

    public function getCodigoVoucher()
    {
        return $this->codigoVoucher;
    }

    public function getIdPlantilla()
    {
        return $this->idPlantilla;
    }

    public function setIdPlantilla(int $idPlantilla)
    {
        $this->idPlantilla = $idPlantilla;

        return $this;
    }

    public function getIdUsuario()
    {
        return $this->idUsuario;
    }

    public function setIdUsuario(int $idUsuario)
    {
        $this->idUsuario = $idUsuario;

        return $this;
    }

    public function getIdCliente()
    {
        return $this->idCliente;
    }

    public function setIdCliente(int $idCliente)
    {
        $this->idCliente = $idCliente;

        return $this;
    }

    public function getCodTipoVoucher()
    {
        return $this->codTipoVoucher;
    }

    public function setCodTipoVoucher(int $codTipoVoucher)
    {
        $this->codTipoVoucher = $codTipoVoucher;

        return $this;
    }

    public function getMesAsignacion()
    {
        return $this->mesAsignacion;
    }

    public function setMesAsignacion(int $mesAsignacion)
    {
        $this->mesAsignacion = $mesAsignacion;

        return $this;
    }

    public function getAnoAsignacion()
    {
        return $this->anoAsignacion;
    }

    public function setAnoAsignacion(int $anoAsignacion)
    {
        $this->anoAsignacion = $anoAsignacion;

        return $this;
    }

    public function getNbCliente()
    {
        return $this->nbCliente;
    }

    public function setNbCliente(string $nbCliente)
    {
        $this->nbCliente = $nbCliente;

        return $this;
    }

    public function getIdCampana()
    {
        return $this->idCampana;
    }

    public function setIdCampana(int $idCampana)
    {
        $this->idCampana = $idCampana;

        return $this;
    }

    public function getFechaVencimiento()
    {
        return $this->fechaVencimiento;
    }

    public function setFechaVencimiento(\DateTimeInterface $fechaVencimiento)
    {
        $this->fechaVencimiento = $fechaVencimiento;

        return $this;
    }

    public function getRedimido()
    {
        return $this->redimido;
    }

    public function setRedimido(bool $redimido)
    {
        $this->redimido = $redimido;

        return $this;
    }

    public function getMedioRedencion()
    {
        return $this->medioRedencion;
    }

    public function setMedioRedencion(string $medioRedencion)
    {
        $this->medioRedencion = $medioRedencion;

        return $this;
    }

    public function getFechaRedencion()
    {
        return $this->fechaRedencion;
    }

    public function setFechaRedencion(\DateTimeInterface $fechaRedencion)
    {
        $this->fechaRedencion = $fechaRedencion;

        return $this;
    }

    public function getActivo()
    {
        return $this->activo;
    }

    public function setActivo(bool $activo)
    {
        $this->activo = $activo;

        return $this;
    }

    public function getFechaCrea()
    {
        return $this->fechaCrea;
    }

    public function setFechaCrea(\DateTimeInterface $fechaCrea)
    {
        $this->fechaCrea = $fechaCrea;

        return $this;
    }

    public function getCategoriaId()
    {
        return $this->categoriaId;
    }

    public function setCategoriaId(string $categoriaId)
    {
        $this->categoriaId = $categoriaId;

        return $this;
    }

    public function getManual()
    {
        return $this->manual;
    }

    public function setManual(bool $manual)
    {
        $this->manual = $manual;

        return $this;
    }

    public function getTransaccionid()
    {
        return $this->transaccionid;
    }

    public function setTransaccionid(int $transaccionid)
    {
        $this->transaccionid = $transaccionid;

        return $this;
    }


}
