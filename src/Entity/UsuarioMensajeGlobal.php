<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: UsuarioMensajeGlobal Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="usuario_mensaje_global")
 * @ORM\Entity
 */
class UsuarioMensajeGlobal
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="usuarioID", type="integer", nullable=true)
     */
    private $usuarioid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="mensaje_id", type="string", length=45, nullable=true)
     */
    private $mensajeId;

    public function getId()
    {
        return $this->id;
    }

    public function getUsuarioid()
    {
        return $this->usuarioid;
    }

    public function setUsuarioid(int $usuarioid)
    {
        $this->usuarioid = $usuarioid;

        return $this;
    }

    public function getMensajeId()
    {
        return $this->mensajeId;
    }

    public function setMensajeId(string $mensajeId)
    {
        $this->mensajeId = $mensajeId;

        return $this;
    }


}
