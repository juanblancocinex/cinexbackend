<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
  *  Summary.
 *
 * Description: ImagenesFondo Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="imagenes_fondo")
 * @ORM\Entity
 */
class ImagenesFondo
{
    /**
     * @var int
     *
     * @ORM\Column(name="img_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $imgId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="img_nombre", type="string", length=255, nullable=true)
     */
    private $imgNombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="img_img", type="string", length=255, nullable=true)
     */
    private $imgImg;

    /**
     * @var string|null
     *
     * @ORM\Column(name="img_activo", type="string", length=1, nullable=true, options={"default"="S","fixed"=true})
     */
    private $imgActivo = 'S';

    /**
     * @var string|null
     *
     * @ORM\Column(name="img_bgcolor", type="string", length=45, nullable=true, options={"default"="#04113b"})
     */
    private $imgBgcolor = '#04113b';

    public function getImgId()
    {
        return $this->imgId;
    }

    public function getImgNombre()
    {
        return $this->imgNombre;
    }

    public function setImgNombre(string $imgNombre)
    {
        $this->imgNombre = $imgNombre;

        return $this;
    }

    public function getImgImg()
    {
        return $this->imgImg;
    }

    public function setImgImg(string $imgImg)
    {
        $this->imgImg = $imgImg;

        return $this;
    }

    public function getImgActivo()
    {
        return $this->imgActivo;
    }

    public function setImgActivo(string $imgActivo)
    {
        $this->imgActivo = $imgActivo;

        return $this;
    }

    public function getImgBgcolor()
    {
        return $this->imgBgcolor;
    }

    public function setImgBgcolor(string $imgBgcolor)
    {
        $this->imgBgcolor = $imgBgcolor;

        return $this;
    }


}
