<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: PeliculaCensura Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 *
 * @ORM\Table(name="pelicula_censura")
 * @ORM\Entity
 */
class PeliculaCensura
{
    /**
     * @var int
     *
     * @ORM\Column(name="detalleID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $detalleid;

    /**
     * @var string
     *
     * @ORM\Column(name="complejoID", type="string", length=3, nullable=false)
     */
    private $complejoid;

    /**
     * @var string
     *
     * @ORM\Column(name="censura", type="string", length=3, nullable=false)
     */
    private $censura;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=15, nullable=false)
     */
    private $descripcion;

    /**
     * @var int
     *
     * @ORM\Column(name="orden", type="integer", nullable=false)
     */
    private $orden;

    public function getDetalleid()
    {
        return $this->detalleid;
    }

    public function getComplejoid()
    {
        return $this->complejoid;
    }

    public function setComplejoid(string $complejoid)
    {
        $this->complejoid = $complejoid;

        return $this;
    }

    public function getCensura()
    {
        return $this->censura;
    }

    public function setCensura(string $censura)
    {
        $this->censura = $censura;

        return $this;
    }

    public function getDescripcion()
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getOrden()
    {
        return $this->orden;
    }

    public function setOrden(int $orden)
    {
        $this->orden = $orden;

        return $this;
    }


}
