<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: Publicidad Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="publicidad")
 * @ORM\Entity
 */
class Publicidad
{
    /**
     * @var int
     *
     * @ORM\Column(name="publicidadID", type="integer", nullable=false, options={"unsigned"=true,"comment"="identificador de la ciudad"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $publicidadid;

    /**
     * @var string
     *
     * @ORM\Column(name="nombreSeccion", type="string", length=50, nullable=false, options={"comment"="nombre de la ciudad"})
     */
    private $nombreseccion;

    /**
     * @var string
     *
     * @ORM\Column(name="URLImagen", type="string", length=250, nullable=false, options={"comment"="nombre de la ciudad"})
     */
    private $urlimagen;

    /**
     * @var string
     *
     * @ORM\Column(name="vigente", type="string", length=1, nullable=false, options={"comment"="nombre de la ciudad"})
     */
    private $vigente;

    /**
     * @var string|null
     *
     * @ORM\Column(name="url", type="string", length=100, nullable=true)
     */
    private $url;

    public function getPublicidadid()
    {
        return $this->publicidadid;
    }

    public function getNombreseccion()
    {
        return $this->nombreseccion;
    }

    public function setNombreseccion(string $nombreseccion)
    {
        $this->nombreseccion = $nombreseccion;

        return $this;
    }

    public function getUrlimagen()
    {
        return $this->urlimagen;
    }

    public function setUrlimagen(string $urlimagen)
    {
        $this->urlimagen = $urlimagen;

        return $this;
    }

    public function getVigente()
    {
        return $this->vigente;
    }

    public function setVigente(string $vigente)
    {
        $this->vigente = $vigente;

        return $this;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl(string $url)
    {
        $this->url = $url;

        return $this;
    }


}
