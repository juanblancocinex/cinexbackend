<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: PopupSite Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 *
 * @ORM\Table(name="popup_site")
 * @ORM\Entity
 */
class PopupSite
{
    /**
     * @var int
     *
     * @ORM\Column(name="popup_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $popupId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="popup_nombre", type="string", length=255, nullable=true)
     */
    private $popupNombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="popup_imagen", type="string", length=255, nullable=true)
     */
    private $popupImagen;

    /**
     * @var string|null
     *
     * @ORM\Column(name="popup_url", type="string", length=255, nullable=true)
     */
    private $popupUrl;

    /**
     * @var int|null
     *
     * @ORM\Column(name="popup_ancho", type="integer", nullable=true)
     */
    private $popupAncho = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="popup_alto", type="integer", nullable=true)
     */
    private $popupAlto = '0';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="popup_fecha", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $popupFecha = 'CURRENT_TIMESTAMP';

    /**
     * @var string|null
     *
     * @ORM\Column(name="popup_activo", type="string", length=1, nullable=true, options={"default"="S"})
     */
    private $popupActivo = 'S';

    /**
     * @var string|null
     *
     * @ORM\Column(name="invasivo", type="string", length=1, nullable=true, options={"default"="S"})
     */
    private $invasivo = 'S';

    /**
     * @var int|null
     *
     * @ORM\Column(name="popup_duracion", type="integer", nullable=true)
     */
    private $popupDuracion = '0';

    public function getPopupId()
    {
        return $this->popupId;
    }

    public function getPopupNombre()
    {
        return $this->popupNombre;
    }

    public function setPopupNombre(string $popupNombre)
    {
        $this->popupNombre = $popupNombre;

        return $this;
    }

    public function getPopupImagen()
    {
        return $this->popupImagen;
    }

    public function setPopupImagen(string $popupImagen)
    {
        $this->popupImagen = $popupImagen;

        return $this;
    }

    public function getPopupUrl()
    {
        return $this->popupUrl;
    }

    public function setPopupUrl(string $popupUrl)
    {
        $this->popupUrl = $popupUrl;

        return $this;
    }

    public function getPopupAncho()
    {
        return $this->popupAncho;
    }

    public function setPopupAncho(int $popupAncho)
    {
        $this->popupAncho = $popupAncho;

        return $this;
    }

    public function getPopupAlto()
    {
        return $this->popupAlto;
    }

    public function setPopupAlto(int $popupAlto)
    {
        $this->popupAlto = $popupAlto;

        return $this;
    }

    public function getPopupFecha()
    {
        return $this->popupFecha;
    }

    public function setPopupFecha(\DateTimeInterface $popupFecha)
    {
        $this->popupFecha = $popupFecha;

        return $this;
    }

    public function getPopupActivo()
    {
        return $this->popupActivo;
    }

    public function setPopupActivo(string $popupActivo)
    {
        $this->popupActivo = $popupActivo;

        return $this;
    }

    public function getInvasivo()
    {
        return $this->invasivo;
    }

    public function setInvasivo(string $invasivo)
    {
        $this->invasivo = $invasivo;

        return $this;
    }

    public function getPopupDuracion()
    {
        return $this->popupDuracion;
    }

    public function setPopupDuracion(int $popupDuracion)
    {
        $this->popupDuracion = $popupDuracion;

        return $this;
    }


}
