<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: Maestrocomida Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="maestroComida")
 * @ORM\Entity
 */
class Maestrocomida
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comida", type="string", length=45, nullable=true)
     */
    private $comida;

    public function getId()
    {
        return $this->id;
    }

    public function getComida()
    {
        return $this->comida;
    }

    public function setComida(string $comida)
    {
        $this->comida = $comida;

        return $this;
    }


}
