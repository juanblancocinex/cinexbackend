<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
  *  Summary.
 *
 * Description: ListEstado Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="list_estado")
 * @ORM\Entity
 */
class ListEstado
{
    /**
     * @var int
     *
     * @ORM\Column(name="liste_id", type="integer", nullable=false, options={"comment"="ID de los Estados"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $listeId;

    /**
     * @var string
     *
     * @ORM\Column(name="liste_codigo", type="string", length=5, nullable=false, options={"fixed"=true,"comment"="Codigo del Estado"})
     */
    private $listeCodigo;

    /**
     * @var string
     *
     * @ORM\Column(name="liste_nombre", type="string", length=40, nullable=false, options={"comment"="Estados"})
     */
    private $listeNombre;

    /**
     * @var string
     *
     * @ORM\Column(name="liste_relacion", type="string", length=3, nullable=false, options={"fixed"=true,"comment"="Codigo del Pais"})
     */
    private $listeRelacion;

    public function getListeId()
    {
        return $this->listeId;
    }

    public function getListeCodigo()
    {
        return $this->listeCodigo;
    }

    public function setListeCodigo(string $listeCodigo)
    {
        $this->listeCodigo = $listeCodigo;

        return $this;
    }

    public function getListeNombre()
    {
        return $this->listeNombre;
    }

    public function setListeNombre(string $listeNombre)
    {
        $this->listeNombre = $listeNombre;

        return $this;
    }

    public function getListeRelacion()
    {
        return $this->listeRelacion;
    }

    public function setListeRelacion(string $listeRelacion)
    {
        $this->listeRelacion = $listeRelacion;

        return $this;
    }


}
