<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
  *  Summary.
 *
 * Description: TrxDetalle Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="trx_detalle", indexes={@ORM\Index(name="fk_trx_detalle_1", columns={"id_tipo"}), @ORM\Index(name="fk_trx_detalle_2", columns={"id_voucher"}), @ORM\Index(name="fk_trx_detalle_3", columns={"id_trx"}), @ORM\Index(name="fk_trx_detalle_4", columns={"id_tipo", "id_voucher"})})
 * @ORM\Entity
 */
class TrxDetalle
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="id_tipo", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $idTipo;

    /**
     * @var int
     *
     * @ORM\Column(name="id_voucher", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $idVoucher;

    /**
     * @var int
     *
     * @ORM\Column(name="id_trx", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $idTrx;

    public function getId()
    {
        return $this->id;
    }

    public function getIdTipo()
    {
        return $this->idTipo;
    }

    public function setIdTipo(int $idTipo)
    {
        $this->idTipo = $idTipo;

        return $this;
    }

    public function getIdVoucher()
    {
        return $this->idVoucher;
    }

    public function setIdVoucher(int $idVoucher)
    {
        $this->idVoucher = $idVoucher;

        return $this;
    }

    public function getIdTrx()
    {
        return $this->idTrx;
    }

    public function setIdTrx(int $idTrx)
    {
        $this->idTrx = $idTrx;

        return $this;
    }


}
