<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: Pdf Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="pdf")
 * @ORM\Entity
 */
class Pdf
{
    /**
     * @var int
     *
     * @ORM\Column(name="pdf_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $pdfId;

    /**
     * @var string
     *
     * @ORM\Column(name="pdf_nombre", type="string", length=255, nullable=false)
     */
    private $pdfNombre;

    /**
     * @var string
     *
     * @ORM\Column(name="pdf_file", type="string", length=255, nullable=false)
     */
    private $pdfFile;

    /**
     * @var string
     *
     * @ORM\Column(name="pdf_seccion", type="string", length=255, nullable=false)
     */
    private $pdfSeccion;

    /**
     * @var string
     *
     * @ORM\Column(name="pdf_activo", type="string", length=1, nullable=false, options={"default"="S","fixed"=true})
     */
    private $pdfActivo = 'S';

    public function getPdfId()
    {
        return $this->pdfId;
    }

    public function getPdfNombre()
    {
        return $this->pdfNombre;
    }

    public function setPdfNombre(string $pdfNombre)
    {
        $this->pdfNombre = $pdfNombre;

        return $this;
    }

    public function getPdfFile()
    {
        return $this->pdfFile;
    }

    public function setPdfFile(string $pdfFile)
    {
        $this->pdfFile = $pdfFile;

        return $this;
    }

    public function getPdfSeccion()
    {
        return $this->pdfSeccion;
    }

    public function setPdfSeccion(string $pdfSeccion)
    {
        $this->pdfSeccion = $pdfSeccion;

        return $this;
    }

    public function getPdfActivo()
    {
        return $this->pdfActivo;
    }

    public function setPdfActivo(string $pdfActivo)
    {
        $this->pdfActivo = $pdfActivo;

        return $this;
    }


}
