<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Symfony\Component\Security\Core\User\UserInterface;
use JMS\Serializer\Annotation as Serializer;
 

/**
 *  Summary.
 *
 * Description: Usuario Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="usuario", indexes={@ORM\Index(name="correoElectronico", columns={"correoElectronico"})})
 * @ORM\Entity(repositoryClass="App\Repository\UsuarioRepository")
 */
class Usuario implements UserInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="usuarioID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $usuarioid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="correoElectronico", type="string", length=100, nullable=true)
     */
    private $correoelectronico;
    
        
    /**
     * @ORM\Column(name="username", type="string", length=255, unique=true)
     */
    protected $username;


     /**
     * @ORM\Column(name="password", type="string", length=255)
     * @Serializer\Exclude()
     */
    private $password;


    /**
     * @var string
     */
    protected $plainPassword;


    /**
     * @var string|null
     *
     * @ORM\Column(name="clave", type="string", length=256, nullable=true)
     */
    private $clave;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre", type="string", length=50, nullable=true)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="apellido", type="string", length=50, nullable=true)
     */
    private $apellido;

    /**
     * @var string|null
     *
     * @ORM\Column(name="recibirInformacion", type="string", length=1, nullable=true)
     */
    private $recibirinformacion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CI", type="string", length=10, nullable=true)
     */
    private $ci;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaNacimiento", type="datetime", nullable=true)
     */
    private $fechanacimiento;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sexo", type="string", length=1, nullable=true)
     */
    private $sexo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="direccion", type="string", length=500, nullable=true)
     */
    private $direccion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="numCelular", type="string", length=100, nullable=true)
     */
    private $numcelular;

    /**
     * @var string|null
     *
     * @ORM\Column(name="userVistaID", type="string", length=25, nullable=true)
     */
    private $uservistaid;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ciudad", type="integer", nullable=true)
     */
    private $ciudad;

    /**
     * @var string|null
     *
     * @ORM\Column(name="numLocal", type="string", length=100, nullable=true)
     */
    private $numlocal;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="emailLista", type="boolean", nullable=true)
     */
    private $emaillista = '0';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaCreacion", type="datetime", nullable=true)
     */
    private $fechacreacion;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="ultimaModificacion", type="datetime", nullable=true)
     */
    private $ultimamodificacion;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="emailListaCiudad", type="boolean", nullable=true)
     */
    private $emaillistaciudad = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="memberCardNumber", type="string", length=16, nullable=true)
     */
    private $membercardnumber;

    /**
     * @var string|null
     * @ORM\Column(name="memberId", type="string", length=45, nullable=true)
     */
    private $memberid;

    /**
     * @var integer
     * @ORM\Column(name="tipo_usuario", type="integer")
     */
    private $tipo_usuario;

 /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }
 
    /**
     * @param mixed $password
     * @return self
     */
    public function setPassword($password)
    {
        $this->password = $password;
 
        return $this;
    }
 
    /**
     * @return string
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }
 
    /**
     * @param $plainPassword
     */
    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;
 
        $this->password = null;
    }
 
    /**
     * Set roles
     *
     * @param array $roles
     *
     * @return User
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;
 
        return $this;
    }
 
    /**
     * Get roles
     *
     * @return array
     */
    public function getRoles()
    {
        return ["ROLE_USER"];
    }
 
    public function getSalt() {}
 
    public function eraseCredentials() {}
 
    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }
    
    /**
     * @param mixed $username
     * @return self
     */
    public function setUsername($username)
    {
        $this->username = $username;
        
        return $this;
    }

    public function getUsuarioid()
    {
        return $this->usuarioid;
    }

    public function getCorreoelectronico()
    {
        return $this->correoelectronico;
    }

    public function setCorreoelectronico(string $correoelectronico)
    {
        $this->correoelectronico = $correoelectronico;

        return $this;
    }

    public function getClave()
    {
        return $this->clave;
    }

    public function setClave(string $clave)
    {
        $this->clave = $clave;

        return $this;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getApellido()
    {
        return $this->apellido;
    }

    public function setApellido(string $apellido)
    {
        $this->apellido = $apellido;

        return $this;
    }

    public function getRecibirinformacion()
    {
        return $this->recibirinformacion;
    }

    public function setRecibirinformacion(string $recibirinformacion)
    {
        $this->recibirinformacion = $recibirinformacion;

        return $this;
    }

    public function getCi()
    {
        return $this->ci;
    }

    public function setCi(string $ci)
    {
        $this->ci = $ci;

        return $this;
    }

    public function getFechanacimiento()
    {
        return $this->fechanacimiento;
    }

    public function setFechanacimiento(\DateTimeInterface $fechanacimiento)
    {
        $this->fechanacimiento = $fechanacimiento;

        return $this;
    }

    public function getSexo()
    {
        return $this->sexo;
    }

    public function setSexo(string $sexo)
    {
        $this->sexo = $sexo;

        return $this;
    }

    public function getDireccion()
    {
        return $this->direccion;
    }

    public function setDireccion(string $direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    public function getNumcelular()
    {
        return $this->numcelular;
    }

    public function setNumcelular(string $numcelular)
    {
        $this->numcelular = $numcelular;

        return $this;
    }

    public function getUservistaid()
    {
        return $this->uservistaid;
    }

    public function setUservistaid(string $uservistaid)
    {
        $this->uservistaid = $uservistaid;

        return $this;
    }

    public function getCiudad()
    {
        return $this->ciudad;
    }

    public function setCiudad(int $ciudad)
    {
        $this->ciudad = $ciudad;

        return $this;
    }

    public function getNumlocal()
    {
        return $this->numlocal;
    }

    public function setNumlocal(string $numlocal)
    {
        $this->numlocal = $numlocal;

        return $this;
    }

    public function getEmaillista()
    {
        return $this->emaillista;
    }

    public function setEmaillista(bool $emaillista)
    {
        $this->emaillista = $emaillista;

        return $this;
    }

    public function getFechacreacion()
    {
        return $this->fechacreacion;
    }

    public function setFechacreacion(\DateTimeInterface $fechacreacion)
    {
        $this->fechacreacion = $fechacreacion;

        return $this;
    }

    public function getUltimamodificacion()
    {
        return $this->ultimamodificacion;
    }

    public function setUltimamodificacion(\DateTimeInterface $ultimamodificacion)
    {
        $this->ultimamodificacion = $ultimamodificacion;

        return $this;
    }

    public function getEmaillistaciudad()
    {
        return $this->emaillistaciudad;
    }

    public function setEmaillistaciudad(bool $emaillistaciudad)
    {
        $this->emaillistaciudad = $emaillistaciudad;

        return $this;
    }

    public function getMembercardnumber()
    {
        return $this->membercardnumber;
    }

    public function setMembercardnumber($membercardnumber)
    {
        $this->membercardnumber = $membercardnumber;

        return $this;
    }

    public function getMemberid()
    {
        return $this->memberid;
    }

    public function setMemberid(string $memberid)
    {
        $this->memberid = $memberid;

        return $this;
    }

    public function getTipousuario()
    {
        return $this->tipo_usuario;
    }

    public function setTipousuario($tipo_usuario)
    {
        $this->tipo_usuario = $tipo_usuario;
        return $this;
    }
}
