<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: UsuariosMercadeo2 Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="usuarios_mercadeo_2")
 * @ORM\Entity
 */
class UsuariosMercadeo2
{
    /**
     * @var int
     *
     * @ORM\Column(name="usuarioID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $usuarioid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre", type="text", length=65535, nullable=true)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="telefono", type="text", length=65535, nullable=true)
     */
    private $telefono;

    /**
     * @var string|null
     *
     * @ORM\Column(name="correo", type="text", length=65535, nullable=true)
     */
    private $correo;

    public function getUsuarioid()
    {
        return $this->usuarioid;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getTelefono()
    {
        return $this->telefono;
    }

    public function setTelefono(string $telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    public function getCorreo()
    {
        return $this->correo;
    }

    public function setCorreo(string $correo)
    {
        $this->correo = $correo;

        return $this;
    }


}
