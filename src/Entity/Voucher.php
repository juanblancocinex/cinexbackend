<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: Voucher Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="voucher", uniqueConstraints={@ORM\UniqueConstraint(name="codigo_voucher", columns={"codigo"})}, indexes={@ORM\Index(name="user_vista_id", columns={"user_vista_id"})})
 * @ORM\Entity
 */
class Voucher
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=16, nullable=false)
     */
    private $codigo;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha_exp", type="datetime", nullable=true)
     */
    private $fechaExp;

    /**
     * @var string|null
     *
     * @ORM\Column(name="monto", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $monto;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comision", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $comision;

    /**
     * @var int|null
     *
     * @ORM\Column(name="boletos", type="integer", nullable=true)
     */
    private $boletos;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="activo", type="boolean", nullable=true, options={"default"="1"})
     */
    private $activo = '1';

    /**
     * @var string|null
     *
     * @ORM\Column(name="user_vista_id", type="string", length=25, nullable=true)
     */
    private $userVistaId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id_beneficio", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $idBeneficio;

    public function getId()
    {
        return $this->id;
    }

    public function getCodigo()
    {
        return $this->codigo;
    }

    public function setCodigo(string $codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    public function getFechaExp()
    {
        return $this->fechaExp;
    }

    public function setFechaExp(\DateTimeInterface $fechaExp)
    {
        $this->fechaExp = $fechaExp;

        return $this;
    }

    public function getMonto()
    {
        return $this->monto;
    }

    public function setMonto($monto)
    {
        $this->monto = $monto;

        return $this;
    }

    public function getComision()
    {
        return $this->comision;
    }

    public function setComision($comision)
    {
        $this->comision = $comision;

        return $this;
    }

    public function getBoletos()
    {
        return $this->boletos;
    }

    public function setBoletos(int $boletos)
    {
        $this->boletos = $boletos;

        return $this;
    }

    public function getActivo()
    {
        return $this->activo;
    }

    public function setActivo(bool $activo)
    {
        $this->activo = $activo;

        return $this;
    }

    public function getUserVistaId()
    {
        return $this->userVistaId;
    }

    public function setUserVistaId(string $userVistaId)
    {
        $this->userVistaId = $userVistaId;

        return $this;
    }

    public function getIdBeneficio()
    {
        return $this->idBeneficio;
    }

    public function setIdBeneficio(int $idBeneficio)
    {
        $this->idBeneficio = $idBeneficio;

        return $this;
    }


}
