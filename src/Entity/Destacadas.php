<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description:  Destacadas Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 *
 * @ORM\Table(name="destacadas")
 * @ORM\Entity
 */
class Destacadas
{
    /**
     * @var int
     *
     * @ORM\Column(name="destacadasID", type="integer", nullable=false, options={"unsigned"=true,"comment"="clave artificial del top"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $destacadasid;

    /**
     * @var string
     *
     * @ORM\Column(name="nombrePelicula", type="string", length=50, nullable=false, options={"comment"="nombre de la pelicula"})
     */
    private $nombrepelicula;

    /**
     * @var int
     *
     * @ORM\Column(name="posicion", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $posicion;

    /**
     * @var string
     *
     * @ORM\Column(name="vigente", type="string", length=1, nullable=false, options={"comment"="vigente Y no vigente N"})
     */
    private $vigente;

    /**
     * @var string|null
     *
     * @ORM\Column(name="urlDestacado", type="string", length=500, nullable=true)
     */
    private $urldestacado;

    /**
     * @var string|null
     *
     * @ORM\Column(name="urlSliderMini", type="string", length=500, nullable=true)
     */
    private $urlslidermini;

    /**
     * @var string|null
     *
     * @ORM\Column(name="urlBaseDestino", type="string", length=500, nullable=true)
     */
    private $urlbasedestino;

    /**
     * @var string|null
     *
     * @ORM\Column(name="urlTrailer", type="string", length=500, nullable=true)
     */
    private $urltrailer;

    /**
     * @var string|null
     *
     * @ORM\Column(name="esPelicula", type="string", length=1, nullable=true)
     */
    private $espelicula;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombreEvento", type="string", length=45, nullable=true)
     */
    private $nombreevento;

    /**
     * @var string|null
     *
     * @ORM\Column(name="urlImagen", type="string", length=500, nullable=true)
     */
    private $urlimagen;

    public function getDestacadasid()
    {
        return $this->destacadasid;
    }

    public function getNombrepelicula()
    {
        return $this->nombrepelicula;
    }

    public function setNombrepelicula(string $nombrepelicula)
    {
        $this->nombrepelicula = $nombrepelicula;

        return $this;
    }

    public function getPosicion()
    {
        return $this->posicion;
    }

    public function setPosicion(int $posicion)
    {
        $this->posicion = $posicion;

        return $this;
    }

    public function getVigente()
    {
        return $this->vigente;
    }

    public function setVigente(string $vigente)
    {
        $this->vigente = $vigente;

        return $this;
    }

    public function getUrldestacado()
    {
        return $this->urldestacado;
    }

    public function setUrldestacado(string $urldestacado)
    {
        $this->urldestacado = $urldestacado;

        return $this;
    }

    public function getUrlslidermini()
    {
        return $this->urlslidermini;
    }

    public function setUrlslidermini(string $urlslidermini)
    {
        $this->urlslidermini = $urlslidermini;

        return $this;
    }

    public function getUrlbasedestino()
    {
        return $this->urlbasedestino;
    }

    public function setUrlbasedestino(string $urlbasedestino)
    {
        $this->urlbasedestino = $urlbasedestino;

        return $this;
    }

    public function getUrltrailer()
    {
        return $this->urltrailer;
    }

    public function setUrltrailer(string $urltrailer)
    {
        $this->urltrailer = $urltrailer;

        return $this;
    }

    public function getEspelicula()
    {
        return $this->espelicula;
    }

    public function setEspelicula(string $espelicula)
    {
        $this->espelicula = $espelicula;

        return $this;
    }

    public function getNombreevento()
    {
        return $this->nombreevento;
    }

    public function setNombreevento(string $nombreevento)
    {
        $this->nombreevento = $nombreevento;

        return $this;
    }

    public function getUrlimagen()
    {
        return $this->urlimagen;
    }

    public function setUrlimagen(string $urlimagen)
    {
        $this->urlimagen = $urlimagen;

        return $this;
    }


}
