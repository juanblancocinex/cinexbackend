<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: Partido Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="partido", indexes={@ORM\Index(name="fk_equi1", columns={"equipo1_id"}), @ORM\Index(name="fk_equi2", columns={"equipo2_id"}), @ORM\Index(name="fk_ronda", columns={"ronda_id"})})
 * @ORM\Entity
 */
class Partido
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=false)
     */
    private $fecha;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="marcador1", type="boolean", nullable=true)
     */
    private $marcador1;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="marcador2", type="boolean", nullable=true)
     */
    private $marcador2;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ganador", type="integer", nullable=true, options={"comment"="id de equipo ganador"})
     */
    private $ganador;

    /**
     * @var bool
     *
     * @ORM\Column(name="estatus", type="boolean", nullable=false, options={"comment"="0 por efectuar, 1 terminado, 2 aplicado"})
     */
    private $estatus = '0';

    /**
     * @var \Equipo
     *
     * @ORM\ManyToOne(targetEntity="Equipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="equipo1_id", referencedColumnName="id")
     * })
     */
    private $equipo1;

    /**
     * @var \Equipo
     *
     * @ORM\ManyToOne(targetEntity="Equipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="equipo2_id", referencedColumnName="id")
     * })
     */
    private $equipo2;

    /**
     * @var \Ronda
     *
     * @ORM\ManyToOne(targetEntity="Ronda")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ronda_id", referencedColumnName="id")
     * })
     */
    private $ronda;

    public function getId()
    {
        return $this->id;
    }

    public function getFecha()
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getMarcador1(): bool
    {
        return $this->marcador1;
    }

    public function setMarcador1(bool $marcador1)
    {
        $this->marcador1 = $marcador1;

        return $this;
    }

    public function getMarcador2()
    {
        return $this->marcador2;
    }

    public function setMarcador2(bool $marcador2)
    {
        $this->marcador2 = $marcador2;

        return $this;
    }

    public function getGanador()
    {
        return $this->ganador;
    }

    public function setGanador(int $ganador)
    {
        $this->ganador = $ganador;

        return $this;
    }

    public function getEstatus()
    {
        return $this->estatus;
    }

    public function setEstatus(bool $estatus)
    {
        $this->estatus = $estatus;

        return $this;
    }

    public function getEquipo1()
    {
        return $this->equipo1;
    }

    public function setEquipo1(Equipo $equipo1)
    {
        $this->equipo1 = $equipo1;

        return $this;
    }

    public function getEquipo2()
    {
        return $this->equipo2;
    }

    public function setEquipo2(Equipo $equipo2)
    {
        $this->equipo2 = $equipo2;

        return $this;
    }

    public function getRonda()
    {
        return $this->ronda;
    }

    public function setRonda(Ronda $ronda)
    {
        $this->ronda = $ronda;

        return $this;
    }


}
