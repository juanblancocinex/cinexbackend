<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
  * Summary.
 *
 * Description:  Concesiones Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="concesiones")
 * @ORM\Entity
 */
class Concesiones
{
    /**
     * @var int
     *
     * @ORM\Column(name="con_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $conId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="cat_id", type="integer", nullable=true)
     */
    private $catId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="codigoComplejo", type="string", length=255, nullable=true)
     */
    private $codigocomplejo;

    /**
     * @var int|null
     *
     * @ORM\Column(name="con_itemcode", type="integer", nullable=true)
     */
    private $conItemcode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="con_nombre_item", type="text", length=0, nullable=true)
     */
    private $conNombreItem;

    /**
     * @var float|null
     *
     * @ORM\Column(name="con_item_price", type="float", precision=10, scale=0, nullable=true)
     */
    private $conItemPrice;

    /**
     * @var float|null
     *
     * @ORM\Column(name="con_item_tax", type="float", precision=10, scale=0, nullable=true)
     */
    private $conItemTax;

    /**
     * @var int|null
     *
     * @ORM\Column(name="con_cantidad", type="integer", nullable=true)
     */
    private $conCantidad;

    /**
     * @var string|null
     *
     * @ORM\Column(name="con_imagen", type="blob", length=16777215, nullable=true)
     */
    private $conImagen;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="con_activo", type="boolean", nullable=true, options={"default"="b'1'"})
     */
    private $conActivo = 'b\'1\'';

    public function getConId()
    {
        return $this->conId;
    }

    public function getCatId()
    {
        return $this->catId;
    }

    public function setCatId(int $catId)
    {
        $this->catId = $catId;

        return $this;
    }

    public function getCodigocomplejo()
    {
        return $this->codigocomplejo;
    }

    public function setCodigocomplejo(string $codigocomplejo)
    {
        $this->codigocomplejo = $codigocomplejo;

        return $this;
    }

    public function getConItemcode()
    {
        return $this->conItemcode;
    }

    public function setConItemcode(int $conItemcode)
    {
        $this->conItemcode = $conItemcode;

        return $this;
    }

    public function getConNombreItem()
    {
        return $this->conNombreItem;
    }

    public function setConNombreItem(string $conNombreItem)
    {
        $this->conNombreItem = $conNombreItem;

        return $this;
    }

    public function getConItemPrice()
    {
        return $this->conItemPrice;
    }

    public function setConItemPrice(float $conItemPrice)
    {
        $this->conItemPrice = $conItemPrice;

        return $this;
    }

    public function getConItemTax()
    {
        return $this->conItemTax;
    }

    public function setConItemTax(float $conItemTax)
    {
        $this->conItemTax = $conItemTax;

        return $this;
    }

    public function getConCantidad()
    {
        return $this->conCantidad;
    }

    public function setConCantidad(int $conCantidad)
    {
        $this->conCantidad = $conCantidad;

        return $this;
    }

    public function getConImagen()
    {
        return $this->conImagen;
    }

    public function setConImagen($conImagen)
    {
        $this->conImagen = $conImagen;

        return $this;
    }

    public function getConActivo()
    {
        return $this->conActivo;
    }

    public function setConActivo(bool $conActivo)
    {
        $this->conActivo = $conActivo;

        return $this;
    }


}
