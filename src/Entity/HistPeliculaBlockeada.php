<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: HistPeliculaBlockeada Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="hist_pelicula_blockeada")
 * @ORM\Entity
 */
class HistPeliculaBlockeada
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codvista", type="string", length=255, nullable=false)
     */
    private $codvista;

    /**
     * @var string
     *
     * @ORM\Column(name="codsinec", type="string", length=255, nullable=false)
     */
    private $codsinec;

    /**
     * @var string
     *
     * @ORM\Column(name="nombresinformato", type="text", length=65535, nullable=false)
     */
    private $nombresinformato;

    /**
     * @var int
     *
     * @ORM\Column(name="peliculaID", type="integer", nullable=false)
     */
    private $peliculaid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_bloqueo", type="datetime", nullable=false)
     */
    private $fechaBloqueo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_desbloqueo", type="datetime", nullable=false)
     */
    private $fechaDesbloqueo;

    public function getId()
    {
        return $this->id;
    }

    public function getCodvista()
    {
        return $this->codvista;
    }

    public function setCodvista(string $codvista)
    {
        $this->codvista = $codvista;

        return $this;
    }

    public function getCodsinec()
    {
        return $this->codsinec;
    }

    public function setCodsinec(string $codsinec)
    {
        $this->codsinec = $codsinec;

        return $this;
    }

    public function getNombresinformato()
    {
        return $this->nombresinformato;
    }

    public function setNombresinformato(string $nombresinformato)
    {
        $this->nombresinformato = $nombresinformato;

        return $this;
    }

    public function getPeliculaid()
    {
        return $this->peliculaid;
    }

    public function setPeliculaid(int $peliculaid)
    {
        $this->peliculaid = $peliculaid;

        return $this;
    }

    public function getFechaBloqueo()
    {
        return $this->fechaBloqueo;
    }

    public function setFechaBloqueo(\DateTimeInterface $fechaBloqueo)
    {
        $this->fechaBloqueo = $fechaBloqueo;

        return $this;
    }

    public function getFechaDesbloqueo()
    {
        return $this->fechaDesbloqueo;
    }

    public function setFechaDesbloqueo(\DateTimeInterface $fechaDesbloqueo)
    {
        $this->fechaDesbloqueo = $fechaDesbloqueo;

        return $this;
    }


}
