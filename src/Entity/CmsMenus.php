<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Summary.
 *
 * Description:  CmsMenus Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="cms_menus")
 * @ORM\Entity
 */
class CmsMenus
{
    /**
     * @var int
     *
     * @ORM\Column(name="menu_id", type="integer", nullable=false, options={"comment"="id autoincremental"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $menuId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="menu_nombre", type="string", length=255, nullable=true, options={"comment"="nombre"})
     */
    private $menuNombre;

    /**
     * @var int|null
     *
     * @ORM\Column(name="menu_orden", type="integer", nullable=true, options={"comment"="orden del menu"})
     */
    private $menuOrden;

    /**
     * @var string|null
     *
     * @ORM\Column(name="menu_activo", type="string", length=1, nullable=true, options={"default"="S","fixed"=true,"comment"="S=ACTIVO;N=INACTIVO"})
     */
    private $menuActivo = 'S';

    public function getMenuId()
    {
        return $this->menuId;
    }

    public function getMenuNombre()
    {
        return $this->menuNombre;
    }

    public function setMenuNombre(string $menuNombre)
    {
        $this->menuNombre = $menuNombre;

        return $this;
    }

    public function getMenuOrden()
    {
        return $this->menuOrden;
    }

    public function setMenuOrden(int $menuOrden)
    {
        $this->menuOrden = $menuOrden;

        return $this;
    }

    public function getMenuActivo()
    {
        return $this->menuActivo;
    }

    public function setMenuActivo(string $menuActivo)
    {
        $this->menuActivo = $menuActivo;

        return $this;
    }


}
