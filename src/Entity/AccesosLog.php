<?php

namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Summary.
 *
 * Description: AccesosLog Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 */
/**
 *
 * @ORM\Table(name="accesos_log")
 * @ORM\Entity
 */
class AccesosLog
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="user", type="integer", nullable=false)
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="perfil", type="string", length=0, nullable=false)
     */
    private $perfil;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=false)
     */
    private $fecha;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cliente_info", type="text", length=16777215, nullable=true)
     */
    private $clienteInfo;

    public function getId()
    {
        return $this->id;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser(int $user)
    {
        $this->user = $user;

        return $this;
    }

    public function getPerfil()
    {
        return $this->perfil;
    }

    public function setPerfil(string $perfil)
    {
        $this->perfil = $perfil;

        return $this;
    }

    public function getFecha()
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getClienteInfo()
    {
        return $this->clienteInfo;
    }

    public function setClienteInfo(string $clienteInfo)
    {
        $this->clienteInfo = $clienteInfo;

        return $this;
    }


}
