<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
  *  Summary.
 *
 * Description: UsuarioMensaje Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="usuario_mensaje")
 * @ORM\Entity
 */
class UsuarioMensaje
{
    /**
     * @var int
     *
     * @ORM\Column(name="mensaje_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $mensajeId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="usuarioID", type="integer", nullable=true)
     */
    private $usuarioid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="titulo", type="string", length=255, nullable=true)
     */
    private $titulo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="mensaje", type="text", length=0, nullable=true)
     */
    private $mensaje;

    /**
     * @var string|null
     *
     * @ORM\Column(name="link", type="string", length=255, nullable=true)
     */
    private $link;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="enviado", type="boolean", nullable=true)
     */
    private $enviado;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechahora", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $fechahora = 'CURRENT_TIMESTAMP';

    public function getMensajeId()
    {
        return $this->mensajeId;
    }

    public function getUsuarioid()
    {
        return $this->usuarioid;
    }

    public function setUsuarioid(int $usuarioid)
    {
        $this->usuarioid = $usuarioid;

        return $this;
    }

    public function getTitulo()
    {
        return $this->titulo;
    }

    public function setTitulo(string $titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    public function getMensaje()
    {
        return $this->mensaje;
    }

    public function setMensaje(string $mensaje)
    {
        $this->mensaje = $mensaje;

        return $this;
    }

    public function getLink()
    {
        return $this->link;
    }

    public function setLink(string $link)
    {
        $this->link = $link;

        return $this;
    }

    public function getEnviado()
    {
        return $this->enviado;
    }

    public function setEnviado(bool $enviado)
    {
        $this->enviado = $enviado;

        return $this;
    }

    public function getFechahora()
    {
        return $this->fechahora;
    }

    public function setFechahora(\DateTimeInterface $fechahora)
    {
        $this->fechahora = $fechahora;

        return $this;
    }


}
