<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * 
 * Summary.
 *
 * Description:  Banners Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 * 
 * @ORM\Table(name="banners")
 * @ORM\Entity
 */
class Banners
{
    /**
     * @var int
     *
     * @ORM\Column(name="bann_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $bannId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="bann_nombre", type="string", length=255, nullable=true)
     */
    private $bannNombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="bann_url", type="string", length=255, nullable=true, options={"default"="#"})
     */
    private $bannUrl = '#';

    /**
     * @var string|null
     *
     * @ORM\Column(name="bann_img", type="string", length=255, nullable=true, options={"default"="#"})
     */
    private $bannImg = '#';

    /**
     * @var string|null
     *
     * @ORM\Column(name="bann_target", type="string", length=255, nullable=true, options={"default"="_SELF"})
     */
    private $bannTarget = '_SELF';

    /**
     * @var string|null
     *
     * @ORM\Column(name="bann_orden", type="string", length=255, nullable=true)
     */
    private $bannOrden = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="bann_activo", type="string", length=1, nullable=true, options={"default"="S","fixed"=true})
     */
    private $bannActivo = 'S';

    public function getBannId()
    {
        return $this->bannId;
    }

    public function getBannNombre()
    {
        return $this->bannNombre;
    }

    public function setBannNombre(?string $bannNombre): self
    {
        $this->bannNombre = $bannNombre;

        return $this;
    }

    public function getBannUrl()
    {
        return $this->bannUrl;
    }

    public function setBannUrl(?string $bannUrl): self
    {
        $this->bannUrl = $bannUrl;

        return $this;
    }

    public function getBannImg()
    {
        return $this->bannImg;
    }

    public function setBannImg(?string $bannImg): self
    {
        $this->bannImg = $bannImg;

        return $this;
    }

    public function getBannTarget()
    {
        return $this->bannTarget;
    }

    public function setBannTarget(string $bannTarget): self
    {
        $this->bannTarget = $bannTarget;

        return $this;
    }

    public function getBannOrden()
    {
        return $this->bannOrden;
    }

    public function setBannOrden(string $bannOrden): self
    {
        $this->bannOrden = $bannOrden;

        return $this;
    }

    public function getBannActivo()
    {
        return $this->bannActivo;
    }

    public function setBannActivo(string $bannActivo): self
    {
        $this->bannActivo = $bannActivo;

        return $this;
    }


}
