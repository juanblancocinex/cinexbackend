<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
*  Summary.
 *
 * Description:  WebPrices Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 *
 * @ORM\Table(name="web_prices")
 * @ORM\Entity
 */
class WebPrices
{
    /**
     * @var int
     *
     * @ORM\Column(name="price_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $priceId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cinema_id", type="string", length=45, nullable=true)
     */
    private $cinemaId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="price_version", type="string", length=255, nullable=true)
     */
    private $priceVersion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ticket_type_name", type="text", length=0, nullable=true)
     */
    private $ticketTypeName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="price_web", type="decimal", precision=18, scale=2, nullable=true)
     */
    private $priceWeb;

    /**
     * @var string|null
     *
     * @ORM\Column(name="price_comision", type="decimal", precision=18, scale=2, nullable=true)
     */
    private $priceComision;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="price_dateupdate", type="datetime", nullable=true)
     */
    private $priceDateupdate;

    public function getPriceId()
    {
        return $this->priceId;
    }

    public function getCinemaId()
    {
        return $this->cinemaId;
    }

    public function setCinemaId(string $cinemaId)
    {
        $this->cinemaId = $cinemaId;

        return $this;
    }

    public function getPriceVersion()
    {
        return $this->priceVersion;
    }

    public function setPriceVersion(string $priceVersion)
    {
        $this->priceVersion = $priceVersion;

        return $this;
    }

    public function getTicketTypeName()
    {
        return $this->ticketTypeName;
    }

    public function setTicketTypeName(string $ticketTypeName)
    {
        $this->ticketTypeName = $ticketTypeName;

        return $this;
    }

    public function getPriceWeb()
    {
        return $this->priceWeb;
    }

    public function setPriceWeb($priceWeb)
    {
        $this->priceWeb = $priceWeb;

        return $this;
    }

    public function getPriceComision()
    {
        return $this->priceComision;
    }

    public function setPriceComision($priceComision)
    {
        $this->priceComision = $priceComision;

        return $this;
    }

    public function getPriceDateupdate()
    {
        return $this->priceDateupdate;
    }

    public function setPriceDateupdate(\DateTimeInterface $priceDateupdate)
    {
        $this->priceDateupdate = $priceDateupdate;

        return $this;
    }


}
