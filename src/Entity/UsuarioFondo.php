<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: UsuarioFondo Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 *
 * @ORM\Table(name="usuario_fondo")
 * @ORM\Entity
 */
class UsuarioFondo
{
    /**
     * @var int
     *
     * @ORM\Column(name="usuarioID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $usuarioid;

    /**
     * @var int
     *
     * @ORM\Column(name="anchura", type="smallint", nullable=false)
     */
    private $anchura;

    /**
     * @var int
     *
     * @ORM\Column(name="altura", type="smallint", nullable=false)
     */
    private $altura;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=15, nullable=false, options={"fixed"=true})
     */
    private $tipo;

    /**
     * @var string
     *
     * @ORM\Column(name="imagen", type="blob", length=16777215, nullable=false)
     */
    private $imagen;

    public function getUsuarioid()
    {
        return $this->usuarioid;
    }

    public function getAnchura()
    {
        return $this->anchura;
    }

    public function setAnchura(int $anchura)
    {
        $this->anchura = $anchura;

        return $this;
    }

    public function getAltura()
    {
        return $this->altura;
    }

    public function setAltura(int $altura)
    {
        $this->altura = $altura;

        return $this;
    }

    public function getTipo()
    {
        return $this->tipo;
    }

    public function setTipo(string $tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    public function getImagen()
    {
        return $this->imagen;
    }

    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }


}
