<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Summary.
 *
 * Description:  Boletines Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="boletines")
 * @ORM\Entity
 */
class Boletines
{
    /**
     * @var int
     *
     * @ORM\Column(name="boletin_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $boletinId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="boletin_nombre", type="string", length=255, nullable=true)
     */
    private $boletinNombre;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="boletin_fecha", type="datetime", nullable=true)
     */
    private $boletinFecha;

    /**
     * @var string|null
     *
     * @ORM\Column(name="boletin_activo", type="string", length=1, nullable=true, options={"default"="S","fixed"=true})
     */
    private $boletinActivo = 'S';

    /**
     * @var string|null
     *
     * @ORM\Column(name="boletin_dirname", type="string", length=255, nullable=true)
     */
    private $boletinDirname;

    /**
     * @var string|null
     *
     * @ORM\Column(name="boletin_html", type="string", length=255, nullable=true)
     */
    private $boletinHtml;

    public function getBoletinId()
    {
        return $this->boletinId;
    }

    public function getBoletinNombre()
    {
        return $this->boletinNombre;
    }

    public function setBoletinNombre(string $boletinNombre)
    {
        $this->boletinNombre = $boletinNombre;

        return $this;
    }

    public function getBoletinFecha()
    {
        return $this->boletinFecha;
    }

    public function setBoletinFecha(\DateTimeInterface $boletinFecha)
    {
        $this->boletinFecha = $boletinFecha;

        return $this;
    }

    public function getBoletinActivo()
    {
        return $this->boletinActivo;
    }

    public function setBoletinActivo(string $boletinActivo)
    {
        $this->boletinActivo = $boletinActivo;

        return $this;
    }

    public function getBoletinDirname()
    {
        return $this->boletinDirname;
    }

    public function setBoletinDirname(string $boletinDirname)
    {
        $this->boletinDirname = $boletinDirname;

        return $this;
    }

    public function getBoletinHtml()
    {
        return $this->boletinHtml;
    }

    public function setBoletinHtml(string $boletinHtml)
    {
        $this->boletinHtml = $boletinHtml;

        return $this;
    }


}
