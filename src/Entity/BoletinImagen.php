<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * 
 * Summary.
 *
 * Description:  BoletinImagen Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="boletin_imagen")
 * @ORM\Entity
 */
class BoletinImagen
{
    /**
     * @var int
     *
     * @ORM\Column(name="imagen_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $imagenId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="boletin_id", type="integer", nullable=true)
     */
    private $boletinId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="imagen_imagen", type="string", length=255, nullable=true)
     */
    private $imagenImagen;

    public function getImagenId()
    {
        return $this->imagenId;
    }

    public function getBoletinId()
    {
        return $this->boletinId;
    }

    public function setBoletinId(int $boletinId)
    {
        $this->boletinId = $boletinId;

        return $this;
    }

    public function getImagenImagen()
    {
        return $this->imagenImagen;
    }

    public function setImagenImagen(string $imagenImagen)
    {
        $this->imagenImagen = $imagenImagen;

        return $this;
    }


}
