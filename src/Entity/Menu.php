<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
  *  Summary.
 *
 * Description: Menu Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="menu")
 * @ORM\Entity
 */
class Menu
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="texto", type="string", length=45, nullable=true)
     */
    private $texto;

    /**
     * @var string|null
     *
     * @ORM\Column(name="encabezado", type="string", length=45, nullable=true)
     */
    private $encabezado;

    /**
     * @var string|null
     *
     * @ORM\Column(name="url", type="string", length=250, nullable=true)
     */
    private $url;

    /**
     * @var string|null
     *
     * @ORM\Column(name="visible", type="string", length=45, nullable=true)
     */
    private $visible;

    public function getId()
    {
        return $this->id;
    }

    public function getTexto()
    {
        return $this->texto;
    }

    public function setTexto(string $texto)
    {
        $this->texto = $texto;

        return $this;
    }

    public function getEncabezado()
    {
        return $this->encabezado;
    }

    public function setEncabezado(string $encabezado)
    {
        $this->encabezado = $encabezado;

        return $this;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl(string $url)
    {
        $this->url = $url;

        return $this;
    }

    public function getVisible()
    {
        return $this->visible;
    }

    public function setVisible(string $visible)
    {
        $this->visible = $visible;

        return $this;
    }


}
