<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Summary.
 *
 * Description:  Bitacora Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="bitacora")
 * @ORM\Entity
 */
class Bitacora
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="login", type="string", length=45, nullable=true)
     */
    private $login;

    /**
     * @var int|null
     *
     * @ORM\Column(name="menuID", type="integer", nullable=true)
     */
    private $menuid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="detalle", type="string", length=100, nullable=true)
     */
    private $detalle;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=true)
     */
    private $fecha;

    public function getId()
    {
        return $this->id;
    }

    public function getLogin()
    {
        return $this->login;
    }

    public function setLogin(string $login)
    {
        $this->login = $login;

        return $this;
    }

    public function getMenuid()
    {
        return $this->menuid;
    }

    public function setMenuid(int $menuid)
    {
        $this->menuid = $menuid;

        return $this;
    }

    public function getDetalle()
    {
        return $this->detalle;
    }

    public function setDetalle(string $detalle)
    {
        $this->detalle = $detalle;

        return $this;
    }

    public function getFecha()
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }


}
