<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: Top5Settings Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 *
 * @ORM\Table(name="top5_settings")
 * @ORM\Entity
 */
class Top5Settings
{
    /**
     * @var bool
     *
     * @ORM\Column(name="automatico", type="boolean", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $automatico;

    public function getAutomatico()
    {
        return $this->automatico;
    }


}
