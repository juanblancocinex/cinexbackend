<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
  *  Summary.
 *
 * Description: ImagenesEspecialesBg Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 *
 * @ORM\Table(name="imagenes_especiales_bg")
 * @ORM\Entity
 */
class ImagenesEspecialesBg
{
    /**
     * @var int
     *
     * @ORM\Column(name="bg_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $bgId;

    /**
     * @var string
     *
     * @ORM\Column(name="bg_color", type="string", length=255, nullable=false)
     */
    private $bgColor = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="bg_blanconegro", type="string", length=255, nullable=false)
     */
    private $bgBlanconegro = '0';

    public function getBgId()
    {
        return $this->bgId;
    }

    public function getBgColor()
    {
        return $this->bgColor;
    }

    public function setBgColor(string $bgColor)
    {
        $this->bgColor = $bgColor;

        return $this;
    }

    public function getBgBlanconegro()
    {
        return $this->bgBlanconegro;
    }

    public function setBgBlanconegro(string $bgBlanconegro)
    {
        $this->bgBlanconegro = $bgBlanconegro;

        return $this;
    }


}
