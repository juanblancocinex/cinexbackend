<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Summary.
 *
 * Description:  Complejo Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="complejo", uniqueConstraints={@ORM\UniqueConstraint(name="codigoComplejo", columns={"codigoComplejo"})}, indexes={@ORM\Index(name="ciudadID", columns={"ciudadID"})})
  * @ORM\Entity(repositoryClass="App\Repository\ComplejoRepository")
 */
class Complejo
{
    /**
     * @var int
     *
     * @ORM\Column(name="complejoID", type="integer", nullable=false, options={"unsigned"=true,"comment"="clave artificial del complejo"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $complejoid;

    /**
     * @var string
     *
     * @ORM\Column(name="nombreComplejo", type="string", length=50, nullable=false, options={"comment"="nombre del complejo"})
     */
    private $nombrecomplejo;

    /**
     * @var string
     *
     * @ORM\Column(name="codigoComplejo", type="string", length=50, nullable=false, options={"comment"="codigo interno para interactuar con Vista y Sinec"})
     */
    private $codigocomplejo;

    /**
     * @var int|null
     */
     /**
     * Many Complejos have One Ciudad.
     * @ORM\ManyToOne(targetEntity="Ciudad", inversedBy="complejos")
     * @ORM\JoinColumn(name="ciudadID", referencedColumnName="ciudadID", nullable=true)
     */
    private $ciudadId;


    /**
     * @var string
     *
     * @ORM\Column(name="esProductivo", type="string", length=1, nullable=false, options={"comment"="S si es un complejo productivo, N si es un complejo para ambientes de desarrollo y certificación"})
     */
    private $esproductivo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="servicios", type="string", length=500, nullable=true)
     */
    private $servicios;

    /**
     * @var string|null
     *
     * @ORM\Column(name="direccion", type="string", length=500, nullable=true)
     */
    private $direccion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="longitud", type="string", length=500, nullable=true)
     */
    private $longitud;

    /**
     * @var string|null
     *
     * @ORM\Column(name="latitud", type="string", length=500, nullable=true)
     */
    private $latitud;

    /**
     * @var string|null
     *
     * @ORM\Column(name="telefonoAtencion", type="string", length=500, nullable=true)
     */
    private $telefonoatencion;

    /**
     * @var int|null
     *
     * @ORM\Column(name="codigoInterno", type="integer", nullable=true)
     */
    private $codigointerno;

    /**
     * @var int|null
     *
     * @ORM\Column(name="codigoNativa", type="integer", nullable=true)
     */
    private $codigonativa;

    /**
     * @var string|null
     *
     * @ORM\Column(name="banderasNativa", type="string", length=50, nullable=true)
     */
    private $banderasnativa;

    /**
     * @var string|null
     *
     * @ORM\Column(name="urlImagen1", type="string", length=100, nullable=true)
     */
    private $urlimagen1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="urlImagen2", type="string", length=100, nullable=true)
     */
    private $urlimagen2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="urlImagen3", type="string", length=100, nullable=true)
     */
    private $urlimagen3;

    /**
     * @var string|null
     *
     * @ORM\Column(name="urlImagen4", type="string", length=100, nullable=true)
     */
    private $urlimagen4;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcionBoletos", type="string", length=10000, nullable=true)
     */
    private $descripcionboletos;

    /**
     * @var string|null
     *
     * @ORM\Column(name="urlImagen5", type="string", length=1000, nullable=true)
     */
    private $urlimagen5;

    /**
     * @var string|null
     *
     * @ORM\Column(name="urlImagen6", type="string", length=1000, nullable=true)
     */
    private $urlimagen6;

    /**
     * @var string|null
     *
     * @ORM\Column(name="urlImagen7", type="string", length=1000, nullable=true)
     */
    private $urlimagen7;

    /**
     * @var string|null
     *
     * @ORM\Column(name="urlImagen8", type="string", length=1000, nullable=true)
     */
    private $urlimagen8;

    /**
     * @var int|null
     *
     * @ORM\Column(name="orden", type="integer", nullable=true)
     */
    private $orden;

    /**
     * @var string|null
     *
     * @ORM\Column(name="color", type="string", length=15, nullable=true)
     */
    private $color;

    /**
     * @var string|null
     *
     * @ORM\Column(name="place_id", type="text", length=65535, nullable=true)
     */
    private $placeId;

     /**
     * One Complejo has Many Programaciones.
     * @ORM\OneToMany(targetEntity="ProgramacionTest", mappedBy="codigocomplejo")
     */
    private $programaciones;

    public function __construct()
    {
        $this->programaciones = new ArrayCollection();
    }

    public function getComplejoid()
    {
        return $this->complejoid;
    }

    public function getNombrecomplejo()
    {
        return $this->nombrecomplejo;
    }

    public function setNombrecomplejo(string $nombrecomplejo)
    {
        $this->nombrecomplejo = $nombrecomplejo;

        return $this;
    }

    public function getCodigocomplejo()
    {
        return $this->codigocomplejo;
    }

    public function setCodigocomplejo(string $codigocomplejo)
    {
        $this->codigocomplejo = $codigocomplejo;

        return $this;
    }

    public function getEsproductivo()
    {
        return $this->esproductivo;
    }

    public function setEsproductivo(string $esproductivo)
    {
        $this->esproductivo = $esproductivo;

        return $this;
    }

    public function getServicios()
    {
        return $this->servicios;
    }

    public function setServicios(string $servicios)
    {
        $this->servicios = $servicios;

        return $this;
    }

    public function getDireccion()
    {
        return $this->direccion;
    }

    public function setDireccion(string $direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    public function getLongitud()
    {
        return $this->longitud;
    }

    public function setLongitud(string $longitud)
    {
        $this->longitud = $longitud;

        return $this;
    }

    public function getLatitud()
    {
        return $this->latitud;
    }

    public function setLatitud(string $latitud)
    {
        $this->latitud = $latitud;

        return $this;
    }

    public function getTelefonoatencion()
    {
        return $this->telefonoatencion;
    }

    public function setTelefonoatencion(string $telefonoatencion)
    {
        $this->telefonoatencion = $telefonoatencion;

        return $this;
    }

    public function getCodigointerno()
    {
        return $this->codigointerno;
    }

    public function setCodigointerno(int $codigointerno)
    {
        $this->codigointerno = $codigointerno;

        return $this;
    }

    public function getCodigonativa()
    {
        return $this->codigonativa;
    }

    public function setCodigonativa(int $codigonativa)
    {
        $this->codigonativa = $codigonativa;

        return $this;
    }

    public function getBanderasnativa()
    {
        return $this->banderasnativa;
    }

    public function setBanderasnativa(string $banderasnativa)
    {
        $this->banderasnativa = $banderasnativa;

        return $this;
    }

    public function getUrlimagen1()
    {
        return $this->urlimagen1;
    }

    public function setUrlimagen1(string $urlimagen1)
    {
        $this->urlimagen1 = $urlimagen1;

        return $this;
    }

    public function getUrlimagen2()
    {
        return $this->urlimagen2;
    }

    public function setUrlimagen2(string $urlimagen2)
    {
        $this->urlimagen2 = $urlimagen2;

        return $this;
    }

    public function getUrlimagen3()
    {
        return $this->urlimagen3;
    }

    public function setUrlimagen3(string $urlimagen3)
    {
        $this->urlimagen3 = $urlimagen3;

        return $this;
    }

    public function getUrlimagen4()
    {
        return $this->urlimagen4;
    }

    public function setUrlimagen4(string $urlimagen4)
    {
        $this->urlimagen4 = $urlimagen4;

        return $this;
    }

    public function getDescripcionboletos()
    {
        return $this->descripcionboletos;
    }

    public function setDescripcionboletos(string $descripcionboletos)
    {
        $this->descripcionboletos = $descripcionboletos;

        return $this;
    }

    public function getUrlimagen5()
    {
        return $this->urlimagen5;
    }

    public function setUrlimagen5(string $urlimagen5)
    {
        $this->urlimagen5 = $urlimagen5;

        return $this;
    }

    public function getUrlimagen6()
    {
        return $this->urlimagen6;
    }

    public function setUrlimagen6(string $urlimagen6)
    {
        $this->urlimagen6 = $urlimagen6;

        return $this;
    }

    public function getUrlimagen7()
    {
        return $this->urlimagen7;
    }

    public function setUrlimagen7(string $urlimagen7)
    {
        $this->urlimagen7 = $urlimagen7;

        return $this;
    }

    public function getUrlimagen8()
    {
        return $this->urlimagen8;
    }

    public function setUrlimagen8(string $urlimagen8)
    {
        $this->urlimagen8 = $urlimagen8;

        return $this;
    }

    public function getOrden()
    {
        return $this->orden;
    }

    public function setOrden(int $orden)
    {
        $this->orden = $orden;

        return $this;
    }

    public function getColor()
    {
        return $this->color;
    }

    public function setColor(string $color)
    {
        $this->color = $color;

        return $this;
    }

    public function getPlaceId()
    {
        return $this->placeId;
    }

    public function setPlaceId(string $placeId)
    {
        $this->placeId = $placeId;

        return $this;
    }

    public function getCiudadId()
    {
        return $this->ciudadId;
    }

    public function setCiudadId(Ciudad $ciudadId)
    {
        $this->ciudadId = $ciudadId;

        return $this;
    }

    /**
     * @return Collection|ProgramacionTest[]
     */
    public function getProgramaciones(): Collection
    {
        return $this->programaciones;
    }

    public function addProgramacione(ProgramacionTest $programacione)
    {
        if (!$this->programaciones->contains($programacione)) {
            $this->programaciones[] = $programacione;
            $programacione->setCodigocomplejo($this);
        }

        return $this;
    }

    public function removeProgramacione(ProgramacionTest $programacione)
    {
        if ($this->programaciones->contains($programacione)) {
            $this->programaciones->removeElement($programacione);
            // set the owning side to null (unless already changed)
            if ($programacione->getCodigocomplejo() === $this) {
                $programacione->setCodigocomplejo(null);
            }
        }

        return $this;
    }


   


}
