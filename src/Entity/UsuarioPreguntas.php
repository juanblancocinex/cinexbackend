<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: UsuarioPartido Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="usuario_preguntas")
 * @ORM\Entity
 */
class UsuarioPreguntas
{
    /**
     * @var int
     *
     * @ORM\Column(name="pregunta_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $preguntaId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="usuarioID", type="string", length=45, nullable=true)
     */
    private $usuarioid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="pregunta_pregunta", type="string", length=255, nullable=true)
     */
    private $preguntaPregunta;

    /**
     * @var string|null
     *
     * @ORM\Column(name="pregunta_respuesta", type="string", length=255, nullable=true)
     */
    private $preguntaRespuesta;

    public function getPreguntaId()
    {
        return $this->preguntaId;
    }

    public function getUsuarioid()
    {
        return $this->usuarioid;
    }

    public function setUsuarioid(string $usuarioid)
    {
        $this->usuarioid = $usuarioid;

        return $this;
    }

    public function getPreguntaPregunta()
    {
        return $this->preguntaPregunta;
    }

    public function setPreguntaPregunta(string $preguntaPregunta)
    {
        $this->preguntaPregunta = $preguntaPregunta;

        return $this;
    }

    public function getPreguntaRespuesta()
    {
        return $this->preguntaRespuesta;
    }

    public function setPreguntaRespuesta(string $preguntaRespuesta)
    {
        $this->preguntaRespuesta = $preguntaRespuesta;

        return $this;
    }


}
