<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: Promociones Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="promociones")
 * @ORM\Entity
 */
class Promociones
{
    /**
     * @var int
     *
     * @ORM\Column(name="promo_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $promoId;

    /**
     * @var string
     *
     * @ORM\Column(name="promo_nombre", type="string", length=255, nullable=false)
     */
    private $promoNombre;

    /**
     * @var string
     *
     * @ORM\Column(name="promo_img", type="string", length=255, nullable=false)
     */
    private $promoImg;

    /**
     * @var string
     *
     * @ORM\Column(name="promo_desc", type="text", length=0, nullable=false)
     */
    private $promoDesc;

    /**
     * @var int
     *
     * @ORM\Column(name="promo_tipo", type="integer", nullable=false)
     */
    private $promoTipo;

    /**
     * @var string
     *
     * @ORM\Column(name="promo_activo", type="string", length=1, nullable=false, options={"default"="S","fixed"=true})
     */
    private $promoActivo = 'S';

    /**
     * @var int
     *
     * @ORM\Column(name="promo_orden", type="integer", nullable=false)
     */
    private $promoOrden = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="promo_link", type="string", length=255, nullable=true)
     */
    private $promoLink;

    public function getPromoId()
    {
        return $this->promoId;
    }

    public function getPromoNombre()
    {
        return $this->promoNombre;
    }

    public function setPromoNombre(string $promoNombre)
    {
        $this->promoNombre = $promoNombre;

        return $this;
    }

    public function getPromoImg()
    {
        return $this->promoImg;
    }

    public function setPromoImg(string $promoImg)
    {
        $this->promoImg = $promoImg;

        return $this;
    }

    public function getPromoDesc()
    {
        return $this->promoDesc;
    }

    public function setPromoDesc(string $promoDesc)
    {
        $this->promoDesc = $promoDesc;

        return $this;
    }

    public function getPromoTipo()
    {
        return $this->promoTipo;
    }

    public function setPromoTipo(int $promoTipo)
    {
        $this->promoTipo = $promoTipo;

        return $this;
    }

    public function getPromoActivo()
    {
        return $this->promoActivo;
    }

    public function setPromoActivo(string $promoActivo)
    {
        $this->promoActivo = $promoActivo;

        return $this;
    }

    public function getPromoOrden()
    {
        return $this->promoOrden;
    }

    public function setPromoOrden(int $promoOrden)
    {
        $this->promoOrden = $promoOrden;

        return $this;
    }

    public function getPromoLink()
    {
        return $this->promoLink;
    }

    public function setPromoLink(string $promoLink)
    {
        $this->promoLink = $promoLink;

        return $this;
    }


}
