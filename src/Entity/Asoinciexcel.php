<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * 
 * Summary.
 *
 * Description:  Asoinciexcel Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 * 
 *
 * @ORM\Table(name="asoinciexcel")
 * @ORM\Entity
 */
class Asoinciexcel
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="co_complejo", type="string", length=45, nullable=true)
     */
    private $coComplejo;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="FechaExhibicion", type="datetime", nullable=true)
     */
    private $fechaexhibicion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ComplejoCinematografico", type="string", length=45, nullable=true)
     */
    private $complejocinematografico;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Sala", type="string", length=45, nullable=true)
     */
    private $sala;

    /**
     * @var string|null
     *
     * @ORM\Column(name="TituloObra", type="string", length=45, nullable=true)
     */
    private $tituloobra;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Idioma", type="string", length=45, nullable=true)
     */
    private $idioma;

    /**
     * @var string|null
     *
     * @ORM\Column(name="FormatoProyecccion", type="string", length=45, nullable=true)
     */
    private $formatoproyecccion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="HoraExhibicion", type="string", length=10, nullable=true)
     */
    private $horaexhibicion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Funcion", type="string", length=45, nullable=true)
     */
    private $funcion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="TipoBoleto", type="string", length=45, nullable=true)
     */
    private $tipoboleto;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PrecioBoleto", type="string", length=45, nullable=true)
     */
    private $precioboleto;

    /**
     * @var string|null
     *
     * @ORM\Column(name="NroEspectadores", type="string", length=45, nullable=true)
     */
    private $nroespectadores;

    /**
     * @var string|null
     *
     * @ORM\Column(name="TotalRecaudacion", type="string", length=45, nullable=true)
     */
    private $totalrecaudacion;

    public function getId()
    {
        return $this->id;
    }

    public function getCoComplejo()
    {
        return $this->coComplejo;
    }

    public function setCoComplejo(string $coComplejo)
    {
        $this->coComplejo = $coComplejo;

        return $this;
    }

    public function getFechaexhibicion()
    {
        return $this->fechaexhibicion;
    }

    public function setFechaexhibicion(\DateTimeInterface $fechaexhibicion)
    {
        $this->fechaexhibicion = $fechaexhibicion;

        return $this;
    }

    public function getComplejocinematografico()
    {
        return $this->complejocinematografico;
    }

    public function setComplejocinematografico(string $complejocinematografico)
    {
        $this->complejocinematografico = $complejocinematografico;

        return $this;
    }

    public function getSala()
    {
        return $this->sala;
    }

    public function setSala(string $sala)
    {
        $this->sala = $sala;

        return $this;
    }

    public function getTituloobra()
    {
        return $this->tituloobra;
    }

    public function setTituloobra(string $tituloobra)
    {
        $this->tituloobra = $tituloobra;

        return $this;
    }

    public function getIdioma()
    {
        return $this->idioma;
    }

    public function setIdioma(string $idioma)
    {
        $this->idioma = $idioma;

        return $this;
    }

    public function getFormatoproyecccion()
    {
        return $this->formatoproyecccion;
    }

    public function setFormatoproyecccion(string $formatoproyecccion)
    {
        $this->formatoproyecccion = $formatoproyecccion;

        return $this;
    }

    public function getHoraexhibicion()
    {
        return $this->horaexhibicion;
    }

    public function setHoraexhibicion(string $horaexhibicion)
    {
        $this->horaexhibicion = $horaexhibicion;

        return $this;
    }

    public function getFuncion()
    {
        return $this->funcion;
    }

    public function setFuncion(string $funcion)
    {
        $this->funcion = $funcion;

        return $this;
    }

    public function getTipoboleto()
    {
        return $this->tipoboleto;
    }

    public function setTipoboleto(string $tipoboleto)
    {
        $this->tipoboleto = $tipoboleto;

        return $this;
    }

    public function getPrecioboleto()
    {
        return $this->precioboleto;
    }

    public function setPrecioboleto(string $precioboleto)
    {
        $this->precioboleto = $precioboleto;

        return $this;
    }

    public function getNroespectadores(){
        return $this->nroespectadores;
    }

    public function setNroespectadores(string $nroespectadores)
    {
        $this->nroespectadores = $nroespectadores;

        return $this;
    }

    public function getTotalrecaudacion()
    {
        return $this->totalrecaudacion;
    }

    public function setTotalrecaudacion(string $totalrecaudacion)
    {
        $this->totalrecaudacion = $totalrecaudacion;

        return $this;
    }


}
