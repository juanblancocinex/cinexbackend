<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: UsuarioPartido Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="usuario_partido", indexes={@ORM\Index(name="fk_partido", columns={"partido_id"}), @ORM\Index(name="idx_ronda", columns={"ronda_id"}), @ORM\Index(name="idx_estatus", columns={"estatus"})})
 * @ORM\Entity
 */
class UsuarioPartido
{
    /**
     * @var int
     *
     * @ORM\Column(name="usuario_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $usuarioId;

    /**
     * @var int
     *
     * @ORM\Column(name="partido_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $partidoId;

    /**
     * @var int
     *
     * @ORM\Column(name="ronda_id", type="integer", nullable=false)
     */
    private $rondaId;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="marcador1", type="boolean", nullable=true)
     */
    private $marcador1;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="marcador2", type="boolean", nullable=true)
     */
    private $marcador2;

    /**
     * @var bool
     *
     * @ORM\Column(name="estatus", type="boolean", nullable=false, options={"comment"="0,1"})
     */
    private $estatus = '0';

    /**
     * @var bool
     *
     * @ORM\Column(name="puntaje", type="boolean", nullable=false)
     */
    private $puntaje = '0';

    public function getUsuarioId()
    {
        return $this->usuarioId;
    }

    public function getPartidoId()
    {
        return $this->partidoId;
    }

    public function getRondaId()
    {
        return $this->rondaId;
    }

    public function setRondaId(int $rondaId)
    {
        $this->rondaId = $rondaId;

        return $this;
    }

    public function getMarcador1()
    {
        return $this->marcador1;
    }

    public function setMarcador1(bool $marcador1)
    {
        $this->marcador1 = $marcador1;

        return $this;
    }

    public function getMarcador2()
    {
        return $this->marcador2;
    }

    public function setMarcador2(bool $marcador2)
    {
        $this->marcador2 = $marcador2;

        return $this;
    }

    public function getEstatus()
    {
        return $this->estatus;
    }

    public function setEstatus(bool $estatus)
    {
        $this->estatus = $estatus;

        return $this;
    }

    public function getPuntaje()
    {
        return $this->puntaje;
    }

    public function setPuntaje(bool $puntaje)
    {
        $this->puntaje = $puntaje;

        return $this;
    }


}
