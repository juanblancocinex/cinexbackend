<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: ImagenesCinexclusivo Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="imagenes_cinexclusivo")
 * @ORM\Entity
 */
class ImagenesCinexclusivo
{
    /**
     * @var int
     *
     * @ORM\Column(name="img_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $imgId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="img_nombre", type="string", length=255, nullable=true)
     */
    private $imgNombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="img_img", type="string", length=255, nullable=true)
     */
    private $imgImg;

    /**
     * @var int|null
     *
     * @ORM\Column(name="img_orden", type="integer", nullable=true)
     */
    private $imgOrden = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="img_activo", type="string", length=1, nullable=true, options={"default"="S","fixed"=true})
     */
    private $imgActivo = 'S';

    public function getImgId()
    {
        return $this->imgId;
    }

    public function getImgNombre()
    {
        return $this->imgNombre;
    }

    public function setImgNombre(string $imgNombre)
    {
        $this->imgNombre = $imgNombre;

        return $this;
    }

    public function getImgImg()
    {
        return $this->imgImg;
    }

    public function setImgImg(string $imgImg)
    {
        $this->imgImg = $imgImg;

        return $this;
    }

    public function getImgOrden()
    {
        return $this->imgOrden;
    }

    public function setImgOrden(int $imgOrden)
    {
        $this->imgOrden = $imgOrden;

        return $this;
    }

    public function getImgActivo()
    {
        return $this->imgActivo;
    }

    public function setImgActivo(string $imgActivo)
    {
        $this->imgActivo = $imgActivo;

        return $this;
    }


}
