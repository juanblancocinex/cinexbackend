<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description:  VouchersEnviados Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="vouchers_enviados")
 * @ORM\Entity(repositoryClass="App\Repository\VouchersEnviadosRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class VouchersEnviados
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="codigo_voucher", type="string", length=255, nullable=true)
     */
    private $codigoVoucher;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombresapellidos", type="string", length=255, nullable=true)
     */
    private $nombresapellidos;

    /**
     * @var string|null
     *
     * @ORM\Column(name="correoElectronico", type="string", length=255, nullable=true)
     */
    private $correoelectronico;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=true)
     */
    private $fecha;

    public function getId()
    {
        return $this->id;
    }

    public function getCodigoVoucher()
    {
        return $this->codigoVoucher;
    }

    public function setCodigoVoucher(string $codigoVoucher)
    {
        $this->codigoVoucher = $codigoVoucher;

        return $this;
    }

    public function getNombresapellidos()
    {
        return $this->nombresapellidos;
    }

    public function setNombresapellidos(string $nombresapellidos)
    {
        $this->nombresapellidos = $nombresapellidos;

        return $this;
    }

    public function getCorreoelectronico()
    {
        return $this->correoelectronico;
    }

    public function setCorreoelectronico(string $correoelectronico)
    {
        $this->correoelectronico = $correoelectronico;

        return $this;
    }

    public function getFecha()
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->fecha = new \DateTime();
    }
  
    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue()
    {
        $this->fecha = new \DateTime();
    }

}
