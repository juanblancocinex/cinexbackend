<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: ListParroquia Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 *
 * @ORM\Table(name="list_parroquia")
 * @ORM\Entity
 */
class ListParroquia
{
    /**
     * @var int
     *
     * @ORM\Column(name="listprr_id", type="integer", nullable=false, options={"comment"="ID de la Parroquia"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $listprrId;

    /**
     * @var string
     *
     * @ORM\Column(name="listprr_codigo", type="string", length=6, nullable=false, options={"fixed"=true,"comment"="Codigo de la Parroquia"})
     */
    private $listprrCodigo;

    /**
     * @var string
     *
     * @ORM\Column(name="listprr_nombre", type="string", length=140, nullable=false, options={"comment"="Nombre o Descripcion de la Parroquia"})
     */
    private $listprrNombre;

    /**
     * @var int|null
     *
     * @ORM\Column(name="listprr_relacion", type="integer", nullable=true)
     */
    private $listprrRelacion;

    public function getListprrId()
    {
        return $this->listprrId;
    }

    public function getListprrCodigo()
    {
        return $this->listprrCodigo;
    }

    public function setListprrCodigo(string $listprrCodigo)
    {
        $this->listprrCodigo = $listprrCodigo;

        return $this;
    }

    public function getListprrNombre()
    {
        return $this->listprrNombre;
    }

    public function setListprrNombre(string $listprrNombre)
    {
        $this->listprrNombre = $listprrNombre;

        return $this;
    }

    public function getListprrRelacion()
    {
        return $this->listprrRelacion;
    }

    public function setListprrRelacion(int $listprrRelacion)
    {
        $this->listprrRelacion = $listprrRelacion;

        return $this;
    }


}
