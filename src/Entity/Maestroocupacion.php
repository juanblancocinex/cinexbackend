<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: Maestroocupacion Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="maestroOcupacion")
 * @ORM\Entity
 */
class Maestroocupacion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ocupacion", type="string", length=45, nullable=true)
     */
    private $ocupacion;

    public function getId()
    {
        return $this->id;
    }

    public function getOcupacion()
    {
        return $this->ocupacion;
    }

    public function setOcupacion(string $ocupacion)
    {
        $this->ocupacion = $ocupacion;

        return $this;
    }


}
