<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description:  VouchersDigitalesTipo Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="vouchers_digitales_tipo")
 * @ORM\Entity(repositoryClass="App\Repository\VouchersDigitalesTipoRepository")
 */
class VouchersDigitalesTipo
{
    /**
     * @var int
     *
     * @ORM\Column(name="cod_tipo_voucher", type="integer", nullable=false, options={"comment"="codigo de tipo de vouchers"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $codTipoVoucher;

    /**
     * @var string|null
     *
     * @ORM\Column(name="categoria_id", type="string", length=255, nullable=true)
     */
    private $categoriaId;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255, nullable=false, options={"comment"="nombre del vouchers"})
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="ruta_front", type="string", length=255, nullable=false, options={"comment"="ruta o nombre de la plantilla"})
     */
    private $rutaFront;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ruta_back", type="string", length=255, nullable=true)
     */
    private $rutaBack;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ruta_enviado", type="string", length=255, nullable=true)
     */
    private $rutaEnviado;

    /**
     * @var string
     *
     * @ORM\Column(name="condiciones", type="text", length=0, nullable=false, options={"comment"="condiciones de uso del vouchers"})
     */
    private $condiciones;

    /**
     * @var string
     *
     * @ORM\Column(name="restricciones", type="text", length=0, nullable=false, options={"comment"="restricciones de uso del vouchers"})
     */
    private $restricciones;

    /**
     * @var string|null
     *
     * @ORM\Column(name="letra", type="string", length=1, nullable=true, options={"fixed"=true})
     */
    private $letra;

    /**
     * @var string|null
     *
     * @ORM\Column(name="formatos_validos", type="string", length=255, nullable=true)
     */
    private $formatosValidos;

    public function getCodTipoVoucher()
    {
        return $this->codTipoVoucher;
    }

    public function getCategoriaId()
    {
        return $this->categoriaId;
    }

    public function setCategoriaId(string $categoriaId)
    {
        $this->categoriaId = $categoriaId;

        return $this;
    }

    public function getDescripcion()
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getRutaFront()
    {
        return $this->rutaFront;
    }

    public function setRutaFront(string $rutaFront)
    {
        $this->rutaFront = $rutaFront;

        return $this;
    }

    public function getRutaBack()
    {
        return $this->rutaBack;
    }

    public function setRutaBack(string $rutaBack)
    {
        $this->rutaBack = $rutaBack;

        return $this;
    }

    public function getRutaEnviado()
    {
        return $this->rutaEnviado;
    }

    public function setRutaEnviado(string $rutaEnviado)
    {
        $this->rutaEnviado = $rutaEnviado;

        return $this;
    }

    public function getCondiciones()
    {
        return $this->condiciones;
    }

    public function setCondiciones(string $condiciones)
    {
        $this->condiciones = $condiciones;

        return $this;
    }

    public function getRestricciones()
    {
        return $this->restricciones;
    }

    public function setRestricciones(string $restricciones)
    {
        $this->restricciones = $restricciones;

        return $this;
    }

    public function getLetra()
    {
        return $this->letra;
    }

    public function setLetra(string $letra)
    {
        $this->letra = $letra;

        return $this;
    }

    public function getFormatosValidos()
    {
        return $this->formatosValidos;
    }

    public function setFormatosValidos(string $formatosValidos)
    {
        $this->formatosValidos = $formatosValidos;

        return $this;
    }


}
