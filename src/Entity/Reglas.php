<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
  *  Summary.
 *
 * Description: Reglas Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="reglas")
 * @ORM\Entity
 */
class Reglas
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="complejoCodigo", type="string", length=50, nullable=true)
     */
    private $complejocodigo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sala", type="string", length=45, nullable=true)
     */
    private $sala;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vigente", type="string", length=1, nullable=true)
     */
    private $vigente;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="desde", type="datetime", nullable=true)
     */
    private $desde;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="hasta", type="datetime", nullable=true)
     */
    private $hasta;

    public function getId()
    {
        return $this->id;
    }

    public function getComplejocodigo()
    {
        return $this->complejocodigo;
    }

    public function setComplejocodigo(string $complejocodigo)
    {
        $this->complejocodigo = $complejocodigo;

        return $this;
    }

    public function getSala()
    {
        return $this->sala;
    }

    public function setSala(string $sala)
    {
        $this->sala = $sala;

        return $this;
    }

    public function getVigente()
    {
        return $this->vigente;
    }

    public function setVigente(string $vigente)
    {
        $this->vigente = $vigente;

        return $this;
    }

    public function getDesde()
    {
        return $this->desde;
    }

    public function setDesde(\DateTimeInterface $desde)
    {
        $this->desde = $desde;

        return $this;
    }

    public function getHasta()
    {
        return $this->hasta;
    }

    public function setHasta(\DateTimeInterface $hasta)
    {
        $this->hasta = $hasta;

        return $this;
    }


}
