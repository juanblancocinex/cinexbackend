<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: Pelicula Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="pelicula", indexes={@ORM\Index(name="nombreSinFormato", columns={"nombreSinFormato"}), @ORM\Index(name="idx_pelicula_codVista", columns={"codVista"}), @ORM\Index(name="idx_referencia", columns={"referencia"}), @ORM\Index(name="idx_referencia_fechaEstreno", columns={"referencia", "fechaEstreno"})})
 * @ORM\Entity
 */
class Pelicula
{
    /**
     * @var int
     *
     * @ORM\Column(name="peliculaID", type="integer", nullable=false, options={"unsigned"=true,"comment"="clave artificial de la pelicula"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $peliculaid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombreSinFormato", type="string", length=100, nullable=true, options={"comment"="nombre de la película sin formato"})
     */
    private $nombresinformato;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre", type="string", length=100, nullable=true, options={"comment"="nombre de la pelicula con formato"})
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombreOriginal", type="string", length=100, nullable=true, options={"comment"="nombre original (en ingles) de la película"})
     */
    private $nombreoriginal;

    /**
     * @var string|null
     *
     * @ORM\Column(name="referencia", type="string", length=100, nullable=true)
     */
    private $referencia;

    /**
     * @var string|null
     *
     * @ORM\Column(name="censura", type="string", length=3, nullable=true, options={"comment"="censura"})
     */
    private $censura;

    /**
     * @var string|null
     *
     * @ORM\Column(name="genero", type="string", length=15, nullable=true, options={"comment"="genero"})
     */
    private $genero;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaEstreno", type="datetime", nullable=true, options={"comment"="fecha estreno"})
     */
    private $fechaestreno;

    /**
     * @var string|null
     *
     * @ORM\Column(name="urlTrailer", type="string", length=500, nullable=true, options={"comment"="url del trailer"})
     */
    private $urltrailer;

    /**
     * @var string|null
     *
     * @ORM\Column(name="urlPoster1", type="string", length=500, nullable=true, options={"comment"="url del poster para cartelera"})
     */
    private $urlposter1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="urlPoster2", type="string", length=500, nullable=true, options={"comment"="url del poster para sinopsis"})
     */
    private $urlposter2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="codVista", type="string", length=20, nullable=true, options={"comment"="codigo de la pelicula en Vista"})
     */
    private $codvista;

    /**
     * @var string|null
     *
     * @ORM\Column(name="codSinec", type="string", length=20, nullable=true, options={"comment"="codigo de la pelicula en Sinec"})
     */
    private $codsinec;

    /**
     * @var string|null
     *
     * @ORM\Column(name="versiones", type="string", length=20, nullable=true, options={"comment"="versiones de la pelicula"})
     */
    private $versiones;

    /**
     * @var string|null
     *
     * @ORM\Column(name="director", type="string", length=50, nullable=true, options={"comment"="director de la pelicula en Vista"})
     */
    private $director;

    /**
     * @var string|null
     *
     * @ORM\Column(name="elenco", type="string", length=150, nullable=true, options={"comment"="elenco de la pelicula en Vista"})
     */
    private $elenco;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tipo", type="string", length=1, nullable=true, options={"comment"="P proximo estreno; C Cartelera"})
     */
    private $tipo;

    /**
     * @var int|null
     *
     * @ORM\Column(name="duracion", type="integer", nullable=true, options={"comment"="duracion de la pelicula"})
     */
    private $duracion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcion", type="text", length=65535, nullable=true, options={"comment"="sinopsis de la pelicula"})
     */
    private $descripcion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="contenido", type="string", length=1500, nullable=true, options={"comment"="version resumida de la sinopsis"})
     */
    private $contenido;

    /**
     * @var string|null
     *
     * @ORM\Column(name="urlPoster3", type="string", length=500, nullable=true)
     */
    private $urlposter3;

    /**
     * @var string|null
     *
     * @ORM\Column(name="urlPoster4", type="string", length=500, nullable=true)
     */
    private $urlposter4;

    /**
     * @var string|null
     *
     * @ORM\Column(name="esDestacada", type="string", length=1, nullable=true)
     */
    private $esdestacada;

    /**
     * @var string|null
     *
     * @ORM\Column(name="esPreVenta", type="string", length=1, nullable=true)
     */
    private $espreventa;

    /**
     * @var string|null
     *
     * @ORM\Column(name="esVenezolana", type="string", length=1, nullable=true)
     */
    private $esvenezolana;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaCreacion", type="datetime", nullable=true)
     */
    private $fechacreacion;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="ultimaModificacion", type="datetime", nullable=true)
     */
    private $ultimamodificacion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="esPromocion", type="string", length=1, nullable=true)
     */
    private $espromocion;

    public function getPeliculaid()
    {
        return $this->peliculaid;
    }

    public function getNombresinformato()
    {
        return $this->nombresinformato;
    }

    public function setNombresinformato(string $nombresinformato)
    {
        $this->nombresinformato = $nombresinformato;

        return $this;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getNombreoriginal()
    {
        return $this->nombreoriginal;
    }

    public function setNombreoriginal(string $nombreoriginal)
    {
        $this->nombreoriginal = $nombreoriginal;

        return $this;
    }

    public function getReferencia()
    {
        return $this->referencia;
    }

    public function setReferencia(string $referencia)
    {
        $this->referencia = $referencia;

        return $this;
    }

    public function getCensura()
    {
        return $this->censura;
    }

    public function setCensura(string $censura)
    {
        $this->censura = $censura;

        return $this;
    }

    public function getGenero()
    {
        return $this->genero;
    }

    public function setGenero(string $genero)
    {
        $this->genero = $genero;

        return $this;
    }

    public function getFechaestreno()
    {
        return $this->fechaestreno;
    }

    public function setFechaestreno(\DateTimeInterface $fechaestreno)
    {
        $this->fechaestreno = $fechaestreno;

        return $this;
    }

    public function getUrltrailer()
    {
        return $this->urltrailer;
    }

    public function setUrltrailer(string $urltrailer)
    {
        $this->urltrailer = $urltrailer;

        return $this;
    }

    public function getUrlposter1()
    {
        return $this->urlposter1;
    }

    public function setUrlposter1(string $urlposter1)
    {
        $this->urlposter1 = $urlposter1;

        return $this;
    }

    public function getUrlposter2()
    {
        return $this->urlposter2;
    }

    public function setUrlposter2(string $urlposter2)
    {
        $this->urlposter2 = $urlposter2;

        return $this;
    }

    public function getCodvista()
    {
        return $this->codvista;
    }

    public function setCodvista(string $codvista)
    {
        $this->codvista = $codvista;

        return $this;
    }

    public function getCodsinec()
    {
        return $this->codsinec;
    }

    public function setCodsinec(string $codsinec)
    {
        $this->codsinec = $codsinec;

        return $this;
    }

    public function getVersiones()
    {
        return $this->versiones;
    }

    public function setVersiones(string $versiones)
    {
        $this->versiones = $versiones;

        return $this;
    }

    public function getDirector()
    {
        return $this->director;
    }

    public function setDirector(string $director)
    {
        $this->director = $director;

        return $this;
    }

    public function getElenco()
    {
        return $this->elenco;
    }

    public function setElenco(string $elenco)
    {
        $this->elenco = $elenco;

        return $this;
    }

    public function getTipo()
    {
        return $this->tipo;
    }

    public function setTipo(string $tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    public function getDuracion()
    {
        return $this->duracion;
    }

    public function setDuracion(int $duracion)
    {
        $this->duracion = $duracion;

        return $this;
    }

    public function getDescripcion()
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getContenido()
    {
        return $this->contenido;
    }

    public function setContenido(string $contenido)
    {
        $this->contenido = $contenido;

        return $this;
    }

    public function getUrlposter3()
    {
        return $this->urlposter3;
    }

    public function setUrlposter3(string $urlposter3)
    {
        $this->urlposter3 = $urlposter3;

        return $this;
    }

    public function getUrlposter4()
    {
        return $this->urlposter4;
    }

    public function setUrlposter4(string $urlposter4)
    {
        $this->urlposter4 = $urlposter4;

        return $this;
    }

    public function getEsdestacada()
    {
        return $this->esdestacada;
    }

    public function setEsdestacada(string $esdestacada)
    {
        $this->esdestacada = $esdestacada;

        return $this;
    }

    public function getEspreventa()
    {
        return $this->espreventa;
    }

    public function setEspreventa(string $espreventa)
    {
        $this->espreventa = $espreventa;

        return $this;
    }

    public function getEsvenezolana()
    {
        return $this->esvenezolana;
    }

    public function setEsvenezolana(string $esvenezolana)
    {
        $this->esvenezolana = $esvenezolana;

        return $this;
    }

    public function getFechacreacion()
    {
        return $this->fechacreacion;
    }

    public function setFechacreacion(\DateTimeInterface $fechacreacion)
    {
        $this->fechacreacion = $fechacreacion;

        return $this;
    }

    public function getUltimamodificacion()
    {
        return $this->ultimamodificacion;
    }

    public function setUltimamodificacion(\DateTimeInterface $ultimamodificacion)
    {
        $this->ultimamodificacion = $ultimamodificacion;

        return $this;
    }

    public function getEspromocion()
    {
        return $this->espromocion;
    }

    public function setEspromocion(string $espromocion)
    {
        $this->espromocion = $espromocion;

        return $this;
    }


}
