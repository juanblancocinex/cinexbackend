<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description:  Contactos Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 * 
 *
 * @ORM\Table(name="contactos")
 * @ORM\Entity
 */
class Contactos
{
    /**
     * @var int
     *
     * @ORM\Column(name="contactoID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $contactoid;

    /**
     * @var int
     *
     * @ORM\Column(name="ciudadID", type="integer", nullable=false)
     */
    private $ciudadid;

    /**
     * @var int
     *
     * @ORM\Column(name="topicoID", type="integer", nullable=false)
     */
    private $topicoid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre", type="string", length=50, nullable=true)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="apellido", type="string", length=50, nullable=true)
     */
    private $apellido;

    /**
     * @var string|null
     *
     * @ORM\Column(name="correoElectronico", type="string", length=100, nullable=true)
     */
    private $correoelectronico;

    /**
     * @var string|null
     *
     * @ORM\Column(name="telefono", type="string", length=15, nullable=true)
     */
    private $telefono;

    /**
     * @var string|null
     *
     * @ORM\Column(name="complejoID", type="string", length=5, nullable=true)
     */
    private $complejoid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="mensaje", type="string", length=2000, nullable=true)
     */
    private $mensaje;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=true)
     */
    private $fecha;

    public function getContactoid()
    {
        return $this->contactoid;
    }

    public function getCiudadid()
    {
        return $this->ciudadid;
    }

    public function setCiudadid(int $ciudadid)
    {
        $this->ciudadid = $ciudadid;

        return $this;
    }

    public function getTopicoid()
    {
        return $this->topicoid;
    }

    public function setTopicoid(int $topicoid)
    {
        $this->topicoid = $topicoid;

        return $this;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getApellido()
    {
        return $this->apellido;
    }

    public function setApellido(string $apellido)
    {
        $this->apellido = $apellido;

        return $this;
    }

    public function getCorreoelectronico()
    {
        return $this->correoelectronico;
    }

    public function setCorreoelectronico(string $correoelectronico)
    {
        $this->correoelectronico = $correoelectronico;

        return $this;
    }

    public function getTelefono()
    {
        return $this->telefono;
    }

    public function setTelefono(string $telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    public function getComplejoid()
    {
        return $this->complejoid;
    }

    public function setComplejoid(string $complejoid)
    {
        $this->complejoid = $complejoid;

        return $this;
    }

    public function getMensaje()
    {
        return $this->mensaje;
    }

    public function setMensaje(string $mensaje)
    {
        $this->mensaje = $mensaje;

        return $this;
    }

    public function getFecha()
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }


}
