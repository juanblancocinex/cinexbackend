<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: Noticias Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 *
 * @ORM\Table(name="noticias", uniqueConstraints={@ORM\UniqueConstraint(name="noticiaID_UNIQUE", columns={"noticiaID"})})
 * @ORM\Entity(repositoryClass="App\Repository\NoticiasRepository")
 */
class Noticias
{
    /**
     * @var int
     *
     * @ORM\Column(name="noticiaID", type="integer", nullable=false, options={"comment"="identificador de la ciudad"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $noticiaid;

    /**
     * @var string
     *
     * @ORM\Column(name="resumen", type="text", length=65535, nullable=false, options={"comment"="nombre de la ciudad"})
     */
    private $resumen;

    /**
     * @var string
     *
     * @ORM\Column(name="contenido", type="text", length=65535, nullable=false, options={"comment"="nombre de la ciudad"})
     */
    private $contenido;

    /**
     * @var string
     *
     * @ORM\Column(name="urlImagen", type="string", length=250, nullable=false)
     */
    private $urlimagen;

    /**
     * @var string
     *
     * @ORM\Column(name="vigente", type="string", length=1, nullable=false, options={"comment"="nombre de la ciudad"})
     */
    private $vigente;

    /**
     * @var string|null
     *
     * @ORM\Column(name="titulo", type="text", length=65535, nullable=true)
     */
    private $titulo;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=true)
     */
    private $fecha;

    /**
     * @var string|null
     *
     * @ORM\Column(name="destacada", type="string", length=1, nullable=true)
     */
    private $destacada;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombreRedactor", type="string", length=45, nullable=true)
     */
    private $nombreredactor;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tags", type="string", length=2, nullable=true)
     */
    private $tags;

    /**
     * @var string|null
     *
     * @ORM\Column(name="imagen", type="string", length=250, nullable=true)
     */
    private $imagen;

    /**
     * @var string|null
     *
     * @ORM\Column(name="utm_source", type="string", length=140, nullable=true)
     */
    private $utmSource;

    /**
     * @var string|null
     *
     * @ORM\Column(name="utm_medium", type="string", length=140, nullable=true)
     */
    private $utmMedium;

    /**
     * @var string|null
     *
     * @ORM\Column(name="utm_campaing", type="string", length=140, nullable=true)
     */
    private $utmCampaing;

    /**
     * @var string|null
     *
     * @ORM\Column(name="utm_content", type="string", length=140, nullable=true)
     */
    private $utmContent;

    /**
     * @var string|null
     *
     * @ORM\Column(name="utm_term", type="string", length=140, nullable=true)
     */
    private $utmTerm;

    public function getNoticiaid()
    {
        return $this->noticiaid;
    }

    public function getResumen()
    {
        return $this->resumen;
    }

    public function setResumen(string $resumen)
    {
        $this->resumen = $resumen;

        return $this;
    }

    public function getContenido()
    {
        return $this->contenido;
    }

    public function setContenido(string $contenido)
    {
        $this->contenido = $contenido;

        return $this;
    }

    public function getUrlimagen()
    {
        return $this->urlimagen;
    }

    public function setUrlimagen(string $urlimagen)
    {
        $this->urlimagen = $urlimagen;

        return $this;
    }

    public function getVigente()
    {
        return $this->vigente;
    }

    public function setVigente(string $vigente)
    {
        $this->vigente = $vigente;

        return $this;
    }

    public function getTitulo()
    {
        return $this->titulo;
    }

    public function setTitulo(string $titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    public function getFecha()
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getDestacada()
    {
        return $this->destacada;
    }

    public function setDestacada(string $destacada)
    {
        $this->destacada = $destacada;

        return $this;
    }

    public function getNombreredactor()
    {
        return $this->nombreredactor;
    }

    public function setNombreredactor(string $nombreredactor)
    {
        $this->nombreredactor = $nombreredactor;

        return $this;
    }

    public function getTags()
    {
        return $this->tags;
    }

    public function setTags(string $tags)
    {
        $this->tags = $tags;

        return $this;
    }

    public function getImagen()
    {
        return $this->imagen;
    }

    public function setImagen(string $imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    public function getUtmSource()
    {
        return $this->utmSource;
    }

    public function setUtmSource(string $utmSource)
    {
        $this->utmSource = $utmSource;

        return $this;
    }

    public function getUtmMedium()
    {
        return $this->utmMedium;
    }

    public function setUtmMedium(string $utmMedium)
    {
        $this->utmMedium = $utmMedium;

        return $this;
    }

    public function getUtmCampaing()
    {
        return $this->utmCampaing;
    }

    public function setUtmCampaing(string $utmCampaing)
    {
        $this->utmCampaing = $utmCampaing;

        return $this;
    }

    public function getUtmContent()
    {
        return $this->utmContent;
    }

    public function setUtmContent(string $utmContent)
    {
        $this->utmContent = $utmContent;

        return $this;
    }

    public function getUtmTerm()
    {
        return $this->utmTerm;
    }

    public function setUtmTerm(string $utmTerm)
    {
        $this->utmTerm = $utmTerm;

        return $this;
    }


}
