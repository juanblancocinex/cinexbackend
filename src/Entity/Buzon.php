<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* Summary.
 *
 * Description:  Buzon Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="buzon")
 * @ORM\Entity(repositoryClass="App\Repository\BuzonRepository")
 */
class Buzon
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="usuarioid", type="integer", nullable=true)
     */
    private $usuarioid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="uservistaid", type="string", length=45, nullable=true)
     */
    private $uservistaid;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tipomensajeid", type="integer", nullable=true)
     */
    private $tipomensajeid;

    /**
     * @var int|null
     *
     * @ORM\Column(name="transaccionID", type="integer", nullable=true)
     */
    private $transaccionid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="contenido", type="text", length=65535, nullable=true)
     */
    private $contenido;

    /**
     * @var string|null
     *
     * @ORM\Column(name="asunto", type="text", length=65535, nullable=true)
     */
    private $asunto;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="lectura", type="boolean", nullable=true)
     */
    private $lectura = '0';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="oculto", type="boolean", nullable=true)
     */
    private $oculto = '0';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="borrado", type="boolean", nullable=true)
     */
    private $borrado = '0';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=true)
     */
    private $fecha;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechalectura", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $fechalectura = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechasistema", type="datetime", nullable=true)
     */
    private $fechasistema;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CI", type="string", length=15, nullable=true)
     */
    private $ci;

    public function getId()
    {
        return $this->id;
    }

    public function getUsuarioid()
    {
        return $this->usuarioid;
    }

    public function setUsuarioid(int $usuarioid)
    {
        $this->usuarioid = $usuarioid;

        return $this;
    }

    public function getUservistaid()
    {
        return $this->uservistaid;
    }

    public function setUservistaid(string $uservistaid)
    {
        $this->uservistaid = $uservistaid;

        return $this;
    }

    public function getTipomensajeid()
    {
        return $this->tipomensajeid;
    }

    public function setTipomensajeid(int $tipomensajeid)
    {
        $this->tipomensajeid = $tipomensajeid;

        return $this;
    }

    public function getTransaccionid()
    {
        return $this->transaccionid;
    }

    public function setTransaccionid(int $transaccionid)
    {
        $this->transaccionid = $transaccionid;

        return $this;
    }

    public function getContenido()
    {
        return $this->contenido;
    }

    public function setContenido(string $contenido)
    {
        $this->contenido = $contenido;

        return $this;
    }

    public function getAsunto()
    {
        return $this->asunto;
    }

    public function setAsunto(string $asunto)
    {
        $this->asunto = $asunto;

        return $this;
    }

    public function getLectura()
    {
        return $this->lectura;
    }

    public function setLectura(bool $lectura)
    {
        $this->lectura = $lectura;

        return $this;
    }

    public function getOculto()
    {
        return $this->oculto;
    }

    public function setOculto(bool $oculto)
    {
        $this->oculto = $oculto;

        return $this;
    }

    public function getBorrado()
    {
        return $this->borrado;
    }

    public function setBorrado(bool $borrado)
    {
        $this->borrado = $borrado;

        return $this;
    }

    public function getFecha()
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getFechalectura()
    {
        return $this->fechalectura;
    }

    public function setFechalectura(\DateTimeInterface $fechalectura)
    {
        $this->fechalectura = $fechalectura;

        return $this;
    }

    public function getFechasistema()
    {
        return $this->fechasistema;
    }

    public function setFechasistema(\DateTimeInterface $fechasistema)
    {
        $this->fechasistema = $fechasistema;

        return $this;
    }

    public function getCi()
    {
        return $this->ci;
    }

    public function setCi(string $ci)
    {
        $this->ci = $ci;

        return $this;
    }
}
