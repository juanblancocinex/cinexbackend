<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
  *  Summary.
 *
 * Description: RespuestasDesafio Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="respuestas_desafio")
 * @ORM\Entity
 */
class RespuestasDesafio
{
    /**
     * @var int
     *
     * @ORM\Column(name="respuesta_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $respuestaId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="respuesta_desc", type="string", length=255, nullable=true)
     */
    private $respuestaDesc;

    public function getRespuestaId()
    {
        return $this->respuestaId;
    }

    public function getRespuestaDesc()
    {
        return $this->respuestaDesc;
    }

    public function setRespuestaDesc(string $respuestaDesc)
    {
        $this->respuestaDesc = $respuestaDesc;

        return $this;
    }


}
