<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * * Summary.
 *
 * Description:  Contactomotivos Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 * 
 *
 * @ORM\Table(name="contactomotivos")
 * @ORM\Entity
 */
class Contactomotivos
{
    /**
     * @var int
     *
     * @ORM\Column(name="idcontactomotivos", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcontactomotivos;

    /**
     * @var string|null
     *
     * @ORM\Column(name="contactomotivosdescripcion", type="string", length=255, nullable=true)
     */
    private $contactomotivosdescripcion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="contactomotivosactivo", type="string", length=1, nullable=true, options={"fixed"=true})
     */
    private $contactomotivosactivo;

    public function getIdcontactomotivos()
    {
        return $this->idcontactomotivos;
    }

    public function getContactomotivosdescripcion()
    {
        return $this->contactomotivosdescripcion;
    }

    public function setContactomotivosdescripcion(string $contactomotivosdescripcion)
    {
        $this->contactomotivosdescripcion = $contactomotivosdescripcion;

        return $this;
    }

    public function getContactomotivosactivo()
    {
        return $this->contactomotivosactivo;
    }

    public function setContactomotivosactivo(string $contactomotivosactivo)
    {
        $this->contactomotivosactivo = $contactomotivosactivo;

        return $this;
    }


}
