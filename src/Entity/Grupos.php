<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description:  Grupos Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="grupos")
 * @ORM\Entity
 */
class Grupos
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=250, nullable=false)
     */
    private $nombre;

    /**
     * @var int
     *
     * @ORM\Column(name="estado", type="integer", nullable=false, options={"default"="1","unsigned"=true})
     */
    private $estado = '1';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_registro", type="date", nullable=false)
     */
    private $fechaRegistro;

    /**
     * @var int
     *
     * @ORM\Column(name="usuario_registro", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $usuarioRegistro = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_actualizacion", type="date", nullable=false)
     */
    private $fechaActualizacion;

    /**
     * @var int
     *
     * @ORM\Column(name="usuario_actualizacion", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $usuarioActualizacion = '0';

    public function getId()
    {
        return $this->id;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getEstado()
    {
        return $this->estado;
    }

    public function setEstado(int $estado)
    {
        $this->estado = $estado;

        return $this;
    }

    public function getFechaRegistro()
    {
        return $this->fechaRegistro;
    }

    public function setFechaRegistro(\DateTimeInterface $fechaRegistro)
    {
        $this->fechaRegistro = $fechaRegistro;

        return $this;
    }

    public function getUsuarioRegistro()
    {
        return $this->usuarioRegistro;
    }

    public function setUsuarioRegistro(int $usuarioRegistro)
    {
        $this->usuarioRegistro = $usuarioRegistro;

        return $this;
    }

    public function getFechaActualizacion()
    {
        return $this->fechaActualizacion;
    }

    public function setFechaActualizacion(\DateTimeInterface $fechaActualizacion)
    {
        $this->fechaActualizacion = $fechaActualizacion;

        return $this;
    }

    public function getUsuarioActualizacion()
    {
        return $this->usuarioActualizacion;
    }

    public function setUsuarioActualizacion(int $usuarioActualizacion)
    {
        $this->usuarioActualizacion = $usuarioActualizacion;

        return $this;
    }


}
