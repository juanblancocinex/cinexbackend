<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: PeliculaBlockeada Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="pelicula_blockeada", uniqueConstraints={@ORM\UniqueConstraint(name="idx_pelicula_blockeada_peliculaID", columns={"peliculaID"})})
 * @ORM\Entity
 */
class PeliculaBlockeada
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codvista", type="string", length=255, nullable=false)
     */
    private $codvista = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="codsinec", type="string", length=255, nullable=false)
     */
    private $codsinec = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="nombresinformato", type="text", length=65535, nullable=false)
     */
    private $nombresinformato;

    /**
     * @var string|null
     *
     * @ORM\Column(name="bloqueocms", type="string", length=1, nullable=true, options={"fixed"=true})
     */
    private $bloqueocms;

    /**
     * @var int|null
     *
     * @ORM\Column(name="peliculaID", type="integer", nullable=true)
     */
    private $peliculaid;

    public function getId()
    {
        return $this->id;
    }

    public function getCodvista()
    {
        return $this->codvista;
    }

    public function setCodvista(string $codvista)
    {
        $this->codvista = $codvista;

        return $this;
    }

    public function getCodsinec()
    {
        return $this->codsinec;
    }

    public function setCodsinec(string $codsinec)
    {
        $this->codsinec = $codsinec;

        return $this;
    }

    public function getNombresinformato()
    {
        return $this->nombresinformato;
    }

    public function setNombresinformato(string $nombresinformato)
    {
        $this->nombresinformato = $nombresinformato;

        return $this;
    }

    public function getBloqueocms()
    {
        return $this->bloqueocms;
    }

    public function setBloqueocms(string $bloqueocms)
    {
        $this->bloqueocms = $bloqueocms;

        return $this;
    }

    public function getPeliculaid()
    {
        return $this->peliculaid;
    }

    public function setPeliculaid(int $peliculaid)
    {
        $this->peliculaid = $peliculaid;

        return $this;
    }


}
