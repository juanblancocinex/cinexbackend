<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: SettingsPayment Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="settings_payment")
 * @ORM\Entity(repositoryClass="App\Repository\SettingsPaymentRepository")
 * 
 */
class SettingsPayment
{
    /**
     * @var int
     *
     * @ORM\Column(name="setting_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $settingId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="setting_desc", type="text", length=65535, nullable=true)
     */
    private $settingDesc;

    /**
     * @var string|null
     *
     * @ORM\Column(name="setting_value", type="text", length=0, nullable=true)
     */
    private $settingValue;

    public function getSettingId()
    {
        return $this->settingId;
    }

    public function getSettingDesc()
    {
        return $this->settingDesc;
    }

    public function setSettingDesc(string $settingDesc)
    {
        $this->settingDesc = $settingDesc;

        return $this;
    }

    public function getSettingValue()
    {
        return $this->settingValue;
    }

    public function setSettingValue(string $settingValue)
    {
        $this->settingValue = $settingValue;

        return $this;
    }


}
