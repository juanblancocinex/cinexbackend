<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description:  FormatosPeliculas Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="formatos_peliculas")
 * @ORM\Entity
 */
class FormatosPeliculas
{
    /**
     * @var int
     *
     * @ORM\Column(name="idformatos_peliculas", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idformatosPeliculas;

    /**
     * @var string|null
     *
     * @ORM\Column(name="formato", type="string", length=45, nullable=true)
     */
    private $formato;

    public function getIdformatosPeliculas()
    {
        return $this->idformatosPeliculas;
    }

    public function getFormato()
    {
        return $this->formato;
    }

    public function setFormato(string $formato)
    {
        $this->formato = $formato;

        return $this;
    }


}
