<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: ImagenesEspeciales Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="imagenes_especiales")
 * @ORM\Entity
 */
class ImagenesEspeciales
{
    /**
     * @var int
     *
     * @ORM\Column(name="img_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $imgId;

    /**
     * @var string
     *
     * @ORM\Column(name="img_nombre", type="string", length=255, nullable=false)
     */
    private $imgNombre;

    /**
     * @var string
     *
     * @ORM\Column(name="img_img", type="string", length=255, nullable=false)
     */
    private $imgImg;

    /**
     * @var string
     *
     * @ORM\Column(name="img_link", type="string", length=255, nullable=false, options={"default"="#"})
     */
    private $imgLink = '#';

    /**
     * @var string
     *
     * @ORM\Column(name="img_target", type="string", length=255, nullable=false, options={"default"="_SELF"})
     */
    private $imgTarget = '_SELF';

    /**
     * @var int
     *
     * @ORM\Column(name="img_orden", type="integer", nullable=false)
     */
    private $imgOrden = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="img_activo", type="string", length=1, nullable=false, options={"default"="S","fixed"=true})
     */
    private $imgActivo = 'S';

    public function getImgId()
    {
        return $this->imgId;
    }

    public function getImgNombre()
    {
        return $this->imgNombre;
    }

    public function setImgNombre(string $imgNombre)
    {
        $this->imgNombre = $imgNombre;

        return $this;
    }

    public function getImgImg()
    {
        return $this->imgImg;
    }

    public function setImgImg(string $imgImg)
    {
        $this->imgImg = $imgImg;

        return $this;
    }

    public function getImgLink()
    {
        return $this->imgLink;
    }

    public function setImgLink(string $imgLink)
    {
        $this->imgLink = $imgLink;

        return $this;
    }

    public function getImgTarget()
    {
        return $this->imgTarget;
    }

    public function setImgTarget(string $imgTarget)
    {
        $this->imgTarget = $imgTarget;

        return $this;
    }

    public function getImgOrden()
    {
        return $this->imgOrden;
    }

    public function setImgOrden(int $imgOrden)
    {
        $this->imgOrden = $imgOrden;

        return $this;
    }

    public function getImgActivo()
    {
        return $this->imgActivo;
    }

    public function setImgActivo(string $imgActivo)
    {
        $this->imgActivo = $imgActivo;

        return $this;
    }


}
