<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description:  EstadisticaPublicidad Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 *
 * @ORM\Table(name="estadistica_publicidad")
 * @ORM\Entity
 */
class EstadisticaPublicidad
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="evento", type="integer", nullable=false)
     */
    private $evento;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=false)
     */
    private $fecha;

    /**
     * @var int
     *
     * @ORM\Column(name="zona", type="integer", nullable=false)
     */
    private $zona;

    /**
     * @var int
     *
     * @ORM\Column(name="pagina", type="integer", nullable=false)
     */
    private $pagina;

    public function getId()
    {
        return $this->id;
    }

    public function getEvento()
    {
        return $this->evento;
    }

    public function setEvento(int $evento)
    {
        $this->evento = $evento;

        return $this;
    }

    public function getFecha()
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getZona()
    {
        return $this->zona;
    }

    public function setZona(int $zona)
    {
        $this->zona = $zona;

        return $this;
    }

    public function getPagina()
    {
        return $this->pagina;
    }

    public function setPagina(int $pagina)
    {
        $this->pagina = $pagina;

        return $this;
    }


}
