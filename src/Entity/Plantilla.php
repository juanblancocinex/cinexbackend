<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: Plantilla Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="plantilla")
 * @ORM\Entity(repositoryClass="App\Repository\PlantillaRepository") 
 */
class Plantilla
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tipomensajeid", type="integer", nullable=true)
     */
    private $tipomensajeid;

    /**
     * @var int|null
     *
     * @ORM\Column(name="cms_usuariocreacionid", type="integer", nullable=true)
     */
    private $cmsUsuariocreacionid;

    /**
     * @var int|null
     *
     * @ORM\Column(name="cms_usuariomodificacionid", type="integer", nullable=true)
     */
    private $cmsUsuariomodificacionid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombreplantilla", type="string", length=255, nullable=true)
     */
    private $nombreplantilla;

    /**
     * @var string|null
     *
     * @ORM\Column(name="contenido", type="text", length=0, nullable=true)
     */
    private $contenido;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechacreacion", type="datetime", nullable=true)
     */
    private $fechacreacion;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
     */
    private $fechamodificacion;

    public function getId()
    {
        return $this->id;
    }

    public function getTipomensajeid()
    {
        return $this->tipomensajeid;
    }

    public function setTipomensajeid(int $tipomensajeid)
    {
        $this->tipomensajeid = $tipomensajeid;

        return $this;
    }

    public function getCmsUsuariocreacionid()
    {
        return $this->cmsUsuariocreacionid;
    }

    public function setCmsUsuariocreacionid(int $cmsUsuariocreacionid)
    {
        $this->cmsUsuariocreacionid = $cmsUsuariocreacionid;

        return $this;
    }

    public function getCmsUsuariomodificacionid()
    {
        return $this->cmsUsuariomodificacionid;
    }

    public function setCmsUsuariomodificacionid(int $cmsUsuariomodificacionid)
    {
        $this->cmsUsuariomodificacionid = $cmsUsuariomodificacionid;

        return $this;
    }

    public function getNombreplantilla()
    {
        return $this->nombreplantilla;
    }

    public function setNombreplantilla(string $nombreplantilla)
    {
        $this->nombreplantilla = $nombreplantilla;

        return $this;
    }

    public function getContenido()
    {
        return $this->contenido;
    }

    public function setContenido(string $contenido)
    {
        $this->contenido = $contenido;

        return $this;
    }

    public function getFechacreacion()
    {
        return $this->fechacreacion;
    }

    public function setFechacreacion(\DateTimeInterface $fechacreacion)
    {
        $this->fechacreacion = $fechacreacion;

        return $this;
    }

    public function getFechamodificacion()
    {
        return $this->fechamodificacion;
    }

    public function setFechamodificacion(\DateTimeInterface $fechamodificacion)
    {
        $this->fechamodificacion = $fechamodificacion;

        return $this;
    }


}
