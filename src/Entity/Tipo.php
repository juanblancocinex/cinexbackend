<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: Tipo Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="tipo", indexes={@ORM\Index(name="fk_tipo_producto", columns={"id_prod"})})
 * @ORM\Entity
 */
class Tipo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=100, nullable=false)
     */
    private $descripcion;

    /**
     * @var int
     *
     * @ORM\Column(name="sell_one", type="integer", nullable=false)
     */
    private $sellOne = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="comentario", type="string", length=1000, nullable=false)
     */
    private $comentario = '';

    /**
     * @var int
     *
     * @ORM\Column(name="id_prod", type="integer", nullable=false)
     */
    private $idProd;

    public function getId()
    {
        return $this->id;
    }

    public function getDescripcion()
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getSellOne()
    {
        return $this->sellOne;
    }

    public function setSellOne(int $sellOne)
    {
        $this->sellOne = $sellOne;

        return $this;
    }

    public function getComentario()
    {
        return $this->comentario;
    }

    public function setComentario(string $comentario)
    {
        $this->comentario = $comentario;

        return $this;
    }

    public function getIdProd()
    {
        return $this->idProd;
    }

    public function setIdProd(int $idProd)
    {
        $this->idProd = $idProd;

        return $this;
    }


}
