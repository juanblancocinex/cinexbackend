<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: TokenTrans Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="token_trans")
 * @ORM\Entity(repositoryClass="App\Repository\TokenTransRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class TokenTrans
{
    /**
     * @var int
     *
     * @ORM\Column(name="token_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $tokenId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="token_token", type="string", length=255, nullable=true)
     */
    private $tokenToken;

    /**
     * @var int|null
     *
     * @ORM\Column(name="token_userid", type="integer", nullable=true)
     */
    private $tokenUserid;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="token_datetime", type="datetime", nullable=true)
     */
    private $tokenDatetime;

    public function getTokenId()
    {
        return $this->tokenId;
    }

    public function getTokenToken()
    {
        return $this->tokenToken;
    }

    public function setTokenToken(string $tokenToken)
    {
        $this->tokenToken = $tokenToken;

        return $this;
    }

    public function getTokenUserid()
    {
        return $this->tokenUserid;
    }

    public function setTokenUserid(int $tokenUserid)
    {
        $this->tokenUserid = $tokenUserid;

        return $this;
    }

    public function getTokenDatetime()
    {
        return $this->tokenDatetime;
    }

    public function setTokenDatetime(\DateTimeInterface $tokenDatetime)
    {
        $this->tokenDatetime = $tokenDatetime;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->tokenDatetime = new \DateTime();
    }
    
    
}
