<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: PreguntasDesafio Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="preguntas_desafio")
 * @ORM\Entity
 */
class PreguntasDesafio
{
    /**
     * @var int
     *
     * @ORM\Column(name="pregunta_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $preguntaId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="pregunta_desc", type="string", length=255, nullable=true)
     */
    private $preguntaDesc;

    public function getPreguntaId()
    {
        return $this->preguntaId;
    }

    public function getPreguntaDesc()
    {
        return $this->preguntaDesc;
    }

    public function setPreguntaDesc(string $preguntaDesc)
    {
        $this->preguntaDesc = $preguntaDesc;

        return $this;
    }


}
