<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Summary.
 *
 * Description:  CmsPerfiles Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="cms_perfiles")
 * @ORM\Entity
 */
class CmsPerfiles
{
    /**
     * @var int
     *
     * @ORM\Column(name="perfil_id", type="integer", nullable=false, options={"comment"="id autoincremental"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $perfilId;

    /**
     * @var string
     *
     * @ORM\Column(name="perfil_nombre", type="string", length=255, nullable=false, options={"comment"="nombre"})
     */
    private $perfilNombre;

    /**
     * @var string
     *
     * @ORM\Column(name="perfil_permisos", type="text", length=0, nullable=false, options={"comment"="contiene los id de los submenu permitidos para cada perfil"})
     */
    private $perfilPermisos;

    /**
     * @var string
     *
     * @ORM\Column(name="perfil_activo", type="string", length=1, nullable=false, options={"default"="S","fixed"=true,"comment"="S=ACTIVO;N=INACTIVO"})
     */
    private $perfilActivo = 'S';

    public function getPerfilId()
    {
        return $this->perfilId;
    }

    public function getPerfilNombre()
    {
        return $this->perfilNombre;
    }

    public function setPerfilNombre(string $perfilNombre)
    {
        $this->perfilNombre = $perfilNombre;

        return $this;
    }

    public function getPerfilPermisos()
    {
        return $this->perfilPermisos;
    }

    public function setPerfilPermisos(string $perfilPermisos)
    {
        $this->perfilPermisos = $perfilPermisos;

        return $this;
    }

    public function getPerfilActivo()
    {
        return $this->perfilActivo;
    }

    public function setPerfilActivo(string $perfilActivo)
    {
        $this->perfilActivo = $perfilActivo;

        return $this;
    }


}
