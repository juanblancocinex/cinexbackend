<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: ProgramacionProgramacionTest Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="programacion_test")
 * @ORM\Entity(repositoryClass="App\Repository\ProgramacionTestRepository")
 */
class ProgramacionTest
{
    /**
     * @var int
     *
     * @ORM\Column(name="programacionID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $programacionid;



    /**
     * Many Programaciones have One Complejo.
     * @ORM\ManyToOne(targetEntity="Complejo", inversedBy="programaciones")
     * @ORM\JoinColumn(name="codigoComplejo", referencedColumnName="codigoComplejo")
     */
    private $codigocomplejo;



    /**
     * Many Programaciones have One Pelicula.
     * @ORM\ManyToOne(targetEntity="PeliculaTest", inversedBy="programaciones")
     * @ORM\JoinColumn(name="codigoPelicula", referencedColumnName="codVista", nullable=true)
     */
    private $codigopelicula;


    /**
     * @var int|null
     *
     * @ORM\Column(name="sesionID", type="integer", nullable=true)
     */
    private $sesionid;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha_programacion", type="datetime", nullable=true)
     */
    private $fechaProgramacion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sala", type="string", length=20, nullable=true)
     */
    private $sala;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hora", type="string", length=10, nullable=true)
     */
    private $hora;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ventaActiva", type="string", length=1, nullable=true)
     */
    private $ventaactiva;

    /**
     * @var string|null
     *
     * @ORM\Column(name="seleccionAsientosActiva", type="string", length=1, nullable=true)
     */
    private $seleccionasientosactiva;

    /**
     * @var string|null
     *
     * @ORM\Column(name="codigoNativa", type="string", length=5, nullable=true)
     */
    private $codigonativa;

    /**
     * @var string|null
     *
     * @ORM\Column(name="censura", type="string", length=5, nullable=true)
     */
    private $censura;

    /**
     * @var string|null
     *
     * @ORM\Column(name="activo", type="string", length=1, nullable=true)
     */
    private $activo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="activa", type="string", length=1, nullable=true)
     */
    private $activa;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ventaActivaOriginal", type="string", length=1, nullable=true)
     */
    private $ventaactivaoriginal;

    public function getProgramacionid()
    {
        return $this->programacionid;
    }

    public function getSesionid()
    {
        return $this->sesionid;
    }

    public function setSesionid(int $sesionid)
    {
        $this->sesionid = $sesionid;

        return $this;
    }

    public function getFechaProgramacion()
    {
        return $this->fechaProgramacion;
    }

    public function setFechaProgramacion(\DateTimeInterface $fechaProgramacion)
    {
        $this->fechaProgramacion = $fechaProgramacion;

        return $this;
    }

    public function getSala()
    {
        return $this->sala;
    }

    public function setSala(string $sala)
    {
        $this->sala = $sala;

        return $this;
    }

    public function getHora()
    {
        return $this->hora;
    }

    public function setHora(string $hora)
    {
        $this->hora = $hora;

        return $this;
    }

    public function getVentaactiva()
    {
        return $this->ventaactiva;
    }

    public function setVentaactiva(string $ventaactiva)
    {
        $this->ventaactiva = $ventaactiva;

        return $this;
    }

    public function getSeleccionasientosactiva()
    {
        return $this->seleccionasientosactiva;
    }

    public function setSeleccionasientosactiva(string $seleccionasientosactiva)
    {
        $this->seleccionasientosactiva = $seleccionasientosactiva;

        return $this;
    }

    public function getCodigonativa()
    {
        return $this->codigonativa;
    }

    public function setCodigonativa(string $codigonativa)
    {
        $this->codigonativa = $codigonativa;

        return $this;
    }

    public function getCensura()
    {
        return $this->censura;
    }

    public function setCensura(string $censura)
    {
        $this->censura = $censura;

        return $this;
    }

    public function getActivo()
    {
        return $this->activo;
    }

    public function setActivo(string $activo)
    {
        $this->activo = $activo;

        return $this;
    }

    public function getActiva()
    {
        return $this->activa;
    }

    public function setActiva(string $activa)
    {
        $this->activa = $activa;

        return $this;
    }

    public function getVentaactivaoriginal()
    {
        return $this->ventaactivaoriginal;
    }

    public function setVentaactivaoriginal(string $ventaactivaoriginal)
    {
        $this->ventaactivaoriginal = $ventaactivaoriginal;

        return $this;
    }

    public function getCodigocomplejo()
    {
        return $this->codigocomplejo;
    }

    public function setCodigocomplejo(Complejo $codigocomplejo)
    {
        $this->codigocomplejo = $codigocomplejo;

        return $this;
    }

    public function getCodigopelicula()
    {
        return $this->codigopelicula;
    }

    public function setCodigopelicula(PeliculaTest $codigopelicula)
    {
        $this->codigopelicula = $codigopelicula;

        return $this;
    }

    


}
