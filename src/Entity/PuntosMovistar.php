<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: PuntosMovistar Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 *
 * @ORM\Table(name="puntos_movistar")
 * @ORM\Entity
 */
class PuntosMovistar
{
    /**
     * @var int
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tasa", type="string", length=255, nullable=true)
     */
    private $tasa;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="lastUpdate", type="datetime", nullable=true)
     */
    private $lastupdate;

    public function getId()
    {
        return $this->id;
    }

    public function getTasa()
    {
        return $this->tasa;
    }

    public function setTasa(string $tasa)
    {
        $this->tasa = $tasa;

        return $this;
    }

    public function getLastupdate()
    {
        return $this->lastupdate;
    }

    public function setLastupdate(\DateTimeInterface $lastupdate)
    {
        $this->lastupdate = $lastupdate;

        return $this;
    }


}
