<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Summary.
 *
 * Description:  CmsSubmenus Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 *
 * @ORM\Table(name="cms_submenus")
 * @ORM\Entity
 */
class CmsSubmenus
{
    /**
     * @var int
     *
     * @ORM\Column(name="submenu_id", type="integer", nullable=false, options={"comment"="id autoincremental"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $submenuId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="menu_id", type="integer", nullable=true, options={"comment"="id del menu"})
     */
    private $menuId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="submenu_nombre", type="string", length=255, nullable=true, options={"comment"="nombre"})
     */
    private $submenuNombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="submenu_modulo", type="string", length=255, nullable=true, options={"comment"="nombre del modulo"})
     */
    private $submenuModulo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="submenu_activo", type="string", length=1, nullable=true, options={"fixed"=true,"comment"="S=ACTIVO;N=INACTIVO"})
     */
    private $submenuActivo;

    /**
     * @var int
     *
     * @ORM\Column(name="submenu_orden", type="integer", nullable=false)
     */
    private $submenuOrden = '0';

    public function getSubmenuId()
    {
        return $this->submenuId;
    }

    public function getMenuId()
    {
        return $this->menuId;
    }

    public function setMenuId(int $menuId)
    {
        $this->menuId = $menuId;

        return $this;
    }

    public function getSubmenuNombre(): string
    {
        return $this->submenuNombre;
    }

    public function setSubmenuNombre(string $submenuNombre)
    {
        $this->submenuNombre = $submenuNombre;

        return $this;
    }

    public function getSubmenuModulo(): string
    {
        return $this->submenuModulo;
    }

    public function setSubmenuModulo(string $submenuModulo)
    {
        $this->submenuModulo = $submenuModulo;

        return $this;
    }

    public function getSubmenuActivo(): string
    {
        return $this->submenuActivo;
    }

    public function setSubmenuActivo(string $submenuActivo)
    {
        $this->submenuActivo = $submenuActivo;

        return $this;
    }

    public function getSubmenuOrden()
    {
        return $this->submenuOrden;
    }

    public function setSubmenuOrden(int $submenuOrden)
    {
        $this->submenuOrden = $submenuOrden;

        return $this;
    }


}
