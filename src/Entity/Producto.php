<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: Producto Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="producto", indexes={@ORM\Index(name="nombre", columns={"nombre"}), @ORM\Index(name="fk_producto_categoria", columns={"id_categoria", "nombre"})})
 * @ORM\Entity
 */
class Producto
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=45, nullable=false)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=150, nullable=false)
     */
    private $descripcion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_ini", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $fechaIni = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha_fin", type="datetime", nullable=true)
     */
    private $fechaFin;

    /**
     * @var bool
     *
     * @ORM\Column(name="activo", type="boolean", nullable=false, options={"default"="1"})
     */
    private $activo = '1';

    /**
     * @var int
     *
     * @ORM\Column(name="id_categoria", type="integer", nullable=false)
     */
    private $idCategoria;

    public function getId()
    {
        return $this->id;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getDescripcion()
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getFechaIni()
    {
        return $this->fechaIni;
    }

    public function setFechaIni(\DateTimeInterface $fechaIni)
    {
        $this->fechaIni = $fechaIni;

        return $this;
    }

    public function getFechaFin()
    {
        return $this->fechaFin;
    }

    public function setFechaFin(\DateTimeInterface $fechaFin)
    {
        $this->fechaFin = $fechaFin;

        return $this;
    }

    public function getActivo()
    {
        return $this->activo;
    }

    public function setActivo(bool $activo)
    {
        $this->activo = $activo;

        return $this;
    }

    public function getIdCategoria()
    {
        return $this->idCategoria;
    }

    public function setIdCategoria(int $idCategoria)
    {
        $this->idCategoria = $idCategoria;

        return $this;
    }


}
