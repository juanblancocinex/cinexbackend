<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: SyncExecuted Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="sync_executed")
 * @ORM\Entity
 */
class SyncExecuted
{
    /**
     * @var int
     *
     * @ORM\Column(name="sync_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $syncId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="usuario_username", type="string", length=255, nullable=true)
     */
    private $usuarioUsername;

    /**
     * @var int|null
     *
     * @ORM\Column(name="sync_type", type="integer", nullable=true)
     */
    private $syncType;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="sync_fechahora", type="datetime", nullable=true)
     */
    private $syncFechahora;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sync_activa", type="string", length=1, nullable=true, options={"default"="N","fixed"=true})
     */
    private $syncActiva = 'N';

    public function getSyncId()
    {
        return $this->syncId;
    }

    public function getUsuarioUsername()
    {
        return $this->usuarioUsername;
    }

    public function setUsuarioUsername(string $usuarioUsername)
    {
        $this->usuarioUsername = $usuarioUsername;

        return $this;
    }

    public function getSyncType()
    {
        return $this->syncType;
    }

    public function setSyncType(int $syncType)
    {
        $this->syncType = $syncType;

        return $this;
    }

    public function getSyncFechahora()
    {
        return $this->syncFechahora;
    }

    public function setSyncFechahora(\DateTimeInterface $syncFechahora)
    {
        $this->syncFechahora = $syncFechahora;

        return $this;
    }

    public function getSyncActiva()
    {
        return $this->syncActiva;
    }

    public function setSyncActiva(string $syncActiva)
    {
        $this->syncActiva = $syncActiva;

        return $this;
    }


}
