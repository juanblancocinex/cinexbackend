<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: PagosImagenes Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="pagos_imagenes")
 * @ORM\Entity
 */
class PagosImagenes
{
    /**
     * @var int
     *
     * @ORM\Column(name="imagen_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $imagenId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="imagen_nombre", type="string", length=255, nullable=true)
     */
    private $imagenNombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="imagen_selected", type="blob", length=16777215, nullable=true)
     */
    private $imagenSelected;

    /**
     * @var string|null
     *
     * @ORM\Column(name="imagen_over", type="blob", length=16777215, nullable=true)
     */
    private $imagenOver;

    /**
     * @var string|null
     *
     * @ORM\Column(name="imagen_noselected", type="blob", length=16777215, nullable=true)
     */
    private $imagenNoselected;

    /**
     * @var string|null
     *
     * @ORM\Column(name="imagen_file_selected", type="string", length=255, nullable=true)
     */
    private $imagenFileSelected;

    /**
     * @var string|null
     *
     * @ORM\Column(name="imagen_file_over", type="string", length=255, nullable=true)
     */
    private $imagenFileOver;

    /**
     * @var string|null
     *
     * @ORM\Column(name="imagen_file_noselected", type="string", length=255, nullable=true)
     */
    private $imagenFileNoselected;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="imagen_activo", type="boolean", nullable=true, options={"default"="b'1'"})
     */
    private $imagenActivo = 'b\'1\'';

    /**
     * @var int|null
     *
     * @ORM\Column(name="imagen_orden", type="integer", nullable=true)
     */
    private $imagenOrden = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="imagen_metodo", type="integer", nullable=true)
     */
    private $imagenMetodo;

    public function getImagenId()
    {
        return $this->imagenId;
    }

    public function getImagenNombre()
    {
        return $this->imagenNombre;
    }

    public function setImagenNombre(string $imagenNombre)
    {
        $this->imagenNombre = $imagenNombre;

        return $this;
    }

    public function getImagenSelected()
    {
        return $this->imagenSelected;
    }

    public function setImagenSelected($imagenSelected)
    {
        $this->imagenSelected = $imagenSelected;

        return $this;
    }

    public function getImagenOver()
    {
        return $this->imagenOver;
    }

    public function setImagenOver($imagenOver)
    {
        $this->imagenOver = $imagenOver;

        return $this;
    }

    public function getImagenNoselected()
    {
        return $this->imagenNoselected;
    }

    public function setImagenNoselected($imagenNoselected)
    {
        $this->imagenNoselected = $imagenNoselected;

        return $this;
    }

    public function getImagenFileSelected()
    {
        return $this->imagenFileSelected;
    }

    public function setImagenFileSelected(string $imagenFileSelected)
    {
        $this->imagenFileSelected = $imagenFileSelected;

        return $this;
    }

    public function getImagenFileOver()
    {
        return $this->imagenFileOver;
    }

    public function setImagenFileOver(string $imagenFileOver)
    {
        $this->imagenFileOver = $imagenFileOver;

        return $this;
    }

    public function getImagenFileNoselected()
    {
        return $this->imagenFileNoselected;
    }

    public function setImagenFileNoselected(string $imagenFileNoselected)
    {
        $this->imagenFileNoselected = $imagenFileNoselected;

        return $this;
    }

    public function getImagenActivo()
    {
        return $this->imagenActivo;
    }

    public function setImagenActivo(bool $imagenActivo)
    {
        $this->imagenActivo = $imagenActivo;

        return $this;
    }

    public function getImagenOrden()
    {
        return $this->imagenOrden;
    }

    public function setImagenOrden(int $imagenOrden)
    {
        $this->imagenOrden = $imagenOrden;

        return $this;
    }

    public function getImagenMetodo()
    {
        return $this->imagenMetodo;
    }

    public function setImagenMetodo(int $imagenMetodo)
    {
        $this->imagenMetodo = $imagenMetodo;

        return $this;
    }


}
