<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
*  Summary.
 *
 * Description: Temas Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="temas")
 * @ORM\Entity
 */
class Temas
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="sitio", type="string", length=50, nullable=false)
     */
    private $sitio;

    /**
     * @var string
     *
     * @ORM\Column(name="pagina", type="string", length=50, nullable=false)
     */
    private $pagina;

    /**
     * @var string
     *
     * @ORM\Column(name="fondo", type="string", length=10, nullable=false)
     */
    private $fondo;

    /**
     * @var string
     *
     * @ORM\Column(name="boton", type="string", length=10, nullable=false)
     */
    private $boton;

    /**
     * @var string
     *
     * @ORM\Column(name="botonHover", type="string", length=10, nullable=false)
     */
    private $botonhover;

    /**
     * @var string
     *
     * @ORM\Column(name="botonCerrar", type="string", length=10, nullable=false)
     */
    private $botoncerrar;

    public function getId()
    {
        return $this->id;
    }

    public function getSitio()
    {
        return $this->sitio;
    }

    public function setSitio(string $sitio)
    {
        $this->sitio = $sitio;

        return $this;
    }

    public function getPagina()
    {
        return $this->pagina;
    }

    public function setPagina(string $pagina)
    {
        $this->pagina = $pagina;

        return $this;
    }

    public function getFondo()
    {
        return $this->fondo;
    }

    public function setFondo(string $fondo)
    {
        $this->fondo = $fondo;

        return $this;
    }

    public function getBoton()
    {
        return $this->boton;
    }

    public function setBoton(string $boton)
    {
        $this->boton = $boton;

        return $this;
    }

    public function getBotonhover()
    {
        return $this->botonhover;
    }

    public function setBotonhover(string $botonhover)
    {
        $this->botonhover = $botonhover;

        return $this;
    }

    public function getBotoncerrar()
    {
        return $this->botoncerrar;
    }

    public function setBotoncerrar(string $botoncerrar)
    {
        $this->botoncerrar = $botoncerrar;

        return $this;
    }


}
