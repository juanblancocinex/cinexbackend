<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: Precios Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="precios")
 * @ORM\Entity
 */
class Precios
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="complejo", type="string", length=3, nullable=false)
     */
    private $complejo;

    /**
     * @var string
     *
     * @ORM\Column(name="categoria", type="string", length=15, nullable=false)
     */
    private $categoria;

    /**
     * @var string
     *
     * @ORM\Column(name="boleto", type="string", length=45, nullable=false)
     */
    private $boleto;

    /**
     * @var string
     *
     * @ORM\Column(name="electronico_POS", type="decimal", precision=18, scale=2, nullable=false)
     */
    private $electronicoPos;

    /**
     * @var string
     *
     * @ORM\Column(name="electronico_Web", type="decimal", precision=18, scale=2, nullable=false)
     */
    private $electronicoWeb;

    /**
     * @var string
     *
     * @ORM\Column(name="efectivo_POS", type="decimal", precision=18, scale=2, nullable=false)
     */
    private $efectivoPos;

    /**
     * @var string
     *
     * @ORM\Column(name="kiosco", type="decimal", precision=18, scale=2, nullable=false)
     */
    private $kiosco;

    /**
     * @var string
     *
     * @ORM\Column(name="comision_web", type="decimal", precision=18, scale=2, nullable=false)
     */
    private $comisionWeb;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_aplica", type="datetime", nullable=false)
     */
    private $fechaAplica;

    /**
     * @var int
     *
     * @ORM\Column(name="orden", type="integer", nullable=false)
     */
    private $orden;

    public function getId()
    {
        return $this->id;
    }

    public function getComplejo()
    {
        return $this->complejo;
    }

    public function setComplejo(string $complejo)
    {
        $this->complejo = $complejo;

        return $this;
    }

    public function getCategoria()
    {
        return $this->categoria;
    }

    public function setCategoria(string $categoria)
    {
        $this->categoria = $categoria;

        return $this;
    }

    public function getBoleto()
    {
        return $this->boleto;
    }

    public function setBoleto(string $boleto)
    {
        $this->boleto = $boleto;

        return $this;
    }

    public function getElectronicoPos()
    {
        return $this->electronicoPos;
    }

    public function setElectronicoPos($electronicoPos)
    {
        $this->electronicoPos = $electronicoPos;

        return $this;
    }

    public function getElectronicoWeb()
    {
        return $this->electronicoWeb;
    }

    public function setElectronicoWeb($electronicoWeb)
    {
        $this->electronicoWeb = $electronicoWeb;

        return $this;
    }

    public function getEfectivoPos()
    {
        return $this->efectivoPos;
    }

    public function setEfectivoPos($efectivoPos)
    {
        $this->efectivoPos = $efectivoPos;

        return $this;
    }

    public function getKiosco()
    {
        return $this->kiosco;
    }

    public function setKiosco($kiosco)
    {
        $this->kiosco = $kiosco;

        return $this;
    }

    public function getComisionWeb()
    {
        return $this->comisionWeb;
    }

    public function setComisionWeb($comisionWeb)
    {
        $this->comisionWeb = $comisionWeb;

        return $this;
    }

    public function getFechaAplica()
    {
        return $this->fechaAplica;
    }

    public function setFechaAplica(\DateTimeInterface $fechaAplica)
    {
        $this->fechaAplica = $fechaAplica;

        return $this;
    }

    public function getOrden()
    {
        return $this->orden;
    }

    public function setOrden(int $orden)
    {
        $this->orden = $orden;

        return $this;
    }


}
