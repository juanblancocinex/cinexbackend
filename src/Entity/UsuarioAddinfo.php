<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Symfony\Component\Security\Core\User\UserInterface;
use JMS\Serializer\Annotation as Serializer;

/**
 *  Summary.
 *
 * Description: UsuarioAddinfo Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="usuario_addinfo")
 * @ORM\Entity(repositoryClass="App\Repository\UsuarioAddinfoRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class UsuarioAddinfo
{
    /**
     * @var int
     *
     * @ORM\Column(name="usuarioID", type="integer", nullable=false)
     * @ORM\Id
     */
    private $usuarioid;


    /**
     * @var int|null
     *
     * @ORM\Column(name="liste_id", type="integer", nullable=true)
     */
    private $listeId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="listc_id", type="integer", nullable=true)
     */
    private $listcId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="listprr_id", type="integer", nullable=true)
     */
    private $listprrId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="listm_id", type="integer", nullable=true)
     */
    private $listmId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="facebook", type="string", length=255, nullable=true)
     */
    private $facebook;

    /**
     * @var string|null
     *
     * @ORM\Column(name="twitter", type="string", length=255, nullable=true)
     */
    private $twitter;

    /**
     * @var string|null
     *
     * @ORM\Column(name="instagram", type="string", length=255, nullable=true)
     */
    private $instagram;

    /**
     * @var string|null
     *
     * @ORM\Column(name="linkedin", type="string", length=255, nullable=true)
     */
    private $linkedin;

    /**
     * @var string|null
     *
     * @ORM\Column(name="emailadd", type="string", length=255, nullable=true)
     */
    private $emailadd;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="aniversario", type="datetime", nullable=true)
     */
    private $aniversario;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha_registro", type="datetime", nullable=true)
     */

    private $fechaRegistro;

    /**
     * @var string|null
     *
     * @ORM\Column(name="memberCardNumber", type="string", length=20, nullable=true)
     */
    private $membercardnumber;

    /**
     * @var string|null
     *
     * @ORM\Column(name="civil", type="string", length=45, nullable=true, options={"comment"="estado civil"})
     */
    private $civil;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ingreso", type="string", length=45, nullable=true, options={"comment"="nivel de ingreso"})
     */
    private $ingreso;

    /**
     * @var int|null
     *
     * @ORM\Column(name="familiar", type="integer", nullable=true, options={"comment"="grupo familiar"})
     */
    private $familiar;

    /**
     * @var string|null
     *
     * @ORM\Column(name="educacion", type="string", length=45, nullable=true, options={"comment"="nivel educacion"})
     */
    private $educacion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ocupacion", type="string", length=45, nullable=true, options={"comment"="ocupacion"})
     */
    private $ocupacion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="favorita", type="string", length=45, nullable=true)
     */
    private $favorita;

    /**
     * @var string|null
     *
     * @ORM\Column(name="asiento", type="string", length=45, nullable=true, options={"comment"="asiento"})
     */
    private $asiento;

    /**
     * @var string|null
     *
     * @ORM\Column(name="bebida", type="string", length=45, nullable=true, options={"comment"="bebida"})
     */
    private $bebida;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comida", type="string", length=45, nullable=true, options={"comment"="comida"})
     */
    private $comida;

    /**
     * @var string|null
     *
     * @ORM\Column(name="formato", type="string", length=45, nullable=true, options={"comment"="formato"})
     */
    private $formato;

    /**
     * @var int|null
     *
     * @ORM\Column(name="concresa", type="integer", nullable=true)
     */
    private $concresa;

    /**
     * @var int|null
     *
     * @ORM\Column(name="recreo", type="integer", nullable=true)
     */
    private $recreo;

    /**
     * @var int|null
     *
     * @ORM\Column(name="lago", type="integer", nullable=true)
     */
    private $lago;

    /**
     * @var int|null
     *
     * @ORM\Column(name="lido", type="integer", nullable=true)
     */
    private $lido;

    /**
     * @var int|null
     *
     * @ORM\Column(name="proceres", type="integer", nullable=true)
     */
    private $proceres;

    /**
     * @var int|null
     *
     * @ORM\Column(name="manzana", type="integer", nullable=true)
     */
    private $manzana;

    /**
     * @var int|null
     *
     * @ORM\Column(name="paraiso", type="integer", nullable=true)
     */
    private $paraiso;

    /**
     * @var int|null
     *
     * @ORM\Column(name="hatillo", type="integer", nullable=true)
     */
    private $hatillo;

    /**
     * @var int|null
     *
     * @ORM\Column(name="sambil", type="integer", nullable=true)
     */
    private $sambil;

    /**
     * @var int|null
     *
     * @ORM\Column(name="sanignacio", type="integer", nullable=true)
     */
    private $sanignacio;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tolon", type="integer", nullable=true)
     */
    private $tolon;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ventura", type="integer", nullable=true)
     */
    private $ventura;

    /**
     * @var int|null
     *
     * @ORM\Column(name="suspenso", type="integer", nullable=true)
     */
    private $suspenso;

    /**
     * @var int|null
     *
     * @ORM\Column(name="infantil", type="integer", nullable=true)
     */
    private $infantil;

    /**
     * @var int|null
     *
     * @ORM\Column(name="drama", type="integer", nullable=true)
     */
    private $drama;

    /**
     * @var int|null
     *
     * @ORM\Column(name="deporte", type="integer", nullable=true)
     */
    private $deporte;

    /**
     * @var int|null
     *
     * @ORM\Column(name="musical", type="integer", nullable=true)
     */
    private $musical;

    /**
     * @var int|null
     *
     * @ORM\Column(name="epica", type="integer", nullable=true)
     */
    private $epica;

    /**
     * @var int|null
     *
     * @ORM\Column(name="animacion", type="integer", nullable=true)
     */
    private $animacion;

    /**
     * @var int|null
     *
     * @ORM\Column(name="documental", type="integer", nullable=true)
     */
    private $documental;

    /**
     * @var int|null
     *
     * @ORM\Column(name="comedia", type="integer", nullable=true)
     */
    private $comedia;

    /**
     * @var int|null
     *
     * @ORM\Column(name="fantasia", type="integer", nullable=true)
     */
    private $fantasia;

    /**
     * @var int|null
     *
     * @ORM\Column(name="venezolana", type="integer", nullable=true)
     */
    private $venezolana;

    /**
     * @var int|null
     *
     * @ORM\Column(name="alternativo", type="integer", nullable=true)
     */
    private $alternativo;

    /**
     * @var int|null
     *
     * @ORM\Column(name="estatus", type="integer", nullable=true, options={"comment"="0=neutro  1=insert  2=update"})
     */
    private $estatus = '0';

    public function getId()
    {
        return $this->id;
    }

    public function getUsuarioid()
    {
        return $this->usuarioid;
    }

    public function setUsuarioid($usuarioid)
    {
        $this->usuarioid = $usuarioid;

        return $this;
    }

    public function getListeId()
    {
        return $this->listeId;
    }

    public function setListeId(int $listeId)
    {
        $this->listeId = $listeId;

        return $this;
    }

    public function getListcId()
    {
        return $this->listcId;
    }

    public function setListcId(int $listcId)
    {
        $this->listcId = $listcId;

        return $this;
    }

    public function getListprrId()
    {
        return $this->listprrId;
    }

    public function setListprrId(int $listprrId)
    {
        $this->listprrId = $listprrId;

        return $this;
    }

    public function getListmId()
    {
        return $this->listmId;
    }

    public function setListmId(int $listmId)
    {
        $this->listmId = $listmId;

        return $this;
    }

    public function getFacebook()
    {
        return $this->facebook;
    }

    public function setFacebook($facebook)
    {
        $this->facebook = $facebook;

        return $this;
    }

    public function getTwitter()
    {
        return $this->twitter;
    }

    public function setTwitter($twitter)
    {
        $this->twitter = $twitter;

        return $this;
    }

    public function getInstagram()
    {
        return $this->instagram;
    }

    public function setInstagram($instagram)
    {
        $this->instagram = $instagram;

        return $this;
    }

    public function getLinkedin()
    {
        return $this->linkedin;
    }

    public function setLinkedin($linkedin)
    {
        $this->linkedin = $linkedin;

        return $this;
    }

    public function getEmailadd()
    {
        return $this->emailadd;
    }

    public function setEmailadd($emailadd)
    {
        $this->emailadd = $emailadd;

        return $this;
    }

    public function getAniversario()
    {
        return $this->aniversario;
    }

    public function setAniversario($aniversario = null)
    {
        $this->aniversario = $aniversario;

        return $this;
    }

    public function getFechaRegistro()
    {
        return $this->fechaRegistro;
    }

    /**
     * @ORM\PrePersist
     */
    public function setFechaRegistro()
    {
        $this->fechaRegistro = new \DateTime();

        return $this;
    }

    public function getMembercardnumber()
    {
        return $this->membercardnumber;
    }

    public function setMembercardnumber($membercardnumber)
    {
        $this->membercardnumber = $membercardnumber;

        return $this;
    }

    public function getCivil()
    {
        return $this->civil;
    }

    public function setCivil($civil)
    {
        $this->civil = $civil;

        return $this;
    }

    public function getIngreso()
    {
        return $this->ingreso;
    }

    public function setIngreso($ingreso)
    {
        $this->ingreso = $ingreso;

        return $this;
    }

    public function getFamiliar()
    {
        return $this->familiar;
    }

    public function setFamiliar($familiar)
    {
        $this->familiar = $familiar;

        return $this;
    }

    public function getEducacion()
    {
        return $this->educacion;
    }

    public function setEducacion($educacion)
    {
        $this->educacion = $educacion;

        return $this;
    }

    public function getOcupacion()
    {
        return $this->ocupacion;
    }

    public function setOcupacion($ocupacion)
    {
        $this->ocupacion = $ocupacion;

        return $this;
    }

    public function getFavorita()
    {
        return $this->favorita;
    }

    public function setFavorita($favorita)
    {
        $this->favorita = $favorita;

        return $this;
    }

    public function getAsiento()
    {
        return $this->asiento;
    }

    public function setAsiento($asiento)
    {
        $this->asiento = $asiento;

        return $this;
    }

    public function getBebida()
    {
        return $this->bebida;
    }

    public function setBebida($bebida)
    {
        $this->bebida = $bebida;

        return $this;
    }

    public function getComida()
    {
        return $this->comida;
    }

    public function setComida($comida)
    {
        $this->comida = $comida;

        return $this;
    }

    public function getFormato()
    {
        return $this->formato;
    }

    public function setFormato($formato)
    {
        $this->formato = $formato;

        return $this;
    }

    public function getConcresa()
    {
        return $this->concresa;
    }

    public function setConcresa(int $concresa)
    {
        $this->concresa = $concresa;

        return $this;
    }

    public function getRecreo()
    {
        return $this->recreo;
    }

    public function setRecreo(int $recreo)
    {
        $this->recreo = $recreo;

        return $this;
    }

    public function getLago()
    {
        return $this->lago;
    }

    public function setLago(int $lago)
    {
        $this->lago = $lago;

        return $this;
    }

    public function getLido()
    {
        return $this->lido;
    }

    public function setLido(int $lido)
    {
        $this->lido = $lido;

        return $this;
    }

    public function getProceres()
    {
        return $this->proceres;
    }

    public function setProceres(int $proceres)
    {
        $this->proceres = $proceres;

        return $this;
    }

    public function getManzana()
    {
        return $this->manzana;
    }

    public function setManzana(int $manzana)
    {
        $this->manzana = $manzana;

        return $this;
    }

    public function getParaiso()
    {
        return $this->paraiso;
    }

    public function setParaiso(int $paraiso)
    {
        $this->paraiso = $paraiso;

        return $this;
    }

    public function getHatillo()
    {
        return $this->hatillo;
    }

    public function setHatillo(int $hatillo)
    {
        $this->hatillo = $hatillo;

        return $this;
    }

    public function getSambil()
    {
        return $this->sambil;
    }

    public function setSambil(int $sambil)
    {
        $this->sambil = $sambil;

        return $this;
    }

    public function getSanignacio()
    {
        return $this->sanignacio;
    }

    public function setSanignacio(int $sanignacio)
    {
        $this->sanignacio = $sanignacio;

        return $this;
    }

    public function getTolon()
    {
        return $this->tolon;
    }

    public function setTolon(int $tolon)
    {
        $this->tolon = $tolon;

        return $this;
    }

    public function getVentura()
    {
        return $this->ventura;
    }

    public function setVentura(int $ventura)
    {
        $this->ventura = $ventura;

        return $this;
    }

    public function getSuspenso()
    {
        return $this->suspenso;
    }

    public function setSuspenso(int $suspenso)
    {
        $this->suspenso = $suspenso;

        return $this;
    }

    public function getInfantil()
    {
        return $this->infantil;
    }

    public function setInfantil(int $infantil)
    {
        $this->infantil = $infantil;

        return $this;
    }

    public function getDrama()
    {
        return $this->drama;
    }

    public function setDrama(int $drama)
    {
        $this->drama = $drama;

        return $this;
    }

    public function getDeporte()
    {
        return $this->deporte;
    }

    public function setDeporte(int $deporte)
    {
        $this->deporte = $deporte;

        return $this;
    }

    public function getMusical()
    {
        return $this->musical;
    }

    public function setMusical(int $musical)
    {
        $this->musical = $musical;

        return $this;
    }

    public function getEpica()
    {
        return $this->epica;
    }

    public function setEpica(int $epica)
    {
        $this->epica = $epica;

        return $this;
    }

    public function getAnimacion()
    {
        return $this->animacion;
    }

    public function setAnimacion(int $animacion)
    {
        $this->animacion = $animacion;

        return $this;
    }

    public function getDocumental()
    {
        return $this->documental;
    }

    public function setDocumental(int $documental)
    {
        $this->documental = $documental;

        return $this;
    }

    public function getComedia()
    {
        return $this->comedia;
    }

    public function setComedia(int $comedia)
    {
        $this->comedia = $comedia;

        return $this;
    }

    public function getFantasia()
    {
        return $this->fantasia;
    }

    public function setFantasia(int $fantasia)
    {
        $this->fantasia = $fantasia;

        return $this;
    }

    public function getVenezolana()
    {
        return $this->venezolana;
    }

    public function setVenezolana(int $venezolana)
    {
        $this->venezolana = $venezolana;

        return $this;
    }

    public function getAlternativo()
    {
        return $this->alternativo;
    }

    public function setAlternativo(int $alternativo)
    {
        $this->alternativo = $alternativo;

        return $this;
    }

    public function getEstatus()
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus)
    {
        $this->estatus = $estatus;

        return $this;
    }


}