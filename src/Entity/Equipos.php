<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description:  Equipos Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="equipos")
 * @ORM\Entity
 */
class Equipos
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=250, nullable=false)
     */
    private $nombre;

    /**
     * @var int
     *
     * @ORM\Column(name="grupo", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $grupo = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="posicion", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $posicion = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="imagen", type="string", length=250, nullable=false)
     */
    private $imagen;

    /**
     * @var int
     *
     * @ORM\Column(name="estado", type="integer", nullable=false, options={"default"="1","unsigned"=true})
     */
    private $estado = '1';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_registro", type="date", nullable=false)
     */
    private $fechaRegistro;

    /**
     * @var int
     *
     * @ORM\Column(name="usuario_registro", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $usuarioRegistro = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_actualizacion", type="date", nullable=false)
     */
    private $fechaActualizacion;

    /**
     * @var int
     *
     * @ORM\Column(name="usuario_actualizacion", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $usuarioActualizacion = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombreCorto", type="string", length=10, nullable=true)
     */
    private $nombrecorto;

    /**
     * @var string|null
     *
     * @ORM\Column(name="visible", type="string", length=1, nullable=true)
     */
    private $visible;

    public function getId()
    {
        return $this->id;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getGrupo()
    {
        return $this->grupo;
    }

    public function setGrupo(int $grupo)
    {
        $this->grupo = $grupo;

        return $this;
    }

    public function getPosicion()
    {
        return $this->posicion;
    }

    public function setPosicion(int $posicion)
    {
        $this->posicion = $posicion;

        return $this;
    }

    public function getImagen()
    {
        return $this->imagen;
    }

    public function setImagen(string $imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    public function getEstado()
    {
        return $this->estado;
    }

    public function setEstado(int $estado)
    {
        $this->estado = $estado;

        return $this;
    }

    public function getFechaRegistro()
    {
        return $this->fechaRegistro;
    }

    public function setFechaRegistro(\DateTimeInterface $fechaRegistro)
    {
        $this->fechaRegistro = $fechaRegistro;

        return $this;
    }

    public function getUsuarioRegistro()
    {
        return $this->usuarioRegistro;
    }

    public function setUsuarioRegistro(int $usuarioRegistro)
    {
        $this->usuarioRegistro = $usuarioRegistro;

        return $this;
    }

    public function getFechaActualizacion()
    {
        return $this->fechaActualizacion;
    }

    public function setFechaActualizacion(\DateTimeInterface $fechaActualizacion)
    {
        $this->fechaActualizacion = $fechaActualizacion;

        return $this;
    }

    public function getUsuarioActualizacion()
    {
        return $this->usuarioActualizacion;
    }

    public function setUsuarioActualizacion(int $usuarioActualizacion)
    {
        $this->usuarioActualizacion = $usuarioActualizacion;

        return $this;
    }

    public function getNombrecorto()
    {
        return $this->nombrecorto;
    }

    public function setNombrecorto(string $nombrecorto)
    {
        $this->nombrecorto = $nombrecorto;

        return $this;
    }

    public function getVisible()
    {
        return $this->visible;
    }

    public function setVisible(string $visible)
    {
        $this->visible = $visible;

        return $this;
    }


}
