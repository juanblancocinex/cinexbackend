<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
  *  Summary.
 *
 * Description: UsuariosCsi4dx Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="usuarios_csi_4dx", uniqueConstraints={@ORM\UniqueConstraint(name="correo_UNIQUE", columns={"correo"})})
 * @ORM\Entity
 */
class UsuariosCsi4dx
{
    /**
     * @var int
     *
     * @ORM\Column(name="idusuarios", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idusuarios;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="text", length=65535, nullable=false)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="apellido", type="text", length=65535, nullable=false)
     */
    private $apellido;

    /**
     * @var string|null
     *
     * @ORM\Column(name="telefono", type="text", length=65535, nullable=true)
     */
    private $telefono;

    /**
     * @var string
     *
     * @ORM\Column(name="correo", type="string", length=500, nullable=false)
     */
    private $correo;

    /**
     * @var string
     *
     * @ORM\Column(name="cedula", type="string", length=45, nullable=false)
     */
    private $cedula;

    /**
     * @var int|null
     *
     * @ORM\Column(name="email", type="integer", nullable=true)
     */
    private $email = '0';

    public function getIdusuarios()
    {
        return $this->idusuarios;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getApellido()
    {
        return $this->apellido;
    }

    public function setApellido(string $apellido)
    {
        $this->apellido = $apellido;

        return $this;
    }

    public function getTelefono()
    {
        return $this->telefono;
    }

    public function setTelefono(string $telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    public function getCorreo()
    {
        return $this->correo;
    }

    public function setCorreo(string $correo)
    {
        $this->correo = $correo;

        return $this;
    }

    public function getCedula()
    {
        return $this->cedula;
    }

    public function setCedula(string $cedula)
    {
        $this->cedula = $cedula;

        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail(int $email)
    {
        $this->email = $email;

        return $this;
    }


}
