<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
  * Summary.
 *
 * Description:  Ciudades Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 *
 * @ORM\Table(name="ciudades", indexes={@ORM\Index(name="id_estado", columns={"id_estado"})})
 * @ORM\Entity(repositoryClass="App\Repository\CiudadesRepository")
 */
class Ciudades
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_ciudad", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idCiudad;


    /**
     * @var string
     *
     * @ORM\Column(name="ciudad", type="string", length=200, nullable=false)
     */
    private $ciudad;

    /**
     * @var bool
     *
     * @ORM\Column(name="capital", type="boolean", nullable=false)
     */
    private $capital = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="concomplejo", type="string", length=1, nullable=true, options={"default"="N","fixed"=true})
     */
    private $concomplejo = 'N';

    /**
     * Many Ciudades have One Estado.
     * @ORM\ManyToOne(targetEntity="Estados", inversedBy="ciudades")
     * @ORM\JoinColumn(name="id_estado", referencedColumnName="id_estado", nullable=true)
     */
    private $estado;

    public function getIdCiudad(): ?int
    {
        return $this->idCiudad;
    }

  
    public function getCiudad(): ?string
    {
        return $this->ciudad;
    }

    public function setCiudad(string $ciudad): self
    {
        $this->ciudad = $ciudad;

        return $this;
    }

    public function getCapital(): ?bool
    {
        return $this->capital;
    }

    public function setCapital(bool $capital): self
    {
        $this->capital = $capital;

        return $this;
    }

    public function getConcomplejo(): ?string
    {
        return $this->concomplejo;
    }

    public function setConcomplejo(?string $concomplejo): self
    {
        $this->concomplejo = $concomplejo;

        return $this;
    }

    public function getEstado(): ?Estados
    {
        return $this->estado;
    }

    public function setEstado(?Estados $estado): self
    {
        $this->estado = $estado;

        return $this;
    }





    
}
