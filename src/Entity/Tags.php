<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: Tags Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="tags")
 * @ORM\Entity
 */
class Tags
{
    /**
     * @var int
     *
     * @ORM\Column(name="tagsID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $tagsid;

    /**
     * @var int
     *
     * @ORM\Column(name="noticiaID", type="integer", nullable=false)
     */
    private $noticiaid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=true)
     */
    private $name;

    public function getTagsid()
    {
        return $this->tagsid;
    }

    public function getNoticiaid()
    {
        return $this->noticiaid;
    }

    public function setNoticiaid(int $noticiaid)
    {
        $this->noticiaid = $noticiaid;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }


}
