<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: PeliculaVersiones Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 *
 * @ORM\Table(name="pelicula_versiones")
 * @ORM\Entity
 */
class PeliculaVersiones
{
    /**
     * @var string
     *
     * @ORM\Column(name="nombreSinFormato", type="string", length=255, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $nombresinformato;

    /**
     * @var string|null
     *
     * @ORM\Column(name="versiones", type="string", length=255, nullable=true)
     */
    private $versiones;

    /**
     * @var string|null
     *
     * @ORM\Column(name="trailer", type="string", length=255, nullable=true)
     */
    private $trailer;

    public function getNombresinformato()
    {
        return $this->nombresinformato;
    }

    public function getVersiones()
    {
        return $this->versiones;
    }

    public function setVersiones(string $versiones)
    {
        $this->versiones = $versiones;

        return $this;
    }

    public function getTrailer()
    {
        return $this->trailer;
    }

    public function setTrailer(string $trailer)
    {
        $this->trailer = $trailer;

        return $this;
    }


}
