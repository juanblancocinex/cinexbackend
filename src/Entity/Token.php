<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: Token Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="token", indexes={@ORM\Index(name="CEL_TOKEN_FECHA", columns={"celular", "token", "fechaEnvio"})})
 * @ORM\Entity
 */
class Token
{
    /**
     * @var int
     *
     * @ORM\Column(name="tokenID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $tokenid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="celular", type="string", length=20, nullable=true)
     */
    private $celular;

    /**
     * @var string|null
     *
     * @ORM\Column(name="token", type="string", length=8, nullable=true)
     */
    private $token;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaEnvio", type="datetime", nullable=true)
     */
    private $fechaenvio;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vigente", type="string", length=1, nullable=true)
     */
    private $vigente;

    /**
     * @var int|null
     *
     * @ORM\Column(name="intento", type="integer", nullable=true)
     */
    private $intento;

    public function getTokenid()
    {
        return $this->tokenid;
    }

    public function getCelular()
    {
        return $this->celular;
    }

    public function setCelular(string $celular)
    {
        $this->celular = $celular;

        return $this;
    }

    public function getToken()
    {
        return $this->token;
    }

    public function setToken(string $token)
    {
        $this->token = $token;

        return $this;
    }

    public function getFechaenvio()
    {
        return $this->fechaenvio;
    }

    public function setFechaenvio(\DateTimeInterface $fechaenvio)
    {
        $this->fechaenvio = $fechaenvio;

        return $this;
    }

    public function getVigente()
    {
        return $this->vigente;
    }

    public function setVigente(string $vigente)
    {
        $this->vigente = $vigente;

        return $this;
    }

    public function getIntento()
    {
        return $this->intento;
    }

    public function setIntento(int $intento)
    {
        $this->intento = $intento;

        return $this;
    }


}
