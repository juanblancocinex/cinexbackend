<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
  *  Summary.
 *
 * Description: VouchersCategoria Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="vouchers_categoria")
 * @ORM\Entity
 */
class VouchersCategoria
{
    /**
     * @var int
     *
     * @ORM\Column(name="categoria_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $categoriaId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="categoria_nombre", type="string", length=255, nullable=true)
     */
    private $categoriaNombre;

    public function getCategoriaId()
    {
        return $this->categoriaId;
    }

    public function getCategoriaNombre()
    {
        return $this->categoriaNombre;
    }

    public function setCategoriaNombre(string $categoriaNombre)
    {
        $this->categoriaNombre = $categoriaNombre;

        return $this;
    }


}
