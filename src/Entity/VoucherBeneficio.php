<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: VoucherBeneficio Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="voucher_beneficio", uniqueConstraints={@ORM\UniqueConstraint(name="fk_beneficio_voucher", columns={"id_beneficio", "id_tipo"}), @ORM\UniqueConstraint(name="fk_voucher_beneficio", columns={"id_tipo", "id_beneficio"})}, indexes={@ORM\Index(name="IDX_6822C5E47CEDE51F", columns={"id_beneficio"}), @ORM\Index(name="IDX_6822C5E4FB0D0145", columns={"id_tipo"})})
 * @ORM\Entity
 */
class VoucherBeneficio
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Beneficio
     *
     * @ORM\ManyToOne(targetEntity="Beneficio")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_beneficio", referencedColumnName="id")
     * })
     */
    private $idBeneficio;

    /**
     * @var \Tipo
     *
     * @ORM\ManyToOne(targetEntity="Tipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tipo", referencedColumnName="id")
     * })
     */
    private $idTipo;

    public function getId()
    {
        return $this->id;
    }

    public function getIdBeneficio()
    {
        return $this->idBeneficio;
    }

    public function setIdBeneficio(Beneficio $idBeneficio)
    {
        $this->idBeneficio = $idBeneficio;

        return $this;
    }

    public function getIdTipo()
    {
        return $this->idTipo;
    }

    public function setIdTipo(Tipo $idTipo)
    {
        $this->idTipo = $idTipo;

        return $this;
    }


}
