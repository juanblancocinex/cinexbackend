<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: PreguntasRespuestasDesafio Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="preguntas_respuestas_desafio")
 * @ORM\Entity
 */
class PreguntasRespuestasDesafio
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="pregunta_id", type="integer", nullable=true)
     */
    private $preguntaId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="respuesta_id", type="string", length=45, nullable=true)
     */
    private $respuestaId;

    public function getId()
    {
        return $this->id;
    }

    public function getPreguntaId()
    {
        return $this->preguntaId;
    }

    public function setPreguntaId(int $preguntaId)
    {
        $this->preguntaId = $preguntaId;

        return $this;
    }

    public function getRespuestaId()
    {
        return $this->respuestaId;
    }

    public function setRespuestaId(string $respuestaId)
    {
        $this->respuestaId = $respuestaId;

        return $this;
    }


}
