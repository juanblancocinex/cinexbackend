<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: VitrinasInternas Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="vitrinas_internas")
 * @ORM\Entity
 */
class VitrinasInternas
{
    /**
     * @var int
     *
     * @ORM\Column(name="vitrina_id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $vitrinaId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vitrina_nombre", type="string", length=255, nullable=true)
     */
    private $vitrinaNombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vitrina_url", type="string", length=255, nullable=true)
     */
    private $vitrinaUrl;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vitrina_link", type="string", length=255, nullable=true)
     */
    private $vitrinaLink;

    /**
     * @var int|null
     *
     * @ORM\Column(name="vitrina_orden", type="integer", nullable=true)
     */
    private $vitrinaOrden;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vitrina_seccion", type="string", length=255, nullable=true)
     */
    private $vitrinaSeccion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vitrina_activo", type="string", length=1, nullable=true, options={"default"="S","fixed"=true})
     */
    private $vitrinaActivo = 'S';

    public function getVitrinaId()
    {
        return $this->vitrinaId;
    }

    public function getVitrinaNombre()
    {
        return $this->vitrinaNombre;
    }

    public function setVitrinaNombre(string $vitrinaNombre)
    {
        $this->vitrinaNombre = $vitrinaNombre;

        return $this;
    }

    public function getVitrinaUrl()
    {
        return $this->vitrinaUrl;
    }

    public function setVitrinaUrl(string $vitrinaUrl)
    {
        $this->vitrinaUrl = $vitrinaUrl;

        return $this;
    }

    public function getVitrinaLink()
    {
        return $this->vitrinaLink;
    }

    public function setVitrinaLink(string $vitrinaLink)
    {
        $this->vitrinaLink = $vitrinaLink;

        return $this;
    }

    public function getVitrinaOrden()
    {
        return $this->vitrinaOrden;
    }

    public function setVitrinaOrden(int $vitrinaOrden)
    {
        $this->vitrinaOrden = $vitrinaOrden;

        return $this;
    }

    public function getVitrinaSeccion()
    {
        return $this->vitrinaSeccion;
    }

    public function setVitrinaSeccion(string $vitrinaSeccion)
    {
        $this->vitrinaSeccion = $vitrinaSeccion;

        return $this;
    }

    public function getVitrinaActivo()
    {
        return $this->vitrinaActivo;
    }

    public function setVitrinaActivo(string $vitrinaActivo)
    {
        $this->vitrinaActivo = $vitrinaActivo;

        return $this;
    }


}
