<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Summary.
 *
 * Description:  ConcesionCompraTmp Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="concesion_compra_tmp")
 * @ORM\Entity
 */
class ConcesionCompraTmp
{
    /**
     * @var int
     *
     * @ORM\Column(name="con_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $conId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="con_userid", type="integer", nullable=true)
     */
    private $conUserid;

    /**
     * @var int|null
     *
     * @ORM\Column(name="con_strid", type="integer", nullable=true)
     */
    private $conStrid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="con_strdesc", type="text", length=65535, nullable=true)
     */
    private $conStrdesc;

    /**
     * @var float|null
     *
     * @ORM\Column(name="con_intprice", type="float", precision=10, scale=0, nullable=true)
     */
    private $conIntprice;

    /**
     * @var int|null
     *
     * @ORM\Column(name="con_intqty", type="integer", nullable=true)
     */
    private $conIntqty;

    /**
     * @var string|null
     *
     * @ORM\Column(name="con_catname", type="text", length=65535, nullable=true)
     */
    private $conCatname;

    public function getConId()
    {
        return $this->conId;
    }

    public function getConUserid()
    {
        return $this->conUserid;
    }

    public function setConUserid(int $conUserid)
    {
        $this->conUserid = $conUserid;

        return $this;
    }

    public function getConStrid()
    {
        return $this->conStrid;
    }

    public function setConStrid(int $conStrid)
    {
        $this->conStrid = $conStrid;

        return $this;
    }

    public function getConStrdesc()
    {
        return $this->conStrdesc;
    }

    public function setConStrdesc(string $conStrdesc)
    {
        $this->conStrdesc = $conStrdesc;

        return $this;
    }

    public function getConIntprice()
    {
        return $this->conIntprice;
    }

    public function setConIntprice(float $conIntprice)
    {
        $this->conIntprice = $conIntprice;

        return $this;
    }

    public function getConIntqty()
    {
        return $this->conIntqty;
    }

    public function setConIntqty(int $conIntqty)
    {
        $this->conIntqty = $conIntqty;

        return $this;
    }

    public function getConCatname()
    {
        return $this->conCatname;
    }

    public function setConCatname(string $conCatname)
    {
        $this->conCatname = $conCatname;

        return $this;
    }


}
