<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Summary.
 *
 * Description: Top5 Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 * @ORM\Table(name="top5")
 * @ORM\Entity(repositoryClass="App\Repository\Top5Repository")
 **/
class Top5
{
    /**
     * @var int
     *
     * @ORM\Column(name="topID", type="integer", nullable=false, options={"unsigned"=true,"comment"="clave artificial del top"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $topid;

    /**
     * @var string
     *
     * @ORM\Column(name="nombrePelicula", type="string", length=50, nullable=false, options={"comment"="nombre de la pelicula"})
     */
    private $nombrepelicula;

    /**
     * @var int
     *
     * @ORM\Column(name="posicion", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $posicion;

    /**
     * @var string
     *
     * @ORM\Column(name="vigente", type="string", length=1, nullable=false, options={"comment"="vigente Y no vigente N"})
     */
    private $vigente;

    public function getTopid()
    {
        return $this->topid;
    }

    public function getNombrepelicula()
    {
        return $this->nombrepelicula;
    }

    public function setNombrepelicula(string $nombrepelicula)
    {
        $this->nombrepelicula = $nombrepelicula;

        return $this;
    }

    public function getPosicion()
    {
        return $this->posicion;
    }

    public function setPosicion(int $posicion)
    {
        $this->posicion = $posicion;

        return $this;
    }

    public function getVigente()
    {
        return $this->vigente;
    }

    public function setVigente(string $vigente)
    {
        $this->vigente = $vigente;

        return $this;
    }


}
