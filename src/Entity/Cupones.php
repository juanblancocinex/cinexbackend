<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
*  Summary.
 *
 * Description:  Cupones Entity
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 *
 *
 * @ORM\Table(name="cupones", indexes={@ORM\Index(name="fk_cupones_1_idx", columns={"id_promocion"})})
 * @ORM\Entity
 */
class Cupones
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="code", type="string", length=100, nullable=true)
     */
    private $code;

    /**
     * @var int|null
     *
     * @ORM\Column(name="delivered", type="integer", nullable=true)
     */
    private $delivered;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id_user", type="integer", nullable=true)
     */
    private $idUser;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="valid_from", type="datetime", nullable=true)
     */
    private $validFrom;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="valid_to", type="datetime", nullable=true)
     */
    private $validTo;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="delivered_date", type="datetime", nullable=true)
     */
    private $deliveredDate;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id_promocion", type="integer", nullable=true)
     */
    private $idPromocion;

    public function getId()
    {
        return $this->id;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setCode(string $code)
    {
        $this->code = $code;

        return $this;
    }

    public function getDelivered()
    {
        return $this->delivered;
    }

    public function setDelivered(int $delivered)
    {
        $this->delivered = $delivered;

        return $this;
    }

    public function getIdUser()
    {
        return $this->idUser;
    }

    public function setIdUser(int $idUser)
    {
        $this->idUser = $idUser;

        return $this;
    }

    public function getValidFrom()
    {
        return $this->validFrom;
    }

    public function setValidFrom(\DateTimeInterface $validFrom)
    {
        $this->validFrom = $validFrom;

        return $this;
    }

    public function getValidTo()
    {
        return $this->validTo;
    }

    public function setValidTo(\DateTimeInterface $validTo)
    {
        $this->validTo = $validTo;

        return $this;
    }

    public function getDeliveredDate()
    {
        return $this->deliveredDate;
    }

    public function setDeliveredDate(\DateTimeInterface $deliveredDate)
    {
        $this->deliveredDate = $deliveredDate;

        return $this;
    }

    public function getIdPromocion()
    {
        return $this->idPromocion;
    }

    public function setIdPromocion(int $idPromocion)
    {
        $this->idPromocion = $idPromocion;

        return $this;
    }


}
