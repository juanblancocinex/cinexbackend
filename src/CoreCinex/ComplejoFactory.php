<?php

namespace App\CoreCinex;

use Doctrine\ORM\Mapping as ORM;

/**
 * Summary.
 *
 * Description: Logica de Negocio del Modulo de Complejo
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 */

 /**
  * @method array getServicios(string data)
  */ 
class ComplejoFactory
{
    /**
     * Crea un array con los servicios de cada complejos .
     * @param string $data string de servicios de complejos.
    **/
    function getServicios ($data){
        $arrayServicios=array();
        $listaServicios=explode(";",$data);
        foreach($listaServicios as $row){
            if(strlen($row)>1)
                $arrayServicios[]=array("servicios"=>$row);
        }
        return $arrayServicios;
    }
}
