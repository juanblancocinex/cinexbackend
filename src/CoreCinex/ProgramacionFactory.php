<?php

namespace App\CoreCinex;

use Doctrine\ORM\Mapping as ORM;
use App\Lib\ApiUtilFunctions;
use App\Lib\Cors;
use App\Lib\Constantes;
/**
 * Summary.
 *
 * Description: Logica de Negocio del Modulo de Programación
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 */
/**
  * @method array getServicios(string data) Crea un array con los servicios de cada complejos
  * @method array getProgramacion(string data) Informacion de programacion de cada complejo por pelicula
  * @method array getProgramacionCiudades(string data) Crea un array Agrupado por Ciudades
  * @method array mapComplejos($ciudad,$data) Parsea Complejos por Ciudades
  * @method array mapPeliculas($complejo,$data) Parsea Pelicupas Por Complejos
  * @method array getArrayPeliculas($lenguajeVersionAndIcono,$valor,$data) Crea atributos de cada pelicula
  */ 
class ProgramacionFactory
{
    

    private $dao;

    public function __construct($dao)
    {
       $this->dao = $dao;
    }

    



     /**
     * Crea un array con los servicios de cada complejos.
     * @param string $data string de servicios de complejos.
     * @return string json programacion.
    **/
    function getServicios ($data){
        $arrayServicios=array();
        $listaServicios=explode(";",$data);
        foreach($listaServicios as $row){
            if(strlen($row)>1)
                $arrayServicios[]=array("servicios"=>$row);
        }
        return $arrayServicios;
    }

    /**  .
     * @param array $data Array de Programacion.
     * @return string json programacion.
    **/
    function getProgramacion($data){
        return $this->getProgramacionCiudades($data);
    }

    /**  .
     * @param array $data Array de Programacion.
     * @return string json programacion.
    **/
    function getProgramacionCiudades($data){
        $ciudad=null;
        $dataJson=[];
        foreach($data as $valor){
            if ($ciudad!=$valor["nombreCiudad"]){
                $ciudad=$valor["nombreCiudad"];
                $dataJson["ciudades"][]=array("ciudad"=>$valor["nombreCiudad"],"complejos"=>$this->mapComplejos($valor["nombreCiudad"],$data));
            }
        }
        return $dataJson;
    }
    
    /**  .
     * @param string $ciudad Ciudad que agrupa los complejos.
     * @param array $data Array de Programacion.
     * @return string json complejos por ciudades.
    **/
    function mapComplejos($ciudad,$data){
        $complejo = "";
        foreach($data as $valor){
            if($valor["nombreCiudad"]==$ciudad){
                if($complejo!=$valor["nombreComplejo"]){
                    $complejo=$valor["nombreComplejo"];
                    $complejos[] = array("complejoNombre"=>$valor["nombreComplejo"],"complejoCodigo"=>$valor["codigoComplejo"],"complejoDireccion"=>$valor["direccion"],
                        "complejoTelefono"=>$valor["telefonoAtencion"],"peliculas"=>$this->mapPeliculas($valor["nombreComplejo"],$data));
                }
            }
        }
        return $complejos;
    }

    /**  .
     * @param string $complejo Complejo.
     * @param array $data arreglo original de programación.
     * @return string json peliculas por complejo.
    **/
    function mapPeliculas($complejo,$data){
        $apiUtilFunctions = new ApiUtilFunctions();
        $versiones =  "";
        $pelicula="";
        foreach($data as $valor){
            if($valor["nombreComplejo"]==$complejo  && $pelicula!=$valor["codVista"]){
                $pelicula=$valor["codVista"];
                $sala = $valor["sala"];
                $versiones = explode(" ",$valor["versiones"]);
               
                $lenguajeVersionAndIcono=$apiUtilFunctions::getLenguajeVersionAndIcono($versiones);
                
                $peliculas[] =$this->getArrayPeliculas($lenguajeVersionAndIcono,$valor,$data);
                
            }elseif($valor["nombreComplejo"]==$complejo  && $valor["sala"] != $sala) {
                $pelicula=$valor["codVista"];
                $sala = $valor["sala"];
                $versiones = explode(" ",$valor["versiones"]);
                
                $lenguajeVersionAndIcono=$apiUtilFunctions::getLenguajeVersionAndIcono($versiones);
                $peliculas[] =$this->getArrayPeliculas($lenguajeVersionAndIcono,$valor,$data);
            }
        }
        return $peliculas;
    }
     
    /**  .
     * @param array $lenguajeVersionAndIcono Lenguaje version e Icono de la Versión .
     * @param array $valor posición del array mapeado.
     * @param array $data Array de Programacion.
     * @return array información de la pelicula.
    **/
    private function getArrayPeliculas($lenguajeVersionAndIcono,$valor,$data){

        return array("peliculaNombre"=>$valor["referencia"],
            "sala"=>$valor["sala"],
            "lenguaje"=>$lenguajeVersionAndIcono["lenguaje"],
            "icono_lenguaje"=>$lenguajeVersionAndIcono["icono_lenguaje"],
            "censura"=>$valor["censura"],
            "peliculaCodigo"=>$valor["codVista"],
            "peliculaReferencia"=>$valor["referencia"],
            "versionDetalle"=>$lenguajeVersionAndIcono["versionDetalle"],
            "icono_version"=>$lenguajeVersionAndIcono["icono_version"],
            "version"=>$valor["versiones"],
            "funciones"=>$this->mapFunciones($valor["nombreComplejo"],$valor["sala"],$valor["codVista"],$data)
        );
    }

    /**  
     * @param string $complejo Nombre de Complejo .
     * @param string $sala Sala.
     * @param string $pelicula Pelicula.
     * @param array $data Array de Programacion.
     * @return array funciones.
    **/
    function mapFunciones($complejo,$sala,$pelicula,$data){
        $url = "";
        $funcion = "";
        $color="#5cb85c";
        $difference = 0;
        foreach($data as $valor){
            if($valor["nombreComplejo"]==$complejo && $valor["sala"]==$sala && $valor["codVista"] == $pelicula){
                $color="#5cb85c";
                if($funcion!=$valor["hora"] &&  $valor["codVista"] == $pelicula){
                
                   /* $response = Cors::requestGet(Constantes::APICORS["semaforo"], 
                                                  array('Accept' => 'application/json'),
                                                  array('method' => 'getsessionoccupation','sesionid' => $valor["sesionID"])
                                                );*/
                    $sql= " SELECT seats_avalaible, color FROM session_occupation WHERE session_id =".$valor["sesionID"];
                    $sessiones = $this->dao->prepareStament($sql,"asociativo");
                    if(count($sessiones)==0) {
                        $jResultService["color"]="#5cb85c";
                        $jResultService["asientosdisponibles"]="";                           
                    } else {
                        // EVALUAR SI LA FUNCION ES DEL DIA DE HOY Y LA FUNCION YA CADUCO, EN ESE CASO DEVOLVER ROJO
                        if ($valor["fecha"] == date("d-m-Y")){
                            $starttimestamp = strtotime(date("d-m-Y H:m:s"));
                            $endtimestamp = strtotime($valor["fecha"]." ".$valor["hora"]);
                            $difference = ($endtimestamp - $starttimestamp)/3600;
                            if ($difference < 0){
                                $color="#ff0a19"; // si ya paso la funcion, colocar rojo
                            }
                        }else{
                            $color=$sessiones[0]["color"];
                        }
                        $jResultService["color"]=$color;
                        $jResultService["asientosdisponibles"]=$sessiones[0]["seats_avalaible"];
                    }                            
                    /*if(!$response->body->result) {
                        $jResultService["color"]="#5cb85c";
                        $jResultService["asientosdisponibles"]="";   
                        
                    } else {

                        $jResultService["color"]=$response->body->color;
                        $jResultService["asientosdisponibles"]=$response->body->asientosdisponibles;
                    }*/
                    $funcion=$valor["hora"];                    
                    $funciones[] = array("hora"=>$valor["hora"],
                        "ventaActiva"=>$valor["ventaActiva"],
                        "fecha"=>$valor["fecha"],
                        "abierta"=>$valor["ventaActiva"],
                        "censura"=>$valor["censura"],
                        "seleccionAsientosActiva"=>($valor["seleccionAsientosActiva"]=="Y") ? true : false,
                        "horaFormateada"=>$valor["horaFormateada"],
                        "sesionID"=>$valor["sesionID"],
                        "asientosdisponibles"=>$jResultService["asientosdisponibles"],
                        "color"=>$jResultService["color"],
                        "hourdiff"=>$difference,
                        "serverdatetime"=>date("Y/m/d H:m:s"),
                    );
                }
            }
        }
        return $funciones;
    }

    /**  .
     * @param array $data Array de Programacion por Complejo.
     * @return string json programacion.
    **/    
    function getProgramacionComplejo($data){
        $dataJson=[];
        $complejo=[];
        foreach($data as $valor){
            if ($complejo!=$valor["codigoComplejo"]){
                $complejo=$valor["codigoComplejo"];
                $dataJson["complejos"][]=array("codigoComplejo"=>$valor["codigoComplejo"],
                                               "complejo"=>$valor["nombreComplejo"],
                                               "peliculas"=>$this->mapPeliculasComplejo($valor["codigoComplejo"],$data));
            }
        }
        return $dataJson;
    }

    /**  .
     * @param string $complejo Complejo.
     * @param array $data arreglo original de programación.
     * @return string json peliculas por complejo.
    **/    
    function mapPeliculasComplejo($complejo,$data){
        $pelicula = "";
        foreach($data as $valor){
            if($valor["codigoComplejo"]==$complejo && $pelicula!=$valor["referencia"]){
                $pelicula=$valor["referencia"];
                $peliculas[] = array("nombre"=>$valor["referencia"],
                                     "genero"=>$valor["genero"],
                                     "censura"=>$valor["censura"],
                                     "duracion"=>$valor["duracion"],
                                     "director"=>$valor["director"],
                                     "detallePelicula"=>$this->mapeaPeliculasDetalle($pelicula,$data)
                                    );
            }
        }
        return $peliculas;
    }
    
    /**  .
     * @param string $pelicula descripcion de la pelicula.
     * @param array $data Array de Programacion.
     * @return array información de la pelicula.
    **/
    function mapeaPeliculasDetalle($pelicula,$data){
        $apiUtilFunctions = new ApiUtilFunctions();
        $versiones =  "";
        $codigoPelicula = "";
        $sala = "";
        foreach($data as $valor){
            if($valor["referencia"]==$pelicula  &&  $valor["codVista"] != $codigoPelicula){
                $codigoPelicula=$valor["codVista"];
                $sala = $valor["sala"];
                $versiones = explode(" ",$valor["versiones"]);
                $lenguajeVersionAndIcono=$apiUtilFunctions::getLenguajeVersionAndIcono($versiones); 
                $detalles[] =$this->getDetalles($lenguajeVersionAndIcono,$valor,$data);
            }elseif($valor["referencia"]==$pelicula  &&  $valor["sala"] != $sala){
                $versiones = explode(" ",$valor["versiones"]);
                $sala = $valor["sala"];
                $lenguajeVersionAndIcono=$apiUtilFunctions::getLenguajeVersionAndIcono($versiones); 
                $detalles[] =$this->getDetalles($lenguajeVersionAndIcono,$valor,$data);
            }
        }
        return $detalles;
    }
    
    /**  .
     * @param array $lenguajeVersionAndIcono Lenguaje version e Icono de la Versión .
     * @param array $valor posición del array mapeado.
     * @param array $data Array de Programacion.
     * @return array información de la pelicula.
    **/
    function getDetalles($lenguajeVersionAndIcono,$valor,$data){
        return array(
            "sala"=>strtoupper($valor["sala"]),
            "lenguaje"=>$lenguajeVersionAndIcono["lenguaje"],
            "icono_lenguaje"=>$lenguajeVersionAndIcono["icono_lenguaje"],
            "peliculaCodigo"=>$valor["codVista"],
            "versionDetalle"=>$lenguajeVersionAndIcono["versionDetalle"],
            "icono_version"=>$lenguajeVersionAndIcono["icono_version"],
            "version"=>$valor["versiones"],
            "funciones"=>$this->mapFuncionesPeliculasComplejos($valor["nombreComplejo"],$valor["sala"],$valor["codVista"],$data)
        );
    }

    /**  
     * @param string $complejo Nombre de Complejo .
     * @param string $sala Sala.
     * @param string $pelicula Pelicula.
     * @param array $data Array de Programacion.
     * @return array funciones.
    **/
    function mapFuncionesPeliculasComplejos($complejo,$sala,$pelicula,$data){
        $funcion="";
        $color="#5cb85c";
        $difference = 0;
        foreach($data as $valor){
            if($valor["nombreComplejo"]==$complejo && $valor["sala"]==$sala && $valor["codVista"] == $pelicula){
                $color="#5cb85c";
                if($funcion!=$valor["hora"] && $valor["codVista"] == $pelicula){
                    $funcion=$valor["hora"];
                    $sql= " SELECT seats_avalaible, color FROM session_occupation WHERE session_id =".$valor["sesionID"];
                    $sessiones = $this->dao->prepareStament($sql,"asociativo");
                    if(count($sessiones)==0) {
                        $jResultService["color"]="#ff0a19";
                        $jResultService["asientosdisponibles"]="";                           
                    } else {
                        // EVALUAR SI LA FUNCION ES DEL DIA DE HOY Y LA FUNCION YA CADUCO, EN ESE CASO DEVOLVER ROJO
                        if ($valor["fecha"] == date("Y/m/d")){
                            $starttimestamp = strtotime(date("Y/m/d H:m:s"));
                            $endtimestamp = strtotime($valor["fecha"]." ".$valor["hora"]);
                            $difference = ($endtimestamp - $starttimestamp)/3600;
                            if ($difference < 0){
                                $color="#ff0a19"; // si ya paso la funcion, colocar rojo
                            }else{
                                $color=$sessiones[0]["color"];
                            }
                        }                        
                        $jResultService["color"]=$color;
                        $jResultService["asientosdisponibles"]=$sessiones[0]["seats_avalaible"];
                    }
                    $funciones[] = array("hora"=>$valor["hora"],
                        "ventaActiva"=>$valor["ventaActiva"],
                        "fecha"=>$valor["fecha"],
                        "abierta"=>$valor["ventaActiva"],
                        "seleccionAsientosActiva"=>($valor["seleccionAsientosActiva"]=="Y") ? true : false,
                        "horaFormateada"=>$valor["horaFormateada"],
                        "sesionID"=>$valor["sesionID"],
                        "asientosDisponible"=>$jResultService["asientosdisponibles"],
                        "colorSemaforo"=>$jResultService["color"],
                        "hourdiff"=>$difference,
                        "serverdatetime"=>date("Y/m/d H:m:s"),
                    );
                }
            }
        }
        return $funciones;
    }



}
