<?php
namespace App\tests\Lib;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Lib\ApiUtilFunctions;
class ApiUtilFunctionsTest extends WebTestCase
{
    public $apiUtilFunction;

    public function setUp()
    {
        $this->apiUtilFunction = new ApiUtilFunctions();

    }

    public function testGetMonthDescription(){

           $this->assertEquals("ENERO", $this->apiUtilFunction->getMonthDescription()[1]);
           $this->assertEquals("FEBRERO", $this->apiUtilFunction->getMonthDescription()[2]);
           $this->assertEquals("MARZO", $this->apiUtilFunction->getMonthDescription()[3]);
           $this->assertEquals("ABRIL", $this->apiUtilFunction->getMonthDescription()[4]);
           $this->assertEquals("MAYO", $this->apiUtilFunction->getMonthDescription()[5]);
           $this->assertEquals("JUNIO", $this->apiUtilFunction->getMonthDescription()[6]);
           $this->assertEquals("JULIO", $this->apiUtilFunction->getMonthDescription()[7]);
           $this->assertEquals("AGOSTO", $this->apiUtilFunction->getMonthDescription()[8]);
           $this->assertEquals("SEPTIEMBRE", $this->apiUtilFunction->getMonthDescription()[9]);
           $this->assertEquals("OCTUBRE", $this->apiUtilFunction->getMonthDescription()[10]);
           $this->assertEquals("NOVIEMBRE", $this->apiUtilFunction->getMonthDescription()[11]);
           $this->assertEquals("DICIEMBRE", $this->apiUtilFunction->getMonthDescription()[12]);
    }

    public function testGetLenguajeVersionAndIcono()
    {
         $this->assertEquals("SUB", $this->apiUtilFunction->getLenguajeVersionAndIcono(['DIG','SUB'])["lenguaje"]);
         $this->assertEquals("ESP", $this->apiUtilFunction->getLenguajeVersionAndIcono(['DIG','ESP'])["lenguaje"]);
         $this->assertEquals("SUB", $this->apiUtilFunction->getLenguajeVersionAndIcono(['3D','SUB'])["lenguaje"]);
         $this->assertEquals("ESP", $this->apiUtilFunction->getLenguajeVersionAndIcono(['3D','ESP'])["lenguaje"]);
         $this->assertEquals("SUB", $this->apiUtilFunction->getLenguajeVersionAndIcono(['4DX','SUB'])["lenguaje"]);
         $this->assertEquals("ESP", $this->apiUtilFunction->getLenguajeVersionAndIcono(['4DX','ESP'])["lenguaje"]);
         $this->assertEquals("DIG", $this->apiUtilFunction->getLenguajeVersionAndIcono(['DIG','SUB'])["versionDetalle"]);
         $this->assertEquals("DIG", $this->apiUtilFunction->getLenguajeVersionAndIcono(['DIG','ESP'])["versionDetalle"]);
         $this->assertEquals("3D", $this->apiUtilFunction->getLenguajeVersionAndIcono(['3D','SUB'])["versionDetalle"]);
         $this->assertEquals("3D", $this->apiUtilFunction->getLenguajeVersionAndIcono(['3D','ESP'])["versionDetalle"]);
         $this->assertEquals("4DX", $this->apiUtilFunction->getLenguajeVersionAndIcono(['4DX','SUB'])["versionDetalle"]);
         $this->assertEquals("4DX", $this->apiUtilFunction->getLenguajeVersionAndIcono(['4DX','ESP'])["versionDetalle"]);
    }

    public function testGetVersionsDB()
    {
         $this->assertEquals("3D|4DX|2D", $this->apiUtilFunction->getVersionsDB('3D ESP|3D SUB|4DX ESP|4DX SUB|DIG ESP|DIG SUB|'));
         $this->assertEquals("3D|4DX", $this->apiUtilFunction->getVersionsDB('3D ESP|3D SUB|4DX ESP|4DX SUB|'));
         $this->assertEquals("3D|2D", $this->apiUtilFunction->getVersionsDB('3D ESP|3D SUB|DIG ESP|DIG SUB|'));
         $this->assertEquals("3D|2D", $this->apiUtilFunction->getVersionsDB('3D ESP|3D SUB|DIG ESP|DIG SUB|'));
         $this->assertEquals("2D", $this->apiUtilFunction->getVersionsDB('DIG ESP|DIG SUB|'));
         $this->assertEquals("3D", $this->apiUtilFunction->getVersionsDB('3D ESP|3D SUB|'));
         $this->assertEquals("4DX", $this->apiUtilFunction->getVersionsDB('4DX ESP|4DX SUB|'));

    }
    


}