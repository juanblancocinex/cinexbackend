<?php
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class OcupacionMapaControllerTest extends WebTestCase
{
    private $http;

    public function setUp()
    {
        $client = static::createClient();
        $servidorTest = $client->getKernel()->getContainer()->getParameter('servidortest');
        $this->http = new GuzzleHttp\Client(['base_uri' => $servidorTest]);
    }

    public function tearDown() {
        $this->http = null;
    }
  
    public function testGetOccupatedSeatsFromUserId()
    {
        $response = $this->http->request('GET', 'api/cinexdataservice/getoccupatedseatsfromuserid/sesionid/2222dddd/userid/333333');
        $this->assertEquals(200, $response->getStatusCode());
    
    }

    public function testGetLastOccupatedSeatsFromUserID()
    {
        $response = $this->http->request('GET', 'api/cinexdataservice/getlastoccupatedseatsfromuserid/sesionid/335555/userid/555554');
        $this->assertEquals(200, $response->getStatusCode());
    
    }

    public function testCleanOccupatedSeatsFromUserId()
    {
        $response = $this->http->request('GET', 'api/cinexdataservice/cleanoccupatedseatsfromuserid/sesionid/234/userid/23');
        $this->assertEquals(200, $response->getStatusCode());
    
    }


    public function testUpdateBadget()
    {
        $response = $this->http->request('GET', 'api/cinexdataservice/updateBadget/sesionid/222/userid/2222');
        $this->assertEquals(200, $response->getStatusCode());
    
    }

    public function testGetCountOccupatedSeats()
    {
        $response = $this->http->request('GET', 'api/cinexdataservice/getcountoccupatedseats/sesionid/2222/userid/444');
        $this->assertEquals(200, $response->getStatusCode());
    
    }

    public function testSetSeat()
    {
        $response = $this->http->request('POST', 'api/cinexdataservice/setseat', [
            'form_params' => ["userid"=> "4670362",
            "sessionid"=> "58240",
            "row"=> "8",
            "column"=> "11",
            "posicion"=> "D12",
            "typecode"=> "432",
            "ticketprice"=> "600000",
            "tickettype"=> "STANDARD"]
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    
    }
    
    public function testUnsetSeat()
    {
        $response = $this->http->request('POST', 'api/cinexdataservice/unsetseat', [
            'form_params' => [  "userid"=> "4670362",
            "sessionid"=> "58240",
            "row"=> "8",
            "column"=> "11"]
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    
    }

    public function testGetSeatStatus()
    {
        $response = $this->http->request('GET', 'api/cinexdataservice/getseatstatus/sesionid/222dddddd22222/fila/44/columna/2');
        $this->assertEquals(200, $response->getStatusCode());
    
    }
    
}

