<?php
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class ContactControllerTest extends WebTestCase
{
    private $http;

    public function setUp()
    {
        $client = static::createClient();
        $servidorTest = $client->getKernel()->getContainer()->getParameter('servidortest');
        $this->http = new GuzzleHttp\Client(['base_uri' => $servidorTest]);
    }

    public function tearDown() {
        $this->http = null;
    }
  
    public function testAgregarContacto()
    {
        $response = $this->http->request('GET', 'api/contacto/agregarContacto/ciudad/caracas/topico/11/nombre/maria/apellido/diaz/email/prueba@cinex.com.ve/telefono/2222/cine/dddd/comentario/ddd/nombre_ciudad/ddd22/nombre_cine/cinex');
        $this->assertEquals(200, $response->getStatusCode());
    
    }

   

}

