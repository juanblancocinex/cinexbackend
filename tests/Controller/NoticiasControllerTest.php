<?php
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class NoticiasControllerTest extends WebTestCase
{
  
    private $http;

    public function setUp()
    {
        $client = static::createClient();
        $servidorTest = $client->getKernel()->getContainer()->getParameter('servidortest');
        $this->http = new GuzzleHttp\Client(['base_uri' => $servidorTest]);
    }

    public function tearDown() {
        $this->http = null;
    }
  
    public function testGetNoticiasLimit()
    {
        $response = $this->http->request('GET', 'api/noticias/limit/1');
        $this->assertEquals(200, $response->getStatusCode());
    
    }
    public function testGetNoticias()
    {
        $response = $this->http->request('GET', 'api/noticias/');
        $this->assertEquals(200, $response->getStatusCode());
    
    }
    public function testGetNoticiasById()
    {
        $response = $this->http->request('GET', 'api/noticias/id/2');
        $this->assertEquals(200, $response->getStatusCode());
    
    }
}

