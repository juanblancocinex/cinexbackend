<?php
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class VouchersDigitalesControllerTest extends WebTestCase
{
  
    private $http;

    public function setUp()
    {
        $client = static::createClient();
        $servidorTest = $client->getKernel()->getContainer()->getParameter('servidortest');
        $this->http = new GuzzleHttp\Client(['base_uri' => $servidorTest]);
    }

    public function tearDown() {
        $this->http = null;
    }
  
    public function testGetVouchersDigitales()
    {                                               
        $response = $this->http->request('GET', 'api/vouchersdigitales/cedula/1233/tipovoucher/2222');
        $this->assertEquals(200, $response->getStatusCode());
    
    }
 
    public function testGetCinexPassRedeemStatus()
    {
        $response = $this->http->request('GET', 'api/vouchersdigitalestatus/codigo/dddddd');
        $this->assertEquals(200, $response->getStatusCode());
    }      

    public function testGetCinexPassOwner()
    {
        $response = $this->http->request('GET', 'api/vouchersdigitalesusuario/cedula/2222222/tipovoucher/33333');
        $this->assertEquals(200, $response->getStatusCode());
    }   
    
}

