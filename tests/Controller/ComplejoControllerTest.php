<?php
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class ComplejoControllerTest extends WebTestCase
{
  
    private $http;

    public function setUp()
    {
        $client = static::createClient();
        $servidorTest = $client->getKernel()->getContainer()->getParameter('servidortest');
        $this->http = new GuzzleHttp\Client(['base_uri' => $servidorTest]);
    }

    public function tearDown() {
        $this->http = null;
    }
  
    public function testGetComplejoDetalles()
    {
        $response = $this->http->request('GET', 'api/complejos/complejo/detalle/LID');
        $this->assertEquals(200, $response->getStatusCode());
    
    }
    public function testGetComplejos()
    {
        $response = $this->http->request('GET', 'api/complejos/');
        $this->assertEquals(200, $response->getStatusCode());
    
    }
}

