<?php
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class MovistarControllerTest extends WebTestCase
{
  
    private $http;

    public function setUp()
    {
        $client = static::createClient();
        $servidorTest = $client->getKernel()->getContainer()->getParameter('servidortest');
        $this->http = new GuzzleHttp\Client(['base_uri' => $servidorTest]);
    }

    public function tearDown() {
        $this->http = null;
    }
  
  
    public function testBuscarPuntos()
    {
        $response = $this->http->request('GET', 'api/payment/buscarpuntosmovistar/cedula/23333/movil/0412555555/reintentos/0');
        $this->assertEquals(200, $response->getStatusCode());
    
    }
    
    public function testDescontarPuntos()
    {
        $response = $this->http->request('GET', 'api/payment/descontarpuntosmovistar/token/22222/cedula/222222/movil/04128888822/puntos/2/reintentos/0');
        $this->assertEquals(200, $response->getStatusCode());
    
    }

    public function testGenerateToken()
    {
        $response = $this->http->request('POST', 'api/payment/generatetokenmovistar/userid/2');
        $this->assertEquals(200, $response->getStatusCode());
    
    }
    public function testDeleteToken()
    {
        $response = $this->http->request('DELETE', 'api/payment/deletetokenmovistar/userid/2');
        $this->assertEquals(200, $response->getStatusCode());
    
    }
}

