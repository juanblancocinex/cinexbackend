<?php
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class VistaTicketingServiceControllerTest extends WebTestCase
{
  
    private $http;

    public function setUp()
    {
        $client = static::createClient();
        $servidorTest = $client->getKernel()->getContainer()->getParameter('servidortest');
        $this->http = new GuzzleHttp\Client(['base_uri' => $servidorTest]);
    }

    public function tearDown() {
        $this->http = null;
    }
  
  
    public function testSetSelectedSeats()
    {
        $response = $this->http->request('GET', 'api/vistaticketingservice/setselectedseat/dataseats/23/params/222');
        $this->assertEquals(200, $response->getStatusCode());
    
    }
    
    public function testSetCancelOrder()
    {
        $response = $this->http->request('GET', 'api/vistaticketingservice/cancelorder/usersession/23333');
        $this->assertEquals(200, $response->getStatusCode());
    
    }

    public function testGetOrder()
    {
        $response = $this->http->request('GET', 'api/vistaticketingservice/getorder/sessionid/3355555');
        $this->assertEquals(200, $response->getStatusCode());
    
    }    

    public function testAddVoucher()
    {
        $response = $this->http->request('GET', 'api/vistaticketingservice/addtvouchert/datatickets/3dddd3/params/d3333/mode/2');
        $this->assertEquals(200, $response->getStatusCode());
    
    }    

    public function testAddTicket()
    {
        $response = $this->http->request('GET', 'api/vistaticketingservice/addticket/datatickets/22222/params/222');
        $this->assertEquals(200, $response->getStatusCode());
    
    }      

    public function testGetMap()
    {
        $response = $this->http->request('GET', 'api/vistaticketingservice/getmap/cinemaid/LID/sessionid/333333/usersessionid/dsd33333/reintentos/0');
        $this->assertEquals(200, $response->getStatusCode());
    
    }   

}

