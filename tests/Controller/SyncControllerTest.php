<?php
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class SyncControllerTest extends WebTestCase
{
  
    private $http;

    public function setUp()
    {
        $client = static::createClient();
        $servidorTest = $client->getKernel()->getContainer()->getParameter('servidortest');
        $this->http = new GuzzleHttp\Client(['base_uri' => $servidorTest]);
    }

    public function tearDown() {
        $this->http = null;
    }
  
    public function testGetCommingSoon()
    {
        $response = $this->http->request('GET', 'api/datasynccontroller/getcommingsoon');
        $this->assertEquals(200, $response->getStatusCode());
    
    }
    public function testGetSinecMovieInfo()
    {
        $response = $this->http->request('GET', 'api/datasynccontroller/getsinecmovieinfo/movieid/MEGALODON');
        $this->assertEquals(200, $response->getStatusCode());
    }
    public function testGetSinecMovieList()
    {
        $response = $this->http->request('GET', 'api/datasynccontroller/getsinecmovielist/cinemaid/MEGALODON');
        $this->assertEquals(200, $response->getStatusCode());
    }

  /*  public function testDoSync()
    {
        $response = $this->http->request('GET', 'api/datasynccontroller/dosync');
        $this->assertEquals(200, $response->getStatusCode());
    }*/
    


}

