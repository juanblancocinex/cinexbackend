<?php
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class TransaccionControllerTest extends WebTestCase
{
  
    private $http;

    public function setUp()
    {
        $client = static::createClient();
        $servidorTest = $client->getKernel()->getContainer()->getParameter('servidortest');
        $this->http = new GuzzleHttp\Client(['base_uri' => $servidorTest]);
    }

    public function tearDown() {
        $this->http = null;
    }
  
    public function testCreateTransaccion()
    {
        $response = $this->http->request('POST', 'api/transaccion/create', [ 'form_params' => [
                    "ordenid"=>"wpgcnx0001",
                    "nombreentarjeta"=>"Luis%20Baez",
                    "celular"=>"04125950736",
                    "fecha"=>"2018-09-11",
                    "tarjeta"=>"2491971869049848",
                    "tipotarjeta"=>"MASTERCARD",
                    "monto"=>"0,01",
                    "asientos"=>"A14,A15",
                    "programacionid"=>"194907",
                    "cedula"=>"V12557310",
                    "pelicula"=>"HOTEL TRANSYLVANIA 3",
                    "hora"=>"17=>40=>00",
                    "complejo"=>" LOS PR\u00d3CERES",
                    "mensaje"=>"TRANS.RECHAZADA",
                    "sala"=>"Sala 1","cantidadboletos"=>2,
                    "fechafuncion"=>"11-09-2018",
                    "tecnologia"=>"MOV",
                    "correo"=>"baezgregoric@hotmail.com",
                    "vencashvoucher"=>null,
                    "bookingnumber"=>123,
                    "detalleboleto"=>null,
                    "vistamontoincent"=>null,
                    "porreversar"=>"Y",
                    "causa"=>"VISTA",
                    "respuesta"=>"REPROBADO",
                    "montompfull"=>null,
                    "serviciompfull"=>null,
                    "voucher"=>"MARIANO HABLAME",
                    "tipotrans"=>0,
                    "membercard"=>"9009260902860390",
                    "vistavoucher"=>null,"usersessionid"=>
                    "28417f1c42feb16da9ca4cc71",
                    "codigocomplejo"=>"IPC","codigopelicula"=>
                    "HO00002684","authid"=>"",
                    "memberid"=>"CCRLYMG75YCF",
                    "giftnombre"=>null,
                    "giftcorreo"=>null,
                    "giftmensaje"=>null,
                    "giftimage"=>null,
                    "giftcinta"=>null,
                    "giftde"=>null
            ]       
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }
    
}

