<?php
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class VouchersDigitalesTipoControllerTest extends WebTestCase
{
  
    private $http;

    public function setUp()
    {
        $client = static::createClient();
        $servidorTest = $client->getKernel()->getContainer()->getParameter('servidortest');
        $this->http = new GuzzleHttp\Client(['base_uri' => $servidorTest]);
    }

    public function tearDown() {
        $this->http = null;
    }
  
    public function testGetVouchersDigitalesTipo()
    {                                               
        $response = $this->http->request('GET', 'api/vouchersdigitalestipo');
        $this->assertEquals(200, $response->getStatusCode());
    
    }
 

}

