<?php
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class PeliculaTestControllerTest extends WebTestCase
{
  
    private $http;

    public function setUp()
    {
        $client = static::createClient();
        $servidorTest = $client->getKernel()->getContainer()->getParameter('servidortest');
        $this->http = new GuzzleHttp\Client(['base_uri' => $servidorTest]);
    }

    public function tearDown() {
        $this->http = null;
    }
  
    public function testGetProximamente()
    {
        $response = $this->http->request('GET', 'api/peliculas/proximamente', [
            'query' => ['mostrartodo' => 'si']
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    
    }
    public function testGetBuscarPeliculas()
    {
        $response = $this->http->request('GET', 'api/peliculas/buscarpelicula/prueba');
        $this->assertEquals(200, $response->getStatusCode());
    }
    public function testGetPeliculaSesionesEspeciales()
    {
        $response = $this->http->request('GET', 'api/peliculas/sesiones/especiales/1');
        $this->assertEquals(200, $response->getStatusCode());
    
    }
    



}

