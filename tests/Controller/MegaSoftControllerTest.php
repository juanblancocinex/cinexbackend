<?php
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class MegaSoftControllerTest extends WebTestCase
{
  
    private $http;

    public function setUp()
    {
        $client = static::createClient();
        $servidorTest = $client->getKernel()->getContainer()->getParameter('servidortest');
        $this->http = new GuzzleHttp\Client(['base_uri' => $servidorTest]);
    }

    public function tearDown() {
        $this->http = null;
    }
  
  
    public function testMegasoftdoPayment()
    {
        $response = $this->http->request('GET', 'api/payment/dopayment/token/2222/cod_afiliacion/22233/transcode/33444/pan/4455/cvv2/3444/cid/4455/expdate/2018-01-01/amount/3333/client/3445/factura/44555/userid/3334/reintentos/333/testmode/222');
        $this->assertEquals(200, $response->getStatusCode());
    
    }
    
    public function testMegasoftGenerateToken()
    {
        $response = $this->http->request('Post', 'api/payment/megasoftgeneratetoken/userid/222');
        $this->assertEquals(200, $response->getStatusCode());
    
    }

    public function testMegasoftDeleteToken()
    {
        $response = $this->http->request('Delete', 'api/payment/megasoftdeleteToken/userid/1');
        $this->assertEquals(200, $response->getStatusCode());
    
    }

    public function testMegasoftValidateToken()
    {
        $response = $this->http->request('Get', 'api/payment/megasoftvalidatetoken/token/1/userid/22');
        $this->assertEquals(200, $response->getStatusCode());
    
    }
}

