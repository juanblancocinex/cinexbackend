<?php
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class SessionOccupationTest extends WebTestCase
{
  
    private $http;

    public function setUp()
    {
        $client = static::createClient();
        $servidorTest = $client->getKernel()->getContainer()->getParameter('servidortest');
        $this->http = new GuzzleHttp\Client(['base_uri' => $servidorTest]);
    }

    public function tearDown() {
        $this->http = null;
    }
  
    public function testGetSessionOccupation()
    {
        $response = $this->http->request('GET', 'api/cinexdataservice/getsessionoccupation/222');
        $this->assertEquals(200, $response->getStatusCode());
    
    }
    public function testGetComplejos()
    {
        $response = $this->http->request('GET', 'api/complejos/');
        $this->assertEquals(200, $response->getStatusCode());
    
    }
}

