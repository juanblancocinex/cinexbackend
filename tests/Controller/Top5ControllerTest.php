<?php
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class Top5ControllerTest extends WebTestCase
{
  
    private $http;

    public function setUp()
    {
        $client = static::createClient();
        $servidorTest = $client->getKernel()->getContainer()->getParameter('servidortest');
        $this->http = new GuzzleHttp\Client(['base_uri' => $servidorTest]);
    }

    public function tearDown() {
        $this->http = null;
    }
  
    public function testTop5()
    {
        $response = $this->http->request('GET', 'api/top5');
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    public function getTop5Tiny()
    {
        $response = $this->http->request('GET', 'api/cinexdataservice/top5tiny');
        $this->assertEquals(200, $response->getStatusCode());
    }
}

