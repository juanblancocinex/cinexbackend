<?php
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class ProgramacionTestControllerTest extends WebTestCase
{
  
    private $http;

    public function setUp()
    {
        $client = static::createClient();
        $servidorTest = $client->getKernel()->getContainer()->getParameter('servidortest');
        $this->http = new GuzzleHttp\Client(['base_uri' => $servidorTest]);
    }

    public function tearDown() {
        $this->http = null;
    }
  
    public function testGetCartelera()
    {
        $response = $this->http->request('GET', 'api/programacion/cartelera/');
        $this->assertEquals(200, $response->getStatusCode());
    
    }
    public function testGetCarteleraTiny()
    {
        $response = $this->http->request('GET', 'api/programacion/carteleratiny/');
        $this->assertEquals(200, $response->getStatusCode());
    
    }
    public function testGetBusquedaProgramacionByPeliculaAndFechaAndVersion()
    {
        $response = $this->http->request('GET', 'api/programacion/pelicula/prueba/fecha/2018-01-01/version/2D');
        $this->assertEquals(200, $response->getStatusCode());
    
    }
    public function testGetBusquedaProgramacionByPeliculaAndFecha()
    {
        $response = $this->http->request('GET', 'api/programacion/pelicula/prueba/fecha/2018-01-01');
        $this->assertEquals(200, $response->getStatusCode());
    
    }
    public function testGetBusquedaProgramacionByPelicula()
    {
        $response = $this->http->request('GET', 'api/programacion/pelicula/prueba');
        $this->assertEquals(200, $response->getStatusCode());
    
    }
    public function testGetBusquedaProgramacionComplejo()
    {
        $response = $this->http->request('GET', 'api/programacion/complejo/LID/fecha/2018-01-01');
        $this->assertEquals(200, $response->getStatusCode());
    
    }
    public function testGetProgramacionFechas()
    {
        $response = $this->http->request('GET', 'api/programacion/fechas/complejo/LID');
        $this->assertEquals(200, $response->getStatusCode());
    
    }
    public function testGetObtenerBuscadores()
    {
        $response = $this->http->request('GET', 'api/programacion/obtenerbuscadores', [
            'form_params' => ['complejo' => 'LID',
            'pelicula' => 'Megalodon',
            'version' => '2D',
            'ciudad' => 'Caracas'
            ]
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    
    }


    
}

