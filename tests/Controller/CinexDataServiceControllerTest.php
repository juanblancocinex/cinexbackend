<?php
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class CinexDataServiceControllerTest extends WebTestCase
{
    private $http;

    public function setUp()
    {
        $client = static::createClient();
        $servidorTest = $client->getKernel()->getContainer()->getParameter('servidortest');
        $this->http = new GuzzleHttp\Client(['base_uri' => $servidorTest]);
    }

    public function tearDown() {
        $this->http = null;
    }
  
    public function testGetSessionList()
    {
        $response = $this->http->request('GET', 'api/cinexdataservice/getsessionlist/LID/MEGALODON');
        $this->assertEquals(200, $response->getStatusCode());
    
    }


    public function testMovieinfoList()
    {
        $response = $this->http->request('GET', 'api/cinexdataservice/getmovieinfolist/MEGALODON');
        $this->assertEquals(200, $response->getStatusCode());
    
    }

    public function testGetCinemaList()
    {
        $response = $this->http->request('GET', 'api/cinexdataservice/getcinemalist/moviename/MEGALODON/cinemaid/LID');
        $this->assertEquals(200, $response->getStatusCode());
    
    }


    public function testGetCinemaListByMovie()
    {
        $response = $this->http->request('GET', 'api/cinexdataservice/getcinemalistbymovie/moviename/MEGALODON');
        $this->assertEquals(200, $response->getStatusCode());
    
    }

    public function testGetCinemaListAll()
    {
        $response = $this->http->request('GET', 'api/cinexdataservice/getcinemalistall');
        $this->assertEquals(200, $response->getStatusCode());
    
    }
    
    public function testGetMoviePeopleAll()
    {
        $response = $this->http->request('GET', 'api/cinexdataservice/getmoviepeople/HO00000609');
        $this->assertEquals(200, $response->getStatusCode());
    
    }

    public function testGetTicketTypeList()
    {
        $response = $this->http->request('GET', 'api/cinexdataservice/gettickettypelist', [
            'query' => ['cinemaid' => 'LID',
            'sessionid' => '444555555']
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }


    public function testGetSessionInfo()
    {
        $response = $this->http->request('GET', 'api/cinexdataservice/getsessioninfo', [
            'query' => ['cinemaid' => 'LID',
            'sessionid' => '444555555']
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testGetSeatsAvailable(){
        $response = $this->http->request('GET', 'api/cinexdataservice/getseatsavailable', [
            'query' => ['cinemaid' => 'LID',
            'sessionid' => '444555555']
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    //PENDIENTE
    /*public function testGetPercentageAvailable(){
        $response = $this->http->request('GET', 'api/cinexdataservice/getpercentageavailable', [
            'query' => ['cinemaid' => 'LID',
            'sessionid' => '444555555',
            'uservistaid'=>'DDDDDD',
            'reintentos'=>2]
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }*/

    
    
    public function testGetMovieOccupationInfo(){
        $response = $this->http->request('GET', 'api/cinexdataservice/getMovieOccupationInfo', [
            'query' => ['movieName' => 'MEGALODON',
            'datesession' => '2018-01-01']
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }


    public function testGetMovieOccupationInfoForCinema(){
        $response = $this->http->request('GET', 'api/cinexdataservice/getMovieOccupationInfoForCinema', [
            'query' => ['movieName' => 'MEGALODON',
            'datesession' => '2018-01-01']
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testGetOccupatedSeatsFromUserId()
    {
        $response = $this->http->request('GET', 'api/cinexdataservice/getoccupatedseatsfromuserid/sesionid/222/userid/22333');
        $this->assertEquals(200, $response->getStatusCode());
    
    }

    public function testCleanMovieListSchedule()
    {
        $response = $this->http->request('DELETE', 'api/cinexdataservice/cleanmovielistschedule');
        $this->assertEquals(200, $response->getStatusCode());
    
    }
    
    public function testGetBookingFeeValue()
    {
        $response = $this->http->request('GET', 'api/cinexdataservice/getbookingfeevalue', [
            'query' => ['complejoid' => 'REC',
            'codigoticketho' => '4601']
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    
    }

    public function testDoCheckCommingSoon()
    {
        $response = $this->http->request('DELETE', 'api/cinexdataservice/cinexdataservicedoCheckcommingsoon');
        $this->assertEquals(200, $response->getStatusCode());
    
    }

}

