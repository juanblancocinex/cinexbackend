<?php
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class UsuarioControllerTest extends WebTestCase
{
  
    private $http;

    public function setUp()
    {
        $client = static::createClient();
        $servidorTest = $client->getKernel()->getContainer()->getParameter('servidortest');
        $this->http = new GuzzleHttp\Client(['base_uri' => $servidorTest]);
    }

    public function tearDown() {
        $this->http = null;
    }
  
    public function testGetUser()
    {
        $response = $this->http->request('GET', 'api/usuario/prueba/1233');
        $this->assertEquals(200, $response->getStatusCode());
    
    }
 
    public function testGetUserSessionIdByEmail()
    {
        $response = $this->http->request('GET', 'api/usuario/jblanco@cinex.com.ve');
        $this->assertEquals(200, $response->getStatusCode());
    }      

    public function testGetUserById()
    {
        $response = $this->http->request('GET', 'api/usuario/getusuariobyid/usuarioid/prueba');
        $this->assertEquals(200, $response->getStatusCode());
    }   
    
}

