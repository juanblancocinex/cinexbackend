<?php
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class LoyaltySoapControllerTest extends WebTestCase
{
    private $http;

    public function setUp()
    {
        $client = static::createClient();
        $servidorTest = $client->getKernel()->getContainer()->getParameter('servidortest');
        $this->http = new GuzzleHttp\Client(['base_uri' => $servidorTest]);
    }

    public function tearDown() {
        $this->http = null;
    }
  
    public function testGetMember()
    {
        $response = $this->http->request('GET', 'api/loyalty/getmember/23334444');
        $this->assertEquals(200, $response->getStatusCode());
    
    }

    public function testCreateMember()
    {
        $response = $this->http->request('POST', 'api/loyalty/createmember', [
            'form_params' => ['sessionid' => 'LID',
            'firstname' => 'Prueba']
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testGetMemberSearch()
    {
        $response = $this->http->request('GET', 'api/loyalty/getmembersearch/usersessionid/22222/cardnumber/2222');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testGetMemberActivacionCode()
    {
        $response = $this->http->request('GET', 'api/loyalty/getactivacioncode/344444');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testGetMemberItemList()
    {
        $response = $this->http->request('GET', 'api/loyalty/getMemberItemList/444444');
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    public function testUpdateMember()
    {
        $response = $this->http->request('PUT', 'api/loyalty/updatemember', [
            'form_params' => ['sessionid' => 'LID',
            'firstname' => 'Prueba']
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }
   
    public function testSetMembershipActivated()
    {
        $response = $this->http->request('PUT', 'api/loyalty/membershipactivated', [
            'form_params' => ['sessionid' => 'LID',
            'memberid' => 'Prueba']
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }
   
    public function testActivateWebAccount()
    {
        $response = $this->http->request('GET', 'api/loyalty/activatewebaccount/memberid/2222/activationcode/222');                                                    
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testValidateMember()
    {
        $response = $this->http->request('GET', 'api/loyalty/validatemember/usersessionid/22222/cardnumber/2222');                                                    
        $this->assertEquals(200, $response->getStatusCode());
    } 
    

    public function testGetWebAccountActivationCode()
    {
        $response = $this->http->request('GET', 'api/loyalty/getwebaccountactivationcode/222222');                                                    
        $this->assertEquals(200, $response->getStatusCode());
    } 
    

}

