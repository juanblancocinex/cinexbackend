<?php
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class MotivosContactoControllerTest extends WebTestCase
{
    private $http;

    public function setUp()
    {
        $client = static::createClient();
        $servidorTest = $client->getKernel()->getContainer()->getParameter('servidortest');
        $this->http = new GuzzleHttp\Client(['base_uri' => $servidorTest]);
    }

    public function tearDown() {
        $this->http = null;
    }
  
    public function testComplejos()
    {
        $response = $this->http->request('GET', 'api/contactos/motivos');
        $this->assertEquals(200, $response->getStatusCode());
    
    }
}

